/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.Core.Xml;

namespace Composite.AspNet.Razor
{
    /// <summary>
    /// Base class for c1 functions based on razor 
    /// </summary>
    /// 

    public interface IRazorFunction
    {
        String GetFunctionDescription();
        Type GetFunctionReturnType();
    }

    public abstract class RazorFunction : RazorFunctionBase<Object>
    {
    }

    public abstract class RazorFunctionBase<T> : CompositeC1WebPage<T>, IRazorFunction where T : class
    {
        /// <summary>
        /// Gets the function description. Override this to make a custom description.
        /// </summary>
        /// <example>
        ///public override string FunctionDescription
        ///{
        ///    get { return "Will show recent Twitter activity for a given keyword."; }
        ///}
        /// </example>
        public virtual string FunctionDescription
        {
            get { return string.Empty; }
        }

        /// <summary>
        /// Gets the return type. By default this is XhtmlDocument (html). Override this to set another return type, like string or XElement.
        /// </summary>
        /// <example>
        ///public override Type FunctionReturnType
	    ///{
        ///    get { return typeof(string); }
        ///}
        /// </example>
        public virtual Type FunctionReturnType
        {
            get { return typeof (XhtmlDocument); }
        }

        public String GetFunctionDescription()
        {
            return FunctionDescription;
        }

        public Type GetFunctionReturnType()
        {
            return FunctionReturnType;
        }
    }
}
