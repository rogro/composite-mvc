/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Web;
using System.Web.WebPages;
using System.Xml.Linq;
using Composite.Data;
using System.Threading;
using System.Collections.Generic;
using Composite.Functions;
using Composite.Core.WebClient.Renderings.Page;
using System.Web.Mvc;

namespace Composite.AspNet.Razor
{
    /// <summary>
    /// Defines a composite C1 razor control
    /// </summary>
	public abstract class CompositeC1WebPage<T> : WebViewPage<T>, IDisposable
	{
		private bool _disposed;
		private readonly DataConnection _data;

        /// <summary>
        /// Initializes a new instance of the <see cref="CompositeC1WebPage"/> class.
        /// </summary>
        protected CompositeC1WebPage()
        {
            _data = new DataConnection();
        }

        /// <summary>
        /// Gets a <see cref="DataConnection"/> object.
        /// </summary>
		public DataConnection Data
		{
            get { return _data; }
		}

        /// <summary>
        /// Gets a <see cref="SitemapNavigator"/> object.
        /// </summary>
		public SitemapNavigator Sitemap
		{
			get { return Data.SitemapNavigator; }
		}


        /// <summary>
        /// Gets the home page node.
        /// </summary>
		public PageNode HomePageNode
		{
			get { return Sitemap.CurrentHomePageNode; }
		}


        /// <summary>
        /// Gets the current page node.
        /// </summary>
		public PageNode CurrentPageNode
		{
			get { return Sitemap.CurrentPageNode; }
		}

        /// <summary>
        /// Updates ViewData with the specified Model.
        /// </summary>
        public void SetViewModel(T Model)
        {
            this.ViewData = new ViewDataDictionary<T>(Model);

            /* also update ViewContext with the new ViewData */
            this.ViewContext.ViewData = this.ViewData;
        }

        /// <summary>
        /// Includes a named Page Template Feature. Page Template Feature are managed in '~/App_Data/PageTemplateFeatures' 
        /// or via the C1 Console's Layout perspective. They contain html and functional snippets.
        /// </summary>
        /// <param name="featureName">Name of the Page Template Feature to include. Names do not include an extension.</param>
        /// <returns></returns>
        public IHtmlString PageTemplateFeature(string featureName)
        {
            return Html.C1().GetPageTemplateFeature(featureName);
        }


        /// <summary>
        /// Renders the specified XNode.
        /// </summary>
        /// <param name="xNode">The <see cref="XNode">XNode</see>.</param>
        /// <returns></returns>
        public IHtmlString Markup(XNode xNode)
        {
            return Html.C1().Markup(xNode);
        }


        /// <summary>
        /// Gets to letter ISO Language Name representing the pages language - use this like &lt;html lang="@Lang" /&gt;
        /// </summary>
        public string Lang
        {
            get
            {
                return Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
            }
        }


        /// <summary>
        /// Gets the function context container.
        /// </summary>
        public FunctionContextContainer FunctionContextContainer
        {
            get { return GetFunctionContext(); }
        }


		/// <summary>
		/// Executes a C1 Function.
		/// </summary>
		/// <param name="name">Function name.</param>
		/// <returns></returns>
		public IHtmlString Function(string name)
		{
			return Function(name, null);
		}

		/// <summary>
		/// Executes a C1 Function.
		/// </summary>
		/// <param name="name">Function name.</param>
		/// <param name="parameters">The parameters.</param>
		/// <returns></returns>
		public IHtmlString Function(string name, object parameters)
		{
            return Function(name, Functions.ObjectToDictionary(parameters));
		}

		/// <summary>
		/// Executes a C1 Function.
		/// </summary>
		/// <param name="name">Function name.</param>
		/// <param name="parameters">The parameters.</param>
		/// <returns></returns>
		public IHtmlString Function(string name, IDictionary<string, object> parameters)
		{
            return Html.C1().Function(name, parameters, GetFunctionContext());
		}


        private FunctionContextContainer GetFunctionContext()
        {
            if (!PageData.ContainsKey(RazorHelper.PageContext_FunctionContextContainer))
            {
                return null;
            }

            return PageData[RazorHelper.PageContext_FunctionContextContainer];
        }

        /// <exclude />
		public void Dispose()
		{
			Dispose(true);

			GC.SuppressFinalize(this);
		}

        /// <exclude />
		protected virtual void Dispose(bool disposing)
		{
			if (!_disposed)
			{
				if (disposing)
				{
                    _data.Dispose();
				}

				_disposed = true;
			}
		}

        /// <exclude />
		~CompositeC1WebPage()
		{
			Dispose(false);
		}
	}
}
