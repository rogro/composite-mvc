/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Specialized;
using System.Web;
using System.Web.Hosting;

namespace Composite.AspNet.Razor
{
    internal class NoHttpRazorRequest : HttpRequestBase
    {
        private NameValueCollection _form;
        private NameValueCollection _queryString;
        private NameValueCollection _headers;
        private NameValueCollection _params;
        private NameValueCollection _serverVariables;
        private HttpCookieCollection _cookies;

        public override string ApplicationPath
        {
            get { return HostingEnvironment.ApplicationVirtualPath; }
        }

        public override string PhysicalApplicationPath
        {
            get { return HostingEnvironment.ApplicationPhysicalPath; }
        }

        public override HttpCookieCollection Cookies
        {
            get { return _cookies ?? (_cookies = new HttpCookieCollection()); }
        }

        public override bool IsLocal
		{
			get { return false; }
		}

        public override NameValueCollection Form
        {
            get { return _form ?? (_form = new NameValueCollection()); }
        }

        public override NameValueCollection Headers
        {
            get { return _headers ?? (_headers = new NameValueCollection());}
        }

        public override string HttpMethod
        {
            get { return "GET"; }
        }

        public override bool IsAuthenticated
        {
            get { return false; }
        }

        public override bool IsSecureConnection
        {
            get { return false; }
        }

        public override string this[string key]
        {
            get { return null; }
        }

        public override NameValueCollection Params
        {
            get { return _params ?? (_params = new NameValueCollection()); }
        }

        public override string PathInfo
        {
            get { return null; }
        }

        public override NameValueCollection QueryString
        {
            get { return _queryString ?? (_queryString = new NameValueCollection()); }
        }

        public override string RequestType 
        {
            get { return HttpMethod; }
            set { throw new NotSupportedException();} 
        }

        public override NameValueCollection ServerVariables
        {
            get { return _serverVariables ?? (_serverVariables = new NameValueCollection()); }
        }

        public override string UserAgent
        {
            get { return ""; }
        }
    }
}
