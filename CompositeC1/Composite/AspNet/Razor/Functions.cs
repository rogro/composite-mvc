/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using Composite.Functions;
using Composite.Core.Extensions;

namespace Composite.AspNet.Razor
{
    /// <summary>
    /// Utility class for working with C1 functions from Razor code
    /// </summary>
	public static class Functions
	{
        /// <summary>
        /// Executes the function.
        /// </summary>
        /// <param name="name">The name.</param>
		public static object ExecuteFunction(string name)
		{
			return ExecuteFunction(name, null);
		}

        /// <summary>
        /// Executes the function.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
		public static object ExecuteFunction(string name, object parameters)
		{
			return ExecuteFunction(name, ObjectToDictionary(parameters));
		}

        /// <summary>
        /// Executes the function.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
		public static object ExecuteFunction(string name, IDictionary<string, object> parameters)
		{
            return ExecuteFunction(name, parameters, new FunctionContextContainer());
		}

        /// <summary>
        /// Executes the function.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="functionContextContainer">The function context container</param>
        /// <returns></returns>
        public static object ExecuteFunction(string name, IDictionary<string, object> parameters, FunctionContextContainer functionContextContainer)
        {
            IFunction function;
            if (!FunctionFacade.TryGetFunction(out function, name))
            {
                throw new InvalidOperationException("Failed to load function '{0}'".FormatWith(name));
            }

            functionContextContainer = functionContextContainer ?? new FunctionContextContainer();

            return FunctionFacade.Execute<object>(function, parameters, functionContextContainer);
        }

        /// <summary>
        /// Builds a dictionary for object properties' values.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
		public static IDictionary<string, object> ObjectToDictionary(object instance)
		{
			if (instance == null)
			{
				return null;
			}

			var dictionary = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

			foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(instance))
			{
				object obj = descriptor.GetValue(instance);

				dictionary.Add(descriptor.Name, obj);
			}

			return dictionary;
		}
	}
}
