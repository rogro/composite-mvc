/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Web;
//using System.Web.Instrumentation;
using System.Web.WebPages;
using System.Xml;
using System.Xml.Linq;
//using Composite.Core.Extensions;
//using Composite.Core.IO;
using Composite.Core.Types;
using Composite.Core.Xml;
using Composite.Functions;
using System.Web.Mvc;
using Composite.Core.WebClient.Renderings.Page;
using Composite.AspNet.Mvc;

namespace Composite.AspNet.Razor
{
    /// <summary>
    /// Exposes method to execute razor pages.
    /// </summary>
    public static class RazorHelper
    {
        internal static readonly string PageContext_FunctionContextContainer = "C1.FunctionContextContainer";

        /// <summary>
        /// Executes the razor page.
        /// </summary>
        /// <param name="virtualPath">The virtual path.</param>
        /// <param name="setParameters">Delegate to set the parameters.</param>
        /// <param name="resultType">The type of the result.</param>
        /// <param name="functionContextContainer">The function context container</param>
        /// <returns></returns>
        public static object ExecuteRazorPage(
            string virtualPath, 
            Action<WebPageBase> setParameters,
            Type resultType,
            FunctionContextContainer functionContextContainer)
        {
            WebPageBase webPage = null;
            try
            {
                webPage = WebPageBase.CreateInstanceFromVirtualPath(virtualPath);

                return ExecuteRazorPage(webPage, setParameters, resultType, functionContextContainer);
            }
            finally
            {
                if (webPage is IDisposable)
                {
                    (webPage as IDisposable).Dispose();
                }
            }
        }

        /// <summary>
        /// Executes the razor page.
        /// </summary>
        /// <param name="webPage">The web page.</param>
        /// <param name="setParameters">Delegate to set the parameters.</param>
        /// <param name="resultType">The type of the result.</param>
        /// <param name="functionContextContainer">The function context container</param>
        /// <returns></returns>
        public static object ExecuteRazorPage(
            WebPageBase webPage,
            Action<WebPageBase> setParameters, 
            Type resultType, 
            FunctionContextContainer functionContextContainer)
        {
            HttpContext currentContext = HttpContext.Current;
            HttpContextBase httpContext;

            if (currentContext == null)
            {
                httpContext = new NoHttpRazorContext();
            }
            else
            {
                httpContext = new HttpContextWrapper(currentContext);
            }

            var controllerContext = httpContext.Items["controller_context"] as ControllerContext;
            if (controllerContext == null)
            {
                controllerContext = new ControllerContext(httpContext.Request.RequestContext, new StubController());
            }

            var startPage = ViewStartPage.GetStartPage(webPage, "_PageStart", new[] { "cshtml" });
            var pageContext = new WebPageContext(httpContext, webPage, webPage);

            var viewContext = new ViewContext()
            {
                ViewData = PageRenderer.CurrentModel != null ? new ViewDataDictionary(PageRenderer.CurrentModel) : new ViewDataDictionary(),
                TempData = new TempDataDictionary(),
                RouteData = controllerContext.RouteData,
                HttpContext = controllerContext.HttpContext,
                RequestContext = controllerContext.RequestContext,
                Controller = controllerContext.Controller,
            };

            var mvcPage = webPage as WebViewPage;

            mvcPage.ViewContext = viewContext;
            
            try
            {
                mvcPage.ViewData = new ViewDataDictionary<object>(viewContext.ViewData.Model);
            }
            catch { }

            if (functionContextContainer != null)
            {
                pageContext.PageData.Add(PageContext_FunctionContextContainer, functionContextContainer);
            }

            if (setParameters != null)
            {
                setParameters(webPage);
            }

            mvcPage.InitHelpers();
                
            var sb = new StringBuilder();
            using (var writer = new StringWriter(sb))
            {
                webPage.ExecutePageHierarchy(pageContext, writer, startPage);
            }

            string output = sb.ToString();
            

			if (resultType == typeof(XhtmlDocument))
			{
			    if (string.IsNullOrWhiteSpace(output)) return new XhtmlDocument();

				try
                {
                    return OutputToXhtmlDocument(output);
				}
				catch (XmlException ex)
				{
				    string[] codeLines = output.Split(new [] { Environment.NewLine, "\n" }, StringSplitOptions.None);

				    XhtmlErrorFormatter.EmbedSouceCodeInformation(ex, codeLines, ex.LineNumber);

				    throw;
				}
			}

			return ValueTypeConverter.Convert(output, resultType);
        }

        private static XhtmlDocument OutputToXhtmlDocument(string output)
        {
            var nodes = new List<XNode>();

            using (var stringReader = new StringReader(output))
            {
                var xmlReaderSettings = new XmlReaderSettings
                {
                    IgnoreWhitespace = true,
                    DtdProcessing = DtdProcessing.Parse,
                    MaxCharactersFromEntities = 10000000,
                    XmlResolver = null,
                    ConformanceLevel = ConformanceLevel.Fragment // Allows multipe XNode-s
                };

                using (var xmlReader = XmlReader.Create(stringReader, xmlReaderSettings))
                {
                    xmlReader.MoveToContent();

                    while (!xmlReader.EOF)
                    {
                        XNode node = XNode.ReadFrom(xmlReader);
                        nodes.Add(node);
                    }
                }
            }

            if (nodes.Count == 1 && nodes[0] is XElement && (nodes[0] as XElement).Name.LocalName == "html")
            {
                return new XhtmlDocument(nodes[0] as XElement);
            }

            var document = new XhtmlDocument();
            document.Body.Add(nodes);

            return document;
        }

        /// <summary>
        /// Executes the razor page.
        /// </summary>
        /// <typeparam name="ResultType">The result type.</typeparam>
        /// <param name="virtualPath">The virtual path.</param>
        /// <param name="setParameters">Delegate to set the parameters.</param>
        /// <param name="functionContextContainer">The function context container.</param>
        /// <returns></returns>
        public static ResultType ExecuteRazorPage<ResultType>(
            string virtualPath, 
            Action<WebPageBase> setParameters, 
            FunctionContextContainer functionContextContainer = null) where ResultType: class
        {
            return (ResultType) ExecuteRazorPage(virtualPath, setParameters, typeof(ResultType), functionContextContainer);
        }
    }
}
