/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Composite.Data.Types;
using Composite.Core.Caching;

namespace Composite.AspNet
{
    /// <summary>
    /// Allows switching context of SiteMap
    /// </summary>
    public class SiteMapContext: IDisposable
    {
        /// <summary>
        /// Gets the root page.
        /// </summary>
        public IPage RootPage { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SiteMapContext"/> class.
        /// </summary>
        /// <param name="rootPage">The root page.</param>
        public SiteMapContext(IPage rootPage)
        {
            RootPage = rootPage;

            SiteMapStack.Push(this);
        }

        /// <summary>
        /// Gets the current SiteMapContext. Can be null.
        /// </summary>
        /// <value>
        /// The current context.
        /// </value>
        public static SiteMapContext Current
        {
            get
            {
                var currentStack = SiteMapStack;

                return currentStack.Any() ? currentStack.Peek() : null;
            }
        }

        /// <exclude />
        public void Dispose()
        {
            var top = SiteMapStack.Pop();
            Verify.That(object.ReferenceEquals(top, this), "SiteMapContext weren't disposed properly");
        }



        private static Stack<SiteMapContext> SiteMapStack
        {
            get
            {
                return RequestLifetimeCache.GetCachedOrNew<Stack<SiteMapContext>>("SiteMapContext:Stack");
            }
        }

    }
}
