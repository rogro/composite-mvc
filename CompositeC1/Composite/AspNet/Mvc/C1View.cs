﻿using Composite.Core.Routing;
using Composite.Core.Routing.Pages;
using Composite.Core.WebClient.Renderings;
using Composite.Core.WebClient.Renderings.Page;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace Composite.AspNet.Mvc
{
    /// <summary>
    /// C1 Mvc view action
    /// </summary>
    public class C1View<T> : ActionResult
    {
        T _modelData;
        String _path;
        RenderingContext _renderingContext;

        /// <summary>
        /// Initializes a C1ModelPage action
        /// </summary>
        /// <param name="ModelData">Model data</param>
        /// <param name="Path">Custom path to page used for rendering</param>
        public C1View(T ModelData = default(T), String Path = null)
        {
            _modelData = ModelData;
            _path = Path;
        }
        /// <summary>
        /// Executes this action
        /// </summary>
        /// <param name="context"></param>
        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.Items.Add("controller_context", context);

            PageUrlData pageUrlData = Composite.Core.Routing.PageUrls.ParseUrl(_path ?? context.HttpContext.Request.Path);
            C1PageRoute.PageUrlData = pageUrlData;

            var renderPage = new RenderPage();

            _renderingContext = RenderingContext.InitializeFromHttpContext(_modelData, renderPage);

            var markupBuilder = new StringBuilder();
            var sw = new StringWriter(markupBuilder);
            var htmlWriter = new HtmlTextWriter(sw);

            context.HttpContext.Server.Execute(renderPage, htmlWriter, false);

            string xhtml = _renderingContext.ConvertInternalLinks(markupBuilder.ToString());
            xhtml = _renderingContext.FormatXhtml(xhtml);

            context.HttpContext.Response.Write(xhtml);

            _renderingContext.Dispose();
        }

        private class RenderPage : System.Web.UI.Page
        {
        }
    }
}
