﻿using Composite.Core.Routing;
using Composite.Data.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Routing;

namespace Composite.AspNet.Mvc
{
    /// <summary>
    /// Route constraint based on Composite C1 PageType
    /// </summary>
    public class C1PageTypeRouteConstraint : IRouteConstraint
    {
        private readonly Guid validPageType = Guid.Empty;

        public C1PageTypeRouteConstraint(string typeName)
        {
            using (Composite.Data.DataConnection Conn = new Data.DataConnection())
            {
                var pageType = Conn.Get<IPageType>().FirstOrDefault(p => p.Name.ToLowerInvariant().Equals(typeName.ToLowerInvariant()));
                    if (pageType != null)
                    {
                        validPageType = pageType.Id;
                    }
            }
        }

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            var path = httpContext.Request.Path;
            var matchesPageType = false;

            PageUrlData pageUrlData = Composite.Core.Routing.PageUrls.ParseUrl(path);
            if (pageUrlData != null)
            {
                using (Composite.Data.DataConnection conn = new Data.DataConnection())
                {
                    matchesPageType = conn.Get<IPage>().Where(p => p.Id.Equals(pageUrlData.PageId) && validPageType.Equals(p.PageTypeId)).Count() > 0;
                }
            }

            return matchesPageType;
        }
    }
}
