﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc.Routing;
using System.Web.Routing;

namespace Composite.AspNet.Mvc
{
    /// <summary>
    /// Routing attribute which allows specifying a Composite C1 PageType constraint
    /// </summary>
    public class C1PageTypeRouteAttribute : RouteFactoryAttribute
    {
	    public string PageType
	    {
		    get;
		    private set;
	    }

        public C1PageTypeRouteAttribute(string template, string type)
		    : base(template)
	    {
		    PageType = type;
	    }

        public override RouteValueDictionary Constraints
        {
            get
            {
                var constraints = new RouteValueDictionary();
                constraints.Add("pagetype", new C1PageTypeRouteConstraint(PageType));

                return constraints;
            }
        }
    }
}
