/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.Data.DynamicTypes;


namespace Composite.Data
{
    /// <summary>
    /// Specifies what physical store type should be used to store this property.
    /// </summary>
    /// <example> This sample shows how to use the StoreFieldType attribute. 
    /// Here a string field with a maximum of 40 characters.
    /// <code>
    /// // data interface attributes ...
    /// interface IMyDataType : IData
    /// {
    ///     [StoreFieldType(PhysicalStoreFieldType.String, 40)]
    ///     [ImmutableFieldId("{D75EA67F-AD14-4BAB-8547-6D87002809F1}")]
    ///     string ProductName { get; set; }
    ///     
    ///     // more data properties ...
    ///     
    /// }
    /// </code>
    /// </example>    
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public sealed class StoreFieldTypeAttribute : Attribute
    {
        private StoreFieldTypeAttribute()
        {
            this.IsNullable = false;
        }


        /// <summary>
        /// Specifies what physical store type should be used to store this property.
        /// This overload is intended for int, long, large string, date time, guid and bool.
        /// For decimal and string, use other overload where you can specify length values also.
        /// </summary>
        /// <param name="physicalStoreFieldType">PhysicalStoreFieldType to use</param>
        public StoreFieldTypeAttribute(PhysicalStoreFieldType physicalStoreFieldType)
            : this()
        {
            switch (physicalStoreFieldType)
            {
                case PhysicalStoreFieldType.Integer:
                    this.StoreFieldType = StoreFieldType.Integer;
                    return;
                case PhysicalStoreFieldType.Long:
                    this.StoreFieldType = StoreFieldType.Long;
                    return;
                case PhysicalStoreFieldType.LargeString:
                    this.StoreFieldType = StoreFieldType.LargeString;
                    return;
                case PhysicalStoreFieldType.DateTime:
                    this.StoreFieldType = StoreFieldType.DateTime;
                    return;
                case PhysicalStoreFieldType.Guid:
                    this.StoreFieldType = StoreFieldType.Guid;
                    return;
                case PhysicalStoreFieldType.Boolean:
                    this.StoreFieldType = StoreFieldType.Boolean;
                    return;
            }

            throw new ArgumentException(string.Format("[{0}] - field type {1}.{2} require some aditional arguments in the attribute constructor.",
                this.GetType().Name, typeof(PhysicalStoreFieldType).Name, physicalStoreFieldType));
        }


        /// <summary>
        /// Specifies what physical store type should be used to store this property.
        /// This overload is intended for string.
        /// </summary>
        /// <param name="physicalStoreFieldType">PhysicalStoreFieldType to use - PhysicalStoreFieldType.String expected</param>
        /// <param name="maxLength">Number of characters to reserve in physical store</param>
        public StoreFieldTypeAttribute(PhysicalStoreFieldType physicalStoreFieldType, int maxLength)
            : this()
        {
            switch (physicalStoreFieldType)
            {
                case PhysicalStoreFieldType.String:
                    this.StoreFieldType = StoreFieldType.String(maxLength);
                    return;
            }

            throw new ArgumentException(string.Format("[{0}] - field type {1}.{2} does not take an int argument in the attribute constructor.",
                this.GetType().Name, typeof(PhysicalStoreFieldType).Name, physicalStoreFieldType));
        }


        /// <summary>
        /// Specifies what physical store type should be used to store this property.
        /// This overload is intended for decimal.
        /// </summary>
        /// <param name="physicalStoreFieldType">PhysicalStoreFieldType to use - PhysicalStoreFieldType.Decimal expected</param>
        /// <param name="numericPrecision">Numeric precision for decimal</param>
        /// <param name="numericScale">Numeric scale for decimal</param>
        public StoreFieldTypeAttribute(PhysicalStoreFieldType physicalStoreFieldType, int numericPrecision, int numericScale)
            : this()
        {
            switch (physicalStoreFieldType)
            {
                case PhysicalStoreFieldType.Decimal:
                    this.StoreFieldType = StoreFieldType.Decimal(numericPrecision, numericScale);
                    return;
            }

            throw new ArgumentException(string.Format("[{0}] - field type {1}.{2} does not take two 'int' arguments in the attribute constructor.",
                this.GetType().Name, typeof(PhysicalStoreFieldType).Name, physicalStoreFieldType));
        }


        /// <exclude />
        public StoreFieldType StoreFieldType { get; private set; }


        /// <summary>
        /// When true the data store can allow null values. Default is false.
        /// </summary>
        public bool IsNullable { get; set; }
    }
}
