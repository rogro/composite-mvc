/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Composite.Core;
using Composite.Core.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Validation;


namespace Composite.Data.Validation
{
    internal class ValidationFacadeImpl : IValidationFacade
    {
        private ResourceLocker<Resources> _resourceLocker = new ResourceLocker<Resources>(new Resources(), Resources.Initialize);


        public ValidationResults Validate<T>(T data)
            where T : class, IData
        {
            ValidationResults validationResults = Microsoft.Practices.EnterpriseLibrary.Validation.Validation.ValidateFromAttributes<T>(data);

            return validationResults;
        }



        public ValidationResults Validate(Type interfaceType, IData data)
        {
            MethodInfo methodInfo;

            using (_resourceLocker.Locker)
            {
                if (_resourceLocker.Resources.MethodCache.TryGetValue(interfaceType, out methodInfo) == false)
                {
                    methodInfo =
                        (from mi in typeof(ValidationFacade).GetMethods()
                         where (mi.Name == "Validate") &&
                               (mi.ContainsGenericParameters)
                         select mi).First();

                    methodInfo = methodInfo.MakeGenericMethod(new Type[] { interfaceType });

                    _resourceLocker.Resources.MethodCache.Add(interfaceType, methodInfo);
                }
            }

            ValidationResults validationResults;

            try
            {
                validationResults = (ValidationResults)methodInfo.Invoke(null, new object[] { data });
            }
            catch (TargetInvocationException ex)
            {
                Log.LogError("ValidationFacade", ex);
                validationResults = new ValidationResults();
                validationResults.AddResult(new ValidationResult("Exception thrown while validating. Please check field values.", data, "", "", null));
            }

            return validationResults;
        }



        public void OnFlush()
        {
            _resourceLocker.ResetInitialization();
        }



        private sealed class Resources
        {
            public Dictionary<Type, MethodInfo> MethodCache { get; set; }

            public static void Initialize(Resources resources)
            {
                resources.MethodCache = new Dictionary<Type, MethodInfo>();
            }
        }
    }
}
