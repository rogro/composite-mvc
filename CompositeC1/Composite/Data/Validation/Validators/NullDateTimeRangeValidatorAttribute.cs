/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Globalization;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

namespace Composite.Data.Validation.Validators
{
    /// <summary>
    /// Validator rule for data type properties.
    /// Validate that a Nullable&lt;DateTime&gt; - when not null - has a value than falls within a minimum and maximum value. 
    /// </summary>    
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Method | AttributeTargets.Parameter, AllowMultiple = true, Inherited = false)]
    public sealed class NullDateTimeRangeValidatorAttribute : ValueValidatorAttribute
    {
        private readonly DateTime _lowerBound;
        private readonly DateTime _upperBound;
        private readonly RangeBoundaryType _upperBoundType;
        private readonly RangeBoundaryType _lowerBoundType;


        /// <exclude />
        public NullDateTimeRangeValidatorAttribute(string upperBound)
            : this(ConvertToISO8601Date(upperBound))
        {
        }

        /// <exclude />
        public NullDateTimeRangeValidatorAttribute(DateTime upperBound)
            : this(DateTime.MinValue, RangeBoundaryType.Ignore, upperBound, RangeBoundaryType.Inclusive)
        {
        }

        /// <exclude />
        public NullDateTimeRangeValidatorAttribute(string lowerBound, string upperBound)
            : this(ConvertToISO8601Date(lowerBound), ConvertToISO8601Date(upperBound))
        {
        }

        /// <exclude />
        public NullDateTimeRangeValidatorAttribute(DateTime lowerBound, DateTime upperBound)
            : this(lowerBound, RangeBoundaryType.Inclusive, upperBound, RangeBoundaryType.Inclusive)
        {
        }

        /// <exclude />
        public NullDateTimeRangeValidatorAttribute(string lowerBound, RangeBoundaryType lowerBoundType, string upperBound, RangeBoundaryType upperBoundType)
            : this(ConvertToISO8601Date(lowerBound), lowerBoundType, ConvertToISO8601Date(upperBound), upperBoundType)
        {
        }

        /// <exclude />
        public NullDateTimeRangeValidatorAttribute(DateTime lowerBound, RangeBoundaryType lowerBoundType, DateTime upperBound, RangeBoundaryType upperBoundType)
        {
            _lowerBound = lowerBound;
            _lowerBoundType = lowerBoundType;
            _upperBound = upperBound;
            _upperBoundType = upperBoundType;
        }

        /// <exclude />
        protected override Validator DoCreateValidator(Type targetType)
        {
            return new NullableDateTimeRangeValidator(new RangeValidator<DateTime>(
                _lowerBound, _lowerBoundType,
                _upperBound, _upperBoundType));
        }

        private static DateTime ConvertToISO8601Date(string iso8601DateString)
        {
            if (string.IsNullOrEmpty(iso8601DateString))
            {
                return new DateTime();
            }

            return DateTime.ParseExact(iso8601DateString, "s", CultureInfo.InvariantCulture);
        }


        private class NullableDateTimeRangeValidator : Validator<DateTime?>
        {
            private readonly RangeValidator<DateTime> _innerValidator;

            public NullableDateTimeRangeValidator(RangeValidator<DateTime> innerValidator)
                : base(null, null)
            {
                _innerValidator = innerValidator;
            }

            protected override string DefaultMessageTemplate
            {
                get { return _innerValidator.MessageTemplate; }
            }

            protected override void DoValidate(DateTime? objectToValidate, object currentTarget, string key, ValidationResults validationResults)
            {
                if (objectToValidate == null)
                {
                    return;
                }

                validationResults.AddAllResults(_innerValidator.Validate(objectToValidate.Value));
            }
        }
    }
}
