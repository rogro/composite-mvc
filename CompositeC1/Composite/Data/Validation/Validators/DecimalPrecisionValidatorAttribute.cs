/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;


namespace Composite.Data.Validation.Validators
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Method | AttributeTargets.Parameter, AllowMultiple = true, Inherited = false)]
    public sealed class DecimalPrecisionValidatorAttribute : Microsoft.Practices.EnterpriseLibrary.Validation.Validators.ValueValidatorAttribute
    {
        /// <exclude />
        [Obsolete("Use constructor that allow for both precision and scale")]
        public DecimalPrecisionValidatorAttribute(int digits)
        {
            this.Precision = digits + 10; // uneducated guess
            this.Scale = digits;
        }


        /// <summary>
        /// Creates a decimal precision validator
        /// </summary>
        /// <param name="precision">Digits in total</param>
        /// <param name="scale">Digits after decimal point</param>
        public DecimalPrecisionValidatorAttribute(int precision, int scale)
        {
            this.Precision = precision;
            this.Scale = scale;
        }


        /// <summary>
        /// Number of digits after decimal point - normally you would call this scale.
        /// </summary>
        [Obsolete("Use 'Scale'")]
        public int Digits
        {
            get { return this.Scale; }
        }


        /// <summary>
        /// Number of digits after the separator
        /// </summary>
        public int Scale
        {
            get;
            private set;
        }

        /// <summary>
        /// Number of digits in total
        /// </summary>
        public int Precision
        {
            get;
            private set;
        }


        /// <exclude />
        protected override Microsoft.Practices.EnterpriseLibrary.Validation.Validator DoCreateValidator(Type targetType)
        {
            return new DecimalPrecisionValidator();
        }
    }
}
