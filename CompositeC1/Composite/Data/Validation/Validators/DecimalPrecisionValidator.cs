/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Globalization;
using Composite.Core.Extensions;
using Composite.Core.ResourceSystem;
using Composite.Core.Types;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using System.Collections.Generic;
using System;
using System.Linq;

namespace Composite.Data.Validation.Validators
{
    internal sealed class DecimalPrecisionValidator : Validator
    {
        public DecimalPrecisionValidator()
            : base("To many digits", "decimal")
        {
        }


        protected override string DefaultMessageTemplate
        {
            get { return "To many digits"; }
        }


        protected override void DoValidate(object objectToValidate, object currentTarget, string key, ValidationResults validationResults)
        {
            if(objectToValidate == null)
            {
                // Skipping valudation if optional decimal is a null
                return;
            }

            var number = (decimal)objectToValidate;

            List<DecimalPrecisionValidatorAttribute> attributes = currentTarget.GetType().GetProperty(key).GetCustomAttributesRecursively<DecimalPrecisionValidatorAttribute>().ToList();
            var validatiorAttribute = attributes[0];

            if (number != Decimal.Round(number, validatiorAttribute.Scale))
            {

                LogValidationResult(validationResults, GetString("Validation.Decimal.SymbolsAfterPointAllowed").FormatWith(validatiorAttribute.Scale), currentTarget, key);
                return;
            }

            string str = number.ToString(CultureInfo.InvariantCulture);
            int separatorIndex = str.IndexOf('.');
            if(separatorIndex > 0)
            {
                str = str.Substring(0, separatorIndex);
            }

            if (str.StartsWith("-")) str = str.Substring(1);

            int allowedDigitsBeforeSeparator = validatiorAttribute.Precision - validatiorAttribute.Scale;
            if (str.Length > allowedDigitsBeforeSeparator)
            {
                LogValidationResult(validationResults, GetString("Validation.Decimal.SymbolsBeforePointAllowed").FormatWith(allowedDigitsBeforeSeparator), currentTarget, key);
            }
        }

        private static string GetString(string key)
        {
            return StringResourceSystemFacade.GetString("Composite.Management", key);
        }
    }
}
