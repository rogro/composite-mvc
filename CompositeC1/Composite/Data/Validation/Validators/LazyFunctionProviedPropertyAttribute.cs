/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using Composite.Core;
using Composite.Functions;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;


namespace Composite.Data.Validation.Validators
{
    /// <summary>
    /// This is for internal use only! It is a work around to function providing
    /// a IPropertyValidatorBuilder that provides CodeDOM when the data type
    /// interface is code generated. This means that the functions should
    /// be loaded BEFORE the interface is code generated, which is bad architecture.
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Method | AttributeTargets.Parameter, AllowMultiple = true, Inherited = false)]
    public class LazyFunctionProviedPropertyAttribute : ValueValidatorAttribute
    {
        private static MethodInfo _doCreateValidatorMethodInfo;
        private string FunctionMarkup { get; set; }


        /// <summary>
        /// </summary>
        /// <param name="functionMarkup"></param>
        /// <exclude />
        public LazyFunctionProviedPropertyAttribute(string functionMarkup)
        {
            FunctionMarkup = functionMarkup;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        /// <exclude />
        protected override Validator DoCreateValidator(Type targetType)
        {
            try
            {
                BaseRuntimeTreeNode node = FunctionFacade.BuildTree(XElement.Parse(FunctionMarkup));
                
                IPropertyValidatorBuilder propertyValidatorBuilder = node.GetValue<IPropertyValidatorBuilder>();

                ValidatorAttribute validatorAttribute = (ValidatorAttribute)propertyValidatorBuilder.GetAttribute();

                Validator validator = (Validator)DoCreateValidatorMethodInfo.Invoke(validatorAttribute, new object[] { targetType });

                return validator;
            }
            catch (Exception ex)
            {
                string message = string.Format("Validator function markup parse / execution failed with the following error: '{0}'. The validator attribute is dropped.", ex.Message);
                Log.LogError("LazyFunctionProviedPropertyAttribute", message);
            }

            return new LazyFunctionProviedPropertyValidator();
        }



        private static MethodInfo DoCreateValidatorMethodInfo
        {
            get
            {
                if (_doCreateValidatorMethodInfo == null)
                {
                    _doCreateValidatorMethodInfo = 
                        typeof(ValidatorAttribute).GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).
                        Where(f => f.Name == "DoCreateValidator" && f.GetParameters().Length == 1).
                        Single();
                }

                return _doCreateValidatorMethodInfo;
            }
        }
    }
}
