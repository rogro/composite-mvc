/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Configuration;
using Composite.Core.Collections.Generic;
using Composite.Data.Validation.ClientValidationRules;
using Composite.Data.Validation.Plugins.ClientValidationRuleTranslator;
using Composite.Data.Validation.Plugins.ClientValidationRuleTranslator.Runtime;


namespace Composite.Data.Validation.Foundation.PluginFacades
{
    internal sealed class ClientValidationRuleTranslatorPluginFacadeImpl : IClientValidationRuleTranslatorPluginFacade
    {
        private ResourceLocker<Resources> _resourceLocker;


        public ClientValidationRuleTranslatorPluginFacadeImpl()
        {
            _resourceLocker = new ResourceLocker<Resources>(new Resources { Owner = this }, Resources.Initialize);
        }


        public IEnumerable<Type> GetSupportedAttributeTypes(string translatorName)
        {
            if (string.IsNullOrEmpty(translatorName)) throw new ArgumentNullException("translatorName");

                using (_resourceLocker.ReadLocker)
                {
                    IClientValidationRuleTranslator provider = GetClientValidationRuleTranslator(translatorName);

                    return provider.GetSupportedAttributeTypes();
                }
        }



        public ClientValidationRule Translate(string translatorName, Attribute attribute)
        {
            if (string.IsNullOrEmpty(translatorName)) throw new ArgumentNullException("translatorName");
            if (attribute == null) throw new ArgumentNullException("attribute");

                using (_resourceLocker.ReadLocker)
                {
                    IClientValidationRuleTranslator provider = GetClientValidationRuleTranslator(translatorName);

                    return provider.Translate(attribute);
                }
        }



        public void OnFlush()
        {
            _resourceLocker.ResetInitialization();
        }



        private IClientValidationRuleTranslator GetClientValidationRuleTranslator(string providerName)
        {
            using (_resourceLocker.Locker)
            {
                IClientValidationRuleTranslator provider;

                if (_resourceLocker.Resources.TranslatorCache.TryGetValue(providerName, out provider) == false)
                {
                    try
                    {
                        provider = _resourceLocker.Resources.Factory.Create(providerName);

                        _resourceLocker.Resources.TranslatorCache.Add(providerName, provider);
                    }
                    catch (ArgumentException ex)
                    {
                        HandleConfigurationError(ex);
                    }
                    catch (ConfigurationErrorsException ex)
                    {
                        HandleConfigurationError(ex);
                    }
                }

                return provider;
            }
        }



        private void HandleConfigurationError(Exception ex)
        {
            OnFlush();

            throw new ConfigurationErrorsException(string.Format("Failed to load the configuration section '{0}' from the configuration.", ClientValidationRuleTranslatorSettings.SectionName), ex);
        }



        private sealed class Resources
        {
            public ClientValidationRuleTranslatorFactory Factory { get; set; }
            public Dictionary<string, IClientValidationRuleTranslator> TranslatorCache { get; set; }
            public ClientValidationRuleTranslatorPluginFacadeImpl Owner { get; set; }

            public static void Initialize(Resources resources)
            {
                try
                {
                    resources.Factory = new ClientValidationRuleTranslatorFactory();
                }
                catch (NullReferenceException ex)
                {
                    resources.Owner.HandleConfigurationError(ex);
                }
                catch (ConfigurationErrorsException ex)
                {
                    resources.Owner.HandleConfigurationError(ex);
                }

                resources.TranslatorCache = new Dictionary<string, IClientValidationRuleTranslator>();
            }
        }
    }
}
