/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Configuration;
using Composite.Core.Collections.Generic;
using Composite.Core.Configuration;
using Composite.Data.Validation.Foundation.PluginFacades;
using Composite.Data.Validation.Plugins.ClientValidationRuleTranslator;
using Composite.Data.Validation.Plugins.ClientValidationRuleTranslator.Runtime;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;


namespace Composite.Data.Validation.Foundation
{
    internal class ClientValidationRuleTranslatorRegistryImpl : IClientValidationRuleTranslatorRegistry
    {
        private ResourceLocker<Resources> _resourceLocker = new ResourceLocker<Resources>(new Resources(), Resources.Initialize);



        public string GetTranslatorName(Type attributeType)
        {
            if (attributeType == null) throw new ArgumentNullException("attributeType");

            using (_resourceLocker.Locker)
            {
                string name = null;

                _resourceLocker.Resources.AttributeTypeToTranslatorName.TryGetValue(attributeType, out name);

                return name;
            }
        }



        public void OnFlush()
        {
            _resourceLocker.ResetInitialization();
        }



        private sealed class Resources
        {
            public Dictionary<Type, string> AttributeTypeToTranslatorName { get; set; }

            public static void Initialize(Resources resources)
            {
                resources.AttributeTypeToTranslatorName = new Dictionary<Type, string>();

                if (ConfigurationServices.ConfigurationSource == null) throw new ConfigurationErrorsException(string.Format("No configuration source specified"));

                ClientValidationRuleTranslatorSettings settings = ConfigurationServices.ConfigurationSource.GetSection(ClientValidationRuleTranslatorSettings.SectionName) as ClientValidationRuleTranslatorSettings;
                if (settings == null) throw new ConfigurationErrorsException(string.Format("Failed to load the configuration section '{0}' from the configuration.", ClientValidationRuleTranslatorSettings.SectionName));

                foreach (ClientValidationRuleTranslatorData data in settings.ClientValidationRuleTranslatorPlugins)
                {
                    IEnumerable<Type> types = ClientValidationRuleTranslatorPluginFacade.GetSupportedAttributeTypes(data.Name);

                    foreach (Type type in types)
                    {
                        if (resources.AttributeTypeToTranslatorName.ContainsKey(type)) throw new InvalidOperationException(string.Format("The attribute type '{0}' is already handle by a nother translator", type));
                        if (typeof(ValidatorAttribute).IsAssignableFrom(type) == false) throw new InvalidOperationException(string.Format("The type '{0}' is not an {1}", type, typeof(ValidatorAttribute)));

                        resources.AttributeTypeToTranslatorName.Add(type, data.Name);
                    }
                }
            }
        }
    }
}
