/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Reflection;


namespace Composite.Data
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
	public sealed class ForeignPropertyInfo
	{
        internal ForeignPropertyInfo(PropertyInfo sourcePropertyInfo, Type targetType, string targetKeyPropertyName, bool allowCascadeDeletes, object nullReferenceValue, Type nullReferenceValueType, bool isNullableString)
            : this(sourcePropertyInfo, targetType, targetKeyPropertyName, allowCascadeDeletes, isNullableString)
        {
            this.NullReferenceValue = nullReferenceValue;
            this.NullReferenceValueType = nullReferenceValueType;
            this.IsNullReferenceValueSet = true;
        }



        internal ForeignPropertyInfo(PropertyInfo sourcePropertyInfo, Type targetType, string targetKeyPropertyName, bool allowCascadeDeletes, bool isNullableString)
        {
            this.SourcePropertyName = sourcePropertyInfo.Name;
            this.SourcePropertyInfo = sourcePropertyInfo;
            this.TargetType = targetType;
            this.TargetKeyPropertyName = targetKeyPropertyName;
            this.AllowCascadeDeletes = allowCascadeDeletes;
            this.IsNullableString = isNullableString;
        }



        /// <exclude />
        public string SourcePropertyName
        {
            get;
            private set;
        }        


        internal PropertyInfo SourcePropertyInfo
        {
            get;
            private set;
        }


        /// <exclude />
        public Type TargetType
        {
            get;
            private set;
        }


        /// <exclude />
        public string TargetKeyPropertyName
        {
            get;
            private set;
        }


        /// <exclude />
        public bool AllowCascadeDeletes
        {
            get;
            private set;
        }


        /// <exclude />
        public object NullReferenceValue
        {
            get;
            private set;
        }


        /// <exclude />
        public Type NullReferenceValueType
        {
            get;
            private set;
        }


        /// <exclude />
        public bool IsNullReferenceValueSet
        {
            get;
            private set;
        }


        /// <exclude />
        public bool IsNullableString
        {
            get;
            private set;
        }


        /// <exclude />
        public bool IsOptionalReference
        {
            get { return IsNullableString || SourcePropertyInfo.PropertyType == typeof (Guid?); }
        }


        internal PropertyInfo TargetKeyPropertyInfo
        {
            get;
            set;
        }
	}
}
