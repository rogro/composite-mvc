/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using Composite.C1Console.Elements;
using Composite.C1Console.Users;
using System.Globalization;
using Composite.C1Console.Security;


namespace Composite.Data.ProcessControlled.ProcessControllers.GenericLocalizeProcessController
{
	internal sealed class GenericLocalizeProcessController : ILocalizeProcessController
	{
        public bool OnAfterBuildNew(IData data)
        {
            ILocalizedControlled localizedData = data as ILocalizedControlled;
            if (localizedData != null)
            {
                if (!LocalizationScopeManager.IsEmpty)
                {
                    localizedData.SourceCultureName = LocalizationScopeManager.CurrentLocalizationScope.Name;
                }
                else
                {
                    CultureInfo cultureInfo = null;

                    if (UserValidationFacade.IsLoggedIn())
                    {
                        cultureInfo = UserSettings.ActiveLocaleCultureInfo;
                    }

                    if (cultureInfo == null)
                    {
                        cultureInfo = DataLocalizationFacade.DefaultLocalizationCulture;
                    }

                    if (cultureInfo != null)
                    {
                        localizedData.SourceCultureName = cultureInfo.Name;
                    }
                }
            }

            return false;
        }



        public List<ElementAction> GetActions(IData data, Type elementProviderType)
        {
            // TODO: Add Localize action here?????
            return new List<ElementAction>();
        }
    }
}
