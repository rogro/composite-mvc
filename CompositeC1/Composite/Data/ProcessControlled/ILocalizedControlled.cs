/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using Composite.Data.ProcessControlled.ProcessControllers.GenericLocalizeProcessController;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;


namespace Composite.Data.ProcessControlled
{
    /// <summary>
    /// Implement this interface to allow your data type to exist in multiple language scopes (i.e. enable localizing of data).
    /// </summary>
    [LocalizeProcessControllerType(typeof(GenericLocalizeProcessController))]
    public interface ILocalizedControlled : IProcessControlled
    {
        /// <summary>
        /// The locale data originaled from. This field is automatically maintaned, do not set it unless implementing localization logic.
        /// </summary>
        [StoreFieldType(PhysicalStoreFieldType.String, 16)]
        [ImmutableFieldId("{0456EBB0-7FB1-46cd-9A23-4AE9AA3337FA}")]
        [NotNullValidator]
        [DefaultFieldStringValue("")] // Invariant
        string SourceCultureName { get; set; }
    }
}
