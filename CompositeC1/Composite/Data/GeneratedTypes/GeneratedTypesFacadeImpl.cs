/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Transactions;
using Composite.Core;
using Composite.Data.DynamicTypes;
using Composite.Data.Foundation.PluginFacades;
using Composite.Data.Plugins.DataProvider.Runtime;
using Composite.Core.Extensions;
using Composite.Core.Instrumentation;
using Composite.Core.Linq;
using Composite.Data.Transactions;
using Composite.Core.Types;


namespace Composite.Data.GeneratedTypes
{
    internal sealed class GeneratedTypesFacadeImpl : IGeneratedTypesFacade
    {
        public void GenerateNewType(string providerName, DataTypeDescriptor dataTypeDescriptor, bool makeAFlush)
        {
            Verify.ArgumentNotNullOrEmpty(providerName, "providerName");
            Verify.ArgumentNotNull(dataTypeDescriptor, "dataTypeDescriptor");
            
            UpdateWithNewMetaDataForeignKeySystem(dataTypeDescriptor);
            UpdateWithNewPageFolderForeignKeySystem(dataTypeDescriptor, false);

            Type type = InterfaceCodeManager.GetType(dataTypeDescriptor);

            dataTypeDescriptor.TypeManagerTypeName = TypeManager.SerializeType(type);

            DynamicTypeManager.CreateStore(providerName, dataTypeDescriptor, makeAFlush);

            if (makeAFlush && dataTypeDescriptor.IsCodeGenerated)
            {
                CodeGenerationManager.GenerateCompositeGeneratedAssembly(true);
            }
        }



        public void DeleteType(string providerName, DataTypeDescriptor dataTypeDescriptor, bool makeAFlush)
        {
            Verify.ArgumentNotNullOrEmpty(providerName, "providerName");
            Verify.ArgumentNotNull(dataTypeDescriptor, "dataTypeDescriptor");

            using (TransactionScope transactionScope = TransactionsFacade.CreateNewScope())
            {                
                DynamicTypeManager.DropStore(providerName, dataTypeDescriptor, makeAFlush);

                transactionScope.Complete();
            }

            if (makeAFlush && dataTypeDescriptor.IsCodeGenerated)
            {
                CodeGenerationManager.GenerateCompositeGeneratedAssembly(true);
            }
        }



        public bool CanDeleteType(DataTypeDescriptor dataTypeDescriptor, out string errorMessage)
        {
            CompatibilityCheckResult compatibilityCheckResult = CodeCompatibilityChecker.CheckIfAppCodeDependsOnInterface(dataTypeDescriptor);

            if (!compatibilityCheckResult.Successful)
            {
                errorMessage = "Cannot delete the type since it will cause a build error for App_Code files. " + compatibilityCheckResult.ErrorMessage;

                return false;
            }
            errorMessage = string.Empty;
            return true;
        }



        
        public void UpdateType(UpdateDataTypeDescriptor updateDataTypeDescriptor)
        {
            Verify.ArgumentNotNullOrEmpty(updateDataTypeDescriptor.ProviderName, "providerName");
            Verify.ArgumentNotNull(updateDataTypeDescriptor.OldDataTypeDescriptor, "oldDataTypeDescriptor");
            Verify.ArgumentNotNull(updateDataTypeDescriptor.NewDataTypeDescriptor, "newDataTypeDescriptor");

            Type interfaceType = null;
            if (updateDataTypeDescriptor.OldDataTypeDescriptor.IsCodeGenerated)
            {
                interfaceType = InterfaceCodeManager.GetType(updateDataTypeDescriptor.NewDataTypeDescriptor, true);
            }
            else
            {
                interfaceType = DataTypeTypesManager.GetDataType(updateDataTypeDescriptor.NewDataTypeDescriptor);
            }

            updateDataTypeDescriptor.NewDataTypeDescriptor.TypeManagerTypeName = TypeManager.SerializeType(interfaceType);

            DynamicTypeManager.AlterStore(updateDataTypeDescriptor, false);

            CodeGenerationManager.GenerateCompositeGeneratedAssembly(true);            
        }



        /// <summary>
        /// This method will remove a foreign key (if any exists) that is no longer possible with the
        /// new meta data system (IPageMetaDataDefinition)
        /// </summary>
        /// <param name="dataTypeDescriptor"></param>
        /// <param name="dataStoreExists"></param>
        private void UpdateWithNewPageFolderForeignKeySystem(DataTypeDescriptor dataTypeDescriptor, bool dataStoreExists)
        {            
            if (dataTypeDescriptor.IsPageFolderDataType == false)
            {
                return;
            }

            DataFieldDescriptor dataFieldDescriptor = dataTypeDescriptor.Fields["IAggregationDescriptionIdForeignKey"];
            if (dataFieldDescriptor == null)
            {
                return;
            }

            Log.LogVerbose("GeneratedTypesFacade", string.Format("Removing the property {0} on the type {1}.{2}", dataFieldDescriptor.Name, dataTypeDescriptor.Namespace, dataTypeDescriptor.Name));

            if(!dataStoreExists)
            {
                dataTypeDescriptor.Fields.Remove(dataFieldDescriptor);
                DynamicTypeManager.UpdateDataTypeDescriptor(dataTypeDescriptor, false);
                return;
            }

            DataTypeDescriptor oldDataTypeDescriptor = dataTypeDescriptor.Clone();
            dataTypeDescriptor.Fields.Remove(dataFieldDescriptor);

            var dataTypeChangeDescriptor = new DataTypeChangeDescriptor(oldDataTypeDescriptor, dataTypeDescriptor);

            UpdateDataTypeDescriptor updateDataTypeDescriptor = new UpdateDataTypeDescriptor(oldDataTypeDescriptor, dataTypeDescriptor);

            DynamicTypeManager.AlterStore(updateDataTypeDescriptor, false);
        }



        /// <summary>
        /// This method will remove a foreign key (if any exists) that is no longer possible with the
        /// new meta data system (IPageMetaDataDefinition)
        /// </summary>
        /// <param name="dataTypeDescriptor"></param>
        private void UpdateWithNewMetaDataForeignKeySystem(DataTypeDescriptor dataTypeDescriptor)
        {
            if (dataTypeDescriptor.IsPageMetaDataType)
            {
                DataFieldDescriptor dataFieldDescriptor = dataTypeDescriptor.Fields[PageMetaDataFacade.MetaDataType_MetaDataDefinitionFieldName];
                if ((dataFieldDescriptor != null) && (dataFieldDescriptor.ForeignKeyReferenceTypeName != null)) // This should never fail, but want to be sure
                {
                    dataFieldDescriptor.ForeignKeyReferenceTypeName = null;
                    
                    DynamicTypeManager.UpdateDataTypeDescriptor(dataTypeDescriptor, false);

                    Log.LogVerbose("GeneratedTypesFacade", string.Format("Removing foreign on the property {0} on the type {1}.{2}", dataFieldDescriptor.Name, dataTypeDescriptor.Namespace, dataTypeDescriptor.Name));
                }
            }
        }
    }
}
