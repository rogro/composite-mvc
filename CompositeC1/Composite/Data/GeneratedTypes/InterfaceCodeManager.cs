/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Composite.Core.Types;
using Composite.Data.DynamicTypes;


namespace Composite.Data.GeneratedTypes
{
    /// <summary>
    /// This class handles the caching of code generated data interface.
    /// It also through <see cref="InterfaceCodeGenerator"/> generated data interfaces,
    /// that does not exist.    
    /// </summary>
    internal static class InterfaceCodeManager
    {
        private static readonly object _lock = new object();

        /// <summary>
        /// This method will return type given by the dataTypeDescriptor.
        /// If the data type does not exist, one will be dynamically
        /// runtime code generated.
        /// </summary>
        /// <param name="dataTypeDescriptor"></param>
        /// <param name="forceReCompilation">If this is true a new type will be compiled regardless if one already exists.</param>
        /// <returns></returns>
        public static Type GetType(DataTypeDescriptor dataTypeDescriptor, bool forceReCompilation = false)
        {
            Verify.ArgumentNotNull(dataTypeDescriptor, "dataTypeDescriptor");

            if (!forceReCompilation)
            {
                Type type = TypeManager.TryGetType(dataTypeDescriptor.GetFullInterfaceName());
                if (type != null) return type;

                if (!dataTypeDescriptor.IsCodeGenerated)
                {
                    type = TypeManager.TryGetType(dataTypeDescriptor.TypeManagerTypeName);
                    if (type != null) return type;
                }
            }

            if (!dataTypeDescriptor.IsCodeGenerated) return null;

            lock (_lock)
            {
                Type type = TypeManager.TryGetType(dataTypeDescriptor.GetFullInterfaceName());
                if (type != null) return type;

                var codeGenerationBuilder = new CodeGenerationBuilder("DataInterface: " + dataTypeDescriptor.Name);
                InterfaceCodeGenerator.AddAssemblyReferences(codeGenerationBuilder, dataTypeDescriptor);
                InterfaceCodeGenerator.AddInterfaceTypeCode(codeGenerationBuilder, dataTypeDescriptor);

                IEnumerable<Type> types = CodeGenerationManager.CompileRuntimeTempTypes(codeGenerationBuilder);

                return types.Single();
            }
        }
    }
}
