/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.ComponentModel;
using Composite.Data.DynamicTypes;
using Composite.Data.Foundation;


namespace Composite.Data.GeneratedTypes
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [EditorBrowsable(EditorBrowsableState.Never)] 
    public class GenerateNewTypeEventArgs : EventArgs
    {
    }



    /// <summary>    
    /// </summary>
    /// <exclude />
    [EditorBrowsable(EditorBrowsableState.Never)] 
    public class UpdateTypeEventArgs : EventArgs
    {
    }



    /// <summary>    
    /// </summary>
    /// <exclude />
    [EditorBrowsable(EditorBrowsableState.Never)] 
    public class DeleteTypeEventArgs : EventArgs
    {
    }





    /// <summary>    
    /// This class is used to create/update/delete generated types. That is, types
    /// that is not defined in code, but designed through the UI and is dynamicly
    /// compiled. 
    /// When a data type is created/updated/deleted the interface is (re)code-generated/deleted 
    /// and the store is created/updated/deleted through <see cref="Composite.Data.DynamicTypes.DynamicTypeManager"/>
    /// </summary>
    /// <exclude />
    [EditorBrowsable(EditorBrowsableState.Never)] 
    public static class GeneratedTypesFacade
    {
        /// <exclude />
        public delegate void GenerateNewTypeDelegate(GenerateNewTypeEventArgs args);

        /// <exclude />
        public delegate void UpdateTypeDelegate(UpdateTypeEventArgs args);

        /// <exclude />
        public delegate void DeleteTypeDelegate(DeleteTypeEventArgs args);


        private static event GenerateNewTypeDelegate _generateNewTypeDelegate;
        private static event UpdateTypeDelegate _updateTypeDelegate;
        private static event DeleteTypeDelegate _deleteTypeDelegate;


        private static IGeneratedTypesFacade _generatedTypesFacade = new GeneratedTypesFacadeImpl();


        internal static IGeneratedTypesFacade Implementation { get { return _generatedTypesFacade; } set { _generatedTypesFacade = value; } }
        


        // Overload
        /// <exclude />
        public static void GenerateNewType(DataTypeDescriptor dataTypeDescriptor)
        {
            GenerateNewType(DataProviderRegistry.DefaultDynamicTypeDataProviderName, dataTypeDescriptor, true);
        }



        // Overload
        /// <exclude />
        public static void GenerateNewType(DataTypeDescriptor dataTypeDescriptor, bool makeAFlush)
        {
            GenerateNewType(DataProviderRegistry.DefaultDynamicTypeDataProviderName, dataTypeDescriptor, makeAFlush);
        }



        /// <exclude />
        public static void GenerateNewType(string providerName, DataTypeDescriptor dataTypeDescriptor, bool makeAFlush)
        {
            using (GlobalInitializerFacade.CoreLockScope)
            {
                _generatedTypesFacade.GenerateNewType(providerName, dataTypeDescriptor, makeAFlush);

                if (_generateNewTypeDelegate != null)
                {
                    GenerateNewTypeDelegate generateNewTypeDelegate = _generateNewTypeDelegate;
                    generateNewTypeDelegate(new GenerateNewTypeEventArgs());
                }
            }
        }



        /// <exclude />
        public static bool CanDeleteType(DataTypeDescriptor dataTypeDescriptor, out string errorMessage)
        {
            return _generatedTypesFacade.CanDeleteType(dataTypeDescriptor, out errorMessage);
        }


        /// <exclude />
        public static void DeleteType(DataTypeDescriptor dataTypeDescriptor)
        {
            DeleteType(DataProviderRegistry.DefaultDynamicTypeDataProviderName, dataTypeDescriptor, true);
        }



        internal static void DeleteType(DataTypeDescriptor dataTypeDescriptor, bool makeAFlush)
        {
            DeleteType(DataProviderRegistry.DefaultDynamicTypeDataProviderName, dataTypeDescriptor, makeAFlush);
        }



        /// <exclude />
        public static void DeleteType(string providerName, DataTypeDescriptor dataTypeDescriptor)
        {
            DeleteType(providerName, dataTypeDescriptor, true);
        }



        internal static void DeleteType(string providerName, DataTypeDescriptor dataTypeDescriptor, bool makeAFlush)
        {
            using (GlobalInitializerFacade.CoreLockScope)
            {
                PageFolderFacade.RemoveAllFolderDefinitions(dataTypeDescriptor.DataTypeId, false);
                PageMetaDataFacade.RemoveAllDefinitions(dataTypeDescriptor.DataTypeId, false);

                _generatedTypesFacade.DeleteType(providerName, dataTypeDescriptor, makeAFlush);

                if (_deleteTypeDelegate != null)
                {
                    DeleteTypeDelegate deleteDelegate = _deleteTypeDelegate;
                    deleteDelegate(new DeleteTypeEventArgs());
                }
            }
        }



        ///// <exclude />
        //public static void UpdateType(DataTypeDescriptor oldDataTypeDescriptor, DataTypeDescriptor newDataTypeDescriptor, bool originalTypeHasData)
        //{
        //    UpdateType(DataProviderRegistry.DefaultDynamicTypeDataProviderName, oldDataTypeDescriptor, newDataTypeDescriptor, originalTypeHasData);
        //}



        ///// <exclude />
        //public static void UpdateType(DataTypeDescriptor oldDataTypeDescriptor, DataTypeDescriptor newDataTypeDescriptor)
        //{
        //    UpdateType(DataProviderRegistry.DefaultDynamicTypeDataProviderName, oldDataTypeDescriptor, newDataTypeDescriptor, true);
        //}



        /// <exclude />
        public static void UpdateType(UpdateDataTypeDescriptor updateDataTypeDescriptor)
        {
            using (GlobalInitializerFacade.CoreLockScope)
            {                
                _generatedTypesFacade.UpdateType(updateDataTypeDescriptor);

                if (_updateTypeDelegate != null)
                {
                    UpdateTypeDelegate updateDelegate = _updateTypeDelegate;
                    updateDelegate(new UpdateTypeEventArgs());
                }
            }
        }



        /// <exclude />
        public static void SubscribeToGenerateNewTypeEvent(GenerateNewTypeDelegate eventDelegate)
        {
            _generateNewTypeDelegate += eventDelegate;
        }



        /// <exclude />
        public static void SubscribeToUpdateTypeEvent(UpdateTypeDelegate eventDelegate)
        {
            _updateTypeDelegate += eventDelegate;
        }



        /// <exclude />
        public static void SubscribeToDeleteTypeEvent(DeleteTypeDelegate eventDelegate)
        {
            _deleteTypeDelegate += eventDelegate;
        }



        /// <exclude />
        public static void UnsubscribeToGenerateNewTypeEvent(GenerateNewTypeDelegate eventDelegate)
        {
            _generateNewTypeDelegate -= eventDelegate;
        }



        /// <exclude />
        public static void UnsubscribeToUpdateTypeEvent(UpdateTypeDelegate eventDelegate)
        {
            _updateTypeDelegate -= eventDelegate;
        }



        /// <exclude />
        public static void UnsubscribeToDeleteTypeEvent(DeleteTypeDelegate eventDelegate)
        {
            _deleteTypeDelegate -= eventDelegate;
        }        
    }
}
