/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Reflection;
using Composite.C1Console.Events;


namespace Composite.Data.Caching.Foundation
{
    internal static class CachingQueryableCache
    {
        private static readonly ConcurrentDictionary<Type, MethodInfo> _getEnumeratorMethodInfoCache = new ConcurrentDictionary<Type, MethodInfo>();
        private static readonly ConcurrentDictionary<Type, Type> _queryableTypeCache = new ConcurrentDictionary<Type, Type>();
        private static readonly ConcurrentDictionary<Tuple<Type, Type>, MethodInfo> _executeMethodInfoCache = new ConcurrentDictionary<Tuple<Type, Type>, MethodInfo>();


        static CachingQueryableCache()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(args => Flush());
        }



        public static MethodInfo GetCachingQueryableGetEnumeratorMethodInfo(Type genericType)
        {
            return _getEnumeratorMethodInfoCache.GetOrAdd(genericType, genType =>
            {
                Type type = typeof (CachingQueryable<>).MakeGenericType(new[] {genType});

                return type.GetMethods().First(method => method.Name == "GetEnumerator");
            });
        }



        public static Type GetCachingQueryableType(Type elementType)
        {
            return _queryableTypeCache.GetOrAdd(elementType, eType => typeof(CachingQueryable<>).MakeGenericType(new[] { eType }));
        }
        

        public static MethodInfo GetCachingQueryableExecuteMethodInfo(Type genericType, Type expressionType)
        {
            return _executeMethodInfoCache.GetOrAdd(new Tuple<Type, Type>(genericType, expressionType), pair =>
            {
                Type type = typeof(CachingQueryable<>).MakeGenericType(new[] { pair.Item1 });

                var genericMethodInfo = type.GetMethods().First(method => method.Name == "Execute" && method.IsGenericMethod);

                return genericMethodInfo.MakeGenericMethod(new[] { pair.Item2 });
            });
        }


        private static void Flush()
        {
            _getEnumeratorMethodInfoCache.Clear();
            _queryableTypeCache.Clear();
            _executeMethodInfoCache.Clear();
        }
    }
}
