/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Globalization;
using System.Threading;
using Composite.Core.Collections.Generic;
using Composite.Data.Types;
using Composite.C1Console.Events;
using Composite.Core.Types;

namespace Composite.Data.Caching
{
    internal static class TableVersion
    {
        private static readonly Hashtable<string, ExtendedNullable<int>> _versionNumbers = new Hashtable<string, ExtendedNullable<int>>();
        private static Hashset<Type> _subscribedTo = new Hashset<Type>();
        private static int _flushCounter;

        static TableVersion()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlushEvent);
        }

        public static int Get(Type type)
        {
            Verify.ArgumentNotNull(type, "type");

            if(typeof(IMediaFile).IsAssignableFrom(type))
            {
                type = typeof (IMediaFileData);
            }

            EnsureSubscribtion(type);

            string key = GetKey(type);
            ExtendedNullable<int> record = _versionNumbers[key];

            return _flushCounter + (record == null ? 0 : record.Value);
        }

        private static string GetKey(Type type)
        {
            return GetKey(type, DataScopeManager.MapByType(type), LocalizationScopeManager.MapByType(type));
        }

        private static string GetKey(Type type, DataScopeIdentifier dataScopeIdentifier, CultureInfo cultureInfo)
        {
            return type.FullName + " " + dataScopeIdentifier.Name + " " + cultureInfo.Name;
        }

        private static void IncreaseTableVersion(Type type, DataScopeIdentifier dataScopeIdentifier, CultureInfo locale)
        {
            string key =  GetKey(type, dataScopeIdentifier, locale);

            lock (_versionNumbers)
            {
                if (_versionNumbers.ContainsKey(key))
                {
                    _versionNumbers[key].Value++;
                }
                else
                {
                    _versionNumbers.Add(key, 1);
                }
            }
        }

        private static void EnsureSubscribtion(Type type)
        {
            if (!_subscribedTo.Contains(type))
            {
                lock (_subscribedTo)
                {
                    if (!_subscribedTo.Contains(type))
                    {
                        _subscribedTo.Add(type);

                        DataEventSystemFacade.SubscribeToStoreChanged(type, (sender, storeEventArgs) => IncreaseTableVersion(type, DataScopeIdentifier.FromPublicationScope(storeEventArgs.PublicationScope), storeEventArgs.Locale), false);
                    }
                }
            }
        }

        private static void OnFlushEvent(FlushEventArgs args)
        {
            Interlocked.Increment(ref _flushCounter);
            _subscribedTo = new Hashset<Type>();
        }
    }
}
