/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections;
using System.Collections.Generic;

namespace Composite.Data.Caching
{
    internal class WeakRefCache<K, V> : Cache where V : class
    {
        private int _counter;

        public WeakRefCache(string name)
            : base(name)
        {
        }

        public WeakRefCache(string name, int maximumSize)
            : base(name, maximumSize)
        {
        }

        public V Get(K key)
        {
            var weakReference = base.Get(key) as WeakReference;

            return (weakReference != null ? weakReference.Target : null) as V;
        }

        public void Remove(K key)
        {
            base.Remove(key);
        }

        public void Add(K key, V value)
        {
            base.Add(key, new WeakReference(value));

            // Cleaning-up "dead" references
            _counter++;
            if (_counter % 500 == 0)
            {
                foreach (K k in GetKeys())
                {
                    WeakReference weakRef = Get(k) as WeakReference;

                    if (weakRef != null && !weakRef.IsAlive)
                    {
                        Remove(k);
                    }
                }
            }
        }

        public IEnumerable<K> GetKeys()
        {
            lock(_syncRoot)
            {
                ICollection keys = _table.Keys;
                K[] result = new K[keys.Count];
                keys.CopyTo(result, 0);

                return result;
            }
        }
    }
}
