/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections;
using System.Collections.Generic;
using Composite.C1Console.Events;
using Composite.Core;
using Composite.Core.Configuration;


namespace Composite.Data.Caching
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public class Cache<K, V>: Cache where V: class
    {
        /// <exclude />
        public Cache(string name)
            : base(name)
        {
        }

        /// <exclude />
        public Cache(string name, int maximumSize)
            : base(name, maximumSize)
        {
        }

        /// <exclude />
        public V Get(K key)
        {
            return base.Get(key) as V;
        }

        /// <exclude />
        public void Remove(K key)
        {
            base.Remove(key);
        }

        /// <exclude />
        public void Add(K key, V value)
        {
            base.Add(key, value);
        }

        /// <exclude />
        public IEnumerable<K> GetKeys()
        {
            lock(_syncRoot)
            {
                ICollection keys = _table.Keys;
                K[] result = new K[keys.Count];
                keys.CopyTo(result, 0);

                return result;
            }
        }
    }


    /// <summary>
    /// Represents a cache.
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public class Cache
    {
        private static readonly int DefaultMaximumCacheSize = 1000;

        /// <exclude />
        protected readonly Hashtable _table = new Hashtable();

        /// <exclude />
        protected readonly object _syncRoot = new object();

        private bool _enabled = true;
        private bool _clearOnFlush = true;
        private int _maxSize;
        private int _defaultMaximumSize;


        /// <exclude />
        public Cache(string name): this(name, DefaultMaximumCacheSize)
        {
        }

        /// <exclude />
        public Cache(string name, int defaultMaximumSize)
        {
            Verify.ArgumentCondition(defaultMaximumSize >= 10, "maximumSize", "Maximum cache size should be at least 10 element.");
            Verify.ArgumentNotNullOrEmpty(name, "name");

            Name = name;
            _defaultMaximumSize = defaultMaximumSize;
           
            ReadConfiguration();

            GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlush);
            GlobalEventSystemFacade.SubscribeToFlushEvent(OnPostFlush);
        }


        /// <exclude />
        public bool Enabled
        {
            get { return _enabled; }
        }


        /// <exclude />
        public string Name { get; private set; }


        /// <exclude />
        protected object Get(object key)
        {
            return _table[key];
        }


        /// <exclude />
        protected void Add(object key, object value)
        {
            if(!_enabled)
            {
                return;
            }

            lock (_syncRoot)
            {
                if (_maxSize != -1 && _table.Count > _maxSize)
                {
                    Log.LogWarning("Cache", "Clearing cache '{0}' as it exceeded maximum size {1} elements. Edit configuration file to increase the cache size.", Name, _maxSize);
                    _table.Clear();
                }

                if (_table.Contains(key))
                {
                    _table.Remove(key);
                }

                _table.Add(key, value);
            }
        }


        /// <exclude />
        protected void Remove(object key)
        {
            if (!_enabled)
            {
                return;
            }

            if(!_table.Contains(key))
                return;

            lock(_syncRoot)
            {
                if (!_table.Contains(key))
                    return;

                _table.Remove(key);
            }
        }


        /// <exclude />
        public void Clear()
        {
            lock (_syncRoot)
            {
                _table.Clear();
            }
        }

        void OnFlush(FlushEventArgs args)
        {
            if (_clearOnFlush)
            {
                Clear();
            }
        }

        void OnPostFlush(FlushEventArgs args)
        {
            ReadConfiguration();
        }


        /// <exclude />
        public bool ClearOnFlush
        {
            set { _clearOnFlush = value; }
        }


        /// <exclude />
        protected void ReadConfiguration()
        {
            CachingSettings cachingSettings = GlobalSettingsFacade.GetNamedCaching(this.Name);
            _enabled = cachingSettings.Enabled;
            _maxSize = cachingSettings.GetSize(_defaultMaximumSize);
        }
	}
}
