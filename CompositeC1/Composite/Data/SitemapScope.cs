/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

namespace Composite.Data
{

    /// <summary>
    /// Define a set of elements in a tree structure, relative to a particular node.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "Sitemap")]
    public enum SitemapScope
    {
        /// <summary>
        /// This page.
        /// </summary>
        Current = 0,

        /// <summary>
        /// Children of this page.
        /// </summary>
        Children = 2,

        /// <summary>
        /// All descendants of this page.
        /// </summary>
        Descendants = 20,

        /// <summary>
        /// All descendants of this page and this page.
        /// </summary>
        DescendantsAndCurrent = 1,

        /// <summary>
        /// Pages sharing the same parent as this page, excluding this page.
        /// </summary>
        Siblings = 15,

        /// <summary>
        /// Pages sharing the same parent as this page, including this page.
        /// </summary>
        SiblingsAndSelf = 21,

        /// <summary>
        /// All ancenstor pages
        /// </summary>
        Ancestors = 3,

        /// <exclude />
        AncestorsAndCurrent = 4,

        /// <exclude />
        Parent = 5,

        /// <exclude />
        Level1 = 6,

        /// <exclude />
        Level1AndSiblings = 16,

        /// <exclude />
        Level1AndDescendants = 10,

        /// <exclude />
        Level2 = 7,

        /// <exclude />
        Level2AndSiblings = 17,

        /// <exclude />
        Level2AndDescendants = 11,

        /// <exclude />
        Level3 = 8,

        /// <exclude />
        Level3AndSiblings = 18,

        /// <exclude />
        Level3AndDescendants = 12,

        /// <exclude />
        Level4 = 9,

        /// <exclude />
        Level4AndSiblings = 19,

        /// <exclude />
        Level4AndDescendants = 13,

        /// <exclude />
        All = 14
    }
}
