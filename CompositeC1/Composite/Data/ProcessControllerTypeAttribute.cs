/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.Data.ProcessControlled;


namespace Composite.Data
{
    /// <summary> 
    /// Binds imlementations of <see cref="IProcessController"/> to data types
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public abstract class ProcessControllerTypeAttribute : Attribute
    {
        /// <exclude />
        protected ProcessControllerTypeAttribute(Type processControllerType)
        {
            this.ProcessControllerType = processControllerType;
        }


        /// <exclude />
        public Type ProcessControllerType
        {
            get;
            private set;
        }
    }



    /// <summary>
    /// Add this attribute to your data interface to specify what controller to use for publishing.
    /// Your data type is expected to implement <see cref="Composite.Data.ProcessControlled.IPublishControlled"/> when this attribute is used.
    /// The type you specify is expected to implement <see cref="Composite.Data.ProcessControlled.IPublishProcessController"/>.
    /// For default publishing behaviour use the type <see cref="Composite.Data.ProcessControlled.ProcessControllers.GenericPublishProcessController.GenericPublishProcessController"/>
    /// </summary>
    /// <example> This sample shows how to use the PublishProcessControllerType attribute.
    /// <code>
    /// [PublishProcessControllerType(typeof(GenericPublishProcessController))]
    /// // (other IData attributes)
    /// interface IMyDataType : IData, IPublishControlled
    /// {
    ///     // data type properties
    /// }
    /// </code>
    /// </example>    
    [AttributeUsage(AttributeTargets.Interface, AllowMultiple = true, Inherited = true)]
    public sealed class PublishProcessControllerTypeAttribute : ProcessControllerTypeAttribute
    {
        /// <summary>
        /// Specify what controller to use for publishing.
        /// For default publishing behaviour use the type <see cref="Composite.Data.ProcessControlled.ProcessControllers.GenericPublishProcessController.GenericPublishProcessController"/>
        /// </summary>
        /// <param name="processControllerType">Controller to use in publishing flow</param>
        public PublishProcessControllerTypeAttribute(Type processControllerType)
            : base(processControllerType)
        {
        }
    }



    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [AttributeUsage(AttributeTargets.Interface, AllowMultiple = true, Inherited = true)]
    public sealed class LocalizeProcessControllerTypeAttribute : ProcessControllerTypeAttribute
    {
        /// <exclude />
        public LocalizeProcessControllerTypeAttribute(Type processControllerType)
            : base(processControllerType)
        {
        }
    }
}
