/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Globalization;


namespace Composite.Data
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class DataScope : IDisposable
    {
        private readonly bool _dataScopePushed = false;
        private readonly bool _cultureInfoPushed = false;


        /// <exclude />
        public DataScope(DataScopeIdentifier dataScope)
            : this(dataScope, null)
        {
        }

        /// <exclude />
        public DataScope(PublicationScope publicationScope)
            : this(DataScopeIdentifier.FromPublicationScope(publicationScope), null)
        {
        }


        /// <exclude />
        public DataScope(CultureInfo cultureInfo)
        {
            if (cultureInfo != null)
            {
                LocalizationScopeManager.PushLocalizationScope(cultureInfo);
                _cultureInfoPushed = true;
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataScope"></param>
        /// <param name="cultureInfo">null for default culture</param>
        public DataScope(DataScopeIdentifier dataScope, CultureInfo cultureInfo)
        {
            DataScopeManager.PushDataScope(dataScope);
            _dataScopePushed = true;


            if (cultureInfo != null)
            {
                LocalizationScopeManager.PushLocalizationScope(cultureInfo);
                _cultureInfoPushed = true;
            }
            else if (LocalizationScopeManager.IsEmpty)
            {
                LocalizationScopeManager.PushLocalizationScope(DataLocalizationFacade.DefaultLocalizationCulture);
                _cultureInfoPushed = true;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="publicationScope">Publication scope</param>
        /// <param name="cultureInfo">null for default culture</param>
        public DataScope(PublicationScope publicationScope, CultureInfo cultureInfo)
            : this(DataScopeIdentifier.FromPublicationScope(publicationScope), cultureInfo)
        {
        }

        /// <exclude />
        ~DataScope()
        {
            Dispose();
        }



        /// <exclude />
        public void Dispose()
        {
            if (_dataScopePushed)
            {
                DataScopeManager.PopDataScope();
            }

            if (_cultureInfoPushed)
            {
                LocalizationScopeManager.PopLocalizationScope();
            }
        }
    }
}
