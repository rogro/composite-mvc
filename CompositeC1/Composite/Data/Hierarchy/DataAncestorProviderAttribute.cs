/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;

namespace Composite.Data.Hierarchy
{
    /// <summary> 
    /// This attribute is requried if data items of this type is used as elements in a tree view.
    /// This attribute points to a type that can resolve parent relations. 
    /// See <see cref="Composite.Data.Hierarchy.IDataAncestorProvider"/> for more information on parent relations.
    /// In case that the data items do not have natural parents, use the default implementaion <see cref="Composite.Data.Hierarchy.DataAncestorProviders.NoAncestorDataAncestorProvider"/>
    /// </summary>
    [AttributeUsageAttribute(AttributeTargets.Interface, Inherited = true, AllowMultiple = false)]
    public sealed class DataAncestorProviderAttribute : Attribute
    {
        private Type _dataAncestorProviderType;


        /// <summary>
        /// Create a instance of <see cref="DataAncestorProviderAttribute"/> given the <paramref name="dataAncestorProviderType"/> type.
        /// </summary>
        /// <param name="dataAncestorProviderType">The type that can resolve parent relations. See <see cref="Composite.Data.Hierarchy.IDataAncestorProvider"/>.</param>
        public DataAncestorProviderAttribute(Type dataAncestorProviderType)
        {
            _dataAncestorProviderType = dataAncestorProviderType;
        }



        /// <summary>
        /// The type that can resolve parent relations. See <see cref="Composite.Data.Hierarchy.IDataAncestorProvider"/>.
        /// </summary>
        public Type DataAncestorProviderType
        {
            get { return _dataAncestorProviderType; }
        }
    }
}
