/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using System.Collections.Generic;
using Composite.Core.Collections.Generic;
using Composite.Core.Types;
using Composite.C1Console.Events;


namespace Composite.Data.Hierarchy.Foundation
{
    internal static class DataAncestorProviderCache
    {
        private static Hashtable<Type, IDataAncestorProvider> _dataAncestorProviderCache = new Hashtable<Type, IDataAncestorProvider>();

        private static readonly object _syncRoot = new object();



        static DataAncestorProviderCache()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(args => Flush());
        }


        public static IDataAncestorProvider GetDataAncestorProvider(IData data)
        {
            if (data == null) throw new ArgumentNullException("data");

            Type dataType = data.GetType();

            var cache = _dataAncestorProviderCache;

            IDataAncestorProvider dataAncestorProvider = cache[dataType];

            if (dataAncestorProvider != null)
            {
                return dataAncestorProvider;
            }

            lock (_syncRoot)
            {
                dataAncestorProvider = cache[dataType];

                if (dataAncestorProvider != null)
                {
                    return dataAncestorProvider;
                }

                List<DataAncestorProviderAttribute> attributes = dataType.GetCustomInterfaceAttributes<DataAncestorProviderAttribute>().ToList();


                if (attributes.Count == 0) throw new InvalidOperationException(string.Format("Missing {0} attribute on the data type {1}", typeof(DataAncestorProviderAttribute), dataType));
                if (attributes.Count > 1) throw new InvalidOperationException(string.Format("Only one {0} attribute is allowed on the data type {1}", typeof(DataAncestorProviderAttribute), dataType));

                DataAncestorProviderAttribute attribute = attributes[0];

                if (attribute.DataAncestorProviderType == null) throw new InvalidOperationException(string.Format("Data ancestor provider type can not be null on the data type {0}", data));
                if (typeof(IDataAncestorProvider).IsAssignableFrom(attribute.DataAncestorProviderType) == false) throw new InvalidOperationException(string.Format("Data ancestor provider {0} should implement the interface {1}", attribute.DataAncestorProviderType, typeof(IDataAncestorProvider)));

                dataAncestorProvider = (IDataAncestorProvider)Activator.CreateInstance(attribute.DataAncestorProviderType);

                cache.Add(dataType, dataAncestorProvider);

                return dataAncestorProvider;
            }
        }

        private static void Flush()
        {
            _dataAncestorProviderCache = new Hashtable<Type, IDataAncestorProvider>();
        }
    }
}
