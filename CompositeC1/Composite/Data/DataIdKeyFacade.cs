/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.C1Console.Events;


namespace Composite.Data
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class DataIdKeyFacade
    {
        private static IDataIdKeyFacade _implementation = new DataIdKeyFacadeImpl();

        /// <exclude />
        static DataIdKeyFacade()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlushEvent);
        }


              
        // Overload
        /// <exclude />
        public static T GetKeyValue<T>(this DataSourceId dataSourceId, string keyName = null)
        {           
            return (T)_implementation.GetKeyValue(dataSourceId.DataId, keyName);
        }



        // Overload
        /// <exclude />
        public static T GetKeyValue<T>(IDataId dataId, string keyName = null)
        {
            return (T)_implementation.GetKeyValue(dataId, keyName);
        }


       
        // Overload
        /// <exclude />
        public static object GetKeyValue(this DataSourceId dataSourceId, string keyName = null)
        {           
            return _implementation.GetKeyValue(dataSourceId.DataId, keyName);
        }



        /// <exclude />
        public static object GetKeyValue(IDataId dataId, string keyName = null)
        {
            return _implementation.GetKeyValue(dataId, keyName);
        }




        // Overload
        /// <exclude />
        public static string GetDefaultKeyName(IDataId dataId)
        {
            return _implementation.GetDefaultKeyName(dataId.GetType());
        }



        /// <exclude />
        public static string GetDefaultKeyName(Type dataIdType)
        {
            return _implementation.GetDefaultKeyName(dataIdType);
        }



        private static void Flush()
        {
            _implementation.OnFlush();
        }



        private static void OnFlushEvent(FlushEventArgs args)
        {
            Flush();
        }
    }
}
