/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.Data.Hierarchy;
using Composite.Data.Hierarchy.DataAncestorProviders;


namespace Composite.Data.Types
{
    /// <summary>    
    /// A named function call for an xslt function
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [AutoUpdateble]
    [ImmutableTypeId("{7eccf947-1abd-43d5-b28b-551a44a3fe96}")]
    [KeyPropertyName(0, "XsltFunctionId")]
    [KeyPropertyName(1, "Name")]
    [DataAncestorProvider(typeof(NoAncestorDataAncestorProvider))]
    [DataScope(DataScopeIdentifier.PublicName)]
    [Caching(CachingType.Full)]
    [NotReferenceable]
    public interface INamedFunctionCall : IData
    {
        /// <exclude />
        [ImmutableFieldId("{f60e9cda-a461-4234-b225-5ea3eb51a1fb}")]
        [StoreFieldType(PhysicalStoreFieldType.Guid)]
        Guid XsltFunctionId { get; set; }


        /// <exclude />
        [ImmutableFieldId("{4c042ad7-9bf5-4875-a369-3720c1b79380}")]
        [StoreFieldType(PhysicalStoreFieldType.String, 256)]
        string Name { get; set; }


        /// <exclude />
        [ImmutableFieldId("{cfc67bfa-2252-4642-91dc-623ca9e1d027}")]
        [StoreFieldType(PhysicalStoreFieldType.LargeString)]
        string SerializedFunction { get; set; }
    }
}
