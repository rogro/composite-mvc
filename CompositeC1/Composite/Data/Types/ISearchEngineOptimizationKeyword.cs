/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using Composite.Data.Hierarchy;
using Composite.Data.Hierarchy.DataAncestorProviders;
using Composite.Data.Validation.Validators;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;


namespace Composite.Data.Types
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [AutoUpdateble]
    [Caching(CachingType.Full)]
    [KeyPropertyName("Keyword")]
    [DataScope(DataScopeIdentifier.PublicName)]
    [ImmutableTypeId("{895F967D-EEAA-437a-9463-E1F95C214ED6}")]
    [DataAncestorProvider(typeof(NoAncestorDataAncestorProvider))]
    public interface ISearchEngineOptimizationKeyword : IData
    {
        /// <exclude />
        [NotNullValidator()]
        [StringSizeValidator(1, 128)]
        [StoreFieldType(PhysicalStoreFieldType.String, 128)]
        [ImmutableFieldId("{87EE9B0E-3212-472b-8FDB-2984798CDB79}")]
        string Keyword { get; set; }
    }
}
