/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.Data.Hierarchy;
using Composite.Data.Hierarchy.DataAncestorProviders;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;


namespace Composite.Data.Types
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]  
    [Title("Hostname configuration")]
    [AutoUpdateble]
    [ImmutableTypeId("{5552C35A-A72A-55F1-9630-ACC66FE44BBE}")]
    [DataScope(DataScopeIdentifier.PublicName)]
    [KeyPropertyName("Id")]
    [CachingAttribute(CachingType.Full)]
    [DataAncestorProvider(typeof(NoAncestorDataAncestorProvider))]
    public interface IUrlConfiguration : IData
    {
        /// <exclude />
        [StoreFieldType(PhysicalStoreFieldType.Guid)]
        [ImmutableFieldId("{D0C48D08-1318-7441-D2C0-425D31894C1F}")]
        Guid Id { get; set; }
        
        /// <exclude />
        [StoreFieldType(PhysicalStoreFieldType.String, 255, IsNullable = false)]
        [ImmutableFieldId("{C0504E0C-B487-3951-AA18-DBF28DD681B5}")]
        [RegexValidator(@"^((\.([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9]))*)$")]
        string PageUrlSuffix { get; set; }
    } 
}
