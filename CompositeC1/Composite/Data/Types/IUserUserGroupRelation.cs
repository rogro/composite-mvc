/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.Data.Hierarchy;
using Composite.Data.Hierarchy.DataAncestorProviders;


namespace Composite.Data.Types
{
    /// <summary>    
    /// This data interface represents a user relation to a user group in Composite C1. This can be used to query user group members through a <see cref="Composite.Data.DataConnection"/>. 
    /// </summary>
    [AutoUpdateble]
    [KeyPropertyName(0, "UserId")]
    [KeyPropertyName(1, "UserGroupId")]
    [DataScope(DataScopeIdentifier.PublicName)]
    [ImmutableTypeId("{956BC414-4612-4a8a-A673-B82695F322DD}")]
    [DataAncestorProvider(typeof(NoAncestorDataAncestorProvider))]
    public interface IUserUserGroupRelation : IData
	{
        /// <exclude />
        [StoreFieldType(PhysicalStoreFieldType.Guid)]
        [ForeignKey(typeof(IUser), "Id", AllowCascadeDeletes = true)]
        [ImmutableFieldId("{529E233A-0386-4cca-8A32-69FE156EAEE1}")]
        Guid UserId { get; set; }


        /// <exclude />
        [StoreFieldType(PhysicalStoreFieldType.Guid)]
        [ForeignKey(typeof(IUserGroup), "Id", AllowCascadeDeletes = true)]
        [ImmutableFieldId("{9BB13E81-3125-45eb-87A8-D0DE671A3102}")]
        Guid UserGroupId { get; set; }
    }
}
