/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq.Expressions;
using System.Reflection;


namespace Composite.Data.Types.StoreIdFilter.Foundation
{
    /// <summary>
    /// Searches for a "StoreId" filtering in an expression tree.
    /// </summary>
    internal sealed class StoreIdFilterQueryableExpressionVisitor : ExpressionVisitor
    {
        private static readonly MemberInfo _mediaFileStoreIdMemberInfo = typeof(IMediaFile).GetMember("StoreId")[0];
        private static readonly MemberInfo _mediaFileFolderStoreIdMemberInfo = typeof(IMediaFileFolder).GetMember("StoreId")[0];



        public StoreIdFilterQueryableExpressionVisitor()
        {
            this.FoundStoreId = null;
        }

        protected override Expression VisitConstant(ConstantExpression c)
        {
            if (c.Value is IStorageFilter)
            {
                FoundStoreId = (c.Value as IStorageFilter).StoreId;
            }

            return base.VisitConstant(c);
        }

        protected override Expression VisitBinary(BinaryExpression b)
        {
            if (b.Method != null && b.Method.Name == "op_Equality")
            {
                bool hasStoreIdMemberExpression = IsStoreIdMemberExpression(b.Left) || IsStoreIdMemberExpression(b.Right);

                if (hasStoreIdMemberExpression)
                {
                    string storeId = GetStoreId(b.Left) ?? GetStoreId(b.Right);

                    if (storeId != null)
                    {
                        this.FoundStoreId = storeId;
                    }
                }
            }

            return base.VisitBinary(b);
        }



        public string FoundStoreId
        {
            get;
            private set;
        }



        private bool IsStoreIdMemberExpression(Expression expression)
        {
            MemberExpression memberExpression = expression as MemberExpression;

            if (memberExpression == null) return false;

            if (memberExpression.Expression.Type != typeof(IMediaFile) &&
                memberExpression.Expression.Type != typeof(IMediaFileFolder))
            {
                return false;
            }

            if (memberExpression.Member != _mediaFileStoreIdMemberInfo &&
                memberExpression.Member != _mediaFileFolderStoreIdMemberInfo)
            {
                return false;
            }

            return true;
        }



        private string GetStoreId(Expression expression)
        {
            if (expression is ConstantExpression)
            {
                var constantExpression = expression as ConstantExpression;

                if (constantExpression.Value == null || constantExpression.Type != typeof (string)) return null;

                return (string)constantExpression.Value;
            }

            if (expression is MemberExpression) 
            {
                var memberExpression = expression as MemberExpression;

                if (memberExpression.Expression.NodeType != ExpressionType.Constant) return null;

                object obj = ((ConstantExpression)memberExpression.Expression).Value;

                FieldInfo fieldInfo = memberExpression.Member as FieldInfo;

                if (fieldInfo == null) return null;

                return (string)fieldInfo.GetValue(obj);
            }
            
            throw new InvalidOperationException("This line should not be reachable");
        }
    }
}
