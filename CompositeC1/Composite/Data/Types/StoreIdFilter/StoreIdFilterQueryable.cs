/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Composite.Data.Types.StoreIdFilter.Foundation;


namespace Composite.Data.Types.StoreIdFilter
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public interface IStorageFilter 
    {
        /// <exclude />
        string StoreId { get; }
    }




    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class StoreIdFilterQueryable<T> : IStoreIdFilterQueryable, IOrderedQueryable<T>, IQueryProvider, IStorageFilter
    {
        private readonly IQueryable<T> _originalQueryable;
        private readonly string _storeId;
        private readonly Expression _currentExpression;


        private static readonly PropertyInfo _mediaFileStoreIdPropertyInfo = typeof(IMediaFile).GetProperty("StoreId");
        private static readonly PropertyInfo _mediaFileFolderStoreIdPropertyInfo = typeof(IMediaFileFolder).GetProperty("StoreId");

        /// <exclude />
        public StoreIdFilterQueryable(IQueryable<T> originalQueryable, string storeId)
        {
            _originalQueryable = originalQueryable;
            _storeId = storeId;
            _currentExpression = Expression.Constant(this);
        }



        /// <exclude />
        public StoreIdFilterQueryable(IQueryable<T> originalQueryable, string storeId, Expression currentExpression)
        {
            _originalQueryable = originalQueryable;
            _storeId = storeId;
            _currentExpression = currentExpression;
        }



        /// <exclude />
        public IQueryable<S> CreateQuery<S>(Expression expression)
        {
            var visitor = new StoreIdFilterQueryableChangeSourceExpressionVisitor();

            Expression newExpression = visitor.Visit(expression);

            IQueryable<S> newOriginalQueryable = _originalQueryable.Provider.CreateQuery<S>(newExpression);

            return new StoreIdFilterQueryable<S>(newOriginalQueryable, _storeId, expression);
        }



        /// <exclude />
        public IQueryable CreateQuery(Expression expression)
        {
            if (_currentExpression == expression) return this;

            MethodInfo methodInfo = StoreIdFilterQueryableCache.GetStoreIdFilterQueryableCreateQueryMethodInfo(typeof(T), expression.Type);

            return (IQueryable)methodInfo.Invoke(this, new object[] { expression });
        }



        /// <exclude />
        public S Execute<S>(Expression expression)
        {
            var visitor = new StoreIdFilterQueryableExpressionVisitor();

            visitor.Visit(_currentExpression);

            if (visitor.FoundStoreId == _storeId)
            {
                var sourceChangingVisitor = new StoreIdFilterQueryableChangeSourceExpressionVisitor();

                Expression newExpression = sourceChangingVisitor.Visit(expression);

                return _originalQueryable.Provider.Execute<S>(newExpression);
            }
            else
            {
                List<T> emptyList = new List<T>();

                var sourceChangingVisitor = new StoreIdFilterQueryableChangeSourceExpressionVisitor(emptyList.AsQueryable().Expression);

                Expression newExpression = sourceChangingVisitor.Visit(expression);

                return emptyList.AsQueryable().Provider.Execute<S>(newExpression);
            }
        }



        /// <exclude />
        public object Execute(Expression expression)
        {
            MethodInfo methodInfo = StoreIdFilterQueryableCache.GetStoreIdFilterQueryableExecuteMethodInfo(typeof(T), expression.Type);

            return methodInfo.Invoke(this, new object[] { expression });
        }



        /// <exclude />
        public IEnumerator<T> GetEnumerator()
        {
            var visitor = new StoreIdFilterQueryableExpressionVisitor();

            visitor.Visit(_currentExpression);

            if (visitor.FoundStoreId == null)
            {
                throw new InvalidOperationException("Missing storeId test found in where");
            }

            if (visitor.FoundStoreId != _storeId)
            {
                return new List<T>().GetEnumerator();
            }

            return FilterByStoreId(_originalQueryable).GetEnumerator();
        }


        IEnumerable<T> FilterByStoreId(IEnumerable<T> enumeration)
        {
            foreach (T item in enumeration)
            {
                if (GetStoreId(item) == _storeId)
                {
                    yield return item;
                }
            }
        }

        static string GetStoreId(T item)
        {
            if (item is IMediaFile)
            {
                return (string) _mediaFileStoreIdPropertyInfo.GetValue(item, null);
            }

            if (item is IMediaFileFolder)
            {
                return (string)_mediaFileFolderStoreIdPropertyInfo.GetValue(item, null);
            }

            throw new InvalidOperationException("This line should not be reachable");
        }

        /// <exclude />
        IEnumerator IEnumerable.GetEnumerator()
        {
            MethodInfo methodInfo = StoreIdFilterQueryableCache.GetStoreIdFilterQueryableGetEnumeratorMethodInfo(typeof(T));

            return (IEnumerator)methodInfo.Invoke(this, null);
        }



        /// <exclude />
        public Expression Expression
        {
            get { return _currentExpression; }
        }



        /// <exclude />
        public Type ElementType
        {
            get { return typeof(T); }
        }



        /// <exclude />
        public IQueryProvider Provider
        {
            get { return this; }
        }



        /// <exclude />
        public IQueryable Source
        {
            get { return _originalQueryable; }
        }


        /// <exclude />
        public string StoreId
        {
            get { return _storeId; }
        }
    }
}
