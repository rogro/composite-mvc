/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.IO;
using System.Web.Hosting;
using Composite.Core.IO;
using Composite.Data.Plugins.DataProvider.Streams;
using Composite.Data.Streams;


namespace Composite.Data.Types
{
    /// <summary>
    /// Extension methods for IFile
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class IFileExtensions
    {
        /// <exclude />
        public static Stream GetReadStream(this IFile file)
        {
            IFileStreamManager manager = FileStreamManagerLocator.GetFileStreamManager(file.GetType());

            return manager.GetReadStream(file);
        }



        /// <exclude />
        public static Stream GetNewWriteStream(this IFile file)
        {
            IFileStreamManager manager = FileStreamManagerLocator.GetFileStreamManager(file.GetType());

            return manager.GetNewWriteStream(file);
        }



        /// <exclude />
        public static void SubscribeOnChanged(this IFile file, OnFileChangedDelegate handler)
        {
            IFileStreamManager manager = FileStreamManagerLocator.GetFileStreamManager(file.GetType());

            manager.SubscribeOnFileChanged(file, handler);
        }

        /// <summary>
        /// Returns all text from the stream associated with the provided IFile
        /// </summary>
        public static string ReadAllText(this IFile file)
        {
            using (Stream fileStream = GetReadStream(file))
            {
                using (C1StreamReader sr = new C1StreamReader(fileStream))
                {
                    return sr.ReadToEnd();
                }
            }
        }



        /// <summary>
        /// Replaces the all files content with some new content
        /// </summary>
        /// <param name="file"></param>
        /// <param name="newContent"></param>
        public static void SetNewContent(this IFile file, string newContent)
        {
            using (C1StreamWriter sw = new C1StreamWriter(GetNewWriteStream(file)))
            {
                sw.Write(newContent);
            }
        }

        internal static string GetFilePath(this IFile file)
        {
            return (file is FileSystemFileBase) ? (file as FileSystemFileBase).SystemPath : null;
        }

        internal static string GetRelativeFilePath(this IFile file)
        {
            string filePath = GetFilePath(file);
            if (filePath == null) return null;

            return filePath.StartsWith(PathUtil.BaseDirectory) ? filePath.Substring(PathUtil.BaseDirectory.Length) : filePath;
        }
    }
}