/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Composite.Data.ProcessControlled;
using Composite.Data.ProcessControlled.ProcessControllers.GenericPublishProcessController;
using Composite.Data.Transactions;


namespace Composite.Data.Types.Foundation
{
    internal sealed class PagePublishControlledAuxiliary : IPublishControlledAuxiliary
    {
        public void OnAfterDataUpdated(IData data)
        {
            IPage page = (IPage)data;

            IEnumerable<IPagePlaceholderContent> pagePlaceholderContents;
            using (DataScope dataScope = new DataScope(DataScopeIdentifier.Administrated))
            {
                pagePlaceholderContents =
                    (from content in DataFacade.GetData<IPagePlaceholderContent>()
                     where content.PageId == page.Id
                     select content).ToList();
            }

            if (page.PublicationStatus == GenericPublishProcessController.Published)
            {
                using (TransactionScope transactionScope = TransactionsFacade.CreateNewScope())
                {
                    using (DataScope dataScope = new DataScope(DataScopeIdentifier.Public))
                    {
                        DataFacade.Delete<IPagePlaceholderContent>(f => f.PageId == page.Id);
                    }

                    foreach (IPagePlaceholderContent pagePlaceholderContent in pagePlaceholderContents)
                    {
                        pagePlaceholderContent.PublicationStatus = page.PublicationStatus;

                        DataFacade.Update(pagePlaceholderContent);
                    }

                    using (DataScope dataScope = new DataScope(DataScopeIdentifier.Administrated))
                    {
                        IPagePublishSchedule pagePublishSchedule =
                            (from pps in DataFacade.GetData<IPagePublishSchedule>()
                             where pps.PageId == page.Id
                             select pps).SingleOrDefault();

                        if (pagePublishSchedule != null)
                        {
                            DataFacade.Delete<IPagePublishSchedule>(pagePublishSchedule);
                        }
                    }

                    transactionScope.Complete();
                }
            }
            else
            {
                foreach (IPagePlaceholderContent pagePlaceholderContent in pagePlaceholderContents)
                {
                    if (pagePlaceholderContent.PublicationStatus != page.PublicationStatus)
                    {
                        pagePlaceholderContent.PublicationStatus = page.PublicationStatus;

                        DataFacade.Update(pagePlaceholderContent);
                    }
                }
            }
        }


        public void OnAfterBuildNew(IData data)
        {
            // Noop
        }
    }
}
