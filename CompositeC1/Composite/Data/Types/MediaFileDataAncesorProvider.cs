/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Composite.Data.Hierarchy;
using Composite.Data;
using Composite.Data.Types;
using Composite.Core.Extensions;

namespace Composite.Data.Types
{
    internal sealed class MediaFileDataAncesorProvider : IDataAncestorProvider
	{

        public IData GetParent(IData data)
        {
            IData parent = null;
            if (data is IMediaFile)
            {
                IMediaFile file = (IMediaFile)data;

                parent = (from item in DataFacade.GetData<IMediaFileFolder>()
                          where item.Path == file.FolderPath && item.StoreId == file.StoreId
                          select item).FirstOrDefault();
 
            }
            else if (data is IMediaFileFolder)
            {
                IMediaFileFolder folder = (IMediaFileFolder)data;

                int lastIndex = folder.Path.LastIndexOf('/');
                if(lastIndex == 0)
                {
                    return null;
                }

                string parentPath = folder.Path.Substring(0, lastIndex);
                parent = (from item in DataFacade.GetData<IMediaFileFolder>()
                          where item.Path == parentPath && item.StoreId == folder.StoreId
                          select item).FirstOrDefault();
            }
            else
            {
                throw new ArgumentException("Must be either of type IMediaFile or IMediaFileFolder", "data");
            }


            return parent;
        }

    }
}
