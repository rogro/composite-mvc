/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.Data;
using Composite.Data.Hierarchy;
using Composite.Data.Hierarchy.DataAncestorProviders;
using Composite.Data.Caching;
using Composite.Data.Validation.Validators;


namespace Composite.Data.Types
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [AutoUpdateble]
    [ImmutableTypeId("{F6F0B424-0AFA-4d9a-9DF1-C57F2B7F7C8D}")]
    [KeyPropertyName("Id")]
    [DataAncestorProvider(typeof(NoAncestorDataAncestorProvider))]
    [DataScope(DataScopeIdentifier.PublicName)]
    public interface IXsltFunction : IData
    {
        /// <exclude />
        [ImmutableFieldId("{25E73650-E7F8-4a4b-8510-9BDA1C1B2D61}")]
        [StoreFieldType(PhysicalStoreFieldType.Guid)]
        Guid Id { get; set; }


        /// <exclude />
        [ImmutableFieldId("{8DD6A2E7-CDCE-46b9-B517-36D633B98311}")]
        [StoreFieldType(PhysicalStoreFieldType.String, 256)]
        [Composite.Data.Validation.Validators.StringSizeValidator(1, 256)]
        string Name { get; set; }


        /// <exclude />
        [ImmutableFieldId("{DC241562-2B30-4d06-852D-12E5CFF81EE8}")]
        [StoreFieldType(PhysicalStoreFieldType.String, 512)]
        [Composite.Data.Validation.Validators.StringSizeValidator(1, 512)]
        string Namespace { get; set; }


        /// <exclude />
        [ImmutableFieldId("{2670BEEE-4A6A-4f0f-83E6-8103EEA25D09}")]
        [StoreFieldType(PhysicalStoreFieldType.String, 512)]
        string Description { get; set; }


        /// <exclude />
        [ImmutableFieldId("{1B695E1A-9EBD-4ce8-BA36-711D1E84D8AF}")]
        [StoreFieldType(PhysicalStoreFieldType.String, 32)]
        string OutputXmlSubType { get; set; }


        /// <exclude />
        [ImmutableFieldId("{271AEA09-75CC-45d5-9D7F-C96B1177D046}")]
        [StoreFieldType(PhysicalStoreFieldType.String, 1024)]
        string XslFilePath { get; set; }
    }
}
