/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using Composite.Data.Types.Foundation;
using Composite.Core.Serialization;
using Composite.Data.DynamicTypes;


namespace Composite.Data.Types
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [BuildNewHandler(typeof(IFileBuildNewHandler))]
    [DataScope(DataScopeIdentifier.PublicName)]
    [SerializerHandler(typeof(ExceptingSerializerHandler))]
    public interface IFile : IData
    {
        /// <exclude />
        [ImmutableFieldId("{B9B50947-EE78-4744-9128-0832A66243D5}")]
        [StoreFieldType(PhysicalStoreFieldType.String, 512)]
        string FolderPath { get; set; }


        /// <exclude />
        [ImmutableFieldId("{4FD98564-6077-4c6d-86E1-9C7F44B65E50}")]
        [StoreFieldType(PhysicalStoreFieldType.String, 128)]
        string FileName { get; set; }
    }




    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class IFileUtils
    {
        /// <exclude />
        public static bool ValidateValueLengths(this IFile file)
        {
            if ((file.FolderPath != null) && (file.FolderPath.Length > DataTypeDescriptor.Fields["FolderPath"].StoreType.MaximumLength)) return false;
            if ((file.FileName != null) && (file.FileName.Length > DataTypeDescriptor.Fields["FileName"].StoreType.MaximumLength)) return false;

            return true;
        }



        private static DataTypeDescriptor _dataTypeDescriptor = null;
        private static DataTypeDescriptor DataTypeDescriptor
        {
            get
            {
                if (_dataTypeDescriptor == null)
                {
                    _dataTypeDescriptor = DynamicTypeManager.GetDataTypeDescriptor(typeof(IFile));
                }

                return _dataTypeDescriptor;
            }
        }
    }
}