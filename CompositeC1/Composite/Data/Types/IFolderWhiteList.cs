/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.Data.Hierarchy;
using Composite.Data.Hierarchy.DataAncestorProviders;
using Composite.Core.IO;


namespace Composite.Data.Types
{
    /// <summary>    
    /// Reference to a folder to be shown in the 'Layout' perspective.
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [AutoUpdateble]
    [ImmutableTypeId("{b831fee3-fb55-44be-b00d-034bbd83574f}")]
    [KeyPropertyName(0, "KeyName")]
    [KeyPropertyName(1, "TildeBasedPath")]
    [DataAncestorProvider(typeof(NoAncestorDataAncestorProvider))]
    [DataScope(DataScopeIdentifier.AdministratedName)]
    public interface IFolderWhiteList : IData
    {
        /// <exclude />
        [ImmutableFieldId("{cb0bb5a7-c1fe-47a1-bfb4-6565bc9ffd4d}")]
        [StoreFieldType(PhysicalStoreFieldType.String, 128)]
        string KeyName { get; set; }


        /// <exclude />
        [ImmutableFieldId("{d03415bd-ec41-4a57-8072-59e0a761f113}")]
        [StoreFieldType(PhysicalStoreFieldType.String, 512)]
        string TildeBasedPath { get; set; }
    }




    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class IFolderWhiteListExtensions
    {
        /// <exclude />
        public static string GetTildePath(string fullPath)
        {
            try
            {
                if (PathUtil.BaseDirectory.StartsWith(fullPath))
                {
                    return "~\\";
                }

                string withoutBase = fullPath.Substring(PathUtil.BaseDirectory.Length);
                if (withoutBase.StartsWith("\\") == false)
                {
                    withoutBase = "\\" + withoutBase;
                }

                return "~" + withoutBase;
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(string.Format("Failed to get tilde based path from '{0}'", fullPath), ex);
            }
        }


        /// <exclude />
        public static string GetFullPath(this IFolderWhiteList folderWhiteList)
        {
            return PathUtil.Resolve(folderWhiteList.TildeBasedPath);
        }
    }
}