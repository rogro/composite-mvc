/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

namespace Composite.Data.Types
{

    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [AutoUpdateble]
    [ImmutableTypeId("{67cf4b4d-1376-4589-abd4-5cfa9966670b}")]
    [KeyPropertyName("FunctionName")]
    [DataScope(DataScopeIdentifier.PublicName)]
    [CachingAttribute(CachingType.Full)]
    public interface ICustomFunctionCallEditorMapping: IData
    {
        /// <exclude />
        [StoreFieldType(PhysicalStoreFieldType.String, 255)]
        [ImmutableFieldId("{1145f17b-c93b-443f-917e-88b7cc4e85c5}")]
        string FunctionName { get; set; }

        /// <exclude />
        [StoreFieldType(PhysicalStoreFieldType.String, 255)]
        [ImmutableFieldId("{96671d53-4657-439b-8abd-4e01f3c80fd7}")]
        string CustomEditorPath { get; set; }

        /// <exclude />
        [StoreFieldType(PhysicalStoreFieldType.Integer, IsNullable = true)]
        [ImmutableFieldId("{3af4e226-c06d-448d-a93e-3457cd4e9bf6}")]
        int? Width { get; set; }

        /// <exclude />
        [StoreFieldType(PhysicalStoreFieldType.Integer, IsNullable = true)]
        [ImmutableFieldId("{7bd25407-193b-42e5-ae59-26a99bdee2c1}")]
        int? Height { get; set; }
    }
}
 