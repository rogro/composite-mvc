/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.Data.Hierarchy;
using Composite.Data.Hierarchy.DataAncestorProviders;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;


namespace Composite.Data.Types
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]  
    [Title("Hostname mapping")]
    [AutoUpdateble]
    [ImmutableTypeId("{2B0B1268-7237-4482-97A3-1BD4CAD6A08C}")]
    [KeyPropertyName("Id")]
    [LabelPropertyName("Hostname")]
    [DataScope(DataScopeIdentifier.PublicName)]
    [CachingAttribute(CachingType.Full)]
    [DataAncestorProvider(typeof(NoAncestorDataAncestorProvider))]
    public interface IHostnameBinding : IData
    {
        /// <exclude />
        [StoreFieldType(PhysicalStoreFieldType.Guid)]
        [ImmutableFieldId("{405A7A25-6E53-4DDF-A6C2-26714D26D1F5}")]
        Guid Id { get; set; }

        /// <exclude />
        [StoreFieldType(PhysicalStoreFieldType.String, 255, IsNullable = false)]
        [ImmutableFieldId("{36E7E803-178A-4453-9B5B-BF7148BA077B}")]
        [RegexValidator(@"^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$")]
        [NotNullValidator]
        string Hostname { get; set; }

        /// <exclude />
        [StoreFieldType(PhysicalStoreFieldType.String, 64, IsNullable = false)]
        [ImmutableFieldId("{A9C79722-D62A-481A-B1DE-CFB37A68EB9A}")]
        [NotNullValidator]
        string Culture { get; set; }

        /// <exclude />
        [StoreFieldType(PhysicalStoreFieldType.Guid)]
        [ImmutableFieldId("{2C2AEBF7-2CFC-4B6B-9199-991E4ABD8FFC}")]
        [DefaultFieldGuidValue("{00000000-0000-0000-0000-000000000000}")]
        Guid HomePageId { get; set; }

        /// <exclude />
        [StoreFieldType(PhysicalStoreFieldType.String, 255, IsNullable = true)]
        [ImmutableFieldId("{B0ADBBBF-15C9-4902-B202-BD4A1014725D}")]
        string PageNotFoundUrl { get; set; }

        /// <exclude />
        [StoreFieldType(PhysicalStoreFieldType.String, 512, IsNullable = true)]
        [ImmutableFieldId("{80C298F2-F493-465D-9F28-4F50DD0C03D5}")]
        string Aliases { get; set; }

        /// <exclude />
        [StoreFieldType(PhysicalStoreFieldType.Boolean)]
        [ImmutableFieldId("{3ED674CC-24C2-2DDB-C53D-71421AA03127}")]
        bool IncludeHomePageInUrl { get; set; }

        /// <exclude />
        [StoreFieldType(PhysicalStoreFieldType.Boolean)]
        [ImmutableFieldId("{A554BD68-C2F5-6F6F-4D3B-1FC2760BDE38}")]
        bool IncludeCultureInUrl { get; set; }
    }
}
