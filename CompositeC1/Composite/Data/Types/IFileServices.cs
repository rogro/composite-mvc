/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.IO;
using System.Linq;


namespace Composite.Data.Types
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class IFileServices
    {
        /// <exclude />
        public static T GetFile<T>(string filePath)
            where T : class, IFile
        {
            string folderPath = Path.GetDirectoryName(filePath);
            string fileName = Path.GetFileName(filePath);

            var foundFile =
                (from file in DataFacade.GetData<T>()
                 where string.Compare(file.FolderPath, folderPath, StringComparison.OrdinalIgnoreCase) == 0
                    && string.Compare(file.FileName, fileName, StringComparison.OrdinalIgnoreCase) == 0
                 select file).ToList();

            if (foundFile.Count == 0) throw new InvalidOperationException(string.Format("Missing file '{0}'", filePath));
            if (foundFile.Count > 1) throw new InvalidOperationException(string.Format("More than one file named '{0}'", filePath));

            return foundFile[0];
        }



        /// <exclude />
        public static T TryGetFile<T>(string filePath)
            where T : class, IFile
        {
            string folderPath = Path.GetDirectoryName(filePath);
            string fileName = Path.GetFileName(filePath);

            var foundFile =
                (from file in DataFacade.GetData<T>()
                 where file.FolderPath.Equals(folderPath, StringComparison.OrdinalIgnoreCase) 
                    && file.FileName.Equals(fileName, StringComparison.OrdinalIgnoreCase)
                 select file).ToList();

            if (foundFile.Count == 0) return null;
            if (foundFile.Count > 1) return null;

            return foundFile[0];
        }
    }
}
