/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using Composite.Data.Hierarchy;
using Composite.Data.Hierarchy.DataAncestorProviders;
using Composite.Data.Validation.Validators;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;


namespace Composite.Data.Types
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    [AutoUpdateble]
    [Caching(CachingType.Full)]
    [KeyPropertyName("Username")]
    [LabelPropertyName("Username")]
    [DataScope(DataScopeIdentifier.PublicName)]
    [ImmutableTypeId("{F89054CB-C0E0-41ad-948D-4AEA4055C3A3}")]
    [DataAncestorProvider(typeof(NoAncestorDataAncestorProvider))]
    public interface IUserSettings : IData
    {
        /// <exclude />
        [NotNullValidator()]
        [StringSizeValidator(2, 64)]
        [StoreFieldType(PhysicalStoreFieldType.String, 64)]
        [ImmutableFieldId("{A7BEC71E-0CA9-4b33-A1E7-F2B5B5744647}")]
        [ForeignKey(typeof(IUser), "Username", AllowCascadeDeletes = true)]
        string Username { get; set; }


        /// <exclude />
        [NotNullValidator()]
        [StringSizeValidator(2, 16)]
        [StoreFieldType(PhysicalStoreFieldType.String, 16)]
        [ImmutableFieldId("{9D62C8D3-E42F-4926-8E45-5B465A59C8A6}")]
        string CultureName { get; set; }

        /// <exclude />
        [NotNullValidator()]
        [StringSizeValidator(2, 16)]
        [StoreFieldType(PhysicalStoreFieldType.String, 16)]
        [ImmutableFieldId("{F1BC3D80-AD86-4730-A6EE-65E19BEFD443}")]
        [DefaultFieldStringValue("en-US")] //for easy upgrade
        string C1ConsoleUiLanguage { get; set; }

        /// <exclude />
        [StoreFieldType(PhysicalStoreFieldType.String, 16, IsNullable = true)]
        [ImmutableFieldId("{931130A1-5CDD-487a-B51F-76A0396D3216}")]
        string CurrentActiveLocaleCultureName { get; set; }


        /// <exclude />
        [StoreFieldType(PhysicalStoreFieldType.String, 16, IsNullable = true)]
        [ImmutableFieldId("{0D354A92-461D-4ff8-B797-F2897119CD3B}")]
        string ForeignLocaleCultureName { get; set; }
    }
}
