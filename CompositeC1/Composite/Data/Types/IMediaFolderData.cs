/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;

namespace Composite.Data.Types
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [AutoUpdateble]
    [ImmutableTypeId("{cb22316c-4e41-4fe5-a30d-5abc35af124a}")]
    [KeyPropertyName("Id")]
    [DataScope(DataScopeIdentifier.PublicName)]
    [NotReferenceable]
    [CachingAttribute(CachingType.Full)]
    public interface IMediaFolderData : IData
	{
        /// <exclude />
        [ImmutableFieldId("{aa5d40be-d250-4794-b517-bd6977658273}")]
        [StoreFieldType(PhysicalStoreFieldType.Guid)]        
        Guid Id { get; set; }


        /// <exclude />
        [ImmutableFieldId("{cd4f0524-3bfd-4110-bc93-e99713a7a5b7}")]
        [StoreFieldType(PhysicalStoreFieldType.String, 2048, IsNullable = false)]        
        string Path { get; set; }


        /// <exclude />
        [ImmutableFieldId("{4df0e07f-24c0-4d62-bdc7-d50620a530db}")]
        [StoreFieldType(PhysicalStoreFieldType.String, 256, IsNullable = true)]
        string Title { get; set; }


        /// <exclude />
        [ImmutableFieldId("{65e42128-8580-4898-a2fd-0e35cf771c24}")]
        [StoreFieldType(PhysicalStoreFieldType.LargeString, IsNullable = true)]
        string Description { get; set; }
	}
}
