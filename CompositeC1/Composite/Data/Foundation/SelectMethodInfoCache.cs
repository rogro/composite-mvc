/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;


namespace Composite.Data.Foundation
{
    internal sealed class SelectMethodInfoCache
    {
        private static Dictionary<CacheEntry, MethodInfo> _methodInfoCache = new Dictionary<CacheEntry, MethodInfo>();

        
        public static MethodInfo GetSelectMethod(Type sourceType, Type targetType)
        {
            CacheEntry entry = new CacheEntry(sourceType, targetType);
            
            if ( false == _methodInfoCache.ContainsKey(entry))
            {
                MethodInfo genericSelectMethod = 
                    (from method in typeof(Queryable).GetMethods(BindingFlags.Static | BindingFlags.Public)
                     where method.Name == "Select" && 
                           method.IsGenericMethod && 
                           method.GetGenericArguments().Length == 2
                     select method).First();

                MethodInfo selectMethod = genericSelectMethod.MakeGenericMethod(new Type[] { sourceType, targetType });

                _methodInfoCache.Add(entry, selectMethod);
            }

            return _methodInfoCache[entry];
        }


        private sealed class CacheEntry
        {
            private Type _sourceType;
            private Type _targetType;

            public CacheEntry(Type sourceType, Type targetType)
            {
                _sourceType = sourceType;
                _targetType = targetType;
            }

            public override bool Equals(object obj)
            {
                CacheEntry entry = obj as CacheEntry;
                
                if (null == entry) return false;

                return entry._sourceType == _sourceType && entry._targetType == _targetType;
            }

            public override int GetHashCode()
            {
                return _sourceType.GetHashCode() ^ _targetType.GetHashCode();
            }
        }
    }
}
