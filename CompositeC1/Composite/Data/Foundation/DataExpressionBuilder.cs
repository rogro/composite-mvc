/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Linq.Expressions;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using System;
using Composite.Core.Linq;
using Composite.C1Console.Events;
using Composite.Data.ProcessControlled;


namespace Composite.Data.Foundation
{
    internal static class DataExpressionBuilder
    {
        private static object _lock = new object();



        static DataExpressionBuilder()
        {
        }



        /// <summary>
        /// This method returns a queryable containing data with the same keys
        /// </summary>
        /// <param name="data"></param>        
        /// <returns></returns>
        public static IQueryable GetQueryableByData(IData data)
        {
            return GetQueryableByData(data, true);
        }



        public static IQueryable GetQueryableByData(IData data, bool ignoreVersioning)
        {
            return GetQueryableByData(data, null, ignoreVersioning);
        }



        public static IQueryable GetQueryableByData(IData data, IQueryable sourceQueryable, bool ignoreVersioning)
        {
            LambdaExpression whereLambdaExpression = GetWhereLambdaExpression(data, ignoreVersioning);

            if (sourceQueryable == null)
            {
                sourceQueryable = DataFacade.GetData(data.DataSourceId.InterfaceType);
            }

            Expression whereExpression = ExpressionCreator.Where(sourceQueryable.Expression, whereLambdaExpression);

            IQueryable newQueryable = sourceQueryable.Provider.CreateQuery(whereExpression);

            return newQueryable;
        }



        public static IQueryable<T> GetQueryableByData<T>(T data, bool ignoreVersioning)
            where T : class, IData
        {
            LambdaExpression whereLambdaExpression = GetWhereLambdaExpression(data, ignoreVersioning);

            IQueryable queryable = DataFacade.GetData(data.DataSourceId.InterfaceType);

            Expression whereExpression = ExpressionCreator.Where(queryable.Expression, whereLambdaExpression);

            MethodInfo methodInfo = DataFacadeReflectionCache.GetCreateQueryMethodInfo(data.DataSourceId.InterfaceType);

            return (IQueryable<T>)methodInfo.Invoke(queryable.Provider, new object[] { whereExpression });
        }



        public static Delegate GetWherePredicateDelegate(IData data, bool ignoreVersioning)
        {
            LambdaExpression whereLambdaExpression = GetWhereLambdaExpression(data, ignoreVersioning);

            Delegate resultDelegate = whereLambdaExpression.Compile();

            return resultDelegate;
        }



        private static LambdaExpression GetWhereLambdaExpression(IData data, bool ignoreVersioning)
        {
            List<PropertyInfo> propertyInfoes = data.DataSourceId.InterfaceType.GetKeyProperties();

            if (propertyInfoes.Count == 0)
            {
                throw new InvalidOperationException(string.Format("The data type '{0}' does not have any keys specified", data.DataSourceId.InterfaceType));
            }

            ParameterExpression parameterExpression = Expression.Parameter(data.DataSourceId.InterfaceType, "parameter");

            Expression whereBodyExpression = null;
            foreach (PropertyInfo propertyInfo in propertyInfoes)
            {
                object value = propertyInfo.GetGetMethod().Invoke(data, new object[] { });

                Expression expression =
                    Expression.Equal(
                        Expression.Property(parameterExpression, propertyInfo),
                        Expression.Constant(value)
                    );

                if (whereBodyExpression == null)
                {
                    whereBodyExpression = expression;
                }
                else
                {
                    whereBodyExpression = Expression.And(whereBodyExpression, expression);
                }
            }

            LambdaExpression whereLambdaExpression = Expression.Lambda(whereBodyExpression, new ParameterExpression[] { parameterExpression });

            return whereLambdaExpression;
        }
    }
}
