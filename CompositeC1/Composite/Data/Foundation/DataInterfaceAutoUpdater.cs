/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Composite.Data.DynamicTypes;
using Composite.Core.Instrumentation;
using Composite.Core.Logging;
using Composite.Data.Types;


namespace Composite.Data.Foundation
{
    internal static class DataInterfaceAutoUpdater
    {
        internal static bool EnsureUpdateAllInterfaces()
        {
            using (TimerProfilerFacade.CreateTimerProfiler())
            {
                bool doFlush = false;

                var knownInterafces = DataProviderRegistry.AllKnownInterfaces.ToList(); 

                if(!knownInterafces.Contains(typeof(IXmlPageTemplate)))
                {
                    knownInterafces.Insert(0, typeof(IXmlPageTemplate));
                }

                foreach (Type interfaceType in knownInterafces)
                {
                    if (!interfaceType.IsAutoUpdateble()) continue;
                    if (interfaceType.IsGenerated()) continue;

                    foreach (string providerName in DataProviderRegistry.GetDataProviderNamesByInterfaceType(interfaceType))
                    {
                        try
                        {
                            if (DynamicTypeManager.EnsureUpdateStore(interfaceType, providerName, true))
                            {
                                doFlush = true;
                            }
                        }
                        catch (TypeUpdateVersionException)
                        {
                            throw;
                        }
                        catch (TypeInitializationException tiex)
                        {
                            throw new InvalidOperationException("The data type meta stored did not initialize. Check configuration", tiex);
                        }
                        catch (Exception ex)
                        {
                            LoggingService.LogCritical("DataInterfaceAutoUpdater", string.Format("Update failed for the interface '{0}' on the '{1}' data provider", interfaceType, providerName));
                            LoggingService.LogCritical("DataInterfaceAutoUpdater", ex);
                        }
                    }
                }

                return doFlush;
            }
        }



        internal static void TestEnsureUpdateAllInterfaces()
        {
            using (TimerProfiler timerProfiler = TimerProfilerFacade.CreateTimerProfiler())
            {
                foreach (Type interfaceType in DataProviderRegistry.AllInterfaces)
                {
                    if (!interfaceType.IsAutoUpdateble()) continue;
                    if (interfaceType.IsGenerated()) continue;

                    foreach (string providerName in DataProviderRegistry.GetDataProviderNamesByInterfaceType(interfaceType))
                    {
                        try
                        {
                            if (DynamicTypeManager.IsEnsureUpdateStoreNeeded(interfaceType))
                            {
                                LoggingService.LogError("DataInterfaceAutoUpdater", string.Format("Autoupdating the data interface '{0}' on the '{1}' data provider failed!", interfaceType, providerName));
                            }
                        }
                        catch (Exception ex)
                        {
                            LoggingService.LogCritical("DataInterfaceAutoUpdater", string.Format("Update failed for the interface '{0}' on the '{1}' data provider", interfaceType, providerName));
                            LoggingService.LogCritical("DataInterfaceAutoUpdater", ex);
                        }
                    }
                }
            }
        }
    }
}
