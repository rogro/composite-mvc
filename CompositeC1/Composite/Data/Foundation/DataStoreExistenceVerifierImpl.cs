/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Composite.Core;
using Composite.Data.DynamicTypes;
using Composite.Data.Foundation.PluginFacades;
using Composite.Data.Plugins.DataProvider.Runtime;
using Composite.Data.Types;
using Composite.Plugins.WebClient.SessionStateProviders.DefaultSessionStateProvider;


namespace Composite.Data.Foundation
{   
    internal sealed class DataStoreExistenceVerifierImpl : IDataStoreExistenceVerifier
    {
        // Interfaces in this list will have a store created on system start if they do not exists
        private static readonly List<Type> _interfaceTypes = new List<Type>
                {
                    typeof(ICompositionContainer),
                    typeof(ICustomFunctionCallEditorMapping),
                    typeof(IDataItemTreeAttachmentPoint),
                    typeof(IFlowInformation),
                    typeof(IFolderWhiteList),
                    typeof(IGeneratedTypeWhiteList),
                    typeof(IHostnameBinding),
                    typeof(IInlineFunction),
                    typeof(IInlineFunctionAssemblyReference),
                    typeof(ILockingInformation),
                    typeof(IMediaFile),
                    typeof(IMediaFileData),
                    typeof(IMediaFileFolder),
                    typeof(IMediaFolderData),
                    typeof(IMethodBasedFunctionInfo),
                    typeof(INamedFunctionCall),
                    typeof(IPackageServerSource),
                    typeof(IPage),
                    typeof(IPageFolderDefinition),
                    typeof(IPageMetaDataDefinition),
                    typeof(IPagePlaceholderContent),
                    typeof(IPagePublishSchedule),
                    typeof(IPageStructure),
                    typeof(IXmlPageTemplate),
                    typeof(IPageType),
                    typeof(IPageTypeDataFolderTypeLink),
                    typeof(IPageTypeDefaultPageContent),
                    typeof(IPageTypeMetaDataTypeLink),
                    typeof(IPageTypePageTemplateRestriction),
                    typeof(IPageTypeParentRestriction),
                    typeof(IPageTypeTreeLink),
                    typeof(IPageUnpublishSchedule),
                    typeof(IParameter),
                    typeof(ISearchEngineOptimizationKeyword),
                    typeof(ISessionStateEntry),
                    typeof(ISqlConnection),
                    typeof(ISqlFunctionInfo),
                    typeof(ISystemActiveLocale),
                    typeof(ITaskItem),
                    typeof(IUrlConfiguration),
                    typeof(IUser),
                    typeof(IUserActiveLocale),
                    typeof(IUserActivePerspective),
                    typeof(IUserConsoleInformation),
                    typeof(IUserDeveloperSettings),
                    typeof(IUserGroup),
                    typeof(IUserGroupActivePerspective),
                    typeof(IUserGroupPermissionDefinition),
                    typeof(IUserGroupPermissionDefinitionPermissionType),
                    typeof(IUserPermissionDefinition),
                    typeof(IUserPermissionDefinitionPermissionType),
                    typeof(IUserSettings),
                    typeof(IUserUserGroupRelation),
                    typeof(IVisualFunction),
                    typeof(IXsltFunction)
                };        


        public IEnumerable<Type> InterfaceTypes
        {
            get { return _interfaceTypes; }
        }



        public bool EnsureDataStores()
        {
            if (DataProviderPluginFacade.HasConfiguration())
            {
                List<DataTypeDescriptor> typeDescriptors = new List<DataTypeDescriptor>();

                foreach (Type type in _interfaceTypes)
                {
                    try
                    {
                        if (!DataProviderRegistry.AllKnownInterfaces.Contains(type))
                        {
                            var dataTypeDescriptor = DynamicTypeManager.BuildNewDataTypeDescriptor(type);

                            dataTypeDescriptor.Validate();

                            DataProviderPluginFacade.CreateStore(DataProviderRegistry.DefaultDynamicTypeDataProviderName, dataTypeDescriptor);

                            typeDescriptors.Add(dataTypeDescriptor);

                            Log.LogVerbose("DataStoreExistenceVerifier", string.Format("A store for the type '{0}' has been created", type));
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new InvalidOperationException(string.Format("Creating '{0}' store failed", type), ex);
                    }
                }

                return typeDescriptors.Count > 0;
            }
            
            Log.LogError("DataStoreExistenceVerifier", string.Format("Failed to load the configuration section '{0}' from the configuration", DataProviderSettings.SectionName));
            return false;
        }
    }
}
