/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.ComponentModel;
using System.Configuration;
using Composite.Core.Configuration;
using Composite.Data.ProcessControlled;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace Composite.Data.Foundation
{
    internal class ProcessControllerSettings : SerializableConfigurationSection
    {
        public const string SectionName = "Composite.Data.Foundation.ProcessControllerConfiguration";

        private const string ProcessControllersPropertyName = "ProcessControllers";
        [ConfigurationProperty(ProcessControllersPropertyName)]
        public NamedElementCollection<ProcessControllerData> ProcessControllers
        {
            get
            {
                return (NamedElementCollection<ProcessControllerData>)base[ProcessControllersPropertyName];
            }
        }
    }

    internal class ProcessControllerData : NamedConfigurationElement
    {
        private const string InterfaceTypePropertyName = "interfaceType";
        private const string AttributeTypePropertyName = "attributeType";

        /// <summary>
        /// The interface type for the <see cref="IProcessController"/> that will
        /// do the processing
        /// </summary>
        [ConfigurationProperty(InterfaceTypePropertyName, IsRequired = true)]
        [TypeConverter(typeof(TypeManagerTypeNameConverter))]
        public Type InterfaceType
        {
            get { return (Type)this[InterfaceTypePropertyName]; }
            set { this[InterfaceTypePropertyName] = value; }
        }

        /// <summary>
        /// The <see cref="ProcessControllerTypeAttribute"/> attached to the IData
        /// object
        /// </summary>
        [ConfigurationProperty(AttributeTypePropertyName, IsRequired = true)]
        [TypeConverter(typeof(TypeManagerTypeNameConverter))]
        public Type AttributeType
        {
            get { return (Type)this[AttributeTypePropertyName]; }
            set { this[AttributeTypePropertyName] = value; }
        }

    }
}