/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;


namespace Composite.Data
{
    /// <summary>
    /// Add this attribute to your data interface to make it visible in the C1 Console developer UI.
    /// </summary>
    /// <example> This sample shows how to use the RelevantToUserType attribute.
    /// <code>
    /// [RelevantToUserType(UserType.Developer)]
    /// // (other IData attributes)
    /// interface IMyDataType : IData
    /// {
    ///     // data type properties
    /// }
    /// </code>
    /// </example>    
    [AttributeUsage(AttributeTargets.Interface, AllowMultiple = true, Inherited = true)]
    public sealed class RelevantToUserTypeAttribute : Attribute
    {
        /// <summary>
        /// Make your data interface visible in the C1 Console developer UI.
        /// </summary>
        /// <param name="relevantToUserType">The <see cref="Composite.Data.UserType"/> this data type is relevant to. Only 'Developer' is available.</param>
        public RelevantToUserTypeAttribute(UserType relevantToUserType)
        {
            this.UserType = relevantToUserType;
        }



        /// <exclude />
        public RelevantToUserTypeAttribute(string relevantToUserTypeString)
        {
            this.UserType = (UserType)Enum.Parse( typeof(UserType), relevantToUserTypeString );
        }



        /// <exclude />
        public UserType UserType
        {
            get;
            private set;
        }
    }
}
