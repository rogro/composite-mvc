/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Data.SqlClient;

namespace Composite.Data
{
    /// <summary>
    /// Add this attribute to your data interface to specify one or more primary key fields.
    /// </summary>
    /// <example> This sample shows how to use the KeyPropertyName attribute:
    /// <code>
    /// [KeyPropertyName("Id")]
    /// // (other IData attributes)
    /// interface IMyDataType : IData
    /// {
    ///     Guid Id { get; set; }
    ///     
    ///     // other data type properties
    /// }
    /// </code>
    /// 
    /// This example shows how to specify a compound key. :
    /// <code>
    /// [KeyPropertyName(0, "FolderName")]
    /// [KeyPropertyName(1, "FileName")]
    /// // (other IData attributes)
    /// interface IMyDataType : IData
    /// {
    ///     string FolderName { get; set; }
    ///     string FileName { get; set; }
    ///     
    ///     // other data type properties
    /// }
    /// </code>
    /// </example>    
    [AttributeUsage(AttributeTargets.Interface, AllowMultiple = true, Inherited = true)]
    public sealed class KeyPropertyNameAttribute : Attribute
    {
        /// <summary>
        /// Specify the name of a data property to be used as primary key.
        /// </summary>
        /// <param name="compoundKeyIndex">The index of the field in a multi part key.</param>
        /// <param name="propertyName">Name of data property</param>
        public KeyPropertyNameAttribute(int compoundKeyIndex, string propertyName)
        {
            Index = compoundKeyIndex;
            KeyPropertyName = propertyName;
        }

        /// <summary>
        /// Specify the name of a data property to be used as primary key.
        /// </summary>
        /// <param name="propertyName">Name of data property</param>
        public KeyPropertyNameAttribute(string propertyName)
        {
            this.KeyPropertyName = propertyName;
        }

        /// <summary>
        /// Gets the name of the key property.
        /// </summary>
        /// <value>
        /// The name of the key property.
        /// </value>
        /// <exclude />
        public string KeyPropertyName { get; private set; }

        /// <summary>
        /// An index of the field in a multi part primary key.
        /// </summary>
        public int Index { get; private set; }

        // TODO: implement support for sort order

        ///// <summary>
        ///// A sort order for indexing.
        ///// </summary>
        //public SortOrder SortOrder { get; set; }
    }
}
