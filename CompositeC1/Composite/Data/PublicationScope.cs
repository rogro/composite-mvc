/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Composite.Data
{
    /// <summary>
    /// Define the scope of data in relation to publication status. Data which support publication should always be maintained 
    /// in the “Unpublihed” scope, while reading data on the public website should always be done in the “Published” scope. 
    /// Correct setting of the PublicationScope is typically handled by Composite C1 and should in general not be changed by developers. 
    /// Setting an explicit PublicationScope is typically only needed on new service end-points or 
    /// if specific features relating to data updating / publication is desired.
    /// See <see cref="Composite.Data.DataConnection"/>    
    /// </summary>
    ///// <seealso cref="Composite.Data.PageDataConnection"/>
    public enum PublicationScope
    {
        /// <summary>
        /// Only show data that has been published.
        /// </summary>
        Published = 1,

        /// <summary>
        /// Show / update unpublished data.
        /// </summary>
        Unpublished = 0
    }
}
