/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;

namespace Composite.Data
{
    /// <summary>
    /// Add this attribute to define an additional index to a tables representing the data type.
    /// </summary>
    /// <example> This sample shows how to use the KeyPropertyName attribute:
    /// <code>
    /// [KeyPropertyName("Id")] 
    /// [Index("FolderName", IndexDirection.Ascending, "FileName", IndexDirection.Descending)]
    /// interface IMyDataType : IData
    /// {
    ///     Guid Id { get; set; }
    ///     string FolderName { get; set; }
    ///     string FileName { get; set; }
    ///     
    ///     // other data type properties
    /// }
    /// </code>
    /// </example>  
    [AttributeUsage(AttributeTargets.Interface, AllowMultiple = true, Inherited = true)]
    public sealed class IndexAttribute: Attribute
    {
        private readonly IReadOnlyCollection<Tuple<string, IndexDirection>> _fields;

        /// <exclude />
        public IndexAttribute(string fieldName, IndexDirection indexDirection)
        {
            _fields = new List<Tuple<string, IndexDirection>>
            {
                new Tuple<string, IndexDirection>(fieldName, indexDirection)
            };
        }

        /// <exclude />
        public IndexAttribute(string field1Name, IndexDirection indexDirection1,
                              string field2Name, IndexDirection indexDirection2)
        {
            _fields = new List<Tuple<string, IndexDirection>>
            {
                new Tuple<string, IndexDirection>(field1Name, indexDirection1),
                new Tuple<string, IndexDirection>(field2Name, indexDirection2)
            };
        }

        /// <exclude />
        public IndexAttribute(string field1Name, IndexDirection indexDirection1,
                              string field2Name, IndexDirection indexDirection2,
                              string field3Name, IndexDirection indexDirection3)
        {
            _fields = new List<Tuple<string, IndexDirection>>
            {
                new Tuple<string, IndexDirection>(field1Name, indexDirection1),
                new Tuple<string, IndexDirection>(field2Name, indexDirection2),
                new Tuple<string, IndexDirection>(field3Name, indexDirection3)
            };
        }

        /// <exclude />
        public IndexAttribute(string field1Name, IndexDirection indexDirection1,
                              string field2Name, IndexDirection indexDirection2,
                              string field3Name, IndexDirection indexDirection3,
                              string field4Name, IndexDirection indexDirection4)
        {
            _fields = new List<Tuple<string, IndexDirection>>
            {
                new Tuple<string, IndexDirection>(field1Name, indexDirection1),
                new Tuple<string, IndexDirection>(field2Name, indexDirection2),
                new Tuple<string, IndexDirection>(field3Name, indexDirection3),
                new Tuple<string, IndexDirection>(field4Name, indexDirection4)
            };
        }

        /// <exclude />
        public IndexAttribute(string field1Name, IndexDirection indexDirection1,
                              string field2Name, IndexDirection indexDirection2,
                              string field3Name, IndexDirection indexDirection3,
                              string field4Name, IndexDirection indexDirection4,
                              string field5Name, IndexDirection indexDirection5)
        {
            _fields = new List<Tuple<string, IndexDirection>>
            {
                new Tuple<string, IndexDirection>(field1Name, indexDirection1),
                new Tuple<string, IndexDirection>(field2Name, indexDirection2),
                new Tuple<string, IndexDirection>(field3Name, indexDirection3),
                new Tuple<string, IndexDirection>(field4Name, indexDirection4),
                new Tuple<string, IndexDirection>(field5Name, indexDirection5)
            };
        }

        /// <summary>
        /// Gets the list of fields
        /// </summary>
        public IReadOnlyCollection<Tuple<string, IndexDirection>> Fields
        {
            get { return _fields; }
        }

        /// <summary>
        /// Defines whether current index is clustered. Only one index per data type can be choosen as clustered.
        /// </summary>
        public bool Clustered { get; set; }
    }
}
