/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;


namespace Composite.Data.Streams
{
    /// <summary>
    /// Expected on <see cref="Composite.Data.Types.IFile"/> classes to identify what <see cref="IFileStreamManager"/> can provide read/write and monitoring access to files.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    public class FileStreamManagerAttribute : Attribute
    {
        private Type _fileStreamManagerType;


        /// <summary>
        /// Identify the <see cref="IFileStreamManager"/> that provide read/write and monitoring access to the file represented by the <see cref="Composite.Data.Types.IFile"/> this 
        /// <see cref="FileStreamManagerAttribute"/> is attached to.
        /// </summary>
        /// <param name="fileStreamManagerType">the type of the class implementing <see cref="IFileStreamManager"/> for this file.</param>
        public FileStreamManagerAttribute(Type fileStreamManagerType)
        {
            _fileStreamManagerType = fileStreamManagerType;
        }


        internal Type FileStreamManagerResolverType
        {
            get { return _fileStreamManagerType; }
        }
    }
}