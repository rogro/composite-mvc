/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;
using Composite.Core.Collections.Generic;
using Composite.C1Console.Events;
using Composite.Core.Logging;


namespace Composite.Data.Streams
{
    internal static class FileStreamManagerLocator
    {
        private static ResourceLocker<Resources> _resourceLocker = new ResourceLocker<Resources>(new Resources(), Resources.Initialize);



        static FileStreamManagerLocator()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlushEvent);
        }



        public static IFileStreamManager GetFileStreamManager(Type fileType)
        {
            if (fileType == null) throw new ArgumentNullException("fileType");

            IFileStreamManager fileStreamManager;

            if (_resourceLocker.Resources.FileStreamManagerCache.TryGetValue(fileType, out fileStreamManager) == false)
            {
                using (_resourceLocker.ReadLocker)
                {
                    if (_resourceLocker.Resources.FileStreamManagerCache.TryGetValue(fileType, out fileStreamManager) == false)
                    {
                        object[] attributes = fileType.GetCustomAttributes(typeof (FileStreamManagerAttribute), true);

                        if (attributes.Length == 0) throw new InvalidOperationException(string.Format("The type '{0}' is missing the attribute '{1}'", fileType, typeof (FileStreamManagerAttribute)));
                        FileStreamManagerAttribute fileStreamManagerResolverAttribute = (FileStreamManagerAttribute) attributes[0];

                        Type fileStreamManagerType = fileStreamManagerResolverAttribute.FileStreamManagerResolverType;

                        if (fileStreamManagerType == null) throw new InvalidOperationException(string.Format("The constructor argument of '{0}' may not be null", typeof (FileStreamManagerAttribute)));
                        if (typeof (IFileStreamManager).IsAssignableFrom(fileStreamManagerType) == false) throw new InvalidOperationException( string.Format("The type '{0}' does not implement the interface '{1}'", fileStreamManagerType, typeof (IFileStreamManager)));

                        fileStreamManager = (IFileStreamManager) Activator.CreateInstance(fileStreamManagerType);

                        _resourceLocker.Resources.FileStreamManagerCache.Add(fileType, fileStreamManager);
                    }
                }
            }

            return fileStreamManager;
        }



        private static void Flush()
        {
            _resourceLocker.ResetInitialization();
        }



        private static void OnFlushEvent(FlushEventArgs args)
        {
            Flush();
        }



        private sealed class Resources
        {
            public Hashtable<Type, IFileStreamManager> FileStreamManagerCache { get; set; }

            public static void Initialize(Resources resources)
            {
                resources.FileStreamManagerCache = new Hashtable<Type, IFileStreamManager>();
            }
        }
    }
}