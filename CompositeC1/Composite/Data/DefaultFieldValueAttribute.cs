/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.Data.DynamicTypes;


namespace Composite.Data
{
    /// <summary>    
    /// This abstract class is used by data providers when a new column is added to a table. Extend this class to create your own.
    /// </summary>
    public abstract class DefaultFieldValueAttribute : Attribute
	{
        internal DefaultFieldValueAttribute() { }

        /// <exclude />
        public abstract DefaultValue GetDefaultValue();        
	}



    /// <summary>    
    /// Associate a static default value to a string property on a data type.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public sealed class DefaultFieldStringValueAttribute : DefaultFieldValueAttribute
    {
        private string _defaultValue;


        /// <summary>    
        /// Associate a static default value to a string property on a data type.
        /// </summary>
        public DefaultFieldStringValueAttribute(string defaultValue)
        {
            if (defaultValue == null) throw new ArgumentNullException("defaultValue");
            _defaultValue = defaultValue;
        }


        /// <exclude />
        public override DefaultValue GetDefaultValue()
        {
            return DefaultValue.String(_defaultValue);
        }
    }



    /// <summary>    
    /// Associate a static default value to an integer property on a data type.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public sealed class DefaultFieldIntValueAttribute : DefaultFieldValueAttribute
    {
        private int _defaultValue;


        /// <summary>    
        /// Associate a static default value to an integer property on a data type.
        /// </summary>
        public DefaultFieldIntValueAttribute(int defaultValue)
        {
            _defaultValue = defaultValue;
        }


        /// <exclude />
        public override DefaultValue GetDefaultValue()
        {
            return DefaultValue.Integer(_defaultValue);
        }
    }



    /// <summary>    
    /// Associate a static default value to a decimal property on a data type.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public sealed class DefaultFieldDecimalValueAttribute : DefaultFieldValueAttribute
    {
        private decimal _defaultValue;

        /// <summary>
        /// Decimals are not allowed as attribute parameter values. They are structs.
        /// Use int value.
        /// </summary>
        /// <param name="defaultValue"></param>
        public DefaultFieldDecimalValueAttribute(int defaultValue)
        {
            _defaultValue = defaultValue;
        }


        /// <exclude />
        public override DefaultValue GetDefaultValue()
        {
            return DefaultValue.Decimal(_defaultValue);
        }
    }



    /// <summary>    
    /// Associate a static default value to a boolean property on a data type.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public sealed class DefaultFieldBoolValueAttribute : DefaultFieldValueAttribute
    {
        private bool _defaultValue;


        /// <summary>    
        /// Associate a static default value to a boolean property on a data type.
        /// </summary>
        public DefaultFieldBoolValueAttribute(bool defaultValue)
        {
            _defaultValue = defaultValue;
        }


        /// <exclude />
        public override DefaultValue GetDefaultValue()
        {
            return DefaultValue.Boolean(_defaultValue);
        }
    }



    /// <summary>    
    /// Associate a static default value to a GUID property on a data type.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public sealed class DefaultFieldGuidValueAttribute : DefaultFieldValueAttribute
    {
        private Guid _defaultValue;


        /// <summary>    
        /// Associate a static default value to a GUID property on a data type.
        /// </summary>
        public DefaultFieldGuidValueAttribute(string guidString)
        {
            _defaultValue = new Guid(guidString);
        }


        /// <exclude />
        public override DefaultValue GetDefaultValue()
        {
            return DefaultValue.Guid(_defaultValue);
        }
    }



    /// <summary>    
    /// Associate a random new GUID to a GUID property on a data type.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public sealed class DefaultFieldNewGuidValueAttribute : DefaultFieldValueAttribute
    {
        /// <summary>    
        /// Associate a random new GUID to a GUID property on a data type.
        /// </summary>
        public DefaultFieldNewGuidValueAttribute()
        {
        }


        /// <exclude />
        public override DefaultValue GetDefaultValue()
        {
            return DefaultValue.NewGuid;
        }
    }



    /// <summary>    
    /// Associate the current date and time to a DateTime property on a data type.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public sealed class DefaultFieldNowDateTimeValueAttribute : DefaultFieldValueAttribute
    {
        /// <summary>
        /// Associate the current date and time to a DateTime property on a data type.
        /// </summary>
        public DefaultFieldNowDateTimeValueAttribute()
        {
        }


        /// <exclude />
        public override DefaultValue GetDefaultValue()
        {
            return DefaultValue.Now;
        }
    }
}
