/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Composite.Core.Serialization;
using Composite.Core.Types;


namespace Composite.Data
{
	internal static class DataIdSerializer
	{
        public static string Serialize(this IDataId dataId)
        {
            if (dataId == null) throw new ArgumentNullException("dataId");

            StringBuilder sb = new StringBuilder();
            StringConversionServices.SerializeKeyValuePair(sb, "_dataIdType_", TypeManager.SerializeType(dataId.GetType()));
            StringConversionServices.SerializeKeyValuePair(sb, "_dataId_", SerializationFacade.Serialize(dataId));

            return sb.ToString();
        }



        public static IDataId Deserialize(string serializedDataId)
        {
            Dictionary<string, string> dic = StringConversionServices.ParseKeyValueCollection(serializedDataId);

            if ((dic.ContainsKey("_dataIdType_") == false) ||
                (dic.ContainsKey("_dataId_") == false))
            {
                throw new ArgumentException("The serializedDataId is not a serialized data id", "serializedDataId");
            }

            string dataIdType = StringConversionServices.DeserializeValueString(dic["_dataIdType_"]);
            string serializedDataIdString = StringConversionServices.DeserializeValueString(dic["_dataId_"]);            

            Type type = TypeManager.TryGetType(dataIdType);
            if (type == null)
            {
                throw new InvalidOperationException(string.Format("The type {0} could not be found", dataIdType));
            }

            IDataId dataId = SerializationFacade.Deserialize<IDataId>(type, serializedDataIdString);

            return dataId;
        }
	}
}
