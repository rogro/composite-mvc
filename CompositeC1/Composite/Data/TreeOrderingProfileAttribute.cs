/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;


namespace Composite.Data
{
    /// <summary>
    /// Assign this to properties on your IData interfaces to control default ordering of tree items.
    /// </summary>
    /// <example> This sample shows how to use the TreeOrdering attribute.
    /// <code>
    /// // data interface attributes ...
    /// interface IMyDataType : IData
    /// {
    ///     [TreeOrdering(1)]
    ///     [StoreFieldType(PhysicalStoreFieldType.String, 40)]
    ///     [ImmutableFieldId("{D75EA67F-AD14-4BAB-8547-6D87002809F1}")]
    ///     string ProductName { get; set; }
    ///     
    ///     // more data properties ...
    ///     
    /// }
    /// </code>
    /// </example>    
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
	public sealed class TreeOrderingAttribute : Attribute
	{
        /// <summary>
        /// Specify that this field should be used for default tree node ordering. Order is by default ascending.
        /// Multiple fields on a data type may have ordering. In that case the orderPriority has importance.
        /// </summary>
        /// <param name="orderPriority">Priority controls which fields are used first when ordering. Low number win.</param>
        public TreeOrderingAttribute(int orderPriority)
        {
            this.Priority = orderPriority;
        }

        /// <summary>
        /// Specify that this field should be used for default tree node ordering. Order is by default ascending.
        /// Multiple fields on a data type may have ordering. In that case the orderPriority has importance.
        /// </summary>
        /// <param name="orderPriority">Priority controls which fields are used first when ordering. Low number win.</param>
        /// <param name="orderDescending">When true this field will be used in descending (Z-A) order.</param>
        public TreeOrderingAttribute(int orderPriority, bool orderDescending)
        {
            this.Priority = orderPriority;
            this.Descending = orderDescending;
        }


        /// <summary>
        /// Priority for ordering. When multiple fields have this attribute attached this field is used. Low number win.
        /// </summary>
        public int Priority { get; set; }

        /// <summary>
        /// When true descending order (Z-A) will be used.
        /// </summary>
        public bool Descending { get; set; }
    }
}
