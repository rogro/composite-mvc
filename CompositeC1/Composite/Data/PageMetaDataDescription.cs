/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Text;
using Composite.Core.Serialization;


namespace Composite.Data
{
    internal sealed class PageMetaDataDescriptionSerializerHandler : ISerializerHandler
    {
        public string Serialize(object objectToSerialize)
        {
            PageMetaDataDescription dataAssociationVisabilityRule = (PageMetaDataDescription)objectToSerialize;

            return dataAssociationVisabilityRule.Serialize();
        }

        public object Deserialize(string serializedObject)
        {
            return PageMetaDataDescription.Deserialize(serializedObject);
        }
    }



    /// <summary>
    /// This class is used when adding a new page metadata type to a given page. In other words, in workflow only.
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [SerializerHandler(typeof(PageMetaDataDescriptionSerializerHandler))]
    public sealed class PageMetaDataDescription
    {
        /// <exclude />
        public static PageMetaDataDescription OneToOne() { return new PageMetaDataDescription(PageMetaDataDescriptionType.OneToOne); }

        /// <exclude />
        public static PageMetaDataDescription Branch() { return new PageMetaDataDescription(PageMetaDataDescriptionType.Branch, 0, 100000); }

        /// <exclude />
        public static PageMetaDataDescription Branch(int startLevel) { return new PageMetaDataDescription(PageMetaDataDescriptionType.Branch, startLevel, int.MaxValue); }

        /// <exclude />
        public static PageMetaDataDescription Branch(int startLevel, int levels) { return new PageMetaDataDescription(PageMetaDataDescriptionType.Branch, startLevel, levels); }



        internal PageMetaDataDescription(PageMetaDataDescriptionType dataAssociationVisabilityRuleType)
            : this(dataAssociationVisabilityRuleType, 0, 0)
        {
        }
        
       

        internal PageMetaDataDescription(PageMetaDataDescriptionType dataAssociationVisabilityRuleType, int startLevel, int levels)
        {
            this.PageMetaDataDescriptionType = dataAssociationVisabilityRuleType;
            this.StartLevel = startLevel;
            this.Levels = levels;            
        }


        /// <exclude />
        public PageMetaDataDescriptionType PageMetaDataDescriptionType
        {
            get;
            private set;
        }


        /// <exclude />
        public int StartLevel
        {
            get;
            private set;
        }


        /// <exclude />
        public int Levels
        {
            get;
            private set;
        }


        /// <exclude />
        public string Serialize()
        {
            StringBuilder sb = new StringBuilder();
            StringConversionServices.SerializeKeyValuePair(sb, "_PageMetaDataDescriptionType_", this.PageMetaDataDescriptionType.ToString());
            StringConversionServices.SerializeKeyValuePair(sb, "_StartLevel_", this.StartLevel.ToString());
            StringConversionServices.SerializeKeyValuePair(sb, "_Levels_", this.Levels.ToString());           

            return sb.ToString();
        }



        internal static PageMetaDataDescription Deserialize(string serializedData)
        {
            // DataAssociationVisabilityRuleType is here for backwards compatibility - after 1.3 its not used any more
            Dictionary<string, string> dic = StringConversionServices.ParseKeyValueCollection(serializedData);
            if (((dic.ContainsKey("DataAssociationVisabilityRuleType") == false) && (dic.ContainsKey("_PageMetaDataDescriptionType_") == false)) ||
                (dic.ContainsKey("_StartLevel_") == false) ||
                (dic.ContainsKey("_Levels_") == false))
            {
                throw new ArgumentException(string.Format("The serializedData is not a serialized '{0}'", typeof(PageMetaDataDescription)), "serializedData");
            }

            string serializedDataAssociationVisabilityRuleType;
            if (dic.ContainsKey("_PageMetaDataDescriptionType_"))
            {
                serializedDataAssociationVisabilityRuleType = StringConversionServices.DeserializeValueString(dic["_PageMetaDataDescriptionType_"]);
            }
            else
            {
                serializedDataAssociationVisabilityRuleType = StringConversionServices.DeserializeValueString(dic["DataAssociationVisabilityRuleType"]);
            }

            PageMetaDataDescriptionType type = (PageMetaDataDescriptionType)Enum.Parse(typeof(PageMetaDataDescriptionType), serializedDataAssociationVisabilityRuleType);            

            string serializedStartLevel = StringConversionServices.DeserializeValueString(dic["_StartLevel_"]);
            string serializedLevels = StringConversionServices.DeserializeValueString(dic["_Levels_"]);
            
            int startLevel = int.Parse(serializedStartLevel);
            int levels = int.Parse(serializedLevels);

            return new PageMetaDataDescription(type, startLevel, levels);
        }



        /// <exclude />
        public override bool Equals(object obj)
        {
            return Equals(obj as PageMetaDataDescription);
        }



        /// <exclude />
        public bool Equals(PageMetaDataDescription dataAssociationVisabilityRule)
        {
            if (dataAssociationVisabilityRule == null) return false;

            return
                this.PageMetaDataDescriptionType == dataAssociationVisabilityRule.PageMetaDataDescriptionType &&
                this.StartLevel == dataAssociationVisabilityRule.StartLevel &&
                this.Levels == dataAssociationVisabilityRule.Levels;
        }



        /// <exclude />
        public override int GetHashCode()
        {
            return
                this.PageMetaDataDescriptionType.GetHashCode() ^
                this.StartLevel.GetHashCode() ^
                this.Levels.GetHashCode();
        }
    }



    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public enum PageMetaDataDescriptionType
    {
        /// <exclude />
        OneToOne = 0,

        /// <exclude />
        Branch = 1,

        /// <exclude />
        PageType = 2
    }
}
