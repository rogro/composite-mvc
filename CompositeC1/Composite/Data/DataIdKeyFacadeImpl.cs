/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Concurrent;
using System.Reflection;


namespace Composite.Data
{
    internal class DataIdKeyFacadeImpl : IDataIdKeyFacade
    {
        private ConcurrentDictionary<Type, string> _defaultKeyNameCache = new ConcurrentDictionary<Type, string>();
        private ConcurrentDictionary<Type, PropertyInfo> _keyPropertyInfoCache = new ConcurrentDictionary<Type, PropertyInfo>();



        public object GetKeyValue(IDataId dataId, string keyName)
        {
            if (keyName == null)
            {
                keyName = this.GetDefaultKeyName(dataId.GetType());
                if (keyName == null) throw new InvalidOperationException("Could not find default key for the type: " + dataId.GetType());
            }


            Func<Type, PropertyInfo> valueFactory = f =>
            {
                return f.GetProperty(keyName);
            };

            PropertyInfo keyPropertyInfo = _keyPropertyInfoCache.GetOrAdd(dataId.GetType(), valueFactory);

            object keyValue = keyPropertyInfo.GetValue(dataId, null);

            return keyValue;
        }



        public string GetDefaultKeyName(Type dataIdType)
        {
            Func<Type, string> valueFactory = f =>
            {
                PropertyInfo[] propertyInfoes = f.GetProperties();

                if (propertyInfoes.Length != 1) return null;

                return propertyInfoes[0].Name;
            };

            string defaultKeyName = _defaultKeyNameCache.GetOrAdd(dataIdType, valueFactory);

            return defaultKeyName;
        }



        public void OnFlush()
        {
            _defaultKeyNameCache = new ConcurrentDictionary<Type, string>();
            _keyPropertyInfoCache = new ConcurrentDictionary<Type, PropertyInfo>();
        }
    }
}
