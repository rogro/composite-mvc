/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;


namespace Composite.Data
{
    /// <summary>
    /// Assigns an immutable id to this type. The id must be unique and is used to identify the type even if 
    /// the type name, namespace or version should change. The Dynamic Type system uses this value to detect data schema changes.
    /// </summary>
    /// <example> This sample shows how to use the ImmutableTypeId attribute:
    /// <code>
    /// [ImmutableTypeId("b3bada55-0e7e-4195-86e6-92770c381df3")]
    /// // (other IData attributes)
    /// interface IMyDataType : IData
    /// {
    ///     // data type properties
    /// }
    /// </code>
    /// </example>    
    [AttributeUsage(AttributeTargets.Interface, AllowMultiple = false, Inherited = true)]
    public class ImmutableTypeIdAttribute : Attribute
    {
        /// <summary>
        /// Specify the immutable id of this data type. This value must be unique among types and should not change.
        /// </summary>
        /// <param name="immutableTypeId">Unique GUID string</param>
        public ImmutableTypeIdAttribute(string immutableTypeId)
        {
            this.ImmutableTypeId = new Guid(immutableTypeId);
        }

        /// <exclude />
        public Guid ImmutableTypeId { get; set; }
    }
}
