/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Reflection;
using System;


namespace Composite.Data
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class IDataIdExtensions
    {
        /// <exclude />
        public static void FullCopyTo(this IDataId sourceDataId, IDataId targetDataId)
        {
            if (sourceDataId == null) throw new ArgumentNullException("sourceDataId");
            if (targetDataId == null) throw new ArgumentNullException("targetDataId");

            foreach (PropertyInfo sourcePropertyInfo in sourceDataId.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                PropertyInfo targetPropertyInfo = targetDataId.GetType().GetProperty(sourcePropertyInfo.Name, BindingFlags.Public | BindingFlags.Instance);

                object value = sourcePropertyInfo.GetValue(sourceDataId, null);

                targetPropertyInfo.SetValue(targetDataId, value, null);
            }
        }



        /// <exclude />
        public static bool CompareTo(this IDataId sourceDataId, IDataId targetDataId)
        {
            return CompareTo(sourceDataId, targetDataId, false);
        }


        /// <exclude />
        public static bool CompareTo(this IDataId sourceDataId, IDataId targetDataId, bool throwExceptionOnTypeMismathc)
        {
            if (sourceDataId == null) throw new ArgumentNullException("sourceDataId");
            if (targetDataId == null) throw new ArgumentNullException("targetDataId");


            if (sourceDataId.GetType() != targetDataId.GetType())
            {
                if (throwExceptionOnTypeMismathc) throw new ArgumentException(string.Format("Type mismatch {0} and {1}", sourceDataId.GetType(), targetDataId.GetType()));
                return false;
            }

            bool equal = true;
            foreach (PropertyInfo sourcePropertyInfo in sourceDataId.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                PropertyInfo targetPropertyInfo = targetDataId.GetType().GetProperty(sourcePropertyInfo.Name, BindingFlags.Public | BindingFlags.Instance);

                object sourceValue = sourcePropertyInfo.GetValue(sourceDataId, null);
                object targetValue = targetPropertyInfo.GetValue(targetDataId, null);

                if (object.Equals(sourceValue, targetValue) == false)
                {
                    equal = false;
                    break;
                }
            }

            return equal;
        }
    }
}
