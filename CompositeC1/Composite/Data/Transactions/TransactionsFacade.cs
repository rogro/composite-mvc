/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Transactions;
using System;

namespace Composite.Data.Transactions
{
    /// <summary>
    /// Ensures C1 compiant System.Transactions.TransactionScope services.
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
	public static class TransactionsFacade
	{
        /// <exclude />
        public static TimeSpan DefaultTransactionTimeout { get { return TimeSpan.FromMinutes(3); } }

        /// <exclude />
        public static IsolationLevel DefaultTransactionIsolationLevel { get { return IsolationLevel.RepeatableRead; } }


        /// <exclude />
        public static TransactionScope CreateNewScope()
        {
            return Create(false, DefaultTransactionTimeout);
        }


        /// <exclude />
        public static TransactionScope Create(bool requiresNew)
        {
            return Create(requiresNew, DefaultTransactionTimeout);
        }


        /// <exclude />
        public static TransactionScope Create(bool requiresNew, TimeSpan timeSpan)
        {
            IsolationLevel isolationLevel = (Transaction.Current != null ? Transaction.Current.IsolationLevel : DefaultTransactionIsolationLevel);
            
            var transOptions = new TransactionOptions
            {
                IsolationLevel = isolationLevel,
                Timeout = timeSpan
            };


            return new TransactionScope(requiresNew ? TransactionScopeOption.RequiresNew : TransactionScopeOption.Required, transOptions);
        }



        /// <exclude />
        public static TransactionScope CreateNewScope(TimeSpan timeSpan)
        {
            return Create(false, timeSpan);
        }



        /// <exclude />
        public static TransactionScope SuppressTransactionScope()
        {
            return new TransactionScope(TransactionScopeOption.Suppress);
        }
    }
}
