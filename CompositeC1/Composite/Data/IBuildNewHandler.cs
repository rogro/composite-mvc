/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.CodeDom;


namespace Composite.Data
{
    /// <summary>
    /// This interface is used togehter with the attribute <see cref="BuildNewHandlerAttribute"/>.
    /// It is possible to overwrite the default behavior when a new data item is created through the method <see cref="DataConnection.New"/>
    /// To do this, you have to implement this interface and attach it to your <see cref="IData"/> type by using the attribute <see cref="BuildNewHandlerAttribute"/>
    /// <example>
    /// <code>
    /// [BuildNewHandlerAttribute(typeof(MyBuildNewHandler))
    /// [AutoUpdateble]
    /// [KeyPropertyName("Id")]
    /// [DataAncestorProvider(typeof(NoAncestorDataAncestorProvider))]
    /// [ImmutableTypeId("{10D6CA29-5B01-45EE-9405-9B027F4C949C}")]    
    /// interface IMyDataType : IData
    /// {
    ///     [StoreFieldType(PhysicalStoreFieldType.Guid)]
    ///     [ImmutableFieldId("{B99F4AF2-859D-4235-887B-E5A06BBB9892}")]
    ///     Guid Id { get; set; }
    ///         
    ///     [StoreFieldType(PhysicalStoreFieldType.String, 256)]
    ///     [ImmutableFieldId("{A8127C77-5083-4409-9EA6-1E3BB696310D}")]
    ///     string Name { get; set; }
    /// }
    /// 
    /// class MyBuildNewHandler : IBuildNewHandler
    /// {
    ///     public Type GetTypeToBuild(Type dataType)
    ///     {
    ///         /* dataType will always be typeof(IMyDataType) */
    ///         
    ///         return typeof(MyDataType);
    ///     }
    /// }
    /// 
    /// 
    /// class MyDataType : IMyDataType
    /// {
    ///     puglic MyDataType()
    ///     {
    ///         /* All new instances of IMyDataType will becrated through this constructor */
    ///         this.Id = Guid.NewGuid();
    ///         this.Name = "RandomName";
    ///     }
    ///     
    ///     public Id { get; set; }
    ///     
    ///     public Name { get; set; }
    /// }
    /// </code>
    /// </example>
    /// </summary>
    public interface IBuildNewHandler
    {
        /// <summary>
        /// The method should return a type with parameterless constructor that will be used to create a new <see cref="IData"/> instance.
        /// The returned type is used by C1 to construct a new object when <see cref="DataConnection.New"/> is called.
        /// </summary>
        /// <param name="dataType">
        /// The data interface type in question. This interface type is inheriting <see cref="IData"/>.
        /// And the interface type is also decorated with the attribute <see cref="BuildNewHandlerAttribute"/>.
        /// </param>
        /// <returns>Should return a type that will be used to create an object that implements the given <paramref name="dataType"/> interface.</returns>
        Type GetTypeToBuild(Type dataType);
    }
}
