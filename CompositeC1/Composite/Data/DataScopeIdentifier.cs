/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Diagnostics;


namespace Composite.Data
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [DebuggerDisplay("Name = {Name}")]
    public sealed class DataScopeIdentifier
    {
        /// <exclude />
        public const string PublicName = "public";

        /// <exclude />
        public const string AdministratedName = "administrated";

        /// <exclude />
        public static DataScopeIdentifier Public { get { return new DataScopeIdentifier(PublicName); } }

        /// <exclude />
        public static DataScopeIdentifier Administrated { get { return new DataScopeIdentifier(AdministratedName); } }

        /// <exclude />
        public static DataScopeIdentifier GetDefault()
        {
            return DataScopeIdentifier.Public;
        }


        private DataScopeIdentifier(string dataScope)
        {
            this.Name = dataScope;
        }


        /// <exclude />
        public string Name
        {
            get;
            private set;
        }


        /// <exclude />
        public string Serialize()
        {
            return this.Name;
        }


        /// <exclude />
        public static DataScopeIdentifier Deserialize(string serializedData)
        {
            if (serializedData == null) throw new ArgumentNullException("serializedData");

            switch (serializedData)
            {
                case "public":
                    return DataScopeIdentifier.Public;

                case "administrated":
                    return DataScopeIdentifier.Administrated;

                default:
                    throw new InvalidOperationException("The serializedData argument was not a serialized DataScope");
            }
        }


        /// <exclude />
        public PublicationScope ToPublicationScope()
        {
            return Name == "public" ? PublicationScope.Published : PublicationScope.Unpublished;
        }


        /// <exclude />
        public static DataScopeIdentifier FromPublicationScope(PublicationScope publicationScope)
        {
            return publicationScope == PublicationScope.Published ? Public : Administrated;
        }


        /// <exclude />
        public static bool IsLegasyDataScope(string name)
        {
            name = name.ToLowerInvariant();
            return name == "deleted" || name == "versioned";
        }


        /// <exclude />
        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            return Equals(obj as DataScopeIdentifier);
        }


        /// <exclude />
        public bool Equals(DataScopeIdentifier dataScope)
        {
            return !ReferenceEquals(dataScope, null) && this.Name == dataScope.Name;
        }

        /// <exclude />
        public static bool operator==(DataScopeIdentifier a, DataScopeIdentifier b)
        {
            return (object)a == null ? (object)b == null : a.Equals(b);
        }

        /// <exclude />
        public static bool operator!=(DataScopeIdentifier a, DataScopeIdentifier b)
        {
            return !(a == b);
        }
        

        /// <exclude />
        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }


        /// <exclude />
        public override string ToString()
        {
            return this.Serialize();
        }       
    }
}
