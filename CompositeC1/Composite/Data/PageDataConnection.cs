/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Globalization;
using System.Linq;
using Composite.Core.Implementation;


namespace Composite.Data
{
    ///// <summary>    
    ///// </summary>
    ///// <exclude />
    //[System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    //public class PageDataConnection : ImplementationContainer<PageDataConnectionImplementation>, IDisposable
    //{
    //    ImplementationContainer<DataConnection> _dataConnection;
    //    ImplementationContainer<SitemapNavigator> _sitemapNavigator;



    //    public PageDataConnection()
    //        : base(() => ImplementationFactory.CurrentFactory.CreatePageDataConnection(null, null))
    //    {
    //        _dataConnection = new ImplementationContainer<DataConnection>(() => new DataConnection());
    //        _sitemapNavigator = new ImplementationContainer<SitemapNavigator>(() => new SitemapNavigator(this.DataConnection));
    //    }



    //    public PageDataConnection(PublicationScope scope)
    //        : base(() => ImplementationFactory.CurrentFactory.CreatePageDataConnection(scope, null))
    //    {
    //        if ((scope < PublicationScope.Unpublished) || (scope > PublicationScope.Published)) throw new ArgumentOutOfRangeException("scope");

    //        _dataConnection = new ImplementationContainer<DataConnection>(() => new DataConnection(scope));
    //        _sitemapNavigator = new ImplementationContainer<SitemapNavigator>(() => new SitemapNavigator(this.DataConnection));
    //    }



    //    public PageDataConnection(CultureInfo locale)
    //        : base(() => ImplementationFactory.CurrentFactory.CreatePageDataConnection(null, locale))
    //    {
    //        _dataConnection = new ImplementationContainer<DataConnection>(() => new DataConnection(locale));
    //        _sitemapNavigator = new ImplementationContainer<SitemapNavigator>(() => new SitemapNavigator(this.DataConnection));
    //    }



    //    public PageDataConnection(PublicationScope scope, CultureInfo locale)
    //        : base(() => ImplementationFactory.CurrentFactory.CreatePageDataConnection(scope, locale))
    //    {
    //        if ((scope < PublicationScope.Unpublished) || (scope > PublicationScope.Published)) throw new ArgumentOutOfRangeException("scope");

    //        _dataConnection = new ImplementationContainer<DataConnection>(() => new DataConnection(scope, locale));
    //        _sitemapNavigator = new ImplementationContainer<SitemapNavigator>(() => new SitemapNavigator(this.DataConnection));
    //    }



    //    public TData GetPageMetaData<TData>(string fieldName)
    //        where TData : IPageMetaData
    //    {
    //        if (string.IsNullOrWhiteSpace(fieldName)) throw new ArgumentNullException("fieldName");

    //        return this.Implementation.GetPageMetaData<TData>(fieldName);
    //    }



    //    public TData GetPageMetaData<TData>(string fieldName, Guid pageId)
    //        where TData : IPageMetaData
    //    {
    //        if (string.IsNullOrWhiteSpace(fieldName)) throw new ArgumentNullException("fieldName");

    //        return this.Implementation.GetPageMetaData<TData>(fieldName, pageId);
    //    }



    //    public IQueryable<TData> GetPageMetaData<TData>(string fieldName, SitemapScope scope)
    //        where TData : IPageMetaData
    //    {
    //        if (string.IsNullOrWhiteSpace(fieldName)) throw new ArgumentNullException("fieldName");
    //        if ((scope < SitemapScope.Current) || (scope > SitemapScope.SiblingsAndSelf)) throw new ArgumentOutOfRangeException("scope");

    //        return this.Implementation.GetPageMetaData<TData>(fieldName, scope);
    //    }



    //    public IQueryable<TData> GetPageMetaData<TData>(string fieldName, SitemapScope scope, Guid pageId)
    //        where TData : IPageMetaData
    //    {
    //        if (string.IsNullOrWhiteSpace(fieldName)) throw new ArgumentNullException("fieldName");
    //        if ((scope < SitemapScope.Current) || (scope > SitemapScope.SiblingsAndSelf)) throw new ArgumentOutOfRangeException("scope");

    //        return this.Implementation.GetPageMetaData<TData>(fieldName, scope, pageId);
    //    }



    //    public IQueryable<TData> GetPageData<TData>()
    //        where TData : IPageData
    //    {
    //        return this.Implementation.GetPageData<TData>();
    //    }



    //    public IQueryable<TData> GetPageData<TData>(SitemapScope scope)
    //        where TData : IPageData
    //    {
    //        if ((scope < SitemapScope.Current) || (scope > SitemapScope.SiblingsAndSelf)) throw new ArgumentOutOfRangeException("scope");

    //        return this.Implementation.GetPageData<TData>(scope);
    //    }



    //    public IQueryable<TData> GetPageData<TData>(SitemapScope scope, Guid sourcePageId)
    //        where TData : IPageData
    //    {
    //        if ((scope < SitemapScope.Current) || (scope > SitemapScope.SiblingsAndSelf)) throw new ArgumentOutOfRangeException("scope");

    //        return this.Implementation.GetPageData<TData>(scope, sourcePageId);
    //    }



    //    public SitemapNavigator SitemapNavigator
    //    {
    //        get
    //        {
    //            return _sitemapNavigator.Implementation;
    //        }
    //    }



    //    public DataConnection DataConnection
    //    {
    //        get
    //        {
    //            return _dataConnection.Implementation;
    //        }
    //    }



    //    public void Dispose()
    //    {
    //        Dispose(true);
    //        GC.SuppressFinalize(this);
    //    }



    //    ~PageDataConnection()
    //    {
    //        Dispose(false);
    //    }



    //    protected virtual void Dispose(bool disposing)
    //    {
    //        if (disposing)
    //        {
    //            if (_dataConnection != null)
    //            {
    //                _dataConnection.DisposeImplementation();
    //                _sitemapNavigator.DisposeImplementation();
    //            }
    //        }
    //    }
    //}
}
