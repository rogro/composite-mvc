/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.Core.Extensions;


namespace Composite.Data
{
    /// <summary>
    /// This class contains information for data events. See also <see cref="Composite.Data.StoreEventArgs"/>.
    /// </summary>
    public class DataEventArgs : EventArgs
    {
        private readonly Type _dataType;
        private readonly IData _data;



        /// <summary>        
        /// </summary>
        /// <param name="dataType"></param>
        /// <param name="data"></param>
        /// <exclude />
        internal DataEventArgs(Type dataType, IData data)
        {
            _dataType = dataType;
            _data = data;
        }



        /// <summary>
        /// This is the data item that is the subject of the event fired.
        /// </summary>
        /// <example>
        /// <code>
        /// void MyMethod()
        /// {
        ///    DataEvents&lt;IMyDataType&gt;().OnBeforeAdd += new DataEventHandler(DataEvents_OnBeforeAdd);
        ///    
        ///    using (DataConnection connection = new DataConnection())
        ///    {
        ///       IMyDataType myDataType = DataConnection.New&lt;IMyDataType&gt;();
        ///       myDataType.Name = "Foo";
        ///       
        ///       connection.Add&lt;IMyDataType&gt;(myDataType); // This will fire the event!
        ///    }
        /// }
        /// 
        /// 
        /// void DataEvents_OnBeforeAdd(object sender, DataEventArgs dataEventArgs)
        /// {        
        ///    IData myData = dataEventArgs.Data; // This will be the myDataType instance just created
        /// }
        /// </code>
        /// </example>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1721:PropertyNamesShouldNotMatchGetMethods", Justification = "We had to be backwards compatible")]
        public IData Data
        {
            get { return _data; }
        }



        /// <summary>
        /// This is the type of the data item that is the subject of the event fired.
        /// </summary>
        /// <example>
        /// <code>
        /// void MyMethod()
        /// {
        ///    DataEvents&lt;IMyDataType&gt;().OnBeforeAdd += new DataEventHandler(DataEvents_OnBeforeAdd);
        ///    
        ///    using (DataConnection connection = new DataConnection())
        ///    {
        ///       IMyDataType myDataType = DataConnection.New&lt;IMyDataType&gt;();
        ///       myDataType.Name = "Foo";
        ///       
        ///       connection.Add&lt;IMyDataType&gt;(myDataType); // This will fire the event!
        ///    }
        /// }
        /// 
        /// 
        /// void DataEvents_OnBeforeAdd(object sender, DataEventArgs dataEventArgs)
        /// {        
        ///    Type type = dataEventArgs.DataType; // This will be the type of myDataType instance. E.i. IMyDataType
        /// }
        /// </code>
        /// </example>
        public Type DataType
        {
            get { return _dataType; }
        }




        /// <summary>
        /// This is the data item that is the subject of the event fired.
        /// </summary>
        /// <example>
        /// <code>
        /// void MyMethod()
        /// {
        ///    DataEvents&lt;IMyDataType&gt;().OnBeforeAdd += new DataEventHandler(DataEvents_OnBeforeAdd);
        ///    
        ///    using (DataConnection connection = new DataConnection())
        ///    {
        ///       IMyDataType myDataType = DataConnection.New&lt;IMyDataType&gt;();
        ///       myDataType.Name = "Foo";
        ///       
        ///       connection.Add&lt;IMyDataType&gt;(myDataType); // This will fire the event!
        ///    }
        /// }
        /// 
        /// 
        /// void DataEvents_OnBeforeAdd(DataEventArgs dataEventArgs)
        /// {        
        ///    IMyDataType myDataType = dataEventArgs.GetData&lt;IMyDataType&gt;(); // This will be the myDataType instance just created
        /// }
        /// </code>
        /// </example>
        /// <typeparam name="TData">An IData interface</typeparam>
        /// <returns>Returns a casted version of the data item that is the suvject of the event fired.</returns>
        public TData GetData<TData>()
            where TData : IData
        {
            if (_dataType.IsAssignableFrom(typeof(TData)) == false)
            {
                throw new ArgumentException("TData is of wrong type ('{0}'). Data type is '{1}'".FormatWith(typeof(TData), _dataType));
            }

            return (TData)_data;
        }
    }
}
