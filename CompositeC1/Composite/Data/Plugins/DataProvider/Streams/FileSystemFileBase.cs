/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using Composite.Core.Configuration;
using Composite.Core.Extensions;
using Composite.Core.IO;

namespace Composite.Data.Plugins.DataProvider.Streams
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    public class FileSystemFileBase
    {
        /// <exclude />
        [SuppressMessage("Composite.IO", "Composite.DoNotUseFileStreamClass:DoNotUseFileStreamClass")]
        protected FileStream TemporaryFileStream { get; set; }

        /// <exclude />
        protected string TemporaryFilePath { get; set; }

        /// <exclude />
        public virtual string SystemPath { get; set; }

        /// <exclude />
        [SuppressMessage("Composite.IO", "Composite.DoNotUseFileStreamClass:DoNotUseFileStreamClass")]
        [SuppressMessage("Composite.IO", "Composite.DoNotUseFileClass:DoNotUseFileClass")]
        public FileStream GetNewWriteStream()
        {
            if (TemporaryFileStream != null)
            {
                Verify.That(!TemporaryFileStream.CanWrite, "Stream for writing has not been closed.");

                TemporaryFileStream = null;
                File.Delete(TemporaryFilePath);
            }

            string tempFile = Path.Combine(PathUtil.Resolve(GlobalSettingsFacade.TempDirectory),  "upload" + Path.GetRandomFileName());

            this.TemporaryFilePath = tempFile;
            this.TemporaryFileStream = File.Open(tempFile, FileMode.Create, FileAccess.ReadWrite, FileShare.None);

            return TemporaryFileStream;
        }

        /// <exclude />
        [SuppressMessage("Composite.IO", "Composite.DoNotUseFileStreamClass:DoNotUseFileStreamClass")]
        public Stream GetReadStream()
        {
            string filePath;

            if (this.TemporaryFileStream != null)
            {
                Verify.That(!TemporaryFileStream.CanWrite, "Stream for writing has not been closed.");

                filePath = this.TemporaryFilePath;
            }
            else
            {
                filePath = this.SystemPath;
            }

            return new FileStream(filePath, FileMode.Open, FileAccess.Read);
        }

        /// <exclude />
        [SuppressMessage("Composite.IO", "Composite.DoNotUseFileClass:DoNotUseFileClass")]
        public void CommitChanges()
        {
            if (this.TemporaryFileStream == null) return;

            Verify.That(!TemporaryFileStream.CanWrite, "Stream for writing has not been closed.");

            DirectoryUtils.EnsurePath(this.SystemPath);

            if (!this.SystemPath.IsNullOrEmpty())
            {
                File.Delete(this.SystemPath);
            }

            File.Move(this.TemporaryFilePath, this.SystemPath);

            this.TemporaryFileStream = null;
            this.TemporaryFilePath = null;
        }

        /// <exclude />
        [SuppressMessage("Composite.IO", "Composite.DoNotUseFileClass:DoNotUseFileClass")]
        ~FileSystemFileBase()
        {
            if (TemporaryFilePath != null)
            {
                try
                {
                    File.Delete(TemporaryFilePath);
                }
                catch
                {
                }
            }
        }
    }
}
