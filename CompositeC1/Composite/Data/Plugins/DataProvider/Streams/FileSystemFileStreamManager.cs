/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.IO;
using Composite.Core.Extensions;
using Composite.Core.IO;
using Composite.Data.Streams;
using Composite.Data.Types;


namespace Composite.Data.Plugins.DataProvider.Streams
{
    internal sealed class FileSystemFileStreamManager : IFileStreamManager
    {
        public Stream GetReadStream(IFile file)
        {
            Verify.ArgumentNotNull(file, "file");
            Verify.ArgumentCondition(file is FileSystemFileBase, "file", "The type '{0}' does not inherit the class '{1}'".FormatWith(file.GetType(), typeof(FileSystemFileBase)));

            var baseFile = file as FileSystemFileBase;

            return baseFile.GetReadStream();
        }



        public Stream GetNewWriteStream(IFile file)
        {
            Verify.ArgumentNotNull(file, "file");
            Verify.ArgumentCondition(file is FileSystemFileBase, "file", "The type '{0}' does not inherit the class '{1}'".FormatWith(file.GetType(), typeof(FileSystemFileBase)));

            var baseFile = file as FileSystemFileBase;

            return baseFile.GetNewWriteStream();
        }



        internal static void DeleteFile(string filename)
        {
            Verify.ArgumentNotNullOrEmpty(filename, "filename");

            DirectoryUtils.DeleteFile(filename, true);
        }



        internal static void DeleteFile(IFile file)
        {
            Verify.ArgumentNotNull(file, "file");
            Verify.ArgumentCondition(file is FileSystemFileBase, "file", "The type '{0}' does not inherit the class '{1}'".FormatWith(file.GetType(), typeof(FileSystemFileBase)));

            var baseFile = file as FileSystemFileBase;

            DeleteFile(baseFile.SystemPath);
        }



        internal static void WriteFileToDisk(IFile file)
        {
            Verify.ArgumentNotNull(file, "file");
            Verify.ArgumentCondition(file is FileSystemFileBase, "file", "The type '{0}' does not inherit the class '{1}'".FormatWith(file.GetType(), typeof(FileSystemFileBase)));

            var baseFile = file as FileSystemFileBase;

            baseFile.CommitChanges();
        }

        public void SubscribeOnFileChanged(IFile file, OnFileChangedDelegate handler)
        {
            FileChangeNotificator.Subscribe(file as FileSystemFileBase, handler);
        }
    }
}
