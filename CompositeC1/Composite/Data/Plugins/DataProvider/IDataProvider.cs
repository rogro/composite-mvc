/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Composite.Data.Plugins.DataProvider.Runtime;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ObjectBuilder;


namespace Composite.Data.Plugins.DataProvider
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [CustomFactory(typeof(DataProviderCustomFactory))]
    [ConfigurationNameMapper(typeof(DataProviderDefaultNameRetriever))]
    public interface IDataProvider
    {
        /// <summary>
        /// This is set by the system and is used to create DataSourceId's
        /// </summary>
        DataProviderContext Context { set; }

        /// <summary>
        /// This method should return all supported data interfaces. 
        /// That is, all data interfaces that this provider is currently handling.
        /// </summary>
        /// <returns></returns>
        IEnumerable<Type> GetSupportedInterfaces();

        /// <exclude />
        IQueryable<T> GetData<T>() where T : class, IData;

        /// <summary>
        /// This method should return null if the given dataId does not correspond to an IData
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataId"></param>
        /// <returns></returns>
        T GetData<T>(IDataId dataId) where T : class, IData;
    }
}
