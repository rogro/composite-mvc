/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Text;
using System.Reflection;
using System.Collections.Generic;
using Composite.Core.Types;
using Composite.Core.ResourceSystem;


namespace Composite.Data.Plugins.DataProvider
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class DataInterfaceValidator
    {
        /// <exclude />
        public static bool TryValidate(Type interfaceType, out IEnumerable<string> errorMessages)
        {
            List<string> errors = new List<string>();
            errorMessages = errors;


            if (interfaceType.IsInterface == false)
            {
                string errorMessage = StringResourceSystemFacade.GetString("Composite.Management", "DataInterfaceValidator.TypeNotAnInterface");
                errors.Add(string.Format(errorMessage, interfaceType));
            }


            if (typeof(IData).IsAssignableFrom(interfaceType) == false)
            {
                string errorMessage = StringResourceSystemFacade.GetString("Composite.Management", "DataInterfaceValidator.TypeDoesNotImplementInterface");
                errors.Add(string.Format(errorMessage, interfaceType, typeof(IData)));
            }


            PropertyInfo[] properties = interfaceType.GetProperties();
            foreach (PropertyInfo propertyInfo in properties)
            {
                bool acceptedType = PrimitiveTypes.IsPrimitiveOrNullableType(propertyInfo.PropertyType);                

                if (acceptedType == false)
                {
                    string errorMessage = StringResourceSystemFacade.GetString("Composite.Management", "DataInterfaceValidator.NotAcceptedType");
                    errors.Add(string.Format(errorMessage, propertyInfo.PropertyType, interfaceType));
                }
            }

            return errors.Count == 0;
        }



        /// <exclude />
        public static bool TryValidate(Type interfaceType)
        {
            IEnumerable<string> errorMessages;

            return TryValidate(interfaceType, out errorMessages);
        }



        /// <exclude />
        public static bool TryValidate<T>(out IEnumerable<string> errorMessages)
            where T : class, IData
        {
            return TryValidate(typeof(T), out errorMessages);
        }



        /// <exclude />
        public static bool TryValidate<T>()
            where T : class, IData
        {
            IEnumerable<string> errors;

            return TryValidate(typeof(T), out errors);
        }



        /// <exclude />
        public static void Validate(Type interfaceType)
        {
            IEnumerable<string> errors;
            bool res = TryValidate(interfaceType, out errors);

            if (res == false)
            {
                StringBuilder sb = new StringBuilder();

                string errorMessage = StringResourceSystemFacade.GetString("Composite.Management", "DataInterfaceValidator.NotValidIDataInterface");
                sb.AppendLine(string.Format(errorMessage, interfaceType));

                foreach (string error in errors)
                {
                    sb.AppendLine(error);
                }

                throw new ArgumentException(sb.ToString());
            }
        }



        /// <exclude />
        public static void Validate<T>()
            where T : class, IData
        {
            Validate(typeof(T));
        }      
    }
}
