/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Globalization;
using Composite.Core.Extensions;
using Composite.Data.Foundation.CodeGeneration;


namespace Composite.Data.Plugins.DataProvider
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class DataProviderContext
    {
        private readonly string _providerName;

        /// <exclude />
        public DataProviderContext(string providerName)
        {
            Verify.ArgumentNotNullOrEmpty(providerName, "providerName");

            _providerName = providerName;
        }



        /// <exclude />
        public string ProviderName
        {
            get { return _providerName; }
        }



        /// <exclude />
        public DataSourceId CreateDataSourceId(IDataId dataId, Type interfaceType)
        {
            Verify.ArgumentNotNull(dataId, "dataId");
            Verify.ArgumentNotNull(interfaceType, "interfaceType");
            Verify.ArgumentCondition(typeof(IData).IsAssignableFrom(interfaceType), "interfaceType", "The interface type '{0}' does not inherit the interface '{1}'".FormatWith(interfaceType, typeof(IData)));

            return new DataSourceId(dataId, _providerName, interfaceType, DataScopeManager.MapByType(interfaceType), LocalizationScopeManager.MapByType(interfaceType));
        }

        /// <exclude />
        public DataSourceId CreateDataSourceId(IDataId dataId, Type interfaceType, DataScopeIdentifier dataScopeIdentifier, CultureInfo cultureInfo)
        {
            Verify.ArgumentNotNull(dataId, "dataId");
            Verify.ArgumentNotNull(interfaceType, "interfaceType");
            Verify.ArgumentNotNull(dataScopeIdentifier, "dataScopeIdentifier");
            Verify.ArgumentNotNull(cultureInfo, "cultureInfo");
            Verify.ArgumentCondition(typeof(IData).IsAssignableFrom(interfaceType), "interfaceType", "The interface type '{0}' does not inherit the interface '{1}'".FormatWith(interfaceType, typeof(IData)));

            return new DataSourceId(dataId, _providerName, interfaceType, dataScopeIdentifier, cultureInfo);
        }



        /// <exclude />
        public void UpdateDataSourceId(IData data, IDataId dataId, Type interfaceType)
        {
            Verify.ArgumentNotNull(data, "data");
            Verify.ArgumentNotNull(dataId, "dataId");
            Verify.ArgumentNotNull(interfaceType, "interfaceType");
            Verify.ArgumentCondition(typeof(IData).IsAssignableFrom(interfaceType), "interfaceType", "The interface type '{0}' does not inherit the interface '{1}'".FormatWith(interfaceType, typeof(IData)));

            var emptyDataClassBase = data as EmptyDataClassBase;
            if (emptyDataClassBase == null)
            {
                throw new InvalidOperationException("Updates on DataSourceIds can only be done on objects that are returned by the DataFacade.BuildNew<T>() method");
            }

            if (emptyDataClassBase.DataSourceId.ExistsInStore)
            {
                throw new InvalidOperationException("Updates on DataSourceIds can only be done once");
            }

            emptyDataClassBase.DataSourceId = CreateDataSourceId(dataId, interfaceType);
        }



        /// <exclude />
        public void UpdateDataSourceIdDataScope(IData data)
        {
            Verify.ArgumentNotNull(data, "data");

            data.DataSourceId.DataScopeIdentifier = DataScopeManager.MapByType(data);
        }
    }
}
