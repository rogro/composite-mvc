/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

#define USE_TEMPTYPECREATOR
using System;
using System.Collections.Generic;
using System.Linq;
using Composite.Data.DynamicTypes;
using Composite.Data.Foundation;


namespace Composite.Data.ExtendedDataType.Debug
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class DynamicTempTypeCreator
    {
        private string _namePrefix;
        private List<DataFieldDescriptor> _dataFieldDescriptors;


        /// <exclude />
        public DynamicTempTypeCreator(string namePrefix)
        {
            _namePrefix = namePrefix;

            Initialize();
        }



        /// <exclude />
        public static bool UseTempTypeCreator
        {
            get
            {
#if USE_TEMPTYPECREATOR
                return true;
#else
                return false;
#endif
            }
        }



        /// <exclude />
        public string TypeName
        {
            get;
            private set;
        }



        /// <exclude />
        public string TypeTitle
        {
            get;
            private set;
        }



        /// <exclude />
        public List<DataFieldDescriptor> DataFieldDescriptors
        {
            get
            {
                return _dataFieldDescriptors;
            }
        }


        private void Initialize()
        {
            int counter = 1;
            string typeName = string.Format("{0}{1}", _namePrefix, counter);
            while (true)
            {

                DataTypeDescriptor dataTypeDescriptor =
                    (from d in DataMetaDataFacade.GeneratedTypeDataTypeDescriptors
                     where d.Name == typeName
                     select d).FirstOrDefault();

                if (dataTypeDescriptor == null)
                {
                    this.TypeName = typeName;
                    this.TypeTitle = typeName;
                    break;
                }
                else
                {
                    typeName = string.Format("{0}{1}", _namePrefix, counter++);
                }
            }

            _dataFieldDescriptors = new List<DataFieldDescriptor>();

            DataFieldDescriptor stringDataFieldDescriptor = new DataFieldDescriptor(
                    Guid.NewGuid(),
                    "MyStringField",
                    StoreFieldType.String(64),
                    typeof(string)
                );            

            stringDataFieldDescriptor.Position = 10;
            stringDataFieldDescriptor.FormRenderingProfile = new DataFieldFormRenderingProfile
            {
                Label = "MyStringField",
                HelpText = "This is an auto-generated field.",
                WidgetFunctionMarkup = @"<f:widgetfunction xmlns:f=""http://www.composite.net/ns/function/1.0"" name=""Composite.Widgets.String.TextBox"" label="""" bindingsourcename=""""><f:helpdefinition xmlns:f=""http://www.composite.net/ns/function/1.0"" helptext="""" /></f:widgetfunction>"
            };

            stringDataFieldDescriptor.TreeOrderingProfile = new DataFieldTreeOrderingProfile
            {
                OrderPriority = 1,
                OrderDescending = false,
            };            

            _dataFieldDescriptors.Add(stringDataFieldDescriptor);


            DataFieldDescriptor intDataFieldDescriptor = new DataFieldDescriptor(
                    Guid.NewGuid(),
                    "MyIntField",
                    StoreFieldType.Integer,
                    typeof(int)
                );
            intDataFieldDescriptor.Position = 11;
            intDataFieldDescriptor.FormRenderingProfile = new DataFieldFormRenderingProfile
            {
                Label = "MyIntField",
                HelpText = "This is an auto-generated field.",
                WidgetFunctionMarkup = @"<f:widgetfunction xmlns:f=""http://www.composite.net/ns/function/1.0"" name=""Composite.Widgets.String.TextBox"" label="""" bindingsourcename=""""><f:helpdefinition xmlns:f=""http://www.composite.net/ns/function/1.0"" helptext="""" /></f:widgetfunction>"
            };
            intDataFieldDescriptor.TreeOrderingProfile = new DataFieldTreeOrderingProfile
            {
                OrderPriority = 2,
                OrderDescending = true,
            };            


            _dataFieldDescriptors.Add(intDataFieldDescriptor);


            DataFieldDescriptor dateTimeDataFieldDescriptor = new DataFieldDescriptor(
                    Guid.NewGuid(),
                    "MyDateTimeField",
                    StoreFieldType.DateTime,
                    typeof(DateTime?)
                );
            dateTimeDataFieldDescriptor.IsNullable = true;
            dateTimeDataFieldDescriptor.Position = 12;
            dateTimeDataFieldDescriptor.FormRenderingProfile = new DataFieldFormRenderingProfile
            {
                Label = "MyDateTimeField",
                HelpText = "This is an auto-generated field.",
                WidgetFunctionMarkup = @"<f:widgetfunction xmlns:f=""http://www.composite.net/ns/function/1.0"" name=""Composite.Widgets.Date.DateSelector"" label="""" bindingsourcename=""""><f:helpdefinition xmlns:f=""http://www.composite.net/ns/function/1.0"" helptext="""" /></f:widgetfunction>"
            };
            intDataFieldDescriptor.TreeOrderingProfile = new DataFieldTreeOrderingProfile
            {
                OrderPriority = null,
            };            

            _dataFieldDescriptors.Add(dateTimeDataFieldDescriptor);
        }
    }
}
