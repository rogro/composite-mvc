/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Composite.Data.DynamicTypes
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    [Serializable()]
    public class DataFieldTreeOrderingProfile
    {
        /// <exclude />
        public DataFieldTreeOrderingProfile()
        {
            OrderPriority = null;
            OrderDescending = false;
        }

        /// <exclude />
        public virtual int? OrderPriority { get; set; }

        /// <exclude />
        public virtual bool OrderDescending { get; set; }

        /// <exclude />
        public override string ToString()
        {
            return String.Format("{0},{1}", OrderPriority, OrderDescending);
        }

        /// <exclude />
        public static DataFieldTreeOrderingProfile FromString(string str)
        {
            var parts = str.Split(',');
            int priority;
            bool order;

            DataFieldTreeOrderingProfile treeOrderingProfile = new DataFieldTreeOrderingProfile();

            if (int.TryParse(parts[0], out priority))
                treeOrderingProfile.OrderPriority = priority;

            if (parts.Length > 1 && bool.TryParse(parts[1], out order))
                treeOrderingProfile.OrderDescending = order;

            return treeOrderingProfile;
        }

    }
}
