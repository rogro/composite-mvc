/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Composite.Core;
using Composite.Data;


namespace Composite.Data.DynamicTypes
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [EditorBrowsable(EditorBrowsableState.Never)]
    internal static class DataTypeValidator
    {
        /// <summary>
        /// This method validates if the existing .NET runtime type match the recorded meta data (DataTypeDescriptor).
        /// In case there is a mismatch, changes might have been done to the runtime type and an update on 
        /// the existing store(s)could not be performed.
        /// </summary>
        /// <param name="interfaceType"></param>
        /// <param name="existingDataTypeDescriptor"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static bool Validate(Type interfaceType, DataTypeDescriptor existingDataTypeDescriptor, out string errorMessage)
        {
            DataTypeDescriptor newDataTypeDescriptor;
            
            try
            {
                newDataTypeDescriptor  = DynamicTypeManager.BuildNewDataTypeDescriptor(interfaceType);
            }
            catch (Exception)
            {
                errorMessage = null;
                return true;
            }

            try
            {
                new DataTypeChangeDescriptor(existingDataTypeDescriptor, newDataTypeDescriptor);
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return false;
            }

            errorMessage = null;
            return true;
        }
    }
}
