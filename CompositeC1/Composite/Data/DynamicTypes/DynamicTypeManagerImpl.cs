/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Globalization;
using System.Transactions;
using Composite.Data.DynamicTypes.Foundation;
using Composite.Data.Foundation.PluginFacades;
using Composite.C1Console.Events;
using Composite.Data.Transactions;
using Composite.Core.Types;


namespace Composite.Data.DynamicTypes
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class DynamicTypeManagerImpl : IDynamicTypeManager
    {
        /// <exclude />
        public DataTypeDescriptor BuildNewDataTypeDescriptor(Type typeToDescript)
        {
            if (typeToDescript == null) throw new ArgumentNullException("typeToDescript");

            return ReflectionBasedDescriptorBuilder.Build(typeToDescript);
        }



        /// <exclude />
        public bool TryGetDataTypeDescriptor(Guid immuteableTypeId, out DataTypeDescriptor dataTypeDescriptor)
        {
            dataTypeDescriptor = DataMetaDataFacade.GetDataTypeDescriptor(immuteableTypeId);

            return dataTypeDescriptor != null;
        }



        /// <exclude />
        public void UpdateDataTypeDescriptor(DataTypeDescriptor dataTypeDescriptor, bool flushTheSystem)
        {
            dataTypeDescriptor.Validate();

            DataMetaDataFacade.PersistMetaData(dataTypeDescriptor);

            if (flushTheSystem)
            {
                GlobalEventSystemFacade.FlushTheSystem();
            }
        }



        /// <exclude />
        public void CreateStore(string providerName, DataTypeDescriptor typeDescriptor, bool doFlush)
        {
            if (string.IsNullOrEmpty(providerName)) throw new ArgumentNullException("providerName");
            if (typeDescriptor == null) throw new ArgumentNullException("typeDescriptor");

            typeDescriptor.Validate();

            using (TransactionScope transactionScope = TransactionsFacade.CreateNewScope())
            {
                DataMetaDataFacade.PersistMetaData(typeDescriptor);

                DataProviderPluginFacade.CreateStore(providerName, typeDescriptor);                                
                
                transactionScope.Complete();
            }

            if (doFlush)
            {
                GlobalEventSystemFacade.FlushTheSystem();
            }
        }



        /// <exclude />
        public void AlterStore(UpdateDataTypeDescriptor updateDataTypeDescriptor, bool forceCompile)
        {
            DataTypeChangeDescriptor dataTypeChangeDescriptor = updateDataTypeDescriptor.CreateDataTypeChangeDescriptor(); 

            dataTypeChangeDescriptor.AlteredType.Validate();

            using (TransactionScope transactionScope = TransactionsFacade.CreateNewScope())
            {
                DataMetaDataFacade.PersistMetaData(dataTypeChangeDescriptor.AlteredType);

                if (dataTypeChangeDescriptor.AlteredTypeHasChanges)
                {
                    DataProviderPluginFacade.AlterStore(updateDataTypeDescriptor, forceCompile);
                }                

                transactionScope.Complete();
            }
        }



        /// <exclude />
        public void DropStore(string providerName, DataTypeDescriptor typeDescriptor, bool makeAFlush)
        {
            Verify.ArgumentNotNullOrEmpty(providerName, "providerName");
            Verify.ArgumentNotNull(typeDescriptor, "typeDescriptor");

            typeDescriptor.Validate();

            using (var transactionScope = TransactionsFacade.CreateNewScope())
            {
                DataProviderPluginFacade.DropStore(providerName, typeDescriptor);
                DataMetaDataFacade.DeleteMetaData(typeDescriptor.DataTypeId);
                transactionScope.Complete();
            }

            if (makeAFlush)
            {
                GlobalEventSystemFacade.FlushTheSystem();
            }
        }



        /// <exclude />
        public void AddLocale(string providerName, CultureInfo cultureInfo)
        {
            if (string.IsNullOrEmpty(providerName)) throw new ArgumentNullException("providerName");
            if (cultureInfo == null) throw new ArgumentNullException("cultureInfo");

            using (var transactionScope = TransactionsFacade.CreateNewScope())
            {
                DataProviderPluginFacade.AddLocale(providerName, cultureInfo);
                transactionScope.Complete();
            }

            CodeGenerationManager.GenerateCompositeGeneratedAssembly(true);           
        }



        /// <exclude />
        public void RemoveLocale(string providerName, CultureInfo cultureInfo)
        {
            if (string.IsNullOrEmpty(providerName)) throw new ArgumentNullException("providerName");
            if (cultureInfo == null) throw new ArgumentNullException("cultureInfo");

            using (var transactionScope = TransactionsFacade.CreateNewScope())
            {
                DataProviderPluginFacade.RemoveLocale(providerName, cultureInfo);
                transactionScope.Complete();
            }

            CodeGenerationManager.GenerateCompositeGeneratedAssembly(true);            
        }
    }
}
