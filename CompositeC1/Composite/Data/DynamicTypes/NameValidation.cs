/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Globalization;
using System.Linq;
using Composite.Core.ResourceSystem;


namespace Composite.Data.DynamicTypes
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class NameValidation
    {
        /// <exclude />
        public static string ValidateNamespace(string namespaceString)
        {
            string errorMessage;
            if (TryValidateNamespace(namespaceString, out errorMessage) == false)
            {
                throw new ArgumentException(errorMessage);
            }

            return namespaceString;
        }



        /// <exclude />
        public static bool TryValidateNamespace(string namespaceString)
        {
            string errorMessage;

            return TryValidateNamespace(namespaceString, out errorMessage);
        }



        /// <exclude />
        public static bool TryValidateNamespace(string namespaceString, out string errorMessage)
        {
            errorMessage = "";

            if (string.IsNullOrEmpty(namespaceString))
            {
                errorMessage = StringResourceSystemFacade.GetString("Composite.NameValidation", "EmptyNamespace");
                return false;
            }


            string[] namespaceElements = namespaceString.Split('.');

            foreach (string namespaceElement in namespaceElements)
            {
                if (NameValidation.TryValidateName(namespaceElement, out errorMessage) == false)
                {
                    return false;
                }
            }

            if (namespaceElements.Distinct().Count() < namespaceElements.Count())
            {
                errorMessage = StringResourceSystemFacade.GetString("Composite.NameValidation", "DuplicateElementNamespace");
                return false;
            }

            return true;
        }




        /// <summary>
        /// Returns the name if all characters are valid in a type and field name. Invalid characters generate an exception.
        /// </summary>
        /// <param name="name">The name to validate</param>
        /// <returns>The name that was validated</returns>
        public static string ValidateName(string name)
        {
            string errorMessage;

            if (TryValidateName(name, out errorMessage) == false)
            {
                throw new ArgumentException(errorMessage);
            }

            return name;
        }



        /// <exclude />
        public static bool TryValidateName(string name)
        {
            string errorMessage;

            return TryValidateName(name, out errorMessage);
        }



        /// <exclude />
        public static bool TryValidateName(string name, out string errorMessage)
        {
            errorMessage = "";

            if (string.IsNullOrEmpty(name))
            {
                errorMessage = StringResourceSystemFacade.GetString("Composite.NameValidation", "EmptyName");
                return false;
            }

            for (int i = 0; i < name.Length; i++)
            {
                char ch = name[i];
                UnicodeCategory uc = Char.GetUnicodeCategory(ch);

                if (ch > 127)
                {
                    errorMessage = string.Format(StringResourceSystemFacade.GetString("Composite.NameValidation", "InvalidIdentifier"), name);
                    return false;
                }

                if (i == 0 && uc == UnicodeCategory.DecimalDigitNumber)
                {
                    errorMessage = string.Format(StringResourceSystemFacade.GetString("Composite.NameValidation", "InvalidIdentifierDigit"), name);
                    return false;
                }

                switch (uc)
                {
                    case UnicodeCategory.UppercaseLetter:
                    case UnicodeCategory.LowercaseLetter:
                    case UnicodeCategory.TitlecaseLetter:
                    case UnicodeCategory.DecimalDigitNumber:
                        break;
                    default:
                        if (ch == 95)
                            break;

                        errorMessage = string.Format(StringResourceSystemFacade.GetString("Composite.NameValidation", "InvalidIdentifier"), name);
                        return false;
                }
            }

            return true;
        }
    }
}
