/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Xml;
using System.Xml.Linq;

namespace Composite.Data.DynamicTypes
{
    [Serializable]
    internal class ParseDefinitionFileException : Exception
    {
        private readonly XObject _source;

        public ParseDefinitionFileException(string message, XObject source) 
            : base(GetMessage(message, source))
        {
            _source = source;
            Line = (source as IXmlLineInfo).LineNumber;
            Position = (source as IXmlLineInfo).LinePosition;
        }

        public override string Source
        {
            get
            {
                return _source.BaseUri;
            }
        }

        public int Line { get; private set; }
        public int Position { get; private set; }

        private static string GetMessage(string message, XObject source)
        {
            var lineInfo = source as IXmlLineInfo;

            return string.Format("{0} ({1}:{2}): {3}", source.BaseUri, lineInfo.LineNumber, lineInfo.LinePosition, message);
        }
    }
}
