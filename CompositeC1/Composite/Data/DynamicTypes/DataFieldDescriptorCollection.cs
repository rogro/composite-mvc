/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Composite.Core.Extensions;


namespace Composite.Data.DynamicTypes
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class DataFieldDescriptorCollection : IEnumerable<DataFieldDescriptor>
    {
        private readonly DataTypeDescriptor _parent;
        private readonly List<DataFieldDescriptor> _descriptors = new List<DataFieldDescriptor>();


        internal DataFieldDescriptorCollection(DataTypeDescriptor parent) 
        {
            _parent = parent;
        }


        /// <exclude />
        public int Count
        {
            get { return _descriptors.Count; }
        }

        /// <exclude />
        public void Add(DataFieldDescriptor descriptor)
        {
            Verify.ArgumentNotNull(descriptor, "descriptor");

            if (_descriptors.Contains(descriptor))
            {
                throw new ArgumentException("The specifed DataFieldDescriptor with ID '{0}' has already been added. ".FormatWith(descriptor.Id) +
                                            "Developers should ensure that the Immutable Field Id is unique on all fields.");
            }

            if (this[descriptor.Name] != null)
            {
                throw new InvalidOperationException("The specified field name '{0}' is in use by another DataFieldDescriptor".FormatWith(descriptor.Name));
            }
            if (this[descriptor.Id] != null)
            {
                throw new InvalidOperationException("The specified field Id '{0}' is in use by another DataFieldDescriptor".FormatWith(descriptor.Id));
            }

            _descriptors.Add(descriptor);
        }


        /// <exclude />
        public void Insert(int index, DataFieldDescriptor descriptor)
        {
            Verify.ArgumentNotNull(descriptor, "descriptor");

            if (_descriptors.Contains(descriptor)) throw new ArgumentException("The specified DataFieldDescriptor with ID '{0}' has already been added".FormatWith(descriptor.Id));

            if (this[descriptor.Name] != null)
            {
                throw new InvalidOperationException("The specified field name '{0}' is in use by another DataFieldDescriptor".FormatWith(descriptor.Name));
            }

            if (this[descriptor.Id] != null)
            {
                throw new InvalidOperationException("The specified field Id '{0}' is in use by another DataFieldDescriptor".FormatWith(descriptor.Id));
            }

            _descriptors.Insert(index, descriptor);
        }


        /// <exclude />
        public void Remove(DataFieldDescriptor descriptor)
        {
            Verify.ArgumentNotNull(descriptor, "descriptor");
            if (!_descriptors.Contains(descriptor)) throw new ArgumentException("The specified DataFieldDescriptor was not found");
            if (_parent.KeyPropertyNames.Contains(descriptor.Name)) throw new ArgumentException("The DataFieldDescriptor can not be removed while it is a member of the key field list.");
            if (_parent.StoreSortOrderFieldNames.Contains(descriptor.Name)) throw new ArgumentException("The DataFieldDescriptor can not be removed while it is a member of the physical sort order field list.");

            _descriptors.Remove(descriptor);
        }


        /// <exclude />
        public bool Contains(DataFieldDescriptor descriptor)
        {
            Verify.ArgumentNotNull(descriptor, "descriptor");

            return _descriptors.Contains(descriptor);
        }


        /// <exclude />
        public DataFieldDescriptor this[string name]
        {
            get { return _descriptors.SingleOrDefault(d => d.Name == name); }
        }


        /// <exclude />
        public DataFieldDescriptor this[Guid id]
        {
            get { return _descriptors.SingleOrDefault(d => d.Id == id); }
        }


        /// <exclude />
        public IEnumerator<DataFieldDescriptor> GetEnumerator()
        {
            return _descriptors.OrderBy(f => f.Position).GetEnumerator();
        }


        /// <exclude />
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _descriptors.OrderBy(f => f.Position).GetEnumerator();
        }
    }
}
