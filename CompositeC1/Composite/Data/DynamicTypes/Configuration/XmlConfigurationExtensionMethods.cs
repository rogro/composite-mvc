/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Configuration;
using System.Xml;
using System.Xml.Linq;
using Composite.Core.Extensions;

namespace Composite.Data.DynamicTypes.Configuration
{
    internal static class XmlConfigurationExtensionMethods
    {
        public static XAttribute GetRequiredAttribute(this XElement configurationElement, XName attributeName)
        {
            XAttribute result = configurationElement.Attribute(attributeName);

            if (result == null)
            {
                ThrowConfiguraitonError("Missing required attribute '{0}'".FormatWith(attributeName), configurationElement);
            }

            return result;
        }

        public static XElement GetRequiredElement(this XElement configurationElement, XName elementName)
        {
            var element = configurationElement.Element(elementName);

            if (element == null)
            {
                ThrowConfiguraitonError("Missing required element <{0}>".FormatWith(elementName), configurationElement);
            }

            return element;
        }

        public static string GetRequiredAttributeValue(this XElement configurationElement, XName attributeName)
        {
            string result = (string)configurationElement.Attribute(attributeName);

            if (string.IsNullOrEmpty(result))
            {
                ThrowConfiguraitonError("Required attribute '{0}' is missing or empty".FormatWith(attributeName), configurationElement);
            }

            return result;
        }

        public static void ThrowConfiguraitonError(string message, XObject configurationXObject)
        {
            var document = configurationXObject.Document;
            if (document != null && !string.IsNullOrEmpty(document.BaseUri) && (configurationXObject as IXmlLineInfo).HasLineInfo())
            {
                string fileName = GetFileName(document);
                int lineNumber = (configurationXObject as IXmlLineInfo).LineNumber;

                throw new ConfigurationErrorsException(message, fileName, lineNumber);
            }

            throw new InvalidOperationException(message);
        }

        public static Exception GetConfigurationException(string message, Exception innerException, XObject configurationXObject)
        {
            var document = configurationXObject.Document;
            if (document != null && !string.IsNullOrEmpty(document.BaseUri) && (configurationXObject as IXmlLineInfo).HasLineInfo())
            {
                string fileName = GetFileName(document);
                int lineNumber = (configurationXObject as IXmlLineInfo).LineNumber;

                throw new ConfigurationErrorsException(message, innerException, fileName, lineNumber);
            }

            throw new InvalidOperationException(message, innerException);
        }

        private static string GetFileName(XDocument document)
        {
            const string fileUriPrefix = "file:///";
            string baseUri = document.BaseUri;

            return baseUri.StartsWith(fileUriPrefix, StringComparison.Ordinal)
                ? baseUri.Substring(fileUriPrefix.Length)
                : baseUri;
        }
    }
}
