/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using System.Xml.Linq;
using Composite.Core.Xml;
using Composite.Data.Types;
using Composite.Plugins.Data.DataProviders.FileSystemDataProvider.Foundation;


namespace Composite.Data.DynamicTypes.Foundation
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    public static class DynamicTypesCustomFormFacade
    {
        /// <summary>
        /// Returns custom form markup. 
        /// </summary>
        /// <param name="dataTypeDescriptor">A data type descriptor</param>
        /// <returns></returns>
        public static XDocument GetCustomFormMarkup(DataTypeDescriptor dataTypeDescriptor)
        {
            var file = GetCustomFormMarkupFile(dataTypeDescriptor.Namespace, dataTypeDescriptor.Name);

            if (file == null)
            {
                return null;
            }

            var markupFilePath = (file as FileSystemFile).SystemPath;

            return XDocumentUtils.Load(markupFilePath, LoadOptions.SetBaseUri | LoadOptions.SetLineInfo);
        }

        internal static IFile GetCustomFormMarkupFile(string @namespace, string typeName)
        {
            string dynamicDataFormFolderPath = GetFolderPath(@namespace);
            string dynamicDataFormFileName = GetFilename(typeName);

            IDynamicTypeFormDefinitionFile formOverride =
                DataFacade.GetData<IDynamicTypeFormDefinitionFile>()
                          .FirstOrDefault(f => f.FolderPath.Equals(dynamicDataFormFolderPath, StringComparison.OrdinalIgnoreCase)
                                            && f.FileName.Equals(dynamicDataFormFileName, StringComparison.OrdinalIgnoreCase));

            return formOverride;
        }



        /// <exclude />
        public static void SetCustomForm(DataTypeDescriptor dataTypeDescriptor, string newFormMarkup)
        {
            string dynamicDataFormFolderPath = GetFolderPath(dataTypeDescriptor.Namespace);
            string dynamicDataFormFileName = GetFilename(dataTypeDescriptor.Name);

            // Parsing for assertion
            XDocument.Parse(newFormMarkup);

            IDynamicTypeFormDefinitionFile formDefinitionFile =
                DataFacade.GetData<IDynamicTypeFormDefinitionFile>()
                  .FirstOrDefault(f => f.FolderPath.Equals(dynamicDataFormFolderPath, StringComparison.OrdinalIgnoreCase) 
                                    && f.FileName.Equals(dynamicDataFormFileName, StringComparison.OrdinalIgnoreCase));

            if (formDefinitionFile == null)
            {
                var newFile = DataFacade.BuildNew<IDynamicTypeFormDefinitionFile>();
                newFile.FolderPath = dynamicDataFormFolderPath;
                newFile.FileName = dynamicDataFormFileName;
                newFile.SetNewContent(newFormMarkup);
                formDefinitionFile = DataFacade.AddNew<IDynamicTypeFormDefinitionFile>(newFile);
            }
            else
            {
                formDefinitionFile.SetNewContent(newFormMarkup);
                DataFacade.Update(formDefinitionFile);
            }
        }



        private static string GetFilename(string typeName)
        {
            return string.Format("{0}.xml", typeName);
        }



        private static string GetFolderPath(string @namespace)
        {
            return "\\" + @namespace.Replace('.', '\\');
        }
    }
}
