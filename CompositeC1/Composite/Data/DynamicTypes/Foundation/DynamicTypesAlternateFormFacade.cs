/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using System.Xml.Linq;
using Composite.Data.Types;


namespace Composite.Data.DynamicTypes.Foundation
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    [Obsolete("User DynamicTypesCustomFormFacade instead")]
    public static class DynamicTypesAlternateFormFacade
    {
        
        /// <summary>
        /// Returns null if no alternate form exists.
        /// </summary>
        /// <exclude />
        public static string GetAlternateFormMarkup(DataTypeDescriptor dataTypeDescriptor)
        {
            var file = GetAlternateFormMarkupFile(dataTypeDescriptor.Namespace, dataTypeDescriptor.Name);

            return file != null ? file.ReadAllText() : null;
        }

        internal static IFile GetAlternateFormMarkupFile(string @namespace, string typeName)
        {
            string dynamicDataFormFolderPath = GetFolderPath(@namespace);
            string dynamicDataFormFileName = GetFilename(typeName);

            IDynamicTypeFormDefinitionFile formOverride =
                DataFacade.GetData<IDynamicTypeFormDefinitionFile>()
                          .FirstOrDefault(f => f.FolderPath.Equals(dynamicDataFormFolderPath, StringComparison.OrdinalIgnoreCase)
                                            && f.FileName.Equals(dynamicDataFormFileName, StringComparison.OrdinalIgnoreCase));

            return formOverride;
        }



        /// <exclude />
        public static void SetAlternateForm(DataTypeDescriptor dataTypeDescriptor, string newFormMarkup)
        {
            string dynamicDataFormFolderPath = GetFolderPath(dataTypeDescriptor.Namespace);
            string dynamicDataFormFileName = GetFilename(dataTypeDescriptor.Name);

            // Parsing for assertion
            XDocument.Parse(newFormMarkup);

            IDynamicTypeFormDefinitionFile formDefinitionFile =
                DataFacade.GetData<IDynamicTypeFormDefinitionFile>()
                  .FirstOrDefault(f => f.FolderPath.Equals(dynamicDataFormFolderPath, StringComparison.OrdinalIgnoreCase) 
                                    && f.FileName.Equals(dynamicDataFormFileName, StringComparison.OrdinalIgnoreCase));

            if (formDefinitionFile == null)
            {
                var newFile = DataFacade.BuildNew<IDynamicTypeFormDefinitionFile>();
                newFile.FolderPath = dynamicDataFormFolderPath;
                newFile.FileName = dynamicDataFormFileName;
                newFile.SetNewContent(newFormMarkup);
                formDefinitionFile = DataFacade.AddNew<IDynamicTypeFormDefinitionFile>(newFile);
            }
            else
            {
                formDefinitionFile.SetNewContent(newFormMarkup);
                DataFacade.Update(formDefinitionFile);
            }
        }



        private static string GetFilename(string typeName)
        {
            return string.Format("{0}.xml", typeName);
        }



        private static string GetFolderPath(string @namespace)
        {
            return "\\" + @namespace.Replace('.', '\\');
        }
    }
}
