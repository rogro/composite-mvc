/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Xml.Linq;
using Composite.Core.Types;


namespace Composite.Data.DynamicTypes
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
	public sealed class DataTypeAssociationDescriptor
	{
        /// <exclude />
        public DataTypeAssociationDescriptor(Type associatedInterfaceType, string foreignKeyPropertyName, DataAssociationType dataAssociationType)
        {
            this.AssociatedInterfaceType = associatedInterfaceType;
            this.ForeignKeyPropertyName = foreignKeyPropertyName;
            this.AssociationType = dataAssociationType;
        }


        /// <exclude />
        public Type AssociatedInterfaceType
        {
            get;
            private set;
        }


        /// <exclude />
        public string ForeignKeyPropertyName
        {
            get;
            private set;
        }


        /// <exclude />
        public DataAssociationType AssociationType
        {
            get;
            private set;
        }



        /// <exclude />
        public XElement ToXml()
        {
            XElement element = new XElement("DataTypeAssociationDescriptor");

            element.Add(new XAttribute("associatedInterfaceType", TypeManager.SerializeType(this.AssociatedInterfaceType)));
            element.Add(new XAttribute("foreignKeyPropertyName", this.ForeignKeyPropertyName));
            element.Add(new XAttribute("associationType", this.AssociationType));

            return element;
        }



        /// <exclude />
        public static DataTypeAssociationDescriptor FromXml(XElement element)
        {
            if (element.Name != "DataTypeAssociationDescriptor") throw new ArgumentException("The xml is not correctly formattet");

            XAttribute associatedInterfaceTypeAttribute = element.Attribute("associatedInterfaceType");
            XAttribute foreignKeyPropertyNameAttribute = element.Attribute("foreignKeyPropertyName");
            XAttribute associationTypeAttribute = element.Attribute("associationType");

            if ((associatedInterfaceTypeAttribute == null) || (foreignKeyPropertyNameAttribute == null) || (associatedInterfaceTypeAttribute == null)) throw new ArgumentException("The xml is not correctly formattet");

            Type associatedInterfaceType = TypeManager.GetType(associatedInterfaceTypeAttribute.Value);
            string foreignKeyPropertyName = foreignKeyPropertyNameAttribute.Value;
            DataAssociationType dataAssociationType = (DataAssociationType)Enum.Parse(typeof(DataAssociationType), associationTypeAttribute.Value);

            return new DataTypeAssociationDescriptor(associatedInterfaceType, foreignKeyPropertyName, dataAssociationType);
        }



        /// <exclude />
        public override bool Equals(object obj)
        {
            return Equals(obj as DataTypeAssociationDescriptor);
        }



        /// <exclude />
        public bool Equals(DataTypeAssociationDescriptor dataTypeAssociationDescriptor)
        {
            if (dataTypeAssociationDescriptor == null) return false;

            return
                this.AssociatedInterfaceType == dataTypeAssociationDescriptor.AssociatedInterfaceType &&
                this.ForeignKeyPropertyName == dataTypeAssociationDescriptor.ForeignKeyPropertyName &&
                this.AssociationType == dataTypeAssociationDescriptor.AssociationType;
        }



        /// <exclude />
        public override int GetHashCode()
        {
            return
                this.AssociatedInterfaceType.GetHashCode() ^
                this.ForeignKeyPropertyName.GetHashCode() ^
                this.AssociationType.GetHashCode();
        }
	}
}
