/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using Composite.Core.ResourceSystem;
using Composite.Core.ResourceSystem.Plugins.ResourceProvider;
using Composite.Plugins.ResourceSystem.FileSystemBasedIconResourceProvider.Foundation;
using Composite.Core.WebClient;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ObjectBuilder;
using Microsoft.Practices.ObjectBuilder;


namespace Composite.Plugins.ResourceSystem.FileSystemBasedIconResourceProvider
{
    [ConfigurationElementType(typeof(FileSystemBasedIconResourceProviderData))]
	internal sealed class FileSystemBasedIconResourceProvider : IIconResourceProvider
	{
        private FileSystemBasedIconResourceProviderHelper _helper;

        public const string _iconMappingsFileName = "StandardIcons.xml";

        public readonly string _baseDirectoryPath;

        public FileSystemBasedIconResourceProvider(string baseDirectoryPath, string iconMappingsFilename)
        {
            _baseDirectoryPath = UrlUtils.ResolveAdminUrl("images/icons");
            _helper = new FileSystemBasedIconResourceProviderHelper(baseDirectoryPath, iconMappingsFilename);
        }



        public IEnumerable<string> GetIconNames()
        {
            return _helper.GetIconNames();
        }



        public Bitmap GetIcon(string name, IconSize iconSize, CultureInfo cultureInfo)
        {
            return _helper.GetIcon(name, iconSize, cultureInfo);
        }
    }




    [Assembler(typeof(FileSystemBasedIconResourceProviderAssembler))]
    internal sealed class FileSystemBasedIconResourceProviderData : ResourceProviderData
    {
        private const string _baseDirectoryPathPropertyName = "baseDirectoryPath";
        [ConfigurationProperty(_baseDirectoryPathPropertyName, IsRequired = true)]
        public string BaseDirectoryPath
        {
            get { return (string)base[_baseDirectoryPathPropertyName]; }
            set { base[_baseDirectoryPathPropertyName] = value; }
        }

        private const string _iconMappingsFilenamePropertyName = "iconMappingsFilename";
        [ConfigurationProperty(_iconMappingsFilenamePropertyName, IsRequired = true)]
        public string IconMappingsFilename
        {
            get { return (string)base[_iconMappingsFilenamePropertyName]; }
            set { base[_iconMappingsFilenamePropertyName] = value; }
        }
    }




    internal sealed class FileSystemBasedIconResourceProviderAssembler : IAssembler<IResourceProvider, ResourceProviderData>
    {
        public IResourceProvider Assemble(IBuilderContext context, ResourceProviderData objectConfiguration, IConfigurationSource configurationSource, ConfigurationReflectionCache reflectionCache)
        {
            FileSystemBasedIconResourceProviderData data = (FileSystemBasedIconResourceProviderData)objectConfiguration;

            return new FileSystemBasedIconResourceProvider(data.BaseDirectoryPath, data.IconMappingsFilename);
        }
    }
}
