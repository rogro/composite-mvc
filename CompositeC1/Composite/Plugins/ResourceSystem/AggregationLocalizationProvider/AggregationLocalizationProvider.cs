/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using Composite.Core.Collections.Generic;
using Composite.Core.Configuration;
using Composite.Core.ResourceSystem.Plugins.ResourceProvider;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ObjectBuilder;
using Microsoft.Practices.ObjectBuilder;

namespace Composite.Plugins.ResourceSystem.AggregationLocalizationProvider
{
    /// <summary>
    /// ILocalizationProvider that aggrages multiple IStringResourceProviders. To be used for backward compatibility.
    /// </summary>
    [ConfigurationElementType(typeof(AggregationLocalizationProviderData))]
    internal class AggregationLocalizationProvider: ILocalizationProvider
    {
        private readonly Dictionary<string, IStringResourceProvider> _providers;
        private CultureInfo[] _supportedCultures;

        public AggregationLocalizationProvider(Dictionary<string, IStringResourceProvider> providers)
        {
            _providers = providers;
        }

        public IEnumerable<string> GetSections()
        {
            return _providers.Keys;
        }

        public string GetString(string section, string stringId, CultureInfo cultureInfo)
        {
            IStringResourceProvider stringResourceProvider;

            if (!_providers.TryGetValue(section, out stringResourceProvider))
            {
                return null;
            }

            return stringResourceProvider.GetStringValue(stringId, cultureInfo);
        }

        public ReadOnlyDictionary<string, string> GetAllStrings(string section, CultureInfo cultureInfo)
        {
            IStringResourceProvider stringResourceProvider;

            if (!_providers.TryGetValue(section, out stringResourceProvider))
            {
                return null;
            }

            IDictionary<string, string> dictionary = stringResourceProvider.GetAllStrings(cultureInfo);

            return new ReadOnlyDictionary<string, string>(new Dictionary<string, string>(dictionary));
        }

        public IEnumerable<CultureInfo> GetSupportedCultures()
        { 
            if(_supportedCultures == null)
            {
                var result = new List<CultureInfo>();

                foreach(var provider in _providers.Values)
                {
                    result.AddRange(provider.GetSupportedCultures());
                }

                _supportedCultures = result.Distinct().ToArray();
            }

            return _supportedCultures;
        }


        #region Configuration

        [Assembler(typeof(AggregationLocalizationProviderAssembler))]
        internal sealed class AggregationLocalizationProviderData : ResourceProviderData
        {
            private const string _providersProperty = "Providers";
            [ConfigurationProperty(_providersProperty)]
            public NameTypeManagerTypeConfigurationElementCollection<ResourceProviderData> Providers
            {
                get
                {
                    return (NameTypeManagerTypeConfigurationElementCollection<ResourceProviderData>)base[_providersProperty];
                }
            }
        }

        internal sealed class AggregationLocalizationProviderAssembler : IAssembler<IResourceProvider, ResourceProviderData>
        {
            public IResourceProvider Assemble(IBuilderContext context, ResourceProviderData objectConfiguration, IConfigurationSource configurationSource, ConfigurationReflectionCache reflectionCache)
            {
                var data = (AggregationLocalizationProviderData)objectConfiguration;

                var stringResourceProviders = new Dictionary<string, IStringResourceProvider>();

                // TODO: rewrite using proper Object builder API
                foreach(var configRecord in data.Providers)
                {
                    var type = configRecord.Type;
                    var configTypeAttr = type.GetCustomAttributes(typeof (ConfigurationElementTypeAttribute), true).FirstOrDefault() as ConfigurationElementTypeAttribute;
                    Verify.IsNotNull(configTypeAttr, "Attribute not defined: " + typeof(ConfigurationElementTypeAttribute).FullName);

                    Type configType = configTypeAttr.ConfigurationType;
                    var assemblerAttr = configType.GetCustomAttributes(typeof(AssemblerAttribute), true).FirstOrDefault() as AssemblerAttribute;
                    Verify.IsNotNull(configTypeAttr, "Attribute not defined: " + typeof(AssemblerAttribute).FullName);

                    var assemblerType = assemblerAttr.AssemblerType;

                    var assembler = assemblerType.GetConstructor(new Type[0]).Invoke(new object[0]) as IAssembler<IResourceProvider, ResourceProviderData>;

                    IResourceProvider resourceProvider = assembler.Assemble(context, configRecord, configurationSource, reflectionCache);
                    Verify.That(resourceProvider is IStringResourceProvider, "Provider should inherit " + typeof(IStringResourceProvider).FullName);

                    stringResourceProviders.Add(configRecord.Name, resourceProvider as IStringResourceProvider);
                }

                return new AggregationLocalizationProvider(stringResourceProviders);
            }
        }

        #endregion Configuration
    }
}
