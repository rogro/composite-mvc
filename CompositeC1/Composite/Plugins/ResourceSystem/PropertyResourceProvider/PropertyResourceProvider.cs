/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using Composite.Core.ResourceSystem.Plugins.ResourceProvider;
using Composite.Core.Types;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;


namespace Composite.Plugins.ResourceSystem.PropertyResourceProvider
{
    [ConfigurationElementType(typeof(NonConfigurableResourceProvider))]
    internal sealed class PropertyResourceProvider : IStringResourceProvider
	{
        public string GetStringValue(string stringId, CultureInfo cultureInfo)
        {
            int index = stringId.LastIndexOf('.');
            if (index == -1) throw new InvalidOperationException(string.Format("'{0}' is not of correct format", stringId));

            string typeName = stringId.Substring(0, index);
            string propertyName = stringId.Substring(index + 1);

            Type type = TypeManager.GetType(typeName);
            if (type == null) throw new InvalidOperationException(string.Format("'{0}' type could not found", typeName));

            PropertyInfo propertyInfo = type.GetProperty(propertyName, BindingFlags.Public | BindingFlags.Static);

            if (propertyInfo == null) throw new InvalidOperationException(string.Format("The property '{0}' on the type '{1}' could not be found", propertyInfo));

            return (string)propertyInfo.GetValue(null, null);
        }



        public IDictionary<string, string> GetAllStrings(CultureInfo cultureInfo)
        {
            return new Dictionary<string, string>();
        }



        public IEnumerable<CultureInfo> GetSupportedCultures()
        {
            yield break;
        }        
    }
}
