/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Text.RegularExpressions;
using Composite.C1Console.Events;
using Composite.Core.Logging;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;


namespace Composite.Plugins.Logging.LogTraceListeners.ManagementConsoleLogTracer
{
    [ConfigurationElementType(typeof(CustomTraceListenerData))]
    internal sealed class ManagementConsoleLogTracer : CustomTraceListener
    {
        private LogLevel _logLevel;

        public ManagementConsoleLogTracer()
        {

            _logLevel = LogLevel.Info;
        }

        public ManagementConsoleLogTracer(string logLevel)
        {
            switch (logLevel)
            {
                case "Fine":
                    _logLevel = LogLevel.Fine;
                    break;
                case "Info":
                    _logLevel = LogLevel.Info;
                    break;
                case "Fatal":
                    _logLevel = LogLevel.Fatal;
                    break;
                case "Warning":
                    _logLevel = LogLevel.Warning;
                    break;
                case "Debug":
                    _logLevel = LogLevel.Debug;
                    break;
                case "Error":
                    _logLevel = LogLevel.Error;
                    break;
                default:
                    throw new ArgumentException( "Unhandled log level: " + logLevel);
            }
        }


        public override void Write(string message)
        {
            Regex regex = new Regex(@"RGB\((?<r>[0-9]+), (?<g>[0-9]+), (?<b>[0-9]+)\)");
            Match matchMessage = regex.Match(message);
            if (matchMessage.Success)
            {
                message = message.Replace(matchMessage.Groups[0].Value, "");
            }

            LogEntryMessageQueueItem messageItem = new LogEntryMessageQueueItem { Level = _logLevel, Message = message, Sender = this.GetType() };
            ConsoleMessageQueueFacade.Enqueue(messageItem, null);
        }


        public override void WriteLine(string message)
        {
            this.Write(message);
        }
    }

}
