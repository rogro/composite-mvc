/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Composite.Core.Logging;


namespace Composite.Plugins.Logging.LogTraceListeners.FileLogTraceListener
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [EditorBrowsable(EditorBrowsableState.Never)] 
    internal class PlainFileReader : LogFileReader
    {
        private FileStream _file;
        private readonly string _filePath;
        private int? _entriesCount;

        public PlainFileReader(string filePath, DateTime date)
        {
            _filePath = filePath;
            Date = date;
        }

        [DebuggerStepThrough]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Composite.IO", "Composite.DoNotUseFileClass:DoNotUseFileClass", Justification = "This is what we want, touch is used later on")]
        public override bool Open()
        {
            try
            {
                _file = File.OpenRead(_filePath);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public override void Close()
        {
            if (_file == null) return;

            _file.Close();
            _file.Dispose();
            _file = null;
        }

        public static IEnumerable<LogEntry> ParseLogLines(IEnumerable<string> lines)
        {
            var multilineMessage = new StringBuilder();

            LogEntry currentEntry = null;
            foreach (string line in lines)
            {
                LogEntry nextEntry = LogEntry.Parse(line);
                if (nextEntry != null)
                {
                    if (currentEntry != null)
                    {
                        if (multilineMessage.Length > 0)
                        {
                            currentEntry.Message = multilineMessage.ToString();
                            multilineMessage.Clear();
                        }

                        yield return currentEntry;
                    }
                    currentEntry = nextEntry;
                }
                else
                {
                    if (currentEntry != null)
                    {
                        if (multilineMessage.Length == 0)
                        {
                            multilineMessage.Append(currentEntry.Message);
                        }

                        multilineMessage.AppendLine(line);
                    }
                }
            }

            if (currentEntry != null)
            {
                if (multilineMessage.Length > 0)
                {
                    currentEntry.Message = multilineMessage.ToString();
                    multilineMessage.Clear();
                }

                yield return currentEntry;
            }
        }

        public override IEnumerable<LogEntry> GetLogEntries(DateTime timeFrom, DateTime timeFromTo)
        {
            var logLines = GetLogLinesEnumerable();

            return ParseLogLines(logLines);
        }

        private IEnumerable<string> GetLogLinesEnumerable()
        {
            using (var reader = new StreamReader(_file, Encoding.UTF8))
            {
                while (reader.Peek() >= 0)
                {
                    yield return reader.ReadLine();
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Composite.IO", "Composite.DoNotUseFileClass:DoNotUseFileClass", Justification = "This is what we want, touch is used later on")]
        public override int EntriesCount
        {
            get
            {
                if (_entriesCount == null)
                {
                    try
                    {
                        Open();

                        _entriesCount = GetLogEntries(DateTime.MinValue, DateTime.MaxValue).Count();
                    }
                    finally
                    {
                        Close();
                    }
                }

                return (int)_entriesCount;
            }
        }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Composite.IO", "Composite.DoNotUseFileClass:DoNotUseFileClass", Justification = "This is what we want, touch is used later on")]
        public override bool Delete()
        {
            try
            {
                File.Delete(_filePath);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
