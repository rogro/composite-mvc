/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using Composite.Core.Logging;


namespace Composite.Plugins.Logging.LogTraceListeners.FileLogTraceListener
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [EditorBrowsable(EditorBrowsableState.Never)] 
    internal class CurrentFileReader : LogFileReader
    {
        private readonly FileLogger _fileLogger;

        public CurrentFileReader(FileLogger fileLogger)
        {
            _fileLogger = fileLogger;

            lock (_fileLogger._syncRoot)
            {
                var fileConnection = _fileLogger._fileConnection;

                if (fileConnection != null)
                {
                    Date = fileConnection.CreationDate;
                }
            }

        }

        public override bool Open()
        {
            // do nothing
            return true;
        }

        public override void Close()
        {
            // do nothing
        }

        public override int EntriesCount
        {
            get
            {
                lock (_fileLogger._syncRoot)
                {
                    return _fileLogger._fileConnection.OldEntries.Length +
                           _fileLogger._fileConnection.NewEntries.Count;
                }
            }
        }

        public override IEnumerable<LogEntry> GetLogEntries(DateTime timeFrom, DateTime timeTo)
        {
            if (timeFrom < _fileLogger.StartupTime)
            {
                var parserEnumerable = PlainFileReader.ParseLogLines(_fileLogger._fileConnection.OldEntries);
                foreach (var logEntry in parserEnumerable)
                {
                    yield return logEntry;
                }
            }
            
            LogEntry[] newEntries = null;

            lock (_fileLogger._syncRoot)
            {
                var fileConnection = _fileLogger._fileConnection;
                if (fileConnection != null)
                {
                    newEntries = fileConnection.NewEntries.ToArray();
                }
            }

            if (newEntries != null)
            {
                foreach (var logEntry in newEntries) yield return logEntry;
            }
        }

        public override bool Delete()
        {
            return false;
        }
    }
}
