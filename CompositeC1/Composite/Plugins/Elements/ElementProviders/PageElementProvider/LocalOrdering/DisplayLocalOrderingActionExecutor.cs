/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Web;
using Composite.C1Console.Actions;
using Composite.C1Console.Events;
using Composite.C1Console.Security;


namespace Composite.Plugins.Elements.ElementProviders.PageElementProvider.LocalOrdering
{
    internal sealed class DisplayLocalOrderingActionExecutor : IActionExecutor
	{
        private static string BaseUrl = "/Website/Spikes/PageLocalOrdering/ShowLocalOrdering.aspx";

        public FlowToken Execute(EntityToken entityToken, ActionToken actionToken, FlowControllerServicesContainer flowControllerServicesContainer)
        {
            string currentConsoleId = flowControllerServicesContainer.GetService<Composite.C1Console.Events.IManagementConsoleMessageService>().CurrentConsoleId;

            DisplayLocalOrderingActionToken castedActionToken = (DisplayLocalOrderingActionToken)actionToken;

            string url = string.Format("{0}?ParentPageId={1}", BaseUrl, HttpUtility.UrlEncode(castedActionToken.ParentPageId.ToString()));

            ConsoleMessageQueueFacade.Enqueue(
                new OpenViewMessageQueueItem 
                    { 
                        EntityToken = EntityTokenSerializer.Serialize(entityToken, true),
                        Url = url, 
                        ViewId = Guid.NewGuid().ToString(), 
                        ViewType = ViewType.Main, 
                        Label = "Pages local orderings" 
                    }, 
                currentConsoleId);

            return null;
        }
	}
}
