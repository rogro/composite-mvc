/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Composite.Functions;


namespace Composite.Plugins.Elements.ElementProviders.XsltBasedFunctionProviderElementProvider
{
	internal static class FlowUiQueryMarkupHelper
	{
        private static readonly XNamespace ui = "http://www.w3.org/1999/xhtml";


        public static void ParseXml(string theXml, out IEnumerable<KeyValuePair<string, string>> queries, out ILookup<string, KeyValuePair<string, string>> parameterLookup)
        {
            XDocument document = XDocument.Parse(theXml);

            queries =
                 from nodes in document.Descendants(ui + "treenode")
                 where (string)nodes.Attribute("binding") == "QueryTreeNodeBinding"
                 select new KeyValuePair<string, string>((string)nodes.Attribute("localname"), (string)nodes.Attribute("ElementId"));

            var paramList =
                from nodes in document.Descendants(ui + "treenode")
                where (string)nodes.Attribute("binding") == "QueryParamTreeNodeBinding"
                select new { QueryLocalName = (string)nodes.Parent.Attribute("localname"), Name = (string)nodes.Attribute("paramname"), Value = (string)nodes.Attribute("paramvalue") };

            parameterLookup = paramList.ToLookup(f => f.QueryLocalName, f => new KeyValuePair<string, string>(f.Name, f.Value));
        }



        public static string BuildXml(IEnumerable<KeyValuePair<string, string>> queries, ILookup<string, KeyValuePair<string, string>> parameters)
        {
            
            var usedQueryInfos =
                from query in queries
                select new { LocalName = query.Key, QueryInfo = FunctionFacade.GetFunction(query.Value) };

            var paramsDictionary = parameters.ToDictionary(f => f.Key);

            XElement rootElement = new XElement(ui + "treebody");

            foreach (var namedQueryInfo in usedQueryInfos.OrderBy(f => f.QueryInfo.Namespace + f.QueryInfo.Name + f.LocalName))
            {
                IFunction queryInfo = namedQueryInfo.QueryInfo;

                XElement currentNamespaceFolder = rootElement;

                if (string.IsNullOrEmpty(queryInfo.Namespace) == false)
                {
                    string piecemealNamespace = "";

                    foreach (string namespaceSegment in queryInfo.Namespace.Split('.'))
                    {
                        if (string.IsNullOrEmpty(piecemealNamespace) == false) piecemealNamespace = piecemealNamespace + ".";
                        piecemealNamespace = piecemealNamespace + namespaceSegment;

                        XElement existingFolder =
                            (from folderElement in rootElement.Descendants(ui + "treenode")
                             where (string)folderElement.Attribute("binding") == "QueryFolderTreeNodeBinding" && (string)folderElement.Attribute("ElementId") == piecemealNamespace
                             select folderElement).FirstOrDefault();

                        if (existingFolder == null)
                        {
                            XElement subFolder = new XElement(ui + "treenode",
                                                     new XAttribute("binding", "QueryFolderTreeNodeBinding"),
                                                     new XAttribute("ElementId", piecemealNamespace),
                                                     new XAttribute("label", namespaceSegment));
                            currentNamespaceFolder.Add(subFolder);

                            currentNamespaceFolder = subFolder;
                        }
                        else
                        {
                            currentNamespaceFolder = existingFolder;
                        }
                    }
                }


                IGrouping<string, KeyValuePair<string, string>> currentQueryParams;
                bool hasParams = paramsDictionary.TryGetValue(namedQueryInfo.LocalName, out currentQueryParams);

                currentNamespaceFolder.Add(
                    new XElement(ui + "treenode",
                        new XAttribute("binding", "QueryTreeNodeBinding"),
                        new XAttribute("queryname", queryInfo.Name),
                        new XAttribute("localname", namedQueryInfo.LocalName),
                        new XAttribute("ElementId", queryInfo.CompositeName()),
                        from parameter in queryInfo.ParameterProfiles
                        orderby parameter.Name
                        select new XElement(ui + "treenode",
                            new XAttribute("binding", "QueryParamTreeNodeBinding"),
                            new XAttribute("paramname", parameter.Name),
                            new XAttribute("hasvalue", hasParams && currentQueryParams.Count(f => f.Key == parameter.Name) == 1),
                            (hasParams && currentQueryParams.Count(f => f.Key == parameter.Name && f.Value != null) == 1 ? new XAttribute("paramvalue", currentQueryParams.First(f => f.Key == parameter.Name).Value) : null))));
            }

            XmlDocument output = new XmlDocument();
            output.LoadXml(rootElement.ToString());
            XmlNamespaceManager namespaceManager = new XmlNamespaceManager(output.NameTable);
            namespaceManager.AddNamespace("ui", ui.ToString());
            XmlNodeList allNodes = output.SelectNodes("//*");
            foreach (XmlNode node in allNodes)
            {
                node.Prefix = "ui";
            }
            output.DocumentElement.RemoveAttribute("xmlns");
            return IndentedOuterXml(output);

        }


        public static string IndentedOuterXml(XmlNode node)
        {
            StringWriter stringWriter = new StringWriter();
            XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter);
            xmlTextWriter.Formatting = Formatting.Indented;
            node.WriteTo(xmlTextWriter);
            return stringWriter.ToString();
        }
	}
}
