/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Configuration;

using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;


namespace Composite.Plugins.Elements.ElementProviders.VirtualElementProvider
{
    [Obsolete("Was replaced by VirtualElementConfigurationElement")]
    internal class BaseElementConfigurationElement : NameTypeConfigurationElement
    {
        private const string _idProperty = "id";
        [ConfigurationProperty(_idProperty, IsRequired = true, IsKey = true)]
        public string Id
        {
            get { return (string)base[_idProperty]; }
            set { base[_idProperty] = value; }
        }


        private const string _orderProperty = "order";
        [ConfigurationProperty(_orderProperty, IsRequired = true, IsKey = true)]
        public int Order
        {
            get { return (int)base[_orderProperty]; }
            set { base[_orderProperty] = value; }
        }


        private const string _parentIdProperty = "parentId";
        [ConfigurationProperty(_parentIdProperty)]
        public string ParentId
        {
            get { return (string)base[_parentIdProperty]; }
            set { base[_parentIdProperty] = value; }
        }


        private const string _labelProperty = "label";
        [ConfigurationProperty(_labelProperty, IsRequired = true)]
        public string Label
        {
            get { return (string)base[_labelProperty]; }
            set { base[_labelProperty] = value; }
        }


        private const string _tagProperty = "tag";
        [ConfigurationProperty(_tagProperty, DefaultValue = null)]
        public string Tag
        {
            get { return (string)base[_tagProperty]; }
            set { base[_tagProperty] = value; }
        }


        private const string _closeFolderIconNameProperty = "closeFolderIconName";
        [ConfigurationProperty(_closeFolderIconNameProperty)]
        public string CloseFolderIconName
        {
            get { return (string)base[_closeFolderIconNameProperty]; }
            set { base[_closeFolderIconNameProperty] = value; }
        }



        private const string _openFolderIconNameProperty = "openFolderIconName";
        [ConfigurationProperty(_openFolderIconNameProperty)]
        public string OpenFolderIconName
        {
            get { return (string)base[_openFolderIconNameProperty]; }
            set { base[_openFolderIconNameProperty] = value; }
        }
    }
}
