/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace Composite.Plugins.Elements.ElementProviders.VirtualElementProvider
{
    [ConfigurationElementType(typeof(SimpleVirtualElement))]
    internal class SimpleVirtualElement : VirtualElementConfigurationElement
    {
        private const string _labelProperty = "label";
        [ConfigurationProperty(_labelProperty, IsRequired = true)]
        public string Label
        {
            get { return (string)base[_labelProperty]; }
            set { base[_labelProperty] = value; }
        }

        /// <summary>
        /// Used by the client js to filter perspective related elements
        /// </summary>
        private const string _tagProperty = "tag";
        [ConfigurationProperty(_tagProperty, DefaultValue = null)]
        public string Tag
        {
            get { return (string)base[_tagProperty]; }
            set { base[_tagProperty] = value; }
        }


        private const string _closeFolderIconNameProperty = "closeFolderIconName";
        [ConfigurationProperty(_closeFolderIconNameProperty, DefaultValue = null)]
        public string CloseFolderIconName
        {
            get { return (string)base[_closeFolderIconNameProperty]; }
            set { base[_closeFolderIconNameProperty] = value; }
        }



        private const string _openFolderIconNameProperty = "openFolderIconName";
        [ConfigurationProperty(_openFolderIconNameProperty)]
        public string OpenFolderIconName
        {
            get { return (string)base[_openFolderIconNameProperty]; }
            set { base[_openFolderIconNameProperty] = value; }
        }

        private const string _elementsProperty = "Elements";
        [ConfigurationProperty(_elementsProperty, IsRequired = true)]
        public NameTypeConfigurationElementCollection<VirtualElementConfigurationElement, VirtualElementConfigurationElement> Elements
        {
            get
            {
                return (NameTypeConfigurationElementCollection<VirtualElementConfigurationElement, VirtualElementConfigurationElement>)base[_elementsProperty];
            }
        }
    }
}
