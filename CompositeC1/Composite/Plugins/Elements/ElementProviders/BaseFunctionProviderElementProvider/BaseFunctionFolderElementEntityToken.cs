/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using System.Threading;
using Composite.C1Console.Security;
using Composite.Core.Extensions;


namespace Composite.Plugins.Elements.ElementProviders.BaseFunctionProviderElementProvider
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [SecurityAncestorProvider(typeof(BaseFunctionFolderElementEntityTokenSecurityAncestorProvider))]
	public sealed class BaseFunctionFolderElementEntityToken : EntityToken
	{
        private readonly string _id;
        private string _elementProviderName;
        private string _functionNamespace;

        /// <exclude />
        public BaseFunctionFolderElementEntityToken(string elementProviderName, string functionNamespace)
        {
            Verify.ArgumentCondition(!elementProviderName.Contains('.'), "elementProviderName", "Function element provider name can't contain '.' symbol in its name");

            if (functionNamespace == "")
            {
                _id = "ROOT:" + elementProviderName;
            }
            else
            {
                _id = "ROOT:{0}.{1}".FormatWith(elementProviderName, functionNamespace);
            }
        }

        /// <exclude />
        public BaseFunctionFolderElementEntityToken(string id)
        {
            _id = id;
        }

        /// <exclude />
        public override string Type
        {
            get { return ""; }
        }

        /// <exclude />
        public override string Source
        {
            get { return ""; }
        }

        /// <exclude />
        public override string Id
        {
            get { return _id; }
        }

        /// <exclude />
        public override string Serialize()
        {
            return DoSerialize();
        }

        /// <summary>
        /// Gets the function namespace.
        /// </summary>
        public string FunctionNamespace
        {
            get
            {
                ParseId();
                return _functionNamespace;
            }
        }

        /// <summary>
        /// Gets the name of the function provider.
        /// </summary>
        /// <value>
        /// The name of the function provider.
        /// </value>
        public string ElementProviderName
        {
            get
            {
                ParseId();
                return _elementProviderName;
            }
        }

        private void ParseId()
        {
            if(_elementProviderName != null) return;

            const string prefix = "ROOT:";

            Verify.That(_id.StartsWith(prefix), "Id should start with prefix '{0}'", prefix);

            string providerAndNamespace = _id.Substring(prefix.Length);
            int pointOffset = providerAndNamespace.IndexOf('.');

            if(pointOffset == -1)
            {
                _functionNamespace = null;
                Thread.MemoryBarrier();
                _elementProviderName = providerAndNamespace;
            }
            else
            {
                _functionNamespace = providerAndNamespace.Substring(pointOffset + 1);
                Thread.MemoryBarrier();
                _elementProviderName = providerAndNamespace.Substring(0, pointOffset);
            }
        }

        /// <exclude />
        public static EntityToken Deserialize(string serializedData)
        {
            string type, source, id;

            EntityToken.DoDeserialize(serializedData, out type, out source, out id);

            return new BaseFunctionFolderElementEntityToken(id);
        }
    }
}
