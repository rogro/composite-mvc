/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.Data;
using Composite.Data.Plugins.DataProvider.Streams;
using Composite.Data.Streams;
using Composite.Data.Types;


namespace Composite.Plugins.Elements.ElementProviders.MediaFileProviderElementProvider
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [FileStreamManager(typeof(FileSystemFileStreamManager))]
    public sealed class WorkflowMediaFile : FileSystemFileBase, IMediaFile
    {
        /// <exclude />
        public WorkflowMediaFile()
        {
            Title = string.Empty;
            Description = string.Empty;
            MimeType = string.Empty;
            DataSourceId = new DataSourceId(typeof(IMediaFile));
        }



        /// <exclude />
        public WorkflowMediaFile(IMediaFile file)
        {
            Id = file.Id;
            StoreId = file.StoreId;
            Title = file.Title;
            Culture = file.Culture;
            CreationTime = file.CreationTime;
            DataSourceId = file.DataSourceId;
            Description = file.Description;
            FileName = file.FileName;
            FolderPath = file.FolderPath;
            IsReadOnly = file.IsReadOnly;
            LastWriteTime = file.LastWriteTime;
            Length = file.Length;
            MimeType = file.MimeType;
        }



        /// <exclude />
        public Guid Id
        {
            get; internal set;
        }



        /// <exclude />
        public string KeyPath
        {
            get { return this.GetKeyPath(); }
        }



        /// <exclude />
        public string CompositePath
        {
            get { return this.GetCompositePath(); }
            set { throw new InvalidOperationException(); }
        }



        /// <exclude />
        public string StoreId
        {
            get;
            set;
        }



        /// <exclude />
        public string Title
        {
            get;
            set;
        }



        /// <exclude />
        public string Description
        {
            get;
            set;
        }



        /// <exclude />
        public string Culture
        {
            get;
            set;
        }



        /// <exclude />
        public string MimeType
        {
            get;
            set;
        }



        /// <exclude />
        public int? Length
        {
            get;
            set;
        }



        /// <exclude />
        public DateTime? CreationTime
        {
            get;
            set;
        }



        /// <exclude />
        public DateTime? LastWriteTime
        {
            get;
            set;
        }



        /// <exclude />
        public bool IsReadOnly
        {
            get;
            set;
        }



        /// <exclude />
        public string FolderPath
        {
            get;
            set;
        }



        /// <exclude />
        public string FileName
        {
            get;
            set;
        }



        /// <exclude />
        public DataSourceId DataSourceId
        {
            get;
            set;
        }
    }
}
