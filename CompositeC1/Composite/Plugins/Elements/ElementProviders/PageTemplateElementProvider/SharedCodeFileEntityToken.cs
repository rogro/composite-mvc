/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using Composite.C1Console.Security;

namespace Composite.Plugins.Elements.ElementProviders.PageTemplateElementProvider
{
    /// <summary>
    /// Represents an entity token for a shared code file
    /// </summary>
    [SecurityAncestorProvider(typeof(SharedCodeFileEntityToken.SharedCodeFileSecurityAncestorProvider))]
    public class SharedCodeFileEntityToken : EntityToken
    {
        private readonly string _virtualPath;

        /// <summary>
        /// Initializes a new instance of the <see cref="SharedCodeFileEntityToken"/> class.
        /// </summary>
        /// <param name="virtualPath">The relative file path.</param>
        public SharedCodeFileEntityToken(string virtualPath)
        {
            _virtualPath = virtualPath;
        }

        /// <exclude />
        public override string Type { get { return "_"; } }
        /// <exclude />
        public override string Source { get { return "_"; } }
        /// <exclude />
        public override string Id { get { return "_"; } }
        /// <exclude />
        public override string Serialize() { return _virtualPath; }

        /// <summary>
        /// Gets the relative file path.
        /// </summary>
        public string VirtualPath
        {
            get { return _virtualPath; }
        }

        /// <exclude />
        public static EntityToken Deserialize(string serializedData)
        {
            return new SharedCodeFileEntityToken(serializedData);
        }

        /// <exclude />
        public override bool Equals(object obj)
        {
            return base.Equals(obj) && (obj as SharedCodeFileEntityToken).VirtualPath == VirtualPath;
        }

        /// <exclude />
        public override int GetHashCode()
        {
            return base.GetHashCode() ^ VirtualPath.GetHashCode();
        }

        internal class SharedCodeFileSecurityAncestorProvider : ISecurityAncestorProvider
        {
            public IEnumerable<EntityToken> GetParents(EntityToken entityToken)
            {
                return new[] { new SharedCodeFolderEntityToken() };
            }
        }
    }
}
