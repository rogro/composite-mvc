/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.C1Console.Actions;
using Composite.C1Console.Events;
using Composite.Core.ResourceSystem;
using Composite.C1Console.Security;
using Composite.Core.WebClient;
using Composite.Plugins.Elements.ElementProviders.BaseFunctionProviderElementProvider;
using Composite.Core.Collections;


namespace Composite.Plugins.Elements.ElementProviders.AllFunctionsElementProvider
{
    internal sealed class AllFunctionsProviderActionExecutor : IActionExecutor
    {
        public FlowToken Execute(EntityToken entityToken, ActionToken actionToken, FlowControllerServicesContainer flowControllerServicesContainer)
        {
            string functionNamePrefix = "";
            if (entityToken.Id.IndexOf('.') > -1)
            {
                functionNamePrefix = entityToken.Id.Substring(entityToken.Id.IndexOf('.') + 1);
            }

            bool widgets = entityToken.Id.ToLower().Contains("widget");

            string currentConsoleId = flowControllerServicesContainer.GetService<IManagementConsoleMessageService>().CurrentConsoleId;
            string url = UrlUtils.ResolveAdminUrl(string.Format("content/views/functiondoc/FunctionDocumentation.aspx?functionPrefix={0}&widgets={1}", functionNamePrefix, widgets)); 

            ConsoleMessageQueueFacade.Enqueue(new OpenViewMessageQueueItem
                {
                    Url = url,
                    EntityToken = EntityTokenSerializer.Serialize(entityToken, true),
                    ViewId = Guid.NewGuid().ToString(),
                    ViewType = ViewType.Main,
                    Label = "Documentation"
                }, currentConsoleId);

            return null;
        }
    }
}
