/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;
using System.Text;
using Composite.Core.PackageSystem;


namespace Composite.Plugins.Elements.ElementProviders.PackageElementProvider
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
	public static class WorkflowHelper
	{
        /// <exclude />
        public static object ValidationResultToBinding(List<PackageFragmentValidationResult> packageFragmentValidationResults)
        {
            List<List<string>> rows = new List<List<string>>();
            
            foreach (PackageFragmentValidationResult packageFragmentValidationResult in packageFragmentValidationResults)
            {
                StringBuilder sb = new StringBuilder();

                if (packageFragmentValidationResult.XPath != null)
                {
                    sb.AppendLine(packageFragmentValidationResult.Message);
                    //sb.Append("XPath: ");
                    //sb.Append(packageFragmentValidationResult.XPath);
                }
                else
                {
                    sb.Append(packageFragmentValidationResult.Message);
                }

                List<string> row = new List<string>();
                row.Add(sb.ToString());

                rows.Add(row);
            }

            return rows;
        }
	}
}
