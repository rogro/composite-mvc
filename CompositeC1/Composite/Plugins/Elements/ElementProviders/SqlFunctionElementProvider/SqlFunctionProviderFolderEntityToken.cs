/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;
using System.Text;
using Composite.C1Console.Security;
using Composite.Core.Serialization;


namespace Composite.Plugins.Elements.ElementProviders.SqlFunctionElementProvider
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [SecurityAncestorProvider(typeof(SqlFunctionProviderEntityTokenSecurityAncestorProvider))]
    public sealed class SqlFunctionProviderFolderEntityToken : EntityToken
	{
        private string _id;
        private string _source;
        private string _connectionId;

        internal SqlFunctionProviderFolderEntityToken(string id, string source, string connectionId)
        {
            _id = id;
            _source = source;
            _connectionId = connectionId;
        }



        /// <exclude />
        public override string Type
        {
            get { return ""; }
        }



        /// <exclude />
        public override string Source
        {
            get { return _source; }
        }



        /// <exclude />
        public override string Id
        {
            get { return _id; }
        }



        /// <exclude />
        public string ConnectionId
        {
            get { return _connectionId; }

        }


        /// <exclude />
        public override string Serialize()
        {
            StringBuilder builder = new StringBuilder();

            StringConversionServices.SerializeKeyValuePair<string>(builder, "id", _id);
            StringConversionServices.SerializeKeyValuePair<string>(builder, "source", _source);
            StringConversionServices.SerializeKeyValuePair<string>(builder, "connectionId", _connectionId);

            return builder.ToString();
        }



        /// <exclude />
        public static EntityToken Deserialize(string serializedData)
        {
            IDictionary<string, string> result = StringConversionServices.ParseKeyValueCollection(serializedData);

            string id = StringConversionServices.DeserializeValueString(result["id"]);
            string source = StringConversionServices.DeserializeValueString(result["source"]);
            string connectionId = StringConversionServices.DeserializeValueString(result["connectionId"]);

            return new SqlFunctionProviderFolderEntityToken(id, source, connectionId);
        }


        /// <exclude />
        public override bool Equals(object obj)
        {
            return base.Equals(obj)
                   && (obj as SqlFunctionProviderFolderEntityToken).ConnectionId == this.ConnectionId;
        }


        /// <exclude />
        public override int GetHashCode()
        {
            if (this.HashCode == 0)
            {
                this.HashCode = GetType().GetHashCode() ^ this.Type.GetHashCode() ^ this.Source.GetHashCode() ^ this.Id.GetHashCode() ^ this.ConnectionId.GetHashCode();
            }
            return this.HashCode;
        }
	}
}
