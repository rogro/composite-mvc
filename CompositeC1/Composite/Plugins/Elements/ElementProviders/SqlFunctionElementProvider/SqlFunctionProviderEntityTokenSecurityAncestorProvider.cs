/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Composite.Core.Collections;
using Composite.Data;
using Composite.Data.Types;
using Composite.C1Console.Elements;
using Composite.Core.Extensions;
using Composite.C1Console.Security;


namespace Composite.Plugins.Elements.ElementProviders.SqlFunctionElementProvider
{
    internal sealed class SqlFunctionProviderEntityTokenSecurityAncestorProvider : ISecurityAncestorProvider
    {
        public IEnumerable<EntityToken> GetParents(EntityToken entityToken)
        {
            if ((entityToken is SqlFunctionProviderRootEntityToken))
            {
                return new EntityToken[] { };
            }
            else if ((entityToken is SqlFunctionProviderFolderEntityToken))
            {
                SqlFunctionProviderFolderEntityToken token = entityToken as SqlFunctionProviderFolderEntityToken;

                NamespaceTreeBuilder builder = (NamespaceTreeBuilder)ElementFacade.GetData(new ElementProviderHandle(token.Source), token.ConnectionId);

                NamespaceTreeBuilderFolder folder = builder.FindFolder(f => StringExtensionMethods.CreateNamespace(f.Namespace, f.Name, '.') == token.Id);

                if (folder == null)
                {
                    return null;
                }
                else
                {
                    int idx = token.Id.LastIndexOf('.');
                    if (idx != -1)
                    {
                        return new EntityToken[] { new SqlFunctionProviderFolderEntityToken(token.Id.Remove(idx), token.Source, token.ConnectionId) };
                    }
                    else
                    {
                        Guid id = new Guid(token.ConnectionId);
                        ISqlConnection sqlConnection = DataFacade.GetData<ISqlConnection>(f => f.Id == id).SingleOrDefault();

                        if (sqlConnection == null)
                        {
                            return new EntityToken[] { };
                        }

                        return new EntityToken[] { sqlConnection.GetDataEntityToken() };
                    }
                }
            }
            else
            {
                throw new NotImplementedException();
            }
        }
    }
}
