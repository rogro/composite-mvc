/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.IO;
using System.Text;
using Composite.Core.IO;


namespace Composite.Plugins.Elements.ElementProviders.WebsiteFileElementProvider
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public class WebsiteFile : WebsiteEntity
	{
        private string _filename = null;
        private string _mimeTypeInfo = null;
        private bool? isReadOnly = null;


        /// <exclude />
        public WebsiteFile(string fullPath)
            : base(fullPath, false)
        {
        }


        /// <exclude />
        public string FileName
        {
            get
            {
                if (_filename == null)
                {
                    _filename = Path.GetFileName(this.FullPath);
                }

                return _filename;
            }
        }


        /// <exclude />
        public string MimeType
        {
            get 
            {
                if (_mimeTypeInfo == null)
                {
                    _mimeTypeInfo = MimeTypeInfo.GetCanonicalFromExtension(Path.GetExtension(this.FullPath));
                }

                return _mimeTypeInfo;
            }
        }



        /// <exclude />
        public string ReadAllText()
        {
            return C1File.ReadAllText(this.FullPath);
        }



        /// <exclude />
        public bool IsReadOnly
        {
            get 
            {
                if (isReadOnly.HasValue == false)
                {
                    isReadOnly = (C1File.GetAttributes(this.FullPath) & FileAttributes.ReadOnly) == FileAttributes.ReadOnly;
                }

                return isReadOnly.Value; 
            }
        }



        /// <exclude />
        public void WriteAllText(string content)
        {
            // Default encode is Composite.Core.IO.StreamWriter.UTF8NoBOM, which is UTF8 without encoding signature
            C1File.WriteAllText(this.FullPath, content, Encoding.UTF8);
        }
    }
}
