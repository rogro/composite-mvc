/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;
using System.IO;
using Composite.C1Console.Security;
using Composite.Core.IO;


namespace Composite.Plugins.Elements.ElementProviders.WebsiteFileElementProvider
{
    internal sealed class WebsiteFileProviderEntityTokenSecurityAncestorProvider : ISecurityAncestorProvider
    {
        public IEnumerable<EntityToken> GetParents(EntityToken entityToken)
        {
            WebsiteFileElementProviderEntityToken castedEntityToken = (WebsiteFileElementProviderEntityToken)entityToken;

            if ((C1File.Exists(castedEntityToken.Path) == false) &&
                (C1Directory.Exists(castedEntityToken.Path) == false))
            {
                return null;
            }

            string newFolderPath = Path.GetDirectoryName(castedEntityToken.Path);

            string rootFolder = castedEntityToken.RootPath;

            if (newFolderPath != rootFolder)
            {
                Verify.That(newFolderPath.Length > rootFolder.Length, 
                            "File/folder path '{0}' does not much root folder '{1}'",
                            newFolderPath, rootFolder);

                return new EntityToken[] { new WebsiteFileElementProviderEntityToken(castedEntityToken.Source, newFolderPath, castedEntityToken.RootPath) };
            }
            
            return new EntityToken[] { new WebsiteFileElementProviderRootEntityToken(castedEntityToken.Source) };
        }
    }
}
