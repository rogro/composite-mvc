/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Text;
using Composite.C1Console.Security;
using Composite.Core.Serialization;


namespace Composite.Plugins.Elements.ElementProviders.GeneratedDataTypesElementProvider
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [SecurityAncestorProvider(typeof(GeneratedDataTypesElementProviderSecurityAncestorProvider))]
    public sealed class GeneratedDataTypesElementProviderTypeEntityToken : EntityToken
    {
        private string _id;
        private string _providerName;


        /// <exclude />
        public GeneratedDataTypesElementProviderTypeEntityToken(string serializedTypeName, string providerName, string id)
        {
            _id = id;
            _providerName = providerName;
            this.SerializedTypeName = serializedTypeName;
        }


        /// <exclude />
        public override string Type
        {
            get { return "GeneratedDataTypesElementProvider"; }
        }


        /// <exclude />
        public override string Source
        {
            get { return _providerName; }
            
        }


        /// <exclude />
        public override string Id
        {
            get { return _id; }
        }


        /// <exclude />
        public string SerializedTypeName
        {
            get;
            private set;
        }


        /// <exclude />
        public override string Serialize()
        {
            StringBuilder sb = new StringBuilder();

            DoSerialize(sb);

            StringConversionServices.SerializeKeyValuePair(sb, "_SerializedTypeName_", this.SerializedTypeName);

            return sb.ToString();
        }


        /// <exclude />
        public static EntityToken Deserialize(string serializedEntityToken)
        {
            string type, source, id;
            Dictionary<string, string> dic;

            DoDeserialize(serializedEntityToken, out type, out source, out id, out dic);

            if (dic.ContainsKey("_SerializedTypeName_") == false) 
            {
                throw new ArgumentException("The serializedEntityToken is not a serialized entity token", "serializedEntityToken");
            }

            string serializedTypeName = StringConversionServices.DeserializeValueString(dic["_SerializedTypeName_"]);

            return new GeneratedDataTypesElementProviderTypeEntityToken(serializedTypeName, source, id);
        }


        /// <exclude />
        public override bool Equals(object obj)
        {
            return base.Equals(obj) &&
                   (obj as GeneratedDataTypesElementProviderTypeEntityToken).SerializedTypeName == this.SerializedTypeName;
        }


        /// <exclude />
        public override int GetHashCode()
        {
            if (this.HashCode == 0)
            {
                this.HashCode = GetType().GetHashCode() ^ this.Type.GetHashCode() ^ this.Source.GetHashCode() ^ this.Id.GetHashCode() ^ this.SerializedTypeName.GetHashCode();
            }
            return this.HashCode;
        }
    }
}
