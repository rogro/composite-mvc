/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using Composite.Data.Validation.ClientValidationRules;
using Composite.Data.Validation.Plugins.ClientValidationRuleTranslator;
using Composite.Data.Validation.Validators;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ObjectBuilder;


namespace Composite.Plugins.Validation.ClientValidationRuleTranslators.StandardClientValidationRuleTranslator
{
    [ConfigurationElementType(typeof(StandardClientValidationRuleTranslatorData))]
    internal sealed class StandardClientValidationRuleTranslator : IClientValidationRuleTranslator
    {
        private readonly List<Type> _supportedTypes = new List<Type>
            {
                typeof(RegexValidatorAttribute),
#pragma warning disable 0612
                typeof(StringLengthValidatorAttribute),
#pragma warning restore 0612
                typeof(Microsoft.Practices.EnterpriseLibrary.Validation.Validators.NotNullValidatorAttribute)
            };



        public IEnumerable<Type> GetSupportedAttributeTypes()
        {
            return _supportedTypes;
        }



        public ClientValidationRule Translate(Attribute attribute)
        {
            Type type = attribute.GetType();

            if (type == typeof(RegexValidatorAttribute))
            {
                return new RegexClientValidationRule((attribute as RegexValidatorAttribute).Pattern);
            }
#pragma warning disable 0612
            else if (type == typeof(StringLengthValidatorAttribute))
            {
                return new StringLengthClientValidationRule((attribute as StringLengthValidatorAttribute).LowerBound, (attribute as StringLengthValidatorAttribute).UpperBound);
            }
#pragma warning restore 0612
            else if (type == typeof(StringSizeValidatorAttribute))
            {
                return new StringLengthClientValidationRule((attribute as StringSizeValidatorAttribute).LowerBound, (attribute as StringSizeValidatorAttribute).UpperBound);
            }

            else if (type == typeof(Microsoft.Practices.EnterpriseLibrary.Validation.Validators.NotNullValidatorAttribute))
            {
                return new NotNullClientValidationRule();
            }
            else
            {
                throw new InvalidOperationException(string.Format("The attribute type '{0}' is not supported", attribute.GetType()));
            }
        }
    }



    [Assembler(typeof(NonConfigurableClientValidationRuleTranslatorAssembler))]
    internal sealed class StandardClientValidationRuleTranslatorData : ClientValidationRuleTranslatorData
    {
    }
}
