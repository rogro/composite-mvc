/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Composite.Data;
using Composite.Data.Types;

namespace Composite.Plugins.Routing.Hostnames
{
    internal class FormFunctions
    {
        public static IEnumerable<Tuple<object, object, string>> GetRootPages()
        {
            var result = new List<Tuple<object, object, string>>();

            CultureInfo[] cultures = DataLocalizationFacade.ActiveLocalizationCultures.ToArray();

            foreach(var culture in cultures)
            {
                using(new DataScope(PublicationScope.Unpublished, culture))
                {
                    foreach(Guid rootPageId in PageManager.GetChildrenIDs(Guid.Empty))
                    {
                        IPage page = PageManager.GetPageById(rootPageId);

                        if(page == null) continue;

                        string label = page.Title;
                        if(cultures.Length > 1)
                        {
                            label += ", " + culture.NativeName;
                        }

                        result.Add(new Tuple<object, object, string>(page.Id, culture.Name, label));
                    }
                }
            }

            return result;
        }
    }
}
