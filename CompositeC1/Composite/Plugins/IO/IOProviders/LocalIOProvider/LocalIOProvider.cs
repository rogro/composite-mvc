/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.IO;
using System.Text;
using Composite.Core.IO.Plugins.IOProvider;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;


namespace Composite.Plugins.IO.IOProviders.LocalIOProvider
{
    [ConfigurationElementType(typeof(NonConfigurableIOProvider))]
    internal class LocalIOProvider : IIOProvider
    {
        public IC1Directory C1Directory
        {
            get 
            {
                return new LocalC1Directory();
            }
        }



        public IC1File C1File
        {
            get 
            {
                return new LocalC1File();
            }
        }



        public IC1FileInfo CreateFileInfo(string fileName)
        {
            return new LocalC1FileInfo(fileName);
        }



        public IC1DirectoryInfo CreateDirectoryInfo(string path)
        {
            return new LocalC1DirectoryInfo(path);
        }



        public IC1FileStream CreateFileStream(string path, FileMode mode, FileAccess access, FileShare share, int bufferSize, FileOptions options)
        {
            return new LocalC1FileStream(path, mode, access, share, bufferSize, options);
        }



        public IC1FileSystemWatcher CreateFileSystemWatcher(string path, string filter)
        {
            return new LocalC1FileSystemWatcher(path, filter);
        }



        public IC1StreamReader CreateStreamReader(string path, Encoding encoding, bool detectEncodingFromByteOrderMarks, int bufferSize)
        {
            return new LocalC1StreamReader(path, encoding, detectEncodingFromByteOrderMarks, bufferSize);
        }



        public IC1StreamReader CreateStreamReader(Stream stream, Encoding encoding, bool detectEncodingFromByteOrderMarks, int bufferSize)
        {
            return new LocalC1StreamReader(stream, encoding, detectEncodingFromByteOrderMarks, bufferSize);
        }



        public IC1StreamWriter CreateStreamWriter(string path, bool append, Encoding encoding, int bufferSize)
        {
            return new LocalC1StreamWriter(path, append, encoding, bufferSize);
        }



        public IC1StreamWriter CreateStreamWriter(Stream stream, Encoding encoding, int bufferSize)
        {
            return new LocalC1StreamWriter(stream, encoding, bufferSize);
        }



        public IC1Configuration CreateConfiguration(string path)
        {
            return new LocalC1Configuration(path);
        }        
    }
}
