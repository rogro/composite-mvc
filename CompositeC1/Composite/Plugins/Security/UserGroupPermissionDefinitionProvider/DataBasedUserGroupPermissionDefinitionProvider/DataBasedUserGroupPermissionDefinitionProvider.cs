/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Transactions;
using Composite.Data;
using Composite.Data.Caching;
using Composite.Data.Types;
using Composite.C1Console.Security;
using Composite.C1Console.Security.Plugins.UserGroupPermissionDefinitionProvider;
using Composite.Data.Transactions;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;


namespace Composite.Plugins.Security.UserGroupPermissionDefinitionProvider.DataBasedUserGroupPermissionDefinitionProvider
{
    [ConfigurationElementType(typeof(NonConfigurableUserGroupPermissionDefinitionProvider))]
    class DataBasedUserGroupPermissionDefinitionProvider : IUserGroupPermissionDefinitionProvider
	{
        private static readonly int PermissionCacheSize = 50;
        private static readonly Cache<Guid, ReadOnlyCollection<UserGroupPermissionDefinition>> _permissionCache = new Cache<Guid, ReadOnlyCollection<UserGroupPermissionDefinition>>("DataBasedUserGroupPermissionDefinitionProvider", PermissionCacheSize);

        static DataBasedUserGroupPermissionDefinitionProvider()
        {
            DataEventSystemFacade.SubscribeToDataAfterUpdate<IUserGroupPermissionDefinition>(OnUserGroupPermissionChanged, true);
            DataEventSystemFacade.SubscribeToDataAfterAdd<IUserGroupPermissionDefinition>(OnUserGroupPermissionChanged, true);
            DataEventSystemFacade.SubscribeToDataDeleted<IUserGroupPermissionDefinition>(OnUserGroupPermissionChanged, true);
            DataEventSystemFacade.SubscribeToStoreChanged<IUserGroupPermissionDefinition>(OnUserGroupPermissionStoreChanged, true);
            
        }



        public IEnumerable<UserGroupPermissionDefinition> AllUserGroupPermissionDefinitions
        {
            get 
            {
                return
                    (from urd in DataFacade.GetData<IUserGroupPermissionDefinition>()
                     select (UserGroupPermissionDefinition)new DataUserGroupPermissionDefinition(urd)).ToList();
            }
        }



        public bool CanAlterDefinitions
        {
            get { return true; }
        }



        public IEnumerable<UserGroupPermissionDefinition> GetPermissionsByUserGroup(Guid userGroupId)
        {
            ReadOnlyCollection<UserGroupPermissionDefinition> cachedUserGroupPermissionDefinitions = _permissionCache.Get(userGroupId);
            if (cachedUserGroupPermissionDefinitions != null)
            {
                return cachedUserGroupPermissionDefinitions;
            }

            List<UserGroupPermissionDefinition> userGroupPermissionDefinitions = 
                (from urd in DataFacade.GetData<IUserGroupPermissionDefinition>()
                 where urd.UserGroupId == userGroupId
                 select (UserGroupPermissionDefinition)new DataUserGroupPermissionDefinition(urd)).ToList();

            _permissionCache.Add(userGroupId, new ReadOnlyCollection<UserGroupPermissionDefinition>(userGroupPermissionDefinitions));

            return userGroupPermissionDefinitions;
        }


        private EntityToken DeserializeSilent(string serializedEntityToken)
        {
            try
            {
                return EntityTokenSerializer.Deserialize(serializedEntityToken);
            }
            catch (Exception)
            {
                // Silent
                return null;
            }
        }

        public void SetUserGroupPermissionDefinition(UserGroupPermissionDefinition userGroupPermissionDefinition)
        {
            Guid userGroupId = userGroupPermissionDefinition.UserGroupId;
            string serializedEntityToken = userGroupPermissionDefinition.SerializedEntityToken;

            using (TransactionScope transactionScope = TransactionsFacade.CreateNewScope())
            {
                IEnumerable<IUserGroupPermissionDefinition> existingPermissionDefinitions =
                        DataFacade.GetData<IUserGroupPermissionDefinition>()
                                  .Where(d => d.UserGroupId == userGroupId)
                                  .ToList()
                                  .Where(d => userGroupPermissionDefinition.EntityToken.Equals(DeserializeSilent(d.SerializedEntityToken)))
                                  .ToList();

                DataFacade.Delete(existingPermissionDefinitions);

                IUserGroupPermissionDefinition definition = DataFacade.BuildNew<IUserGroupPermissionDefinition>();
                definition.Id = Guid.NewGuid();
                definition.UserGroupId = userGroupId;
                definition.SerializedEntityToken = serializedEntityToken;

                DataFacade.AddNew(definition);


                foreach (PermissionType permissionType in userGroupPermissionDefinition.PermissionTypes)
                {
                    IUserGroupPermissionDefinitionPermissionType permission = DataFacade.BuildNew<IUserGroupPermissionDefinitionPermissionType>();
                    permission.Id = Guid.NewGuid();
                    permission.PermissionTypeName = permissionType.ToString();
                    permission.UserGroupPermissionDefinitionId = definition.Id;

                    DataFacade.AddNew(permission);
                }

                transactionScope.Complete();
            }
        }



        public void RemoveUserGroupPermissionDefinition(Guid userGroupId, string serializedEntityToken)
        {
            var entityToken = EntityTokenSerializer.Deserialize(serializedEntityToken);

            using (var transactionScope = TransactionsFacade.CreateNewScope())
            {
                IList<IUserGroupPermissionDefinition> toDelete;

                if (entityToken.IsValid())
                {
                    toDelete = DataFacade.GetData<IUserGroupPermissionDefinition>()
                        .Where(ugpd => ugpd.UserGroupId == userGroupId)
                        .ToList()
                        .Where(d => entityToken.Equals(DeserializeSilent(d.SerializedEntityToken)))
                        .ToList();
                }
                else
                {
                    toDelete = DataFacade.GetData<IUserGroupPermissionDefinition>()
                        .Where(ugpd => ugpd.UserGroupId == userGroupId && ugpd.SerializedEntityToken == serializedEntityToken).ToList();
                }

                if (toDelete.Any())
                {
                    DataFacade.Delete<IUserGroupPermissionDefinition>(toDelete);
                }

                transactionScope.Complete();
            }
        }



        private static void OnUserGroupPermissionStoreChanged(object sender, StoreEventArgs storeEventArgs)
        {
            if (!storeEventArgs.DataEventsFired)
            {
                _permissionCache.Clear();
            }
        }


        private static void OnUserGroupPermissionChanged(object sender, DataEventArgs args)
        {
            var userGroupPermissionDefinition = args.Data as IUserGroupPermissionDefinition;
            if (userGroupPermissionDefinition == null)
            {
                return;
            }

            _permissionCache.Remove(userGroupPermissionDefinition.UserGroupId);
        }






        internal sealed class DataUserGroupPermissionDefinition : UserGroupPermissionDefinition
        {
            private IUserGroupPermissionDefinition _userGroupPermissionDefinition;
            private List<PermissionType> _permissionTypes;


            public DataUserGroupPermissionDefinition(IUserGroupPermissionDefinition userGroupPermissionDefinition)
            {
                _userGroupPermissionDefinition = userGroupPermissionDefinition;

                Guid permissionDefinitionId = userGroupPermissionDefinition.Id;

                List<string> permissionTypeNames =
                    (from pt in DataFacade.GetData<IUserGroupPermissionDefinitionPermissionType>()
                     where pt.UserGroupPermissionDefinitionId == permissionDefinitionId
                     select pt.PermissionTypeName).ToList();

                _permissionTypes = permissionTypeNames.FromListOfStrings().ToList();
            }


            public override Guid UserGroupId
            {
                get
                {
                    return _userGroupPermissionDefinition.UserGroupId;
                }
            }


            public override IEnumerable<PermissionType> PermissionTypes
            {
                get
                {
                    foreach (PermissionType permissionType in _permissionTypes)
                    {
                        yield return permissionType;
                    }
                }
            }


            public override string SerializedEntityToken
            {
                get
                {
                    return _userGroupPermissionDefinition.SerializedEntityToken;
                }
            }
        }
    }
}
