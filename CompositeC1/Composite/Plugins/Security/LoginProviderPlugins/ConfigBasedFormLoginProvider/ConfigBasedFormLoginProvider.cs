/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Composite.C1Console.Security;
using Composite.C1Console.Security.Plugins.LoginProvider;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ObjectBuilder;


namespace Composite.Plugins.Security.LoginProviderPlugins.ConfigBasedFormLoginProvider
{
    [ConfigurationElementType(typeof(ConfigBasedFormLoginProviderData))]
    internal sealed class ConfigBasedFormLoginProvider : IFormLoginProvider
    {
        private NamedElementCollection<ValidLoginConfigurationElement> _validLogins;

        internal ConfigBasedFormLoginProvider(NamedElementCollection<ValidLoginConfigurationElement> validLogins)
        {
            _validLogins = validLogins;
        }


        public IEnumerable<string> AllUsernames
        {
            get { return _validLogins.Select(f => f.Name); }
        }



        public bool CanSetUserPassword
        {
            get { return false; }
        }



        LoginResult IFormLoginProvider.Validate(string username, string password)
        {
            if (_validLogins.Contains(username))
            {
                ValidLoginConfigurationElement usernameMatch = _validLogins.Get(username);

                return usernameMatch.Password == password ? LoginResult.Success : LoginResult.IncorrectPassword;
            }

            return LoginResult.UserDoesNotExist;
        }


        public void SetUserPassword(string username, string password)
        {
            throw new NotImplementedException();
        }


        public bool CanAddNewUser
        {
            get { return false; }
        }


        public void AddNewUser(string userName, string password, string group, string email)
        {
            throw new NotImplementedException();
        }


        public bool UsersExists
        {
            get { return _validLogins.Any(); }
        }
    }


    [Assembler(typeof(ConfigBasedFormLoginProviderAssembler))]
    internal sealed class ConfigBasedFormLoginProviderData : LoginProviderData
    {
        private const string _validLoginsProperty = "ValidLogins";
        [ConfigurationProperty(_validLoginsProperty, IsRequired = true)]
        public NamedElementCollection<ValidLoginConfigurationElement> ValidLogins
        {
            get
            {
                return (NamedElementCollection<ValidLoginConfigurationElement>)base[_validLoginsProperty];
            }
        }        
    }

    internal sealed class ConfigBasedFormLoginProviderAssembler : IAssembler<ILoginProvider, LoginProviderData>
    {
        public ILoginProvider Assemble(Microsoft.Practices.ObjectBuilder.IBuilderContext context, LoginProviderData objectConfiguration, IConfigurationSource configurationSource, ConfigurationReflectionCache reflectionCache)
        {
            ConfigBasedFormLoginProviderData myConfiguration = (ConfigBasedFormLoginProviderData)objectConfiguration;
            return new ConfigBasedFormLoginProvider(myConfiguration.ValidLogins);
        }
    }

    internal sealed class ValidLoginConfigurationElement : NamedConfigurationElement
    {
        private const string _passwordProperty = "password";
        [ConfigurationProperty(_passwordProperty, IsRequired = true)]
        public string Password
        {
            get { return (string)base[_passwordProperty]; }
            set { base[_passwordProperty] = value; }
        }
    }
}
