/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System.Collections.Generic;
using System.Management;
using Composite.C1Console.Security.Plugins.LoginProvider;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ObjectBuilder;


namespace Composite.Plugins.Security.LoginProviderPlugins.ValidateAllWindowsLoginProvider
{
    [Assembler(typeof(NonConfigurableLoginProviderAssembler))]
    internal sealed class ValidateAllWindowsLoginProviderData : LoginProviderData
    {
    }


    [ConfigurationElementType(typeof(ValidateAllWindowsLoginProviderData))]
    internal class ValidateAllWindowsLoginProvider : IWindowsLoginProvider
    {
        private List<string> _usernames = null;

        public IEnumerable<string> AllUsernames
        {
            get
            {
                if (_usernames == null)
                {
                    SelectQuery selectQuery = new SelectQuery("Win32_UserAccount");
                    ManagementObjectSearcher managementObjectSearcher = new ManagementObjectSearcher(selectQuery);
                    foreach (ManagementObject managementObject in managementObjectSearcher.Get())
                    {
                        _usernames.Add(managementObject["Name"].ToString());
                    }
                }

                return _usernames;
            }
        }



        public bool CanSetUserPassword
        {
            get { return false; }
        }



        public bool CanAddNewUser
        {
            get { return false; }
        }



        bool IWindowsLoginProvider.Validate(string username, string domainName)
        {
            return true;
        }


        public bool UsersExists
        {
            get { return true; }
        }
    }
}
