/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Diagnostics;
using System.Globalization;
using System.Net;
using System.Security.Cryptography;
using System.Web;
using System.Web.Security;
using Composite.C1Console.Security.Plugins.LoginSessionStore;
using Composite.Core.Caching;
using Composite.Core.Extensions;
using Composite.Core.Threading;
using Composite.Core.WebClient;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ObjectBuilder;


namespace Composite.Plugins.Security.LoginSessionStores.HttpContextBasedLoginSessionStore
{
    [ConfigurationElementType(typeof(HttpContextBasedSessionDataProviderData))]
    internal sealed class HttpContextBasedLoginSessionStore : ILoginSessionStore
    {
        private static readonly string ContextKey = typeof(HttpContextBasedLoginSessionStore).FullName + "StoredUsername";

        private const string AuthCookieName = ".CMSAUTH";

        public bool CanPersistAcrossSessions
        {
            get { return true; }
        }

        public void StoreUsername(string userName, bool persistAcrossSessions)
        {
            Verify.ArgumentNotNullOrEmpty(userName, "userName");

            userName = userName.ToLower(CultureInfo.InvariantCulture);

            int daysToLive = (persistAcrossSessions ? 365 : 2);

            var ticket = new FormsAuthenticationTicket(userName, persistAcrossSessions, (int)TimeSpan.FromDays(daysToLive).TotalMinutes);
            string encryptedTicket = FormsAuthentication.Encrypt(ticket);

            var cookie = CookieHandler.SetCookieInternal(AuthCookieName, encryptedTicket);
            cookie.HttpOnly = true;

            var context = HttpContext.Current;
            if (context != null && context.Request.IsSecureConnection)
            {
                cookie.Secure = true;
            }

            if (persistAcrossSessions)
            {
                cookie.Expires = DateTime.Now.AddDays(daysToLive);
            }
        }


        
        public string StoredUsername
        {
            [DebuggerStepThrough]
            get
            {
                HttpContext context = HttpContext.Current;

                if (context == null || !context.RequestIsAvaliable())
                {
                    return null;
                }

                ThreadDataManagerData threadDataManagerData = ThreadDataManager.GetCurrentNotNull();

                object value;
                if (threadDataManagerData.TryGetParentValue(ContextKey, out value) == false)
                {
                    try
                    {
                        value = GetUsernameFromCookie();

                        if (!string.IsNullOrEmpty(value as string))
                        {
                            threadDataManagerData.SetValue(ContextKey, value);
                        }                        
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }

                return value as string;
            }
        }


        [DebuggerStepThrough]
        private static string GetUsernameFromCookie()
        {
            try
            {
                string cookieValue = CookieHandler.Get(AuthCookieName);

                if (!string.IsNullOrEmpty(cookieValue))
                {
                    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookieValue);
                    return ticket.Name;
                }
            }
            catch (CryptographicException)
            {
            }
            return null;
        }


        public void FlushUsername()
        {
            CookieHandler.Set(AuthCookieName, "", DateTime.Now.AddYears(-10));

            string key = typeof(HttpContextBasedLoginSessionStore) + "StoredUsername";
            if (RequestLifetimeCache.HasKey(key))
            {
                RequestLifetimeCache.Remove(key);
            }
        }



        public IPAddress UserIpAddress
        {
            get
            {
                string ipString = HttpContext.Current.Request.UserHostAddress;
                return ipString != null ? IPAddress.Parse(ipString) : null;
            }
        }
    }



    [Assembler(typeof(NonConfigurableSessionDataProviderAssembler))]
    internal sealed class HttpContextBasedSessionDataProviderData : LoginSessionStoreData
    {
    }
}
