/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using Composite.Core.Instrumentation;
using Composite.Core.Instrumentation.Plugin;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;


namespace Composite.Plugins.Instrumentation.PerformanceCounterProviders.NoPerformanceCounterProvider
{
    [ConfigurationElementType(typeof(NonConfigurablePerformanceCounterProvider))]
    internal sealed class NoPerformanceCounterProvider : IPerformanceCounterProvider
    {
        NoPerformanceCounterProviderPerformanceToken _noPerformanceCounterProviderPerformanceToken = new NoPerformanceCounterProviderPerformanceToken();

        public void SystemStartupIncrement()
        {
        }

        public IPerformanceCounterToken BeginElementCreation()
        {
            return _noPerformanceCounterProviderPerformanceToken;
        }

        public void EndElementCreation(IPerformanceCounterToken performanceToken, int resultElementCount, int totalElementCount)
        {
        }

        public IPerformanceCounterToken BeginAspNetControlCompile()
        {
            return _noPerformanceCounterProviderPerformanceToken;
        }

        public void EndAspNetControlCompile(IPerformanceCounterToken performanceToken, int controlsCompiledCount)
        {
        }

        public IPerformanceCounterToken BeginPageHookCreation()
        {
            return _noPerformanceCounterProviderPerformanceToken;
        }

        public void EndPageHookCreation(IPerformanceCounterToken performanceToken, int pageCount)
        {
        }

        public void EntityTokenParentCacheHitIncrement()
        {            
        }

        public void EntityTokenParentCacheMissIncrement()
        {            
        }

        private sealed class NoPerformanceCounterProviderPerformanceToken : IPerformanceCounterToken
        {
            public void Dispose()
            {
            }
        }        
    }
}
