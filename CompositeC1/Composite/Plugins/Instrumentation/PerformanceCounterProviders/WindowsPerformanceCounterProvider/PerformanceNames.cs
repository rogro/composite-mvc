/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/



namespace Composite.Plugins.Instrumentation.PerformanceCounterProviders.WindowsPerformanceCounterProvider
{
	internal static class PerformanceNames
	{
        public static string CategoryName { get { return "Composite C1"; } }
        public static string CategoryDescription { get { return "Composite C1"; } }

        public static string SystemStartupCountName { get { return "SystemStartupCount"; } }
        public static string SystemStartupCountDescription { get { return "SystemStartupCount"; } }
        public static string ElementResultCreationAverageTimeName { get { return "ElementResultCreationAverageTime"; } }
        public static string ElementResultCreationAverageTimeDescription { get { return "ElementResultCreationAverageTime"; } }
        public static string ElementResultCreationAverageTimeBaseName { get { return "ElementResultCreationAverageTimeBase"; } }
        public static string ElementResultCreationAverageTimeBaseDescription { get { return "ElementResultCreationAverageTimeBase"; } }
        public static string ElementTotalCreationAverageTimeName { get { return "ElementTotalCreationAverageTime"; } }
        public static string ElementTotalCreationAverageTimeDescription { get { return "ElementTotalCreationAverageTime"; } }
        public static string ElementTotalCreationAverageTimeBaseName { get { return "ElementTotalCreationAverageTimeBase"; } }
        public static string ElementTotalCreationAverageTimeBaseDescription { get { return "ElementTotalCreationAverageTimeBase"; } }
        public static string AspNetControlCompileAverageTimeName { get { return "AspNetControlCompileAverageTime"; } }
        public static string AspNetControlCompileAverageTimeDescription { get { return "AspNetControlCompileAverageTime"; } }
        public static string AspNetControlCompileAverageTimeBaseName { get { return "AspNetControlCompileAverageAverageTimeBase"; } }
        public static string AspNetControlCompileAverageTimeBaseDescription { get { return "AspNetControlCompileAverageAverageTimeBase"; } }
        public static string PageHookCreationAverageTimeName { get { return "PageHookCreationAverageTime"; } }
        public static string PageHookCreationAverageTimeDescription { get { return "PageHookCreationAverageTime"; } }
        public static string PageHookCreationAverageTimeBaseName { get { return "PageHookCreationAverageAverageTimeBase"; } }
        public static string PageHookCreationAverageTimeBaseDescription { get { return "PageHookCreationAverageAverageTimeBase"; } }
        public static string EntityTokenParentCacheHitCountName { get { return "EntityTokenParentCacheHitCount"; } }
        public static string EntityTokenParentCacheHitCountDescription { get { return "EntityTokenParentCacheHitCount"; } }
        public static string EntityTokenParentCacheMissCountName { get { return "EntityTokenParentCacheMissCount"; } }
        public static string EntityTokenParentCacheMissCountDescription { get { return "EntityTokenParentCacheMissCount"; } }
	}
}
