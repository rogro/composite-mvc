/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Manageability;


namespace Composite.Plugins.Instrumentation.PerformanceCounterProviders.WindowsPerformanceCounterProvider
{
    /// <summary>
    /// Use Installutil.exe to install performance counters
    /// </summary>
    [RunInstallerAttribute(true)]
    internal sealed class PerformanceCounterInstaller : Installer
	{
        public override void Install(IDictionary stateSaver)
        {
            base.Install(stateSaver);

            if (PerformanceCounterCategory.Exists(PerformanceNames.CategoryName)) throw new InvalidOperationException("Already installed");

            CounterCreationDataCollection collection = new CounterCreationDataCollection();
            collection.Add(new CounterCreationData(PerformanceNames.SystemStartupCountName, PerformanceNames.SystemStartupCountDescription, PerformanceCounterType.NumberOfItems32));
            collection.Add(new CounterCreationData(PerformanceNames.ElementResultCreationAverageTimeName, PerformanceNames.ElementResultCreationAverageTimeDescription, PerformanceCounterType.AverageTimer32));
            collection.Add(new CounterCreationData(PerformanceNames.ElementResultCreationAverageTimeBaseName, PerformanceNames.ElementResultCreationAverageTimeBaseDescription, PerformanceCounterType.AverageBase));
            collection.Add(new CounterCreationData(PerformanceNames.ElementTotalCreationAverageTimeName, PerformanceNames.ElementTotalCreationAverageTimeDescription, PerformanceCounterType.AverageTimer32));
            collection.Add(new CounterCreationData(PerformanceNames.ElementTotalCreationAverageTimeBaseName, PerformanceNames.ElementTotalCreationAverageTimeBaseDescription, PerformanceCounterType.AverageBase));
            collection.Add(new CounterCreationData(PerformanceNames.AspNetControlCompileAverageTimeName, PerformanceNames.AspNetControlCompileAverageTimeDescription, PerformanceCounterType.AverageTimer32));
            collection.Add(new CounterCreationData(PerformanceNames.AspNetControlCompileAverageTimeBaseName, PerformanceNames.AspNetControlCompileAverageTimeBaseDescription, PerformanceCounterType.AverageBase));
            collection.Add(new CounterCreationData(PerformanceNames.PageHookCreationAverageTimeName, PerformanceNames.PageHookCreationAverageTimeDescription, PerformanceCounterType.AverageCount64));
            collection.Add(new CounterCreationData(PerformanceNames.PageHookCreationAverageTimeBaseName, PerformanceNames.PageHookCreationAverageTimeBaseDescription, PerformanceCounterType.AverageBase));
            collection.Add(new CounterCreationData(PerformanceNames.EntityTokenParentCacheHitCountName, PerformanceNames.EntityTokenParentCacheHitCountDescription, PerformanceCounterType.NumberOfItems32));
            collection.Add(new CounterCreationData(PerformanceNames.EntityTokenParentCacheMissCountName, PerformanceNames.EntityTokenParentCacheMissCountDescription, PerformanceCounterType.NumberOfItems32));

            PerformanceCounterCategory.Create(PerformanceNames.CategoryName, PerformanceNames.CategoryDescription, PerformanceCounterCategoryType.MultiInstance, collection);
        }



        public override void Uninstall(IDictionary savedState)
        {
            base.Uninstall(savedState);

            if (PerformanceCounterCategory.Exists(PerformanceNames.CategoryName))
            {
                PerformanceCounterCategory.Delete(PerformanceNames.CategoryName);
            }
        }
	}
}
