/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Web.UI;
using System;
using System.Web;


namespace Composite.Plugins.Forms.WebChannel.Foundation
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    public static class UserControlUtils
    {
        /// <exclude />
        public static TBase ActivateAsUserControl<TBase>(this Type userControlType, string uniqueUserControlId)
            where TBase : UserControl
        {
            if (userControlType == null) throw new ArgumentNullException("userControlType");
            if (typeof(TBase).IsAssignableFrom(userControlType) == false) throw new ArgumentException("The specified type '" + userControlType.FullName + "' must inherit from generic argument " + typeof(TBase).FullName, "userControlType");

            Page currentPage = HttpContext.Current.Handler as Page;
            if (currentPage == null) throw new InvalidOperationException("The Current HttpContext Handler must be a System.Web.Ui.Page");

            Control templateControl = currentPage.LoadControl(userControlType, null);

            templateControl.ID = uniqueUserControlId;

            return (TBase)templateControl;
        }
    }

}