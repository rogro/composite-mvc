/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Configuration;
using System.Web.UI;
using Composite.C1Console.Forms;
using Composite.C1Console.Forms.CoreUiControls;
using Composite.C1Console.Forms.Plugins.UiControlFactory;
using Composite.C1Console.Forms.WebChannel;
using Composite.Plugins.Forms.WebChannel.Foundation;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ObjectBuilder;
using Microsoft.Practices.ObjectBuilder;


namespace Composite.Plugins.Forms.WebChannel.UiControlFactories
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public abstract class FunctionParameterDesignerTemplateUserControlBase : UserControl
    {
        /// <exclude />
        public abstract string SessionStateProvider { get; set; }

        /// <exclude />
        public abstract Guid SessionStateId { get; set; }
    }



    internal sealed class TemplatedFunctionParameterDesignerUiControl : FunctionParameterDesignerUiControl, IWebUiControl
    {
        private Type _userControlType;
        private FunctionParameterDesignerTemplateUserControlBase _userControl;
        private string _sessionStateProvider;
        private Guid _sessionStateId;

        internal TemplatedFunctionParameterDesignerUiControl(Type userControlType)
        {
            _userControlType = userControlType;
        }

        public void InitializeViewState()
        {
        }


        public Control BuildWebControl()
        {
            _userControl = _userControlType.ActivateAsUserControl<FunctionParameterDesignerTemplateUserControlBase>(this.UiControlID);
            _userControl.SessionStateProvider = _sessionStateProvider;
            _userControl.SessionStateId = _sessionStateId;

            return _userControl;
        }

        public override string SessionStateProvider
        {
            get { return _sessionStateProvider; }
            set { _sessionStateProvider = value; }
        }

        public override Guid SessionStateId
        {
            get { return _sessionStateId; }
            set { _sessionStateId = value; }
        }

        public bool IsFullWidthControl { get { return true; } }

        public string ClientName { get { return null; /* _userControl.GetDataFieldClientName(); */ } }
    }


    [ConfigurationElementType(typeof(TemplatedFunctionParameterDesignerUiControlFactoryData))]
    internal sealed class TemplatedFunctionParameterDesignerUiControlFactory : Base.BaseTemplatedUiControlFactory
    {
        public TemplatedFunctionParameterDesignerUiControlFactory(TemplatedFunctionParameterDesignerUiControlFactoryData data)
            : base(data)
        { }

        public override IUiControl CreateControl()
        {
            return new TemplatedFunctionParameterDesignerUiControl(this.UserControlType);
        }
    }


    [Assembler(typeof(TemplatedFunctionParameterDesignerUiControlFactoryAssembler))]
    internal sealed class TemplatedFunctionParameterDesignerUiControlFactoryData : UiControlFactoryData, Base.ITemplatedUiControlFactoryData
    {
        private const string _userControlVirtualPathPropertyName = "userControlVirtualPath";
        private const string _cacheCompiledUserControlTypePropertyName = "cacheCompiledUserControlType";

        [ConfigurationProperty(_userControlVirtualPathPropertyName, IsRequired = true)]
        public string UserControlVirtualPath
        {
            get { return (string)base[_userControlVirtualPathPropertyName]; }
            set { base[_userControlVirtualPathPropertyName] = value; }
        }

        [ConfigurationProperty(_cacheCompiledUserControlTypePropertyName, IsRequired = false, DefaultValue = true)]
        public bool CacheCompiledUserControlType
        {
            get { return (bool)base[_cacheCompiledUserControlTypePropertyName]; }
            set { base[_cacheCompiledUserControlTypePropertyName] = value; }
        }
    }


    internal sealed class TemplatedFunctionParameterDesignerUiControlFactoryAssembler : IAssembler<IUiControlFactory, UiControlFactoryData>
    {
        public IUiControlFactory Assemble(IBuilderContext context, UiControlFactoryData objectConfiguration, IConfigurationSource configurationSource, ConfigurationReflectionCache reflectionCache)
        {
            return new TemplatedFunctionParameterDesignerUiControlFactory(objectConfiguration as TemplatedFunctionParameterDesignerUiControlFactoryData);
        }
    }


}
