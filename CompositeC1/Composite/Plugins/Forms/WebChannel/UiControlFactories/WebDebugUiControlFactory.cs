/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System.Web.UI;
using System.Web.UI.WebControls;
using Composite.C1Console.Forms;
using Composite.C1Console.Forms.CoreUiControls;
using Composite.C1Console.Forms.Plugins.UiControlFactory;
using Composite.C1Console.Forms.WebChannel;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ObjectBuilder;
using Microsoft.Practices.ObjectBuilder;


namespace Composite.Plugins.Forms.WebChannel.UiControlFactories
{
    internal sealed class WebDebugUiControl : DebugUiControl, IWebUiControl
    {
        private Panel _panel;

        public Control BuildWebControl()
        {
            _panel = new Panel();

            _panel.CssClass = "debugPanel";

            Label xpathLabel = new Label();
            xpathLabel.Text = string.Format("XPath: {0}", this.SourceElementXPath);
            xpathLabel.CssClass = "debugPanelHeader";
            AddControl(xpathLabel);

            Control control = (this.UiControl as IWebUiControl).BuildWebControl();

            AddControl(control);


            return _panel;
        }

        public void InitializeViewState()
        {
            (this.UiControl as IWebUiControl).InitializeViewState();
        }

        private void AddControl(Control control)
        {
            _panel.Controls.Add(control);

        }

        public override void BindStateToControlProperties()
        {
            this.UiControl.BindStateToControlProperties();
        }

        public bool IsFullWidthControl { get { return false; } }

        public string ClientName { get { return null; } }
    }


    [ConfigurationElementType(typeof(WebDebugUiControlFactoryData))]
    internal sealed class WebDebugUiControlFactory : IUiControlFactory
    {
        public IUiControl CreateControl()
        {
            return new WebDebugUiControl();
        }
    }


    [Assembler(typeof(WebDebugUiControlFactoryAssembler))]
    internal sealed class WebDebugUiControlFactoryData : DebugUiControlFactoryData
    {
    }


    internal sealed class WebDebugUiControlFactoryAssembler : IAssembler<IUiControlFactory, UiControlFactoryData>
    {
        public IUiControlFactory Assemble(IBuilderContext context, UiControlFactoryData objectConfiguration, IConfigurationSource configurationSource, ConfigurationReflectionCache reflectionCache)
        {
            return new WebDebugUiControlFactory();
        }
    }
}
