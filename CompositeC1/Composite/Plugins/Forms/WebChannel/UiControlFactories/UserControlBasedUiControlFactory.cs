/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using Composite.C1Console.Forms;
using Composite.C1Console.Forms.Plugins.UiControlFactory;
using Composite.C1Console.Forms.WebChannel;
using Composite.Plugins.Forms.WebChannel.Foundation;
using Composite.Data.Validation.ClientValidationRules;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ObjectBuilder;
using Microsoft.Practices.ObjectBuilder;


namespace Composite.Plugins.Forms.WebChannel.UiControlFactories
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    public abstract class UserControlBasedUiControl : UserControl, IWebUiControl, IValidatingUiControl
    {
        private string _uiControlId = "IdNotSet";


        /// <exclude />
        public virtual Control BuildWebControl()
        {
            return this;
        }


        /// <exclude />
        public abstract void InitializeViewState();


        /// <exclude />
        public abstract void BindStateToControlProperties();


        /// <exclude />
        public string ClientName { get; set; }


        /// <exclude />
        public bool IsFullWidthControl { get; set; }


        /// <exclude />
        public string UiControlID 
        {
            get
            {
                return _uiControlId;
            }
            set
            {
                _uiControlId = value;
                this.ID = _uiControlId;
            }
        }


        /// <exclude />
        public IFormChannelIdentifier UiControlChannel { get; set; }

        /// <exclude />
        public string Label { get; set; }

        /// <exclude />
        public string Help { get; set; }

        /// <exclude />
        public List<ClientValidationRule> ClientValidationRules { get; set; }

        /// <exclude />
        public List<string> SourceBindingPaths { get; set; }

        /// <exclude />
        public virtual bool IsValid { get; set; }

        /// <exclude />
        public virtual string ValidationError { get; set; }
    }


    [ConfigurationElementType(typeof(UserControlBasedUiControlFactoryData))]
    internal sealed class UserControlBasedUiControlFactory : Base.BaseTemplatedUiControlFactory
    {
        public UserControlBasedUiControlFactory(UserControlBasedUiControlFactoryData data)
            : base(data)
        { }

        public override IUiControl CreateControl()
        {
            UserControlBasedUiControl userControlBasedUiControl = this.UserControlType.ActivateAsUserControl<UserControlBasedUiControl>(null);
            userControlBasedUiControl.IsValid = true;

            return userControlBasedUiControl;
        }
    }


    [Assembler(typeof(UserControlBasedUiControlFactoryAssembler))]
    internal sealed class UserControlBasedUiControlFactoryData : UiControlFactoryData, Base.ITemplatedUiControlFactoryData
    {
        private const string _userControlVirtualPathPropertyName = "userControlVirtualPath";
        private const string _cacheCompiledUserControlTypePropertyName = "cacheCompiledUserControlType";

        [ConfigurationProperty(_userControlVirtualPathPropertyName, IsRequired = true)]
        public string UserControlVirtualPath
        {
            get { return (string)base[_userControlVirtualPathPropertyName]; }
            set { base[_userControlVirtualPathPropertyName] = value; }
        }

        [ConfigurationProperty(_cacheCompiledUserControlTypePropertyName, IsRequired = false, DefaultValue = true)]
        public bool CacheCompiledUserControlType
        {
            get { return (bool)base[_cacheCompiledUserControlTypePropertyName]; }
            set { base[_cacheCompiledUserControlTypePropertyName] = value; }
        }
    }

    internal sealed class UserControlBasedUiControlFactoryAssembler : IAssembler<IUiControlFactory, UiControlFactoryData>
    {
        public IUiControlFactory Assemble(IBuilderContext context, UiControlFactoryData objectConfiguration, IConfigurationSource configurationSource, ConfigurationReflectionCache reflectionCache)
        {
            return new UserControlBasedUiControlFactory(objectConfiguration as UserControlBasedUiControlFactoryData);
        }
    }
}
