/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Composite.C1Console.Forms;
using Composite.Core.Logging;
using Composite.C1Console.Forms.Flows.Plugins.UiContainerFactory;
using Composite.C1Console.Forms.Flows;
using Composite.Core.WebClient;

namespace Composite.Plugins.Forms.WebChannel.UiContainerFactories.Base
{
    internal abstract class BaseTemplatedUiContainerFactory : IUiContainerFactory
    {
        private ITemplatedUiContainerFactoryData _data;
        private Type _cachedUserControlType = null;
        private object _lock = new object();

        protected BaseTemplatedUiContainerFactory(ITemplatedUiContainerFactoryData data)
        {
            _data = data;
        }


        protected Type UserControlType
        {
            get
            {
                return this.CachedUserControlType;
            }
        }



        private Type CachedUserControlType
        {
            get
            {
                lock (_lock)
                {
                    if (_cachedUserControlType == null || _data.CacheCompiledUserControlType == false)
                    {
                        using (DebugLoggingScope.CompletionTime(this.GetType(), string.Format("getting compiled '{0}'", _data.UserControlVirtualPath, TimeSpan.FromMilliseconds(25))))
                        {
                            _cachedUserControlType = BuildManagerHelper.GetCompiledType(_data.UserControlVirtualPath);
                        }
                    }

                    return _cachedUserControlType;
                }
            }
        }

        public abstract IUiContainer CreateContainer();
    }


}
