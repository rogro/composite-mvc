/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Web.UI;

using Composite.C1Console.Forms;
using Composite.C1Console.Forms.Foundation;
using Composite.C1Console.Forms.WebChannel;
using Composite.Plugins.Forms.WebChannel.Foundation;
using Composite.Plugins.Forms.WebChannel.UiControlFactories;
using Composite.Core.ResourceSystem;


namespace Composite.Plugins.Forms.WebChannel.UiContainerFactories
{
    internal class TemplatedUiContainer : IWebUiContainer
    {
        private Type _templateUserControlType;
        private string _templateFormVirtualPath;
        private TemplatedExecutionContainer _webDocument;

        internal TemplatedUiContainer(Type templateUserControlType, string templateFormVirtualPath)
        {
            _templateUserControlType = templateUserControlType;
            _templateFormVirtualPath = templateFormVirtualPath;
        }


        public IUiControl Render(
            IUiControl innerForm, 
            IUiControl customToolbarItems, 
            IFormChannelIdentifier channel, 
            IDictionary<string, object> eventHandlerBindings, 
            string containerLabel, 
            string containerLabelField, 
            ResourceHandle containerIcon)
        {
            if (string.IsNullOrEmpty(_templateFormVirtualPath) == false)
            {
                WebEmbeddedFormUiControl document = new WebEmbeddedFormUiControl(channel);
                document.FormPath = _templateFormVirtualPath; // "/Composite/Templates/Document.xml";

                document.Bindings = new Dictionary<string, object>(eventHandlerBindings);
                document.Bindings.Add("Form", innerForm);
                if (customToolbarItems != null)
                {
                    document.Bindings.Add("CustomToolbarItems", customToolbarItems);
                }

                _webDocument = new TemplatedExecutionContainer(document, _templateUserControlType, containerLabel, containerLabelField, containerIcon);
            }
            else
            {
                _webDocument = new TemplatedExecutionContainer((IWebUiControl)innerForm, _templateUserControlType, containerLabel, containerLabelField, containerIcon);
            }

            return _webDocument;
        }


        public void ShowFieldMessages(Dictionary<string, string> clientIDPathedMessages)
        {
            _webDocument.ShowFieldMessages( clientIDPathedMessages);
        }

    }




    internal class TemplatedExecutionContainer : UiControl, IWebUiControl
    {
        readonly IWebUiControl _form;
        readonly Type _templateUserControlType;
        readonly string _containerLabel;
        readonly string _containerLabelField;
        readonly ResourceHandle _containerIcon;

        Control _messagePlaceHolder;
        TemplatedUiContainerBase _templateControl;


        public TemplatedExecutionContainer(IWebUiControl form, Type templateUserControlType, string containerLabel, string containerLabelField, ResourceHandle containerIcon)
        {
            _form = form;
            _templateUserControlType = templateUserControlType;
            _containerLabel = containerLabel;
            _containerLabelField = containerLabelField;
            _containerIcon = containerIcon;
        }

        internal Control GetMessagePlaceHolder()
        {
            if (_messagePlaceHolder == null) throw new InvalidOperationException("MessagePlaceHolder has not been initialized. The place holder will not exist before BuildWebControl() has been called");
            return _messagePlaceHolder;
        }


        public override void BindStateToControlProperties()
        {
            _form.BindStateToControlProperties();
        }


        public Control BuildWebControl()
        {
            _templateControl = _templateUserControlType.ActivateAsUserControl<TemplatedUiContainerBase>(this.UiControlID);

            Control formPlaceHolder = _templateControl.GetFormPlaceHolder();
            _messagePlaceHolder = _templateControl.GetMessagePlaceHolder();
            _templateControl.SetContainerTitle(_containerLabel);
            _templateControl.SetContainerTitleField(_containerLabelField);
            _templateControl.SetContainerIcon(_containerIcon);
            _templateControl.SetWebUiControlRef(_form);

            formPlaceHolder.Controls.Add(_form.BuildWebControl());

            return _templateControl;
        }


        public void InitializeViewState()
        {
            _form.InitializeViewState();
        }


        public bool IsFullWidthControl
        {
            get { throw new Exception("I am the root page!"); }
        }

        public string ClientName { get { return null; } }


        internal void ShowFieldMessages(Dictionary<string, string> clientIDPathedMessages)
        {
            _templateControl.ShowFieldMessages(clientIDPathedMessages);;
        }
    }

}
