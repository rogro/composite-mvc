/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System.Configuration;
using Composite.C1Console.Forms.Flows;
using Composite.C1Console.Forms.Flows.Plugins.UiContainerFactory;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ObjectBuilder;
using Microsoft.Practices.ObjectBuilder;


namespace Composite.Plugins.Forms.WebChannel.UiContainerFactories
{
    [ConfigurationElementType(typeof(TemplatedUiContainerFactoryData))]
    internal sealed class TemplatedUiContainerFactory : Base.BaseTemplatedUiContainerFactory
    {
        private TemplatedUiContainerFactoryData _data;

        public TemplatedUiContainerFactory(TemplatedUiContainerFactoryData data)
            : base(data)
        {
            _data = data;
        }


        public override IUiContainer CreateContainer()
        {
            return new TemplatedUiContainer(this.UserControlType, _data.TemplateFormVirtualPath);
        }
    }


    [Assembler(typeof(TemplatedUiContainerFactoryAssembler))]
    internal sealed class TemplatedUiContainerFactoryData : UiContainerFactoryData, Base.ITemplatedUiContainerFactoryData
    {
        private const string _cacheCompiledUserControlTypePropertyName = "cacheCompiledUserControlType";

        [ConfigurationProperty(_cacheCompiledUserControlTypePropertyName, IsRequired = false, DefaultValue = true)]
        public bool CacheCompiledUserControlType
        {
            get { return (bool)base[_cacheCompiledUserControlTypePropertyName]; }
            set { base[_cacheCompiledUserControlTypePropertyName] = value; }
        }
    }


    internal sealed class TemplatedUiContainerFactoryAssembler : IAssembler<IUiContainerFactory, UiContainerFactoryData>
    {
        public IUiContainerFactory Assemble(IBuilderContext context, UiContainerFactoryData objectConfiguration, IConfigurationSource configurationSource, ConfigurationReflectionCache reflectionCache)
        {
            return new TemplatedUiContainerFactory(objectConfiguration as TemplatedUiContainerFactoryData);
        }
    }
}
