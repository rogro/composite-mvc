/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Web.UI;
using System.Collections.Generic;
using Composite.C1Console.Forms.DataServices.UiControls;
using Composite.C1Console.Forms.WebChannel;
using Composite.Core.Extensions;
using Composite.Core.ResourceSystem;
using Composite.Core.WebClient.FlowMediators.FormFlowRendering;
using Composite.Plugins.Forms.WebChannel.UiControlFactories;


namespace Composite.Plugins.Forms.WebChannel.UiContainerFactories
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public abstract class TemplatedUiContainerBase : UserControl
    {
        private IWebUiControl _webUiControl;
        private string _titleField;

        /// <exclude />
        public abstract Control GetFormPlaceHolder();
        
        /// <exclude />
        public abstract Control GetMessagePlaceHolder();

        /// <exclude />
        public abstract void SetContainerTitle(string title);

        /// <exclude />
        public void SetContainerTitleField(string titleField)
        {
            _titleField = titleField;
        }

        /// <exclude />
        public string GetTitleFieldControlId()
        {
            IWebUiControl container = GetContainer();

            if (_titleField.IsNullOrEmpty() || container == null) return string.Empty;

            var mappings = new Dictionary<string, string>();

            FormFlowUiDefinitionRenderer.ResolveBindingPathToCliendIDMappings(container, mappings);

            string cliendId = mappings.ContainsKey(_titleField) ? mappings[_titleField] : "";

            return cliendId;
        }

        /// <exclude />
        public abstract void SetContainerIcon(ResourceHandle icon);


        internal void SetWebUiControlRef(IWebUiControl webUiControl)
        {
            _webUiControl = webUiControl;
        }


        /// <exclude />
        protected override void OnInit(System.EventArgs e)
        {
            base.OnInit(e);

            if (!this.IsPostBack)
            {
                _webUiControl.InitializeViewState();
                return;
            }
            
            if (_webUiControl is EmbeddedFormUiControl)
            {
                var container = GetContainer();
                if(container != null)
                {
                    container.InitializeLazyBindedControls();
                }
            }
        }

        private TemplatedContainerUiControl GetContainer()
        {
            var container = (_webUiControl as EmbeddedFormUiControl).CompiledUiControl as TemplatedContainerUiControl;
            return container;
        }

        /// <exclude />
        public abstract void ShowFieldMessages(Dictionary<string, string> clientIDPathedMessages);
    }
}
