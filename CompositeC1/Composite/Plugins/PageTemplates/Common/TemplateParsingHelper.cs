/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Text;
using Composite.Core.Extensions;
using Composite.Core.IO;

namespace Composite.Plugins.PageTemplates.Common
{
    internal static class TemplateParsingHelper
    {
        public static bool TryExtractTemplateIdFromCSharpCode(string filePath, out Guid templateId, string idTokenBegin, string idTokenEnd)
        {
            templateId = Guid.Empty;

            if (!C1File.Exists(filePath)) return false;

            var allText = C1File.ReadAllText(filePath, Encoding.UTF8);

            allText = RemoveWhiteSpaces(allText);

            string beginning = RemoveWhiteSpaces(idTokenBegin);
            string ending = RemoveWhiteSpaces(idTokenEnd);

            int firstIndex = allText.IndexOf(beginning, StringComparison.Ordinal);
            if (firstIndex < 0) return false;

            int lastIndex = allText.LastIndexOf(beginning, StringComparison.Ordinal);
            if (lastIndex != firstIndex) return false;

            int endOffset = allText.IndexOf(ending, firstIndex, StringComparison.Ordinal);
            if (endOffset < 0) return false;

            int guidOffset = firstIndex + beginning.Length;
            string guidStr = allText.Substring(guidOffset, endOffset - guidOffset);

            return Guid.TryParse(guidStr, out templateId);
        }


        private static string RemoveWhiteSpaces(string str)
        {
            var whiteSpaces = new[] { ' ', '\t', '\r', '\n' };

            whiteSpaces.ForEach(ch => str = str.Replace(new string(ch, 1), ""));

            return str;
        }
    }
}
