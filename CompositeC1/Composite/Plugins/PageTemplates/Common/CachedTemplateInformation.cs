/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Composite.Core.IO;

namespace Composite.Plugins.PageTemplates.Common
{
    internal class CachedTemplateInformation
    {
        private CachedTemplateInformation() {}

        public CachedTemplateInformation(Core.PageTemplates.PageTemplateDescriptor parsedTemplate)
        {
            TemplateId = parsedTemplate.Id;
            Title = parsedTemplate.Title;
        }

        public Guid TemplateId { get; set; }
        public string Title { get; set; }

        public static void SerializeToFile(CachedTemplateInformation pageTemplate, string filePath)
        {
            var lines = new List<string>();

            if (pageTemplate != null)
            {
                lines.Add(pageTemplate.TemplateId.ToString());
                lines.Add(pageTemplate.Title);
            }

            C1File.WriteAllLines(filePath, lines);
        }

        public static CachedTemplateInformation DeserializeFromFile(string filePath)
        {
            var lines = C1File.ReadAllLines(filePath);
            if (lines == null || lines.Length == 0)
            {
                return null;
            }
                
            Guid templateId = Guid.Parse(lines[0]);
            string title = string.Join(Environment.NewLine, lines.Skip(1));

            return new CachedTemplateInformation { TemplateId = templateId, Title = title };
        }
    }
        
}
