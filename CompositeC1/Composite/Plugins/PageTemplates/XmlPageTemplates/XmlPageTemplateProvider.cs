/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Composite.C1Console.Elements;
using Composite.Core.PageTemplates;
using Composite.Core.WebClient.Renderings.Template;
using Composite.Data;
using Composite.Data.Types;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

using SR = Composite.Core.ResourceSystem.StringResourceSystemFacade;

namespace Composite.Plugins.PageTemplates.XmlPageTemplates
{
    [ConfigurationElementType(typeof(XmlPageTemplateProviderData))]
    internal class XmlPageTemplateProvider : IPageTemplateProvider
    {
        public string AddNewTemplateLabel
        {
            get; private set;
        }

        public Type AddNewTemplateWorkflow
        {
            get; private set;
        }

        public XmlPageTemplateProvider(string providerName, string addNewTemplateLabel, Type addNewTemplateWorkflow)
        {
            AddNewTemplateLabel = addNewTemplateLabel;
            AddNewTemplateWorkflow = addNewTemplateWorkflow;
        }

        public IEnumerable<PageTemplateDescriptor> GetPageTemplates()
        {
            using (var conn = new DataConnection(PublicationScope.Published))
            {
                var result = new List<PageTemplateDescriptor>();
                
                foreach (var xmlPageTemplate in conn.Get<IXmlPageTemplate>())
                {
                    string defaultPlaceholderId;
                    PlaceholderDescriptor[] placeholders;

                    ParseLayoutFile(xmlPageTemplate, out placeholders, out defaultPlaceholderId);

                    PageTemplateDescriptor descriptor = new XmlPageTemplateDescriptor(xmlPageTemplate)
                    {
                        Id = xmlPageTemplate.Id,
                        Title = xmlPageTemplate.Title,
                        DefaultPlaceholderId = defaultPlaceholderId,
                        PlaceholderDescriptions = placeholders
                    };
                
                    result.Add(descriptor);
                }
                
                return result;
            }
        }


        private static void ParseLayoutFile(IXmlPageTemplate pageTemplate, out PlaceholderDescriptor[] placeholders, out string defaultPlaceholder)
        {
            var placeholdersInfo = TemplateInfo.GetRenderingPlaceHolders(pageTemplate.Id);

            defaultPlaceholder = placeholdersInfo.DefaultPlaceholderId;

            placeholders = placeholdersInfo
                           .Placeholders
                           .Select(pair => new PlaceholderDescriptor { Id = pair.Key, Title = pair.Value })
                           .ToArray();
        }

        public IPageRenderer BuildPageRenderer(Guid templateId)
        {
            return new XmlPageRenderer();
        }



        public IEnumerable<ElementAction> GetRootActions()
        {
            return new ElementAction[0];
        }

        public void FlushTemplates()
        {
            // Provider holds no state - no need to reinitialize anything
        }
    }
}
