/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.WebPages;
using Composite.AspNet.Razor;
using Composite.Core.PageTemplates;

namespace Composite.Plugins.PageTemplates.Razor
{
    internal class TemplateRenderingInfo
    {
        public TemplateRenderingInfo(string controlVirtualPath, IDictionary<string, PropertyInfo> placeholders)
        {
            ControlVirtualPath = controlVirtualPath;
            PlaceholderProperties = placeholders;
        }

        public string ControlVirtualPath { get; set; }
        public virtual IDictionary<string, PropertyInfo> PlaceholderProperties { get; private set; }
    }

    internal class LazyInitializedTemplateRenderingInfo : TemplateRenderingInfo
    {
        private IDictionary<string, PropertyInfo> _placeholderProperties;
        private readonly RazorPageTemplateProvider _provider;

        public LazyInitializedTemplateRenderingInfo(string controlVirtualPath, RazorPageTemplateProvider provider)
            :base(controlVirtualPath, null)
        {
            _provider = provider;
        }

        public override IDictionary<string, PropertyInfo> PlaceholderProperties
        {
            get
            {
                if (_placeholderProperties == null)
                {
                    lock (this)
                    {
                        if (_placeholderProperties == null)
                        {
                            _placeholderProperties = InitializePlaceholderProperties();
                        }
                    }
                }
                return _placeholderProperties;
            }
        }

        private IDictionary<string, PropertyInfo> InitializePlaceholderProperties()
        {
            WebPageBase webPage;
            PageTemplateDescriptor parsedTemplate;
            IDictionary<string, PropertyInfo> placeholderProperties;
            Exception loadingException;

            if (!_provider.LoadRazorTemplate(ControlVirtualPath, out webPage, out parsedTemplate, out placeholderProperties, out loadingException))
            {
                throw loadingException;
            }

            Verify.IsNotNull(webPage, "Failed to compile razor page template '{0}'", ControlVirtualPath);
            Verify.That(webPage is RazorPageTemplate, "Incorrect base class. '{0}'", ControlVirtualPath);

            return placeholderProperties;
        }
    }
}
