/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Linq;
using Composite.C1Console.Security;
using Composite.Core.Collections.Generic;
using Composite.Core.Extensions;
using Composite.Core.PageTemplates;
using Composite.Core.WebClient.Renderings.Page;
using Composite.Core.Xml;
using Composite.Data.Types;


namespace Composite.Plugins.PageTemplates.MasterPages
{
    internal class MasterPagePageRenderer: IPageRenderer
    {
        private static readonly string PageRenderingJob_Key = "MasterPages.PageRenderingJob";

        private readonly Hashtable<Guid, MasterPageRenderingInfo> _renderingInfo;
        private readonly Hashtable<Guid, Exception> _loadingExceptions;

        public MasterPagePageRenderer(
            Hashtable<Guid, MasterPageRenderingInfo> renderingInfo,
            Hashtable<Guid, Exception> loadingExceptions)
        {
            Verify.ArgumentNotNull(renderingInfo, "renderingInfo");

            _renderingInfo = renderingInfo;
            _loadingExceptions = loadingExceptions;
        }

        public void AttachToPage(Page aspnetPage, PageContentToRender contentToRender)
        {
            Verify.ArgumentNotNull(aspnetPage, "aspnetPage");
            Verify.ArgumentNotNull(contentToRender, "contentToRender");

            aspnetPage.Items.Add(PageRenderingJob_Key, contentToRender);

            Guid templateId = contentToRender.Page.TemplateId;
            var rendering = _renderingInfo[templateId];

            if(rendering == null)
            {
                Exception loadingException = _loadingExceptions[templateId];
                if(loadingException != null)
                {
                    throw loadingException;
                }

                Verify.ThrowInvalidOperationException("Failed to get master page by template ID '{0}'. Check for compilation errors".FormatWith(templateId));
            }

            aspnetPage.MasterPageFile = rendering.VirtualPath;
            aspnetPage.PreRender += (e, args) => PageOnPreRender(aspnetPage, contentToRender.Page);

            var master = aspnetPage.Master as MasterPagePageTemplate;
            TemplateDefinitionHelper.BindPlaceholders(master, contentToRender, rendering.PlaceholderProperties, null);
        }

        private void PageOnPreRender(Page aspnetPage, IPage page)
        {
            bool emitMenuTitleMetaTag = !page.MenuTitle.IsNullOrEmpty();
            bool emitUrlTitleMetaTag = !page.UrlTitle.IsNullOrEmpty();

            if ((emitMenuTitleMetaTag || emitUrlTitleMetaTag)
                && UserValidationFacade.IsLoggedIn())
            {
                var xhtmlDocument = new XhtmlDocument();

                PageRenderer.AppendC1MetaTags(page, xhtmlDocument);


                if (aspnetPage.Header != null)
                {
                    string xml = string.Join(string.Empty, xhtmlDocument.Head.Nodes().Select(node => node.ToString()));

                    aspnetPage.Header.Controls.Add(new Literal {Text = xml});
                }
            }
        }

        public static PageContentToRender GetRenderingInfo(Page page)
        {
            return page.Items[PageRenderingJob_Key] as PageContentToRender;
        }
    }
}
