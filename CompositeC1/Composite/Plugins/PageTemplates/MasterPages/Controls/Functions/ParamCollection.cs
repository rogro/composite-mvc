/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections;

namespace Composite.Plugins.PageTemplates.MasterPages.Controls.Functions
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public class ParamCollection : IList, ICollection, IEnumerable
    {
        private readonly ArrayList _innerList = new ArrayList();

        /// <exclude />
        public void Add(Param value)
        {
            _innerList.Add(value);
        }

        /// <exclude />
        public void Clear()
        {
            _innerList.Clear();
        }

        /// <exclude />
        public bool Contains(Param value)
        {
            return _innerList.Contains(value);
        }

        /// <exclude />
        public int IndexOf(Param value)
        {
            return _innerList.IndexOf(value);
        }

        /// <exclude />
        public void Insert(int index, Param value)
        {
            _innerList.Insert(index, value);
        }

        /// <exclude />
        public bool IsFixedSize
        {
            get { return false; }
        }

        /// <exclude />
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <exclude />
        public void Remove(Param value)
        {
            _innerList.Remove(value);
        }

        /// <exclude />
        public void RemoveAt(int index)
        {
            _innerList.RemoveAt(index);
        }

        /// <exclude />
        public Param this[int index]
        {
            get { return (Param)_innerList[index]; }
            set { _innerList[index] = value; }
        }

        /// <exclude />
        public void CopyTo(Array array, int index)
        {
            _innerList.CopyTo(array, index);
        }

        /// <exclude />
        public int Count
        {
            get { return _innerList.Count; }
        }

        /// <exclude />
        public bool IsSynchronized
        {
            get { return _innerList.IsSynchronized; }
        }

        /// <exclude />
        public object SyncRoot
        {
            get { return this; }
        }

        /// <exclude />
        public IEnumerator GetEnumerator()
        {
            return _innerList.GetEnumerator();
        }

        int IList.Add(object value)
        {
            var param = (Param)value;

            return _innerList.Add(param);
        }

        bool IList.Contains(object value)
        {
            return this.Contains((Param)value);
        }

        int IList.IndexOf(object value)
        {
            return this.IndexOf((Param)value);
        }

        void IList.Insert(int index, object value)
        {
            this.Insert(index, (Param)value);
        }

        void IList.Remove(object value)
        {
            this.Remove((Param)value);
        }

        object IList.this[int index]
        {
            get { return this._innerList[index]; }
            set { this._innerList[index] = (Param)value; }
        }
    }
}
