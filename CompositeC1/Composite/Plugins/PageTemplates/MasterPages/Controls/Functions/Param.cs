/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.ComponentModel;
using System.Web.UI;

namespace Composite.Plugins.PageTemplates.MasterPages.Controls.Functions
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [ControlBuilder(typeof(ParamControlBuilder))]
    public class Param : Control, IParserAccessor
    {
        private object _value;

        /// <exclude />
        [TypeConverter(typeof(StringToObjectConverter))]
        public object Value
        {
            get { return _value; }

            set
            {
                if (value is ParamObjectConverter)
                {
                    _value = ((ParamObjectConverter)value).Value;
                }
                else
                {
                    _value = value;
                }
            }

        }

        /// <exclude />
        public string Name { get; set; }


        /// <exclude />
        public Param() { }

        /// <exclude />
        public Param(string name, object value)
        {
            Name = name;
            Value = value;
        }

        /// <exclude />
        protected override void AddParsedSubObject(object obj)
        {
            if (!HasControls() && (obj is LiteralControl))
            {
                this.Value = ((LiteralControl)obj).Text;
                return;
            }
            
            base.AddParsedSubObject(obj);
        }

        /// <exclude />
        protected override void DataBindChildren()
        {
            base.DataBindChildren();

            if (Value == null)
            {
                if (Controls.Count > 0)
                {
                    var child = Controls[0];
                    if (child is DataBoundLiteralControl)
                    {
                        Value = ((DataBoundLiteralControl)child).Text;
                    }
                }
            }
        }
    }
}
