/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using System.Xml.Linq;

using Composite.Core.Localization;
using Composite.Core.Xml;
using Composite.Plugins.PageTemplates.MasterPages.Controls.Functions;

namespace Composite.Plugins.PageTemplates.MasterPages.Controls.Rendering
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public class Render : Markup
    {
        private string _placeholderId;

        /// <exclude />
        public string PlaceholderID
        {
            get { return _placeholderId;  }
            set { _placeholderId = value;  } 
        }

        /// <exclude />
        public bool HasBody
        {
            get
            {
                EnsureChildControls();

                if (Markup == null)
                {
                    return false;
                }

                var body = new XhtmlDocument(Markup).Body;

                return body != null && body.Nodes().Any();
            }
        }

        /// <exclude />
        public XhtmlDocument Markup
        {
            get { return InnerContent == null ? null : new XhtmlDocument(base.InnerContent); }
            set { InnerContent = value != null ? value.Root : null; }
        }

        /// <exclude />
        protected override void CreateChildControls()
        {
            DataBind();

            if (InnerContent == null)
            {
                var renderingInfo = MasterPagePageRenderer.GetRenderingInfo(this.Page);

                string placeholderId = PlaceholderID ?? ID;

                if (placeholderId != null)
                {
                    var content = renderingInfo.Contents.SingleOrDefault(c => c.PlaceHolderId == placeholderId);
                    if (content == null)
                    {
                        InnerContent = new XElement(Namespaces.Xhtml + "html",
                                new XAttribute(XNamespace.Xmlns + "f", Namespaces.Function10),
                                new XAttribute(XNamespace.Xmlns + "lang", LocalizationXmlConstants.XmlNamespace),
                                    new XElement(Namespaces.Xhtml + "head"),
                                    new XElement(Namespaces.Xhtml + "body"));
                    }
                    else
                    {
                        if (content.Content.StartsWith("<html"))
                        {
                            try
                            {
                                InnerContent = XhtmlDocument.Parse(content.Content).Root;
                            }
                            catch (ArgumentException) { }
                        }
                        else
                        {
                            InnerContent = new XElement(Namespaces.Xhtml + "html",
                               new XAttribute(XNamespace.Xmlns + "f", Namespaces.Function10),
                                   new XElement(Namespaces.Xhtml + "head"),
                                   new XElement(Namespaces.Xhtml + "body", XElement.Parse(content.Content)));
                        }
                    }
                }
            }

            base.CreateChildControls();
        }
    }
}
