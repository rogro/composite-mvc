/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;

namespace Composite.Plugins.PageTemplates.MasterPages
{
    internal class MasterPageRenderingInfo
    {
        public MasterPageRenderingInfo(string virtualPath, IDictionary<string, PropertyInfo> placeholderProperties)
        {
            VirtualPath = virtualPath;
            PlaceholderProperties = placeholderProperties;
        }

        public string VirtualPath { get; private set; }

        public virtual IDictionary<string, PropertyInfo> PlaceholderProperties { get; private set; }
    }

    internal class LazyInitializedMasterPageRenderingInfo : MasterPageRenderingInfo
    {
        private IDictionary<string, PropertyInfo> _placeholderProperties;
        private readonly MasterPagePageTemplateProvider _provider;
        private readonly string _filePath;

        public LazyInitializedMasterPageRenderingInfo(string filePath, string virtualPath, MasterPagePageTemplateProvider provider)
            : base(virtualPath, null)
        {
            _filePath = filePath;
            _provider = provider;
        }

        public override IDictionary<string, PropertyInfo> PlaceholderProperties
        {
            get
            {
                if (_placeholderProperties == null)
                {
                    lock (this)
                    {
                        if (_placeholderProperties == null)
                        {
                            _placeholderProperties = InitializePlaceholderProperties();
                        }
                    }
                }
                return _placeholderProperties;
            }
        }

        private IDictionary<string, PropertyInfo> InitializePlaceholderProperties()
        {
            MasterPage masterPage;
            MasterPagePageTemplateDescriptor parsedPageTemplateDescriptor;
            MasterPageRenderingInfo renderingInfo;
            Exception loadingException;

            if (!_provider.LoadMasterPage(
                _filePath,
                out masterPage,
                out parsedPageTemplateDescriptor,
                out renderingInfo,
                out loadingException))
            {
                throw loadingException;
            }

            Verify.IsNotNull(masterPage, "Failed to compile master page file '{0}'", _filePath);
            Verify.That(masterPage is MasterPagePageTemplate, "Incorrect base class. '{0}'", _filePath);

            return renderingInfo.PlaceholderProperties;
        }
    }
}
