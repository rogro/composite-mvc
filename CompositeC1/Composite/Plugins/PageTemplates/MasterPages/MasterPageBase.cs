/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Web.UI;
using Composite.Core.PageTemplates;
using Composite.Data;
using Composite.Plugins.PageTemplates.MasterPages.Controls.Rendering;

namespace Composite.Plugins.PageTemplates.MasterPages
{
    /// <summary>
    /// Base class for ASP.NET MasterPage classes in Composite C1. Inheriting from this bring common features like easy data and sitemap access. 
    /// This class is intended for use in shared MasterPages, to create a MasterPage based page template for Composite C1 use <see cref="Composite.Plugins.PageTemplates.MasterPages.MasterPagePageTemplate"/>.
    /// </summary>
    public abstract class MasterPageBase : MasterPage
    {
        private DataConnection _dataConnection;

        /// <summary>
        /// Gets or sets the data connection.
        /// </summary>
        /// <value>
        /// The data connection.
        /// </value>
        public DataConnection Data
        {
            get
            {
                if (_dataConnection == null)
                {
                    _dataConnection = new DataConnection();
                }
                return _dataConnection;
            }
        }

        /// <summary>
        /// Gets the sitemap navigator.
        /// </summary>
        public SitemapNavigator SitemapNavigator
        {
            get { return Data.SitemapNavigator; }
        }

        /// <summary>
        /// Gets a Page Template Feature based on name.
        /// </summary>
        /// <param name="featureName">Name of the Page Template Feature to return.</param>
        /// <returns>The Page Template Feature as an ASP.NET Control</returns>
        public Control GetPageTemplateFeature(string featureName)
        {
            var feature = new PageTemplateFeature();
            feature.Name = featureName;
            return feature;
        }


        /// <exclude />
        public override void Dispose()
        {
            if (_dataConnection != null)
            {
                _dataConnection.Dispose();
                _dataConnection = null;
            }

            base.Dispose();
        }
    }
}
