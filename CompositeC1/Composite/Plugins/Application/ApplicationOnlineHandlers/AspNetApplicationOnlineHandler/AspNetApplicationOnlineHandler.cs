/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Configuration;
using System.IO;
using Composite.Core.Application;
using Composite.Core.Application.Plugins.ApplicationOnlineHandler;
using Composite.Core.Extensions;
using Composite.Core.IO;
using Composite.Core.ResourceSystem;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ObjectBuilder;
using Microsoft.Practices.ObjectBuilder;


namespace Composite.Plugins.Application.ApplicationOnlineHandlers.AspNetApplicationOnlineHandler
{
    [ConfigurationElementType(typeof(AspNetApplicationOnlineHandlerData))]
    internal sealed class AspNetApplicationOnlineHandler : IApplicationOnlineHandler
    {
        private readonly string _sourceFilename;


        public AspNetApplicationOnlineHandler(string appOfflineFilename)
        {
            Verify.ArgumentNotNullOrEmpty(appOfflineFilename, "appOfflineFilename");

            _sourceFilename = Path.Combine(PathUtil.BaseDirectory, PathUtil.Resolve(appOfflineFilename));
        }


        public void TurnApplicationOffline()
        {
            ApplicationOfflineCheckHttpModule.FilePath = _sourceFilename;
            ApplicationOfflineCheckHttpModule.IsOffline = true;
        }
        

        public void TurnApplicationOnline()
        {
            ApplicationOfflineCheckHttpModule.IsOffline = false;
        }

        public bool IsApplicationOnline()
        {
            return !ApplicationOfflineCheckHttpModule.IsOffline;
        }


        public bool CanPutApplicationOffline(out string errorMessage)
        {
            if(!C1File.Exists(_sourceFilename))
            {
                errorMessage = "AspNetApplicationOnlineHandler: Template file '{0}' is missing".FormatWith(_sourceFilename);
                return false;
            }

            string websiteRoot = PathUtil.BaseDirectory;
            if (!PathUtil.WritePermissionGranted(websiteRoot))
            {
                errorMessage = StringResourceSystemFacade.GetString(
                    "Composite.Core.PackageSystem.PackageFragmentInstallers", "NotEnoughNtfsPermissions")
                    .FormatWith(websiteRoot);

                return false;
            }

            errorMessage = null;
            return true;
        }
    }


    [Assembler(typeof(AspNetApplicationOnlineHandlerAssembler))]
    internal class AspNetApplicationOnlineHandlerData : ApplicationOnlineHandlerData
    {
        private const string _appOfflineFilenamePropertyName = "appOfflineFilename";
        [ConfigurationProperty(_appOfflineFilenamePropertyName, IsRequired = true)]
        public string AppOfflineFilename
        {
            get { return (string)base[_appOfflineFilenamePropertyName]; }
            set { base[_appOfflineFilenamePropertyName] = value; }
        }
    }


    internal sealed class AspNetApplicationOnlineHandlerAssembler : IAssembler<IApplicationOnlineHandler, ApplicationOnlineHandlerData>
    {
        public IApplicationOnlineHandler Assemble(IBuilderContext context, ApplicationOnlineHandlerData objectConfiguration, IConfigurationSource configurationSource, ConfigurationReflectionCache reflectionCache)
        {
            AspNetApplicationOnlineHandlerData data = (AspNetApplicationOnlineHandlerData)objectConfiguration;

            return new AspNetApplicationOnlineHandler(data.AppOfflineFilename);
        }
    }
}
