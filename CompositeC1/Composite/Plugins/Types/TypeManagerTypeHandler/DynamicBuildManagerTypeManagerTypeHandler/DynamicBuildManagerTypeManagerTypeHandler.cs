/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using System.Collections.Generic;
using Composite.Core.Types;
using Composite.Core.Types.Plugins.TypeManagerTypeHandler;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ObjectBuilder;


namespace Composite.Plugins.Types.TypeManagerTypeHandler.DynamicBuildManagerTypeManagerTypeHandler
{    
    [ConfigurationElementType(typeof(DynamicBuildManagerTypeManagerTypeHandlerData))]
    internal sealed class DynamicBuildManagerTypeManagerTypeHandler : ITypeManagerTypeHandler
    {
        private static readonly string _prefix = "DynamicType:";

        private static readonly object _lock = new object();
        private static readonly Dictionary<Type, string> _serializedCache = new Dictionary<Type, string>();

        public Type GetType(string fullName)
        {
            Type compiledType = CodeGenerationManager.GetCompiledTypes().LastOrDefault(f => f.FullName == fullName);
            if (compiledType != null) return compiledType;

            if (fullName.StartsWith(_prefix) && !fullName.Contains(","))
            {
                string name = fullName.Remove(0, _prefix.Length);
                Type resultType = Type.GetType(name + ", Composite.Generated");

                return resultType;
            }
            
            return null;
        }


        
        public string SerializeType(Type type)
        {
            lock (_lock)
            {
                string result;

                if (_serializedCache.TryGetValue(type, out result) == false)
                {
                    result = SerializeTypeImpl(type);
                    _serializedCache.Add(type, result);
                }

                return result;
            }
        }



        public bool HasTypeWithName(string typeFullname)
        {
            return GetType(typeFullname) != null;
        }



        private static string SerializeTypeImpl(Type type)
        {
            string assemblyLocation = type.Assembly.Location;

            if ((assemblyLocation.StartsWith(CodeGenerationManager.TempAssemblyFolderPath, StringComparison.InvariantCultureIgnoreCase)) ||
                (assemblyLocation.IndexOf(CodeGenerationManager.CompositeGeneratedFileName, StringComparison.InvariantCultureIgnoreCase) >= 0))
            {
                return type.FullName;
            }
                
            return null;
        }
    }



    [Assembler(typeof(NonConfigurableTypeManagerTypeHandlerAssembler))]
    internal sealed class DynamicBuildManagerTypeManagerTypeHandlerData : TypeManagerTypeHandlerData
    {        
    }
}
