/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using System.Reflection;
using System.Web.Compilation;
using Composite.Core.Types.Plugins.TypeManagerTypeHandler;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ObjectBuilder;
using System.Web.Hosting;


namespace Composite.Plugins.Types.TypeManagerTypeHandler.AspNetBuildManagerTypeManagerTypeHandler
{    
    [ConfigurationElementType(typeof(AspNetBuildManagerTypeManagerTypeHandlerData))]
    internal sealed class AspNetBuildManagerTypeManagerTypeHandler : ITypeManagerTypeHandler
    {
        private const string _typeNamePrefix = "AspNetType:";

        public Type GetType(string fullName)
        {
            if (!HostingEnvironment.IsHosted) return null;

            string name = fullName;

            if (name.StartsWith(_typeNamePrefix))
            {
                name = name.Remove(0, _typeNamePrefix.Length);
            }

            if(name.Contains(":"))
            {
                return null;
            }

            return BuildManager.GetType(name, false, true);
        }


        public string SerializeType(Type type)
        {
            if (!HostingEnvironment.IsHosted) return null;

            if (BuildManager.CodeAssemblies != null)
            {
                foreach (object obj in BuildManager.CodeAssemblies)
                {
                    Assembly assembly = obj as Assembly;

                    if (assembly != null)
                    {
                        if (assembly.GetTypes().Contains(type))
                        {
                            return string.Format("{0}{1}", _typeNamePrefix, type.FullName);
                        }
                    }
                }
            }
            
            return null;
        }



        public bool HasTypeWithName(string typeFullname)
        {
            if (!HostingEnvironment.IsHosted) return false;

            return BuildManager.GetType(typeFullname, false, true) != null;
        }
    }

    [Assembler(typeof(NonConfigurableTypeManagerTypeHandlerAssembler))]
    internal sealed class AspNetBuildManagerTypeManagerTypeHandlerData : TypeManagerTypeHandlerData
    {
    }
}
