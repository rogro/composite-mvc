/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Data;
using System.Globalization;
using Composite.Core.Extensions;
using Composite.Data;
using Composite.Data.DynamicTypes;


namespace Composite.Plugins.Data.DataProviders.MSSqlServerDataProvider.Foundation
{
	internal static class DynamicTypesCommon
	{
		internal static string GenerateTableName(DataTypeDescriptor dataTypeDescriptor)
		{
		    return dataTypeDescriptor.GetFullInterfaceName().Replace('.', '_');
		}

		internal static string GenerateTableName(DataTypeDescriptor dataTypeDescriptor, DataScopeIdentifier dataScope, CultureInfo cultureInfo)
		{
		    string tableName = dataTypeDescriptor.GetFullInterfaceName().Replace('.', '_');

			switch (dataScope.Name)
			{
				case DataScopeIdentifier.PublicName:
					break;
				case DataScopeIdentifier.AdministratedName:
			        tableName += "_Unpublished";
					break;
				default:
					throw new InvalidOperationException("Unsupported data scope identifier: '{0}'".FormatWith(dataScope.Name));
			}

            if (!cultureInfo.Name.IsNullOrEmpty())
            {
                tableName += "_" + cultureInfo.Name.Replace('-', '_').Replace(' ', '_');
            }

            return tableName;
		}


		internal static string GenerateListTableName(DataTypeDescriptor typeDescriptor, DataFieldDescriptor fieldDescriptor)
		{
			return string.Format("{0}_{1}", GenerateTableName(typeDescriptor), fieldDescriptor.Name);
		}

		internal static string MapStoreTypeToSqlDataType(StoreFieldType storeType)
		{
			string result = string.Format(" [{0}]", GetStoreTypeToSqlDataTypeMapping(storeType));

			switch (storeType.PhysicalStoreType)
			{
				case PhysicalStoreFieldType.String:
					return string.Format("{0}({1})", result, storeType.MaximumLength);
				case PhysicalStoreFieldType.LargeString:
					return string.Format("{0}({1})", result, "max");
				case PhysicalStoreFieldType.Decimal:
					return string.Format("{0}({1},{2})", result, storeType.NumericPrecision, storeType.NumericScale);
				default:
					return result;
			}
		}

		internal static SqlDbType GetStoreTypeToSqlDataTypeMapping(StoreFieldType storeType)
		{
			switch (storeType.PhysicalStoreType)
			{
				case PhysicalStoreFieldType.Integer:
					return SqlDbType.Int;
				case PhysicalStoreFieldType.Long:
					return SqlDbType.BigInt;
				case PhysicalStoreFieldType.String:
					return SqlDbType.NVarChar;
				case PhysicalStoreFieldType.LargeString:
					return SqlDbType.NVarChar;
				case PhysicalStoreFieldType.DateTime:
					return SqlDbType.DateTime;
				case PhysicalStoreFieldType.Decimal:
					return SqlDbType.Decimal;
				case PhysicalStoreFieldType.Boolean:
					return SqlDbType.Bit;
				case PhysicalStoreFieldType.Guid:
					return SqlDbType.UniqueIdentifier;
				default:
					throw new ArgumentException("Unknown store type on field");
			}
		}

		internal static bool AreSame(DataFieldDescriptor a, DataFieldDescriptor b)
		{
			return a.ToXml().ToString() == b.ToXml().ToString();
		}
	}
}
