/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using Composite.Data.DynamicTypes;


namespace Composite.Plugins.Data.DataProviders.MSSqlServerDataProvider.Foundation
{
    internal static class NamesCreator
    {
        internal static string MakeDataIdClassName(DataTypeDescriptor dataTypeDescriptor)
        {
            return string.Format("{0}DataId", MakeNiceTypeFullName(dataTypeDescriptor));
        }



        internal static string MakeDataIdClassFullName(DataTypeDescriptor dataTypeDescriptor, string providerName)
        {
            return string.Format("{0}.{1}", MakeNamespaceName(providerName), MakeDataIdClassName(dataTypeDescriptor));
        }



        internal static string MakeEntityBaseClassName(DataTypeDescriptor dataTypeDescriptor)
        {
            return string.Format("{0}EntityBase", dataTypeDescriptor.GetFullInterfaceName().Replace('.', '_').Replace('+', '_'));
        }



        internal static string MakeEntityBaseClassFullName(DataTypeDescriptor dataTypeDescriptor, string providerName)
        {
            return string.Format("{0}.{1}", MakeNamespaceName(providerName), MakeEntityBaseClassName(dataTypeDescriptor));
        }



        internal static string MakeEntityClassName(DataTypeDescriptor dataTypeDescriptor, string dataScopeIdentifierName, string localeCultureName)
        {
            if (string.IsNullOrEmpty(localeCultureName)) return string.Format("{0}_{1}Entity", MakeNiceTypeFullName(dataTypeDescriptor), dataScopeIdentifierName);

            return string.Format("{0}_{1}_{2}Entity", MakeNiceTypeFullName(dataTypeDescriptor), dataScopeIdentifierName, localeCultureName.Replace("-", ""));
        }


        internal static string MakeEntityClassFullName(DataTypeDescriptor dataTypeDescriptor, string dataScopeIdentifierName, string localeCultureName, string providerName)
        {
            return string.Format("{0}.{1}", MakeNamespaceName(providerName), MakeEntityClassName(dataTypeDescriptor, dataScopeIdentifierName, localeCultureName));
        }


        internal static string MakeSqlDataProviderHelperClassName(DataTypeDescriptor dataTypeDescriptor, string dataScopeIdentifierName, string localeCultureName)
        {
            if (string.IsNullOrEmpty(localeCultureName)) return string.Format("{0}_{1}SqlDataProviderHelper", MakeNiceTypeFullName(dataTypeDescriptor), dataScopeIdentifierName);

            return string.Format("{0}_{1}_{2}SqlDataProviderHelper", MakeNiceTypeFullName(dataTypeDescriptor), dataScopeIdentifierName, localeCultureName.Replace("-", ""));
        }


        internal static string MakeSqlDataProviderHelperClassFullName(DataTypeDescriptor dataTypeDescriptor, string dataScopeIdentifierName, string localeCultureName, string providerName)
        {
            return string.Format("{0}.{1}", MakeNamespaceName(providerName), MakeSqlDataProviderHelperClassName(dataTypeDescriptor, dataScopeIdentifierName, localeCultureName));
        }


        internal static string MakeDataContextFieldName(string tableName)
        {
            return tableName;
        }


        internal static string MakeDataContextClassName(string providerName)
        {
            return string.Format("{0}DataContext", providerName);
        }


        internal static string MakeDataContextClassFullName(string providerName)
        {
            return string.Format("{0}.{1}", MakeNamespaceName(providerName), MakeDataContextClassName(providerName));
        }


        internal static string MakeNamespaceName(string providerName)
        {
            return "CompositeGenerated." + providerName;
        }



        private static string MakeNiceTypeFullName(DataTypeDescriptor dataTypeDescriptor)
        {
            return dataTypeDescriptor.GetFullInterfaceName().Replace('.', '_').Replace('+', '_');
        }
    }
}
