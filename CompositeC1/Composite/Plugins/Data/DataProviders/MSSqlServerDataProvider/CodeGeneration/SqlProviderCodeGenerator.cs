/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.CodeDom;
using System.Collections.Generic;
using Composite.Data.DynamicTypes;
using Composite.Plugins.Data.DataProviders.MSSqlServerDataProvider.Foundation;


namespace Composite.Plugins.Data.DataProviders.MSSqlServerDataProvider.CodeGeneration
{
    internal class SqlProviderCodeGenerator
    {
        private readonly string _providerName;


        public SqlProviderCodeGenerator(string providerName)
        {
            _providerName = providerName;
        }



        public IEnumerable<CodeTypeDeclaration> CreateCodeDOMs(DataTypeDescriptor dataTypeDescriptor, IEnumerable<SqlDataTypeStoreDataScope> sqlDataTypeStoreDataScopes, out IEnumerable<Tuple<string, string>> entityClassNamesAndDataContextFieldNames)
        {
            List<CodeTypeDeclaration> result = new List<CodeTypeDeclaration>();

            string dataIdClassName = NamesCreator.MakeDataIdClassName(dataTypeDescriptor);
            string entityBaseClassName = NamesCreator.MakeEntityBaseClassName(dataTypeDescriptor);            

            DataIdClassGenerator dataIdClassGenerator = new DataIdClassGenerator(dataTypeDescriptor, dataIdClassName);
            CodeTypeDeclaration dataIdClassCodeTypeDeclaration = dataIdClassGenerator.CreateClass();
            result.Add(dataIdClassCodeTypeDeclaration);


            EntityBaseClassGenerator entityBaseClassGenerator = new EntityBaseClassGenerator(dataTypeDescriptor, entityBaseClassName, dataIdClassName, _providerName);
            CodeTypeDeclaration entityBaseClassCodeTypeDeclaration = entityBaseClassGenerator.CreateClass();
            result.Add(entityBaseClassCodeTypeDeclaration);

            List<Tuple<string, string>> outResult = new List<Tuple<string, string>>();
            foreach (SqlDataTypeStoreDataScope dataScope in sqlDataTypeStoreDataScopes)
            {
                string entityClassName = NamesCreator.MakeEntityClassName(dataTypeDescriptor, dataScope.DataScopeName, dataScope.CultureName);

                EntityClassGenerator entityClassGenerator = new EntityClassGenerator(dataTypeDescriptor, entityClassName, entityBaseClassName, dataScope.TableName, dataScope.DataScopeName, dataScope.CultureName);
                CodeTypeDeclaration entityClassCodeTypeDeclaration = entityClassGenerator.CreateClass();
                result.Add(entityClassCodeTypeDeclaration);

                string sqlDataProviderHelperClassName = NamesCreator.MakeSqlDataProviderHelperClassName(dataTypeDescriptor, dataScope.DataScopeName, dataScope.CultureName);
                string dataContextFieldName = NamesCreator.MakeDataContextFieldName(dataScope.TableName);

                SqlDataProviderHelperGenerator sqlDataProviderHelperGenerator = new SqlDataProviderHelperGenerator(dataTypeDescriptor, sqlDataProviderHelperClassName, dataIdClassName, entityClassName, dataContextFieldName);
                CodeTypeDeclaration sqlDataProviderHelperTypeDeclaration = sqlDataProviderHelperGenerator.CreateClass();
                result.Add(sqlDataProviderHelperTypeDeclaration);

                outResult.Add(new Tuple<string, string>(entityClassName, dataContextFieldName));
            }

            entityClassNamesAndDataContextFieldNames = outResult;

            return result;
        }



        public IEnumerable<Tuple<string, string>> CreateEntityClassNamesAndDataContextFieldNames(DataTypeDescriptor dataTypeDescriptor, IEnumerable<SqlDataTypeStoreDataScope> sqlDataTypeStoreDataScopes)
        {
            List<Tuple<string, string>> entityClassNamesAndDataContextFieldNames = new List<Tuple<string, string>>();

            foreach (SqlDataTypeStoreDataScope dataScope in sqlDataTypeStoreDataScopes)
            {
                string entityClassName = NamesCreator.MakeEntityClassName(dataTypeDescriptor, dataScope.DataScopeName, dataScope.CultureName);
                string dataContextFieldName = NamesCreator.MakeDataContextFieldName(dataScope.TableName);

                entityClassNamesAndDataContextFieldNames.Add(new Tuple<string, string>(entityClassName, dataContextFieldName));
            }

            return entityClassNamesAndDataContextFieldNames;
        }



        public IEnumerable<CodeTypeDeclaration> CreateDataContextCodeDOMs(IEnumerable<Tuple<string, string>> entityClassNamesAndDataContextFieldNames)
        {
            string dataContextClassName = NamesCreator.MakeDataContextClassName(_providerName);

            DataContextClassGenerator dataContextClassGenerator = new DataContextClassGenerator(dataContextClassName, entityClassNamesAndDataContextFieldNames);
            CodeTypeDeclaration dataContextTypeDeclaration = dataContextClassGenerator.CreateClass();
            yield return dataContextTypeDeclaration;
        }
    }
}
