/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Reflection;


namespace Composite.Plugins.Data.DataProviders.MSSqlServerDataProvider.CodeGeneration
{
    internal class DataContextClassGenerator
    {
        private const string SqlDataContextHelperClassName = "_sqlDataContextHelperClass";

        private readonly string _dataContextClassName;
        private readonly IEnumerable<Tuple<string, string>> _entityClassNamesAndDataContextFieldNames;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataContextClassName"></param>
        /// <param name="entityClassNamesAndDataContextFieldNames">For all datatypes datacopes. (EntityClassName, DataContextFieldName)</param>
        public DataContextClassGenerator(string dataContextClassName, IEnumerable<Tuple<string, string>> entityClassNamesAndDataContextFieldNames)
        {
            _dataContextClassName = dataContextClassName;
            _entityClassNamesAndDataContextFieldNames = entityClassNamesAndDataContextFieldNames;
        }



        public CodeTypeDeclaration CreateClass()
        {
            CodeTypeDeclaration declaration = new CodeTypeDeclaration();
            declaration.Name = _dataContextClassName;
            declaration.IsClass = true;
            declaration.TypeAttributes = TypeAttributes.Public | TypeAttributes.Sealed;
            declaration.BaseTypes.Add(typeof(DataContextBase));
            declaration.BaseTypes.Add(typeof(ISqlDataContext));

            CodeMemberField codeMemberField = new CodeMemberField(typeof(SqlDataContextHelperClass), SqlDataContextHelperClassName);
            declaration.Members.Add(codeMemberField);

            AddConstructor(declaration);
            AddEntityFields(declaration);
            AddSqlDataContextMethods(declaration);

            return declaration;
        }



        private static void AddConstructor(CodeTypeDeclaration declaration)
        {
            CodeConstructor constructor = new CodeConstructor();
            constructor.Attributes = MemberAttributes.Public;

            constructor.Parameters.Add(
                new CodeParameterDeclarationExpression(
                    typeof(IDbConnection), "connection"));

            constructor.BaseConstructorArgs.Add(new CodeVariableReferenceExpression("connection"));

            constructor.CustomAttributes.Add(new CodeAttributeDeclaration(
                    new CodeTypeReference(typeof(DebuggerNonUserCodeAttribute))
                ));


            constructor.Statements.Add(
                new CodeAssignStatement(
                    new CodeVariableReferenceExpression(SqlDataContextHelperClassName),
                    new CodeObjectCreateExpression(
                        typeof(SqlDataContextHelperClass),
                        new CodeExpression[] {
                            new CodeThisReferenceExpression()
                        }
                    )
                ));

            declaration.Members.Add(constructor);
        }



        private void AddEntityFields(CodeTypeDeclaration declaration)
        {
            foreach (Tuple<string, string> value in _entityClassNamesAndDataContextFieldNames)
            {
                string entityClassName = value.Item1;
                string dataContextFieldName = value.Item2;

                CodeMemberField codeMemberField = new CodeMemberField(new CodeTypeReference(entityClassName), dataContextFieldName);
                codeMemberField.Attributes = MemberAttributes.Public;

                declaration.Members.Add(codeMemberField);
            }
        }



        private static void AddSqlDataContextMethods(CodeTypeDeclaration declaration)
        {
            CodeMemberMethod codeMemberMethodAdd = new CodeMemberMethod();
            codeMemberMethodAdd.Name = "Add";
            codeMemberMethodAdd.Attributes = MemberAttributes.Public | MemberAttributes.Final;
            codeMemberMethodAdd.Parameters.Add(new CodeParameterDeclarationExpression(typeof(object), "entity"));
            codeMemberMethodAdd.Parameters.Add(new CodeParameterDeclarationExpression(typeof(string), "fieldName"));

            codeMemberMethodAdd.Statements.Add(
                new CodeMethodInvokeExpression(
                    new CodeVariableReferenceExpression(SqlDataContextHelperClassName),
                    "Add",
                    new CodeExpression[] {
                        new CodeVariableReferenceExpression("entity"),
                        new CodeVariableReferenceExpression("fieldName")
                    }
                ));

            declaration.Members.Add(codeMemberMethodAdd);



            CodeMemberMethod codeMemberMethodRemove = new CodeMemberMethod();
            codeMemberMethodRemove.Name = "Remove";
            codeMemberMethodRemove.Attributes = MemberAttributes.Public | MemberAttributes.Final;
            codeMemberMethodRemove.Parameters.Add(new CodeParameterDeclarationExpression(typeof(object), "entity"));
            codeMemberMethodRemove.Parameters.Add(new CodeParameterDeclarationExpression(typeof(string), "fieldName"));

            codeMemberMethodRemove.Statements.Add(
                new CodeMethodInvokeExpression(
                    new CodeVariableReferenceExpression(SqlDataContextHelperClassName),
                    "Remove",
                    new CodeExpression[] {
                        new CodeVariableReferenceExpression("entity"),
                        new CodeVariableReferenceExpression("fieldName")
                    }
                ));

            declaration.Members.Add(codeMemberMethodRemove);
        }
    }
}
