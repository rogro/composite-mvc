/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Data.Linq;
using System.Reflection;
using Composite.Core.Collections.Generic;
using Composite.Plugins.Data.DataProviders.MSSqlServerDataProvider.Foundation;


namespace Composite.Plugins.Data.DataProviders.MSSqlServerDataProvider.CodeGeneration
{
    /// <summary>    
    /// Provides an api to work with generated tables of a DataContext object.
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public class SqlDataContextHelperClass
    {
        readonly DataContext _dataContext;

        private readonly Hashtable<string, ITable> _tables = new Hashtable<string, ITable>();
        private readonly object _syncRoot = new object();


        /// <exclude />
        public SqlDataContextHelperClass(DataContext dataContext)
        {
            _dataContext = dataContext;
        }


        /// <exclude />
        public void Add(object entity, string tableName)
        {
            EnsureTableInitialized(tableName);

            _tables[tableName].InsertOnSubmit(entity);
        }


        /// <exclude />
        public void Remove(object entity, string tableName)
        {
            EnsureTableInitialized(tableName);

            ITable table = _tables[tableName]; 

            table.Attach(entity);
            table.DeleteOnSubmit(entity);
        }


        private void EnsureTableInitialized(string tableName)
        {
            if(_tables.ContainsKey(tableName))
            {
                return;
            }

            lock(_syncRoot)
            {
                if (_tables.ContainsKey(tableName))
                {
                    return;
                }

                Type dataContextType = _dataContext.GetType();

                FieldInfo fi = dataContextType.GetField(tableName);
                Verify.IsNotNull(fi, "DataContext class should have a field with name '{0}'", tableName);

                Type entityType = fi.FieldType;

                // Operation takes 3 ms every request :(
                ITable table = _dataContext.GetTable(entityType);

                _tables.Add(tableName, table);
            }
        }


        /// <exclude />
        internal static ITable GetTable(DataContext _dataContext, SqlDataTypeStoreTable storeInformation)
        {
            FieldInfo fi = storeInformation.DataContextQueryableFieldInfo;
            Verify.IsNotNull(fi, "Missing FieldInfo for a DataContext field.");

            object value = fi.GetValue(_dataContext);
            if(value == null)
            {
                var helperClassFieldInfo =_dataContext.GetType().GetField("_sqlDataContextHelperClass", BindingFlags.NonPublic | BindingFlags.Instance);
                Verify.IsNotNull(helperClassFieldInfo, "Helper field isn't exist in DataContext object.");

                var helper = helperClassFieldInfo.GetValue(_dataContext) as SqlDataContextHelperClass;
                Verify.That(helper != null, "Helper object has not been set");

                helper.EnsureTableInitialized(fi.Name);

                value = helper._tables[fi.Name];
            }
            return Verify.ResultNotNull(value as ITable);
        }
    }
}
