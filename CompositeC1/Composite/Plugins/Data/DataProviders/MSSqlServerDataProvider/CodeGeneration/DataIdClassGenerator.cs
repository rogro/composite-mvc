/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.CodeDom;
using System.Reflection;
using Composite.Data;
using Composite.Data.DynamicTypes;


namespace Composite.Plugins.Data.DataProviders.MSSqlServerDataProvider.CodeGeneration
{
    internal sealed class DataIdClassGenerator
    {
        private readonly string _dataIdClassName;
        private readonly DataTypeDescriptor _dataTypeDescriptor;


        public DataIdClassGenerator(DataTypeDescriptor dataTypeDescriptor, string dataIdClassName)
        {
            _dataTypeDescriptor = dataTypeDescriptor;
            _dataIdClassName = dataIdClassName;            
        }


        public CodeTypeDeclaration CreateClass()
        {
            CodeTypeDeclaration codeTypeDeclaration = new CodeTypeDeclaration(_dataIdClassName);
            codeTypeDeclaration.IsClass = true;
            codeTypeDeclaration.TypeAttributes = (TypeAttributes.Public | TypeAttributes.Sealed);
            codeTypeDeclaration.BaseTypes.Add(typeof(IDataId));

            AddDefaultConstructor(codeTypeDeclaration);
            AddConstructor(codeTypeDeclaration);
            AddProperties(codeTypeDeclaration);            

            return codeTypeDeclaration;
        }



        private static void AddDefaultConstructor(CodeTypeDeclaration declaration)
        {
            CodeConstructor defaultConstructor = new CodeConstructor();

            defaultConstructor.Attributes = MemberAttributes.Public | MemberAttributes.Final;

            declaration.Members.Add(defaultConstructor);
        }



        private void AddConstructor(CodeTypeDeclaration declaration)
        {
            CodeConstructor constructor = new CodeConstructor();
            constructor.Attributes = MemberAttributes.Public | MemberAttributes.Final;

            foreach (string keyPropertyName in _dataTypeDescriptor.KeyPropertyNames)
            {
                Type keyPropertyType = _dataTypeDescriptor.Fields[keyPropertyName].InstanceType;

                constructor.Parameters.Add(new CodeParameterDeclarationExpression(keyPropertyType, MakeParamName(keyPropertyName)));

                AddAsignment(constructor, keyPropertyName);
            }

            declaration.Members.Add(constructor);
        }



        private void AddProperties(CodeTypeDeclaration declaration)
        {
            foreach (string keyPropertyName in _dataTypeDescriptor.KeyPropertyNames)
            {
                Type keyPropertyType = _dataTypeDescriptor.Fields[keyPropertyName].InstanceType;
                string propertyFieldName = MakePropertyFieldName(keyPropertyName);

                declaration.Members.Add(new CodeMemberField(keyPropertyType, propertyFieldName));

                CodeMemberProperty property = new CodeMemberProperty();
                property.Name = keyPropertyName;
                property.Attributes = MemberAttributes.Public | MemberAttributes.Final;
                property.HasSet = true;
                property.HasGet = true;
                property.Type = new CodeTypeReference(keyPropertyType);
                property.GetStatements.Add(new CodeMethodReturnStatement(new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), propertyFieldName)));
                property.SetStatements.Add(new CodeAssignStatement(new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), propertyFieldName), new CodeArgumentReferenceExpression("value")));

                declaration.Members.Add(property);
            }
        }



        private static void AddAsignment(CodeConstructor constructor, string name)
        {
            string paramName = MakeParamName(name);
            string propertyFieldName = MakePropertyFieldName(name);

            constructor.Statements.Add(
                new CodeAssignStatement(
                    new CodeFieldReferenceExpression(
                        new CodeThisReferenceExpression(),
                        propertyFieldName
                    ),
                    new CodeArgumentReferenceExpression(paramName)
                )
            );
        }



        private static string MakeParamName(string name)
        {
            return string.Format("parm{0}", name);
        }



        private static string MakePropertyFieldName(string name)
        {
            return string.Format("_property{0}", name);
        }
    }    
}
