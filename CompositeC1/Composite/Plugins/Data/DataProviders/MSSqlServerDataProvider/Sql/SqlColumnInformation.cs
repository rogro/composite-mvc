/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Data;
using System.ComponentModel;
using Composite.Data.DynamicTypes;
using Composite.Plugins.Data.DataProviders.MSSqlServerDataProvider.Foundation;


namespace Composite.Plugins.Data.DataProviders.MSSqlServerDataProvider.Sql
{
    internal static class SqlColumnInformationExtensions
    {
        public static SqlColumnInformation CreateSqlColumnInformation(this DataTypeDescriptor dataTypeDescriptor, string fieldName)
        {
            DataFieldDescriptor dataFieldDescriptor = dataTypeDescriptor.Fields[fieldName];

            return new SqlColumnInformation(
                dataFieldDescriptor.Name,
                dataTypeDescriptor.KeyPropertyNames.Contains(dataFieldDescriptor.Name),
                false,
                false,
                dataFieldDescriptor.IsNullable,
                dataFieldDescriptor.InstanceType,
                DynamicTypesCommon.GetStoreTypeToSqlDataTypeMapping(dataFieldDescriptor.StoreType)
            );
       }
    }



    /// <summary>    
    /// </summary>
    /// <exclude />
    [EditorBrowsable(EditorBrowsableState.Never)] 
    public sealed class SqlColumnInformation
    {
        private readonly string _columnName;
        private readonly string _trimmedColumnName;
        private readonly bool _isPrimaryKey;
        private readonly bool _isIdentity;
        private readonly bool _isComputed;
        private readonly bool _isNullable;
        private readonly Type _type;
        private readonly SqlDbType _sqlDbType;

        private int? _hasCode = null;

        internal SqlColumnInformation(
            string columnName,
            bool isPrimaryKey,
            bool isIdentity,
            bool isComputed,
            bool isNullable,
            Type type,
            SqlDbType sqlDbType)
        {
            _columnName = columnName;
            _trimmedColumnName = _columnName.Replace(" ", "");            
            _isPrimaryKey = isPrimaryKey;
            _isIdentity = isIdentity;
            _isComputed = isComputed;
            _isNullable = isNullable;
            _type = type;
            _sqlDbType = sqlDbType;
        }        


        /// <exclude />
        public string ColumnName
        {
            get { return _columnName; }
        }


        /// <exclude />
        public string TrimmedColumnName
        {
            get { return _trimmedColumnName; }
        }


        /// <exclude />
        public bool IsPrimaryKey
        {
            get { return _isPrimaryKey; }
        }


        /// <exclude />
        public bool IsIdentity
        {
            get { return _isIdentity; }
        }


        /// <exclude />
        public bool IsComputed
        {
            get { return _isComputed; }
        }


        /// <exclude />
        public bool IsNullable
        {
            get { return _isNullable; }
        }


        /// <exclude />
        public Type Type
        {
            get { return _type; }   
        }


        /// <exclude />
        public SqlDbType SqlDbType
        {
            get { return _sqlDbType; }
        }


        /// <exclude />
        public override int GetHashCode()
        {
            if (false == _hasCode.HasValue)
            {
                _hasCode = _columnName.GetHashCode() ^
                           _trimmedColumnName.GetHashCode() ^
                           _isPrimaryKey.GetHashCode() ^
                           _isIdentity.GetHashCode() ^
                           _isComputed.GetHashCode() ^
                           _isNullable.GetHashCode() ^
                           _type.GetHashCode() ^
                           _sqlDbType.GetHashCode();
            }

            return _hasCode.Value;
        }
    }
}
