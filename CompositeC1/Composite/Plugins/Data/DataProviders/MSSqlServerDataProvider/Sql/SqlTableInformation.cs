/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;

using Composite.Core.Linq;


namespace Composite.Plugins.Data.DataProviders.MSSqlServerDataProvider.Sql
{
    internal sealed class SqlTableInformation : ISqlTableInformation
    {
        private readonly string _tableName;
        private bool _hasIdentityColumn;
        private string _identityColumnName;

        private readonly Dictionary<string, SqlColumnInformation> _columns = new Dictionary<string, SqlColumnInformation>();
        private int? _hashCode;

        internal SqlTableInformation(string tableName)
        {
            _tableName = tableName;
        }


        public string TableName
        {
            get { return _tableName; }
        }


        public bool HasIdentityColumn
        {
            get { return _hasIdentityColumn; }
        }


        public string IdentityColumnName
        {
            get { return _identityColumnName; }
        }


        public SqlColumnInformation this[string columnName]
        {
            get
            {
                return _columns[columnName];
            }
        }


        public IEnumerable<SqlColumnInformation> ColumnInformations
        {
            get
            {
                foreach(SqlColumnInformation column in _columns.Values)
                {
                    yield return column;
                }
            }
        }


        internal void AddColumnInformation(SqlColumnInformation columnInformation)
        {
            if (columnInformation.IsIdentity)
            {
                _hasIdentityColumn = true;
                _identityColumnName = columnInformation.ColumnName;
            }

            _columns.Add(columnInformation.ColumnName, columnInformation);

            _hashCode = null;
        }


        public override int GetHashCode()
        {
            if (!_hashCode.HasValue)
            {
                int calculatedHashCode =_tableName.GetHashCode() ^
                                        _hasIdentityColumn.GetHashCode() ^
                                        _columns.GetContentHashCode();

                if (_hasIdentityColumn)
                {
                    calculatedHashCode  ^= _identityColumnName.GetHashCode();
                }

                _hashCode = calculatedHashCode;
            }

            return _hashCode.Value;
        }
    }
}
