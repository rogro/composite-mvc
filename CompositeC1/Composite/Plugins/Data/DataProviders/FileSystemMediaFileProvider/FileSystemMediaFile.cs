/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Composite.Core.IO;
using Composite.Data;
using Composite.Data.Plugins.DataProvider.Streams;
using Composite.Data.Streams;
using Composite.Data.Types;


namespace Composite.Plugins.Data.DataProviders.FileSystemMediaFileProvider
{
    [FileStreamManager(typeof(FileSystemFileStreamManager))]
    internal sealed class FileSystemMediaFile : FileSystemFileBase, IMediaFile
    {
        private static readonly MD5 HashingAlgorithm = MD5.Create(); 



        public FileSystemMediaFile(string systemPath, string fileName, string folderName, string storeId, DataSourceId dataSourceId)
        {
            Id = CalculateId(folderName, fileName);
            SystemPath = systemPath;
            FileName = fileName;
            FolderPath = folderName;
            StoreId = storeId;
            DataSourceId = dataSourceId;
        }

        private static Guid CalculateId(string folderName, string fileName)
        {
            return GetHashValue(folderName + "/" + fileName);
        }

        private static Guid GetHashValue(string value)
        {
            var bytes = HashingAlgorithm.ComputeHash(Encoding.ASCII.GetBytes(value));
            return new Guid(bytes);
        }


        public Guid Id
        {
            get; internal set;
        }

        public string KeyPath
        {
            get { return this.GetKeyPath();  }
        }


        public string CompositePath
        {
            get { return this.GetCompositePath(); }
            set { throw new NotImplementedException(); }
        }


        public string StoreId
        {
            get;
            set;
        }



        public string Title
        {
            get
            {
                return this.FileName;
            }
            set
            {
                this.FileName = value;
            }
        }



        public string Description
        {
            get
            {
                return "";
            }
            set
            {
            }
        }



        public string Culture
        {
            get
            {
                return CultureInfo.InvariantCulture.Name;
            }
            set
            {
                ;
            }
        }



        public string MimeType
        {
            get { return MimeTypeInfo.GetCanonicalFromExtension(Path.GetExtension(this.FileName)); }
        }




        public int? Length
        {
            get 
            {
                C1FileInfo fileInfo = new C1FileInfo(this.SystemPath);

                return (int)fileInfo.Length;
            }
        }



        public DateTime? CreationTime
        {
            get 
            {
                return C1File.GetCreationTime(this.SystemPath);
            }
        }



        public DateTime? LastWriteTime
        {
            get 
            {
                return C1File.GetLastWriteTime(this.SystemPath);
            }
        }



        public bool IsReadOnly
        {
            get
            {
                return (C1File.GetAttributes(this.SystemPath) & FileAttributes.ReadOnly) == FileAttributes.ReadOnly;
            }
            set
            {
                ;
            }
        }



        public string FolderPath
        {
            get; 
            set;
        }



        public string FileName
        {
            get;
            set;
        }



        public DataSourceId DataSourceId
        {
            get;
            private set;
        }
    }
}
