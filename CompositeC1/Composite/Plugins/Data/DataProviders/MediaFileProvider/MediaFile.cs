/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.Data.Types;
using Composite.Data;
using Composite.Data.Plugins.DataProvider.Streams;
using Composite.Data.Streams;


namespace Composite.Plugins.Data.DataProviders.MediaFileProvider
{
    [FileStreamManager(typeof(FileSystemFileStreamManager))]
    internal class MediaFile : FileSystemFileBase, IMediaFile 
	{
        private readonly DataSourceId _dataSourceId;
        private string _keyPath;

        public MediaFile(IMediaFileData file, string storeId, DataSourceId dataSourceId, string filePath)
        {
            _dataSourceId = dataSourceId;
            StoreId = storeId;

            this.Id = file.Id;
            this.FileName = file.FileName;
            this.FolderPath = file.FolderPath;
            this.Title = file.Title;
            this.Description = file.Description;
            this.MimeType = file.MimeType;
            this.Length = file.Length;
            this.IsReadOnly = false;
            this.Culture = file.CultureInfo;
            this.CreationTime = file.CreationTime;
            this.LastWriteTime = file.LastWriteTime;

            this.SystemPath = filePath;
        }


        public Guid Id
        {
            get; internal set;
        }

        public string KeyPath
        {
            get { return _keyPath ?? (_keyPath = this.GetKeyPath()); }
        }

        public string CompositePath
        {
            get { return this.GetCompositePath(); }
            set { /* Do nothing. Used for deserialization purpouses */ }
        }



        public string StoreId
        {
            get;
            set;
        }

        public string FolderPath
        {
            get;
            set;
        }

        public string FileName
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string Culture
        {
            get;
            set;
        }

        public string MimeType
        {
            get;
            set;
        }

        public int? Length
        {
            get;
            set;
        }

        public DateTime? CreationTime
        {
            get;
            set;
        }

        public DateTime? LastWriteTime
        {
            get;
            set;
        }

        public bool IsReadOnly
        {
            get;
            set;
        }


        public DataSourceId DataSourceId
        {
            get { return _dataSourceId; }
        }
    }
}
