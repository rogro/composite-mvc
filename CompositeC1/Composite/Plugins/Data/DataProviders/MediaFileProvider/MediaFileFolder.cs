/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using Composite.Data.Types;
using Composite.Data;


namespace Composite.Plugins.Data.DataProviders.MediaFileProvider
{
    internal sealed class MediaFileFolder : IMediaFileFolder
    {
        private readonly DataSourceId _dataSourceId;

        public MediaFileFolder(IMediaFolderData folder, string storeId, DataSourceId dataSourceId)
        {
            _dataSourceId = dataSourceId;

            Id = folder.Id;
            Description = folder.Description;
            Title = folder.Title;
            StoreId = storeId;
            Path = folder.Path;
        }

        public Guid Id
        {
            get; private set;
        }

        public string KeyPath
        {
            get { return this.GetKeyPath(); }
        }

        public string CompositePath
        {
            get { return this.GetCompositePath(); }
            set { throw new NotImplementedException(); }
        }

        public string StoreId
        {
            get;
            set;
        }

        public string Path
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public bool IsReadOnly
        {
            get;
            set;
        }

        public DataSourceId DataSourceId
        {
            get { return _dataSourceId; }
        }
    }
}
