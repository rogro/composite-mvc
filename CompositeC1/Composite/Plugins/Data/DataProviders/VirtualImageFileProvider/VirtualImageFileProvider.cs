/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Composite.Core.Linq;
using Composite.Data;
using Composite.Data.Caching;
using Composite.Data.Plugins.DataProvider;
using Composite.Data.Types;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;


namespace Composite.Plugins.Data.DataProviders.VirtualImageFileProvider
{
    [ConfigurationElementType(typeof(NonConfigurableDataProvider))]
    internal sealed class VirtualImageFileProvider : IDataProvider
	{
        public DataProviderContext Context
        {
            set { ; }
        }

        public IEnumerable<Type> GetSupportedInterfaces()
        {
            return new List<Type> { typeof(IImageFile) };
        }

        public IQueryable<T> GetData<T>() where T : class, IData
        {
            if (typeof(T) != typeof(IImageFile)) throw new InvalidOperationException( "Unsupported data interface" );

            return new VirtualImageFileQueryable<T>(
                from mediaFile in DataFacade.GetData<IMediaFile>()
                where mediaFile.MimeType != null && mediaFile.MimeType.StartsWith("image")
                select new VirtualImageFile(mediaFile) as T);
        }

        public T GetData<T>(IDataId dataId) where T : class, IData
        {
            throw new NotImplementedException("Unexpected call. This provider does not produce its own data source id's");
        }
    }
}
