/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using Composite.Data.Types;
using Composite.Data;

namespace Composite.Plugins.Data.DataProviders.VirtualImageFileProvider
{
	internal sealed class VirtualImageFile : IImageFile
	{
        private IMediaFile _sourceFile;

        internal VirtualImageFile(IMediaFile sourceFile)
        {
            _sourceFile = sourceFile;
        }



        public string CompositePath
        {
            get { return this.GetCompositePath(); }
            set { throw new NotImplementedException(); }
        }



        public string FolderPath
        {
            get
            {
                return _sourceFile.FolderPath;
            }
            set
            {
                _sourceFile.FolderPath = value; ;
            }
        }

        public string FileName
        {
            get
            {
                return _sourceFile.FileName;
            }
            set
            {
                _sourceFile.FileName = value; ;
            }
        }

        public DataSourceId DataSourceId
        {
            get { return _sourceFile.DataSourceId; }
        }

        public string StoreId
        {
            get
            {
                return _sourceFile.StoreId;
            }
            set
            {
                _sourceFile.StoreId = value;;
            }
        }

        public string Title
        {
            get
            {
                return _sourceFile.Title;
            }
            set
            {
                _sourceFile.Title = value;;
            }
        }

        public string Description
        {
            get
            {
                return _sourceFile.Description;
            }
            set
            {
                _sourceFile.Description = value;;
            }
        }

        public string Culture
        {
            get
            {
                return _sourceFile.Culture;
            }
            set
            {
                _sourceFile.Culture = value;;
            }
        }

        public string MimeType
        {
            get { return _sourceFile.MimeType; }
        }

        public int? Length
        {
            get { return _sourceFile.Length; }
        }

        public DateTime? CreationTime
        {
            get { return _sourceFile.CreationTime; }
        }

        public DateTime? LastWriteTime
        {
            get { return _sourceFile.LastWriteTime; }
        }

        public bool IsReadOnly
        {
            get { return _sourceFile.IsReadOnly; }
        }

        public Guid Id
        {
            get { return _sourceFile.Id; }
        }

        public string KeyPath
        {
            get { return _sourceFile.KeyPath; }
        }
    }
}
