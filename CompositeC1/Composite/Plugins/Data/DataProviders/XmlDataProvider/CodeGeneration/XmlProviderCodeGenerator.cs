/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using Composite.Data.DynamicTypes;
using Composite.Plugins.Data.DataProviders.XmlDataProvider.Foundation;


namespace Composite.Plugins.Data.DataProviders.XmlDataProvider.CodeGeneration
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [EditorBrowsable(EditorBrowsableState.Never)] 
    internal class XmlProviderCodeGenerator
    {
        private readonly DataTypeDescriptor _dataTypeDescriptor;

        public string DataProviderHelperClassFullName { get; private set; }
        public string WrapperClassFullName { get; private set; }
        public string DataIdClassFullName { get; private set; }


        public XmlProviderCodeGenerator(DataTypeDescriptor dataTypeDescriptor, string namespaceName)
        {
            _dataTypeDescriptor = dataTypeDescriptor;

            DataProviderHelperClassFullName = namespaceName + "." + NamesCreator.MakeDataProviderHelperClassName(dataTypeDescriptor);
            WrapperClassFullName = namespaceName + "." + NamesCreator.MakeWrapperClassName(dataTypeDescriptor);
            DataIdClassFullName = namespaceName + "." + NamesCreator.MakeDataIdClassName(dataTypeDescriptor);
        }



        public IEnumerable<CodeTypeDeclaration> CreateCodeDOMs()
        {
            string dataProviderHelperClassName = NamesCreator.MakeDataProviderHelperClassName(_dataTypeDescriptor);
            string wrapperClassName = NamesCreator.MakeWrapperClassName(_dataTypeDescriptor);
            string dataIdClassName = NamesCreator.MakeDataIdClassName(_dataTypeDescriptor);


            DataProviderHelperClassGenerator classGenerator = new DataProviderHelperClassGenerator(
                dataProviderHelperClassName,
                wrapperClassName,
                dataIdClassName,
                _dataTypeDescriptor                
            );
            CodeTypeDeclaration dataHelperClassCodeTypeDeclaration = classGenerator.CreateClass();
            yield return dataHelperClassCodeTypeDeclaration;


            DataIdClassGenerator dataIdClassGenerator = new DataIdClassGenerator(dataIdClassName, _dataTypeDescriptor);
            CodeTypeDeclaration dataIdClassCodeTypeDeclaration = dataIdClassGenerator.CreateClass();
            yield return dataIdClassCodeTypeDeclaration;


            DataWrapperClassGenerator dataWrapperClassGenerator = new DataWrapperClassGenerator(wrapperClassName, _dataTypeDescriptor);
            CodeTypeDeclaration dataWrapperClassCodeTypeDeclaration = dataWrapperClassGenerator.CreateClass();
            yield return dataWrapperClassCodeTypeDeclaration;
        }
    }
}
