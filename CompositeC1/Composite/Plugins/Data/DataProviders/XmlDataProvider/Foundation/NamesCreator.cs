/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.Core.Extensions;
using Composite.Data;
using Composite.Data.DynamicTypes;


namespace Composite.Plugins.Data.DataProviders.XmlDataProvider.Foundation
{
    internal static class NamesCreator
    {
        internal static string MakeFileName(DataTypeDescriptor dataTypeDescriptor, DataScopeIdentifier dataScopeIdentifier, string cultureName)
        {
            string typeFullName = StringExtensionMethods.CreateNamespace(dataTypeDescriptor.Namespace, dataTypeDescriptor.Name, '.');

            string publicationScopePart = "";

            switch (dataScopeIdentifier.Name)
            {
                case DataScopeIdentifier.PublicName:
                    break;
                case DataScopeIdentifier.AdministratedName:
                    publicationScopePart = "_" + PublicationScope.Unpublished;
                    break;
                default:
                    throw new InvalidOperationException("Unsupported data scope identifier: '{0}'".FormatWith(dataScopeIdentifier.Name));
            }

            string cultureNamePart = "";

            if (cultureName != "")
            {
                cultureNamePart = "_" + cultureName;
            }
                
            return typeFullName + publicationScopePart + cultureNamePart + ".xml";
        }



        internal static string MakeElementName(DataTypeDescriptor dataTypeDescriptor)
        {
            string name = dataTypeDescriptor.Name;

            if (name.StartsWith("I"))
            {
                name = name.Remove(0, 1);
                name = string.Format("{0}{1}Elements", name.Substring(0, 1).ToUpper(), name.Remove(0, 1));
            }

            return name;
        }



        internal static string MakeWrapperClassName(DataTypeDescriptor dataTypeDescriptor)
        {
            return string.Format("{0}Wrapper", dataTypeDescriptor.GetFullInterfaceName().Replace('.', '_').Replace('+', '_'));
        }



        internal static string MakeDataIdClassName(DataTypeDescriptor dataTypeDescriptor)
        {
            return string.Format("{0}DataId", dataTypeDescriptor.GetFullInterfaceName().Replace('.', '_').Replace('+', '_'));
        }



        internal static string MakeDataProviderHelperClassName(DataTypeDescriptor dataTypeDescriptor)
        {
            return string.Format("{0}DataProviderHelper", dataTypeDescriptor.GetFullInterfaceName().Replace('.', '_').Replace('+', '_'));
        }



        internal static string MakeNamespaceName(string providerName)
        {
            return "CompositeGenerated." + providerName;
        }
    }
}
