/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;
using System.Reflection;
using Composite.Data;
using Composite.Data.Caching;
using Composite.Data.DynamicTypes;
using Composite.Core.Logging;
using Composite.Core.Extensions;
using Composite.Core.Types;

namespace Composite.Plugins.Data.DataProviders.XmlDataProvider.Foundation
{
    internal static class ValidationHelper
    {
        private class ValidationInfo
        {
            public List<Pair<PropertyInfo, int>> MaxStringLengthByField;
        }

        private static readonly Cache<Type, ValidationInfo> _validationInfoCache = new Cache<Type, ValidationInfo>("XmlDataProvider.ValidationInfo", 200);
        private static readonly object[] EmptyParameterList = new object[0];

        public static void Validate(IData datum)
        {
            Type type = datum.DataSourceId.InterfaceType;

            ValidationInfo info = _validationInfoCache.Get(type);
            if(info == null)
            {
                lock(_validationInfoCache)
                {
                    info = _validationInfoCache.Get(type);
                    if(info == null)
                    {
                        info = new ValidationInfo { MaxStringLengthByField = new List<Pair<PropertyInfo, int>>() };

                        foreach(PropertyInfo propertyInfo in type.GetProperties())
                        {
                            if(propertyInfo.PropertyType != typeof(string)) continue;

                            object[] attributes = propertyInfo.GetCustomAttributes(typeof(StoreFieldTypeAttribute), false);
                            if(attributes.Length > 1)
                            {
                                LoggingService.LogError(typeof(ValidationHelper).Name, "Encoutered more than one '{0}' attrubute".FormatWith(typeof(StoreFieldTypeAttribute).FullName));
                                continue;
                            }

                            if (attributes.Length == 0) continue;


                            var storeFieldTypeAttr = attributes[0] as StoreFieldTypeAttribute;
                            StoreFieldType storeFieldType = storeFieldTypeAttr.StoreFieldType;

                            if (storeFieldType.IsLargeString || !storeFieldType.IsString) 
                            {
                                continue;
                            }

                            info.MaxStringLengthByField.Add(new Pair<PropertyInfo, int>(propertyInfo, storeFieldType.MaximumLength));
                        }

                        _validationInfoCache.Add(type, info);
                    }
                }
            }

            foreach(Pair<PropertyInfo, int> pair in info.MaxStringLengthByField)
            {
                string fieldValue = pair.First.GetValue(datum, EmptyParameterList) as string;
                if (fieldValue != null && fieldValue.Length > pair.Second)
                {
                    Verify.ThrowInvalidOperationException("Constraint violation. Value for field '{0}' on data type '{1}' is longer than {2} symbols.".FormatWith(pair.First.Name, type.FullName, pair.Second));
                }
            }
        }
    }
}
