/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;
using Composite.Data;
using Composite.Data.Types;
using Composite.Functions;
using Composite.Functions.Plugins.FunctionProvider;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ObjectBuilder;
using Microsoft.Practices.ObjectBuilder;


namespace Composite.Plugins.Functions.FunctionProviders.SqlFunctionProvider
{
    [ConfigurationElementType(typeof(SqlFunctionProviderData))]
    internal sealed class SqlFunctionProvider : IFunctionProvider
    {
        private FunctionNotifier _functionNotifier;


        public SqlFunctionProvider()
        {
            DataEventSystemFacade.SubscribeToStoreChanged<ISqlFunctionInfo>(OnDataChanged, false);
        }



        public FunctionNotifier FunctionNotifier
        {
            set { _functionNotifier = value; }
        }



        public IEnumerable<IFunction> Functions
        {
            get 
            {
                IEnumerable<ISqlFunctionInfo> functionInfos = DataFacade.GetData<ISqlFunctionInfo>();

                IList<IFunction> functions = new List<IFunction>();
                foreach (ISqlFunctionInfo function in functionInfos)
                {
                    functions.Add(new SqlFunction(function));
                }
                return functions;
            }
        }



        private void OnDataChanged(object sender, StoreEventArgs storeEventArgs)
        {
            _functionNotifier.FunctionsUpdated();
        }
    }



    [Assembler(typeof(SqlFunctionProviderAssembler))]
    internal sealed class SqlFunctionProviderData : FunctionProviderData
    {
    }



    internal sealed class SqlFunctionProviderAssembler : IAssembler<IFunctionProvider, FunctionProviderData>
    {
        public IFunctionProvider Assemble(IBuilderContext context, FunctionProviderData objectConfiguration, IConfigurationSource configurationSource, ConfigurationReflectionCache reflectionCache)
        {
            return new SqlFunctionProvider();
        }
    }
}
