/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Web.WebPages;
using Composite.AspNet.Razor;
using Composite.Core.WebClient;
using Composite.Functions;
using Composite.Plugins.Functions.FunctionProviders.FileBasedFunctionProvider;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace Composite.Plugins.Functions.FunctionProviders.RazorFunctionProvider
{
	[ConfigurationElementType(typeof(RazorFunctionProviderData))]
    internal class RazorFunctionProvider : FileBasedFunctionProvider.FileBasedFunctionProvider<RazorBasedFunction>
	{
		protected override string FileExtension
		{
			get { return "cshtml"; }
		}

        protected override string DefaultFunctionNamespace
        {
            get { return "Razor"; }
        }


		public RazorFunctionProvider(string name, string folder) : base(name, folder) { }


		override protected IFunction InstantiateFunction(string virtualPath, string @namespace, string name)
		{
		    WebPageBase razorPage;
            using(BuildManagerHelper.DisableUrlMetadataCachingScope())
            {
                razorPage = WebPage.CreateInstanceFromVirtualPath(virtualPath);
            }

            if(!(razorPage is RazorFunction))
            {
                return null;
            }

		    RazorFunction razorFunction = razorPage as RazorFunction;

		    var functionParameters = FunctionBasedFunctionProviderHelper.GetParameters(razorFunction, typeof(RazorFunction), virtualPath);

            return new RazorBasedFunction(@namespace, name, razorFunction.FunctionDescription, functionParameters, razorFunction.FunctionReturnType, virtualPath, this);
		}

        protected override IFunction InstantiateFunctionFromCache(string virtualPath, string @namespace, string name, Type returnType, string cachedDescription)
        {
            if (returnType != null)
            {
                return new RazorBasedFunction(@namespace, name, cachedDescription, returnType, virtualPath, this);
            }

            return InstantiateFunction(virtualPath, @namespace, name);
        }

		protected override bool HandleChange(string path)
		{
            return path.EndsWith(FileExtension);
		}
    }
}
