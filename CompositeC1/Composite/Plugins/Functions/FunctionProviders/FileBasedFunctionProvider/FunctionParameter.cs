/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Reflection;
using Composite.Core.Extensions;
using Composite.Functions;

namespace Composite.Plugins.Functions.FunctionProviders.FileBasedFunctionProvider
{
    /// <summary>
    /// Represents a function parameter
    /// </summary>
	public class FunctionParameter
	{
        /// <summary>
        /// Gets the parameter name.
        /// </summary>
		public string Name { get; private set; }

        /// <summary>
        /// Gets the parameter type.
        /// </summary>
		public Type Type { get; private set; }

        /// <summary>
        /// Gets the widget provider.
        /// </summary>
		public WidgetFunctionProvider WidgetProvider { get; private set; }

        /// <summary>
        /// Gets the function parameter attribute.
        /// </summary>
        /// <value>
        /// The attribute.
        /// </value>
		public FunctionParameterAttribute Attribute { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FunctionParameter"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="type">The type.</param>
        /// <param name="functionParameterAttribute">The function parameter attribute.</param>
        /// <param name="widgetProvider">The widget provider.</param>
		public FunctionParameter(string name, Type type, FunctionParameterAttribute functionParameterAttribute, WidgetFunctionProvider widgetProvider)
		{
			Name = name;
			Type = type;
            Attribute = functionParameterAttribute;

			WidgetProvider = widgetProvider;
		}

        /// <summary>
        /// Sets the parameter value.
        /// </summary>
        /// <param name="functionObject">The function object.</param>
        /// <param name="parameterValue">The parameter value.</param>
		public void SetValue(object functionObject, object parameterValue)
        {
            GetParameterProperty(functionObject.GetType(), Name).SetValue(functionObject, parameterValue, null);
		}

        private static PropertyInfo GetParameterProperty(Type type, string propertyName)
        {
            var bindingFlags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.SetProperty | BindingFlags.DeclaredOnly;

            Type currentType = type;

            while (currentType != typeof(Type))
            {
                var property = currentType.GetProperty(propertyName, bindingFlags);

                if (property != null) return property;

                currentType = currentType.BaseType;
            }

            throw new InvalidOperationException("Failed to find parameter property '{0}' on type '{1}'".FormatWith(propertyName, type.FullName));
        }
	}
}
