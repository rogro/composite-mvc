/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web;
using Composite.AspNet.Security;
using Composite.C1Console.Security;
using Composite.Core.IO;
using Composite.Core.Xml;
using Composite.Functions;
using Composite.Core.Extensions;

namespace Composite.Plugins.Functions.FunctionProviders.FileBasedFunctionProvider
{
	internal abstract class FileBasedFunction<T> : IFunction where T : FileBasedFunction<T>
	{
		private readonly FileBasedFunctionProvider<T> _provider;

		internal string VirtualPath { get; private set; }
		protected IDictionary<string, FunctionParameter> Parameters { get; set; }

		public string Namespace { get; private set; }
		public string Name { get; private set; }
		public Type ReturnType { get; private set; }
		public virtual string Description { get; private set; }

		public EntityToken EntityToken
		{
			get { return new FileBasedFunctionEntityToken(_provider.Name, String.Join(".", Namespace, Name)); }
		}

	    protected abstract void InitializeParameters();

		public virtual IEnumerable<ParameterProfile> ParameterProfiles
		{
			get
			{
                if (Parameters == null)
                {
                    lock (this)
                    {
                        if (Parameters == null)
                        {
                            try
                            {
                                InitializeParameters();
                            }
                            catch (HttpException ex)
                            {
                                EmbedSourceCodeInformation(ex);
                                throw;
                            }
                            
                            Verify.IsNotNull(Parameters, "Parameters collection is null");
                        }
                    }
			    }
			    
			    foreach (var param in Parameters.Values)
			    {
			        BaseValueProvider defaultValueProvider = new NoValueValueProvider();
			        WidgetFunctionProvider widgetProvider = null;
			        string label = param.Name;
			        bool isRequired = true;
			        string helpText = String.Empty;
			        bool hideInSimpleView = false;

			        if (param.Attribute != null)
			        {
			            if (!param.Attribute.Label.IsNullOrEmpty())
			            {
			                label = param.Attribute.Label;
			            }

			            if (!param.Attribute.Help.IsNullOrEmpty())
			            {
			                helpText = param.Attribute.Help;
			            }

			            isRequired = !param.Attribute.HasDefaultValue;
			            if (!isRequired)
			            {
			                defaultValueProvider = new ConstantValueProvider(param.Attribute.DefaultValue);
			            }

			            widgetProvider = param.WidgetProvider;

			            hideInSimpleView = param.Attribute.HideInSimpleView;
			        }

			        if (widgetProvider == null)
			        {
			            widgetProvider = StandardWidgetFunctions.GetDefaultWidgetFunctionProviderByType(param.Type, isRequired);
			        }

			        yield return new ParameterProfile(param.Name, param.Type, isRequired, defaultValueProvider, widgetProvider, label,
                        new HelpDefinition(helpText),
                        hideInSimpleView);
			        
			    }
			}
		}

		protected FileBasedFunction(string ns, string name, string description, IDictionary<string, FunctionParameter> parameters, Type returnType, string virtualPath, FileBasedFunctionProvider<T> provider)
            :this(ns, name, description, returnType, virtualPath, provider)
		{
			Parameters = parameters;
		}

        protected FileBasedFunction(string ns, string name, string description, Type returnType, string virtualPath, FileBasedFunctionProvider<T> provider)
        {
            _provider = provider;

            Namespace = ns;
            Name = name;
            Description = description;
            ReturnType = returnType;
            VirtualPath = virtualPath;
        }

        protected void EmbedSourceCodeInformation(HttpException ex)
        {
            if (ex is HttpParseException)
            {
                EmbedSourceCodeInformation(ex as HttpParseException);
            }
            else if (ex is HttpCompileException)
            {
                EmbedSourceCodeInformation(ex as HttpCompileException);
            }
        }
	    
        private void EmbedSourceCodeInformation(HttpParseException ex)
	    {
            // Not showing source code of not related files
	        if (!ex.FileName.StartsWith(PathUtil.Resolve(VirtualPath), StringComparison.OrdinalIgnoreCase))
	        {
	            return;
	        }

            string[] sourceCode = C1File.ReadAllLines(ex.FileName);

            XhtmlErrorFormatter.EmbedSouceCodeInformation(ex, sourceCode, ex.Line);
        }

        private void EmbedSourceCodeInformation(HttpCompileException ex)
        {
            var compilationErrors = ex.Results.Errors;
            if (!compilationErrors.HasErrors)
            {
                return;
            }
            
            CompilerError firstError = null;
            
            for (int i = 0; i < compilationErrors.Count; i++)
            {
                if (!compilationErrors[i].IsWarning)
                {
                    firstError = compilationErrors[i];
                    break;
                }
            }

            Verify.IsNotNull(firstError, "Failed to finding an error in the compiler results.");

            // Not showing source code of not related files
	        if (!firstError.FileName.StartsWith(PathUtil.Resolve(VirtualPath), StringComparison.OrdinalIgnoreCase))
	        {
	            return;
	        }

	        string[] sourceCode = C1File.ReadAllLines(firstError.FileName);

	        XhtmlErrorFormatter.EmbedSouceCodeInformation(ex, sourceCode, firstError.Line);
        }

		public abstract object Execute(ParameterList parameters, FunctionContextContainer context);
	}
}
