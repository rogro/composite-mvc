/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Hosting;
using Composite.Core.Extensions;
using Composite.Functions;

namespace Composite.Plugins.Functions.FunctionProviders.FileBasedFunctionProvider
{
    internal class NotLoadedFileBasedFunction<T> : FileBasedFunction<T>, IFunctionInitializationInfo where T : FileBasedFunction<T>
    {
        private readonly Exception _exception;

        public NotLoadedFileBasedFunction(
            FileBasedFunctionProvider<T> provider,
            string @namespace, 
            string functionName,
            string virtualPath,
            Exception exception): 
            base(@namespace, functionName, string.Empty, null, typeof(void), virtualPath, provider)
        {
            _exception = exception;
        }

        public override string Description {
            get {
                return  (_exception != null) ? RemoveApplicationPath(_exception.Message) : base.Description;
            }
        }

        private static string RemoveApplicationPath(string compilationErrorMessage)
        {
            if (compilationErrorMessage.StartsWith(HostingEnvironment.ApplicationPhysicalPath, StringComparison.OrdinalIgnoreCase))
            {
                return compilationErrorMessage.Substring(HostingEnvironment.ApplicationPhysicalPath.Length - 1);
            }

            return compilationErrorMessage;
        }

        override public object Execute(ParameterList parameters, FunctionContextContainer context)
        {
            if (_exception is HttpException)
            {
                EmbedSourceCodeInformation(_exception as HttpException);
            }

            throw new InvalidOperationException("Failed to load function '{0}'".FormatWith(this.CompositeName()), _exception);
        }

        Type IMetaFunction.ReturnType
        {
            get { return typeof(void); }
        }

        protected override void InitializeParameters()
        {
            Parameters = new Dictionary<string, FunctionParameter>();
        }

        IEnumerable<ParameterProfile> IMetaFunction.ParameterProfiles
        {
            get
            {
                return new ParameterProfile[0];
            }
        }

        bool IFunctionInitializationInfo.FunctionInitializedCorrectly
        {
            get { return false; }
        }
    }
}
