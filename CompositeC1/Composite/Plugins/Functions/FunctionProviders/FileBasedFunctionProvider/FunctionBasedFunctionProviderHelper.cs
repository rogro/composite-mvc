/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Composite.Core;
using Composite.Core.Extensions;
using Composite.Functions;

namespace Composite.Plugins.Functions.FunctionProviders.FileBasedFunctionProvider
{
    /// <summary>
    /// Helper class for developing implementations of FileBasedFunctionProvider
    /// </summary>
    public static class FunctionBasedFunctionProviderHelper
    {
        private static readonly string LogTitle = typeof(FunctionBasedFunctionProviderHelper).FullName;

        /// <summary>
        /// Gets the function description from the <see cref="FunctionAttribute" />.
        /// </summary>
        /// <param name="functionName">Name of the function.</param>
        /// <param name="functionObject">The object that represents a function.</param>
        /// <returns></returns>
        public static string GetDescription(string functionName, object functionObject)
        {
            var attr = functionObject.GetType()
                                     .GetCustomAttributes(typeof(FunctionAttribute), false)
                                     .Cast<FunctionAttribute>()
                                     .FirstOrDefault();
            if (attr != null)
            {
                return attr.Description;
            }

            return String.Format("A {0} function", functionName);
        }

        /// <summary>
        /// Extracts the function paramteres from an object that represents a function.
        /// </summary>
        /// <param name="functionObject">The object that represents a function.</param>
        /// <param name="baseFunctionType">Type of the base function.</param>
        /// <param name="filePath">Physical file location of the file behind the function, used for logging.</param>
        /// <returns></returns>
        public static IDictionary<string, FunctionParameter> GetParameters(object functionObject, Type baseFunctionType, string filePath)
        {
            var dict = new Dictionary<string, FunctionParameter>();

            var type = functionObject.GetType();
            while (type != null &&
                    baseFunctionType.IsInterface
                        ? type.BaseType != null && type.BaseType.GetInterfaces().Contains(baseFunctionType)
                        : baseFunctionType != type
                )
            {
                var properties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty | BindingFlags.DeclaredOnly);
                foreach (var property in properties)
                {
                    // Skipping overriden base properties
                    if (property.GetAccessors()[0].GetBaseDefinition().DeclaringType == baseFunctionType) continue;
                    // Skipping private setters
                    if (property.GetSetMethod(false) == null) continue;
                    // Skipping explicitly ignored attributes
                    if (property.GetCustomAttributes(typeof(FunctionParameterIgnoreAttribute), false).Any()) continue;


                    var propType = property.PropertyType;
                    var name = property.Name;

                    FunctionParameterAttribute attr = null;
                    var attributes = property.GetCustomAttributes(typeof(FunctionParameterAttribute), false).Cast<FunctionParameterAttribute>().ToList();


                    if (attributes.Count > 1)
                    {
                        Log.LogWarning(LogTitle, "More than one '{0}' attribute defined on property '{1}'. Location: '{2}'"
                                                 .FormatWith(typeof(FunctionParameterAttribute).Name, name, filePath));
                    }
                    else
                    {
                        attr = attributes.FirstOrDefault();
                    }

                    WidgetFunctionProvider widgetProvider = null;

                    if (attr != null && attr.HasWidgetMarkup)
                    {
                        try
                        {
                            widgetProvider = attr.GetWidgetFunctionProvider(type, property);
                        }
                        catch (Exception ex)
                        {
                            Log.LogWarning(LogTitle, "Failed to get widget function provider for parameter property {0}. Location: '{1}'"
                                                     .FormatWith(property.Name, filePath));
                            Log.LogWarning(LogTitle, ex);
                        }
                    }

                    if (!dict.ContainsKey(name))
                    {
                        dict.Add(name, new FunctionParameter(name, propType, attr, widgetProvider));
                    }
                }

                type = type.BaseType;
            }

            return dict;
        }
    }
}
