/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Xml.Linq;
using Composite.Core.Routing;
using Composite.Data;
using Composite.Data.Types;
using Composite.Functions;
using Composite.Core.WebClient.Renderings.Page;
using Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Foundation;

namespace Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Pages
{
    /// <summary>
    /// Gets information about current page in all the languages.
    /// </summary>
	internal class GetForeignPageInfoFunction: StandardFunctionBase
	{
        public GetForeignPageInfoFunction(EntityTokenFactory entityTokenFactory)
            : base("GetForeignPageInfo", "Composite.Pages", typeof(IEnumerable<XElement>), entityTokenFactory)
        {
        }


        public override object Execute(ParameterList parameters, FunctionContextContainer context)
        {
            return ExecuteInternal();
        }

        private static IEnumerable<XElement> ExecuteInternal()
        {
            // Grab all active languages...
            foreach (CultureInfo culture in DataLocalizationFacade.ActiveLocalizationCultures)
            {
                XElement annotatedMatch;

                // enter the 'data scope' of the next language
                using (new DataScope(culture))
                {
                    // fetch sitemap element for current page - if any
                    IPage match = PageManager.GetPageById(PageRenderer.CurrentPageId);

                    if (match == null)
                    {
                        continue;
                    }

                    annotatedMatch = new XElement("LanguageVersion"
                            , new XAttribute("Culture", culture.Name)
                            , new XAttribute("CurrentCulture", culture.Equals(Thread.CurrentThread.CurrentCulture))
                            , new XAttribute("Id", match.Id)
                            , new XAttribute("Title", match.Title)
                            , (match.MenuTitle == null ? null : new XAttribute("MenuTitle", match.MenuTitle))
                            , new XAttribute("UrlTitle", match.UrlTitle)
                            , new XAttribute("Description", match.Description)
                            , new XAttribute("URL", PageUrls.BuildUrl(match))
                            );

                }

                yield return annotatedMatch;
            }
        }
	}
}
