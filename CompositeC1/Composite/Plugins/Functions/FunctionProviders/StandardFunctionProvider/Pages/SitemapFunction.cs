/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Xml;
using System.Xml.Linq;
using System.Xml.Xsl;
using Composite.Core.WebClient.Renderings.Page;
using Composite.Core.Xml;
using Composite.Functions;
using Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Foundation;

namespace Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Pages
{
	internal sealed class SitemapFunction : StandardFunctionBase
    {
        #region XSLT constants
        const string _sitemapXslTemplate = @"<?xml version=""1.0"" encoding=""UTF-8""?>
<xsl:stylesheet version=""1.0"" xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" exclude-result-prefixes=""xsl""
	xmlns=""http://www.w3.org/1999/xhtml"">

	<xsl:template match=""/"">
		<html>
			<head>
				<style id=""CompositePagesXhtmlSitemapStyle"">
                    ul#Sitemap, ul#Sitemap ul {
                        list-style: none;
                        margin: 0;
                        padding: 0;
                    }


                    ul#Sitemap ul {
                        margin-left: 0.8em;
                    }

                    ul#Sitemap li {
                        padding: 0;
                    }

                    #Sitemap a {
                        text-decoration: none;
                    }

                    #Sitemap a.sitemapCurrentPage
                    {
                        font-weight: bold;
                    }

                    #Sitemap a.sitemapOpenPage
                    {
                        font-style: italic;
                    }
				</style>
			</head>
			<body>
				<ul id=""Sitemap"">
					<xsl:apply-templates select=""/sitemap/Page"" />
				</ul>
			</body>
		</html>
	</xsl:template>
	
	<xsl:template match=""Page[@MenuTitle]"">
		<li>
			<xsl:apply-templates mode=""classAttribute"" select=""."" />
			<a href=""{@URL}"">
				<xsl:apply-templates mode=""classAttribute"" select=""."" />
				<xsl:value-of select=""@MenuTitle"" />
			</a>
			<xsl:if test=""count(Page)&gt;0"">
				<ul>
					<xsl:apply-templates select=""Page"" />
				</ul>
			</xsl:if>
		</li>
	</xsl:template>


	<xsl:template mode=""classAttribute"" match=""Page"">
		<xsl:choose>
			<xsl:when test=""@iscurrent='true'"">
				<xsl:attribute name=""class"">sitemapCurrentPage</xsl:attribute>
			</xsl:when>
			<xsl:when test=""@isopen='true'"">
				<xsl:attribute name=""class"">sitemapOpenPage</xsl:attribute>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>
";

        #endregion

        private static object _lock = new object();
        private static XslCompiledTransform xslt = null;

        private static XslCompiledTransform GetCompiledXslt()
        {
            lock (_lock)
            {
                if (xslt == null)
                {
                    xslt = new XslCompiledTransform();

                    XElement xsltSource = XElement.Parse(_sitemapXslTemplate);

                    xslt.Load(xsltSource.CreateReader());

                }
                return xslt;
            }
        }



        private EntityTokenFactory _entityTokenFactory;

        public SitemapFunction(EntityTokenFactory entityTokenFactory)
            : base("QuickSitemap", "Composite.Pages", typeof(XhtmlDocument), entityTokenFactory)
        {
            _entityTokenFactory = entityTokenFactory;
        }

        public override object Execute(ParameterList parameters, FunctionContextContainer context)
        {
            XDocument xmlSitemap = new XDocument( new XElement("sitemap", PageStructureInfo.GetSiteMapWithActivePageAnnotations()));

            XDocument xhtmlSitemap = new XDocument();
            using (XmlWriter writer = xhtmlSitemap.CreateWriter())
            {
                GetCompiledXslt().Transform(xmlSitemap.CreateReader(), writer);
            }

            return new XhtmlDocument(xhtmlSitemap);
        }


    }
}
