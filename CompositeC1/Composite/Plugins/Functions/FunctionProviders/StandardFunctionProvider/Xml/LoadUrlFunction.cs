/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;
using System.Xml.Linq;
using Composite.Core.Instrumentation;
using Composite.Core.Xml;
using Composite.Functions;
using Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Foundation;

namespace Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Xml
{
    internal sealed class LoadUrlFunction : StandardFunctionBase
    {
        public LoadUrlFunction(EntityTokenFactory entityTokenFactory)
            : base("LoadUrl", "Composite.Xml", typeof(XElement), entityTokenFactory)
        {
        }

        public override object Execute(ParameterList parameters, FunctionContextContainer context)
        {
            string url = parameters.GetParameter<string>("Url");

            bool cachingEnabled = false;

            int cachePeriod;
            if (parameters.TryGetParameter("CacheTime", out cachePeriod))
            {
                cachingEnabled = cachePeriod > 0;
            }

            string cacheKey = null;
            if (cachingEnabled)
            {
                cacheKey = typeof(LoadUrlFunction).FullName + "|" + url;
                var cachedValue = HttpRuntime.Cache.Get(cacheKey) as XElement;
                if (cachedValue != null)
                {
                    return cachedValue;
                }
            }

            using (TimerProfilerFacade.CreateTimerProfiler(url))
            {
                XElement value = XElementUtils.Load(url);

                if (cachingEnabled)
                {
                    HttpRuntime.Cache.Add(cacheKey, value, null, DateTime.Now.AddSeconds(cachePeriod),
                                          Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
                }

                return value;
            }
        }


        protected override IEnumerable<StandardFunctionParameterProfile> StandardFunctionParameterProfiles
        {
            get
            {
                WidgetFunctionProvider textboxWidget = StandardWidgetFunctions.TextBoxWidget;

                yield return new StandardFunctionParameterProfile(
                    "Url", typeof(string), true, new NoValueValueProvider(),textboxWidget);

                yield return new StandardFunctionParameterProfile(
                    "CacheTime", typeof(int), false, new ConstantValueProvider(0), textboxWidget);
            }
        }
    }
}
