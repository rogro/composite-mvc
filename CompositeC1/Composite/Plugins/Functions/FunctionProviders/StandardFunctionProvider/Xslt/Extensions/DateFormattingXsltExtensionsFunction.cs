/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using Composite.Functions;
using Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Foundation;
using Composite.Core.Xml;
using System.Xml;

namespace Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Xslt.Extensions
{
    internal sealed class DateFormattingXsltExtensionsFunction : StandardFunctionBase
    {
        public DateFormattingXsltExtensionsFunction(EntityTokenFactory entityTokenFactory)
            : base("DateFormatting", "Composite.Xslt.Extensions", typeof(IXsltExtensionDefinition), entityTokenFactory)
        {
        }


        public override object Execute(ParameterList parameters, FunctionContextContainer context)
        {
            return new XsltExtensionDefinition<DateFormattingXsltExtensions>
            {
                EntensionObject = new DateFormattingXsltExtensions(),
                ExtensionNamespace = "#dateExtensions"
            };
        }


        internal class DateFormattingXsltExtensions
        {
            public string Now()
            {
                return XmlConvert.ToString(DateTime.Now, XmlDateTimeSerializationMode.Local);
            }

            public string LongDateFormat(string xmlFormattedDate)
            {
                DateTime date = XmlConvert.ToDateTime(xmlFormattedDate, XmlDateTimeSerializationMode.Local);
                return date.ToLongDateString();
            }

            public string LongTimeFormat(string xmlFormattedDate)
            {
                DateTime date = XmlConvert.ToDateTime(xmlFormattedDate, XmlDateTimeSerializationMode.Local);
                return date.ToLongTimeString();
            }

            public string ShortDateFormat(string xmlFormattedDate)
            {
                DateTime date = XmlConvert.ToDateTime(xmlFormattedDate, XmlDateTimeSerializationMode.Local);
                return date.ToShortDateString();
            }

            public string ShortTimeFormat(string xmlFormattedDate)
            {
                DateTime date = XmlConvert.ToDateTime(xmlFormattedDate, XmlDateTimeSerializationMode.Local);
                return date.ToShortTimeString();
            }

            public int Day(string xmlFormattedDate)
            {
                DateTime date = XmlConvert.ToDateTime(xmlFormattedDate, XmlDateTimeSerializationMode.Local);
                return date.Day;
            }

            public int Month(string xmlFormattedDate)
            {
                DateTime date = XmlConvert.ToDateTime(xmlFormattedDate, XmlDateTimeSerializationMode.Local);
                return date.Month;
            }

            public int Year(string xmlFormattedDate)
            {
                DateTime date = XmlConvert.ToDateTime(xmlFormattedDate, XmlDateTimeSerializationMode.Local);
                return date.Year;
            }


            public string LongMonthName(int monthNumber)
            {
                if (monthNumber < 1 || monthNumber > 12) throw new ArgumentOutOfRangeException("monthNumber");

                DateTime date = new DateTime(1, monthNumber, 1);

                return date.ToString("MMMM");
            }


            public string ShortMonthName(int monthNumber)
            {
                if (monthNumber < 1 || monthNumber > 12) throw new ArgumentOutOfRangeException("monthNumber");

                DateTime date = new DateTime(1, monthNumber, 1);

                return date.ToString("MMM");
            }

            public string Format(string xmlFormattedDate, string DateFormat)
            {
                DateTime date = XmlConvert.ToDateTime(xmlFormattedDate, XmlDateTimeSerializationMode.Local);

                return date.ToString(DateFormat);
            }
        }
    }
}
