/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.IO;
using System.Xml.XPath;
using Composite.Core.Xml;
using Composite.Functions;
using Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Foundation;


namespace Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Xslt.Extensions
{
    internal sealed class MarkupParserXsltExtensionsFunction : StandardFunctionBase
    {
        public MarkupParserXsltExtensionsFunction(EntityTokenFactory entityTokenFactory)
            : base("MarkupParser", "Composite.Xslt.Extensions", typeof(IXsltExtensionDefinition), entityTokenFactory)
        {
        }


        public override object Execute(ParameterList parameters, FunctionContextContainer context)
        {
            return new XsltExtensionDefinition<MarkupParserXsltExtensions>
            {
                EntensionObject = new MarkupParserXsltExtensions(),
                ExtensionNamespace = "#MarkupParserExtensions"
            };
        }


        internal class MarkupParserXsltExtensions
        {
            public XPathNavigator ParseWellformedDocumentMarkup(string wellformedMarkupString)
            {
                using (StringReader sr = new StringReader(wellformedMarkupString))
                {
                    XPathDocument doc = new XPathDocument(sr);
                    return doc.CreateNavigator();
                }
            }



            public XPathNavigator ParseXhtmlBodyFragment(string xhtmlBodyFragmentString)
            {
                using (StringReader sr = new StringReader(string.Format("<html xmlns='{0}'><head /><body>{1}</body></html>", Namespaces.Xhtml, xhtmlBodyFragmentString)))
                {
                    XPathDocument doc = new XPathDocument(sr);
                    return doc.CreateNavigator();
                }
            }

        }
    }
}
