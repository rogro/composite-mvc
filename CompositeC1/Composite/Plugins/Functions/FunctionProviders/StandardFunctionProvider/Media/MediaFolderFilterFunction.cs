/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Composite.Data;
using Composite.Data.Types;
using Composite.Core.Extensions;
using Composite.Functions;
using Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Foundation;

namespace Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Media
{
    internal sealed class MediaFolderFilterFunction<TMedia> : StandardFunctionBase where TMedia : IMediaFile
    {
        private static readonly MethodInfo MethodInfoGetFolderPath = typeof(IFile).GetProperty("FolderPath").GetGetMethod();
        private static readonly MethodInfo MethodInfoStringStartsWith = typeof(string).GetMethod("StartsWith", new[] { typeof(string) });


        public MediaFolderFilterFunction(EntityTokenFactory entityTokenFactory)
            : base("MediaFolderFilter", typeof(TMedia).FullName, typeof(Expression<Func<TMedia, bool>>), entityTokenFactory)
        {
        }


        protected override IEnumerable<StandardFunctionParameterProfile> StandardFunctionParameterProfiles
        {
            get
            {
                yield return new StandardFunctionParameterProfile(
                    "MediaFolder",
                    typeof(DataReference<IMediaFileFolder>),
                    false,
                    new ConstantValueProvider(null),
                    StandardWidgetFunctions.GetDataReferenceWidget<IMediaFileFolder>());

                WidgetFunctionProvider widget = StandardWidgetFunctions.GetBoolSelectorWidget("True", "False");

                yield return new StandardFunctionParameterProfile(
                    "IncludeSubfolders",
                    typeof(bool),
                    false,
                    new ConstantValueProvider(false), widget);
            }
        }


        public override object Execute(ParameterList parameters, FunctionContextContainer context)
        {
            var mediaFolderReference = parameters.GetParameter<DataReference<IMediaFileFolder>>("MediaFolder");
            Verify.ArgumentNotNull(mediaFolderReference, "mediaFolderReference");

            bool includeSubfolders = parameters.GetParameter<bool>("IncludeSubfolders");

            IMediaFileFolder mediaFolder = GetMediaFolder(mediaFolderReference);

            if (mediaFolder == null)
            {
                return (Expression<Func<TMedia, bool>>)(f => false);
            }

            string mediaFolderPath = mediaFolder.Path;
            string mediaFolderPathWithSlash = mediaFolder.Path + "/";

            if (includeSubfolders)
            {
                //return (Expression<Func<TMedia, bool>>)
                //       (image => image.FolderPath == mediaFolderPath
                //            || image.FolderPath.StartsWith(mediaFolderPathWithSlash));

                var imageParam = Expression.Parameter(typeof(TMedia), "image");

                var orElse = Expression.OrElse(
                    Expression.Equal(Expression.Property(imageParam, MethodInfoGetFolderPath),
                                     Expression.Constant(mediaFolderPath)),
                    Expression.Call(Expression.Property(imageParam, MethodInfoGetFolderPath),
                                    MethodInfoStringStartsWith,
                                    new Expression[] { Expression.Constant(mediaFolderPathWithSlash) }));

                return Expression.Lambda<Func<TMedia, bool>>(orElse, imageParam);
            }
            // return (Expression<Func<TMedia, bool>>)(imageParam1 => imageParam1.FolderPath == mediaFolderPath);

            var imageParam1 = Expression.Parameter(typeof(TMedia), "image");

            return Expression.Lambda<Func<TMedia, bool>>(
                 Expression.Equal(Expression.Property(imageParam1, MethodInfoGetFolderPath),
                                  Expression.Constant(mediaFolderPath)),
                imageParam1);
        }

        private static IMediaFileFolder GetMediaFolder(DataReference<IMediaFileFolder> mediaFolderReference)
        {
            string mediaFolderKeyPath = mediaFolderReference.KeyValue as string;

            using (new DataScope(DataScopeIdentifier.Public))
            {
                var query = DataFacade.GetData<IMediaFileFolder>();

                if (query.IsEnumerableQuery())
                {
                    return query.AsEnumerable().Where(mf => mf.KeyPath == mediaFolderKeyPath).FirstOrDefault();
                }

                return query.Where(mf => mf.KeyPath == mediaFolderKeyPath).FirstOrDefault();
            }
        }
    }
}