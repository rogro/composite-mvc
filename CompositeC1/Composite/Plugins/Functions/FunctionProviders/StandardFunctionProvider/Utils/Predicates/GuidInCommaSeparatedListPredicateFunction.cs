/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Composite.Core.Linq;
using Composite.Core.Extensions;
using Composite.Functions;
using Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Foundation;

namespace Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Utils.Predicates
{
    internal sealed class GuidInCommaSeparatedListPredicateFunction : StandardFunctionBase
    {
        public GuidInCommaSeparatedListPredicateFunction(EntityTokenFactory entityTokenFactory)
            : base("GuidInCommaSeparatedList", "Composite.Utils.Predicates", typeof(Expression<Func<Guid, bool>>), entityTokenFactory)
        {
        }


        protected override IEnumerable<StandardFunctionParameterProfile> StandardFunctionParameterProfiles
        {
            get
            {
                WidgetFunctionProvider textboxWidget = StandardWidgetFunctions.TextBoxWidget;

                yield return new StandardFunctionParameterProfile(
                    "CommaSeparatedGuids", typeof(string), true, new NoValueValueProvider(), textboxWidget);
            }
        }


        public override object Execute(ParameterList parameters, FunctionContextContainer context)
        {
            string commaSeparatedSearchTerms = parameters.GetParameter<string>("CommaSeparatedGuids");

            if (!commaSeparatedSearchTerms.IsNullOrEmpty())
            {
                ParameterExpression parameter = Expression.Parameter(typeof(Guid), "g");

                IEnumerable<string> guidStrings = commaSeparatedSearchTerms.Split(',');

                Expression body = null;

                foreach (string guidString in guidStrings)
                {
                    Guid temp = new Guid(guidString.Trim());

                    Expression part = Expression.Equal(parameter, Expression.Constant(temp));

                    body = body.NestedOr(part);
                }

                if(body != null)
                {
                    return Expression.Lambda<Func<Guid, bool>>(body, parameter);
                }
            }

            return (Expression<Func<Guid, bool>>) (f => false);
            
        }
    }
}
