/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Linq;
using System.Collections.Generic;
using System.Data.SqlTypes;

using Composite.Functions;

using Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Foundation;
using Composite.Core.Extensions;
using System.Linq.Expressions;

namespace Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Utils.Predicates
{
    internal sealed class StringInListPredicateFunction : StandardFunctionBase
    {
        public StringInListPredicateFunction(EntityTokenFactory entityTokenFactory)
            : base("StringInList", "Composite.Utils.Predicates", typeof(Expression<Func<string, bool>>), entityTokenFactory)
        {
        }


        protected override IEnumerable<StandardFunctionParameterProfile> StandardFunctionParameterProfiles
        {
            get
            {
                yield return new StandardFunctionParameterProfile(
                    "SearchTerms", typeof(IEnumerable<string>), true, new NoValueValueProvider(), null);

                yield return new StandardFunctionParameterProfile(
                    "IgnoreCase", typeof(bool), false, new ConstantValueProvider(true), StandardWidgetFunctions.GetBoolSelectorWidget("Ignore case", "Match case"));
            }
        }


        public override object Execute(ParameterList parameters, FunctionContextContainer context)
        {
            IEnumerable<string> searchTerms = parameters.GetParameter<IEnumerable<string>>("SearchTerms");
            bool ignoreCase = parameters.GetParameter<bool>("IgnoreCase");

            StringComparison stringComparison = (ignoreCase
                                                     ? StringComparison.CurrentCultureIgnoreCase
                                                     : StringComparison.CurrentCulture);

            Expression<Func<string, bool>> predicate = f => false;

            foreach (string searchTerm in searchTerms)
            {
                string temp = searchTerm.Trim();
                predicate = predicate.Or(c => c.Equals(temp, stringComparison));
            }

            return predicate;
        }
    }
}
