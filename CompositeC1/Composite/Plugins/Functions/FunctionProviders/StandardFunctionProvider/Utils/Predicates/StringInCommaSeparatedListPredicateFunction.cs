/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Reflection;
using Composite.Functions;

using Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Foundation;
using Composite.Core.Extensions;
using System.Linq.Expressions;

namespace Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Utils.Predicates
{
    internal sealed class StringInCommaSeparatedListPredicateFunction : StandardFunctionBase
    {
        private static readonly MethodInfo _stringCompareMethodInfo;

        static StringInCommaSeparatedListPredicateFunction()
        {
            _stringCompareMethodInfo = typeof(string).GetMethod("Compare", new[] { typeof(string), typeof(string), typeof(StringComparison) });
        }

        public StringInCommaSeparatedListPredicateFunction(EntityTokenFactory entityTokenFactory)
            : base("StringInCommaSeparatedList", "Composite.Utils.Predicates", typeof(Expression<Func<string, bool>>), entityTokenFactory)
        {
        }


        protected override IEnumerable<StandardFunctionParameterProfile> StandardFunctionParameterProfiles
        {
            get
            {
                WidgetFunctionProvider textboxWidget = StandardWidgetFunctions.TextBoxWidget;

                yield return new StandardFunctionParameterProfile(
                    "CommaSeparatedSearchTerms", typeof(string), true, new NoValueValueProvider(), textboxWidget);

                yield return new StandardFunctionParameterProfile(
                    "IgnoreCase", typeof(bool), false, new ConstantValueProvider(true), StandardWidgetFunctions.GetBoolSelectorWidget("Ignore case", "Match case"));
            }
        }


        public override object Execute(ParameterList parameters, FunctionContextContainer context)
        {
            Expression<Func<string, bool>> emptyPredicate = f => false;

            string commaSeparatedSearchTerms = parameters.GetParameter<string>("CommaSeparatedSearchTerms");

            if (commaSeparatedSearchTerms.IsNullOrEmpty())
            {
                return emptyPredicate;
            }


            bool ignoreCase = parameters.GetParameter<bool>("IgnoreCase");
            string[] searchTerms = commaSeparatedSearchTerms.Split(new [] {','}/*, StringSplitOptions.RemoveEmptyEntries*/);

            if (searchTerms.Length == 0)
            {
                return emptyPredicate;
            }

            StringComparison stringComparison = (ignoreCase
                                                     ? StringComparison.InvariantCultureIgnoreCase
                                                     : StringComparison.InvariantCulture);


            var parameterExpression = Expression.Parameter(typeof(string), "p");
            Expression body = null;

            foreach (string searchTerm in searchTerms)
            {
                string termTrimmed = searchTerm.Trim();

                // string.Compare(p, termTrimmed, stringComparison) == 0 
                // string.Compare() is supported by linq2sql
                Expression condition = Expression.Equal(Expression.Call(
                                                           _stringCompareMethodInfo,
                                                           parameterExpression,
                                                           Expression.Constant(termTrimmed),
                                                           Expression.Constant(stringComparison)), 
                                                       Expression.Constant(0));

                body = body == null ? condition : Expression.Or(body, condition);
            }
            Verify.IsNotNull(body, "Expression body is null");

            return Expression.Lambda<Func<string, bool>>(body, parameterExpression);
        }
    }
}
