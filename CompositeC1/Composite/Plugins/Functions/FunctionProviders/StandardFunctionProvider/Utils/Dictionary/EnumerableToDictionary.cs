/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Foundation;
using System.Collections;
using Composite.Functions;
using System.Reflection;


namespace Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Utils.Dictionary
{
    internal class EnumerableToDictionary : StandardFunctionBase
    {
        public EnumerableToDictionary(EntityTokenFactory entityTokenFactory)
            : base("EnumerableToDictionary", "Composite.Utils.Dictionary", typeof(IDictionary), entityTokenFactory)
        {
        }



        public override object Execute(ParameterList parameters, FunctionContextContainer context)
        {
            IEnumerable elements = parameters.GetParameter<IEnumerable>("Elements");
            string keyPropertyName = parameters.GetParameter<string>("KeyPropertyName");
            string valuePropertyName = parameters.GetParameter<string>("ValuePropertyName");

            Dictionary<string, string> resultDictionary = new Dictionary<string, string>();

            PropertyInfo keyPropertyInfo = null;
            PropertyInfo valuePropertyInfo = null;
            foreach (object element in elements)
            {
                if (keyPropertyInfo == null)
                {
                    keyPropertyInfo = element.GetType().GetProperty(keyPropertyName);
                }

                if (valuePropertyInfo == null)
                {
                    valuePropertyInfo = element.GetType().GetProperty(valuePropertyName);
                }

                string keyValue = keyPropertyInfo.GetValue(element, null).ToString();
                string valueValue = valuePropertyInfo.GetValue(element, null).ToString();

                resultDictionary.Add(keyValue, valueValue);
            }

            return resultDictionary;
        }


        protected override IEnumerable<StandardFunctionParameterProfile> StandardFunctionParameterProfiles
        {
            get
            {
                WidgetFunctionProvider textboxWidget = StandardWidgetFunctions.TextBoxWidget;

                yield return new StandardFunctionParameterProfile(
                    "Elements", typeof(IEnumerable), true, new NoValueValueProvider(), null);

                yield return new StandardFunctionParameterProfile(
                    "KeyPropertyName", typeof(string), true, new NoValueValueProvider(), textboxWidget);

                yield return new StandardFunctionParameterProfile(
                    "ValuePropertyName", typeof(string), true, new NoValueValueProvider(), textboxWidget);
            }
        }
    }
}
