/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;
using System.Text;
using Composite.Functions;
using Composite.C1Console.Security;
using Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Foundation;


namespace Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Utils.String
{
    internal sealed class Split : StandardFunctionBase
    {
        public Split(EntityTokenFactory entityTokenFactory)
            : base("Split", "Composite.Utils.String", typeof(IEnumerable<string>), entityTokenFactory)
        {
        }


        public override object Execute(ParameterList parameters, FunctionContextContainer context)
        {
            string stringToSplit = parameters.GetParameter<string>("String");
            string[] separator = new string[] { parameters.GetParameter<string>("Separator") };



            var resultArray = stringToSplit.Split(separator, StringSplitOptions.RemoveEmptyEntries);

            return new List<string>(resultArray);
        }


        protected override IEnumerable<StandardFunctionParameterProfile> StandardFunctionParameterProfiles
        {
            get
            {
                WidgetFunctionProvider textboxWidget = StandardWidgetFunctions.TextBoxWidget;

                yield return new StandardFunctionParameterProfile(
                    "String", typeof(string), true, new ConstantValueProvider(""), textboxWidget);

                yield return new StandardFunctionParameterProfile(
                    "Separator", typeof(string), false, new ConstantValueProvider(","), textboxWidget);
            }
        }
    }
}
