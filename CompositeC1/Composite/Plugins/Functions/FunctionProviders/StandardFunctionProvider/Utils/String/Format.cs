/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;
using System.Linq;
using Composite.Functions;
using Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Foundation;


namespace Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Utils.String
{
    internal sealed class Format : StandardFunctionBase, ICompoundFunction
    {
        public Format(EntityTokenFactory entityTokenFactory)
            : base("Format", "Composite.Utils.String", typeof(string), entityTokenFactory)
        {
        }


        public override object Execute(ParameterList parameters, FunctionContextContainer context)
        {
            string format = parameters.GetParameter<string>("Format");

            var formatParameters = new object[5];

            formatParameters[0] = parameters.GetParameter<string>("Parameter1");

            Verify.IsNotNull(formatParameters[0], "Parameter1 is null");

            formatParameters[1] = parameters.GetParameter<string>("Parameter2");
            formatParameters[2] = parameters.GetParameter<string>("Parameter3");
            formatParameters[3] = parameters.GetParameter<string>("Parameter4");
            formatParameters[4] = parameters.GetParameter<string>("Parameter5");

            return string.Format(format, formatParameters);
        }


        protected override IEnumerable<StandardFunctionParameterProfile> StandardFunctionParameterProfiles
        {
            get
            {
                WidgetFunctionProvider textboxWidget = StandardWidgetFunctions.TextBoxWidget;

                yield return new StandardFunctionParameterProfile(
                    "FormatString", typeof(IEnumerable<string>), true, new NoValueValueProvider(), null);

                yield return new StandardFunctionParameterProfile(
                    "Parameter1", typeof(string), true, new ConstantValueProvider(null), textboxWidget);

                yield return new StandardFunctionParameterProfile(
                    "Parameter2", typeof(string), false, new ConstantValueProvider(null), textboxWidget);

                yield return new StandardFunctionParameterProfile(
                    "Parameter3", typeof(string), false, new ConstantValueProvider(null), textboxWidget);

                yield return new StandardFunctionParameterProfile(
                    "Parameter4", typeof(string), false, new ConstantValueProvider(null), textboxWidget);

                yield return new StandardFunctionParameterProfile(
                    "Parameter5", typeof(string), false, new ConstantValueProvider(null), textboxWidget);
            }
        }
    
        public bool  AllowRecursiveCall
        {
	        get { return true; }
        }
    }
}
