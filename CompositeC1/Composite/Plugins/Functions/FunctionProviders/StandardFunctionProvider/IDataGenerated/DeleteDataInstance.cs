/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Composite.Core.Linq;
using Composite.Core.Types;
using Composite.Data;
using Composite.Data.DynamicTypes;
using Composite.Functions;
using Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Foundation;


namespace Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.IDataGenerated
{
    internal sealed class DeleteDataInstance<T> : StandardFunctionBase
        where T : class, IData
    {
        private List<StandardFunctionParameterProfile> _parameterProfiles = null;



        public DeleteDataInstance(EntityTokenFactory entityTokenFactory)
            : base("DeleteDataInstance", typeof(T).FullName, typeof(void), entityTokenFactory)
        {
            this.ResourceHandleNameStem = "Composite.IDataGenerated.DeleteDataInstance";
        }



        protected override IEnumerable<StandardFunctionParameterProfile> StandardFunctionParameterProfiles
        {
            get
            {
                if (_parameterProfiles == null)
                {
                    _parameterProfiles = new List<StandardFunctionParameterProfile>();                    

                    Expression<Func<T, bool>> defaultFilter = DataFacade.GetEmptyPredicate<T>();

                    _parameterProfiles.Add(new StandardFunctionParameterProfile(
                        "Filter",
                        typeof(Expression<Func<T, bool>>),
                        false,
                        new ConstantValueProvider(defaultFilter),
                        null
                    ));
                }

                return _parameterProfiles;
            }
        }



        public override object Execute(ParameterList parameters, FunctionContextContainer context)
        {
            Expression<Func<T, bool>> filter = parameters.GetParameter<Expression<Func<T, bool>>>("Filter");

            DataFacade.Delete<T>(filter);

            return null;
        }
    }
}
