/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.IDataGenerated;
using Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Foundation;
using Composite.Functions;
using Composite.Data;
using System.Reflection;
using System.Linq.Expressions;
using Composite.Core.WebClient.Renderings.Page;
using Composite.Data.Types;
using System.Xml.Linq;
using Composite.Core.Linq;

namespace Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.IDataGenerated
{
	internal sealed class GetDataReference<T> : StandardFunctionBase
        where T : class, IData
	{
        private static readonly ParameterExpression _dataItem = Expression.Parameter(typeof(T), "data");

        public GetDataReference(EntityTokenFactory entityTokenFactory)
            : base("GetDataReference", typeof(T).FullName, typeof(DataReference<T>), entityTokenFactory)
        {
            this.ResourceHandleNameStem = "Composite.IDataGenerated.GetDataReference";
        }



        protected override IEnumerable<StandardFunctionParameterProfile> StandardFunctionParameterProfiles
        {
            get
            {
                var keyPropertyInfo = DataAttributeFacade.GetKeyProperties(typeof(T)).Single();

                WidgetFunctionProvider referenceSelector = StandardWidgetFunctions.GetDataReferenceWidget<T>();

                yield return new StandardFunctionParameterProfile(
                    "KeyValue",
                    keyPropertyInfo.PropertyType,
                    true,
                    new NoValueValueProvider(),
                    referenceSelector);
            }
        }



        public override object Execute(ParameterList parameters, FunctionContextContainer context)
        {
            object keyValue = parameters.GetParameter("KeyValue");

            return new DataReference<T>(keyValue);
        }
    }
}
