/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Foundation;
using Composite.Functions;
using Composite.Data;
using System.Linq.Expressions;

namespace Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.IDataGenerated.Filter
{
	internal sealed class CompoundFilter<T> : StandardFunctionBase, ICompoundFunction
        where T : class, IData
	{
        private static readonly ParameterExpression _dataItem = Expression.Parameter(typeof(T), "data");

        public CompoundFilter(EntityTokenFactory entityTokenFactory)
            : base("CompoundFilter", typeof(T).FullName, typeof(Expression<Func<T, bool>>), entityTokenFactory)
        {
            this.ResourceHandleNameStem = "Composite.IDataGenerated.Filter.CompoundFilter";
        }



        protected override IEnumerable<StandardFunctionParameterProfile> StandardFunctionParameterProfiles
        {
            get
            {
                WidgetFunctionProvider isAndQuerySelector = StandardWidgetFunctions.GetBoolSelectorWidget("And query", "Or Query");

                yield return new StandardFunctionParameterProfile(
                    "IsAndQuery",
                    typeof(bool),
                    false,
                    new ConstantValueProvider(true),
                    isAndQuerySelector);

                yield return new StandardFunctionParameterProfile(
                    "Left",
                    typeof(Expression<Func<T, bool>>),
                    true,
                    new NoValueValueProvider(),
                    null);

                yield return new StandardFunctionParameterProfile(
                    "Right",
                    typeof(Expression<Func<T, bool>>),
                    true,
                    new NoValueValueProvider(),
                    null);
            }
        }



        public override object Execute(ParameterList parameters, FunctionContextContainer context)
        {
            bool isAndQuery = parameters.GetParameter<bool>("IsAndQuery");
            Expression<Func<T, bool>> left = parameters.GetParameter<Expression<Func<T, bool>>>("Left");
            Expression<Func<T, bool>> right = parameters.GetParameter<Expression<Func<T, bool>>>("Right");

            Expression compound;

            if (isAndQuery)
            {
                Expression leftFilter = Expression.Invoke(left, _dataItem);
                Expression rightFilter = Expression.Invoke(right, _dataItem);
                compound = Expression.And(leftFilter, rightFilter);
            }
            else
            {
                Expression leftFilter = Expression.Invoke(left, _dataItem);
                Expression rightFilter = Expression.Invoke(right, _dataItem);
                compound = Expression.Or(leftFilter, rightFilter);
            }

            Expression<Func<T, bool>> lambdaExpression = Expression.Lambda<Func<T, bool>>(compound, new ParameterExpression[] { _dataItem });

            return lambdaExpression;
        }

        public bool AllowRecursiveCall
        {
            get { return true; }
        }
    }
}
