/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Collections;
using Composite.Core.Types;
using Composite.Data;

namespace Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.IDataGenerated.Filter.Foundation
{
    internal static class ListPropertyNamesHelper
    {
        public static IEnumerable GetOptionsWithReferences(string typeManagerName)
        {
            Type t = TypeManager.GetType(typeManagerName);
            IEnumerable<ForeignPropertyInfo> foreignKeyProperties = DataReferenceFacade.GetForeignKeyProperties(t);
            List<PropertyInfo> properties = t.GetPropertiesRecursively(f=>f.DeclaringType!=typeof(IData));

            List<string> result = new List<string>();

            foreach (PropertyInfo propertyInfo in properties)
            {
                result.Add( propertyInfo.Name);
                ForeignPropertyInfo foreignKeyInfo = foreignKeyProperties.FirstOrDefault(f=>f.SourcePropertyName==propertyInfo.Name);

                if (foreignKeyInfo!=null)
                {
                    List<PropertyInfo> foreignProperties = foreignKeyInfo.TargetType.GetPropertiesRecursively(f=>f.DeclaringType!=typeof(IData));

                    foreach (PropertyInfo foreignPropertyInfo in foreignProperties)
                    {
                        string foreignKey = string.Format("{0}.{1}", propertyInfo.Name, foreignPropertyInfo.Name );
                        result.Add( foreignKey);
                    }

                }
            }

            return result;
        }


        public static IEnumerable GetOptions(string typeManagerName)
        {
            Type t = TypeManager.GetType(typeManagerName);

            List<PropertyInfo> properties = t.GetPropertiesRecursively();

            List<string> result = new List<string>();

            result.AddRange(
                from property in properties
                where property.DeclaringType != typeof(IData)
                select property.Name);

            return result;
        }

    }
}
