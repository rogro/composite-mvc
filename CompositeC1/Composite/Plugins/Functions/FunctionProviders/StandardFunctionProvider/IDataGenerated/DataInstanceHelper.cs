/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Composite.Functions;
using Composite.Data.DynamicTypes;
using System.Xml.Linq;


namespace Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.IDataGenerated
{
    internal static class DataInstanceHelper
    {
        public static BaseValueProvider GetFallbackValueProvider(DataFieldDescriptor dataFieldDescriptor, bool isKeyProperty)
        {
            if (dataFieldDescriptor.DefaultValue != null)
            {
                return new ConstantValueProvider(dataFieldDescriptor.DefaultValue.Value);
            }

            Type instanceType = GetInstanceType(dataFieldDescriptor);
            if (instanceType == typeof(int))
            {
                return new ConstantValueProvider(0);
            }
            else if (instanceType == typeof(decimal))
            {
                return new ConstantValueProvider(0.0);
            }
            else if (instanceType == typeof(bool))
            {
                return new ConstantValueProvider(false);
            }
            else if (instanceType == typeof(string))
            {
                return new ConstantValueProvider("");
            }
            else if (instanceType == typeof(Guid))
            {
                if (isKeyProperty)
                {
                    return new ConstantValueProvider(Guid.NewGuid());
                }
                else
                {
                    return new ConstantValueProvider(Guid.Empty);
                }
            }
            else if (instanceType == typeof(DateTime))
            {
                return new ConstantValueProvider(DateTime.Now);
            }
            else
            {
                return new ConstantValueProvider("");
            }
        }



        public static WidgetFunctionProvider GetWidgetFunctionProvider(DataFieldDescriptor dataFieldDescriptor)
        {
            if ((dataFieldDescriptor.FormRenderingProfile != null) && (string.IsNullOrEmpty(dataFieldDescriptor.FormRenderingProfile.WidgetFunctionMarkup) == false))
            {
                WidgetFunctionProvider widgetFunctionProvider = new WidgetFunctionProvider(XElement.Parse(dataFieldDescriptor.FormRenderingProfile.WidgetFunctionMarkup));
                return widgetFunctionProvider;
            }
            else
            {
                return StandardWidgetFunctions.GetDefaultWidgetFunctionProviderByType(dataFieldDescriptor.InstanceType);
            }
        }



        public static Type GetInstanceType(DataFieldDescriptor dataFieldDescriptor)
        {
            Type instanceType = dataFieldDescriptor.InstanceType;
            if (instanceType.IsGenericType && instanceType.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                instanceType = instanceType.GetGenericArguments().First();
            }

            return instanceType;
        }
    }
}
