/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using Composite.Core.Types;
using Composite.Data;
using Composite.Data.DynamicTypes;
using Composite.Functions;
using Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Foundation;


namespace Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.IDataGenerated
{
    internal sealed class AddDataInstance<T> : StandardFunctionBase
        where T : class, IData
    {
        private List<StandardFunctionParameterProfile> _parameterProfiles = null;


        public AddDataInstance(EntityTokenFactory entityTokenFactory)
            : base("AddDataInstance", typeof(T).FullName, typeof(void), entityTokenFactory)
        {
            this.ResourceHandleNameStem = "Composite.IDataGenerated.AddDataInstance";
        }



        protected override IEnumerable<StandardFunctionParameterProfile> StandardFunctionParameterProfiles
        {
            get
            {
                if (_parameterProfiles == null)
                {
                    _parameterProfiles = new List<StandardFunctionParameterProfile>();
                    DataTypeDescriptor dataTypeDescriptor = DynamicTypeManager.GetDataTypeDescriptor(typeof(T));

                    foreach (DataFieldDescriptor dataFieldDescriptor in dataTypeDescriptor.Fields)
                    {
                        string helpText = dataFieldDescriptor.Name;
                        if (dataFieldDescriptor.FormRenderingProfile != null && !string.IsNullOrWhiteSpace(dataFieldDescriptor.FormRenderingProfile.HelpText))
                        {
                            helpText = dataFieldDescriptor.FormRenderingProfile.HelpText;
                        }

                        bool isGuidKeyField = dataTypeDescriptor.KeyPropertyNames.Contains(dataFieldDescriptor.Name) && (dataFieldDescriptor.InstanceType == typeof(Guid));

                        bool isRequried = dataFieldDescriptor.DefaultValue == null;
                        if (isGuidKeyField)
                        {
                            isRequried = false;
                        }

                        _parameterProfiles.Add(new StandardFunctionParameterProfile(
                            dataFieldDescriptor.Name,
                            dataFieldDescriptor.InstanceType,
                            isRequried,
                            DataInstanceHelper.GetFallbackValueProvider(dataFieldDescriptor, isGuidKeyField),
                            DataInstanceHelper.GetWidgetFunctionProvider(dataFieldDescriptor)
                        )
                        {
                            CustomLabel = dataFieldDescriptor.Name,
                            CustomHelpText = helpText
                        });
                    }                    
                }

                return _parameterProfiles;
            }
        }



        public override object Execute(ParameterList parameters, FunctionContextContainer context)
        {
            T data = DataFacade.BuildNew<T>();

            List<PropertyInfo> typePropertites = typeof(T).GetPropertiesRecursively();

            foreach (string parameterName in parameters.AllParameterNames)
            {
                PropertyInfo propertyInfo = typePropertites.Where(f => f.Name == parameterName).Single();

                propertyInfo.SetValue(data, parameters.GetParameter(parameterName, propertyInfo.PropertyType), null);
            }
            
            AssignMissingGuidKeyValues(parameters, data);

            DataFacade.AddNew<T>(data);

            return null;
        }



        private static void AssignMissingGuidKeyValues(ParameterList parameters, T data)
        {
            foreach (PropertyInfo keyPropertyInfo in typeof(T).GetKeyProperties())
            {
                if (keyPropertyInfo.PropertyType != typeof(Guid)) continue;

                if (parameters.AllParameterNames.Contains(keyPropertyInfo.Name)) continue;

                keyPropertyInfo.SetValue(data, Guid.NewGuid(), null);
            }
        }        
    }
}
