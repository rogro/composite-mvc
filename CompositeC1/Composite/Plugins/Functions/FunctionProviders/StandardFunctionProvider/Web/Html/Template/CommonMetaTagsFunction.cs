/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;
using System.Threading;
using System.Xml.Linq;
using Composite.Core.Xml;
using Composite.Functions;
using Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Foundation;

namespace Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Web.Html.Template
{
    internal sealed class CommonMetaTagsFunction : StandardFunctionBase
    {
        public CommonMetaTagsFunction(EntityTokenFactory entityTokenFactory)
            : base("CommonMetaTags", "Composite.Web.Html.Template", typeof(IEnumerable<XElement>), entityTokenFactory)
        {
        }



        protected override IEnumerable<StandardFunctionParameterProfile> StandardFunctionParameterProfiles
        {
            get
            {
                WidgetFunctionProvider showGeneratorWidget = StandardWidgetFunctions.GetBoolSelectorWidget("Yes, show Composite C1 support!", "No, please hide this...");

                yield return new StandardFunctionParameterProfile(
                    "ContentType",
                    typeof(string),
                    false,
                    new ConstantValueProvider("text/html; charset=utf-8"),
                    StandardWidgetFunctions.TextBoxWidget);

                yield return new StandardFunctionParameterProfile(
                    "Designer",
                    typeof(string),
                    false,
                    new ConstantValueProvider(""),
                    StandardWidgetFunctions.TextBoxWidget);

                yield return new StandardFunctionParameterProfile(
                    "ShowGenerator",
                    typeof(bool),
                    false,
                    new ConstantValueProvider(true),
                    showGeneratorWidget);
            }
        }



        public override object Execute(ParameterList parameters, FunctionContextContainer context)
        {
            var metaTags = new List<XElement>();

            string contentType = parameters.GetParameter<string>("ContentType");
            string designer = parameters.GetParameter<string>("Designer");
            bool showGenerator = parameters.GetParameter<bool>("ShowGenerator");

            if (!string.IsNullOrWhiteSpace(contentType))
            {
                metaTags.Add(new XElement(Namespaces.Xhtml + "meta",
                    new XAttribute("http-equiv", "Content-Type"),
                    new XAttribute("content", contentType)));
            }

            if (!string.IsNullOrWhiteSpace(designer))
            {
                metaTags.Add(new XElement(Namespaces.Xhtml + "meta",
                    new XAttribute("name", "Designer"),
                    new XAttribute("content", designer)));
            }

            if (showGenerator)
            {
                metaTags.Add(new XElement(Namespaces.Xhtml + "meta",
                    new XAttribute("name", "Generator"),
                    new XAttribute("content", "Composite C1 CMS - Free Open Source from http://composite.net/")));
            }

            return metaTags;
        }
    }
}
