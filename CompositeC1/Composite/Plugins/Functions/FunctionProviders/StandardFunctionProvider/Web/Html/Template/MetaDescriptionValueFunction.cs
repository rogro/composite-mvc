/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Web;
using System.Web.UI;

using Composite.Data;
using Composite.Functions;
using Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Foundation;
using System.Xml.Linq;
using System.Collections.Generic;
using Composite.Core.Xml;


namespace Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Web.Html.Template
{
    internal sealed class MetaDescriptionValueFunction : StandardFunctionBase
    {
        public MetaDescriptionValueFunction(EntityTokenFactory entityTokenFactory)
            : base("MetaDescriptionValue", "Composite.Web.Html.Template", typeof(Control), entityTokenFactory)
        {
        }


        protected override IEnumerable<StandardFunctionParameterProfile> StandardFunctionParameterProfiles
        {
            get
            {
                WidgetFunctionProvider textboxWidget = StandardWidgetFunctions.TextAreaWidget;

                yield return new StandardFunctionParameterProfile("Element", typeof(XElement), false, new ConstantValueProvider(null), textboxWidget);
            }
        }


        public override object Execute(ParameterList parameters, FunctionContextContainer context)
        {
            return new DescriptionControl(parameters.GetParameter<XElement>("Element"));
        }


        private class DescriptionControl : Control
        {
            private XElement _element = null;

            public DescriptionControl(XElement element)
            {
                _element = element;
            }

            protected override void Render(HtmlTextWriter writer)
            {
                string description = Page.Header.Description;

                if (string.IsNullOrWhiteSpace(description) && SiteMap.CurrentNode != null)
                {
                    description = SiteMap.CurrentNode.Description;
                }

                if (string.IsNullOrWhiteSpace(description))
                {
                    using (DataConnection connection = new DataConnection())
                    {
                        description = connection.SitemapNavigator.CurrentPageNode.Description;
                    }
                }

                if (string.IsNullOrWhiteSpace(description)) return;

                if (_element != null)
                {
                    _element.Add(description);
                    string commonNs = string.Format(" xmlns=\"{0}\"", Namespaces.Xhtml );
                    string raw = _element.ToString().Replace(commonNs, "");
                    writer.Write(raw);
                }
                else
                {
                    writer.WriteEncodedText(description);
                }
            }
        }
    }
}
