/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;
using System.Web;
using System.Web.UI;

using Composite.Data;
using Composite.Functions;
using Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Foundation;


namespace Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Web.Html.Template
{
    internal sealed class HtmlTitleValueFunction : StandardFunctionBase
    {
        public HtmlTitleValueFunction(EntityTokenFactory entityTokenFactory)
            : base("HtmlTitleValue", "Composite.Web.Html.Template", typeof(Control), entityTokenFactory)
        {
        }


        protected override IEnumerable<StandardFunctionParameterProfile> StandardFunctionParameterProfiles
        {
            get
            {
                WidgetFunctionProvider textboxWidget = StandardWidgetFunctions.TextAreaWidget;

                yield return new StandardFunctionParameterProfile("PrefixToRemove", typeof(string), false, new ConstantValueProvider(""), textboxWidget);
                yield return new StandardFunctionParameterProfile("PostfixToRemove", typeof(string), false, new ConstantValueProvider(""), textboxWidget);
            }
        }

        
        public override object Execute(ParameterList parameters, FunctionContextContainer context)
        {
            return new TitleControl { 
                PrefixToRemove = parameters.GetParameter<string>("PrefixToRemove"), 
                PostfixToRemove = parameters.GetParameter<string>("PostfixToRemove") 
            };
        }

        private class TitleControl : Control
        {
            public string PrefixToRemove { get; set; }
            public string PostfixToRemove { get; set; }

            protected override void Render(HtmlTextWriter writer)
            {
                string pageTitle = this.Page.Title;

                if (string.IsNullOrWhiteSpace(pageTitle) && SiteMap.CurrentNode != null)
                {
                    pageTitle = SiteMap.CurrentNode.Title;
                }
            
                if (string.IsNullOrWhiteSpace(pageTitle))
                {
                    using (DataConnection connection = new DataConnection())
                    {
                        pageTitle = connection.SitemapNavigator.CurrentPageNode.Title;
                    }
                }

                pageTitle = pageTitle.Trim();

                if (!string.IsNullOrEmpty(this.PrefixToRemove) && pageTitle.StartsWith(this.PrefixToRemove))
                {
                    pageTitle = pageTitle.Remove(0, this.PrefixToRemove.Length);
                }

                if (!string.IsNullOrEmpty(this.PostfixToRemove) && pageTitle.EndsWith(this.PostfixToRemove))
                {
                    pageTitle = pageTitle.Remove(pageTitle.Length - PostfixToRemove.Length);
                }

                writer.Write(pageTitle);
            }
        }
    }
}
