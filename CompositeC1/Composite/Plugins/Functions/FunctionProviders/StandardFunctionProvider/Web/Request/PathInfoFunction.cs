/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using Composite.Core.Extensions;
using Composite.Core.Routing.Pages;
using Composite.Functions;
using Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Foundation;

namespace Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Web.Request
{
    internal sealed class PathInfoFunction : StandardFunctionBase
    {
        public PathInfoFunction(EntityTokenFactory entityTokenFactory)
            : base("PathInfo", "Composite.Web.Request", typeof(string), entityTokenFactory)
        {
        }

        protected override IEnumerable<StandardFunctionParameterProfile> StandardFunctionParameterProfiles
        {
            get
            {
                WidgetFunctionProvider segmentDropDown = StandardWidgetFunctions.DropDownList(
                    typeof(PathInfoFunction), "SegmentSelectorOptionsFull", "Key", "Value", false, true);

                yield return new StandardFunctionParameterProfile(
                    "Segment", typeof(int), true, new ConstantValueProvider("-1"), segmentDropDown);

                yield return new StandardFunctionParameterProfile(
                    "AutoApprove", typeof(bool), false, new ConstantValueProvider(true), StandardWidgetFunctions.CheckBoxWidget);

                yield return new StandardFunctionParameterProfile(
                    "FallbackValue", typeof(string), false, new ConstantValueProvider(""), StandardWidgetFunctions.TextBoxWidget);
            }
        }


        public override object Execute(ParameterList parameters, FunctionContextContainer context)
        {
            int segment = (int)parameters.GetParameter("Segment");
            bool autoApprove = (bool)parameters.GetParameter("AutoApprove");

            string result = GetPathInfoSegment(segment);

            if (autoApprove && !result.IsNullOrEmpty())
            {
                C1PageRoute.RegisterPathInfoUsage();
            }

            return result ?? parameters.GetParameter<string>("FallbackValue");
        }

        public static IEnumerable<KeyValuePair<int, string>> SegmentSelectorOptions()
        {
            return new[]
                       {
                           new KeyValuePair<int, string>(0, "0 "), // Additional space is intentional 
                           new KeyValuePair<int, string>(1, "1"),
                           new KeyValuePair<int, string>(2, "2"),
                           new KeyValuePair<int, string>(3, "3"),
                           new KeyValuePair<int, string>(4, "4"),
                           new KeyValuePair<int, string>(5, "5")
                       };
        }

        public static IEnumerable<KeyValuePair<int, string>> SegmentSelectorOptionsFull()
        {
            yield return new KeyValuePair<int, string>(-1, "-1");
            foreach (var option in SegmentSelectorOptions())
            {
                yield return option;
            }
        }

        internal static string GetPathInfoSegment(int segment)
        {
            string pathInfo = C1PageRoute.GetPathInfo();
            if (segment == -1)
            {
                return pathInfo;
            }

            string[] segments = (pathInfo ?? string.Empty).Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

            if (segments.Length > segment)
            {
                return segments[segment];
            }

            return null;
        }
    }
}