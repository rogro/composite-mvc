/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using Composite.AspNet;
using Composite.Core.IO;
using Composite.Plugins.Functions.FunctionProviders.FileBasedFunctionProvider;

namespace Composite.Plugins.Functions.FunctionProviders.UserControlFunctionProvider
{
    internal class UserControlBasedFunction : FileBasedFunction<UserControlBasedFunction>
    {
        public UserControlBasedFunction(
            string @namespace, 
            string name, 
            string description,
            IDictionary<string, FunctionParameter> parameters, 
            Type returnType,
            string virtualPath, 
            FileBasedFunctionProvider<UserControlBasedFunction> provider)
            : base(@namespace, name, description, parameters, returnType, virtualPath, provider)
        {
        }

        public UserControlBasedFunction(
            string @namespace, 
            string name, 
            string description, 
            string virtualPath, 
            FileBasedFunctionProvider<UserControlBasedFunction> provider)
            : base(@namespace, name, description, typeof(UserControl), virtualPath, provider)
        {
        }

        protected override void InitializeParameters()
        {
            UserControl userControl = UserControlFunctionProvider.CompileFile(VirtualPath);

            Verify.IsNotNull(userControl, "Failed to get UserControl from '{0}'", VirtualPath);

            Type baseControlType = userControl is UserControlFunction ? typeof(UserControlFunction) : typeof(UserControl);

            Parameters = FunctionBasedFunctionProviderHelper.GetParameters(userControl, baseControlType, PathUtil.Resolve(VirtualPath));
        }

        public override object Execute(Composite.Functions.ParameterList parameters,
                                       Composite.Functions.FunctionContextContainer context)
        {
            var httpContext = HttpContext.Current;
            Verify.IsNotNull(httpContext, "HttpContext.Current is null");

            Page currentPage = httpContext.Handler as Page;
            Verify.IsNotNull(currentPage, "The Current HttpContext Handler must be a " + typeof (Page).FullName);

            var userControl = currentPage.LoadControl(VirtualPath);


            foreach (var param in parameters.AllParameterNames)
            {
                var value = parameters.GetParameter(param);
                Parameters[param].SetValue(userControl, value);
            }

            return userControl;
        }
    }
}
