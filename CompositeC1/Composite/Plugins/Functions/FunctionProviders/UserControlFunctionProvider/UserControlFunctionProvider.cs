/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Web.UI;
using Composite.AspNet;
using Composite.Core.WebClient;
using Composite.Functions;
using Composite.Plugins.Functions.FunctionProviders.FileBasedFunctionProvider;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace Composite.Plugins.Functions.FunctionProviders.UserControlFunctionProvider
{
    [ConfigurationElementType(typeof(UserControlFunctionProviderData))]
    internal class UserControlFunctionProvider : FileBasedFunctionProvider.FileBasedFunctionProvider<UserControlBasedFunction>
    {
        public UserControlFunctionProvider(string name, string folder) : base(name, folder) { }

        protected override string FileExtension
        {
            get { return "ascx"; }
        }

        protected override string DefaultFunctionNamespace
        {
            get { return "UserControls"; }
        }

        protected override bool HandleChange(string path)
        {
            return path.EndsWith(".ascx") || path.EndsWith(".ascx.cs");
        }

        public static UserControl CompileFile(string virtualPath)
        { 
            var page = new Page();

            using(BuildManagerHelper.DisableUrlMetadataCachingScope())
            {
                return page.LoadControl(virtualPath) as UserControl;
            }
        }

        protected override IFunction InstantiateFunction(string virtualPath, string @namespace, string name)
        {
            UserControl userControl = CompileFile(virtualPath);

            if(!(userControl is UserControl))
            {
                return null;
            }

            Type baseControlType = userControl is UserControlFunction ? typeof(UserControlFunction) : typeof(UserControl);

            string description = userControl is UserControlFunction 
                ? (userControl as UserControlFunction).FunctionDescription 
                : "";

            var parameters = FunctionBasedFunctionProviderHelper.GetParameters(userControl, baseControlType, virtualPath);

            return new UserControlBasedFunction(@namespace, name, description, parameters, typeof(UserControl), virtualPath, this);
        }

        protected override IFunction InstantiateFunctionFromCache(string virtualPath, string @namespace, string name, Type returnType, string cachedDescription)
        {
            return new UserControlBasedFunction(@namespace, name, cachedDescription, virtualPath, this);
        }

    }
}
