/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System.Configuration;
using Composite.Core.Configuration;
using Composite.Functions.Plugins.XslExtensionsProvider;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ObjectBuilder;

namespace Composite.Plugins.Functions.XslExtensionsProviders.ConfigBasedXslExtensionsProvider
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [Assembler(typeof(ConfigBasedXslExtensionsProviderAssembler))]
	internal sealed class ConfigBasedXslExtensionsProviderData: XslExtensionsProviderData
	{

        public const string SectionName = "Composite.XslExtensions";

        private const string _xslExtensionsProperty = "xslExtensions";
        [ConfigurationProperty(_xslExtensionsProperty)]
        public NameTypeManagerTypeConfigurationElementCollection<ConfigBasedXslExtensionInfo> XslExtensions
        {
            get
            {
                return (NameTypeManagerTypeConfigurationElementCollection<ConfigBasedXslExtensionInfo>)base[_xslExtensionsProperty];
            }
        }
	}



    internal sealed class ConfigBasedXslExtensionsProviderAssembler : IAssembler<IXslExtensionsProvider, XslExtensionsProviderData>
    {
        public IXslExtensionsProvider Assemble(Microsoft.Practices.ObjectBuilder.IBuilderContext context, XslExtensionsProviderData objectConfiguration, IConfigurationSource configurationSource, ConfigurationReflectionCache reflectionCache)
        {
            ConfigBasedXslExtensionsProviderData myConfiguration = (ConfigBasedXslExtensionsProviderData)objectConfiguration;
            return new ConfigBasedXslExtensionsProvider(myConfiguration);
        }
    }



    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public class ConfigBasedXslExtensionInfo : NameTypeManagerTypeConfigurationElement
    {
    }
}

