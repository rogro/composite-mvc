/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;
using System.Reflection;
using Composite.Core.Xml;
using Composite.Core.Types;

namespace Composite.Plugins.Functions.WidgetFunctionProviders.StandardWidgetFunctionProvider.Foundation
{
    internal static class FormFunctionMarkupBuilder
    {
        public static XElement StaticMethodCall(MethodInfo methodInfo, object parameter)
        {
            XNamespace f = Namespaces.BindingFormsStdFuncLib10;

            try
            {
                string parameterSerialized = ValueTypeConverter.Convert<string>(parameter);

                return new XElement(f + "StaticMethodCall",
                           new XAttribute("Type", TypeManager.SerializeType(methodInfo.DeclaringType)),
                           new XAttribute("Method", methodInfo.Name),
                           new XAttribute("Parameters", parameterSerialized ?? ""));
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(string.Format("Failed to generate markup for static method call to '{0}' ('{1}') with parameter of type {2}", methodInfo.Name, methodInfo.DeclaringType.AssemblyQualifiedName, parameter.GetType()), ex);
            }
        }

        internal static object StaticMethodCall(MethodInfo methodInfo)
        {
            XNamespace f = Namespaces.BindingFormsStdFuncLib10;

            return new XElement(f + "StaticMethodCall",
                       new XAttribute("Type", TypeManager.SerializeType(methodInfo.DeclaringType)),
                       new XAttribute("Method", methodInfo.Name));
        }
    }
}
