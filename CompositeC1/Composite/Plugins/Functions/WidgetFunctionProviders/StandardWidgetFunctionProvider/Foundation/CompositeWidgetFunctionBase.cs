/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Composite.C1Console.Security;
using Composite.Core.Xml;
using Composite.Functions;


namespace Composite.Plugins.Functions.WidgetFunctionProviders.StandardWidgetFunctionProvider.Foundation
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public abstract class CompositeWidgetFunctionBase : IWidgetFunction
	{
        /// <exclude />
        protected const string CommonNamespace = "Composite.Widgets";

        /// <exclude />
        protected const string InternalCommonNamespace = "Composite.Widgets.Internal"; // not exposed by provider
        
        private EntityTokenFactory _entityTokenFactory;


        /// <exclude />
        protected CompositeWidgetFunctionBase(string compositeName, Type returnType, EntityTokenFactory entityTokenFactory)
        {
            if (string.IsNullOrEmpty(compositeName)) throw new ArgumentNullException("compositeName");

            this.Namespace = compositeName.Substring(0,compositeName.LastIndexOf('.'));
            this.Name = compositeName.Substring(compositeName.LastIndexOf('.')+1);
            this.ReturnType = returnType;
            this.ParameterProfiles = new List<ParameterProfile>();
            _entityTokenFactory = entityTokenFactory;
        }


        /// <exclude />
        protected void AddParameterProfile(ParameterProfile pp)
        {
            ((List<ParameterProfile>)this.ParameterProfiles).Add(pp);
        }


        /// <exclude />
        public virtual string Name { get; private set; }


        /// <exclude />
        public string Namespace{ get; private set; }


        /// <exclude />
        public virtual string Description { get { return ""; } }


        /// <exclude />
        public Type ReturnType { get; private set; }


        /// <exclude />
        public IEnumerable<ParameterProfile> ParameterProfiles { get; private set; }


        /// <exclude />
        public abstract XElement GetWidgetMarkup(ParameterList parameters, string label, HelpDefinition help, string bindingSourceName);


        /// <exclude />
        protected XElement BuildBasicWidgetMarkup(string uiControlName, string bindingPropertyName, string label, HelpDefinition help, string bindingSourceName)
        {
            return StandardWidgetFunctions.BuildBasicFormsMarkup(Namespaces.BindingFormsStdUiControls10, uiControlName, bindingPropertyName, label, help, bindingSourceName);
        }


        /// <exclude />
        public EntityToken EntityToken
        {
            get { return _entityTokenFactory.CreateEntityToken(this); }
        }
    }
}
