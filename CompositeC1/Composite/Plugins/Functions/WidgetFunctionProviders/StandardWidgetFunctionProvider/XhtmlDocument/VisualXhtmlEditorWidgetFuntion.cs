/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Xml.Linq;
using System.Collections.Generic;
using Composite.Core.Extensions;
using Composite.Functions;
using Composite.Plugins.Functions.WidgetFunctionProviders.StandardWidgetFunctionProvider.Foundation;
using Composite.Core.Types;
using Composite.Core.Xml;

using StringSelector = Composite.Plugins.Functions.WidgetFunctionProviders.StandardWidgetFunctionProvider.String.VisualXhtmlEditorFuntion;

namespace Composite.Plugins.Functions.WidgetFunctionProviders.StandardWidgetFunctionProvider.XhtmlDocument
{
	internal sealed class VisualXhtmlEditorFuntion : CompositeWidgetFunctionBase
    {
        public static IEnumerable<Type> GetOptions(object typeManagerTypeName)
        {
            yield return TypeManager.GetType((string)typeManagerTypeName);
        }

        private const string _functionName = "VisualXhtmlEditor";
        public const string CompositeName = CompositeWidgetFunctionBase.CommonNamespace + ".XhtmlDocument." + _functionName;

        public const string ClassConfigurationNameParameterName = "ClassConfigurationName";
        public const string EmbedableFieldTypeParameterName = "EmbedableFieldsType";


        public VisualXhtmlEditorFuntion(EntityTokenFactory entityTokenFactory)
            : base(CompositeName, typeof(Composite.Core.Xml.XhtmlDocument), entityTokenFactory)
        {
            SetParameterProfiles("common");
        }




        public override XElement GetWidgetMarkup(ParameterList parameters, string label, HelpDefinition help, string bindingSourceName)
        {
            XElement element = base.BuildBasicWidgetMarkup("InlineXhtmlEditor", "Xhtml", label, help, bindingSourceName);
            element.Add(new XAttribute("ClassConfigurationName", parameters.GetParameter<string>(VisualXhtmlEditorFuntion.ClassConfigurationNameParameterName)));

            var pageId = parameters.GetParameter<Guid>(StringSelector.PreviewPageIdParameterName);
            var templateId = parameters.GetParameter<Guid>(StringSelector.PreviewTemplateIdParameterName);
            string placeholderName = parameters.GetParameter<string>(StringSelector.PreviewPlaceholderParameterName);

            if (pageId != Guid.Empty)
            {
                element.Add(new XAttribute("PreviewPageId", pageId));
            }

            if (templateId != Guid.Empty)
            {
                element.Add(new XAttribute("PreviewTemplateId", templateId));
            }

            if (!string.IsNullOrEmpty(placeholderName))
            {
                element.Add(new XAttribute("PreviewPlaceholder", placeholderName));
            }

            Type embedableFieldType = parameters.GetParameter<Type>(VisualXhtmlEditorFuntion.EmbedableFieldTypeParameterName);
            if (embedableFieldType!=null)
            {
                XNamespace f = Namespaces.BindingFormsStdFuncLib10;

                element.Add(
                    new XElement(element.Name.Namespace + "InlineXhtmlEditor.EmbedableFieldsTypes",
                        new XElement(f + "StaticMethodCall",
                           new XAttribute("Type", TypeManager.SerializeType(this.GetType())),
                           new XAttribute("Parameters", TypeManager.SerializeType(embedableFieldType)),
                           new XAttribute("Method", "GetOptions"))));

            }

            return element;
        }


        private void SetParameterProfiles(string classConfigurationName)
        {
            ParameterProfile classConfigNamePP =
                new ParameterProfile(VisualXhtmlEditorFuntion.ClassConfigurationNameParameterName,
                    typeof(string), false,
                    new ConstantValueProvider(classConfigurationName), StandardWidgetFunctions.TextBoxWidget, null,
                    "Class configuration name", new HelpDefinition("The visual editor can be configured to offer the editor a special set of class names for formatting xhtml elements. The default value is '" + classConfigurationName + "'"));

            base.AddParameterProfile(classConfigNamePP);

            ParameterProfile typeNamePP =
                new ParameterProfile(VisualXhtmlEditorFuntion.EmbedableFieldTypeParameterName,
                    typeof(Type), false,
                    new ConstantValueProvider(null), StandardWidgetFunctions.DataTypeSelectorWidget, null,
                    "Embedable fields, Data type", new HelpDefinition("If a data type is selected, fields from this type can be inserted into the xhtml."));

            base.AddParameterProfile(typeNamePP);

            StringSelector.BuildInlineXhtmlEditorParameters().ForEach(AddParameterProfile);
        }
    }
}
