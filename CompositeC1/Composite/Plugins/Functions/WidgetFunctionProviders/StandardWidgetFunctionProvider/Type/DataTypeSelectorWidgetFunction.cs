/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections;
using System.Xml.Linq;
using Composite.Data;
using Composite.Functions;
using Composite.Plugins.Functions.WidgetFunctionProviders.StandardWidgetFunctionProvider.Foundation;
using Composite.Core.Xml;
using Composite.Core.Types;


namespace Composite.Plugins.Functions.WidgetFunctionProviders.StandardWidgetFunctionProvider.DataType
{
    internal sealed class DataTypeSelectorWidgetFunction : CompositeWidgetFunctionBase
    {
        public static IEnumerable GetOptions()
        {
            return
                DataFacade.GetAllKnownInterfaces(UserType.Developer); //.Where(f=>f.Namespace.StartsWith(typeof(Composite.Data.Types.IPage).Namespace)==false).ToList();
        }

        private const string _functionName = "DataTypeSelector";
        public const string CompositeName = CompositeWidgetFunctionBase.CommonNamespace + ".Type." + _functionName;

        public DataTypeSelectorWidgetFunction(EntityTokenFactory entityTokenFactory)
            : base(CompositeName, typeof(Type), entityTokenFactory)
        {
        }



        public override XElement GetWidgetMarkup(ParameterList parameters, string label, HelpDefinition helpDefinition, string bindingSourceName)
        {
            string tagName = "TypeSelector";

            XElement selector = StandardWidgetFunctions.BuildBasicFormsMarkup(Namespaces.BindingFormsStdUiControls10, tagName, "SelectedType", label, helpDefinition, bindingSourceName);
            XNamespace f = Namespaces.BindingFormsStdFuncLib10;

            selector.Add(
                new XElement(selector.Name.Namespace + (tagName + ".TypeOptions"),
                    new XElement(f + "StaticMethodCall",
                       new XAttribute("Type", TypeManager.SerializeType(this.GetType())),
                       new XAttribute("Method", "GetOptions"))));

            return selector;
        }
    }
}
