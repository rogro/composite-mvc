/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections;
using System.Linq;
using System.Xml.Linq;
using Composite.Functions;
using Composite.Plugins.Functions.WidgetFunctionProviders.StandardWidgetFunctionProvider.DataReference;
using Composite.Plugins.Functions.WidgetFunctionProviders.StandardWidgetFunctionProvider.Foundation;
using Composite.Core.Types;
using Composite.Core.Xml;


namespace Composite.Plugins.Functions.WidgetFunctionProviders.StandardWidgetFunctionProvider.String
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class DataIdMultiSelectorWidgetFunction : CompositeWidgetFunctionBase
    {
        private const string _functionName = "DataIdMultiSelector";
        internal const string CompositeName = CompositeWidgetFunctionBase.CommonNamespace + ".String." + _functionName;

        /// <summary> 
        /// Is called through reflection   
        /// </summary>
        /// <exclude />
        public static IEnumerable GetOptions(string typeManagerName)
        {
            return GetOptionsCommon.GetOptions(typeManagerName);
        }

        /// <exclude />
        public DataIdMultiSelectorWidgetFunction(EntityTokenFactory entityTokenFactory)
            : base(CompositeName, typeof(string), entityTokenFactory)
        {
            SetParameterProfiles();
        }


        private void SetParameterProfiles()
        {
            ParameterProfile dataTypePP =
                new ParameterProfile("OptionsType",
                    typeof(Type), 
                    true,
                    new NoValueValueProvider(), 
                    StandardWidgetFunctions.DataTypeSelectorWidget, 
                    null,
                    "Data type to select from", new HelpDefinition("The list of options the user can choose from will be selected from this type."));

            base.AddParameterProfile(dataTypePP);

            ParameterProfile compactModePP =
                new ParameterProfile("CompactMode",
                    typeof(bool),
                    false,
                    new ConstantValueProvider(false),
                    StandardWidgetFunctions.GetBoolSelectorWidget("Compact", "Verbose"),
                    null,
                    "Compact UI", new HelpDefinition("When true, a more compact representation of long option lists is used. Default is false (verbose)."));

            base.AddParameterProfile(compactModePP);
        }

        /// <exclude />
        public override XElement GetWidgetMarkup(ParameterList parameters, string label, HelpDefinition helpDefinition, string bindingSourceName)
        {
            Type optionsType = parameters.GetParameter<Type>("OptionsType");
            bool compactMode = parameters.GetParameter<bool>("CompactMode");
            return BuildStaticCallPopulatedSelectorFormsMarkup(
                    parameters,
                    label,
                    helpDefinition,
                    bindingSourceName,
                    this.GetType(),
                    "GetOptions",
                    TypeManager.SerializeType(optionsType),
                    "Key",
                    "Label",
                    false,
                    compactMode);
        }


        private static XElement BuildStaticCallPopulatedSelectorFormsMarkup(ParameterList parameters, string label, HelpDefinition helpDefinition, string bindingSourceName, Type optionsGeneratingStaticType, string optionsGeneratingStaticMethodName, object optionsGeneratingStaticMethodParameterValue, string optionsObjectKeyPropertyName, string optionsObjectLabelPropertyName, bool required, bool compactMode)
        {
            string tagName = "MultiKeySelector";

            XElement selector = StandardWidgetFunctions.BuildBasicFormsMarkup(Namespaces.BindingFormsStdUiControls10, tagName, "SelectedAsString", label, helpDefinition, bindingSourceName);
            XNamespace f = Namespaces.BindingFormsStdFuncLib10;

            selector.Add(
                new XAttribute("OptionsKeyField", optionsObjectKeyPropertyName),
                new XAttribute("OptionsLabelField", optionsObjectLabelPropertyName),
                new XAttribute("Required", required),
                new XAttribute("CompactMode", compactMode),
                new XElement(selector.Name.Namespace + (tagName + ".Options"),
                    new XElement(f + "StaticMethodCall",
                       new XAttribute("Type", TypeManager.SerializeType(optionsGeneratingStaticType)),
                       new XAttribute("Method", optionsGeneratingStaticMethodName))));

            if (optionsGeneratingStaticMethodParameterValue != null) selector.Descendants(f + "StaticMethodCall").First().Add(
                  new XAttribute("Parameters", optionsGeneratingStaticMethodParameterValue));

            return selector;
        }
    }
}
