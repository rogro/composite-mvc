/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Xml.Linq;
using System.Collections.Generic;

using Composite.Functions;
using Composite.Plugins.Functions.WidgetFunctionProviders.StandardWidgetFunctionProvider.Foundation;
using Composite.Core.Xml;


namespace Composite.Plugins.Functions.WidgetFunctionProviders.StandardWidgetFunctionProvider.String
{
	internal sealed class FontIconSelectorWidgetFuntion : CompositeWidgetFunctionBase
    {
        private const string _functionName = "FontIconSelector";

        /// <exclude />
        public const string CompositeName = CompositeWidgetFunctionBase.CommonNamespace + ".String." + _functionName;

        /// <exclude />
        public const string ClassNameOptionsParameterName = "ClassNameOptions";
        public const string StylesheetPathParameterName = "StylesheetPath";
        public const string ClassNamePrefixParameterName = "ClassNamePrefix";

        public FontIconSelectorWidgetFuntion(EntityTokenFactory entityTokenFactory)
            : base(CompositeName, typeof(string), entityTokenFactory)
        {
            base.AddParameterProfile(
                new ParameterProfile(FontIconSelectorWidgetFuntion.ClassNameOptionsParameterName,
                    typeof(object), true,
                    new ConstantValueProvider(null), StandardWidgetFunctions.TextAreaWidget, null,
                    "Class Name Options", new HelpDefinition("A list of class names the user should be able to choose from. Pass in a (comma seperated) string, a IEnumerable<string> or a Dictionary<string,string>.")));

            base.AddParameterProfile(
                new ParameterProfile(FontIconSelectorWidgetFuntion.StylesheetPathParameterName,
                    typeof(string), true,
                    new ConstantValueProvider(null), StandardWidgetFunctions.TextBoxWidget, null,
                    "Stylesheet Path", new HelpDefinition("Path to style sheet containing class name definitions and web font include. Example: ~/Frontend/styles/glyphicons.less")));

            base.AddParameterProfile(
                new ParameterProfile(FontIconSelectorWidgetFuntion.ClassNamePrefixParameterName,
                    typeof(string), false,
                    new ConstantValueProvider(""), StandardWidgetFunctions.TextBoxWidget, null,
                    "Class Name Prefix", new HelpDefinition("String to always prepend to the class name options for font icons to be shown in drop down. Bootstrap Example: 'glyphicon glyphicon-'")));
        }



        public override XElement GetWidgetMarkup(ParameterList parameters, string label, HelpDefinition help, string bindingSourceName)
        {
            string stylesheetPath = parameters.GetParameter<string>(FontIconSelectorWidgetFuntion.StylesheetPathParameterName);
            string classNamePrefix = parameters.GetParameter<string>(FontIconSelectorWidgetFuntion.ClassNamePrefixParameterName);

            XElement formElement = base.BuildBasicWidgetMarkup("FontIconSelector", "SelectedClassName", label, help, bindingSourceName);
            formElement.Add(new XElement(Namespaces.BindingFormsStdUiControls10+"FontIconSelector.ClassNameOptions", 
                GetClassNameOptionsValueNodes(parameters)));
            formElement.Add(new XAttribute("StylesheetPath", stylesheetPath));
            formElement.Add(new XAttribute("ClassNamePrefix", classNamePrefix));
            return formElement;
        }

        private IEnumerable<XNode> GetClassNameOptionsValueNodes(ParameterList parameters)
        {
            BaseRuntimeTreeNode classNameOptionsRuntimeTreeNode = null;
            if (parameters.TryGetParameterRuntimeTreeNode(FontIconSelectorWidgetFuntion.ClassNameOptionsParameterName, out classNameOptionsRuntimeTreeNode))
            {
                object value = parameters.GetParameter(FontIconSelectorWidgetFuntion.ClassNameOptionsParameterName);
                if (value is string)
                {
                    yield return new XText((string)value);
                }
                else
                {
                    foreach (var node in classNameOptionsRuntimeTreeNode.Serialize().Nodes())
                    {
                        yield return node;
                    }
                }
            }
        }
    }
}
