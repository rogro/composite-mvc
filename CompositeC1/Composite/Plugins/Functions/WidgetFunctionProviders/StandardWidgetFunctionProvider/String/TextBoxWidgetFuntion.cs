/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Xml.Linq;
using System.Collections.Generic;

using Composite.Functions;
using Composite.Plugins.Functions.WidgetFunctionProviders.StandardWidgetFunctionProvider.Foundation;


namespace Composite.Plugins.Functions.WidgetFunctionProviders.StandardWidgetFunctionProvider.String
{
	internal sealed class TextBoxWidgetFuntion : CompositeWidgetFunctionBase
    {
        private const string _functionName = "TextBox";

        /// <exclude />
        public const string CompositeName = CompositeWidgetFunctionBase.CommonNamespace + ".String." + _functionName;

        /// <exclude />
        public const string SpellCheckParameterName = "SpellCheck";

        public TextBoxWidgetFuntion(EntityTokenFactory entityTokenFactory)
            : base(CompositeName, typeof(string), entityTokenFactory)
        {
            ParameterProfile spellCheckPP =
                new ParameterProfile(TextBoxWidgetFuntion.SpellCheckParameterName,
                    typeof(bool), false,
                    new ConstantValueProvider(true), StandardWidgetFunctions.GetBoolSelectorWidget("Allow spell checking", "Do not allow spell checking"), null,
                    "Spell check", new HelpDefinition("By default text will be spell checked (when available). You should explicitly disable spell checking on fields that contain e-mails, code values etc. not suitable for spell checking. "));

            base.AddParameterProfile(spellCheckPP);
        }



        public override XElement GetWidgetMarkup(ParameterList parameters, string label, HelpDefinition help, string bindingSourceName)
        {
            bool spellCheck = parameters.GetParameter<bool>(TextBoxWidgetFuntion.SpellCheckParameterName);

            XElement formElement = base.BuildBasicWidgetMarkup("TextBox", "Text", label, help, bindingSourceName);
            formElement.Add(new XAttribute("SpellCheck", spellCheck));
            return formElement;
        }
    }
}
