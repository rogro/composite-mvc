/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Xml.Linq;
using System.Collections.Generic;

using Composite.Functions;
using Composite.Plugins.Functions.WidgetFunctionProviders.StandardWidgetFunctionProvider.Foundation;
using Composite.C1Console.Forms.CoreUiControls;


namespace Composite.Plugins.Functions.WidgetFunctionProviders.StandardWidgetFunctionProvider.Bool
{
    internal sealed class BoolSelectorWidgetFuntion : CompositeWidgetFunctionBase
    {
        private const string _functionName = "BoolSelector";
        public const string CompositeName = CompositeWidgetFunctionBase.CommonNamespace + ".Bool." + _functionName;

        public const string TrueLabelParameterName = "TrueLabel";
        public const string FalseLabelParameterName = "FalseLabel";

        public BoolSelectorWidgetFuntion(EntityTokenFactory entityTokenFactory)
            : base(CompositeName, typeof(bool), entityTokenFactory)
        {
            SetParameterProfiles( true, "True", "False");
        }

        public BoolSelectorWidgetFuntion(EntityTokenFactory entityTokenFactory, string trueLabel, string falseLabel)
            : base(CompositeName, typeof(bool), entityTokenFactory)
        {
            SetParameterProfiles(false, trueLabel, falseLabel);
        }

        private void SetParameterProfiles(bool require, string trueLabel, string falseLabel)
        {
            ParameterProfile trueLabelPP =
                new ParameterProfile(BoolSelectorWidgetFuntion.TrueLabelParameterName,
                    typeof(string), require,
                    new ConstantValueProvider(trueLabel), StandardWidgetFunctions.TextBoxWidget, null,
                    "True label", new HelpDefinition("Label to show when value is true"));

            ParameterProfile falseLabelPP =
                new ParameterProfile(BoolSelectorWidgetFuntion.FalseLabelParameterName,
                    typeof(string), require,
                    new ConstantValueProvider(falseLabel), StandardWidgetFunctions.TextBoxWidget, null,
                    "False label", new HelpDefinition("Label to show when value is false"));

            base.AddParameterProfile(trueLabelPP);
            base.AddParameterProfile(falseLabelPP);
        }



        public override XElement GetWidgetMarkup(ParameterList parameters, string label, HelpDefinition help, string bindingSourceName)
        {
            XElement boolSelectorMarkup = base.BuildBasicWidgetMarkup("BoolSelector", "IsTrue", label, help, bindingSourceName);

            boolSelectorMarkup.Add(new XAttribute("TrueLabel", parameters.GetParameter<string>(BoolSelectorWidgetFuntion.TrueLabelParameterName)));
            boolSelectorMarkup.Add(new XAttribute("FalseLabel", parameters.GetParameter<string>(BoolSelectorWidgetFuntion.FalseLabelParameterName)));

            return boolSelectorMarkup;
        }
    }
}
