/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Web.Routing;
using Composite.Core.Routing.Pages;
using Composite.Core.WebClient;

namespace Composite.Core.Routing
{
    /// <summary>
    /// Allows adding custom routes with a priority in relation to defined by CompositeC1 routes.
    /// </summary>
    public static class Routes
    {
        /// <exclude />
        [Obsolete("Use RegisterPageRoute() and Register404Route() instead", true)]
        public static void Register()
        {
            var routes = RouteTable.Routes;

            RegisterPageRoute(routes);
            Register404Route(routes);
        }

        /// <summary>
        /// Registers C1's page route.
        /// </summary>
        public static void RegisterPageRoute(RouteCollection routes)
        {
            routes.Ignore("Composite/{*pathInfo}");
            routes.Ignore("{resource}.axd/{*pathInfo}");

            AddFunctionBoxRoute(routes);
            AddSiteMapRoutes(routes);

            if (OnBeforePageRouteAdded != null)
            {
                OnBeforePageRouteAdded(routes);
            }

            routes.Add("c1 page route", new C1PageRoute());

            if (OnAfterPageRouteAdded != null)
            {
                OnAfterPageRouteAdded(routes);
            }
        }

        /// <summary>
        /// Registers C1's 404 route that catches all requests. 
        /// This method should be called only after all other routes are registered.
        /// </summary>
        public static void Register404Route(RouteCollection routes)
        {
            // Ignoring routes that shouldn't be caught by 404 handler
            routes.Ignore("Renderers/{*pathInfo}");
            routes.Ignore("{*all_css_aspx}", new { all_css_aspx = @".*\.css.aspx(/.*)?" });
            routes.Ignore("{*all_js_aspx}", new { all_js_aspx = @".*\.js.aspx(/.*)?" });

            routes.Add("c1 404 route", new PageNotFoundRoute());
        }

        private static void AddSiteMapRoutes(RouteCollection routes)
        {
            routes.Ignore("sitemap.xml");
            routes.Ignore("{language}/sitemap.xml");
            routes.Ignore("{language}/{urlTitle}/sitemap.xml");
        }

        private static void AddFunctionBoxRoute(RouteCollection routes)
        {
            routes.Add("c1 function image", new FunctionBoxRoute());
            routes.Add("c1 template preview", new TemplatePreviewRoute());
        }

        /// <summary>
        /// Occurs before C1 page route is added.
        /// </summary>
        public static event RouteRegistration OnBeforePageRouteAdded;

        /// <summary>
        /// Occurs after C1 page route is added.
        /// </summary>
        public static event RouteRegistration OnAfterPageRouteAdded;

        /// <summary>
        /// Handles route registration
        /// </summary>
        /// <param name="routeCollection">The route collection.</param>
        public delegate void RouteRegistration(RouteCollection routeCollection);
    }
}
