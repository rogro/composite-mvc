/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Configuration;
using Composite.Core.Configuration;
using Composite.Core.Routing.Plugins.PageUrlsProviders.Runtime;
using Composite.Core.Routing.Plugins.UrlFormatters.Runtime;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;


namespace Composite.Core.Routing.Plugins.Runtime
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class UrlsConfiguration : SerializableConfigurationSection
    {
        /// <exclude />
        public const string SectionName = "Composite.Core.Urls";


        private const string _defaultPageUrlProviderNameProperty = "defaultPageUrlProviderName";
        /// <exclude />
        [ConfigurationProperty(_defaultPageUrlProviderNameProperty, IsRequired = true)]
        public string DefaultPageUrlProviderName
        {
            get { return (string)base[_defaultPageUrlProviderNameProperty]; }
            set { base[_defaultPageUrlProviderNameProperty] = value; }
        }


        private const string _pageUrlProvidersProperty = "PageUrlProviders";
        /// <exclude />
        [ConfigurationProperty(_pageUrlProvidersProperty)]
        public NameTypeManagerTypeConfigurationElementCollection<PageUrlProviderData> PageUrlProviders
        {
            get
            {
                return (NameTypeManagerTypeConfigurationElementCollection<PageUrlProviderData>)base[_pageUrlProvidersProperty];
            }
        }

        private const string _urlFormattersProperty = "UrlFormatters";
        /// <exclude />
        [ConfigurationProperty(_urlFormattersProperty)]
        public NameTypeManagerTypeConfigurationElementCollection<UrlFormatterData> UrlFormatters
        {
            get
            {
                return (NameTypeManagerTypeConfigurationElementCollection<UrlFormatterData>)base[_urlFormattersProperty];
            }
        }
    }
}
