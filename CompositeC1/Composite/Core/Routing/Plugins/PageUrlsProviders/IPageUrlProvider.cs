/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Globalization;
using Composite.Core.Routing.Plugins.PageUrlsProviders.Runtime;
using Composite.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ObjectBuilder;

namespace Composite.Core.Routing.Plugins.PageUrlsProviders
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    [CustomFactory(typeof(PageUrlProviderCustomFactory))]
    public interface IPageUrlProvider
    {
        /// <summary>
        /// Creates a new instance of PageUrlBuilder which will be used while building a C1 pages sitemap
        /// </summary>
        /// <param name="publicationScope">The publication scope.</param>
        /// <param name="localizationScope">The localization scope.</param>
        /// <param name="urlSpace">The URL space. Is used for providing different urls for f.e. different hostnames, etc.</param>
        /// <returns></returns>
        [Obsolete]
        IPageUrlBuilder CreateUrlBuilder(PublicationScope publicationScope, CultureInfo localizationScope, UrlSpace urlSpace);

        /// <exclude />
        bool IsInternalUrl(string relativeUrl);

        /// <exclude />
        PageUrlData ParseInternalUrl(string relativeUrl);

        /// <exclude />
        PageUrlData ParseUrl(string relativeUrl, UrlSpace urlSpace, out UrlKind urlKind);

        /// <exclude />
        PageUrlData ParseUrl(string absoluteUrl, out UrlKind urlKind);

        /// <exclude />
        string BuildUrl(PageUrlData pageUrlData, UrlKind urlKind, UrlSpace urlSpace);
    }
}
