/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using Composite.C1Console.Events;
using Composite.Core.Collections.Generic;
using Composite.Core.Configuration;
using Composite.Core.Routing.Plugins.Runtime;
using Composite.Core.Routing.Plugins.UrlFormatters;
using Composite.Core.Routing.Plugins.UrlFormatters.Runtime;

namespace Composite.Core.Routing.Foundation.PluginFacades
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class UrlFormattersPluginFacade
    {
        private static readonly string LogTitle = typeof(UrlFormattersPluginFacade).FullName;
        private static readonly ResourceLocker<Resources> _resourceLocker = new ResourceLocker<Resources>(new Resources(), Resources.Initialize);

        static UrlFormattersPluginFacade()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(args => _resourceLocker.ResetInitialization());
        }

        /// <exclude />
        public static string FormatUrl(string url, bool onlyMandatory)
        {
            IEnumerable<Tuple<IUrlFormatter, bool>> urlFormatters = _resourceLocker.Resources.UrlFormatters;

            foreach(var urlFormatter in urlFormatters)
            {
                if (!onlyMandatory || urlFormatter.Item2)
                {
                    url = urlFormatter.Item1.FormatUrl(url);
                }
            }

            return url;
        }

        private sealed class Resources
        {
            public IEnumerable<Tuple<IUrlFormatter, bool>> UrlFormatters { get; private set; }

            public static void Initialize(Resources resources)
            {
                const string sectionName = UrlsConfiguration.SectionName;
                var routingConfiguration = ConfigurationServices.ConfigurationSource.GetSection(sectionName) as UrlsConfiguration;
                Verify.IsNotNull(routingConfiguration, "Config section '{0}' is missing", sectionName);

                var factory = new UrlFormatterFactory();

                var formatters = new List<Tuple<IUrlFormatter, bool>>();

                var urlFormattersConfigNode = routingConfiguration.UrlFormatters;
                if (urlFormattersConfigNode != null)
                {
                    foreach (var urlFormatterData in urlFormattersConfigNode)
                    {
                        string name = urlFormatterData.Name;

                        try
                        {
                            formatters.Add(new Tuple<IUrlFormatter, bool>(factory.Create(name), urlFormatterData.Mandatory));
                        }
                        catch(Exception ex)
                        {
                            Log.LogError(LogTitle, "Failed to load url formatter '{0}'", name);
                            Log.LogError(LogTitle, ex);
                        }
                    }
                }

                resources.UrlFormatters = formatters;
            }
        }
    }
}
