/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using Composite.C1Console.Events;
using Composite.Core.Collections.Generic;
using Composite.Core.Configuration;
using Composite.Core.Routing.Plugins.PageUrlsProviders;
using Composite.Core.Routing.Plugins.PageUrlsProviders.Runtime;
using Composite.Core.Routing.Plugins.Runtime;

namespace Composite.Core.Routing.Foundation.PluginFacades
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    public static class PageUrlProviderPluginFacade
    {
        private static readonly ResourceLocker<Resources> _resourceLocker = new ResourceLocker<Resources>(new Resources(), Resources.Initialize);

        static PageUrlProviderPluginFacade()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(args => _resourceLocker.ResetInitialization());
        }

        /// <exclude />
        public static IPageUrlProvider GetDefaultProvider()
        {
            var resources = _resourceLocker.Resources;

            string providerName = resources.DefaultPageUrlProviderName;

            var provider = resources.ProviderCache[providerName];

            if (provider == null)
            {
                lock (resources.ProviderCache)
                {
                    provider = resources.ProviderCache[providerName];
                    if (provider == null)
                    {
                        provider = resources.Factory.Create(providerName);
                        Verify.IsNotNull(provider, "Failed to build page url provider '{0}'", providerName);

                        resources.ProviderCache.Add(providerName, provider);
                    }
                }
            }

            return provider;
        }

        private sealed class Resources
        {
            private string _defaultPageUrlProviderName;
            public PageUrlProviderFactory Factory { get; set; }
            public Hashtable<string, IPageUrlProvider> ProviderCache { get; set; }

            public static void Initialize(Resources resources)
            {
                resources.Factory = new PageUrlProviderFactory();
                resources.ProviderCache = new Hashtable<string, IPageUrlProvider>();
            }

            public string DefaultPageUrlProviderName
            {
                get
                {
                    if (_defaultPageUrlProviderName == null)
                    {
                        string sectionName = UrlsConfiguration.SectionName;
                        var routingConfiguration = ConfigurationServices.ConfigurationSource.GetSection(sectionName) as UrlsConfiguration;

                        Verify.IsNotNull(routingConfiguration, "Missing configuration section '{0}'", sectionName);

                        _defaultPageUrlProviderName = routingConfiguration.DefaultPageUrlProviderName;
                    }

                    return _defaultPageUrlProviderName;
                }
            }
        }
    }
}
