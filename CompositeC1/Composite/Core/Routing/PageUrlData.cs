/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Specialized;
using System.Globalization;
using Composite.Data;
using Composite.Data.Types;

namespace Composite.Core.Routing
{
    /// <summary>
    /// Information stored in a Composite C1 page url
    /// </summary>
    public class PageUrlData
    {
        /// <exclude />
        public PageUrlData()
        {
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="PageUrlData"/> class.
        /// </summary>
        /// <param name="page">The page.</param>
        public PageUrlData(IPage page)
        {
            Verify.ArgumentNotNull(page, "page");

            PageId = page.Id;
            this.PublicationScope = page.DataSourceId.PublicationScope;
            this.LocalizationScope = page.DataSourceId.LocaleScope;
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="PageUrlData"/> class.
        /// </summary>
        /// <param name="pageId">The page id.</param>
        /// <param name="publicationScope">The publication scope.</param>
        /// <param name="localizationScope">The localization scope.</param>
        public PageUrlData(Guid pageId, PublicationScope publicationScope, CultureInfo localizationScope)
        {
            PageId = pageId;
            PublicationScope = publicationScope;
            LocalizationScope = localizationScope;
        }


        /// <summary>
        /// Gets or sets the page id.
        /// </summary>
        /// <value>
        /// The page id.
        /// </value>
        public Guid PageId { get; set; }


        /// <summary>
        /// Gets or sets the publication scope.
        /// </summary>
        /// <value>
        /// The publication scope.
        /// </value>
        public PublicationScope PublicationScope { get; set; }


        /// <summary>
        /// Gets or sets the localization scope.
        /// </summary>
        /// <value>
        /// The localization scope.
        /// </value>
        public CultureInfo LocalizationScope { get; set; }


        /// <summary>
        /// Gets or sets the path info.
        /// </summary>
        /// <value>
        /// The path info.
        /// </value>
        public virtual string PathInfo { get; set; }


        /// <summary>
        /// Gets or sets the query parameters.
        /// </summary>
        /// <value>
        /// The query parameters.
        /// </value>
        public virtual NameValueCollection QueryParameters { get; set; }
    }
}
