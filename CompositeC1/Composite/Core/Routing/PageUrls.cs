/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using Composite.Core.Routing.Foundation.PluginFacades;
using Composite.Core.Routing.Plugins.PageUrlsProviders;
using Composite.Data.Types;

namespace Composite.Core.Routing
{
    /// <summary> 
    /// Responsible for parsing and building page urls
    /// </summary>
    public static class PageUrls
    {
        private static IPageUrlProvider GetDefaultProvider()
        {
            return PageUrlProviderPluginFacade.GetDefaultProvider();
        }

        /// <exclude />
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public static IPageUrlProvider UrlProvider
        {
            get { return GetDefaultProvider(); }
        }

        /// <summary>
        /// Parses the URL.
        /// </summary>
        /// <param name="absoluteUrl">The absolute URL.</param>
        /// <returns></returns>
        public static PageUrlData ParseUrl(string absoluteUrl)
        {
            UrlKind urlKind;
            return ParseUrl(absoluteUrl, out urlKind);
        }

        /// <summary>
        /// Parses the URL.
        /// </summary>
        /// <param name="absoluteUrl">The absolute URL.</param>
        /// <param name="urlKind">Kind of the URL.</param>
        /// <returns></returns>
        public static PageUrlData ParseUrl(string absoluteUrl, out UrlKind urlKind)
        {
            if (absoluteUrl.StartsWith("http") && absoluteUrl.Contains("://"))
            {
                return UrlProvider.ParseUrl(absoluteUrl, out urlKind);
            }

            return UrlProvider.ParseUrl(absoluteUrl, new UrlSpace(), out urlKind);
        }

        /// <summary>
        /// Parses the URL.
        /// </summary>
        /// <param name="relativeUrl">The relative URL.</param>
        /// <param name="urlSpace">The URL space.</param>
        /// <param name="urlKind">Kind of the URL.</param>
        /// <returns></returns>
        public static PageUrlData ParseUrl(string relativeUrl, UrlSpace urlSpace, out UrlKind urlKind) 
        {
            return UrlProvider.ParseUrl(relativeUrl, urlSpace, out urlKind);
        }

        /// <summary>
        /// Builds the URL.
        /// </summary>
        /// <param name="pageUrlData">The page URL data.</param>
        /// <param name="urlKind">Kind of the URL.</param>
        /// <param name="urlSpace">The URL space.</param>
        /// <returns></returns>
        public static string BuildUrl(PageUrlData pageUrlData, UrlKind urlKind, UrlSpace urlSpace) 
        {
            return UrlProvider.BuildUrl(pageUrlData, urlKind, urlSpace);
        }

        /// <summary>
        /// Builds the URL.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="urlKind">Kind of the URL.</param>
        /// <param name="urlSpace">The URL space.</param>
        /// <returns></returns>
        public static string BuildUrl(IPage page, UrlKind urlKind = UrlKind.Public, UrlSpace urlSpace = null)
        {
            Verify.ArgumentNotNull(page, "page");

            return BuildUrl(new PageUrlData(page), urlKind, urlSpace ?? new UrlSpace());
        }
    }
}
