/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

namespace Composite.Core.Routing
{
    /// <summary>
    /// Url kind
    /// </summary>
    public enum UrlKind
    {
        /// <exclude />
        Undefined = 0,
        /// <summary>
        /// A main, human friendly url by which a resource is accessed. F.e.:
        /// Page: "/Home/About"
        /// An image: "/media/6fb4c70b-12a6-4522-add6-1f40828c5452/Sample images/Colors of Inspiration.jpg"
        /// </summary>
        Public = 1,
        /// <summary>
        /// Url to an ASP.NET handler. F.e. link to a page: "/Renderers/Page.aspx?id=7446ceda-df90-49f0-a183-4e02ed6f6eec"
        /// Renderer url is expected to be handled without routing.
        /// </summary>
        Renderer = 2,
        /// <summary>
        /// The way links are kept in html content
        /// For pages 
        ///   Short: ~/page({Page id})
        ///   Full:  ~/page({Page id})[ /c1mode(unpublished) ][ /{PathInfo} ][ ?{Query string} ]
        /// For media archive
        ///   Short: ~/media({Media file Id})
        ///   Full:  ~/media([{Media store}:]{Media file Id})[ ?{Query string} ]
        /// </summary>
        Internal = 5,
        /// <summary>
        /// Friendly url. A short url, by accessing which C1 will make a redirect to related "public" url
        /// </summary>
        Friendly = 3,
        /// <summary>
        /// Redirect url. As in the case of "friendly urls", is used for supporting obsolete urls 
        /// </summary>
        Redirect = 4
    }
}
