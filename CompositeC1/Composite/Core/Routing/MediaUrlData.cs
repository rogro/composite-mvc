/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Specialized;
using Composite.Data.Types;

namespace Composite.Core.Routing
{
    /// <summary> 
    /// Information stored in a media url
    /// </summary>
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    public class MediaUrlData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MediaUrlData"/> class.
        /// </summary>
        public MediaUrlData() {}

        /// <summary>
        /// Initializes a new instance of the <see cref="MediaUrlData" /> class.
        /// </summary>
        /// <param name="mediaStore">The media store.</param>
        /// <param name="mediaId">The media id.</param>
        /// <param name="queryParameters">The query parameters.</param>
        public MediaUrlData(string mediaStore, Guid mediaId, NameValueCollection queryParameters = null)
        {
            MediaStore = mediaStore;
            MediaId = mediaId;
            QueryParameters = queryParameters ?? new NameValueCollection();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MediaUrlData"/> class.
        /// </summary>
        /// <param name="mediaFile">The media file.</param>
        public MediaUrlData(IMediaFile mediaFile)
        {
            MediaStore = mediaFile.StoreId;
            MediaId = mediaFile.Id;
            QueryParameters = new NameValueCollection();
        }


        /// <summary>
        /// Gets or sets the media id.
        /// </summary>
        /// <value>
        /// The media id.
        /// </value>
        public Guid MediaId { get; set; }


        /// <summary>
        /// Gets or sets the media store.
        /// </summary>
        /// <value>
        /// The media store.
        /// </value>
        public string MediaStore { get; set; }


        /// <summary>
        /// Gets or sets the query parameters.
        /// </summary>
        /// <value>
        /// The query parameters.
        /// </value>
        public NameValueCollection QueryParameters { get; set; }
    }
}
