/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Web;
using Composite.Plugins.Routing.Pages;

namespace Composite.Core.Routing
{
    /// <summary>    
    /// Describes a set of page urls, generated specifictly. F.e. for a specific hostname or for previewing.
    /// </summary>
    public class UrlSpace
    {
        internal UrlSpace(string hostname)
        {
            Hostname = hostname;
            ForceRelativeUrls = false;
        }

        internal UrlSpace(string hostname, string relativeUrl)
        {
            Initialize(hostname, relativeUrl);
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="UrlSpace"/> class.
        /// </summary>
        public UrlSpace()
        {
            var httpContext = System.Web.HttpContext.Current;

            if(httpContext != null)
            {
                InitializeThroughHttpContext(httpContext);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UrlSpace"/> class.
        /// </summary>
        /// <param name="httpContext">The HTTP context.</param>
        public UrlSpace(HttpContext httpContext)
        {
            Verify.ArgumentNotNull(httpContext, "httpContext");

            InitializeThroughHttpContext(httpContext);
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="UrlSpace"/> class.
        /// </summary>
        /// <param name="httpContextBase">The HTTP context base.</param>
        public UrlSpace(HttpContextBase httpContextBase)
        {
            Verify.ArgumentNotNull(httpContextBase, "httpContextBase");

            var url = httpContextBase.Request.Url;

            Initialize(url.Host, url.LocalPath);
        }

        private void InitializeThroughHttpContext(HttpContext httpContext)
        {
            Initialize(httpContext.Request.Url.Host, httpContext.Request.Url.LocalPath);
        }

        private void Initialize(string hostname, string relativeUrl)
        {
            ForceRelativeUrls = HttpUtility.UrlDecode(relativeUrl).Contains(DefaultPageUrlProvider.UrlMarker_RelativeUrl);

            if (!ForceRelativeUrls)
            {
                Hostname = hostname;
            }
        }


        /// <summary>
        /// Gets or sets the hostname.
        /// </summary>
        /// <value>
        /// The hostname.
        /// </value>
        public string Hostname { get; set; }

        /// <summary>
        /// Disables hostname bindings, so all output urls will be relative. Is used in in-console preview.
        /// </summary>
        public bool ForceRelativeUrls { get; set; }
    }
}
