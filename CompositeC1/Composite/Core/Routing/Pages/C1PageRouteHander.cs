/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web;
using System.Web.Compilation;
using System.Web.Configuration;
using System.Web.Hosting;
using System.Web.Routing;
using System.Web.UI;
using System.Xml.Linq;
using Composite.Core.Extensions;
using Composite.Core.Linq;

namespace Composite.Core.Routing.Pages
{
    internal class C1PageRouteHandler : IRouteHandler
    {
        private readonly PageUrlData _pageUrlData;

        private static readonly Type _handlerType;


        [SuppressMessage("Composite.IO", "Composite.DoNotUseConfigurationClass:DoNotUseConfigurationClass")]
        static C1PageRouteHandler()
        {
            bool isIntegratedPipeline = HttpRuntime.UsingIntegratedPipeline;
            string sectionName = isIntegratedPipeline ? "system.webServer" : "system.web";

            var config = WebConfigurationManager.OpenWebConfiguration(HostingEnvironment.ApplicationVirtualPath).GetSection(sectionName);
            if (config != null)
            {
                string handlersSectionName = isIntegratedPipeline ? "handlers" : "httpHandlers";

                var handlers = XElement.Parse(config.SectionInformation.GetRawXml()).Element(handlersSectionName);
                if(handlers == null)
                {
                    return;
                }

                var handler = handlers
                    .Elements("add")
                    .Where(e => e.Attribute("path") != null
                          && e.Attribute("path").Value.Equals("Renderers/Page.aspx", StringComparison.OrdinalIgnoreCase))
                    .SingleOrDefaultOrException("Multiple handlers for 'Renderers/Page.aspx' were found'");

                if (handler != null)
                {
                    var typeAttr = handler.Attribute("type");
                    Verify.IsNotNull(typeAttr, "'type' attribute is missing");

                    _handlerType = Type.GetType(typeAttr.Value);
                    if(_handlerType == null)
                    {
                        Log.LogError(typeof(C1PageRouteHandler).Name, "Failed to load type '{0}'", typeAttr.Value);
                    }
                }
            }

        }

        public C1PageRouteHandler(PageUrlData pageUrlData)
        {
            _pageUrlData = pageUrlData;
        }


        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            var context = requestContext.HttpContext;

            string localPath = context.Request.Url.LocalPath;
            string pathInfo = _pageUrlData.PathInfo;

            // Doing a url rewriting so ASP.NET will get correct FilePath/PathInfo properties
            if (!pathInfo.IsNullOrEmpty())
            {
                string filePath = localPath.Substring(0, localPath.Length - pathInfo.Length);

                string queryString = context.Request.Url.Query;
                if (queryString.StartsWith("?"))
                {
                    queryString = queryString.Substring(1);
                }
                context.RewritePath(filePath, pathInfo, queryString);
            }

            // Disabling ASP.NET cache if there's a logged-in user
            if (Composite.C1Console.Security.UserValidationFacade.IsLoggedIn())
            {
                context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            }

            if (_handlerType != null)
            {
                return (IHttpHandler)Activator.CreateInstance(_handlerType);
            }
            
            return (IHttpHandler)BuildManager.CreateInstanceFromVirtualPath("~/Renderers/Page.aspx", typeof(Page));
        }
    }
}
