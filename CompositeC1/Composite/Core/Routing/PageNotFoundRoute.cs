/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Web;
using System.Web.Routing;
using Composite.Core.WebClient;

namespace Composite.Core.Routing
{
    internal class PageNotFoundRoute : Route
    {
        public PageNotFoundRoute()
            : base("{*url}", new PageNotFoundRouteHandler())
        {
        }

        public override RouteData GetRouteData(HttpContextBase httpContext)
        {
            // Skipping the route is there's no associated "Page not found" url
            if(string.IsNullOrEmpty(HostnameBindingsFacade.GetCustomPageNotFoundUrl()))
            {
                return null;
            }

            // Skipping root request
            if(httpContext.Request.RawUrl.Length == UrlUtils.PublicRootPath.Length + 1)
            {
                return null;
            }

            return base.GetRouteData(httpContext);
        }
    }

    internal class PageNotFoundRouteHandler: IRouteHandler
    {
        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            var httpContext = HttpContext.Current;
            if (!HostnameBindingsFacade.RedirectCustomPageNotFoundUrl(httpContext))
            {
                throw new InvalidOperationException("Failed to redirect to 'page not found' url");
            }

            return EmptyHttpHandler.Instance;
        }

        private class EmptyHttpHandler : IHttpHandler
        {
            private EmptyHttpHandler()
            {
            }

            static EmptyHttpHandler()
            {
                Instance = new EmptyHttpHandler();
            }

            public static EmptyHttpHandler Instance { get; private set; }
            

            public bool IsReusable
            {
                get { return true; }
            }

            public void ProcessRequest(HttpContext context)
            {
                throw new InvalidOperationException("This code should not be reachable");
            }
        }
    }
}
