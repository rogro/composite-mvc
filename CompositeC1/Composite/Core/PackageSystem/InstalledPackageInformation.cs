/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.Core.PackageSystem.Foundation;
using Composite.Core.Serialization;


namespace Composite.Core.PackageSystem
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [SerializerHandler(typeof(InstalledPackageInformationSerializerHandler))]
    public sealed class InstalledPackageInformation
    {
        /// <exclude />
        public Guid Id { get; internal set; }

        /// <exclude />
        public string Name { get; internal set; }

        /// <exclude />
        public string GroupName { get; internal set; }

        /// <exclude />
        public string Version { get; internal set; }

        /// <exclude />
        public string Author { get; internal set; }

        /// <exclude />
        public string Website { get; internal set; }

        /// <exclude />
        public string Description { get; internal set; }

        /// <exclude />
        public DateTime InstallDate { get; internal set; }

        /// <exclude />
        public string InstalledBy { get; internal set; }

        /// <exclude />
        public bool IsLocalInstalled { get; internal set; }

        /// <exclude />
        public bool CanBeUninstalled { get; internal set; }

        /// <exclude />
        public bool FlushOnCompletion { get; internal set; }

        /// <exclude />
        public bool ReloadConsoleOnCompletion { get; internal set; }

        /// <exclude />
        public SystemLockingType SystemLockingType { get; internal set; }

        /// <exclude />
        public string PackageServerAddress { get; internal set; }

        /// <exclude />
        internal string PackageInstallPath { get; set; }
    }
}

