/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Composite.C1Console.Events;
using Composite.Core.Types.Foundation;


namespace Composite.Core.PackageSystem
{
    internal static class PackageAssemblyHandler
    {
        private static bool _initialized = false;
        private static readonly object _lock = new object();
        private static AssemblyFilenameCollection _loadedAssemblyFilenames = new AssemblyFilenameCollection();


        public static void Initialize()
        {
            if (!_initialized)
            {
                lock (_lock)
                {
                    if (!_initialized)
                    {
                        GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlushEvent);

                        AppDomain.CurrentDomain.AssemblyResolve += OnAssemblyResolve;

                        _initialized = true;
                    }
                }
            }
        }



        public static void AddAssembly(string assemblyFilePath)
        {
            Initialize();

            lock (_lock)
            {
                _loadedAssemblyFilenames.Add(assemblyFilePath);
            }
        }



        private static Assembly OnAssemblyResolve(object sender, ResolveEventArgs args)
        {
            string filename = args.Name;

            // Why can the system not load the "System.Web.Extensions" assembly? 
            // And "Composite.Core.XmlSerializers" <-- Licensing?
            // For now ignore it, so no exception is thrown /MRJ
            if ((filename == "System.Web.Extensions") ||
                (filename.StartsWith("Composite.Core.XmlSerializers")))
            {
                return null;
            }

            string fn = filename;
            if (fn.Contains(","))
            {
                fn = fn.Remove(fn.IndexOf(",")).Trim();
            }

            if (_loadedAssemblyFilenames.ContainsAssemblyName(fn))
            {
                filename = _loadedAssemblyFilenames.GetFilenameByAssemblyName(fn);
            }

            Assembly assembly = null;
            if (filename.Contains(@":\"))
            {
                try
                {
                    assembly = Assembly.LoadFile(filename);
                }
                catch (Exception ex)
                {
                    Log.LogError("PackageAssemblyHandler", ex);
                }
            }

            return assembly;
        }


        public static void ClearAssemblyList()
        {
            Flush();
        }


        private static void Flush()
        {
            lock (_lock)
            {
                _loadedAssemblyFilenames = new AssemblyFilenameCollection();
            }
        }



        private static void OnFlushEvent(FlushEventArgs args)
        {
            Flush();
        }
    }
}
