/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.Core.Implementation;
using Composite.Plugins.Elements.ElementProviders.PackageElementProvider;


namespace Composite.Core.PackageSystem
{
    /// <summary>
    /// This class contains helper methods for handling packages
    /// </summary>
    public static class PackageUtils
    {
        /// <summary>
        /// This method returns an entity token for a install package in the C1 console.
        /// </summary>
        /// <param name="packageId">The id of the package.</param>
        /// <param name="groupName">The group name of the package.</param>
        /// <returns>Returns an entity token for a installed package.</returns>
        public static PackageElementProviderInstalledPackageItemEntityToken GetInstalledPackageEntityToken(Guid packageId, string groupName)
        {
            return ImplementationFactory.CurrentFactory.StatelessPackageUtils.GetInstalledPackageEntityToken(packageId, groupName);
        }
    }
}
