/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Composite.Core.IO;
using Composite.Data;
using Composite.Data.Types;


namespace Composite.Core.PackageSystem
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public enum ServerUrlValidationResult
    {
        /// <exclude />
        Http = 0,

        /// <exclude />
        Https = 1,

        /// <exclude />
        Invalid = 2
    }



    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class PackageServerFacade 
	{
        private static IPackageServerFacade _packageServerFacade;


        /// <exclude />
        static PackageServerFacade()
        {
            string testFilePath = Path.Combine(PathUtil.Resolve(PathUtil.BaseDirectory), "App_Data/Composite/PackageDescriptions.xml");
            if (C1File.Exists(testFilePath))
            {
                _packageServerFacade = new PackageServerFacadeLocalMock(testFilePath);
            }
            else
            {
                _packageServerFacade = new PackageServerFacadeImpl();
            }
        }



        internal static IPackageServerFacade Implementation { get { return _packageServerFacade; } set { _packageServerFacade = value; } }

        /// <summary>
        /// Connects to the server using either https or http and 
        /// checks to see if it supports the needed services
        /// </summary>
        /// <param name="packageServerUrl">
        /// Cleaned url (ex: www.composite.net)
        /// </param>
        /// <returns>
        /// </returns>
        public static ServerUrlValidationResult ValidateServerUrl(string packageServerUrl)
        {
            return _packageServerFacade.ValidateServerUrl(packageServerUrl);
        }



        /// <summary>
        /// Given a cleaned url, returns a series of PackageDescriptions 
        /// </summary>
        /// <param name="packageServerUrl">
        /// Cleaned url (ex: www.composite.net)
        /// </param>
        /// <param name="installationId"></param>
        /// <param name="userCulture"></param>
        /// <returns></returns>
        public static IEnumerable<PackageDescription> GetPackageDescriptions(string packageServerUrl, Guid installationId, CultureInfo userCulture)
        {
            return _packageServerFacade.GetPackageDescriptions(packageServerUrl, installationId, userCulture);
        }



        // Overload
        /// <exclude />
        public static IEnumerable<PackageDescription> GetAllPackageDescriptions(Guid installationId, CultureInfo userCulture)
        {
            List<IPackageServerSource> packageServerSources = DataFacade.GetData<IPackageServerSource>().ToList();

            foreach (IPackageServerSource packageServerSource in packageServerSources)
            {
                foreach (PackageDescription packageDescription in GetPackageDescriptions(packageServerSource.Url, installationId, userCulture))
                {
                    yield return packageDescription;
                }
            }
        }



        /// <exclude />
        public static string GetEulaText(string packageServerUrl, Guid eulaId, CultureInfo userCulture)
        {
            return _packageServerFacade.GetEulaText(packageServerUrl, eulaId, userCulture);
        }



        /// <exclude />
        public static Stream GetInstallFileStream(string packageFileDownloadUrl)
        {
            return _packageServerFacade.GetInstallFileStream(packageFileDownloadUrl);
        }



        /// <exclude />
        public static void RegisterPackageInstallationCompletion(string packageServerUrl, Guid installationId, Guid packageId, string localUserName, string localUserIp)
        {
            _packageServerFacade.RegisterPackageInstallationCompletion(packageServerUrl, installationId, packageId, localUserName, localUserIp);
        }



        /// <exclude />
        public static void RegisterPackageInstallationFailure(string packageServerUrl, Guid installationId, Guid packageId, string localUserName, string localUserIp, string exceptionString)
        {
            _packageServerFacade.RegisterPackageInstallationFailure(packageServerUrl, installationId, packageId, localUserName, localUserIp, exceptionString);
        }



        /// <exclude />
        public static void RegisterPackageUninstall(string packageServerUrl, Guid installationId, Guid packageId, string localUserName, string localUserIp)
        {
            _packageServerFacade.RegisterPackageUninstall(packageServerUrl, installationId, packageId, localUserName, localUserIp);
        }



        /// <exclude />
        public static void ClearServerCache()
        {
            _packageServerFacade.ClearCache();
        }
	}
}
