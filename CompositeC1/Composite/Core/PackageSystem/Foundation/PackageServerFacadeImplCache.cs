/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using Composite.Core.Logging;


namespace Composite.Core.PackageSystem.Foundation
{
	internal class PackageServerFacadeImplCache
	{
        private TimeSpan _cacheLiveTime;

        private DateTime _packageDescriptionsCacheTimestamp = DateTime.MinValue;
        private Dictionary<string, Dictionary<Guid, Dictionary<CultureInfo, List<PackageDescription>>>> _packageDescriptionsCache = new Dictionary<string, Dictionary<Guid, Dictionary<CultureInfo, List<PackageDescription>>>>();


        public PackageServerFacadeImplCache()
        {
            _cacheLiveTime = TimeSpan.FromMinutes(30);
            if (RuntimeInformation.IsDebugBuild)
            {
                _cacheLiveTime = TimeSpan.FromSeconds(30);
            }
        }


        public List<PackageDescription> GetCachedPackageDescription(string packageServerUrl, Guid installationId, CultureInfo userCulture)
        {
            if ((_packageDescriptionsCacheTimestamp + _cacheLiveTime) < DateTime.Now)
            {
                LoggingService.LogVerbose("PackageServerFacadeCache", "PackageDescription cache miss");
                return null;
            }

            Dictionary<Guid, Dictionary<CultureInfo, List<PackageDescription>>> dic1;
            if (_packageDescriptionsCache.TryGetValue(packageServerUrl, out dic1) == false)
            {
                dic1 = new Dictionary<Guid, Dictionary<CultureInfo, List<PackageDescription>>>();
                _packageDescriptionsCache.Add(packageServerUrl, dic1);
            }

            Dictionary<CultureInfo, List<PackageDescription>> dic2;
            if (dic1.TryGetValue(installationId, out dic2) == false)
            {
                dic2 = new Dictionary<CultureInfo, List<PackageDescription>>();
                dic1.Add(installationId, dic2);
            }

            if (dic2.ContainsKey(userCulture) == false)
            {
                LoggingService.LogVerbose("PackageServerFacadeCache", "PackageDescription cache miss");
                return null;
            }

            LoggingService.LogVerbose("PackageServerFacadeCache", "PackageDescription returned from cache");
            return dic2[userCulture];
        }



        public void AddCachedPackageDescription(string packageServerUrl, Guid installationId, CultureInfo userCulture, List<PackageDescription> packageDescription)
        {
            LoggingService.LogVerbose("PackageServerFacadeCache", "PackageDescription added to cache");

            Dictionary<Guid, Dictionary<CultureInfo, List<PackageDescription>>> dic1;
            if (_packageDescriptionsCache.TryGetValue(packageServerUrl, out dic1) == false)
            {
                dic1 = new Dictionary<Guid, Dictionary<CultureInfo, List<PackageDescription>>>();
                _packageDescriptionsCache.Add(packageServerUrl, dic1);
            }

            Dictionary<CultureInfo, List<PackageDescription>> dic2;
            if (dic1.TryGetValue(installationId, out dic2) == false)
            {
                dic2 = new Dictionary<CultureInfo, List<PackageDescription>>();
                dic1.Add(installationId, dic2);
            }

            dic2[userCulture] = packageDescription;
            _packageDescriptionsCacheTimestamp = DateTime.Now;
        }



        public void Clear()
        {
            _packageDescriptionsCache = new Dictionary<string, Dictionary<Guid, Dictionary<CultureInfo, List<PackageDescription>>>>();
        }
	}
}
