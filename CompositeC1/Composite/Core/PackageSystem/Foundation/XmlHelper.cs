/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Xml.Linq;
using Composite.Core.IO;
using Composite.Core.IO.Zip;


namespace Composite.Core.PackageSystem.Foundation
{
	internal static class XmlHelper
	{
        public static PackageFragmentValidationResult LoadInstallXml(string zipFilename, out XElement installElement)
        {
            installElement = null;

            ZipFileSystem zipFileSystem = null;
            try
            {
                zipFileSystem = new ZipFileSystem(zipFilename);
            }
            catch (Exception ex)
            {
                return new PackageFragmentValidationResult(PackageFragmentValidationResultType.Fatal, ex);
            }

            string filename = string.Format("~/{0}", PackageSystemSettings.InstallFilename);
            if (zipFileSystem.ContainsFile(filename) == false)
            {
                return new PackageFragmentValidationResult(PackageFragmentValidationResultType.Fatal, string.Format("Installation file '{0}' is missing from the zip file", filename));
            }

            try
            {
                using (C1StreamReader streamReader = new C1StreamReader(zipFileSystem.GetFileStream(filename)))
                {
                    string fileContent = streamReader.ReadToEnd();
                    installElement = XElement.Parse(fileContent);
                }
            }
            catch (Exception ex)
            {
                return new PackageFragmentValidationResult(PackageFragmentValidationResultType.Fatal, ex);
            }

            return null;
        }
	}
}
