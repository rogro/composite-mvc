/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Composite.Core.PackageSystem.Foundation
{
	internal static class VersionStringHelper
	{
        public static bool ValidateVersion(string version, out string newVersion)
        {
            Regex shotVersionRegex = new Regex(@"[0-9]+");
            Match shotVersionMatch = shotVersionRegex.Match(version);
            if ((shotVersionMatch.Success) && (shotVersionMatch.Value == version))
            {
                newVersion = string.Format("{0}.0.0.0", version);
                return true;
            }

            Regex versionRegex = new Regex(@"[0-9]+\.[0-9]+\.*[0-9]*\.*[0-9]*");
            Match versionMatch = versionRegex.Match(version);
            if ((versionMatch.Success) && (versionMatch.Groups[0].Value.Length == version.Length))
            {
                newVersion = version;
                return true;
            }

            newVersion = null;
            return false;
        }
	}
}
