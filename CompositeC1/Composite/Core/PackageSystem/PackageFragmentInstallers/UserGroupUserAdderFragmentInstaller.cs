/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Composite.Core.Linq;
using Composite.Data;
using Composite.Data.Types;


namespace Composite.Core.PackageSystem.PackageFragmentInstallers
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class UserGroupUserAdderFragmentInstaller : BasePackageFragmentInstaller
    {
        private List<string> _names = new List<string>();


        /// <exclude />
        public override IEnumerable<PackageFragmentValidationResult> Validate()
        {
            XElement usergroupNamesElement = this.Configuration.Where(f => f.Name == "UsergroupNames").FirstOrDefault();
            if (usergroupNamesElement == null) yield break;

            foreach (XElement usergroupNameElement in usergroupNamesElement.Elements("UsergroupName"))
            {
                XAttribute nameAttribute = usergroupNameElement.Attribute("Name");
                if (nameAttribute == null) continue;

                _names.Add(nameAttribute.Value);
            }

            yield break;
        }



        /// <exclude />
        public override IEnumerable<XElement> Install()
        {
            foreach (string usergroupName in _names)
            {
                IUserGroup userGroup = DataFacade.GetData<IUserGroup>().Where(f => f.Name == usergroupName).SingleOrDefault();
                if (userGroup == null) continue;

                IEnumerable<IUser> users = DataFacade.GetData<IUser>().Evaluate();

                foreach (IUser user in users)
                {
                    IUserUserGroupRelation userUserGroupRelation = DataFacade.BuildNew<IUserUserGroupRelation>();
                    userUserGroupRelation.UserId = user.Id;
                    userUserGroupRelation.UserGroupId = userGroup.Id;
                    DataFacade.AddNew<IUserUserGroupRelation>(userUserGroupRelation);
                }
            }

            yield break;
        }
    }
}
