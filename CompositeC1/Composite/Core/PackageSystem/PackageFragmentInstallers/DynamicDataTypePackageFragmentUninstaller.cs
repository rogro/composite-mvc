/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Composite.Core.Extensions;
using Composite.Data.DynamicTypes;
using Composite.Data.GeneratedTypes;
using Composite.C1Console.Events;
using Composite.Core.Xml;
using Composite.Data;


namespace Composite.Core.PackageSystem.PackageFragmentInstallers
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class DynamicDataTypePackageFragmentUninstaller : BasePackageFragmentUninstaller
    {
        private List<DataTypeDescriptor> _dataTypeDescriptorsToDelete;
        private List<PackageFragmentValidationResult> _validationResult;

        /// <exclude />
        public override bool ValidateFirst { get { return true; } }

        /// <exclude />
        public override IEnumerable<PackageFragmentValidationResult> Validate()
        {
            _validationResult = new List<PackageFragmentValidationResult>();

            if (this.Configuration.Count(f => f.Name == "Types") > 1)
            {
                _validationResult.AddFatal(GetText("DynamicDataTypePackageFragmentUninstaller.OnlyOneElement"));
                return _validationResult;
            }

            XElement typesElement = this.Configuration.SingleOrDefault(f => f.Name == "Types");

            _dataTypeDescriptorsToDelete = new List<DataTypeDescriptor>();

            if (typesElement != null)
            {
                foreach (XElement typeElement in typesElement.Elements("Type").Reverse())
                {
                    XAttribute typeIdAttribute = typeElement.Attribute("typeId");
                    if (typeIdAttribute == null)
                    {
                        _validationResult.AddFatal(GetText("DynamicDataTypePackageFragmentUninstaller.MissingAttribute").FormatWith("typeId"), typeElement);
                        continue;
                    }

                    Guid typeId;
                    if (!typeIdAttribute.TryGetGuidValue(out typeId))
                    {
                        _validationResult.AddFatal(GetText("DynamicDataTypePackageFragmentUninstaller.WrongAttributeFormat"), typeIdAttribute);
                        continue;
                    }

                    var dataTypeDescriptor = DynamicTypeManager.GetDataTypeDescriptor(typeId);
                    if (dataTypeDescriptor == null)
                    {
                        continue;
                    }

                    _dataTypeDescriptorsToDelete.Add(dataTypeDescriptor);

                    Type interfaceType = dataTypeDescriptor.GetInterfaceType();
                    var foreignRefereeTypes = DataReferenceFacade.GetRefereeTypes(interfaceType).Where(f => !_dataTypeDescriptorsToDelete.Any(g => g.GetInterfaceType() == f));
                    foreach (Type foreignRefereeType in foreignRefereeTypes)
                    {
                        // TODO: localize
                        _validationResult.AddFatal(string.Format("Data type '{0}' has references to type '{1}' about to be uninstalled. References must be removed before the package can be uninstalled.", foreignRefereeType, dataTypeDescriptor.TypeManagerTypeName), 
                                                    typeIdAttribute);
                    }

                    UninstallerContext.AddPendingForDeletionDataType(interfaceType);
                }
            }

            if (_validationResult.Count > 0)
            {
                _dataTypeDescriptorsToDelete = null;
            }

            return _validationResult;
        }



        /// <exclude />
        public override void Uninstall()
        {
            Verify.IsNotNull(_dataTypeDescriptorsToDelete, "DynamicDataTypePackageFragmentUninstaller has not been validated");

            bool flushTheSystem = false;
            foreach (DataTypeDescriptor dataTypeDescriptor in _dataTypeDescriptorsToDelete)
            {
                Log.LogVerbose(this.GetType().Name, "Uninstalling the type '{0}'", dataTypeDescriptor);

                GeneratedTypesFacade.DeleteType(dataTypeDescriptor, false);
                flushTheSystem = true;
            }

            if (flushTheSystem)
            {
                GlobalEventSystemFacade.FlushTheSystem(true);
            }
        }

        private static string GetText(string stringId)
        {
            return GetResourceString(stringId);
        }
    }
}
