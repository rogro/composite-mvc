/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using Composite.Core.Extensions;
using Composite.Core.Localization;
using Composite.Core.Xml;
using Composite.Data;


namespace Composite.Core.PackageSystem.PackageFragmentInstallers
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class LocalePackageFragmentInstaller : BasePackageFragmentInstaller
    {
        List<Tuple<CultureInfo, string, bool>> _localesToInstall = null;


        /// <exclude />
        public override IEnumerable<PackageFragmentValidationResult> Validate()
        {
            List<PackageFragmentValidationResult> validationResult = new List<PackageFragmentValidationResult>();

            if (this.Configuration.Where(f => f.Name == "Locales").Count() > 1)
            {
                validationResult.AddFatal(GetText("VirtualElementProviderNodePackageFragmentInstaller.OnlyOneElement"));
                return validationResult;
            }

            XElement areasElement = this.Configuration.Where(f => f.Name == "Locales").SingleOrDefault();

            _localesToInstall = new List<Tuple<CultureInfo, string, bool>>();

            if (areasElement != null)
            {
                foreach (XElement localeElement in areasElement.Elements("Locale"))
                {
                    XAttribute nameAttribute = localeElement.Attribute("name");
                    XAttribute urlMappingNameAttribute = localeElement.Attribute("urlMappingName");
                    XAttribute defaultAttribute = localeElement.Attribute("default");

                    if (nameAttribute == null)
                    {
                        // Missing attribute
                        validationResult.AddFatal(GetText("FilePackageFragmentInstaller.MissingAttribute").FormatWith("order"), localeElement);
                        continue;
                    }

                    CultureInfo cultureInfo;
                    try
                    {
                        cultureInfo = CultureInfo.CreateSpecificCulture(nameAttribute.Value);
                    }
                    catch
                    {
                        // Name error
                        validationResult.AddFatal(GetText("FilePackageFragmentInstaller.MissingAttribute").FormatWith("order"), nameAttribute);
                        continue;
                    }

                    if (LocalizationFacade.IsLocaleInstalled(cultureInfo))
                    {
                        continue; // Skip it, it is installed
                    }

                    if (_localesToInstall.Where(f => f.Item1.Equals(cultureInfo)).Any())
                    {
                        // Already installed or going to be installed
                        validationResult.AddFatal(GetText("FilePackageFragmentInstaller.MissingAttribute").FormatWith("order"), nameAttribute);
                        continue;
                    }

                    string urlMappingName = cultureInfo.Name;
                    if (urlMappingNameAttribute != null)
                    {
                        urlMappingName = urlMappingNameAttribute.Value;
                    }

                    if ((LocalizationFacade.IsUrlMappingNameInUse(urlMappingName)) || (_localesToInstall.Where(f => f.Item2 == urlMappingName).Any()))
                    {
                        // Url mapping name alread used or going to be used
                        validationResult.AddFatal(GetText("FilePackageFragmentInstaller.MissingAttribute").FormatWith("order"), urlMappingNameAttribute);
                        continue;
                    }

                    bool isDefault = false;
                    if (defaultAttribute != null)
                    {
                        if (defaultAttribute.TryGetBoolValue(out isDefault) == false)
                        {
                            // Wrong attribute value
                            validationResult.AddFatal(GetText("FilePackageFragmentInstaller.MissingAttribute").FormatWith("order"), defaultAttribute);
                            continue;
                        }
                    }

                    if ((isDefault) && (_localesToInstall.Where(f => f.Item3).Any()))
                    {
                        // More than one is specified as default
                        validationResult.AddFatal(GetText("FilePackageFragmentInstaller.MissingAttribute").FormatWith("order"), defaultAttribute);
                        continue;
                    }

                    _localesToInstall.Add(new Tuple<CultureInfo, string, bool>(cultureInfo, urlMappingName, isDefault));

                    this.InstallerContext.AddPendingLocale(cultureInfo);
                }
            }


            if (validationResult.Count > 0)
            {
                _localesToInstall = null;
            }

            return validationResult;
        }



        /// <exclude />
        public override IEnumerable<XElement> Install()
        {
            if (_localesToInstall == null) throw new InvalidOperationException("LocalePackageFragmentInstaller has not been validated");

            XAttribute oldDefaultAttribute = null;
            if (DataLocalizationFacade.DefaultLocalizationCulture != null)
            {
                oldDefaultAttribute = new XAttribute("oldDefault", DataLocalizationFacade.DefaultLocalizationCulture.Name);
            }

            List<XElement> localeElements = new List<XElement>();

            foreach (Tuple<CultureInfo, string, bool> tuple in _localesToInstall)
            {
                Log.LogVerbose("LocalePackageFragmentInstaller", string.Format("Adding the locale '{0}'", tuple.Item1));

                LocalizationFacade.AddLocale(tuple.Item1, tuple.Item2, true, false);

                if (tuple.Item3)
                {
                    Log.LogVerbose("LocalePackageFragmentInstaller", string.Format("Setting new default locale to '{0}'", tuple.Item1));

                    LocalizationFacade.SetDefaultLocale(tuple.Item1);
                }

                localeElements.Add(new XElement("Locale", new XAttribute("name", tuple.Item1.Name)));
            }

            XElement element = new XElement("Locales", localeElements);
            
            if (oldDefaultAttribute != null)
            {
                element.Add(oldDefaultAttribute);
            }
            
            yield return element;
        }

        private static string GetText(string stringId)
        {
            return GetResourceString(stringId);
        }
    }
}
