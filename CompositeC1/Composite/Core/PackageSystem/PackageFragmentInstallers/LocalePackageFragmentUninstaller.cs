/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Globalization;
using Composite.Core.Localization;


namespace Composite.Core.PackageSystem.PackageFragmentInstallers
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class LocalePackageFragmentUninstaller : BasePackageFragmentUninstaller
    {
        private List<CultureInfo> _culturesToUninstall = null;
        private CultureInfo _oldDefaultCultureInfo = null;


        /// <exclude />
        public override IEnumerable<PackageFragmentValidationResult> Validate()
        {
            List<PackageFragmentValidationResult> validationResults = new List<PackageFragmentValidationResult>();

            if (this.Configuration.Count(f => f.Name == "Locales") > 1)
            {
                validationResults.AddFatal(GetText("VirtualElementProviderNodePackageFragmentUninstaller.OnlyOneElement"));
                return validationResults;
            }

            XElement localesElement = this.Configuration.SingleOrDefault(f => f.Name == "Locales");

            _culturesToUninstall = new List<CultureInfo>();

            if (localesElement != null)
            {
                XAttribute oldDefaultAttribute = localesElement.Attribute("oldDefault");
                if (oldDefaultAttribute != null)
                {
                    _oldDefaultCultureInfo = CultureInfo.CreateSpecificCulture(oldDefaultAttribute.Value);
                }

                foreach (XElement localeElement in localesElement.Elements("Locale").Reverse())
                {
                    CultureInfo locale = CultureInfo.CreateSpecificCulture(localeElement.Attribute("name").Value);

                    if ((_oldDefaultCultureInfo == null) && (LocalizationFacade.IsDefaultLocale(locale)))
                    {
                        // Locale is default -> not possible to unintall
                        validationResults.AddFatal(GetText("VirtualElementProviderNodePackageFragmentUninstaller.OnlyOneElement"));
                        continue;
                    }

                    if (LocalizationFacade.IsOnlyActiveLocaleForSomeUsers(locale))
                    {
                        // only active for the a user
                        validationResults.AddFatal(GetText("VirtualElementProviderNodePackageFragmentUninstaller.OnlyOneElement"));
                        continue;
                    }

                    if (LocalizationFacade.IsLocaleInstalled(locale))
                    {
                        _culturesToUninstall.Add(locale);
                    }
                }
            }


            if (validationResults.Count > 0)
            {
                _culturesToUninstall = null;
                _oldDefaultCultureInfo = null;
            }

            return validationResults;
        }



        /// <exclude />
        public override void Uninstall()
        {
            if (_oldDefaultCultureInfo != null)
            {
                Log.LogVerbose("LocalePackageFragmentUninstaller", string.Format("Restoring default locale to '{0}'", _oldDefaultCultureInfo));

                LocalizationFacade.SetDefaultLocale(_oldDefaultCultureInfo);
            }


            foreach (CultureInfo locale in _culturesToUninstall.Reverse<CultureInfo>())
            {
                Log.LogVerbose("LocalePackageFragmentUninstaller", string.Format("Removing the locale '{0}'", locale));

                LocalizationFacade.RemoveLocale(locale, false);
            }
        }

        private static string GetText(string stringId)
        {
            return GetResourceString(stringId);
        }
    }
}
