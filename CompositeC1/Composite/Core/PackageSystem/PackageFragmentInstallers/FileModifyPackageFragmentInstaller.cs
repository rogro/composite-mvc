/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Composite.Core.IO;
using Texts = Composite.Core.ResourceSystem.LocalizationFiles.Composite_Core_PackageSystem_PackageFragmentInstallers;

namespace Composite.Core.PackageSystem.PackageFragmentInstallers
{
    /// <summary>Package installer for appending content to text files.</summary>
    /// <exclude />
    /// <example>
    ///&lt;mi:Add installerType=&quot;Composite.Core.PackageSystem.PackageFragmentInstallers.FileModifyPackageFragmentInstaller, Composite&quot; 
	///          uninstallerType=&quot;Composite.Core.PackageSystem.PackageFragmentInstallers.FileModifyPackageFragmentUninstaller, Composite&quot;&gt;
	/// &lt;AppendText path=&quot;~/sdfdsfds.css&quot; whenNotExist=&quot;create&quot;&gt;  &lt;!-- whenNotExist=&quot;fail,create,ignore&quot; --&gt;
	///     Line 1
	///     Line 2 
	///     Line 3
	///     &lt;/AppendText&gt;
    ///&lt;/mi:Add&gt;
    /// </example>
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
	public class FileModifyPackageFragmentInstaller : BasePackageFragmentInstaller
	{
        private enum ActionOnMissingFile
        {
            Fail = 0,
            Create = 1,
            Ignore = 2
        }

        internal static readonly string AppendText_ElementName = "AppendText";

        internal static readonly string TargetXml_AttributeName = "path";
        internal static readonly string WhenNotExist_AttributeName = "whenNotExist";

        private List<ContentToAdd> _contentToAdd;

        /// <exclude/>
		public override IEnumerable<PackageFragmentValidationResult> Validate()
		{
			var validationResult = new List<PackageFragmentValidationResult>();

            _contentToAdd = new List<ContentToAdd>();

            foreach (var element in this.Configuration)
            {
                if (element.Name != AppendText_ElementName)
                {
                    validationResult.AddFatal(Texts.PackageFragmentInstaller_IncorrectElement(element.Name.LocalName, AppendText_ElementName), element);
                    continue;
                }

                var pathAttr = element.Attribute(TargetXml_AttributeName);
                if (pathAttr == null)
                {
                    validationResult.AddFatal(Texts.PackageFragmentInstaller_MissingAttribute(TargetXml_AttributeName), element);
                    continue;
                }

                string path = (string)pathAttr;

                var actionOnMissingFile = ActionOnMissingFile.Fail;

                var whenNotExistsAttr = element.Attribute(WhenNotExist_AttributeName);
                if (whenNotExistsAttr != null)
                {
                    actionOnMissingFile = (ActionOnMissingFile) Enum.Parse(typeof (ActionOnMissingFile), whenNotExistsAttr.Value, true);
                }

                string filePath = PathUtil.Resolve(path);
                if (!C1File.Exists(filePath))
                {
                    if (actionOnMissingFile == ActionOnMissingFile.Fail)
                    {
                        validationResult.AddFatal(Texts.FileModifyPackageFragmentInstaller_FileDoesNotExist(filePath), pathAttr);
                        continue;
                    }

                    if (actionOnMissingFile == ActionOnMissingFile.Ignore)
                    {
                        continue;
                    }
                }

                _contentToAdd.Add(new ContentToAdd
                {
                    Path = filePath,
                    Content = element.Value
                });
            }

           
            if (validationResult.Any())
            {
                _contentToAdd = null;
            }
            
			return validationResult;
		}

        /// <exclude/>
		public override IEnumerable<XElement> Install()
		{
            Verify.IsNotNull(_contentToAdd, "FileModifyPackageFragmentInstaller has not been validated");

            foreach (ContentToAdd content in _contentToAdd)
            {
                if (C1File.Exists(content.Path))
                {
                    using (C1StreamWriter sw = C1File.AppendText(content.Path))
                    {
                        sw.Write(content.Content);
                        sw.Close();
                    }
                }
                else
                {
                    C1File.WriteAllText(content.Path, content.Content);
                }
            }
			return new[] {  this.Configuration.FirstOrDefault() };
		}

        internal sealed class ContentToAdd
		{
            public string Path { get; set; }
			public string Content { get; set; }
		}
	}
}
