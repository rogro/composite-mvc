/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Composite.Core.Extensions;
using Composite.Core.Types;
using Composite.C1Console.Elements.Plugins.ElementProvider;
using Composite.Plugins.Elements.ElementProviders.VirtualElementProvider;
using Composite.C1Console.Events;
using Composite.Core.ResourceSystem;


namespace Composite.Core.PackageSystem.PackageFragmentInstallers
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class VirtualElementProviderNodePackageFragmentInstaller : BasePackageFragmentInstaller
    {
        private List<Area> _areasToInstall = null;


        /// <exclude />
        public override IEnumerable<PackageFragmentValidationResult> Validate()
        {
            List<PackageFragmentValidationResult> validationResult = new List<PackageFragmentValidationResult>();

            if (this.Configuration.Count(f => f.Name == "Areas") > 1)
            {
                validationResult.AddFatal(GetText("VirtualElementProviderNodePackageFragmentInstaller.OnlyOneElement"));
                return validationResult;
            }

            XElement areasElement = this.Configuration.SingleOrDefault(f => f.Name == "Areas");

            _areasToInstall = new List<Area>();

            if (areasElement != null)
            {
                foreach (XElement areaElement in areasElement.Elements("Area"))
                {
                    XAttribute orderAttribute = areaElement.Attribute("order");
                    XAttribute elementProviderTypeAttribute = areaElement.Attribute("elementProviderType");
                    XAttribute labelAttribute = areaElement.Attribute("label");

                    XAttribute closeFolderIconNameAttribute = areaElement.Attribute("closeFolderIconName");
                    XAttribute openFolderIconNameAttribute = areaElement.Attribute("openFolderIconName");

                    if ((orderAttribute == null) || (elementProviderTypeAttribute == null) || (labelAttribute == null))
                    {
                        if (orderAttribute == null) validationResult.AddFatal(GetText("FilePackageFragmentInstaller.MissingAttribute").FormatWith("order"), areaElement);
                        if (elementProviderTypeAttribute == null) validationResult.AddFatal(GetText("FilePackageFragmentInstaller.MissingAttribute").FormatWith("elementProviderType"), areaElement);
                        if (labelAttribute == null) validationResult.AddFatal(GetText("FilePackageFragmentInstaller.MissingAttribute").FormatWith("label"), areaElement);
                    }
                    else
                    {
                        Type elementProviderType = null;
                        try
                        {
                            elementProviderType = TypeManager.TryGetType(elementProviderTypeAttribute.Value);
                        }
                        catch (Exception)
                        {
                            validationResult.AddFatal(GetText("VirtualElementProviderNodePackageFragmentInstaller.MissingType").FormatWith(elementProviderTypeAttribute.Value), areaElement);
                            continue;
                        }


                        Area area = new Area();
                        area.Order = (int)orderAttribute;
                        area.ElementProviderTypeName = elementProviderTypeAttribute.Value;
                        area.ElementProviderType = elementProviderType;
                        area.Label = labelAttribute.Value;

                        if (closeFolderIconNameAttribute != null)
                        {
                            if ((closeFolderIconNameAttribute.Value != "") && (IconResourceSystemFacade.GetResourceHandle(closeFolderIconNameAttribute.Value) == null))
                            {
                                validationResult.AddFatal(GetText("VirtualElementProviderNodePackageFragmentInstaller.MissingIcon").FormatWith(closeFolderIconNameAttribute.Value), areaElement);
                                continue;
                            }
                            else
                            {
                                area.CloseFolderIconName = closeFolderIconNameAttribute.Value;
                            }
                        }

                        if (openFolderIconNameAttribute != null)
                        {
                            if ((openFolderIconNameAttribute.Value != "") && (IconResourceSystemFacade.GetResourceHandle(openFolderIconNameAttribute.Value) == null))
                            {
                                validationResult.AddFatal(GetText("VirtualElementProviderNodePackageFragmentInstaller.MissingIcon").FormatWith(openFolderIconNameAttribute.Value), areaElement);
                                continue;
                            }
                            else
                            {
                                area.OpenFolderIconName = openFolderIconNameAttribute.Value;
                            }
                        }

                        _areasToInstall.Add(area);
                    }
                }
            }

            if (validationResult.Count > 0)
            {
                _areasToInstall = null;
            }

            return validationResult;
        }



        /// <exclude />
        public override IEnumerable<XElement> Install()
        {
            if (_areasToInstall == null) throw new InvalidOperationException("VirtualElementProviderNodePackageFragmentInstaller has not been validated");

            List<XElement> areaElements = new List<XElement>();
            foreach (Area area in _areasToInstall)
            {
                string name = string.Format("{0}{1}", area.ElementProviderType.Name, Guid.NewGuid());

                Log.LogVerbose("VirtualElementProviderNodePackageFragmentInstaller", string.Format("Installing the element provider '{0}'", name));

                if (area.ElementProviderType == null)
                {
                    area.ElementProviderType = TypeManager.GetType(area.ElementProviderTypeName);
                }

                HooklessElementProviderData elementProviderData = new HooklessElementProviderData();
                elementProviderData.Type = area.ElementProviderType;
                elementProviderData.Name = name;

                ElementProviderConfigurationServices.SaveElementProviderConfiguration(elementProviderData);
                VirtualElementProviderConfigurationManipulator.AddNewArea(name, area.Order, area.Label, area.CloseFolderIconName, area.OpenFolderIconName);

                XElement areaElement = new XElement("Area", new XAttribute("elementProviderName", name));
                areaElements.Add(areaElement);
            }

            GlobalEventSystemFacade.FlushTheSystem(true);

            yield return new XElement("Areas", areaElements);
        }


        private static string GetText(string stringId)
        {
            return GetResourceString(stringId);
        }


        private sealed class Area
        {
            public int Order { get; set; }
            public string ElementProviderTypeName { get; set; }
            public Type ElementProviderType { get; set; }
            public string Label { get; set; }
            public string CloseFolderIconName { get; set; }
            public string OpenFolderIconName { get; set; }
        }
    }
}
