/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Xsl;
using Composite.Core.IO;
using Composite.Core.Xml;

using Installer = Composite.Core.PackageSystem.PackageFragmentInstallers.FileXslTransformationPackageFragmentInstaller;
using Texts = Composite.Core.ResourceSystem.LocalizationFiles.Composite_Core_PackageSystem_PackageFragmentInstallers;

namespace Composite.Core.PackageSystem.PackageFragmentInstallers
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
	public sealed class FileXslTransformationPackageFragmentUninstaller : BasePackageFragmentUninstaller
	{
        private List<XslTransformation> _xsls;

        /// <exclude />
		public override IEnumerable<PackageFragmentValidationResult> Validate()
		{
            _xsls = new List<XslTransformation>();
			var validationResult = new List<PackageFragmentValidationResult>();

            var filesElement = this.ConfigurationParent.GetSingleConfigurationElement("XslFiles", validationResult, false);
            if (filesElement == null)
            {
                return validationResult;
            }

            foreach (XElement fileElement in filesElement.Elements("XslFile"))
            {
                XAttribute pathXMLAttribute = fileElement.Attribute(Installer.TargetXmlAttributeName);
                XAttribute pathXSLAttribute = fileElement.Attribute(Installer.UninstallXslAttributeName);

                if (pathXMLAttribute == null)
                {
                    validationResult.AddFatal(Texts.PackageFragmentInstaller_MissingAttribute(Installer.TargetXmlAttributeName), fileElement);
                    continue;
                }

                if (pathXSLAttribute == null)
                {
                    //if there isn no uninstall xsl
                    continue;
                }

                string inputPathXMLAttributeValue = PathUtil.Resolve(pathXMLAttribute.Value);
                string inpuPathXSLAttributeValue = pathXSLAttribute.Value;

                _xsls.Add(new XslTransformation
                {
                    pathXml = inputPathXMLAttributeValue,
                    pathXsl = inpuPathXSLAttributeValue
                });
            }
            


            if (validationResult.Count > 0)
            {
                _xsls = null;
            }

			return validationResult;
		}


        /// <exclude />
		public override void Uninstall()
		{
            if (_xsls == null) throw new InvalidOperationException("FileXslTransformationPackageFragmentUninstaller has not been validated");

			Stream stream;
            foreach (XslTransformation xslfile in _xsls)
			{
                Log.LogVerbose("XsltPackageFragmentInstaller",
                    string.Format("Performing XSL-transformation. xml-file: '{0}'; xsl-file: '{1}'", xslfile.pathXml, xslfile.pathXsl));

			    string xmlFilePath = PathUtil.Resolve(xslfile.pathXml);

                using (stream = this.UninstallerContext.ZipFileSystem.GetFileStream(xslfile.pathXsl))
				{
					var xslt = new XslCompiledTransform();
					using (XmlReader xslReader = XmlReader.Create(stream))
					{
						xslt.Load(xslReader);
					}

					var resultDocument = new XDocument();
					using (XmlWriter writer = resultDocument.CreateWriter())
					{
                        xslt.Transform(xmlFilePath, writer);
					}

                    resultDocument.SaveToFile(xmlFilePath);

					Log.LogVerbose("XsltTransformationResult", resultDocument.ToString());
				}
			}

		}

        private sealed class XslTransformation
        {
            public string pathXml { get; set; }
            public string pathXsl { get; set; }
        }
	}
}
