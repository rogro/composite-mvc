/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Xml.Linq;

using Texts = Composite.Core.ResourceSystem.LocalizationFiles.Composite_Core_PackageSystem_PackageFragmentInstallers;

namespace Composite.Core.PackageSystem.PackageFragmentInstallers
{
    /// <exclude />
    public static class PackageFragmentValidationExtension
    {
        internal static void AddFatal(this IList<PackageFragmentValidationResult> validationResults, Exception exception)
        {
            validationResults.Add(new PackageFragmentValidationResult(PackageFragmentValidationResultType.Fatal, exception));
        }

        internal static void AddFatal(this IList<PackageFragmentValidationResult> validationResults, string message)
        {
            validationResults.Add(new PackageFragmentValidationResult(PackageFragmentValidationResultType.Fatal, message));
        }

        internal static void AddFatal(this IList<PackageFragmentValidationResult> validationResults, string message, XObject configurationObject)
        {
            validationResults.Add(new PackageFragmentValidationResult(PackageFragmentValidationResultType.Fatal, message, configurationObject));
        }

        /// <summary>
        /// Gets a single configuration element. Elements other than the specified one will cause validation errors.
        /// </summary>
        internal static XElement GetSingleConfigurationElement(this XElement configurationElement,  string elementName, IList<PackageFragmentValidationResult> validationResult, bool required)
        {
            XElement result = null;

            foreach (var element in configurationElement.Elements())
            {
                if (element.Name.LocalName != elementName)
                {
                    validationResult.AddFatal(Texts.PackageFragmentInstaller_IncorrectElement(element.Name.LocalName, elementName), element);
                    continue;
                }
                 
                if (result != null)
                {
                    validationResult.AddFatal(Texts.PackageFragmentInstaller_OnlyOneElementAllowed(elementName));
                    return null;
                }

                result = element;
            }

            if (required && result == null)
            {
                validationResult.AddFatal(Texts.PackageFragmentInstaller_MissingElement(elementName));
            }

            return result;
        }

        /// <summary>
        /// Gets a child configuration elements. Elements with names other than the specified one will cause validation errors.
        /// </summary>
        internal static IEnumerable<XElement> GetConfigurationElements(this XElement configurationElement, string elementName, IList<PackageFragmentValidationResult> validationResult)
        {
            var result = new List<XElement>();

            foreach (var element in configurationElement.Elements())
            {
                if (element.Name.LocalName != elementName)
                {
                    validationResult.AddFatal(Texts.PackageFragmentInstaller_IncorrectElement(element.Name.LocalName, elementName), element);
                    continue;
                }

                result.Add(element);
            }

            return result;
        }
    }
}
