/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Composite.Core.Configuration;
using Composite.Core.PackageSystem.WebServiceClient;


namespace Composite.Core.PackageSystem.PackageFragmentInstallers
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public class PackageLicenseFragmentInstaller : BasePackageFragmentInstaller
    {
        private string _publicKeyXml;
        private bool _licenseFileExists;


        /// <exclude />
        public override IEnumerable<PackageFragmentValidationResult> Validate()
        {
            var validationResult = new List<PackageFragmentValidationResult>();

            Guid packageId = this.InstallerContext.PackageInformation.Id;
            if(LicenseDefinitionManager.GetLicenseDefinition(packageId) != null)
            {
                _licenseFileExists = true;
                return validationResult;
            }

            XElement publicKeyElement = this.Configuration.SingleOrDefault(f => f.Name == "RSAKeyValue");
            if (publicKeyElement == null)
            {
                validationResult.AddFatal(GetResourceString("PackageLicenseFragmentInstaller.MissingPublicKeyElement"));
                return validationResult;
            }

            _publicKeyXml = publicKeyElement.ToString();

            string validated = LicenseServerFacade.ValidateTrialLicenseDefinitionRequest(InstallationInformationFacade.InstallationId, packageId, _publicKeyXml);

            if (validated != "OK")
            {
                validationResult.Add(new PackageFragmentValidationResult(PackageFragmentValidationResultType.Fatal, validated));
            }

            return validationResult;
        }



        /// <exclude />
        public override IEnumerable<XElement> Install()
        {
            if(_licenseFileExists)
            {
                return new XElement[0];
            }

            LicenseDefinitionDescriptor descriptor = LicenseServerFacade.GetTrialLicenseDefinition(InstallationInformationFacade.InstallationId, this.InstallerContext.PackageInformation.Id, _publicKeyXml);

            PackageLicenseDefinition definition = new PackageLicenseDefinition
            {
                ProductName = this.InstallerContext.PackageInformation.Name,
                InstallationId = descriptor.InstallationId,
                ProductId = descriptor.ProductId,                
                LicenseFileName = "",
                Permanent = descriptor.Permanent,                                
                Expires = descriptor.Expires,
                LicenseKey = descriptor.LicenseKey,
                PurchaseUrl = descriptor.PurchaseUrl
            };

            LicenseDefinitionManager.StoreLicenseDefinition(definition);

            return new XElement[] { };
        }
    }
}
