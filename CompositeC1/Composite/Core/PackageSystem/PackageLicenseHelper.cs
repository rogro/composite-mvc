/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Security.Cryptography;
using Composite.Core.Implementation;


namespace Composite.Core.PackageSystem
{
    /// <summary>
    /// This class contains methods for handling package licenses
    /// <example>
    /// Here is an example of how a pacakge could validat if there is a valid license installed for the pacakge.
    /// This code should be compiled into the pacakge itself to prevent spoofing.
    /// <code>                
    /// Guid productId = ...; // A package should have this compiled into its assembly.
    /// string publicKeyXml = ...; // A package should have this compiled into its assembly.
    /// 
    /// PackageLicenseDefinition licenseDefinition = PackageLicenseHelper.GetLicenseDefinition(productId);
    /// Guid installationId = licenseDefinition.InstallationId; 
    /// bool isPermanent = licenseDefinition.Permanent;
    /// DateTime expiresTime = licenseDefinition.Expires;
    /// 
    /// byte[] signedSignature = licenseDefinition.LicenseKeyBytes;
    /// 
    /// // Create the signature string
    /// string signatureString;
    /// if (isPermanent)
    /// {
    ///     signatureString = string.Format("{0}#{1}#{2}", installationId, productId, isPermanent);
    /// }
    /// else
    /// {
    ///     signatureString = string.Format("{0}#{1}#{2}#{3}", installationId, productId, isPermanent, new XAttribute("date", expiresTime).Value);
    /// }
    /// byte[] signature = PackageLicenseHelper.CreateSignatureBytes(signatureString);
    /// 
    /// // Create the provider to verify the signature string
    /// RSACryptoServiceProvider provider = new RSACryptoServiceProvider();
    /// provider.FromXmlString(publicKeyXml);
    /// 
    /// object hashAlgorithm = PackageLicenseHelper.CreateSignatureHashAlgorithm(publicKeyXml);
    /// 
    /// // isValidKey tells if the package license xml file has been tampered with
    /// bool isValidKey = provider.VerifyData(signature, hashAlgorithm, signedSignature);
    /// 
    /// // isExpried tells if a trail license is expired, false if its a permanent license
    /// bool isExpired = !isPermanent &lt; expiresTime &lt; DateTime.Now;
    /// 
    /// // isLicenseValid is a combination of isValidKey and isExpired and is only true if the package license xml file has not been tampered with and the license is not expired
    /// bool isLicenseValid = isValidKey &amp; !isExpired;
    /// </code>
    /// </example>
    /// </summary>
    public static class PackageLicenseHelper
    {
        /// <summary>
        /// This method returns a license defintion for the given pacakge id
        /// </summary>        
        /// <param name="productId">The package id to locate licende definition for.</param>
        /// <returns>The data for the license definition found. Null if no license is found.</returns>
        public static PackageLicenseDefinition GetLicenseDefinition(Guid productId)
        {
            return ImplementationFactory.CurrentFactory.StatelessPackageLicenseHelper.GetLicenseDefinition(productId);
        }



        /// <summary>
        /// Stores the given license defintion.
        /// </summary>
        /// <param name="licenseDefinition">The license definition to store</param>
        public static void StoreLicenseDefinition(PackageLicenseDefinition licenseDefinition)
        {
            ImplementationFactory.CurrentFactory.StatelessPackageLicenseHelper.StoreLicenseDefinition(licenseDefinition);
        }



        /// <summary>
        /// Removes a license definition given the pacakge id
        /// </summary>
        /// <param name="productId">Package id to which the license definition is to be removed</param>
        public static void RemoveLicenseDefintion(Guid productId)
        {
            ImplementationFactory.CurrentFactory.StatelessPackageLicenseHelper.RemoveLicenseDefinition(productId);
        }



        /// <summary>
        /// This method returns a hash algorithm that can be used when validateting a package license definition. 
        /// </summary>
        /// <param name="publicKeyXml">This is the public key to the private key used by the pacakge server to generate the license key</param>
        /// <returns>A hash algorithm object</returns>
        public static object CreateSignatureHashAlgorithm(string publicKeyXml)
        {
            return ImplementationFactory.CurrentFactory.StatelessPackageLicenseHelper.CreateSignatureHashAlgorithm(publicKeyXml);
        }



        /// <summary>
        /// This method returns a byte representation of the <paramref name="signatureString"/>.
        /// Here is an example of how to create an signature string:
        /// <example>
        /// <code>
        /// string signatureString;
        /// if (isPermanent)
        /// {
        ///     signatureString = string.Format("{0}#{1}#{2}", installationId, productId, isPermanent);
        /// }
        /// else
        /// {
        ///     signatureString = string.Format("{0}#{1}#{2}#{3}", installationId, productId, isPermanent, new XAttribute("date", expiresTime).Value);
        /// }
        /// </code>
        /// </example>
        /// </summary>
        /// <param name="signatureString">A signature string</param>
        /// <returns></returns>
        public static byte[] CreateSignatureBytes(string signatureString)
        {
            return ImplementationFactory.CurrentFactory.StatelessPackageLicenseHelper.CreateSignatureBytes(signatureString);
        }
    }
}
