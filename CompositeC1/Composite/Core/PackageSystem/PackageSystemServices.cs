/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using Composite.Data;
using Composite.Data.Types;
using Composite.Core.Configuration;
using Composite.Core.PackageSystem.Foundation;
using Composite.C1Console.Users;


namespace Composite.Core.PackageSystem
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class PackageSystemServices
    {
        /// <exclude />
        public static IEnumerable<PackageDescription> GetFilteredAllAvailablePackages()
        {
            List<InstalledPackageInformation> installedPackageInformation = PackageManager.GetInstalledPackages().ToList();

            IEnumerable<PackageDescription> descriptions =
                 from description in PackageServerFacade.GetAllPackageDescriptions(InstallationInformationFacade.InstallationId, UserSettings.CultureInfo)
                 where installedPackageInformation.All(f => f.Id != description.Id)
                 select description;

            Dictionary<Guid, List<PackageDescription>> packageDescriptions = new Dictionary<Guid,List<PackageDescription>>();
            foreach (PackageDescription packageDescription in descriptions)
            {
                if ((new Version(packageDescription.MinCompositeVersionSupported) <= RuntimeInformation.ProductVersion) &&
                    (new Version(packageDescription.MaxCompositeVersionSupported) >= RuntimeInformation.ProductVersion))
                {
                    List<PackageDescription> decs;
                    if (!packageDescriptions.TryGetValue(packageDescription.Id, out decs))
                    {
                        decs = new List<PackageDescription>();
                        packageDescriptions.Add(packageDescription.Id, decs);
                    }

                    decs.Add(packageDescription);
                }
            }

            foreach (var kvp in packageDescriptions)
            {
                if (kvp.Value.Count > 1)
                {
                    kvp.Value.Sort(delegate(PackageDescription x, PackageDescription y)
                    {
                        Version xVersion = new Version(x.PackageVersion);
                        Version yVersion = new Version(y.PackageVersion);
                        return xVersion.CompareTo(yVersion);
                    });
                }

                yield return kvp.Value.Last();
            }            
        }



        /// <exclude />
        public static string GetPackageSourceNameByPackageId(Guid id, Guid installationId, CultureInfo userCulture)
        {
            List<IPackageServerSource> packageServerSources = DataFacade.GetData<IPackageServerSource>().ToList();

            foreach (IPackageServerSource packageServerSource in packageServerSources)
            {
                PackageDescription foundPackageDescription = 
                    (from package in PackageServerFacade.GetPackageDescriptions(packageServerSource.Url, installationId, userCulture)
                     where package.Id == id
                     select package).FirstOrDefault();

                if (foundPackageDescription != null)
                {
                    return packageServerSource.Url;
                }
            }

            throw new InvalidOperationException("Source not found");
        }



        /// <exclude />
        public static string GetEulaText(PackageDescription packageDescription)
        {
            string packageSource = PackageSystemServices.GetPackageSourceNameByPackageId(packageDescription.Id, InstallationInformationFacade.InstallationId, UserSettings.CultureInfo);

            string text = PackageServerFacade.GetEulaText(packageSource, packageDescription.EulaId, UserSettings.CultureInfo);

            return text;
        }



        /// <exclude />
        public static PackageInformation GetPackageInformationFromZipfile(string zipFilename)
        {
            XElement installContent;
            PackageFragmentValidationResult packageFragmentValidationResult = XmlHelper.LoadInstallXml(zipFilename, out installContent);
            if (packageFragmentValidationResult != null) throw new InvalidOperationException(packageFragmentValidationResult.Message);

            PackageInformation packageInformation;
            packageFragmentValidationResult = PackageManager.ValidatePackageInformation(installContent, out packageInformation);
            if (packageFragmentValidationResult != null) throw new InvalidOperationException(packageFragmentValidationResult.Message);

            return packageInformation;
        }
    }
}
