/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.Core.Implementation;


namespace Composite.Core.PackageSystem
{
    /// <summary>
    /// A package license key definition
    /// </summary>
    public class PackageLicenseDefinition
    {
        /// <summary>
        /// The name of the package.
        /// </summary>
        public string ProductName { get; set; }


        /// <summary>
        /// The local path of the license file. A serialized version of this class.
        /// </summary>
        public string LicenseFileName { get; set; }


        /// <summary>
        /// False if the license is a trail license. True if its a permanent license.
        /// </summary>
        public bool Permanent { get; set; }


        /// <summary>
        /// The id of the C1 installation where the package was installed.
        /// </summary>
        public Guid InstallationId { get; set; }


        /// <summary>
        /// The id of the pacakge.
        /// </summary>
        public Guid ProductId { get; set; }


        /// <summary>
        /// A RSA signed license key. This is used to verify that the license file has not been tampered with.
        /// </summary>
        public string LicenseKey { get; set; }


        /// <summary>
        /// Url to where to buy a license for the pacakge.
        /// </summary>
        public string PurchaseUrl { get; set; }


        /// <summary>
        /// If its a trail license this property contains the date when the pacakge experies in UTC.
        /// </summary>
        public DateTime Expires { get; set; }


        /// <summary>
        /// The <see cref="LicenseKey"/> serizlied to byte array.
        /// </summary>
        public byte[] LicenseKeyBytes
        {
            get
            {
                return ImplementationFactory.CurrentFactory.StatelessPackageLicenseHelper.GetLicenseKeyBytes(this.LicenseKey);
            }
        }
    }
}
