/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml.Linq;
using Composite.Core.Xml;
using System.Reflection;


namespace Composite.Core.PackageSystem
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [DebuggerDisplay("ValidationResult = {ValidationResult}, Message = {Message}")]
	public sealed class PackageFragmentValidationResult
	{
        /// <exclude />
        public PackageFragmentValidationResult(PackageFragmentValidationResultType validationResult, Exception exception)
        {
            Verify.ArgumentNotNull(exception, "exception");

            if (exception is TargetInvocationException)
            {
                exception = exception.InnerException;
            }

            this.ValidationResult = validationResult;
            this.Message = exception.Message;
            this.Exception = exception;
        }



        /// <exclude />
        public PackageFragmentValidationResult(PackageFragmentValidationResultType validationResult, string message)
            : this(validationResult, message, null)
        {
        }



        /// <exclude />
        public PackageFragmentValidationResult(PackageFragmentValidationResultType validationResult, string message, XObject configurationObject)
        {
            Verify.ArgumentNotNullOrEmpty(message, "message");

            this.ValidationResult = validationResult;
            this.Message = message;

            if (configurationObject != null)
            {
                this.XPath = configurationObject.GetXPath();
            }

            this.Message = message;
        }


        /// <exclude />
        public PackageFragmentValidationResultType ValidationResult { get; private set; }

        /// <exclude />
        public string Message { get; private set; }

        /// <exclude />
        public Exception Exception { get; private set; }

        /// <exclude />
        public string XPath { get; private set; }

        /// <exclude />
        public IEnumerable<PackageFragmentValidationResult> InnerResult { get; set; }
	}
}
