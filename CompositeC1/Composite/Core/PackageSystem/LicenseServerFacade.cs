/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Composite.Core.PackageSystem.WebServiceClient;


namespace Composite.Core.PackageSystem
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    internal static class LicenseServerFacade
    {
        public static string ValidateTrialLicenseDefinitionRequest(Guid installationId, Guid productId, string publicKeyXml)
        {
            LicenseDefinitionServiceSoapClient client = CreateClient();

            return client.ValidateTrialLicenseDefinitionRequest(installationId, productId, publicKeyXml);
        }



        public static LicenseDefinitionDescriptor GetTrialLicenseDefinition(Guid installationId, Guid productId, string publicKeyXml)
        {
            LicenseDefinitionServiceSoapClient client = CreateClient();

            return client.GetTrialLicenseDefinition(installationId, productId, publicKeyXml);
        }
        

        
        private static string LicenseServerUrl
        {
            get
            {
                return "http://package.composite.net/PackageLicense/LicenseDefinitionService.asmx";
            }
        }



        private static LicenseDefinitionServiceSoapClient CreateClient()
        {
            BasicHttpBinding basicHttpBinding = new BasicHttpBinding();

            if (RuntimeInformation.IsDebugBuild)
            {
                basicHttpBinding.CloseTimeout = TimeSpan.FromSeconds(1);
                basicHttpBinding.OpenTimeout = TimeSpan.FromSeconds(1);
                basicHttpBinding.ReceiveTimeout = TimeSpan.FromSeconds(1);
                basicHttpBinding.SendTimeout = TimeSpan.FromSeconds(1);

                basicHttpBinding.CloseTimeout = TimeSpan.FromMinutes(2);
                basicHttpBinding.OpenTimeout = TimeSpan.FromMinutes(2);
                basicHttpBinding.ReceiveTimeout = TimeSpan.FromMinutes(2);
                basicHttpBinding.SendTimeout = TimeSpan.FromMinutes(2);
            }
            else
            {
                basicHttpBinding.CloseTimeout = TimeSpan.FromMinutes(1);
                basicHttpBinding.OpenTimeout = TimeSpan.FromMinutes(1);
                basicHttpBinding.ReceiveTimeout = TimeSpan.FromMinutes(1);
                basicHttpBinding.SendTimeout = TimeSpan.FromMinutes(1);
            }

            if (LicenseServerUrl.StartsWith("https"))
            {
                basicHttpBinding.Security.Mode = BasicHttpSecurityMode.Transport;
            }

            basicHttpBinding.MaxReceivedMessageSize = int.MaxValue;

            LicenseDefinitionServiceSoapClient client = new LicenseDefinitionServiceSoapClient(basicHttpBinding, new EndpointAddress(LicenseServerUrl));

            return client;
        }
    }
}
