/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using Composite.C1Console.Elements;
using Composite.Core.PageTemplates.Plugins.Runtime;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ObjectBuilder;

namespace Composite.Core.PageTemplates
{
    /// <summary>
    /// Defines page template provider
    /// </summary>
    [CustomFactory(typeof(PageTemplateProviderCustomFactory))]
    public interface IPageTemplateProvider
    {
        /// <summary>
        /// Gets the page template descriptors.
        /// </summary>
        /// <returns></returns>
        IEnumerable<PageTemplateDescriptor> GetPageTemplates();

        /// <summary>
        /// Factory that give Composite C1 a IPageLayouter capable of rendering a Composite C1 page with the specified layout ID.
        /// The factory will be called for each individual page rendering 
        /// </summary>
        /// <returns></returns>
        IPageRenderer BuildPageRenderer(Guid templateId);

        /// <summary>
        /// Adds element actions on "Page templates" element
        /// </summary>
        IEnumerable<ElementAction> GetRootActions();

        /// <summary>
        /// Forces provider to reload all the page templates next time they are requested.
        /// </summary>
        void FlushTemplates();

        /// <summary>
        /// Provider's label that is shown when adding a new page template
        /// </summary>
        string AddNewTemplateLabel { get;  }

        /// <summary>
        /// Workflow for adding a new page template
        /// </summary>
        Type AddNewTemplateWorkflow { get; }
    }
}
