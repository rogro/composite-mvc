/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

namespace Composite.Core.PageTemplates
{
    /// <summary>
    /// This class is responsible for rendering the provided job onto the provided asp.net web forms page. 
    /// The AttachToPage method is called at page construction  and is expected to hook on to asp.net page events (like PreInit) to drive the rendering. 
    /// </summary>
    public interface IPageRenderer
    {
        /// <summary>
        /// Attaches rendering code to an instace of <c cref="System.Web.UI.Page"></c>.
        /// </summary>
        /// <param name="renderTaget">The render taget.</param>
        /// <param name="contentToRender">The render job.</param>
        void AttachToPage(System.Web.UI.Page renderTaget, PageContentToRender contentToRender);
    }
}
