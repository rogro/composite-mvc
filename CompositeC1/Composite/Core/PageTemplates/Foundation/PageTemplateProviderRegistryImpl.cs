/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Composite.Core.Collections.Generic;
using Composite.Core.Configuration;
using Composite.Core.PageTemplates.Plugins.Runtime;
using Composite.Core.PageTemplates.Foundation.PluginFacade;

namespace Composite.Core.PageTemplates.Foundation
{
    internal class PageTemplateProviderRegistryImpl : IPageTemplateProviderRegistry
    {
        private readonly ResourceLocker<Resources> _resourceLocker = new ResourceLocker<Resources>(new Resources(), Resources.DoInitialize);


        public void Flush()
        {
            _resourceLocker.ResetInitialization();
        }


        public void FlushTemplates()
        {
            var resources = _resourceLocker.Resources;

            foreach (var providerName in resources.ProviderNames)
            {
                var provider = PageTemplateProviderPluginFacade.GetProvider(providerName);
                provider.FlushTemplates();
            }

            resources.ResetTemplatesCache();
        }

        public IEnumerable<string> ProviderNames
        {
            get
            {
                return _resourceLocker.Resources.ProviderNames;
            }
        }

        private static IEnumerable<string> GetProviderNames()
        {
            var settings = ConfigurationServices.ConfigurationSource.GetSection(PageTemplateProviderSettings.SectionName)
                               as PageTemplateProviderSettings;

            return settings.PageTemplateProviders.Select(provider => provider.Name).ToList();
        }


        public IEnumerable<PageTemplateDescriptor> PageTemplates
        {
            get 
            {
                return _resourceLocker.Resources.Templates.PageTemplates;
            }
        }

        public IPageTemplateProvider GetProviderByTemplateId(Guid pageTemplateId)
        {
            return _resourceLocker.Resources.Templates.ProviderByTemplate[pageTemplateId];
        }



        private sealed class Resources
        {
            public IEnumerable<string> ProviderNames { get; private set; }
            private volatile TemplatesCache _state;

            public TemplatesCache Templates
            {
                get { 
                    var state = _state;
                    if (state != null) return state;

                    lock(this)
                    {
                        state = _state ?? InitializePageTemplatesCache();

                        _state = state;
                    }

                    return state;
                }
            }

            public class TemplatesCache
            {
                public IEnumerable<PageTemplateDescriptor> PageTemplates { get; set; }
                public Hashtable<Guid, IPageTemplateProvider> ProviderByTemplate { get; set; }
            }

            public void ResetTemplatesCache()
            {
                _state = null;
            }

            private TemplatesCache InitializePageTemplatesCache()
            {
                var pageTemplates = new List<PageTemplateDescriptor>();
                var providerByTemplate = new Hashtable<Guid, IPageTemplateProvider>();

                foreach (string providerName in this.ProviderNames)
                {
                    var provider = PageTemplateProviderPluginFacade.GetProvider(providerName);
                    var templates = provider.GetPageTemplates().ToList();

                    pageTemplates.AddRange(templates);

                    foreach (var template in templates)
                    {
                        Verify.That(!providerByTemplate.ContainsKey(template.Id),
                                    "There are muliple layouts with the same ID: '{0}'", template.Id);

                        providerByTemplate.Add(template.Id, provider);
                    }
                }

                return new TemplatesCache { PageTemplates = pageTemplates, ProviderByTemplate = providerByTemplate };
            }

            public static void DoInitialize(Resources resources)
            {
                resources.ProviderNames = GetProviderNames();
            }
        }
    }
}
