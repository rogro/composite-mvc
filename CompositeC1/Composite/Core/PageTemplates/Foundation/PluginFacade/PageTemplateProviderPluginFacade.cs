/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Configuration;
using Composite.Core.Collections.Generic;
using Composite.Core.Extensions;
using Composite.Core.PageTemplates.Plugins.Runtime;
using Composite.C1Console.Events;

namespace Composite.Core.PageTemplates.Foundation.PluginFacade
{
    internal static class PageTemplateProviderPluginFacade
    {
        private static readonly ResourceLocker<Resources> _resourceLocker = new ResourceLocker<Resources>(new Resources(), Resources.Initialize);

        static PageTemplateProviderPluginFacade()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(args => Flush());
        }

        public static IPageTemplateProvider GetProvider(string providerName) {
            var resources  = _resourceLocker;

            IPageTemplateProvider result = resources.Resources.ProviderCache[providerName];

            if (result == null)
            {
                using (resources.Locker)
                {
                    result = resources.Resources.ProviderCache[providerName];
                    if (result == null)
                    {
                        result = resources.Resources.Factory.Create(providerName);
                        resources.Resources.ProviderCache.Add(providerName, result);
                    }
                }
            }

            return result;
        }


        internal static void Flush()
        {
            _resourceLocker.ResetInitialization();
        }


        private static void HandleConfigurationError(Exception ex)
        {
            Flush();

            const string sectionName = PageTemplateProviderSettings.SectionName;

            throw new ConfigurationErrorsException(
                "Failed to load the configuration section '{0}' from the configuration.".FormatWith(sectionName), 
                ex);
        }      


        private sealed class Resources
        {
            public PageTemplateProviderFactory Factory { get; set; }
            public Hashtable<string, IPageTemplateProvider> ProviderCache { get; set; }

            public static void Initialize(Resources resources)
            {
                try
                {
                    resources.Factory = new PageTemplateProviderFactory();
                }
                catch (NullReferenceException ex)
                {
                    HandleConfigurationError(ex);
                }
                catch (ConfigurationErrorsException ex)
                {
                    HandleConfigurationError(ex);
                }

                resources.ProviderCache = new Hashtable<string, IPageTemplateProvider>();
            }
        }
    }
}
