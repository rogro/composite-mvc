/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using Composite.C1Console.Elements;
using Composite.C1Console.Security;

namespace Composite.Core.PageTemplates
{
    /// <summary>
    /// Describes a page layout to the Composite C1 core so it may set up editing UI
    /// </summary>
    public class PageTemplateDescriptor
    {
        /// <summary>
        /// Used to identify page layouts. The value has to be unique for each page template.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        /// 
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the placeholder descriptions.
        /// </summary>
        /// <value>
        /// The placeholder descriptions.
        /// </value>
        public virtual IEnumerable<PlaceholderDescriptor> PlaceholderDescriptions { get; set; }

        /// <summary>
        /// The default is the placeholder to focus/use when users edit a page or a layout is used in ad hoc renderings.
        /// </summary>
        public virtual string DefaultPlaceholderId { get; set; }

        /// <summary>
        /// Gets or sets an exception that occured during loading the template.
        /// </summary>
        public virtual Exception LoadingException { get; set; }

        /// <summary>
        /// Gets a value indicating whether page template is loaded correctly.
        /// Not valid templates should not be used in data as they can not be used in rendering and their IDs may not be valid.
        /// </summary>
        /// <value>
        ///   <c>true</c> if template is loaded; otherwise, <c>false</c>.
        /// </value>
        public virtual bool IsValid
        {
            get { return LoadingException == null; }
        }

        /// <summary>
        /// Gets the entity token.
        /// </summary>
        /// <returns></returns>
        public virtual EntityToken GetEntityToken()
        {
            return new PageTemplateEntityToken(Id);
        }

        /// <summary>
        /// Appends actions to a visual element.
        /// </summary>
        public virtual IEnumerable<ElementAction> GetActions()
        {
            return new ElementAction[0];
        }
    }
}
