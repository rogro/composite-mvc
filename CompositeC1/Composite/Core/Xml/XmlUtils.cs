/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Xml.Linq;


namespace Composite.Core.Xml
{
    /// <summary>
    /// 
    /// </summary>
	public static class XmlUtils
	{
        /// <summary>
        /// Builds an XName - will use null namespace if namespaceName is null or empty.
        /// </summary>
        /// <param name="namespaceName">Namespace of element</param>
        /// <param name="localName">Local name of element</param>
        /// <returns>Corosponding XName</returns>
        public static XName GetXName(string namespaceName, string localName)
        {
            if (string.IsNullOrEmpty(namespaceName) == false)
            {
                return ((XNamespace)namespaceName) + localName;
            }
            else
            {
                return localName;
            }
        }

        /// <summary>
        /// Removes the XML declaration (like &lt;?xml version="1.0" encoding="utf-8"?&gt;) if present from the provided agrument. 
        /// </summary>
        /// <param name="xml">The string to remove XML declaration from</param>
        /// <returns>The XML document without any XML declaration</returns>
        public static string RemoveXmlDeclaration(string xml)
        {
            xml = xml.Trim();

            if (xml.StartsWith("<?xml") && xml.Contains("?>"))
            {
                xml = xml.Substring(xml.IndexOf("?>") + 2);	
            }

            return xml;
        }
	}
}
