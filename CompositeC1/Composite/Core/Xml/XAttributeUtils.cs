/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Xml.Linq;


namespace Composite.Core.Xml
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
	public static class XAttributeUtils
	{
        /// <exclude />
        public static string GetValueOrDefault(this XAttribute attribute, string defaultValue)
        {
            if (attribute == null) return defaultValue;

            return attribute.Value;
        }



        /// <exclude />
        public static bool TryGetIntValue(this XAttribute attribute, out int value)
        {
            if (attribute == null) throw new ArgumentNullException("attribute");

            value = default(int);
            try
            {
                value = (int)attribute;
            }
            catch(FormatException)
            {
                return false;
            }

            return true;
        }



        /// <exclude />
        public static bool TryGetBoolValue(this XAttribute attribute, out bool value)
        {
            if (attribute == null) throw new ArgumentNullException("attribute");

            value = default(bool);
            try
            {
                value = (bool)attribute;
            }
            catch (FormatException)
            {
                return false;
            }

            return true;
        }



        /// <exclude />
        public static bool TryGetGuidValue(this XAttribute attribute, out Guid value)
        {
            if (attribute == null) throw new ArgumentNullException("attribute");

            value = default(Guid);
            try
            {
                value = (Guid)attribute;
            }
            catch (FormatException)
            {
                return false;
            }

            return true;
        }
	}
}
