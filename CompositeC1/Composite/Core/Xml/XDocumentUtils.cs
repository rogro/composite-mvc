/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.IO;
using System.Xml.Linq;
using Composite.Core.IO;


namespace Composite.Core.Xml
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class XDocumentUtils
    {
        /// <summary>
        /// This should be a part of the I/O layer
        /// </summary>
        /// <param name="inputUri">This could be a file or a url</param>
        public static XDocument Load(string inputUri)
        {
            return Load(inputUri, LoadOptions.None);
        }


        /// <summary>
        /// This should be a part of the I/O layer
        /// </summary>
        /// <param name="loadOptions">Load options.</param>
        /// <param name="inputUri">This could be a file or a url</param>
        public static XDocument Load(string inputUri, LoadOptions loadOptions)
        {
            if (inputUri.Contains("://"))
            {
                using (Stream stream = UriResolver.GetStream(inputUri))
                {
                    return XDocument.Load(stream, loadOptions);
                }
            }

            return XDocument.Load(inputUri, loadOptions);
        }


        /// <summary>
        /// This should be a part of the I/O layer
        /// </summary>
        public static void Save(XDocument document, string filename)
        {
            using (C1FileStream stream = new C1FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                document.Save(stream);
            }
        }



        /// <summary>
        /// This should be a part of the I/O layer
        /// </summary>
        public static void SaveToFile(this XDocument document, string filename)
        {
            Save(document, filename);
        }



        /// <exclude />
        public static string GetDocumentAsString(this XDocument document)
        {
            Verify.ArgumentNotNull(document, "document");

            using (var ms = new MemoryStream())
            {
                using (var sw = new C1StreamWriter(ms))
                {
                    document.Save(sw);

                    ms.Seek(0, SeekOrigin.Begin);

                    using (var sr = new C1StreamReader(ms))
                    {
                        return sr.ReadToEnd();
                    }
                }
            }
        }
    }
}
