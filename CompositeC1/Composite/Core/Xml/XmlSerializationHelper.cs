/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Xml.Linq;
using Composite.Core.Types;

namespace Composite.Core.Xml
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    public static class XmlSerializationHelper
    {
        /// <summary>
        /// Converts the object into an xml serializable object.
        /// </summary>
        /// <exclude />
        public static object GetSerializableObject(object value)
        {
            if (value == null) return null;

            var type = value.GetType();
            if (type == typeof (string)
                || type == typeof (double)
                || type == typeof (float)
                || type == typeof (decimal)
                || type == typeof (DateTime)
                || type == typeof (DateTimeOffset)
                || type == typeof (TimeSpan))
            {
                return value;
            }

            return ValueTypeConverter.Convert<string>(value);
        }

        /// <exclude />
        public static object Deserialize(XAttribute attribute, Type type)
        {
            if (type == typeof(string)) return (string)attribute;
            if (type == typeof(double) || type == typeof(double?)) return (double)attribute;
            if (type == typeof(float) || type == typeof(float?)) return (float)attribute;
            if (type == typeof(decimal) || type == typeof(decimal?)) return (decimal)attribute;
            if (type == typeof(DateTime) || type == typeof(DateTime?)) return (DateTime)attribute;
            if (type == typeof(DateTimeOffset) || type == typeof(DateTimeOffset?)) return (DateTimeOffset)attribute;
            if (type == typeof(TimeSpan) || type == typeof(TimeSpan?)) return (TimeSpan)attribute;

            return ValueTypeConverter.Convert(attribute.Value, type);
        }
    }
}
