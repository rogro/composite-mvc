/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Composite.Core.IO;
using System.Collections.Generic;


namespace Composite.Core.Xml
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class XElementUtils
    {
        /// <summary>
        /// This should be a part of the I/O layer
        /// </summary>
        /// <param name="inputUri">This could be a file or a url</param>
        public static XElement Load(string inputUri)
        {
            XElement element;

            using (Stream stream = UriResolver.GetStream(inputUri))
            {
                element = XElement.Load(stream);
            }

            return element;
        }



        /// <summary>
        /// This should be a part of the I/O layer
        /// </summary>
        public static void SaveToPath(this XElement element, string fileName)
        {
            using (C1FileStream stream = new C1FileStream(fileName, FileMode.Create, FileAccess.Write))
            {                
                element.Save(stream);
            }
        }



        /// <exclude />
        public static bool HasSameSiblings(this XElement element)
        {
            if (element == null) throw new ArgumentNullException("element");

            if (element.Parent == null) return false;

            return element.Parent.Elements().Where(f => f.Name == element.Name).Count() > 1;
        }



        /// <exclude />
        public static int GetSameSiblingsBeforeCount(this XElement element)
        {
            if (element == null) throw new ArgumentNullException("element");

            return element.NodesBeforeSelf().Where(f => f is XElement && ((XElement)f).Name == element.Name).Count();
        }


        /// <summary>
        /// Returns the value of the XElement attribute with the specified name. 
        /// </summary>
        /// <param name="element"></param>
        /// <param name="attributeName">The name of the attribute to (try to) get</param>
        /// <returns>The value of the attribute or null if the attribute does not exist.</returns>
        public static string GetAttributeValue(this XElement element, string attributeName)
        {
            if (element == null) throw new ArgumentNullException("element");
            if (string.IsNullOrEmpty(attributeName)) throw new ArgumentNullException("attributeName");

            return GetAttributeValue(element, (XName)attributeName);
        }


        /// <summary>
        /// Returns the value of the XElement attribute with the specified name.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="attributeXName">XName of the attribute.</param>
        /// <returns>
        /// The value of the attribute or null if the attribute does not exist.
        /// </returns>
        public static string GetAttributeValue(this XElement element, XName attributeXName)
        {
            if (element == null) throw new ArgumentNullException("element");
            if (attributeXName == null) throw new ArgumentNullException("attributeXName");

            XAttribute valueAttribute = element.Attribute(attributeXName);

            if (valueAttribute == null)
            {
                return null;
            }
            else
            {
                return valueAttribute.Value;
            }
        }



        /// <summary>
        /// Merge in elements and attributes. New child elements and new attibutes are imported to the source. Conflicts are ignored (not merged).
        /// </summary>
        /// <param name="source">the structure to add new elements and attributes to</param>
        /// <param name="toBeImported">what to import</param>
        /// <returns>The modified source.</returns>
        public static XElement ImportSubtree(this XElement source, XElement toBeImported)
        {
            if (toBeImported != null)
            {
                foreach (XAttribute targetAttribute in from targetAttribute in toBeImported.Attributes() let sourceAttribute = source.Attribute(targetAttribute.Name) where sourceAttribute == null select targetAttribute)
                {
                    source.Add(targetAttribute);
                }

                foreach (XElement targetChild in toBeImported.Elements())
                {
                    XElement sourceChild = FindElement(source, targetChild);

                    if (sourceChild != null && !HasConflict(sourceChild, targetChild))
                    {
                        sourceChild.ImportSubtree(targetChild);
                    }
                    else
                    {
                        if (targetChild.Name.LocalName == "configSections")
                        {
                            source.AddFirst(targetChild);
                        }
                        else
                        {
                            source.Add(targetChild);
                        }
                    }
                }
            }

            return source;
        }


        /// <summary>
        /// Removes atributes and child elements from source which match ditto 100% in tatoBeExcludedget. Elements in source which has other child elements or attributes are not removed.
        /// </summary>
        /// <param name="source">XElement to modify</param>
        /// <param name="toBeExcluded">what to locate and remove</param>
        /// <returns>The modified source.</returns>
        public static XElement RemoveMatches(this XElement source, XElement toBeExcluded)
        {
            if (toBeExcluded != null)
            {
                foreach (XAttribute a in source.Attributes().Where(a => toBeExcluded.GetAttributeValue(a.Name) == a.Value).ToList())
                {
                    a.Remove();
                }

                foreach (XElement sourceChild in source.Elements().ToList())
                {
                    XElement targetChild = FindElement(toBeExcluded, sourceChild);
                    if (targetChild != null && !HasConflict(sourceChild, targetChild))
                    {
                        RemoveMatches(sourceChild, targetChild);

                        if (!sourceChild.HasAttributes && !sourceChild.HasElements)
                        {
                            sourceChild.Remove();
                            targetChild.Remove();
                        }
                    }
                }
            }

            return source;
        }



        private static bool HasConflict(XElement source, XElement target)
        {
            foreach (XAttribute targetAttribute in target.Attributes())
            {
                string sourceAttributeValue = source.GetAttributeValue(targetAttribute.Name);
                if (sourceAttributeValue != null && sourceAttributeValue != targetAttribute.Value)
                {
                    return true;
                }
            }

            return false;
        }



        private static int CountEquals(XElement target, XElement left, XElement right)
        {
            int leftEqualsCount = CountEquals(left, target, IsAttributeEqual);
            int rightEqualsCount = CountEquals(right, target, IsAttributeEqual);

            if (leftEqualsCount == rightEqualsCount)
            {
                int leftNameMatches = CountEquals(left, target, (a, b) => a.Name == b.Name);
                int rightNameMatches = CountEquals(right, target, (a, b) => a.Name == b.Name);

                return rightNameMatches.CompareTo(leftNameMatches);
            }

            return rightEqualsCount.CompareTo(leftEqualsCount);
        }



        private static int CountEquals(XElement left, XElement right, Func<XAttribute, XAttribute, bool> equal)
        {
            IEnumerable<XAttribute> equals = from l in left.Attributes()
                                             from r in right.Attributes()
                                             where equal(l, r)
                                             select l;
            return equals.Count();
        }



        private static bool IsAttributeEqual(XAttribute source, XAttribute target)
        {
            if (source == null && target == null)
            {
                return true;
            }

            if (source == null || target == null)
            {
                return false;
            }

            return source.Name == target.Name && source.Value == target.Value;
        }



        private static XElement FindElement(XElement source, XElement target)
        {
            List<XElement> sourceElements = source.Elements(target.Name).ToList();

            sourceElements.Sort((l, r) => CountEquals(target, l, r));

            return sourceElements.FirstOrDefault();
        }

    }
}
