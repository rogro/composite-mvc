/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml;


namespace Composite.Core.Xml
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class XNodeExtensionMethods
    {
        /// <summary>
        /// Returns an XPath to the specified XObject for documentation purposes. Namespaces
        /// are not handled by this function and using the returned XPath to look up the element
        /// is not guaranteed to work.
        /// </summary>
        /// <param name="xObject"></param>
        /// <returns></returns>
        public static string GetXPath(this XObject xObject)
        {
            StringBuilder sb = new StringBuilder();

            GetXPath(xObject, sb);

            return sb.ToString();
        }


        
        private static void GetXPath(this XObject xObject, StringBuilder currentPath)
        {
            if (xObject == null) throw new ArgumentNullException("xObject");

            XElement element = xObject as XElement;
            if (element != null)
            {
                if (element.Parent != null)
                {
                    if (element.HasSameSiblings() == false)
                    {
                        currentPath.Insert(0, element.Name.LocalName);
                        currentPath.Insert(0, "/");
                    }
                    else
                    {
                        int count = 1 + element.GetSameSiblingsBeforeCount();

                        currentPath.Insert(0, "]");
                        currentPath.Insert(0, count);
                        currentPath.Insert(0, "[");
                        currentPath.Insert(0, element.Name.LocalName);
                        currentPath.Insert(0, "/");
                    }

                    GetXPath(element.Parent, currentPath);
                    return;
                }
                else
                {
                    currentPath.Insert(0, element.Name.LocalName);
                    currentPath.Insert(0, "/");
                    return;
                }
            }

            XAttribute attribute = xObject as XAttribute;
            if (attribute != null)
            {
                currentPath.Insert(0, attribute.Name);
                currentPath.Insert(0, "@");

                GetXPath(attribute.Parent, currentPath);
                return;
            }

            throw new NotSupportedException("Only XElement and XAttribute supported");
        }


        /// <exclude />
        public static string[] InlineElements = new string[]
        {
            "a", 
            "abbr", 
            "acronym", 
            "b", 
            "basefont", 
            "bdo", 
            "big", 
            "br", 
            "cite", 
            "code", 
            "dfn", 
            "em", 
            "font", 
            "i", 
            "img", 
            "input", 
            "kbd", 
            "label", 
            "q", 
            "s", 
            "samp", 
            "select", 
            "small", 
            "span", 
            "strike", 
            "strong", 
            "sub", 
            "sup", 
            "textarea", 
            "tt", 
            "u", 
            "var"
        };

        private static readonly HashSet<string> InlineElementsLookup = new HashSet<string>(InlineElements);

        /// <exclude />
        public static bool IsBlockElement(this XNode node)
        {
            if (node == null) return false;

            XElement element = node as XElement;
            if (element == null) return false;

            if (element.Name.Namespace == Namespaces.Xhtml)
            {
                if (InlineElementsLookup.Contains(element.Name.LocalName.ToLowerInvariant())) 
                    return false;
            }

            return true;
        }



        /// <exclude />
        public static bool IsWhitespaceAware(this XNode node)
        {
            if (node == null) return false;

            XElement element = node as XElement;
            if (element == null) return false;

            string localName = element.Name.LocalName.ToLowerInvariant();
            return (localName == "pre") || (localName == "textarea");
        }      
    }
}
