/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Composite.Data;
using Composite.Core.ResourceSystem;


namespace Composite.Core.Localization
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class LocalizationParser
    {
        private static Regex _attributRegex = new Regex(@"\$\((?<type>[^:]+):(?<id>[^\)]+)\)");


        /// <exclude />
        public static void Parse(XContainer container)
        {
            IEnumerable<XElement> elements = container.Descendants().ToList();

            foreach (XElement element in elements)
            {
                if (element.Name.Namespace == LocalizationXmlConstants.XmlNamespace)
                {
                    if (element.Name.LocalName == "string")
                    {
                        HandleStringElement(element);
                    }
                    else if (element.Name.LocalName == "switch")
                    {
                        HandleSwitchElement(element);
                    }                   
                }

                IEnumerable<XAttribute> attributes = element.Attributes().ToList();
                foreach (XAttribute attribute in attributes)
                {
                    Match match = _attributRegex.Match(attribute.Value);
                    if ((match.Success) && (match.Groups["type"].Value == "lang")) 
                    {
                        string newValue = StringResourceSystemFacade.ParseString(string.Format("${{{0}}}", match.Groups["id"].Value));
                        attribute.SetValue(newValue);
                    }
                }
            }
        }



        private static void HandleStringElement(XElement element)
        {
            XAttribute attribute = element.Attribute("key");
            if (attribute == null) throw new InvalidOperationException(string.Format("Missing attibute named 'key' at {0}", element));

            string newValue = StringResourceSystemFacade.ParseString(string.Format("${{{0}}}", attribute.Value));

            element.ReplaceWith(newValue);
        }



        private static void HandleSwitchElement(XElement element)
        {
            XElement defaultElement = element.Element(((XNamespace)LocalizationXmlConstants.XmlNamespace) + "default");
            Verify.IsNotNull(defaultElement, "Missing element named 'default' at {0}", element);


            XElement newValueParent = defaultElement;

            CultureInfo currentCultureInfo = LocalizationScopeManager.CurrentLocalizationScope;
            foreach (XElement whenElement in element.Elements(((XNamespace)LocalizationXmlConstants.XmlNamespace) + "when"))
            {
                XAttribute cultureAttribute = whenElement.Attribute("culture");
                Verify.IsNotNull(cultureAttribute, "Missing attriubte named 'culture' at {0}", whenElement);

                CultureInfo cultureInfo = new CultureInfo(cultureAttribute.Value);
                if (cultureInfo.Equals(currentCultureInfo))
                {
                    newValueParent = whenElement;
                    break;
                }
            }

            element.ReplaceWith(newValueParent.Nodes());
        }
    }
}
