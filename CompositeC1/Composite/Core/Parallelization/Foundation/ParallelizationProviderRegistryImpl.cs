/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System.Linq;
using Composite.Core.Collections.Generic;
using Composite.Core.Configuration;
using Composite.Core.Parallelization.Plugins.Runtime;


namespace Composite.Core.Parallelization.Foundation
{
    internal sealed class ParallelizationProviderRegistryImpl : IParallelizationProviderRegistry
	{
        // private string _defaultParallelizationProviderName = null;
        private string[] _disabledParallelizationPoints = null;

        private bool? _enabled = null;


        //public string DefaultParallelizationProviderName
        //{
        //    get 
        //    {
        //        if (_defaultParallelizationProviderName == null)                   
        //        {
        //            ParallelizationProviderSettings parallelizationProviderSettings = ConfigurationServices.ConfigurationSource.GetSection(ParallelizationProviderSettings.SectionName) as ParallelizationProviderSettings;

        //            _defaultParallelizationProviderName = parallelizationProviderSettings.DefaultParallelizationProviderName;
        //        }

        //        return _defaultParallelizationProviderName;
        //    }
        //}

        public string[] DisabledParallelizationPoints
        {
            get
            {
                if (_disabledParallelizationPoints == null)
                {
                    var parallelizationProviderSettings = ConfigurationServices.ConfigurationSource.GetSection(ParallelizationProviderSettings.SectionName) as ParallelizationProviderSettings;
                    if (parallelizationProviderSettings != null 
                        && parallelizationProviderSettings.Parallelization != null)
                    {
                        var parConfigNode = parallelizationProviderSettings.Parallelization;

                        _disabledParallelizationPoints = (from ParallelizationSettingsElement configElement in parConfigNode
                                                          where !configElement.Enabled
                                                          select configElement.Name).ToArray();
                    }
                    else
                    {
                        _disabledParallelizationPoints = new string[0];
                    }
                }
                return _disabledParallelizationPoints;
            }
        }

        public bool Enabled
        {
            get
            {
                bool? enabled = _enabled;
                if (enabled == null)
                {
                    var parallelizationProviderSettings = ConfigurationServices.ConfigurationSource.GetSection(ParallelizationProviderSettings.SectionName) as ParallelizationProviderSettings;
                    if(parallelizationProviderSettings == null)
                    {
                        enabled = false;
                    }
                    else
                    {
                        enabled = parallelizationProviderSettings.Parallelization.Enabled;
                    }
                    _enabled = enabled;
                }
                return (bool)enabled;
            }
        }


        public void Flush()
        {
            _enabled = null;
            _disabledParallelizationPoints = null;
        }
    }
}
