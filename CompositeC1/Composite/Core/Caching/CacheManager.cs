/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;
using Composite.Core.Caching.Design;

namespace Composite.Core.Caching
{
    internal static class CacheManager
    {
        private static readonly Dictionary<string, ICache> _cacheCollection = new Dictionary<string, ICache>();

        /// <summary>
        /// Builds a cache instance from a configuration file.
        /// </summary>
        /// <typeparam name="TKey">Key type.</typeparam>
        /// <typeparam name="TValue">Value type.</typeparam>
        /// <param name="name">The name of the cache.</param>
        public static ICache<TKey, TValue> Get<TKey, TValue>(string name) where TValue : class
        {
            throw new NotImplementedException();
        }

        public static ICache<TKey, TValue> Get<TKey, TValue>(string name, CacheSettings cacheOptions) where TValue : class
        {
            name = name ?? string.Empty;
            bool isNamed = name != string.Empty;

            lock(_cacheCollection)
            {
                if(isNamed)
                {
                    if (_cacheCollection.ContainsKey(name))
                    {
                        return (ICache<TKey, TValue>)_cacheCollection[name];
                    }
                }

                ICache<TKey, TValue> result;
                switch (cacheOptions.CacheType)
                {
                    case CacheType.Lightweight:
                        result = new LightweightCache<TKey, TValue>(name, cacheOptions);
                        break;
                    case CacheType.Mixed:
                        Verify.That(typeof(TKey) == typeof(string), "In mixed cache the key can only bee of type 'System.String'");
                        result = new MixedCache<TValue>(name, cacheOptions) as ICache<TKey, TValue>;
                        break;
                    case CacheType.Undefined:
                        throw new InvalidOperationException("Cache type is undefined");
                    default:
                        throw new NotImplementedException();
                }

                Verify.That(result != null, "Failed to create a cache");

                _cacheCollection.Add(name != string.Empty ? name : "unnamed cache", result);

                return result;
            }
        }

        public static ICache[] GetAll()
        {
            lock(_cacheCollection)
            {
                ICollection<ICache> values = _cacheCollection.Values;

                ICache[] result = new ICache[values.Count];
                values.CopyTo(result, 0);
                return result;
            }
        }
    }
}
