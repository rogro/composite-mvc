/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;

namespace Composite.Core.Caching.Design
{
    internal class LightweightCache<TKey, TValue> : ICache<TKey, TValue> where TValue : class
    {
        protected readonly Data.Caching.Cache<TKey, TValue> _innerCache;

        public LightweightCache(string name, CacheSettings cacheOptions)
        {
            _innerCache = new Data.Caching.Cache<TKey, TValue>(name, cacheOptions.Size);
        }

        public TValue Get(TKey key)
        {
            return _innerCache.Get(key);
        }

        public bool TryGet(TKey key, out TValue value)
        {
            value = _innerCache.Get(key);
            return value != null;
        }

        public void Add(TKey key, TValue value)
        {
            _innerCache.Add(key, value);
        }

        public void Add(TKey key, TValue value, CachePriority cachePriority)
        {
            Add(key, value);
        }

        public void Remove(TKey key)
        {
            _innerCache.Remove(key);
        }

        public void Clear()
        {
            _innerCache.Clear();
        }

        public string Name
        {
            get { return _innerCache.Name; }
        }

        public CacheStatistic GetStatistic()
        {
            throw new NotImplementedException();
        }
    }
}
