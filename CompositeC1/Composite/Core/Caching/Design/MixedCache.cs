/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Web;
using System.Web.Caching;
using Composite.Data.Caching;

namespace Composite.Core.Caching.Design
{
    internal class MixedCache<TValue> : ICache<string, TValue> where TValue : class
    {
        private readonly WeakRefCache<string, TValue> _weakReferenceCache;
        private readonly CachePriority _defaultPriority;
        private readonly TimeSpan _slidingExpirationTime;
        private readonly bool _useAspNetCacheByDefault;

        private string _aspNetCachePrefix;

        public MixedCache(string name, CacheSettings settings)
        {
            _weakReferenceCache = new WeakRefCache<string, TValue>(name, settings.Size);
            _defaultPriority = settings.DefaultPriority;
            _slidingExpirationTime = settings.SlidingExpritationPeriod;

            _aspNetCachePrefix = "MixedCache" + Name;

            _useAspNetCacheByDefault = _defaultPriority != CachePriority.WeakReference
                                    && _defaultPriority != CachePriority.Undefined;
        }

        public TValue Get(string key)
        {
            TValue value = _weakReferenceCache.Get(key);

            // "Pinping" asp.net cache
            if(value != null)
            {
                var aspNetCachedValue = HttpRuntime.Cache.Get(GetAspNetKey(key)) as TValue;
                if(_useAspNetCacheByDefault && aspNetCachedValue == null)
                {
                    // Putting resurrected item back to ASP .NET cache as a "Low" priority
                    HttpRuntime.Cache.Add(key, value, null, DateTime.MaxValue, TimeSpan.Zero, CacheItemPriority.Low, null);
                }
            }

            return value;
        }

        public bool TryGet(string key, out TValue value)
        {
            value = Get(key);
            return value != null;
        }

        public void Add(string key, TValue value)
        {
            Add(key, value, _defaultPriority);
        }

        public void Add(string key, TValue value, CachePriority cachePriority)
        {
            _weakReferenceCache.Add(key, value);

            if(cachePriority == CachePriority.WeakReference 
                || cachePriority == CachePriority.Undefined)
            {
                return;
            }

            CacheItemPriority aspNetCachePriority;

            switch (cachePriority)
            {
                case CachePriority.Low:
                    aspNetCachePriority = CacheItemPriority.Low;
                    break;
                case CachePriority.High:
                    aspNetCachePriority = CacheItemPriority.High;
                    break;
                case CachePriority.NeverExpires:
                    aspNetCachePriority = CacheItemPriority.NotRemovable;
                    break;
                default:
                    aspNetCachePriority = CacheItemPriority.Default;
                    break;
            }


            string aspNetKey = GetAspNetKey(key);

            HttpRuntime.Cache.Add(aspNetKey,
                                  value,
                                  null,
                                  DateTime.MaxValue, 
                                  _slidingExpirationTime,
                                  aspNetCachePriority,
                                  null);
        }

        private string GetAspNetKey(string key)
        {
            return _aspNetCachePrefix + key;
        }

        public void Remove(string key)
        {
            _weakReferenceCache.Remove(key);
            HttpRuntime.Cache.Remove(GetAspNetKey(key));
        }

        public void Clear()
        {
            _weakReferenceCache.Clear();
        }

        public string Name
        {
            get { return _weakReferenceCache.Name; }
        }

        public CacheStatistic GetStatistic()
        {
            throw new NotImplementedException();
        }
    }
}
