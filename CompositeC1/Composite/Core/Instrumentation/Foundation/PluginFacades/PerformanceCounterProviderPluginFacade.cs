/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;
using System.Configuration;
using Composite.Core.Collections.Generic;
using Composite.C1Console.Events;
using Composite.Core.Instrumentation.Plugin;
using Composite.Core.Instrumentation.Plugin.Runtime;


namespace Composite.Core.Instrumentation.Foundation.PluginFacades
{
    internal static class PerformanceCounterProviderPluginFacade
    {
        private static ResourceLocker<Resources> _resourceLocker = new ResourceLocker<Resources>(new Resources(), Resources.Initialize);



        static PerformanceCounterProviderPluginFacade()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlushEvent);
        }



        public static void SystemStartupIncrement(string providerName)
        {
            Verify.ArgumentNotNullOrEmpty(providerName, "providerName");

                using (_resourceLocker.Locker)
                {
                    IPerformanceCounterProvider provider = GetPerformanceCounterProvider(providerName);

                    provider.SystemStartupIncrement();
                }
        }



        public static IPerformanceCounterToken BeginElementCreation(string providerName)
        {
            Verify.ArgumentNotNullOrEmpty(providerName, "providerName");

                using (_resourceLocker.Locker)
                {
                    IPerformanceCounterProvider provider = GetPerformanceCounterProvider(providerName);

                    return provider.BeginElementCreation();
                }
        }



        public static void EndElementCreation(string providerName, IPerformanceCounterToken performanceToken, int resultElementCount, int totalElementCount)
        {
            Verify.ArgumentNotNullOrEmpty(providerName, "providerName");

                using (_resourceLocker.Locker)
                {
                    IPerformanceCounterProvider provider = GetPerformanceCounterProvider(providerName);

                    provider.EndElementCreation(performanceToken, resultElementCount, totalElementCount);
                }
        }

        

        public static IPerformanceCounterToken BeginAspNetControlCompile(string providerName)
        {
            Verify.ArgumentNotNullOrEmpty(providerName, "providerName");

                using (_resourceLocker.Locker)
                {
                    IPerformanceCounterProvider provider = GetPerformanceCounterProvider(providerName);

                    return provider.BeginAspNetControlCompile();
                }
        }



        public static void EndAspNetControlCompile(string providerName, IPerformanceCounterToken performanceToken, int controlsCompiledCount)
        {
            Verify.ArgumentNotNullOrEmpty(providerName, "providerName");

                using (_resourceLocker.Locker)
                {
                    IPerformanceCounterProvider provider = GetPerformanceCounterProvider(providerName);

                    provider.EndAspNetControlCompile(performanceToken, controlsCompiledCount);
                }
        }



        public static IPerformanceCounterToken BeginPageHookCreation(string providerName)
        {
            Verify.ArgumentNotNullOrEmpty(providerName, "providerName");

                using (_resourceLocker.Locker)
                {
                    IPerformanceCounterProvider provider = GetPerformanceCounterProvider(providerName);

                    return provider.BeginPageHookCreation();
                }
        }



        public static void EndPageHookCreation(string providerName, IPerformanceCounterToken performanceToken, int controlsCompiledCount)
        {
            Verify.ArgumentNotNullOrEmpty(providerName, "providerName");

                using (_resourceLocker.Locker)
                {
                    IPerformanceCounterProvider provider = GetPerformanceCounterProvider(providerName);

                    provider.EndPageHookCreation(performanceToken, controlsCompiledCount);
                }
        }



        public static void EntityTokenParentCacheHitIncrement(string providerName)
        {
            Verify.ArgumentNotNullOrEmpty(providerName, "providerName");

                using (_resourceLocker.Locker)
                {
                    IPerformanceCounterProvider provider = GetPerformanceCounterProvider(providerName);

                    provider.EntityTokenParentCacheHitIncrement();
                }
        }



        public static void EntityTokenParentCacheMissIncrement(string providerName)
        {
            Verify.ArgumentNotNullOrEmpty(providerName, "providerName");

                using (_resourceLocker.Locker)
                {
                    IPerformanceCounterProvider provider = GetPerformanceCounterProvider(providerName);

                    provider.EntityTokenParentCacheMissIncrement();
                }
        }

        
        
        private static IPerformanceCounterProvider GetPerformanceCounterProvider(string providerName)
        {
            IPerformanceCounterProvider provider;

            if (_resourceLocker.Resources.ProviderCache.TryGetValue(providerName, out provider) == false)
            {
                try
                {
                    provider = _resourceLocker.Resources.Factory.Create(providerName);

                    using (_resourceLocker.Locker)
                    {
                        if (_resourceLocker.Resources.ProviderCache.ContainsKey(providerName) == false)
                        {
                            _resourceLocker.Resources.ProviderCache.Add(providerName, provider);
                        }
                    }
                }
                catch (ArgumentException ex)
                {
                    HandleConfigurationError(ex);
                }
                catch (ConfigurationErrorsException ex)
                {
                    HandleConfigurationError(ex);
                }
            }

            return provider;
        }



        private static void Flush()
        {
            _resourceLocker.ResetInitialization();
        }



        private static void OnFlushEvent(FlushEventArgs args)
        {
            Flush();
        }



        private static void HandleConfigurationError(Exception ex)
        {
            Flush();

            throw new ConfigurationErrorsException(string.Format("Failed to load the configuration section '{0}' from the configuration.", PerformanceCounterProviderSettings.SectionName), ex);
        }



        private sealed class Resources
        {
            public PerformanceCounterProviderFactory Factory { get; set; }
            public Dictionary<string, IPerformanceCounterProvider> ProviderCache { get; set; }

            public static void Initialize(Resources resources)
            {
                try
                {
                    resources.Factory = new PerformanceCounterProviderFactory();
                }
                catch (NullReferenceException ex)
                {
                    HandleConfigurationError(ex);
                }
                catch (ConfigurationErrorsException ex)
                {
                    HandleConfigurationError(ex);
                }

                resources.ProviderCache = new Dictionary<string, IPerformanceCounterProvider>();
            }
        }
    }
}
