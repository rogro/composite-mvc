/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using System.Xml.Linq;


namespace Composite.Core.Instrumentation
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class ProfilerReport
    {
        /// <exclude />
        public static XElement BuildReportXml(Measurement measurement)
        {
            int index = 0;

            var result = new XElement("Measurements", new XAttribute("MemoryUsageKb", measurement.MemoryUsage / 1024));

            foreach (var node in measurement.Nodes)
            {
                result.Add(BuildReportXmlRec(node, node.TotalTime, node.TotalTime, false, index.ToString()));
                index++;
            }

            foreach (var node in measurement.ParallelNodes)
            {
                result.Add(BuildReportXmlRec(node, node.TotalTime, node.TotalTime, true, index.ToString()));
                index++;
            }

            return result;
        }

        private static XElement BuildReportXmlRec(Measurement measurement, /* int level,*/ long totalTime, long parentTime, bool parallel, string id)
        {
            long persentTotal = (measurement.TotalTime * 100) / totalTime;

            long ownTime = measurement.TotalTime - measurement.Nodes.Select(childNode => childNode.TotalTime).Sum();

            var result = new XElement("Measurement",
                                      new XAttribute("_id", id),
                                      new XAttribute("title", measurement.Name),
                                      new XAttribute("totalTime", measurement.TotalTime),
                                      new XAttribute("ownTime", ownTime),
                                      new XAttribute("persentFromTotal", persentTotal),
                                      new XAttribute("parallel", parallel.ToString().ToLowerInvariant()));

            if (measurement.MemoryUsage != 0)
            {
                result.Add(new XAttribute("memoryUsageKb", measurement.MemoryUsage / 1024));
            }

            int index = 0;
            foreach (var childNode in measurement.Nodes.OrderByDescending(c => c.TotalTime))
            {
                result.Add(BuildReportXmlRec(childNode, totalTime, measurement.TotalTime, false, (id + "|" + index)));
                index++;
            }

            foreach (var childNode in measurement.ParallelNodes.OrderByDescending(c => c.TotalTime))
            {
                result.Add(BuildReportXmlRec(childNode, totalTime, measurement.TotalTime, true, (id + "|" + index)));
                index++;
            }  

            return result;
        }
    }
}
