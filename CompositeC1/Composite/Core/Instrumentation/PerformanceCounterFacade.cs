/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using Composite.C1Console.Events;


namespace Composite.Core.Instrumentation
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
	public static class PerformanceCounterFacade
	{
        private static IPerformanceCounterFacade _implementation = new PerformanceCounterFacadeImpl();


        internal static IPerformanceCounterFacade Implementation { get { return _implementation; } set { _implementation = value; } }
        


        static PerformanceCounterFacade()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlushEvent);
        }



        /// <exclude />
        public static void SystemStartupIncrement()
        {
            _implementation.SystemStartupIncrement();
        }



        /// <exclude />
        public static IPerformanceCounterToken BeginElementCreation()
        {
            return _implementation.BeginElementCreation();
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="performanceToken">The token returned by BeginElementCreation</param>
        /// <param name="resultElementCount">Element count after security filtering</param>
        /// <param name="totalElementCount">Element count before security filtering</param>
        public static void EndElementCreation(IPerformanceCounterToken performanceToken, int resultElementCount, int totalElementCount)
        {
            _implementation.EndElementCreation(performanceToken, resultElementCount, totalElementCount);
        }



        /// <exclude />
        public static IPerformanceCounterToken BeginAspNetControlCompile()
        {
            return _implementation.BeginAspNetControlCompile();
        }



        /// <exclude />
        public static void EndAspNetControlCompile(IPerformanceCounterToken performanceToken, int controlsCompiledCount)
        {
            _implementation.EndAspNetControlCompile(performanceToken, controlsCompiledCount);
        }



        /// <exclude />
        public static IPerformanceCounterToken BeginPageHookCreation()
        {
            return _implementation.BeginPageHookCreation();
        }



        /// <exclude />
        public static void EndPageHookCreation(IPerformanceCounterToken performanceToken, int controlsCompiledCount)
        {
            _implementation.EndPageHookCreation(performanceToken, controlsCompiledCount);
        }



        /// <exclude />
        public static void EntityTokenParentCacheHitIncrement()
        {
            _implementation.EntityTokenParentCacheHitIncrement();
        }



        /// <exclude />
        public static void EntityTokenParentCacheMissIncrement()
        {
            _implementation.EntityTokenParentCacheMissIncrement();
        }



        private static void OnFlushEvent(FlushEventArgs args)
        {
            _implementation.Flush();
        }
	}
}
