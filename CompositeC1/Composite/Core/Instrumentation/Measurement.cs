/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;


namespace Composite.Core.Instrumentation
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public class Measurement
    {
        private List<Measurement> _nodes;
        private List<Measurement> _parallelNodes;

        /// <exclude />
        public Measurement(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Total execution time in microseconds (10^-6)
        /// </summary>
        public long TotalTime;

        /// <exclude />
        public long MemoryUsage;

        /// <exclude />
        public string Name { get; set; }

        /// <exclude />
        public List<Measurement> Nodes 
        { 
            get
            {
                if(_nodes == null)
                {
                    _nodes = new List<Measurement>();
                }

                return _nodes;
            }
        }

        /// <exclude />
        public object SyncRoot { get { return this; } }

        /// <exclude />
        public List<Measurement> ParallelNodes
        {
            get
            {
                if (_parallelNodes == null)
                {
                    _parallelNodes = new List<Measurement>();
                }

                return _parallelNodes;
            }
        }
    }
}