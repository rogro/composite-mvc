/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;
using Composite.Core.Configuration.Plugins.GlobalSettingsProvider.Runtime;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ObjectBuilder;


namespace Composite.Core.Configuration.Plugins.GlobalSettingsProvider
{
    [CustomFactory(typeof(GlobalSettingsProviderCustomFactory))]
    [ConfigurationNameMapper(typeof(GlobalSettingsProviderDefaultNameRetriever))]
    internal interface IGlobalSettingsProvider
    {        
        string ApplicationName { get; } 

        string DefaultCultureName { get; }

        string ConfigurationDirectory { get; }

        string GeneratedAssembliesDirectory { get; }

        string SerializedWorkflowsDirectory { get; }

        string SerializedConsoleMessagesDirectory { get; }

        string AppCodeDirectory { get; }

        string BinDirectory { get; }

        string TempDirectory { get; }

        string CacheDirectory { get; }

        string PackageDirectory { get; }

        string AutoPackageInstallDirectory { get; }

        string TreeDefinitionsDirectory { get; }

        string PageTemplateFeaturesDirectory { get; }

        string DataMetaDataDirectory { get; }

        string InlineCSharpFunctionDirectory { get; }

        string PackageLicenseDirectory { get; }

        IResourceCacheSettings ResourceCacheSettings { get; }

        /// <summary>
        /// List of assembly names to exclude from type probing. Use "*" as wildcard, like. "System.*"
        /// </summary>
        IEnumerable<string> NonProbableAssemblyNames { get; }

        int ConsoleMessageQueueItemSecondToLive { get; }

        bool EnableDataTypesAutoUpdate { get; }

        bool BroadcastConsoleElementChanges { get; }

        /// <summary>
        /// If specified, the system will accept this user with any password on a clean system, that has a writeable login provider
        /// The user will then be created.
        /// </summary>
        string AutoCreatedAdministratorUserName { get; }

        string WorkflowTimeout { get; }

        string ConsoleTimeout { get; }

        /// <summary>
        /// When <value>true</value> only pages that are published or awaiting publication can be translated in console.
        /// </summary>
        bool OnlyTranslateWhenApproved { get;  }

        ICachingSettings Caching { get; }

        int ImageQuality { get; }
    }
}
