/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using Composite.Core.Collections.Generic;
using Composite.Core.Configuration;
using Composite.C1Console.Events;
using Composite.Core.Configuration.BuildinPlugins.GlobalSettingsProvider;
using Composite.Core.Configuration.Plugins.GlobalSettingsProvider;
using Composite.Core.Configuration.Plugins.GlobalSettingsProvider.Runtime;


namespace Composite.Core.Configuration.Foundation.PluginFacades
{
    internal sealed class GlobalSettingsProviderPluginFacade
    {
        private static readonly ResourceLocker<Resources> _resourceLocker = 
            new ResourceLocker<Resources>(new Resources(), Resources.Initialize, false);



        static GlobalSettingsProviderPluginFacade()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlush);
        }



        public static string ApplicationName
        {
            get
            {
                return UseReaderLock(provider => provider.ApplicationName);
            }
        }



        public static string AutoCreatedAdministratorUserName
        {
            get
            {
                return UseReaderLock(provider => provider.AutoCreatedAdministratorUserName);
            }
        }



        public static string DefaultCultureName
        {
            get
            {
                return UseReaderLock(provider => provider.DefaultCultureName);
            }
        }



        public static string ConfigurationDirectory
        {
            get
            {
                return UseReaderLock(provider => provider.ConfigurationDirectory);
            }
        }


        public static string GeneratedAssembliesDirectory
        {
            get
            {
                return UseReaderLock(provider => provider.GeneratedAssembliesDirectory);
            }
        }



        public static string SerializedWorkflowsDirectory
        {
            get
            {
                return UseReaderLock(provider => provider.SerializedWorkflowsDirectory);
            }
        }



        public static string SerializedConsoleMessagesDirectory
        {
            get
            {
                return UseReaderLock(provider => provider.SerializedConsoleMessagesDirectory);
            }
        }


        public static string AppCodeDirectory
        {
            get
            {
                return UseReaderLock(provider => provider.AppCodeDirectory);
            }
        }


        public static string BinDirectory
        {
            get
            {
                return UseReaderLock(provider => provider.BinDirectory);
            }
        }



        public static string TempDirectory
        {
            get
            {
                return UseReaderLock(provider => provider.TempDirectory);
            }
        }
        

        public static string CacheDirectory
        {
            get
            {
                return UseReaderLock(provider => provider.CacheDirectory);
            }
        }


        public static string PackageDirectory
        {
            get
            {
                return UseReaderLock(provider => provider.PackageDirectory);
            }
        }



        public static string AutoPackageInstallDirectory
        {
            get
            {
                return UseReaderLock(provider => provider.AutoPackageInstallDirectory);
            }
        }



        public static string TreeDefinitionsDirectory
        {
            get
            {
                return UseReaderLock(provider => provider.TreeDefinitionsDirectory);
            }
        }



        public static string PageTemplateFeaturesDirectory
        {
            get
            {
                return UseReaderLock(provider => provider.PageTemplateFeaturesDirectory);
            }
        }



        public static string DataMetaDataDirectory
        {
            get
            {
                return UseReaderLock(provider => provider.DataMetaDataDirectory);
            }
        }



        public static string InlineCSharpFunctionDirectory
        {
            get
            {
                return UseReaderLock(provider => provider.InlineCSharpFunctionDirectory);
            }
        }



        public static string PackageLicenseDirectory
        {
            get
            {
                return UseReaderLock(provider => provider.PackageLicenseDirectory);
            }
        }
        


        public static IResourceCacheSettings ResourceCacheSettings
        {
            get
            {
                return UseReaderLock(provider => provider.ResourceCacheSettings);
            }
        }



        public static IEnumerable<string> NonProbableAssemblyNames
        {
            get
            {
                return UseReaderLock(provider => provider.NonProbableAssemblyNames);
            }
        }



        public static int ConsoleMessageQueueItemSecondToLive
        {
            get
            {
                return UseReaderLock(provider => provider.ConsoleMessageQueueItemSecondToLive);
            }
        }



        public static bool EnableDataTypesAutoUpdate
        {
            get
            {
                return UseReaderLock(provider => provider.EnableDataTypesAutoUpdate);
            }
        }


        public static bool BroadcastConsoleElementChanges
        {
            get
            {
                return UseReaderLock(provider => provider.BroadcastConsoleElementChanges);
            }
        }


        public static bool OnlyTranslateWhenApproved
        {
            get
            {
                return UseReaderLock(provider => provider.OnlyTranslateWhenApproved);
            }
        } 
        


        public static string WorkflowTimeout
        {
            get
            {
                return UseReaderLock(provider => provider.WorkflowTimeout);
            }
        }



        public static string ConsoleTimeout
        {
            get
            {
                return UseReaderLock(provider => provider.ConsoleTimeout);
            }
        }

        public static ICachingSettings Caching
        {
            get
            {
                return UseReaderLock(provider => provider.Caching);
            }
        }


        public static int ImageQuality
        {
            get
            {
                return UseReaderLock(provider => provider.ImageQuality);
            }
        }


        private static void Flush()
        {
            _resourceLocker.ResetInitialization();
        }



        private static void OnFlush(FlushEventArgs args)
        {
            Flush();
        }



        private static void HandleConfigurationError(Exception ex)
        {
            Flush();

            throw new ConfigurationErrorsException(string.Format("Failed to load the configuration section '{0}' from the configuration.", GlobalSettingsProviderSettings.SectionName), ex);
        }

        private delegate T ExecuteDelegate<T>(IGlobalSettingsProvider provider);
        private static T UseReaderLock<T>(ExecuteDelegate<T> function)
        {
            using (_resourceLocker.ReadLocker)
            {
                return function(_resourceLocker.Resources.Provider);
            }
        }


        private sealed class Resources
        {
            public GlobalSettingsProviderFactory Factory { get; set; }
            public IGlobalSettingsProvider Provider { get; set; }

            public static void Initialize(Resources resources)
            {
                if ((RuntimeInformation.IsDebugBuild) &&
                            ((ConfigurationServices.ConfigurationSource == null) ||
                             (ConfigurationServices.ConfigurationSource.GetSection(GlobalSettingsProviderSettings.SectionName) == null)))
                {
                    resources.Provider = new BuildinGlobalSettingsProvider();
                }
                else
                {
                    try
                    {
                        resources.Factory = new GlobalSettingsProviderFactory();
                        resources.Provider = resources.Factory.CreateDefault();
                    }
                    catch (ArgumentException ex)
                    {
                        HandleConfigurationError(ex);
                    }
                    catch (ConfigurationErrorsException ex)
                    {
                        HandleConfigurationError(ex);
                    }
                }
            }
        }
    }
}
