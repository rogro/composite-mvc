/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.ComponentModel;
using System.Configuration;

using Microsoft.Practices.EnterpriseLibrary.Common.Properties;

using Composite.Core.Types;

namespace Composite.Core.Configuration
{
	/// <summary>
	/// Represents a configuration converter that converts a string to <see cref="Type"/> using the Composite Type Manager.
	/// </summary>
	internal class TypeManagerTypeNameConverter : ConfigurationConverterBase
	{
		/// <summary>
		/// Returns the name for the passed in Type.
		/// </summary>
		/// <param name="context">The container representing this System.ComponentModel.TypeDescriptor.</param>
		/// <param name="culture">Culture info for assembly</param>
		/// <param name="value">Value to convert.</param>
		/// <param name="destinationType">Type to convert to.</param>
		/// <returns>Assembly Qualified Name as a string</returns>
		public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
		{
			Type typeValue = value as Type;
			if (typeValue == null)
			{
				throw new ArgumentException("Can not convert type");
			}
			
			if (typeValue != null) return (typeValue).AssemblyQualifiedName;
			return null;
		}

		/// <summary>
		/// Returns a type based on the name passed in as data.
		/// </summary>
		/// <param name="context">The container representing this System.ComponentModel.TypeDescriptor.</param>
		/// <param name="culture">Culture info for assembly.</param>
		/// <param name="value">Data to convert.</param>
		/// <returns>Type of the data</returns>
		public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
		{
			string stringValue = (string)value;
			Type result = TypeManager.GetType(stringValue);
			if (result == null)
			{
				throw new ArgumentException(string.Format("Type \"{0}\" not found.", stringValue));
			}

			return result;
		}		
	}
}
