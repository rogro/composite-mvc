/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Threading;
using Composite.Core.Configuration.Plugins.GlobalSettingsProvider;


namespace Composite.Core.Configuration.BuildinPlugins.GlobalSettingsProvider
{
    internal sealed class BuildinGlobalSettingsProvider : IGlobalSettingsProvider
    {
        private string _applicationName = "Freja";
        private string _configurationDirectory = "~";
        private string _generatedAssembliesDirectory = "~";
        private string _serializedWorkflowsDirectory = "~";
        private string _appCodeDirectory = "App_Code";
        private string _binDirectory = "~";
        private string _tempDirectory = "~/Temp";
        private string _cacheDirectory = "~/Cache/Startup";
        private string _packageDirectory = "~/Packages";
        private string _autoPackageInstallDirectory = "~/AutoInstallPackages";
        private string _treeDefinitionsDirectory = "~/TreeDefinitions";
        private string _pageTemplateFeaturesDirectory = "~/PageTemplateFeaturesDirectory";
        private string _dataMetaDataDirectory = "~/DataMetaData";
        private string _inlineCSharpFunctionDirectory = "~/InlineCSharpFunctions";
        private string _packageLicenseDirectory = "~/PackageLicenses";
        private readonly IResourceCacheSettings _resourceCacheSettings = new BuildinResourceCacheSettings();
        private readonly ICachingSettings _cachingSettings = new BuildinCachingSettings();
        private readonly List<string> _nonProbableAssemblyNames = new List<string>();
        private readonly int _consoleMessageQueueSecondToLive = TimeSpan.FromMinutes(10).Seconds;
        private bool _enableDataTypesAutoUpdate = false;
        private bool _broadcastConsoleElementChanges = true;

        public string ApplicationName
        {
            get { return _applicationName; }
        }



        public string DefaultCultureName
        {
            get { return Thread.CurrentThread.CurrentCulture.Name; }
        }




        public string ConfigurationDirectory
        {
            get
            {
                return string.Format("{0}/{1}", _configurationDirectory, Guid.NewGuid());
            }
        }



        public string GeneratedAssembliesDirectory
        {
            get
            {
                return string.Format("{0}/{1}", _generatedAssembliesDirectory, Guid.NewGuid());
            }
        }



        public string SerializedWorkflowsDirectory
        {
            get
            {
                return string.Format("{0}/{1}", _serializedWorkflowsDirectory, Guid.NewGuid());

            }
        }


        public string AppCodeDirectory
        {
            get
            {
                return _appCodeDirectory;
            }
        }


        public string BinDirectory
        {
            get
            {
                return _binDirectory;
            }
        }



        public string TempDirectory
        {
            get
            {
                return _tempDirectory;
            }
        }



        public string CacheDirectory
        {
            get { return _cacheDirectory; }
        }



        public string PackageDirectory
        {
            get
            {
                return _packageDirectory;
            }
        }



        public string AutoPackageInstallDirectory
        {
            get
            {
                return _autoPackageInstallDirectory;
            }
        }



        public string TreeDefinitionsDirectory
        {
            get
            {
                return _treeDefinitionsDirectory;
            }
        }



        public string PageTemplateFeaturesDirectory
        {
            get
            {
                return _pageTemplateFeaturesDirectory;
            }
        }



        public string DataMetaDataDirectory
        {
            get
            {
                return _dataMetaDataDirectory;
            }
        }


        public string InlineCSharpFunctionDirectory
        {
            get 
            {
                return _inlineCSharpFunctionDirectory;
            }
        }


        public string PackageLicenseDirectory
        {
            get 
            {
                return _packageLicenseDirectory;
            }
        }        


        public IResourceCacheSettings ResourceCacheSettings
        {
            get { return _resourceCacheSettings; }
        }



        public IEnumerable<string> NonProbableAssemblyNames
        {
            get { return _nonProbableAssemblyNames; }
        }



        public int ConsoleMessageQueueItemSecondToLive
        {
            get { return _consoleMessageQueueSecondToLive; }
        }


        public bool EnableDataTypesAutoUpdate
        {
            get { return _enableDataTypesAutoUpdate; }
        }



        public bool BroadcastConsoleElementChanges
        {
            get { return _broadcastConsoleElementChanges; }
        }


        public string AutoCreatedAdministratorUserName
        {
            get { return null; }
        }


        public string SerializedConsoleMessagesDirectory
        {
            get
            {
                return string.Format("{0}/{1}", _serializedWorkflowsDirectory, Guid.NewGuid());
            }
        }


        public string WorkflowTimeout
        {
            get { return "7.00:00:00"; }
        }


        public string ConsoleTimeout
        {
            get { return "00:01:00"; }
        }

        public bool OnlyTranslateWhenApproved { 
            get { return false; } }

        public ICachingSettings Caching
        {
            get { return _cachingSettings; }
        }

        public int ImageQuality
        {
            get
            {
                return 80;
            }
        }
    }
}
