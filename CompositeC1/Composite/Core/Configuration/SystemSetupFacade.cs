/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.IO;
using System.Xml.Linq;
using Composite.Core.IO;
using Composite.Core.Xml;


namespace Composite.Core.Configuration
{
    /// <summary>
    /// This class may not depended on the system being initialized. 
    /// Any call to the class will never result in the system being initialized
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    public static class SystemSetupFacade
    {
        private static readonly object _lock = new object();
        private static bool? _isSystemFirstTimeInitialized;

        /// <exclude />
        static SystemSetupFacade()
        {
        }


        /// <exclude />
        public static bool IsSystemFirstTimeInitialized
        {
            get
            {
                if (_isSystemFirstTimeInitialized == null)
                {
                    try
                    {
                        _isSystemFirstTimeInitialized = C1File.Exists(SystemInitializedFilePath);
                    }
                    catch (Exception)
                    {
                        _isSystemFirstTimeInitialized = false;
                    }
                }

                return _isSystemFirstTimeInitialized.Value;
            }
            set
            {
                Verify.IsTrue(value, "Only 'true' value is expected");

                lock (_lock)
                {
                    if (!C1File.Exists(SystemInitializedFilePath))
                    {
                        string directory = Path.GetDirectoryName(SystemInitializedFilePath);
                        C1Directory.CreateDirectory(directory);

                        var doc = new XDocument(
                            new XElement("Root", new XAttribute("Status", true))
                        );

                        doc.SaveToFile(SystemInitializedFilePath);
                    }

                    _isSystemFirstTimeInitialized = true;
                }
            }
        }

        /// <exclude />
        public static bool SetupIsRunning { get; set; }

        private static string SystemInitializedFilePath
        {
            get
            {
                return PathUtil.Resolve("~/App_Data/Composite/Configuration/SystemInitialized.xml");
            }
        }



        /// <summary>
        /// This method is used to record the very first time the system is started.
        /// Ths time can later be used to several things like finding files that have been written to etc.
        /// </summary>
        public static void SetFirstTimeStart()
        {
            if (!C1File.Exists(FirstTimeStartFilePath))
            {
                string directory = Path.GetDirectoryName(FirstTimeStartFilePath);
                if (!C1Directory.Exists(directory))
                {
                    C1Directory.CreateDirectory(directory);
                }

                var doc = new XDocument(new XElement("Root", new XAttribute("time", DateTime.Now)));

                doc.SaveToFile(FirstTimeStartFilePath);
            }
        }



        /// <summary>
        /// Returns the time the system was startet the very first time
        /// </summary>
        /// <returns>The very first time the system has been started</returns>        
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Composite.IO", "Composite.DoNotUseFileClass:DoNotUseFileClass", Justification = "This is intended")]
        public static DateTime GetFirstTimeStart()
        {
            Verify.That(File.Exists(FirstTimeStartFilePath), "File '{0}' is missing", FirstTimeStartFilePath);

            var doc = XDocumentUtils.Load(FirstTimeStartFilePath);

            return (DateTime)doc.Element("Root").Attribute("time");
        }



        private static string FirstTimeStartFilePath
        {
            get
            {
                return PathUtil.Resolve("~/App_Data/Composite/Configuration/FirstTimeStart.xml");
            }
        }
    }
}
