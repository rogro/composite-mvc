/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.IO;
using System.Xml.Linq;
using Composite.Core.IO;
using Composite.Core.Xml;


namespace Composite.Core.Configuration
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class InstallationInformationFacade
    {
        private static Guid _installationId;

        static InstallationInformationFacade()
        {
            string filepath = Path.Combine(PathUtil.Resolve(GlobalSettingsFacade.ConfigurationDirectory), "InstallationInformation.xml");

            if (C1File.Exists(filepath))
            {
                XDocument doc = XDocumentUtils.Load(filepath);

                XAttribute idAttribute = doc.Root.Attribute("installationId");

                _installationId = (Guid)idAttribute;
            }
            else
            {
                InitializeNewFile(filepath);
            }
        }



        /// <exclude />
        public static Guid InstallationId
        {
            get 
            {
                return _installationId;
            }
        }



        private static void InitializeNewFile(string filepath)
        {
            _installationId = Guid.NewGuid();

            string directory = Path.GetDirectoryName(filepath);
            if (C1Directory.Exists(directory) == false)
            {
                C1Directory.CreateDirectory(directory);
            }

            XDocument doc = new XDocument(
                        new XElement("InstallationInformation",
                            new XAttribute("installationId", _installationId)
                        )
                    );

            doc.SaveToFile(filepath);
        }
    }
}
