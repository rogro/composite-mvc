/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

namespace Composite.Core.ResourceSystem.Icons
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class CommonCommandIcons
    {
        /// <exclude />
        public static ResourceHandle AddNew { get { return GetIconHandle("generic-add"); } }
        /// <exclude />
        public static ResourceHandle Edit { get { return GetIconHandle("generic-edit"); } }
        /// <exclude />
        public static ResourceHandle Refresh { get { return GetIconHandle("generic-refresh"); } }
        /// <exclude />
        public static ResourceHandle Delete { get { return GetIconHandle("generic-delete"); } }
        /// <exclude />
        public static ResourceHandle Search { get { return GetIconHandle("generic-search"); } }
        /// <exclude />
        public static ResourceHandle SetSecurity { get { return GetIconHandle("generic-set-security"); } }
        /// <exclude />
        public static ResourceHandle ShowHistory { get { return GetIconHandle("generic-show-history"); } }
        /// <exclude />
        public static ResourceHandle ShowReport { get { return GetIconHandle("generic-show-report"); } }


        //private static ResourceHandle GetIconHandle()
        //{
        //    return new ResourceHandle(BuildInIconProviderName.ProviderName, "unknown");
        //}

        private static ResourceHandle GetIconHandle(string name)
        {
            return new ResourceHandle(BuildInIconProviderName.ProviderName, name);
        }
    }
}
