/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using Composite.Core.Extensions;
using Composite.Core.Logging;
using Composite.Core.ResourceSystem.Foundation;
using Composite.Core.ResourceSystem.Foundation.PluginFacades;


namespace Composite.Core.ResourceSystem
{
    internal class IconResourceSystemFacadeImpl : IIconResourceSystemFacade
    {
        public IEnumerable<ResourceHandle> GetAllIconHandles()
        {
            return
                from provider in ResourceProviderRegistry.IconResourceProviderNames
                from iconName in ResourceProviderPluginFacade.GetIconNames(provider)
                select new ResourceHandle(provider, iconName);
        }



        public ResourceHandle GetResourceHandle(string iconName)
        {
            if (iconName != string.Empty)
            {
                if (iconName.IsCorrectNamespace('.'))
                {
                    string resourceName = iconName.GetNameFromNamespace();
                    string namespaceName = iconName.GetNamespace();

                    var count = (from item in GetAllIconHandles()
                                 where item.ResourceName == resourceName && item.ResourceNamespace == namespaceName
                                 select item).Count();

                    if (count > 0)
                    {
                        return new ResourceHandle(namespaceName, resourceName);
                    }
                    else
                    {
                        LoggingService.LogWarning("IconResourceSystemFacade", string.Format("The icon {0} could not be found.", iconName));
                    }
                }
                else
                {
                    LoggingService.LogWarning("IconResourceSystemFacade", string.Format("The namespace {0} is not correct.", iconName));
                }
            }

            return null;
        }



        public Bitmap GetIcon(ResourceHandle resourceHandle, IconSize iconSize)
        {
            if (null == resourceHandle) throw new ArgumentNullException("resourceHandle");

            return ResourceProviderPluginFacade.GetIcon(resourceHandle.ResourceNamespace, resourceHandle.ResourceName, iconSize, Thread.CurrentThread.CurrentUICulture);
        }
    }
}
