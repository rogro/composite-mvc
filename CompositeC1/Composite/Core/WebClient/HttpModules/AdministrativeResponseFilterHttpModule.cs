/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using Composite.Core.Types;
using System.Web.WebPages;


namespace Composite.Core.WebClient.HttpModules
{
    internal class AdministrativeResponseFilterHttpModule : IHttpModule
    {
        private static readonly Pair<string, string>[] ReplacementRules = new []
       {
           new Pair<string, string>(@" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""", @""), 
           new Pair<string, string>(@" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""", @""),
           new Pair<string, string>(@"xmlns:ui=""http://www.composite.net/ns/ui/1.0""", @"xmlns:ui=""http://www.w3.org/1999/xhtml""")
        };

        public void Init(HttpApplication context)
        {
            context.PostMapRequestHandler += AttachFilter;
        }

        private static void AttachFilter(object sender, EventArgs e)
        {
            var httpContext = HttpContext.Current;

            if (
                (httpContext.Handler is Page || httpContext.Handler is WebPageHttpHandler)
                && UrlUtils.IsAdminConsoleRequest(httpContext))
            {
                httpContext.Response.Filter = new ReplacementStream(httpContext.Response.Filter);
            }
        }

        public void Dispose()
        {
        }

        internal class ReplacementStream : Utf8StringTransformationStream
        {
            public ReplacementStream(Stream innerStream) : base(innerStream) {}

            public override string Process(string str)
            {
                var sb = new StringBuilder(str);

                foreach(var kvp in ReplacementRules)
                {
                    sb.Replace(kvp.First, kvp.Second);
                }

                return sb.ToString();
            }
        }
    }
}
