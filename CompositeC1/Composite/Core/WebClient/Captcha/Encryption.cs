/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web.Hosting;
using Composite.Core.IO;


namespace Composite.Core.WebClient.Captcha
{
    internal static class Encryption
    {
        private static readonly byte[] _encryptionKey;
        private static readonly byte[] RijndaelIV = { 1, 84, 22, 19, 154, 221, 4, 30, 56, 4, 114, 59, 90, 2, 5, 10 };

        static Encryption()
        {
            var md5 = MD5.Create();

            string key = Environment.MachineName + CaptchaConfiguration.Password + HostingEnvironment.ApplicationPhysicalPath;

            byte[] keyBytes = Encoding.UTF8.GetBytes(key);

            _encryptionKey = md5.ComputeHash(keyBytes);
        }

        public static string Encrypt(string value)
        {
            Verify.ArgumentNotNullOrEmpty(value, "value");

            // Declare the streams used
            // to encrypt to an in memory
            // array of bytes.
            MemoryStream msEncrypt = null;
            CryptoStream csEncrypt = null;
            C1StreamWriter swEncrypt = null;

            // Declare the RijndaelManaged object
            // used to encrypt the data.
            RijndaelManaged rima = null;

            try
            {
                // Create a RijndaelManaged object
                // with the specified key and IV.
                rima = new RijndaelManaged();
                rima.Key = _encryptionKey;
                rima.IV = RijndaelIV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = rima.CreateEncryptor();

                // Create the streams used for encryption.
                msEncrypt = new MemoryStream();
                csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);
                swEncrypt = new C1StreamWriter(csEncrypt);

                //Write all data to the stream.
                swEncrypt.Write(value);
            }
            finally
            {
                if (swEncrypt != null) swEncrypt.Close();
                if (csEncrypt != null) csEncrypt.Close();
                if (msEncrypt != null) msEncrypt.Close();
                if (rima != null) rima.Clear();
            }

            // Return the encrypted bytes from the memory stream.
            return ByteToHexString(msEncrypt.ToArray());
        }

        public static string Decrypt(string encryptedValue)
        {
            Verify.ArgumentNotNullOrEmpty(encryptedValue, "encryptedValue");
            byte[] encodedSequence = HexStringToByteArray(encryptedValue);

            // TDeclare the streams used
            // to decrypt to an in memory
            // array of bytes.
            MemoryStream msDecrypt = null;
            CryptoStream csDecrypt = null;
            C1StreamReader srDecrypt = null;

            // Declare the RijndaelManaged object
            // used to decrypt the data.
            RijndaelManaged rima = null;

            try
            {
                // Create a RijndaelManaged object
                // with the specified key and IV.
                rima = new RijndaelManaged();
                rima.Key = _encryptionKey;
                rima.IV = RijndaelIV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = rima.CreateDecryptor();

                // Create the streams used for decryption.
                msDecrypt = new MemoryStream(encodedSequence);
                csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);
                srDecrypt = new C1StreamReader(csDecrypt);

                // Read the decrypted bytes from the decrypting stream
                // and place them in a string.
                return srDecrypt.ReadToEnd();
            }
            finally
            {
                if (srDecrypt != null) srDecrypt.Close();
                if (csDecrypt != null) csDecrypt.Close();
                if (msDecrypt != null) msDecrypt.Close();
                if (rima != null) rima.Clear();
            }
        }

        private static string ByteToHexString(byte[] myArray)
        {
            return BitConverter.ToString(myArray);
        }

        private static byte[] HexStringToByteArray(string myArray)
        {
            string[] hexElements = myArray.Split('-');

            byte[] recreatedByteArray = new byte[hexElements.Length];
            for (int i = 0; i < hexElements.Length; i++)
            {
                recreatedByteArray[i] = byte.Parse(hexElements[i].Trim(), NumberStyles.HexNumber, CultureInfo.InvariantCulture);
            }
            return recreatedByteArray;
        }
    }
}
