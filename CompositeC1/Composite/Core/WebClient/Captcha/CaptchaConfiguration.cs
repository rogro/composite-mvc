/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.IO;
using System.Xml;
using Composite.Core.Extensions;
using Composite.Core.IO;

namespace Composite.Core.WebClient.Captcha
{
    internal static class CaptchaConfiguration
    {
        private static readonly string CaptchaConfigurationFilePath = @"App_Data\Composite\Configuration\Captcha.xml";
        private static readonly object _syncRoot = new object();

        private static string _password;

        public static string Password
        {
            get
            {
                if (_password != null) return _password;

                lock (_syncRoot)
                {
                    if (_password != null) return _password;

                    string configurationFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, CaptchaConfigurationFilePath);

                    if (C1File.Exists(configurationFilePath))
                    {
                        var doc = new XmlDocument();
                        try
                        {
                            using (var sr = new C1StreamReader(configurationFilePath))
                            {
                                doc.Load(sr);
                            }

                            var passwordNode = doc.SelectSingleNode("captcha/password");
                            if (passwordNode != null && !string.IsNullOrEmpty(passwordNode.InnerText))
                            {
                                _password = passwordNode.InnerText;
                            }
                        }
                        catch (Exception)
                        {
                            // Do nothing
                        }

                        if (_password != null) return _password;

                        // Deleting configuration file
                        C1File.Delete(configurationFilePath);
                    }


                    _password = Guid.NewGuid().ToString();

                    string configFile = @"<captcha> <password>{0}</password> </captcha>".FormatWith(_password);

                    C1File.WriteAllText(configurationFilePath, configFile);

                    return _password;
                }
            }
        }
    }
}
