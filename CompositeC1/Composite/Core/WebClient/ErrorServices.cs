/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Text;
using System.Web;
using Composite.Core.Extensions;
using Composite.Core.Logging;
using Composite.C1Console.Events;


namespace Composite.Core.WebClient
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class ErrorServices
    {
        /// <exclude />
        public static void DocumentAdministrativeError(Exception exception)
        {

            StringBuilder consoleMsg = new StringBuilder();
            consoleMsg.AppendLine(exception.GetBaseException().ToString());

            Log.LogCritical("Web Application Error, Exception", exception);

            var httpContext = HttpContext.Current;
            if (httpContext != null && httpContext.Request != null && httpContext.Request.Url != null)
            {
                consoleMsg.AppendLine();
                consoleMsg.AppendLine("URL:     " + HttpContext.Current.Request.Url);
                if (HttpContext.Current.Request.UrlReferrer != null)
                {
                    consoleMsg.AppendLine("Referer: " + httpContext.Request.UrlReferrer.AbsolutePath);
                }
            }

            string consoleId = ConsoleInfo.TryGetConsoleId();
            if (consoleId != null)
            {
                ConsoleMessageQueueFacade.Enqueue(new LogEntryMessageQueueItem { Level = LogLevel.Error, Message = consoleMsg.ToString(), Sender = typeof(ErrorServices) }, consoleId);
            }
        }



        /// <exclude />
        public static void RedirectUserToErrorPage(string uiContainerName, Exception exception)
        {
            if (HttpContext.Current == null)
                return;

            string redirectUrl;

            switch (uiContainerName)
            {
                case "Document":
                    redirectUrl = UrlUtils.ResolveAdminUrl("content/misc/errors/error.aspx") + "?" + ConvertExceptionToQueryString(exception);
                    break;

                case null:
                case "Wizard":
                case "DataDialog":
                case "ConfirmDialog":
                    redirectUrl = UrlUtils.ResolveAdminUrl("content/misc/errors/error_dialog.aspx") + "?" + ConvertExceptionToQueryString(exception);
                    break;

                default:
                    Log.LogWarning("ErrorServices", string.Format("Unhandled redirect! Unknown container: '{0}", uiContainerName));
                    throw new NotImplementedException(string.Format("Unknown container: '{0}'", uiContainerName));
            }

            HttpContext.Current.Response.Redirect(redirectUrl, true);
        }

        private static string ConvertExceptionToQueryString(Exception exception)
        {
            var sbResult = new StringBuilder();

            int exceptionIndex = 0;

            while (exception != null)
            {
                if(sbResult.Length > 0)
                {
                    sbResult.Append("&");
                }

                string encodedExceptionType = HttpUtility.UrlEncode(exception.GetType().Name);
                string encodedExceptionMessage = HttpUtility.UrlEncode(exception.Message);
                string encodedExceptionStackTrace = HttpUtility.UrlEncode(exception.StackTrace);
                if (encodedExceptionStackTrace.Length > 1000) encodedExceptionStackTrace = encodedExceptionStackTrace.Substring(0, 1000);

                string indexStr = exceptionIndex == 0 ? string.Empty : exceptionIndex.ToString();

                sbResult.Append("type{0}=".FormatWith(indexStr) + encodedExceptionType);
                sbResult.Append("&msg{0}=".FormatWith(indexStr) + encodedExceptionMessage);
                sbResult.Append("&stack{0}=".FormatWith(indexStr) + encodedExceptionStackTrace);

                exceptionIndex++;
                exception = exception.InnerException;
            }

            return sbResult.ToString();
        }
    }
}
