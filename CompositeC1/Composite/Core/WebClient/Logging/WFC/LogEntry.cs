/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Runtime.Serialization;

namespace Composite.Core.WebClient.Logging.WCF
{
    [DataContract(Name = "LogEntry", Namespace = "http://schemas.datacontract.org/2004/07/Composite.Logging.WCF")]
	internal class LogEntry
	{
        public LogEntry()
        {
        }

        public LogEntry(Composite.Core.Logging.LogEntry fileLogEntry)
        {
            TimeStamp = fileLogEntry.TimeStamp;
            ApplicationDomainId = fileLogEntry.ApplicationDomainId;
            ThreadId = fileLogEntry.ThreadId;
            Severity = fileLogEntry.Severity;
            Title = fileLogEntry.Title;
            DisplayOptions = fileLogEntry.DisplayOptions;
            Message = fileLogEntry.Message;
        }

        [DataMember]
        public DateTime TimeStamp;

        [DataMember]
        public int ApplicationDomainId;

        [DataMember]
        public int ThreadId;

        [DataMember]
        public string Severity;

        [DataMember]
        public string Title;

        [DataMember]
        public string DisplayOptions;

        [DataMember]
        public string Message;
	}
}
