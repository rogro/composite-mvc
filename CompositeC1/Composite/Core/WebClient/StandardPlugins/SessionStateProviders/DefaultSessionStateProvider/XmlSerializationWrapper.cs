/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Composite.Core.Extensions;

namespace Composite.Plugins.WebClient.SessionStateProviders.DefaultSessionStateProvider
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [Serializable]
    [XmlRootAttribute(ElementName = "root")]
    public sealed class XmlSerializationWrapper
    {
        /// <exclude />
        public string TypeName;

        /// <exclude />
        public string AssebmlyName;

        /// <exclude />
        public string Value;


        // For serialization purposes
        /// <exclude />
        public XmlSerializationWrapper()
        {
        }


        internal XmlSerializationWrapper(object value)
        {
            Verify.ArgumentNotNull(value, "value");

            Type type = value.GetType();

            TypeName = type.FullName;
            AssebmlyName = type.Assembly.GetName().Name;

            Value = SerializationUtil.SerializeInternal(type, value);
        }

        /// <exclude />
        public object Deserialize()
        {
            Verify.IsNotNull(AssebmlyName, "'AssebmlyName' is null");
            Verify.IsNotNull(TypeName, "'TypeName' is null");

            // TODO: Caching?
            Assembly asm = AppDomain.CurrentDomain.GetAssemblies().Where(a => a.GetName().Name == AssebmlyName).FirstOrDefault();

            if(asm == null) throw new SerializationException("Failed to find assembly '{0}'".FormatWith(AssebmlyName));

            Type type = asm.GetType(TypeName);
            Verify.IsNotNull(type, "Failed to get type '{0}' from assembly '{1}'", AssebmlyName, TypeName);

            return SerializationUtil.DeserializeInternal(type, Value);
        }
    }
}
