/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.Data;

namespace Composite.Plugins.WebClient.SessionStateProviders.DefaultSessionStateProvider
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [Title("Session State Entry")]
    [AutoUpdateble]
    [ImmutableTypeId("{91964c1b-35c1-446c-9e47-6b8d8e65997f}")]
    [KeyPropertyName("Id")]
    [DataScope(DataScopeIdentifier.AdministratedName)]
    [Caching(CachingType.Full)]
    public interface ISessionStateEntry : IData
    {
        /// <exclude />
        [StoreFieldType(PhysicalStoreFieldType.Guid)]
        [ImmutableFieldId("{8e0f00dc-a364-4bdc-bed4-0fa771cce148}")]
        Guid Id { get; set; }

        /// <exclude />
        [StoreFieldType(PhysicalStoreFieldType.DateTime)]
        [ImmutableFieldId("{f2ba2a81-cd6a-4e48-8f12-a4446a1df046}")]
        DateTime ExpirationDate { get; set; }

        /// <exclude />
        [StoreFieldType(PhysicalStoreFieldType.LargeString)]
        [ImmutableFieldId("{174b9f86-c45b-434a-a3a1-083f78fcfa93}")]
        string SerializedValue { get; set; }
    }
}
