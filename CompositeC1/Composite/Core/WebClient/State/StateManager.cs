/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Configuration;
using Composite.Core.Collections.Generic;
using Composite.C1Console.Events;
using Composite.Core.Extensions;
using Composite.Core.WebClient.State.Runtime;

namespace Composite.Core.WebClient.State
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class SessionStateManager
    {
        /// <exclude />
        public static readonly string DefaultProviderName = "Default"; 

        private static readonly ResourceLocker<Resources> _resourceLocker = new ResourceLocker<Resources>(new Resources(), Resources.Initialize);

        static SessionStateManager()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlushEvent);
        }

        /// <exclude />
        public static ISessionStateProvider DefaultProvider
        {
            get { return GetProvider(DefaultProviderName); }
        }

        /// <exclude />
        public static ISessionStateProvider GetProvider(string name)
        {
            var resources = _resourceLocker;

            ISessionStateProvider provider;

            if (!resources.Resources.Providers.TryGetValue(name, out provider))
            {
                lock (resources)
                {
                    if (!resources.Resources.Providers.TryGetValue(name, out provider))
                    {
                        try
                        {
                            provider = resources.Resources.Factory.Create(name);

                            resources.Resources.Providers.Add(name, provider);
                        }
                        catch (ArgumentException ex)
                        {
                            HandleConfigurationError(ex);
                        }
                        catch (ConfigurationErrorsException ex)
                        {
                            HandleConfigurationError(ex);
                        }
                    }
                }
            }

            return provider;
        }

        private static void Flush()
        {
            var resourceLocker = _resourceLocker;
            lock (resourceLocker)
            {
                resourceLocker.ResetInitialization();
            }
        }

        private static void OnFlushEvent(FlushEventArgs args)
        {
            Flush();
        }

        private static void HandleConfigurationError(Exception ex)
        {
            Flush();

            throw new ConfigurationErrorsException("Failed to load the configuration section '{0}' from the configuration.".FormatWith(SessionStateProviderSettings.SectionName), ex);
        }

        private sealed class Resources
        {
            public SessionStateProviderFactory Factory { get; set; }
            public Hashtable<string, ISessionStateProvider> Providers { get; set; }

            public static void Initialize(Resources resources)
            {
                try
                {
                    resources.Factory = new SessionStateProviderFactory();
                }
                catch (NullReferenceException ex)
                {
                    HandleConfigurationError(ex);
                }
                catch (ConfigurationErrorsException ex)
                {
                    HandleConfigurationError(ex);
                }

                resources.Providers = new Hashtable<string, ISessionStateProvider>();
            }
        }
    }
}
