/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using Composite.C1Console.Actions;
using Composite.C1Console.Events;
using Composite.C1Console.Elements;
using Composite.C1Console.Security;
using Composite.C1Console.Tasks;


namespace Composite.Core.WebClient.FlowMediators
{
    internal sealed class ActionExecutionService : IActionExecutionService
    {
        public ActionExecutionService(string elementProviderName, string consoleId)
        {
            this.ElementProviderName = elementProviderName;
            this.ConsoleId = consoleId;
        }

        private string ElementProviderName { get; set; }
        private string ConsoleId { get; set; }

        public void Execute(EntityToken entityToken, ActionToken actionToken, TaskManagerEvent taskManagerEvent)
        {
            FlowControllerServicesContainer flowServicesContainer = new FlowControllerServicesContainer();
            flowServicesContainer.AddService(new ManagementConsoleMessageService(this.ConsoleId));
            flowServicesContainer.AddService(new ElementDataExchangeService(this.ElementProviderName));
            flowServicesContainer.AddService(this);

            FlowToken flowToken = ActionExecutorFacade.Execute(entityToken, actionToken, flowServicesContainer, taskManagerEvent);

            IFlowUiDefinition uiDefinition = FlowControllerFacade.GetCurrentUiDefinition(flowToken, flowServicesContainer);

            ActionResult result = new ActionResult();

            if (typeof(FlowUiDefinitionBase).IsAssignableFrom(uiDefinition.GetType()))
            {
                string serializedEntityToken = EntityTokenSerializer.Serialize(entityToken, true);
                ViewTransitionHelper.HandleNew(this.ConsoleId, this.ElementProviderName, serializedEntityToken, flowToken, (FlowUiDefinitionBase)uiDefinition);
            }
        }
    }
}
