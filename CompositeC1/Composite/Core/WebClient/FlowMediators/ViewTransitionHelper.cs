/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.C1Console.Actions;
using Composite.C1Console.Events;
using System.Web;
using Composite.C1Console.Elements;

namespace Composite.Core.WebClient.FlowMediators
{
    internal static class ViewTransitionHelper
    {
        internal static string MakeViewId(string serializedFlowHandle)
        {
            return "view" + serializedFlowHandle.GetHashCode();
        }

        internal static void HandleRerender(string consoleId, string elementProviderName, FlowToken flowToken, FlowUiDefinitionBase oldUiDefinition, FlowUiDefinitionBase newUiDefinition, FlowControllerServicesContainer servicesContainer)
        {
            if (newUiDefinition.UiContainerType.ActionResultResponseType != oldUiDefinition.UiContainerType.ActionResultResponseType)
            {
                var messageService = servicesContainer.GetService<IManagementConsoleMessageService>();
                messageService.CloseCurrentView();
                HandleNew(consoleId, elementProviderName, string.Empty, flowToken, newUiDefinition);
            }
            else
            {
                // Force update in same container
                HttpContext.Current.Response.Redirect(HttpContext.Current.Request.Url.PathAndQuery, false);
            }
        }


        internal static void HandleNew(string consoleId, string elementProviderName, string serializedEntityToken, FlowToken flowToken, FlowUiDefinitionBase uiDefinition)
        {
            ActionResultResponseType actionViewType = uiDefinition.UiContainerType.ActionResultResponseType;

            if (actionViewType != ActionResultResponseType.None)
            {
                FlowHandle flowHandle = new FlowHandle(flowToken);
                string serializedFlowHandle = flowHandle.Serialize();
                string viewId = MakeViewId(serializedFlowHandle);

                ViewType viewType;
                switch (actionViewType)
                {
                    case ActionResultResponseType.OpenDocument:
                        viewType = ViewType.Main;
                        break;
                    case ActionResultResponseType.OpenModalDialog:
                        viewType = ViewType.ModalDialog;
                        break;
                    default:
                        throw new Exception("unknown action response type");
                }

                string url = string.Format("{0}?consoleId={1}&flowHandle={2}&elementProvider={3}",
                    UrlUtils.ResolveAdminUrl("content/flow/FlowUi.aspx"),
                    consoleId,
                    HttpUtility.UrlEncode(serializedFlowHandle),
                    HttpUtility.UrlEncode(elementProviderName));

                OpenViewMessageQueueItem openView = new OpenViewMessageQueueItem
                {
                    ViewType = viewType,
                    EntityToken = serializedEntityToken,
                    FlowHandle = flowHandle.Serialize(),
                    Url = url,
                    ViewId = viewId
                };

                if (uiDefinition is VisualFlowUiDefinitionBase)
                {
                    VisualFlowUiDefinitionBase visualUiDefinition = (VisualFlowUiDefinitionBase)uiDefinition;
                    if (string.IsNullOrEmpty(visualUiDefinition.ContainerLabel) == false) openView.Label = visualUiDefinition.ContainerLabel;
                }

                ConsoleMessageQueueFacade.Enqueue(openView, consoleId);
            }
        }

        internal static void HandleCloseCurrentView(IFlowUiContainerType uiContainerType)
        {
            string redirectUrl;
            switch (uiContainerType.ContainerName)
            {

                case "Document":
                    redirectUrl = UrlUtils.ResolveAdminUrl("content/flow/FlowUiCompleted.aspx");
                    break;
                case "Wizard":
                case "DataDialog":
                case "ConfirmDialog":
                    redirectUrl = UrlUtils.ResolveAdminUrl("content/flow/FlowUiCompletedDialog.aspx");
                    break;
                default:
                    throw new NotImplementedException("Unknown container " + uiContainerType.ContainerName);
            }

            HttpContext.Current.Response.Redirect(redirectUrl, false);
        }
    }
}
