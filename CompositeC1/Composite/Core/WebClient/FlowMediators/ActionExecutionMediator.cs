/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using Composite.C1Console.Actions;
using Composite.C1Console.Events;
using Composite.C1Console.Elements;
using Composite.C1Console.Security;


namespace Composite.Core.WebClient.FlowMediators
{
    internal static class ActionExecutionMediator
    {
        public static void ExecuteElementAction(ElementHandle elementHandle, ActionHandle actionHandle, string consoleId)
        {
            FlowControllerServicesContainer flowServicesContainer = new FlowControllerServicesContainer();
            flowServicesContainer.AddService(new ManagementConsoleMessageService(consoleId));
            flowServicesContainer.AddService(new ElementDataExchangeService(elementHandle.ProviderName));
            flowServicesContainer.AddService(new ActionExecutionService(elementHandle.ProviderName, consoleId));
            flowServicesContainer.AddService(new ElementInformationService(elementHandle));

            FlowToken flowToken = ActionExecutorFacade.Execute(elementHandle.EntityToken, actionHandle.ActionToken, flowServicesContainer);

            IFlowUiDefinition uiDefinition = FlowControllerFacade.GetCurrentUiDefinition(flowToken, flowServicesContainer);

            if (typeof(FlowUiDefinitionBase).IsAssignableFrom(uiDefinition.GetType()))
            {
                string serializedEntityToken = EntityTokenSerializer.Serialize(elementHandle.EntityToken, true);
                ViewTransitionHelper.HandleNew(consoleId, elementHandle.ProviderName, serializedEntityToken, flowToken, (FlowUiDefinitionBase)uiDefinition);
            }
        }



        public static bool ExecuteElementDraggedAndDropped(ElementHandle draggedElementHandle, ElementHandle newParentdElementHandle, int dropIndex, string consoleId, bool isCopy)
        {
            FlowControllerServicesContainer flowServicesContainer = new FlowControllerServicesContainer();
            flowServicesContainer.AddService(new ManagementConsoleMessageService(consoleId));
            flowServicesContainer.AddService(new ElementDataExchangeService(draggedElementHandle.ProviderName));
            flowServicesContainer.AddService(new ActionExecutionService(draggedElementHandle.ProviderName, consoleId));

            return ElementFacade.ExecuteElementDraggedAndDropped(draggedElementHandle, newParentdElementHandle, dropIndex, isCopy, flowServicesContainer);
        }
    }
}
