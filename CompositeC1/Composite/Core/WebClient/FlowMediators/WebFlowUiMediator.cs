/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Web.UI;
using Composite.C1Console.Actions;
using Composite.C1Console.Events;
using Composite.C1Console.Elements;
using Composite.C1Console.Forms;
using Composite.C1Console.Forms.Flows;
using Composite.C1Console.Forms.WebChannel;
using Composite.Core.WebClient.FlowMediators.FormFlowRendering;


namespace Composite.Core.WebClient.FlowMediators
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class WebFlowUiMediator
    {
        /// <exclude />
        public static Control GetFlowUi(FlowHandle flowHandle, string elementProviderName, string consoleId, out string uiContainerName)
        {
            uiContainerName = null;

            try
            {
                Control webControl = null;
                string viewId = ViewTransitionHelper.MakeViewId(flowHandle.Serialize());

                FlowControllerServicesContainer flowServicesContainer = new FlowControllerServicesContainer();
                flowServicesContainer.AddService(new ActionExecutionService(elementProviderName, consoleId));
                flowServicesContainer.AddService(new ManagementConsoleMessageService(consoleId, viewId));
                flowServicesContainer.AddService(new ElementDataExchangeService(elementProviderName));

                FlowToken flowToken = flowHandle.FlowToken;
                IFlowUiDefinition flowUiDefinition = FlowControllerFacade.GetCurrentUiDefinition(flowToken, flowServicesContainer);

                var formFlowUiDefinition = flowUiDefinition as FormFlowUiDefinition;
                if (formFlowUiDefinition != null)
                {
                    uiContainerName = formFlowUiDefinition.UiContainerType.ContainerName;

                    IUiControl uiForm = FormFlowUiDefinitionRenderer.Render(consoleId, elementProviderName, flowToken, formFlowUiDefinition, WebManagementChannel.Identifier, false, flowServicesContainer);
                    IWebUiControl webForm = (IWebUiControl)uiForm;
                    webControl = webForm.BuildWebControl();

                    if (string.IsNullOrEmpty(webControl.ID)) webControl.ID = "FlowUI";
                }

                return webControl;
            }
            catch (Exception ex)
            {
                ErrorServices.DocumentAdministrativeError(ex);
                ErrorServices.RedirectUserToErrorPage(uiContainerName, ex);
            }

            return new LiteralControl("ERROR");
        }
    }
}
