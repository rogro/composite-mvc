/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;
using System.Diagnostics;
using Composite.Core.ResourceSystem;
using Composite.Core.Types;


namespace Composite.Core.WebClient.Services.TreeServiceObjects
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [DebuggerDisplay("ClientElement: '{Label}'")]
    public sealed class ClientElement
    {
        /// <exclude />
        public string ElementKey { get; set; }  // CORE

        /// <exclude />
        public string ProviderName { get; set; }

        /// <exclude />
        public string EntityToken { get; set; }

        /// <exclude />
        public string Piggybag { get; set; }

        /// <exclude />
        public string PiggybagHash { get; set; }

        /// <exclude />
        public string Label { get; set; }  // CORE

        /// <exclude />
        public string ToolTip { get; set; }  // CORE

        /// <exclude />
        public bool HasChildren { get; set; }  // CORE

        /// <exclude />
        public bool IsDisabled { get; set; }  // CORE

        /// <exclude />
        public ResourceHandle Icon { get; set; }  // CORE

        /// <exclude />
        public ResourceHandle OpenedIcon { get; set; }   // CORE       

        /// <exclude />
        public List<ClientAction> Actions { get; set; }

        /// <exclude />
        public List<string> ActionKeys { get; set; }

        /// <exclude />
        public List<KeyValuePair> PropertyBag { get; set; }  // CORE

        /// <exclude />
        public List<string> DropTypeAccept { get; set; }
        
        /// <exclude />
        public bool DetailedDropSupported { get; set; }

        /// <exclude />
        public string DragType { get; set; }

        /// <exclude />
        public string TagValue { get; set; } // CORE

        /// <exclude />
        public bool ContainsTaggedActions { get; set; } // CORE

        /// <exclude />
        public bool TreeLockEnabled { get; set; }
    }
}
