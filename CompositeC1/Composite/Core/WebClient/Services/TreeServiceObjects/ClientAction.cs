/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using Composite.Core.ResourceSystem;
using System;


namespace Composite.Core.WebClient.Services.TreeServiceObjects
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class ClientAction
    {
        /// <exclude />
        public string ActionToken { get; set; }

        /// <exclude />
        public string Label { get; set; }

        /// <exclude />
        public string ToolTip { get; set; }

        /// <exclude />
        public bool Disabled { get; set; }

        /// <exclude />
        public ResourceHandle Icon { get; set; }

        /// <exclude />
        public ClientActionCategory ActionCategory { get; set; }

        /// <exclude />
        public string CheckboxStatus { get; set; }

        /// <exclude />
        public string TagValue { get; set; }

        /// <exclude />
        public int ActivePositions { get; set; }

        /// <exclude />
        public string ActionKey 
        {
            get
            {
                string secondaryValuesMashup = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}",
                    this.Label,
                    this.ToolTip,
                    this.Disabled,
                    this.Icon.ResourceName,
                    this.Icon.ResourceNamespace,
                    this.ActionCategory.FolderName,
                    this.ActionCategory.GroupId,
                    this.ActionCategory.IsInFolder,
                    this.ActionCategory.IsInToolbar,
                    this.ActionCategory.Name,
                    this.CheckboxStatus);

                return (this.ActionToken + this.Label).GetHashCode() + "::" + secondaryValuesMashup.GetHashCode();
            }
            set
            {
            	// now being returned from client via SOAP!
            	// throw new InvalidOperationException("This can not be set");
            }
        }
    }
}
