/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Linq;
using System.Collections.Generic;
using Composite.C1Console.Elements;
using Composite.C1Console.Security;
using System;


namespace Composite.Core.WebClient.Services.TreeServiceObjects.ExtensionMethods
{
    internal static class ElementActionExtensionMethods
    {
        public static List<ClientAction> ToClientActionList(this IEnumerable<ElementAction> actions)
        {
            var clientActions =
                from action in actions
                orderby action.VisualData.ActionLocation.ActionGroup.Priority, action.VisualData.ActionLocation.ActionGroup.Name, action.VisualData.ActionLocation.ActionType
                select new ClientAction
                      {
                          ActionToken = ActionTokenSerializer.Serialize(action.ActionHandle.ActionToken, true),
                          Label = action.VisualData.Label,
                          ToolTip = action.VisualData.ToolTip,
                          Disabled = action.VisualData.Disabled,
                          Icon = action.VisualData.Icon,
                          CheckboxStatus = GetCheckboxStatusString(action.VisualData.ActionCheckedStatus),
                          ActivePositions = (int)action.VisualData.ActivePositions,
                          TagValue = action.TagValue,                          
                          ActionCategory = new ClientActionCategory
                               {
                                   GroupId = CalculateActionCategoryGroupId(action.VisualData.ActionLocation),
                                   GroupName = action.VisualData.ActionLocation.ActionGroup.Name,
                                   Name = action.VisualData.ActionLocation.ActionType.ToString(),
                                   IsInFolder = action.VisualData.ActionLocation.IsInFolder,
                                   IsInToolbar = action.VisualData.ActionLocation.IsInToolbar,
                                   FolderName = action.VisualData.ActionLocation.FolderName
                               }
                      };

            return clientActions.ToList();
        }


        private static string GetCheckboxStatusString(ActionCheckedStatus actionCheckedStatus)
        {
            switch (actionCheckedStatus)
            {
                case ActionCheckedStatus.Uncheckable:
                    return null;
                case ActionCheckedStatus.Unchecked:
                    return "Unchecked";
                case ActionCheckedStatus.Checked:
                    return "Checked";
                default:
                    throw new InvalidOperationException("Unexpected ActionCheckedStatus value");
            }
        }

        private static string CalculateActionCategoryGroupId(ActionLocation actionLocation)
        {
            // trying to aqvoid overflow - might be lamo way
            return "Key" + Math.Abs(Math.Abs(actionLocation.ActionGroup.Priority.GetHashCode()) - Math.Abs(actionLocation.ActionGroup.Name.GetHashCode())).ToString();
        }

    }
}
