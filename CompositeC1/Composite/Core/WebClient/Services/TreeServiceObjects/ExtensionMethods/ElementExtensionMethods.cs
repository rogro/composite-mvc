/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Composite.C1Console.Elements;
using Composite.C1Console.Security;
using Composite.Core.Types;


namespace Composite.Core.WebClient.Services.TreeServiceObjects.ExtensionMethods
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class ElementExtensionMethods
    {
        /// <exclude />
        public static ClientElement GetClientElement(this Element element)
        {
            if (element.VisualData.Icon == null || element.Actions.Any(a => a.VisualData.Icon == null))
            {
                throw new InvalidOperationException(string.Format("Unable to create ClientElement from Element with entity token '{0}'. The element or one of its actions is missing an icon definition.", element.ElementHandle.EntityToken.Serialize()));
            }

            string entityToken = EntityTokenSerializer.Serialize(element.ElementHandle.EntityToken, true);

            string piggyBag = element.ElementHandle.SerializedPiggyback;
            
            var clientElement = new ClientElement
                   {
                       ElementKey = string.Format("{0}{1}{2}", element.ElementHandle.ProviderName, entityToken, piggyBag), 
                       ProviderName = element.ElementHandle.ProviderName,
                       EntityToken = entityToken,
                       Piggybag = piggyBag,
                       PiggybagHash = HashSigner.GetSignedHash(piggyBag).Serialize(),
                       Label = element.VisualData.Label,
                       HasChildren = element.VisualData.HasChildren,
                       IsDisabled = element.VisualData.IsDisabled,
                       Icon = element.VisualData.Icon,
                       OpenedIcon = element.VisualData.OpenedIcon,
                       ToolTip = element.VisualData.ToolTip,
                       Actions = element.Actions.ToClientActionList(),
                       PropertyBag = element.PropertyBag.ToClientPropertyBag(),
                       TagValue = element.TagValue,
                       ContainsTaggedActions = element.Actions.Any(f => f.TagValue != null),
                       TreeLockEnabled = element.TreeLockBehavior == TreeLockBehavior.Normal
                   };

            clientElement.ActionKeys =
                (from clientAction in clientElement.Actions
                 select clientAction.ActionKey).ToList();

            if (element.MovabilityInfo.DragType != null) clientElement.DragType = element.MovabilityInfo.GetHashedTypeIdentifier();

            List<string> apoptables = element.MovabilityInfo.GetDropHashTypeIdentifiers();
            if (apoptables != null && apoptables.Count > 0)
            {
                clientElement.DropTypeAccept = apoptables;
            }

            clientElement.DetailedDropSupported = element.MovabilityInfo.SupportsIndexedPosition;

            return clientElement;
        }



        /// <exclude />
        public static List<ClientElement> ToClientElementList(this List<Element> elements)
        {
            var list = new List<ClientElement>(elements.Count);

            foreach (Element element in elements)
            {
                list.Add(element.GetClientElement());
            }

            return list;
        }



        /// <exclude />
        public static List<KeyValuePair> ToClientPropertyBag(this Dictionary<string, string> propertyBag)
        {
            if (propertyBag == null || propertyBag.Count == 0) return null;

            var result = new List<KeyValuePair>();

            foreach (KeyValuePair<string, string> kvp in propertyBag)
            {
                result.Add(new KeyValuePair(kvp.Key, kvp.Value));
            }

            return result;
        }

    }
}
