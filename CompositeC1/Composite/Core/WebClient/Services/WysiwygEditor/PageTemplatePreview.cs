/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web;
using Composite.C1Console.Events;
using System.Threading.Tasks;

namespace Composite.Core.WebClient.Services.WysiwygEditor
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class PageTemplatePreview
    {
        private const string RenderingMode = "template";

        static PageTemplatePreview()
        {
            GlobalEventSystemFacade.OnDesignChange += ClearCache;
        }

        /// <exclude />
        public class PlaceholderInformation
        {
            /// <exclude />
            public string PlaceholderId { get; set; }

            /// <exclude />
            public Rectangle ClientRectangle { get; set; }

            /// <exclude />
            public Rectangle ClientRectangleWithZoom { get; set; }
        }

        /// <exclude />
        public static bool GetPreviewInformation(HttpContext context, Guid pageId, Guid templateId, out string imageFilePath, out PlaceholderInformation[] placeholders)
        {
            string serviceUrl = UrlUtils.ResolvePublicUrl("~/Renderers/TemplatePreview.ashx");
            int updateHash = BrowserRender.GetLastCacheUpdateTime(RenderingMode).GetHashCode();
            string requestUrl = new UrlBuilder(context.Request.Url.ToString()).ServerUrl + serviceUrl + "?p=" + pageId + "&t=" + templateId + "&hash=" + updateHash;

            BrowserRender.RenderingResult result = null;

            var renderTask = BrowserRender.RenderUrlAsync(context, requestUrl, RenderingMode);
            renderTask.Wait(10000);
            if (renderTask.Status == TaskStatus.RanToCompletion)
            {
                result = renderTask.Result;
            }

            if (result == null)
            {
                imageFilePath = null;
                placeholders = null;
                return false;
            }

            imageFilePath = result.FilePath;
            string output = result.Output;

            var pList = new List<PlaceholderInformation>();

            string templateInfoPrefix = "templateInfo:";

            if (output.StartsWith(templateInfoPrefix))
            {
                foreach (var infoPart in output.Substring(templateInfoPrefix.Length).Split('|'))
                {
                    string[] parts = infoPart.Split(',');
                    Verify.That(parts.Length == 5, "Incorrectly serialized template part info: " + infoPart);

                    int left = Int32.Parse(parts[1]);
                    int top = Int32.Parse(parts[2]);
                    int width = Int32.Parse(parts[3]);
                    int height = Int32.Parse(parts[4]);

                    var zoom = 1.0;

                    pList.Add(new PlaceholderInformation
                    {
                        PlaceholderId = parts[0], 
                        ClientRectangle = new Rectangle(left, top, width, height),
                        ClientRectangleWithZoom = new Rectangle(
                            (int)Math.Round(zoom * left), 
                            (int)Math.Round(zoom * top),
                            (int)Math.Round(zoom * width),
                            (int)Math.Round(zoom * height))
                    });
                }
            }

            placeholders = pList.ToArray();
            return true;
        }

        /// <exclude />
        public static void ClearCache()
        {
            BrowserRender.ClearCache(RenderingMode);
        }
    }
}
