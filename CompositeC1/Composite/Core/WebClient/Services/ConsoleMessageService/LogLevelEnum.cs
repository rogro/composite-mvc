/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;


namespace Composite.Core.WebClient.Services.ConsoleMessageService
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public enum LogLevel
    {
        /// <exclude />
        Fine,

        /// <exclude />
        Info,

        /// <exclude />
        Debug,

        /// <exclude />
        Warn,

        /// <exclude />
        Error,

        /// <exclude />
        Fatal
    }


    internal static class InternalLogLevelConvertExtensions
    {
        internal static LogLevel AsConsoleType(this Composite.Core.Logging.LogLevel internalLogLevel)
        {
            switch (internalLogLevel)
            {
                case Composite.Core.Logging.LogLevel.Info:
                    return LogLevel.Info;
                case Composite.Core.Logging.LogLevel.Debug:
                    return LogLevel.Debug;
                case Composite.Core.Logging.LogLevel.Fine:
                    return LogLevel.Fine;
                case Composite.Core.Logging.LogLevel.Warning:
                    return LogLevel.Warn;
                case Composite.Core.Logging.LogLevel.Error:
                    return LogLevel.Error;
                case Composite.Core.Logging.LogLevel.Fatal:
                    return LogLevel.Fatal;
                default:
                    throw new ArgumentException("Unknown Composite.Core.Logging.LogLevel " + internalLogLevel.ToString());
            }
        }
    }

}
