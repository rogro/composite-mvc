/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;

namespace Composite.Core.WebClient.Services.ConsoleMessageService
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public enum ViewType
    {
        /// <exclude />
        External = 6,

        /// <exclude />
        Main = 0,

        /// <exclude />
        ModalDialog = 1,

        /// <exclude />
        RightTop = 2,

        /// <exclude />
        RightBottom = 3,

        /// <exclude />
        BottomLeft = 4,

        /// <exclude />
        BottomRight = 5
    }

    internal static class InternalViewTypeConvertExtensions
    {
        internal static ViewType AsConsoleType( this Composite.C1Console.Events.ViewType internalViewType )
        {
            switch (internalViewType)
            {
                case Composite.C1Console.Events.ViewType.External:
                    return ViewType.External;
                case Composite.C1Console.Events.ViewType.Main:
                    return ViewType.Main;
                case Composite.C1Console.Events.ViewType.ModalDialog:
                    return ViewType.ModalDialog;
                case Composite.C1Console.Events.ViewType.RightTop:
                    return ViewType.RightTop;
                case Composite.C1Console.Events.ViewType.RightBottom:
                    return ViewType.RightBottom;
                case Composite.C1Console.Events.ViewType.BottomLeft:
                    return ViewType.BottomLeft;
                case Composite.C1Console.Events.ViewType.BottomRight:
                    return ViewType.BottomRight;
                default:
                    throw new ArgumentException( "Unknown Composite.C1Console.Events.ViewType " + internalViewType.ToString() );
            }
        }
    }
}
