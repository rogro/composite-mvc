/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;


namespace Composite.Core.WebClient.Services.ConsoleMessageService
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public class ConsoleAction
    {
        /// <exclude />
        public ConsoleAction()
        {
            this.Id = Guid.NewGuid().ToString();
        }

        /// <exclude />
        public string Id { get; set; }

        /// <exclude />
        public int SequenceNumber { get; set; }

        /// <exclude />
        public ActionType ActionType { get; set; }


        /// <exclude />
        public OpenViewParams OpenViewParams { get; set; }

        /// <exclude />
        public OpenGenericViewParams OpenGenericViewParams { get; set; }

        /// <exclude />
        public OpenExternalViewParams OpenExternalViewParams { get; set; }

        /// <exclude />
        public DownloadFileParams DownloadFileParams { get; set; }

        /// <exclude />
        public OpenViewDefinitionParams OpenViewDefinitionParams { get; set; }

        /// <exclude />
        public CloseViewParams CloseViewParams { get; set; }

        /// <exclude />
        public RefreshTreeParams RefreshTreeParams { get; set; }

        /// <exclude />
        public MessageBoxParams MessageBoxParams { get; set; }

        /// <exclude />
        public LogEntryParams LogEntryParams { get; set; }

        /// <exclude />
        public BroadcastMessageParams BroadcastMessageParams { get; set; }

        /// <exclude />
        public CloseAllViewsParams CloseAllViewsParams { get; set; }

        /// <exclude />
        public SaveStatusParams SaveStatusParams { get; set; }

        /// <exclude />
        public BindEntityTokenToViewParams BindEntityTokenToViewParams { get; set; }

        /// <exclude />
        public BindEntityTokenToViewParams SelectElementParams { get; set; }        
    }
}
