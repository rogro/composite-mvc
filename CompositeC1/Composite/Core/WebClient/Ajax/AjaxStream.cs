/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.IO;
using System.Web;
using Composite.Core.WebClient.HttpModules;


namespace Composite.Core.WebClient.Ajax
{
    internal class AjaxStream : Utf8StringTransformationStream
    {
        private static readonly string ScriptManagerJS = "UpdateManager.xhtml = null;";

        public AjaxStream(Stream innerOuputStream): base(innerOuputStream) { }

        public override string Process(string str)
        {
            int jsCodePosition = str.IndexOf(ScriptManagerJS, StringComparison.Ordinal);
            if (jsCodePosition == -1)
            {
                return str;
            }

            // Removing encoding symbol
            if (str[0] != '<') // Method StartsWith(...) doesn't work correctly here
            {
                str = str.Substring(1);
                jsCodePosition--;
            }

            string encodedText = HttpContext.Current.Server.UrlEncode(str).Replace("+", "%20"); //EncodeJsString(str);

            return string.Format("{0}UpdateManager.xhtml = \"{1}\";{2}",
                                 str.Substring(0, jsCodePosition),
                                 encodedText,
                                 str.Substring(jsCodePosition + ScriptManagerJS.Length));
        }
    }
}
