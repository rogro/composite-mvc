/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using System.Web;
using Composite.Core.Configuration;

namespace Composite.Core.WebClient
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class CookieHandler
    {
        /// <summary>
        /// Gets a cookie value specific for the current application instance (port and virtual path). 
        /// The actual cookie name will be appended port and path info to ensure a unique cookie across multiple
        /// C1 sites running on the same host name. 
        /// To have explicit control over cookie naming, use the ASP.NET Cookies class.
        /// </summary>
        /// <param name="cookieName">The name used to set this cookie</param>
        /// <returns>Value of the cookie, or null if the cookie was not found</returns>
        public static string Get(string cookieName)
        {
            var context = HttpContext.Current;
            Verify.That(context != null, "HttpContext is not available.");

            cookieName = GetApplicationSpecificCookieName(cookieName);

            var responseCookie = GetCookie(context.Response.Cookies, cookieName);
            if (responseCookie != null)
            {
                return responseCookie.Value;
            }

            var requestCookie = GetCookie(context.Request.Cookies, cookieName);
            if (requestCookie != null)
            {
                return requestCookie.Value;
            }

            return null;
        }


        /// <summary>
        /// Sets a cookie specific for the current application instance (port and virtual path). In order to read this cookie you should use the Get() methos on this class. 
        /// To have explicit control over cookie naming, use the ASP.NET Cookies class.
        /// </summary>
        public static void Set(string cookieName, string value)
        {
            SetCookieInternal(cookieName, value);
        }


        /// <summary>
        /// Sets a cookie specific for the current application instance (port and virtual path). In order to read this cookie you should use the Get() methos on this class. 
        /// To have explicit control over cookie naming, use the ASP.NET Cookies class.
        /// </summary>
        public static void Set(string cookieName, string value, DateTime expires)
        {
            var cookie = SetCookieInternal(cookieName, value);
            cookie.Expires = expires;
        }

        internal static HttpCookie SetCookieInternal(string cookieName, string value)
        {
            cookieName = GetApplicationSpecificCookieName(cookieName);
            var cookie = HttpContext.Current.Response.Cookies[cookieName];
            cookie.Value = value;

            return cookie;
        }

        internal static string GetApplicationSpecificCookieName(string cookieName)
        {
            int siteUniqueHash = InstallationInformationFacade.InstallationId.GetHashCode();
            return string.Format("{0}_{1}_{2}", cookieName, siteUniqueHash, UrlUtils.PublicRootPath.GetHashCode());
        }

        private static HttpCookie GetCookie(HttpCookieCollection cookies, string key)
        {
            if(!cookies.AllKeys.Any(cookieKey => cookieKey == key))
            {
                return null;
            }

            return cookies[key];
        }
    }
}
