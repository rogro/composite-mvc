/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Web;
using System.Web.Routing;
using Composite.C1Console.Security;
using Composite.Core.Extensions;
using Composite.Core.IO;
using Composite.Core.WebClient.Services.WysiwygEditor;


namespace Composite.Core.WebClient
{
    internal class TemplatePreviewRoute : Route
    {
        // Adding "x" as a fictional paramter, so MVC wouldn't use this route for producing outbound links
        public TemplatePreviewRoute() : base("Renderers/TemplatePreviewImag{e}", new TemplatePreviewRouteHandler()) { }
    }

    
    internal class TemplatePreviewRouteHandler : IRouteHandler
    {
        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            return new TemplatePreviewHttpHandler();
        }
    }


    /// <summary>
    /// Renders image that shows information about a function information in Visual Editor
    /// </summary>
    internal class TemplatePreviewHttpHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            if (!UserValidationFacade.IsLoggedIn())
            {
                return;
            }

            try
            {
                string t = context.Request["t"];
                Verify.That(!t.IsNullOrEmpty(), "Missing query string argument 't'");

                string p = context.Request["p"];
                Verify.That(!p.IsNullOrEmpty(), "Missing query string argument 'p'");

                Guid templateId = Guid.Parse(t);
                Guid pageId = Guid.Parse(p);


                string filePath;

                PageTemplatePreview.PlaceholderInformation[] placeholders;
                PageTemplatePreview.GetPreviewInformation(context, pageId, templateId, out filePath, out placeholders);

                Verify.That(C1File.Exists(filePath), "Preview file missing");
                context.Response.ContentType = "image/png";
                context.Response.WriteFile(filePath);
            }
            catch (Exception ex)
            {
                Log.LogError(this.GetType().ToString(), ex.ToString());
                throw;
            }
        }


        public bool IsReusable
        {
            get { return true; }
        }
    }
}
