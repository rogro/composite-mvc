/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Web;
using Composite.Core.Extensions;
using Composite.Core.IO;
using Composite.Core.Logging;


namespace Composite.Core.WebClient.Presentation
{
    class LessRequestHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            if (CookieHandler.Get("mode") == "develop")
            {
                context.Response.Cache.SetExpires(DateTime.Now.AddMonths(-1));
                context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            }
            else
            {
                context.Response.Cache.SetExpires(DateTime.Now.AddMonths(1));
                context.Response.Cache.SetCacheability(HttpCacheability.Private);
            }

            string webPath = context.Request.Path;
            string lessPath = webPath.Substring(0, webPath.LastIndexOf(".aspx"));
            string filePath = context.Server.MapPath(lessPath);

            context.Response.ContentType = "text/javascript";

            if (C1File.Exists(filePath) == false)
            {
                LoggingService.LogWarning("LessRequestHandler", "File {0} not found".FormatWith(filePath));

                context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                context.Response.StatusCode = 404;
                context.ApplicationInstance.CompleteRequest();
            }
            else 
            {
                context.Response.WriteFile(filePath);
            }            
        }

        public bool IsReusable
        {
            get { return false; }
        }
    }
}