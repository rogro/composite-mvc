/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Reflection;

namespace Composite.Core.WebClient
{
    internal static class BuildManagerHelper
    {
        /// <summary>
        /// Gets a user control. Prevents an exception that appears in Visual Studio while debugging
        /// </summary>
        /// <param name="virtualPath"></param>
        /// <returns></returns>
        public static Type GetCompiledType(string virtualPath)
        {
            using(BuildManagerHelper.DisableUrlMetadataCachingScope())
            {
                return System.Web.Compilation.BuildManager.GetCompiledType(virtualPath);
            }
        }

        /// <summary>
        /// Disabling the "url metadata caching" prevents <see cref="System.Web.HttpException" /> in debugger 
        /// </summary>
        /// <param name="disableCaching"></param>
        public static void DisableUrlMetadataCaching(bool disableCaching)
        {
            if (!RuntimeInformation.IsDebugBuild)
            {
                return;
            }

            var systemWeb = typeof (System.Web.TraceMode).Assembly;
            Type cachedPathData = systemWeb.GetType("System.Web.CachedPathData", false);
            if (cachedPathData == null) return;

            FieldInfo field = cachedPathData.GetField("s_doNotCacheUrlMetadata", BindingFlags.Static | BindingFlags.NonPublic);
            if (field == null) return;

            field.SetValue(null, disableCaching);
        }

        /// <summary>
        /// Disabling the "url metadata caching" prevents <see cref="System.Web.HttpException" /> in debugger 
        /// </summary>
        public static IDisposable DisableUrlMetadataCachingScope()
        {
            return new DisableUrlMedataScope();
        }

        public class DisableUrlMedataScope : IDisposable
        {
            public DisableUrlMedataScope()
            {
                DisableUrlMetadataCaching(true);
            }

            public void Dispose()
            {
                DisableUrlMetadataCaching(false);
            }
        }
    }
}
