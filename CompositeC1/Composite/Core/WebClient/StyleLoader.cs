/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Text;
using System.Web.Hosting;

namespace Composite.Core.WebClient
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class StyleLoader
    {
        /// <exclude />
        public static string Render(string directive = null)
        {
            StringBuilder _builder = new StringBuilder();
            string root = UrlUtils.AdminRootPath;

            if (directive == "compile")
            {
                StyleCompiler.Compile(HostingEnvironment.MapPath(root + "/styles/styles.css"),
                                      HostingEnvironment.MapPath(root + "/styles/styles_compiled.css"));

                StyleCompiler.Compile(HostingEnvironment.MapPath(root + "/skins/skin.css"),
                                      HostingEnvironment.MapPath(root + "/skins/skin_compiled.css"));

                return string.Empty;
            }

            bool isInDevelopMode = CookieHandler.Get("mode") == "develop";

            if (isInDevelopMode)
            {
                _builder.AppendLine(stylesheet(root + "/styles/styles.css.aspx"));
                _builder.AppendLine(stylesheet(root + "/skins/skin.css.aspx"));
            }
            else
            {
                _builder.AppendLine(stylesheet(root + "/styles/styles_compiled.css.aspx"));
                _builder.AppendLine(stylesheet(root + "/skins/skin_compiled.css.aspx"));
            }

            _builder.AppendLine(stylesheet(root + "/skins/dynamicskin.css.aspx"));

            return _builder.ToString();

        }

        private static string stylesheet(string url)
        {
            return @"<link rel=""stylesheet"" type=""text/css"" href=""" + url + @"""/>";
        }
    }

}