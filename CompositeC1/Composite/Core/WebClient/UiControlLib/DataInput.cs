/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

using Composite.Core.WebClient.UiControlLib.Foundation;


namespace Composite.Core.WebClient.UiControlLib
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public enum DataInputType
    {
        /// <exclude />
        Default,

        /// <exclude />
        Integer,

        /// <exclude />
        Decimal,

        /// <exclude />
        Password,

        /// <exclude />
        ProgrammingIdentifier,

        /// <exclude />
        ProgrammingNamespace,

        /// <exclude />
        ReadOnly
    }



    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public class DataInput : TextBox
    {
        /// <exclude />
        public DataInput()
            : base()
        {
            this.InputType = DataInputType.Default;
        }

        /// <exclude />
        [Category("Appearance"), DefaultValue(""), Description("The type of data expected in this field")]
        public virtual DataInputType InputType { get; set; }

        /// <exclude />
        [Category("Appearance"), DefaultValue(0), Description("The required minimum length of this field. Default is 0 (no minimum).")]
        public virtual int MinLength { get; set; }

        /// <exclude />
        protected override HtmlTextWriterTag TagKey
        {
            get { return HtmlTextWriterTag.Unknown; }
        }

        /// <exclude />
        protected override string TagName
        {
            get { return "ui:datainput"; }
        }

        /// <exclude />
        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            writer.AddAttribute("value", this.Text);
            writer.AddAttribute("name", this.UniqueID);

            if (this.MaxLength > 0)
            {
                writer.AddAttribute("maxlength", this.MaxLength.ToString());
            }

            if (this.MinLength > 0)
            {
                writer.AddAttribute("minlength", this.MinLength.ToString());
            }

            if (this.AutoPostBack)
            {
                writer.AddAttribute("callbackid", this.ClientID);
                writer.AddAttribute("onvaluechange", "this.dispatchAction ( PageBinding.ACTION_DOPOSTBACK )");
            }

            if (this.InputType != DataInputType.Default)
            {
                switch (this.InputType)
                {
                    case DataInputType.Integer:
                        writer.AddAttribute("type", "integer");
                        break;
                    case DataInputType.Decimal:
                        writer.AddAttribute("type", "number");
                        break;
                    case DataInputType.Password:
                        writer.AddAttribute("password", "true");
                        break;
                    case DataInputType.ProgrammingIdentifier:
                        writer.AddAttribute("type", "programmingidentifier");
                        break;
                    case DataInputType.ProgrammingNamespace:
                        writer.AddAttribute("type", "programmingnamespace");
                        break;
                    case DataInputType.ReadOnly:
                        writer.AddAttribute("readonly", "true");
                        break;
                    default:
                        break;
                }
            }

            this.AddClientAttributes(writer);
        }
    }
}