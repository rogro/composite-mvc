/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

using Composite.Core.WebClient.UiControlLib.Foundation;
using Composite.Core.ResourceSystem;

namespace Composite.Core.WebClient.UiControlLib
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public class TreeNode : LinkButton
    {
        /// <exclude />
        [Category("Appearance"), DefaultValue(""), Description("Image to show as tree node bullet")]
        public virtual string ImageUrl { get; set; }


        /// <exclude />
        public override void Focus()
        {
            this.Focused = true;
        }


        /// <exclude />
        public virtual bool Focused { get; set; }


        /// <exclude />
        protected override void Render(HtmlTextWriter writer)
        {
            writer.WriteBeginTag("ui:treenode");

            writer.WriteAttribute("label", StringResourceSystemFacade.ParseString(this.Text));

            writer.WriteAttribute("id", this.ClientID);

            // bool checksumAttrRequired = false;

            if(!Focused)
            {
                writer.WriteAttribute("callbackid", this.ClientID);
            }

            if (string.IsNullOrEmpty(this.ImageUrl) == false)
            {
                writer.WriteAttribute("image", this.ImageUrl);
            }

            if(this.Focused)
            {
                // checksumAttrRequired = true;
                writer.WriteAttribute("focused", "true");
            }

            //if(checksumAttrRequired)
            //{
            //    writer.WriteAttribute("checksum", DateTime.Now.Ticks.ToString());
            //}

            //if (this.Focused)
            //{
            //    writer.WriteAttribute("focused", "true");
            //    if (string.IsNullOrEmpty(this.OnClientClick) == false)
            //    {
            //        writer.WriteAttribute("onbindingfocus", this.OnClientClick);
            //    }
            //}
            //else
            //{
            //    string clientScripting = string.Format("this.dispatchAction(PageBinding.ACTION_DOPOSTBACK);{0}", this.OnClientClick);
            //    writer.WriteAttribute("onbindingfocus", clientScripting);
            //}

            this.WriteClientAttributes(writer);

            writer.Write( HtmlTextWriter.SelfClosingTagEnd );
        }
    }
}