/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Web.UI;
using Composite.Core.ResourceSystem;
using Composite.Core.WebClient.UiControlLib.Foundation;



namespace Composite.Core.WebClient.UiControlLib
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public class CheckBox : System.Web.UI.WebControls.CheckBox
    {
        /// <exclude />
        public string ItemLabel { get; set; }



        /// <exclude />
        protected override void Render(HtmlTextWriter writer)
        {
            writer.WriteBeginTag("ui:checkbox");

            writer.WriteAttribute("label", StringResourceSystemFacade.ParseString(this.ItemLabel ?? ""));

            if (string.IsNullOrEmpty(this.ToolTip) == false)
            {
                writer.WriteAttribute("title", StringResourceSystemFacade.ParseString(this.ToolTip ?? ""));
            }

            writer.WriteAttribute("name", this.UniqueID);

            if (this.AutoPostBack)
            {
                writer.WriteAttribute("callbackid", this.ClientID);
                writer.WriteAttribute("oncommand", "this.dispatchAction(PageBinding.ACTIONEVENT_DOPOSTBACK);");
            }

            writer.WriteAttribute("ischecked", this.Checked.ToString().ToLower());

            this.WriteClientAttributes(writer);

            writer.Write(HtmlTextWriter.SelfClosingTagEnd);
        }
    }
}