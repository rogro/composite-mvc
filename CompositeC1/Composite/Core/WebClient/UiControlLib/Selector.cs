/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Composite.Core.WebClient.UiControlLib.Foundation;
using Composite.Core.ResourceSystem;


namespace Composite.Core.WebClient.UiControlLib
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public class Selector : DropDownList
    {
        /// <exclude />
        public Selector()
            : base()
        {
            bool isInternetExplorer = HttpContext.Current.Request.UserAgent.Contains("MSIE");
            this.SimpleSelectorMode = false; // isInternetExplorer;
        }


        /// <exclude />
        public bool SelectionRequired { get; set; }

        /// <exclude />
        public string SelectionRequiredLabel { get; set; }

        /// <exclude />
        public bool SimpleSelectorMode { get; set; }


        /// <exclude />
        protected override HtmlTextWriterTag TagKey
        {
            get { return HtmlTextWriterTag.Unknown; }
        }


        /// <exclude />
        protected override string TagName
        {
            get { return this.SimpleSelectorMode ? "ui:simpleselector" : "ui:selector"; }
            
        }


        /// <exclude />
        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            
             writer.AddAttribute("name", this.UniqueID);
            writer.AddAttribute("callbackid", this.ClientID);

            if (this.AutoPostBack)
            {
                writer.AddAttribute("onchange", "this.dispatchAction(PageBinding.ACTION_DOPOSTBACK)");
            }

            if (this.SelectionRequired)
            {
                writer.AddAttribute("required", "true");
                string requiredLabel = this.SelectionRequiredLabel;
                if (string.IsNullOrEmpty(requiredLabel)) requiredLabel = StringResourceSystemFacade.GetString("Composite.Management", "AspNetUiControl.Selector.SelectValueLabel");
                writer.AddAttribute("label", requiredLabel);
            }

            this.AddClientAttributes(writer);
        }


        /// <exclude />
        protected override void RenderContents(HtmlTextWriter writer)
        {
            for (int i = 0; i < this.Items.Count; i++)
            {
                string label = StringResourceSystemFacade.ParseString(this.Items[i].Text);

                int firstNonSpaceSpacePosition = 0;
                while (firstNonSpaceSpacePosition < label.Length && label.Substring(firstNonSpaceSpacePosition, 1) == " ")
                    firstNonSpaceSpacePosition++;

                string spacing = new String(Convert.ToChar(160), firstNonSpaceSpacePosition * 2);
                this.Items[i].Text = string.Concat(spacing, label.Substring(firstNonSpaceSpacePosition));
            }

            if (this.SimpleSelectorMode == false)
            {
                ListItemCollection items = this.Items;
                int count = items.Count;
                if (count > 0)
                {
                    bool flag = false;
                    for (int i = 0; i < count; i++)
                    {
                        ListItem item = items[i];
                        if (item.Enabled)
                        {
                            writer.WriteBeginTag("ui:selection");
                            if (item.Selected && this.SelectionRequired == false)
                            {
                                if (flag)
                                {
                                    this.VerifyMultiSelect();
                                }
                                flag = true;
                                writer.WriteAttribute("selected", "true");
                            }

                            writer.WriteAttribute("label", item.Text, true);
                            writer.WriteAttribute("value", item.Value, true);
                            writer.WriteAttribute("tooltip", item.Text, true);
                            if (this.Page != null)
                            {
                                this.Page.ClientScript.RegisterForEventValidation(this.UniqueID, item.Value);
                            }
                            writer.Write(HtmlTextWriter.SelfClosingTagEnd);
                        }
                    }
                }
            }
            else
            {
                base.RenderContents(writer);
            }
        }


        /// <exclude />
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            base.RenderBeginTag(writer);

            if (this.SimpleSelectorMode)
            {
                writer.WriteFullBeginTag("select");
            }
        }



        /// <exclude />
        public override void RenderEndTag(HtmlTextWriter writer)
        {
        
        	if (this.SimpleSelectorMode)
            {
                writer.WriteEndTag("select");
            }

            base.RenderEndTag(writer);
        }
    }
}