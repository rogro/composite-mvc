/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

using Composite.Core.WebClient.UiControlLib.Foundation;
using Composite.Core.ResourceSystem;

namespace Composite.Core.WebClient.UiControlLib
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public class ClickButton : LinkButton
    {
        /// <exclude />
        public ClickButton()
        {
            this.AutoPostBack = true;
        }


        /// <exclude />
        [Category("Appearance"), DefaultValue(""), Description("The id the ui client should see")]
        public virtual string CustomClientId { get; set; }


        /// <exclude />
        [Category("Appearance"), DefaultValue(""), Description("Client sceipt that ensure post back should be appended to CustomClientScript. Default is true.")]
        public virtual bool AutoPostBack { get; set; }


        /// <exclude />
        [Category("Appearance"), DefaultValue(""), Description("Image to show in the buttom")]
        public virtual string ImageUrl { get; set; }


        /// <exclude />
        [Category("Appearance"), DefaultValue(""), Description("Image to show in the buttom when the button is disabled")]
        public virtual string ImageUrlWhenDisabled { get; set; }


        /// <exclude />
        protected override void Render(HtmlTextWriter writer)
        {
            writer.WriteBeginTag("ui:clickbutton");

            writer.WriteAttribute("label", StringResourceSystemFacade.ParseString(this.Text));
            writer.WriteAttribute("callbackid", this.ClientID);

            string oncommand = "";
            if (string.IsNullOrEmpty(this.OnClientClick) == false)
            {
                oncommand += this.OnClientClick;
            }
            if (this.AutoPostBack)
            {
                if (oncommand.Length > 0 && oncommand.Trim().EndsWith(";") == false)
                {
                    oncommand += ";";
                }
				
				// now implied by callbackid!
                // oncommand += "this.dispatchAction(PageBinding.ACTION_DOPOSTBACK)";
            }
            if (string.IsNullOrEmpty(oncommand) == false)
            {
                writer.WriteAttribute("oncommand", oncommand);
            }

            if (string.IsNullOrEmpty(this.CustomClientId) == false)
            {
                writer.WriteAttribute("id", this.CustomClientId);
            }
            if (string.IsNullOrEmpty(this.ImageUrl) == false)
            {
                writer.WriteAttribute("image", this.ImageUrl);
            }
            if (string.IsNullOrEmpty(this.ImageUrlWhenDisabled) == false)
            {
                writer.WriteAttribute("image-disabled", this.ImageUrlWhenDisabled);
            }
            if (this.Enabled == false)
            {
                writer.WriteAttribute("isdisabled", "true");
            }

            this.WriteClientAttributes(writer);


            writer.Write(HtmlTextWriter.SelfClosingTagEnd);
        }
    }
}