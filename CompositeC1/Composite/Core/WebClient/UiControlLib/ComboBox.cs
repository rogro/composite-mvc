/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Web.UI;
using System.Web.UI.WebControls;

using Composite.Core.Extensions;
using Composite.Core.ResourceSystem;
using Composite.Core.WebClient.UiControlLib.Foundation;

namespace Composite.Core.WebClient.UiControlLib
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public class ComboBox : DropDownList
    {
        private static readonly string ReservedKey = "___reserved value";

        /// <exclude />
        public bool SelectionRequired { get; set; }

        /// <exclude />
        public string SelectionRequiredLabel { get; set; }



        /// <exclude />
        protected override HtmlTextWriterTag TagKey
        {
            get { return HtmlTextWriterTag.Unknown; }
        }


        /// <exclude />
        protected override string TagName
        {
            get { return "ui:datainputselector"; }
        }


        /// <exclude />
        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            writer.AddAttribute("name", this.UniqueID);
            writer.AddAttribute("callbackid", this.ClientID);

            if (this.AutoPostBack)
            {
                writer.AddAttribute("onchange", "this.dispatchAction(PageBinding.ACTION_DOPOSTBACK)");
            }
            if (this.SelectionRequired)
            {
                writer.AddAttribute("required", "true");
                string requiredLabel = this.SelectionRequiredLabel;
                if (string.IsNullOrEmpty(requiredLabel)) requiredLabel = StringResourceSystemFacade.GetString("Composite.Management", "AspNetUiControl.Selector.SelectValueLabel");
                writer.AddAttribute("label", requiredLabel);
            }

            if(!SelectedValue.IsNullOrEmpty())
            {
                writer.AddAttribute("value", SelectedValue);
            }

            this.AddClientAttributes(writer);
        }


        /// <exclude />
        public override string SelectedValue
        {
            get
            {
                return base.SelectedValue;
            }
            set
            {
                if(this.Items.FindByValue(value) == null)
                {
                    this.Items.Add(new ListItem(ReservedKey, value));
                }
                base.SelectedValue = value;
            }
        }


        /// <exclude />
        protected override void RenderContents(HtmlTextWriter writer)
        {
            for (int i = 0; i < this.Items.Count; i++)
            {
                string label = StringResourceSystemFacade.ParseString(this.Items[i].Text);

                int firstNonSpaceSpacePosition = 0;
                while (firstNonSpaceSpacePosition < label.Length && label.Substring(firstNonSpaceSpacePosition, 1) == " ")
                    firstNonSpaceSpacePosition++;

                string spacing = new String(Convert.ToChar(160), firstNonSpaceSpacePosition * 2);
                this.Items[i].Text = string.Concat(spacing, label.Substring(firstNonSpaceSpacePosition));
            }

            ListItemCollection items = this.Items;

            if (items.Count == 0)
            {
                return;
            }

            foreach(ListItem item in items)
            {
                if (!item.Enabled)
                {
                    continue;
                }

                writer.WriteBeginTag("ui:selection");

                writer.WriteAttribute("value", item.Value, true);

                //if (this.Page != null)
                //{
                //    this.Page.ClientScript.RegisterForEventValidation(this.UniqueID, item.Value);
                //}
                writer.Write(HtmlTextWriter.SelfClosingTagEnd);
            }
        }


        /// <exclude />
        protected override bool LoadPostData(string postDataKey, System.Collections.Specialized.NameValueCollection postCollection)
        {
            string postedValue = postCollection[postDataKey];
            if(!postedValue.IsNullOrEmpty() && this.Items.FindByValue(postedValue) == null)
            {
                this.Items.Add(new ListItem(ReservedKey, postedValue));
            }

            return base.LoadPostData(postDataKey, postCollection);
        } 
    }
}