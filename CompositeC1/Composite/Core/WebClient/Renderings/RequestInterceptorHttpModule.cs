/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Globalization;
using System.Web;
using Composite.Core.Extensions;
using Composite.Core.Routing;
using Composite.Core.Routing.Pages;
using Composite.Core.Threading;
using Composite.Core.Configuration;
using Composite.Data.Types;


namespace Composite.Core.WebClient.Renderings
{
    internal class RequestInterceptorHttpModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.BeginRequest += context_BeginRequest;
            context.PreRequestHandlerExecute += context_PreRequestHandlerExecute;
        }

        void context_BeginRequest(object sender, EventArgs e)
        {
            if (!SystemSetupFacade.IsSystemFirstTimeInitialized) return;

            ThreadDataManager.InitializeThroughHttpContext(true);

            var httpContext = (sender as HttpApplication).Context;

            if (CheckForHostnameAliasRedirect(httpContext))
            {
                return;
            }

            if (HandleMediaRequest(httpContext))
            {
                return;
            }

            SetCultureByHostname();

            HandleRootRequestInClassicMode(httpContext);
        }

        static void SetCultureByHostname()
        {
            IHostnameBinding hostnameBinding = HostnameBindingsFacade.GetBindingForCurrentRequest();
            if(hostnameBinding != null && !hostnameBinding.Culture.IsNullOrEmpty())
            {
                CultureInfo cultureInfo = new CultureInfo(hostnameBinding.Culture);
                var thread = System.Threading.Thread.CurrentThread;
                thread.CurrentCulture = cultureInfo;
                thread.CurrentUICulture = cultureInfo;
            }
        }

        static bool HandleMediaRequest(HttpContext httpContext)
        {
            string rawUrl = httpContext.Request.RawUrl;

            UrlKind urlKind;
            var mediaUrlData = MediaUrls.ParseUrl(rawUrl, out urlKind);

            if(mediaUrlData != null
                && (urlKind == UrlKind.Public || urlKind == UrlKind.Internal))
            {
                string rendererUrl = MediaUrls.BuildUrl(mediaUrlData, UrlKind.Renderer);

                httpContext.RewritePath(rendererUrl);
                return true;
            }

            return false;
        }

        void context_PreRequestHandlerExecute(object sender, EventArgs e)
        {
            if (!SystemSetupFacade.IsSystemFirstTimeInitialized) return;

            // Left for backward compatibility with Contrib master pages support, to be removed 
            // when support for master pages is implemented in C1
            // RenderingContext.PreRenderRedirectCheck() does the same logic
            var httpContext = (sender as HttpApplication).Context;

            var page = httpContext.Handler as System.Web.UI.Page;
            if (page == null)
            {
                return;
            }

            if (!string.IsNullOrEmpty(C1PageRoute.GetPathInfo()))
            {
                page.PreRender += (a, b) => CheckThatPathInfoHasBeenUsed(httpContext, page);
            }

            // Setting 404 response code if it is a request to a custom "Page not found" page
            if (HostnameBindingsFacade.IsPageNotFoundRequest())
            {
                page.PreRender += (a, b) =>
                {
                    httpContext.Response.TrySkipIisCustomErrors = true;
                    httpContext.Response.StatusCode = 404;
                };
            }
        }

        private static void HandleRootRequestInClassicMode(HttpContext httpContext)
        {
            if (HttpRuntime.UsingIntegratedPipeline)
            {
                return;
            }

            // Resolving root path "/" for classic mode
            string rawUrl = httpContext.Request.RawUrl;

            string rootPath = UrlUtils.PublicRootPath
                                + (UrlUtils.PublicRootPath.EndsWith("/") ? "" : "/");

            string defaultAspxPath = rootPath + "default.aspx";

            if (rawUrl.StartsWith(defaultAspxPath, StringComparison.InvariantCultureIgnoreCase))
            {
                string query = rawUrl.Substring(defaultAspxPath.Length);

                string shorterQuery = rootPath + query;

                // Checking that there's a related page)
                if (PageUrls.ParseUrl(shorterQuery) != null)
                {
                    httpContext.RewritePath(shorterQuery);
                }
            }
        }

        private static void CheckThatPathInfoHasBeenUsed(HttpContext httpContext, System.Web.UI.Page page)
        {
            if (C1PageRoute.PathInfoUsed)
            {
                return;
            }

            // Redirecting to PageNotFoundUrl or setting 404 response code if PathInfo url part hasn't been used
            if (!HostnameBindingsFacade.RedirectCustomPageNotFoundUrl(httpContext))
            {
                page.Response.StatusCode = 404;
            }

            page.Response.End();
        }


        public void Dispose()
        {
        }

        static bool CheckForHostnameAliasRedirect(HttpContext httpContext)
        {
            if (UrlUtils.IsAdminConsoleRequest(httpContext) 
                || UrlUtils.IsRendererRequest(httpContext)  
                || new UrlSpace(httpContext).ForceRelativeUrls)
            {
                return false;
            }

            var hostnameBinding = HostnameBindingsFacade.GetAliasBinding(httpContext);

            if (hostnameBinding == null)
            {
                return false;
            }

            string hostname = httpContext.Request.Url.Host.ToLowerInvariant();

            var request = httpContext.Request;

            string newUrl = request.Url.AbsoluteUri.Replace("://" + hostname, "://" + hostnameBinding.Hostname);

            httpContext.Response.Redirect(newUrl, false);
            httpContext.ApplicationInstance.CompleteRequest();
            return true;
        }
    }
}