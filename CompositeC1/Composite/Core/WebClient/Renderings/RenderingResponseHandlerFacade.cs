/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using Composite.Data;


namespace Composite.Core.WebClient.Renderings
{
    /// <summary>
    /// Pass information about a request through all <see cref="Composite.Core.WebClient.Renderings.Plugins.RenderingResponseHandler.IDataRenderingResponseHandler"/> 
    /// plugins registered on the Composite C1 site. Use this if you are handling raw page / media http requests yourself.
    /// 
    /// </summary>
	public static class RenderingResponseHandlerFacade
	{
        private static IRenderingResponseHandlerFacade _implementation = new RenderingResponseHandlerFacadeImpl();

        internal static IRenderingResponseHandlerFacade Implementation { get { return _implementation; } set { _implementation = value; } }



        /// <summary>
        /// Pass information about a request through all <see cref="Composite.Core.WebClient.Renderings.Plugins.RenderingResponseHandler.IDataRenderingResponseHandler"/> 
        /// plugins registered on the Composite C1 site. The resulting <see cref="RenderingResponseHandlerResult"/> define how you should treat the request.
        /// </summary>
        /// <param name="requestedItemEntityToken">The data being rendered. This can be <see cref="Composite.Data.Types.IPage"/> and <see cref="Composite.Data.Types.IMediaFile"/>.</param>
        /// <returns>A <see cref="RenderingResponseHandlerResult"/> object detailing what should happen to the user request. Returning null means no special handling should be done (request should continue).</returns>
        public static RenderingResponseHandlerResult GetDataResponseHandling(DataEntityToken requestedItemEntityToken)
        {
            return _implementation.GetDataResponseHandling(requestedItemEntityToken);
        }
	}
}
