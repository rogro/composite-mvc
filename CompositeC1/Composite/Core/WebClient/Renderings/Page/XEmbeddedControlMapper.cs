/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using Composite.Core.Collections.Generic;
using Composite.Core.Xml;
using Composite.Core.Extensions;
using Composite.Functions;

namespace Composite.Core.WebClient.Renderings.Page
{
    internal sealed class XEmbeddedControlMapper : IFunctionResultToXEmbedableMapper, IXElementToControlMapper
	{
        private static readonly XName _markerElementName = Namespaces.AspNetControls + "marker";
        private static readonly XName _formElementName = Namespaces.AspNetControls + "form";
        private static readonly XName _placeholderElementName = Namespaces.AspNetControls + "placeholder";


        private readonly Hashtable<string, Control> _controls = new Hashtable<string, Control>();

        // IFunctionResultToXElementMapper
        public bool TryMakeXEmbedable(FunctionContextContainer contextContainer, object resultObject, out XNode resultElement)
        {
            var control = resultObject as Control;

            if (control == null)
            {
                resultElement = null;
                return false;
            }

            string controlMarkerKey;

            lock (_controls)
            {
                controlMarkerKey = string.Format("[Composite.Function.Render.Asp.Net.Control.{0}]", _controls.Count);
                _controls.Add(controlMarkerKey, control);
            }

            resultElement =
                XElement.Parse(@"<c1marker:{0} xmlns:c1marker=""{1}"" key=""{2}"" />"
                .FormatWith(_markerElementName.LocalName,
                            _markerElementName.Namespace,
                            controlMarkerKey));

            return true;
        }


        // IXElementToControlMapper
        public bool TryGetControlFromXElement(XElement element, out Control control)
        {
            if (element.Name.Namespace != Namespaces.AspNetControls)
            {
                control = null;
                return false;
            }
            
            if (element.Name == _markerElementName)
            {
                control = _controls[element.Attribute("key").Value];
                return true;
            }

            if (element.Name == _formElementName)
            {
                control = new HtmlForm();

                element.CopyAttributes(control as HtmlForm, false);

                foreach (var child in element.Nodes())
                {
                    control.Controls.Add(child.AsAspNetControl(this));
                }

                return true;
            }

            if (element.Name == _placeholderElementName)
            {
                control = new PlaceHolder();

                XAttribute idAttribute = element.Attribute("id");
                if (idAttribute != null)
                {
                    control.ID = idAttribute.Value;
                }

                foreach (var child in element.Nodes())
                {
                    control.Controls.Add(child.AsAspNetControl(this));
                }

                return true;
            }

            throw new InvalidOperationException(string.Format("Unhandled ASP.NET tag '{0}'.", element.Name));
        }
	}
}
