/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

namespace Composite.Core.WebClient.Renderings.Page
{
    /// <summary>
    /// Describes a rendering reason
    /// </summary>
    public enum RenderingReason
    {
        /// <summary>
        /// Undefined
        /// </summary>
        Undefined = 0,
        /// <summary>
        /// A page was requested through a browser
        /// </summary>
        PageView = 1,
        /// <summary>
        /// A page is viewed from C1 Console's browser
        /// </summary>
        C1ConsoleBrowserPageView = 2,
        /// <summary>
        /// A page is reneder from withing an "Edit page" workflow
        /// </summary>
        PreviewUnsavedChanges = 4,
        /// <summary>
        /// A page is rendered to generate an image to be used for function/template visualization
        /// </summary>
        ScreenshotGeneration = 8
    }
}
