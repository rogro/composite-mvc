/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

//namespace Composite.Core.WebClient.Renderings.Page
//{
//    /// <summary>    
//    /// </summary>
//    /// <exclude />
//    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
//    public enum SitemapScope
//    {
//        CurrentPage = 0,
//        CurrentAndDescendantPages = 1,
//        ChildPages = 2,
//        SiblingPages = 15,
//        AncestorPages = 3,
//        AncestorAndCurrent = 4,
//        ParentPage = 5,
//        Level1Page = 6,
//        Level2Page = 7,
//        Level3Page = 8,
//        Level4Page = 9,
//        Level1AndSiblings = 16,
//        Level2AndSiblings = 17,
//        Level3AndSiblings = 18,
//        Level4AndSiblings = 19,
//        Level1AndDescendants = 10,
//        Level2AndDescendants = 11,
//        Level3AndDescendants = 12,
//        Level4AndDescendants = 13,
//        AllPages = 14
//    }
//}
