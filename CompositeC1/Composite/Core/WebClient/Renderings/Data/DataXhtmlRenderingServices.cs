/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Composite.Data;
using Composite.Core.Types;
using Composite.Core.Xml;


namespace Composite.Core.WebClient.Renderings.Data
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
	public static class DataXhtmlRenderingServices
	{
        /// <exclude />
        public static bool CanRender(Type dataTypeToRender, XhtmlRenderingType renderingType)
        {
            IEnumerable<XhtmlRendererProviderAttribute> rendererAttributes = dataTypeToRender.GetCustomInterfaceAttributes<XhtmlRendererProviderAttribute>();
            return rendererAttributes.Any(f => f.SupportedRenderingType == renderingType);
        }


        /// <exclude />
        public static XhtmlDocument Render(IDataReference dataToRender, XhtmlRenderingType renderingType)
        {
            Type dataTypeToRender = dataToRender.ReferencedType;
            IEnumerable<XhtmlRendererProviderAttribute> rendererAttributes = dataTypeToRender.GetCustomInterfaceAttributes<XhtmlRendererProviderAttribute>();

            XhtmlRendererProviderAttribute rendererAttribute = rendererAttributes.FirstOrDefault(f => f.SupportedRenderingType == renderingType);

            if (rendererAttribute == null) throw new NotImplementedException(string.Format("No '{0}' xhtml renderer found for type '{1}'",renderingType, dataTypeToRender.FullName));

            IDataXhtmlRenderer renderer = rendererAttribute.BuildRenderer();

            return renderer.Render(dataToRender);
        }
	}
}
