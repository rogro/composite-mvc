/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Composite.Core.Xml;

namespace Composite.Core.WebClient.Renderings
{
    /// <summary>
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
	public static class RenderingElementNames
	{
        /// <exclude />
        public static XName PlaceHolder { get { return _placeholder; } }

        /// <exclude />
        public static XName PlaceHolderIdAttribute { get { return "id"; } }

        /// <exclude />
        public static XName PlaceHolderTitleAttribute { get { return "title"; } }

        /// <exclude />
        public static XName PlaceHolderDefaultAttribute { get { return "default"; } }


        /// <exclude />
        public static XName PageTitle { get { return _pageTitle; } }

        /// <exclude />
        public static XName PageAbstract { get { return _pageAbstract; } }

        /// <exclude />
        public static XName PageMetaTagDescription { get { return _pageMetaTagDescription; } }


        private static XName _placeholder = Namespaces.Rendering10 + "placeholder";
        private static XName _pageTitle = Namespaces.Rendering10 + "page.title";
        private static XName _pageAbstract = Namespaces.Rendering10 + "page.description";
        private static XName _pageMetaTagDescription = Namespaces.Rendering10 + "page.metatag.description";
    }
}
