/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Configuration;
using Composite.Core.Collections.Generic;
using Composite.Data;
using Composite.Core.WebClient.Renderings.Plugins.RenderingResponseHandler;
using Composite.Core.WebClient.Renderings.Plugins.RenderingResponseHandler.Runtime;


namespace Composite.Core.WebClient.Renderings.Foundation.PluginFacades
{
    internal static class RenderingResponseHandlerPluginFacade
    {
        private static readonly ResourceLocker<Resources> _resourceLocker = new ResourceLocker<Resources>(new Resources(), Resources.Initialize);


        public static RenderingResponseHandlerResult GetDataResponseHandling(string handlerName, DataEntityToken requestedItemEntityToken)
        {
            IDataRenderingResponseHandler handler = GetRenderingResponseHandler(handlerName) as IDataRenderingResponseHandler;
            Verify.IsNotNull(handler, "The Rendering Response Handler named '{0}' does not implement the required interface '{1}'", handlerName, typeof(IDataRenderingResponseHandler));

            return handler.GetDataResponseHandling(requestedItemEntityToken);
        }



        public static bool IsDataRenderingResponseHandler(string handlerName)
        {
            IDataRenderingResponseHandler handler = GetRenderingResponseHandler(handlerName) as IDataRenderingResponseHandler;

            return handler != null;
        }



        private static IRenderingResponseHandler GetRenderingResponseHandler(string handlerName)
        {
            IRenderingResponseHandler applicationStartupHandler;

            var resources = _resourceLocker;

            var providerCache = resources.Resources.ProviderCache;
            if (providerCache.TryGetValue(handlerName, out applicationStartupHandler))
            {
                return applicationStartupHandler;
            }

            using (resources.Locker)
            {
                if (providerCache.TryGetValue(handlerName, out applicationStartupHandler) == false)
                {
                    try
                    {
                        applicationStartupHandler = resources.Resources.Factory.Create(handlerName);

                        providerCache.Add(handlerName, applicationStartupHandler);
                    }
                    catch (ArgumentException ex)
                    {
                        HandleConfigurationError(ex);
                    }
                    catch (ConfigurationErrorsException ex)
                    {
                        HandleConfigurationError(ex);
                    }
                }
            }

            return applicationStartupHandler;
        }



        private static void Flush()
        {
            _resourceLocker.ResetInitialization();
        }



        private static void HandleConfigurationError(Exception ex)
        {
            Flush();

            throw new ConfigurationErrorsException(string.Format("Failed to load the configuration section '{0}' from the configuration.", RenderingResponseHandlerSettings.SectionName), ex);
        }



        private sealed class Resources
        {
            public RenderingResponseHandlerFactory Factory { get; set; }
            public Hashtable<string, IRenderingResponseHandler> ProviderCache { get; set; }

            public static void Initialize(Resources resources)
            {
                try
                {
                    resources.Factory = new RenderingResponseHandlerFactory();
                }
                catch (NullReferenceException ex)
                {
                    HandleConfigurationError(ex);
                }
                catch (ConfigurationErrorsException ex)
                {
                    HandleConfigurationError(ex);
                }

                resources.ProviderCache = new Hashtable<string, IRenderingResponseHandler>();
            }
        }
    }
}
