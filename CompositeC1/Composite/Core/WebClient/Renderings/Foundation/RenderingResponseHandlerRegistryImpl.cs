/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System.Collections.Generic;
using Composite.Core.Configuration;
using Composite.Core.WebClient.Renderings.Plugins.RenderingResponseHandler;
using Composite.Core.WebClient.Renderings.Plugins.RenderingResponseHandler.Runtime;


namespace Composite.Core.WebClient.Renderings.Foundation
{
    internal sealed class RenderingResponseHandlerRegistryImpl : IRenderingResponseHandlerRegistry
	{
        private List<string> _renderingResponseHandlerNames = null;
        private static object _lock = new object();


        public IEnumerable<string> RenderingResponseHandlerNames
        {
            get 
            {
                Initialize();

                foreach (string name in _renderingResponseHandlerNames)
                {
                    yield return name;
                }
            }
        }



        public void Flush()
        {
            lock (_lock)
            {
                _renderingResponseHandlerNames = null;
            }
        }



        private void Initialize()
        {
            if (_renderingResponseHandlerNames == null)
            {
                lock (_lock)
                {
                    if (_renderingResponseHandlerNames == null)
                    {
                        _renderingResponseHandlerNames = new List<string>();

                        RenderingResponseHandlerSettings renderingResponseHandlerSettings = ConfigurationServices.ConfigurationSource.GetSection(RenderingResponseHandlerSettings.SectionName) as RenderingResponseHandlerSettings;
                        foreach (RenderingResponseHandlerData renderingResponseHandlerData in renderingResponseHandlerSettings.RenderingResponseHandlerPlugins)
                        {
                            _renderingResponseHandlerNames.Add(renderingResponseHandlerData.Name);
                        }
                    }
                }
            }
        }        
    }
}
