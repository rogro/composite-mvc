/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Composite.Core.Configuration;
using Composite.C1Console.Forms.Flows.Plugins.UiContainerFactory;
using Composite.C1Console.Forms.Flows.Plugins.UiContainerFactory.Runtime;
using Composite.C1Console.Forms.Plugins.UiControlFactory;
using Composite.C1Console.Forms.Plugins.UiControlFactory.Runtime;
using Composite.Core.Instrumentation;
using Composite.Core.Logging;
using System.Collections;


namespace Composite.Core.WebClient
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class ControlCompilerService
    {
        /// <exclude />
        public static IEnumerable<string> GetControlPaths()
        {
            UiControlFactorySettings uiControlFactorySettings = ConfigurationServices.ConfigurationSource.GetSection(UiControlFactorySettings.SectionName) as UiControlFactorySettings;

            foreach (Composite.C1Console.Forms.Plugins.UiControlFactory.Runtime.ChannelConfigurationElement channelElement in uiControlFactorySettings.Channels)
            {
                foreach (NamespaceConfigurationElement namespaceElement in channelElement.Namespaces)
                {
                    foreach (UiControlFactoryData uiControlFactoryData in namespaceElement.Factories)
                    {
                        PropertyInfo propertyInfo = uiControlFactoryData.GetType().GetProperty("UserControlVirtualPath");
                        if (propertyInfo != null)
                        {
                            string path = (string)propertyInfo.GetValue(uiControlFactoryData, null);

                            yield return path;
                        }
                    }
                }
            }


            UiContainerFactorySettings uiContainerFactorySettings = ConfigurationServices.ConfigurationSource.GetSection(UiContainerFactorySettings.SectionName) as UiContainerFactorySettings;

            foreach (Composite.C1Console.Forms.Flows.Plugins.UiContainerFactory.Runtime.ChannelConfigurationElement channelElement in uiContainerFactorySettings.Channels)
            {
                foreach (UiContainerFactoryData uiControlFactoryData in channelElement.Factories)
                {
                    PropertyInfo propertyInfo = uiControlFactoryData.GetType().GetProperty("UserControlVirtualPath");
                    if (propertyInfo != null)
                    {
                        string path = (string)propertyInfo.GetValue(uiControlFactoryData, null);

                        yield return path;
                    }
                }
            }
        }



        /// <exclude />
        public static void CompileAll()
        {
            FieldInfo theBuildManagerFieldInfo = typeof(System.Web.Compilation.BuildManager).GetField("_theBuildManager", BindingFlags.NonPublic | BindingFlags.Static);
            FieldInfo cachesManagerFieldInfo = typeof(System.Web.Compilation.BuildManager).GetField("_caches", BindingFlags.NonPublic | BindingFlags.Instance);

            object currentBuilderManager = theBuildManagerFieldInfo.GetValue(null);
            IEnumerable caches = cachesManagerFieldInfo.GetValue(currentBuilderManager) as IEnumerable;

            Type standardDiskBuildResultCacheType = caches.OfType<object>().Where(f => f.GetType().FullName == "System.Web.Compilation.StandardDiskBuildResultCache").Select(f => f.GetType()).Single();
            FieldInfo maxRecompilationsFieldInfo = standardDiskBuildResultCacheType.BaseType.GetField("s_maxRecompilations", BindingFlags.NonPublic | BindingFlags.Static);

            object oldValue = maxRecompilationsFieldInfo.GetValue(null);
            maxRecompilationsFieldInfo.SetValue(null, 500);

            IEnumerable<string> paths = GetControlPaths();

            IPerformanceCounterToken performanceCounterToken = PerformanceCounterFacade.BeginAspNetControlCompile();

            // ParallelFacade.ForEach(paths, path => // Call to parallelization facade causes a deadlock while starting-up!!!
            foreach (string path in paths)
            {
                int t1 = Environment.TickCount;
                Type type = BuildManagerHelper.GetCompiledType(path);
                int t2 = Environment.TickCount;

                LoggingService.LogVerbose("RGB(180, 180, 255)ControlCompilerService", string.Format("{0} compiled in {1} ms", path, t2 - t1));
            }

            PerformanceCounterFacade.EndAspNetControlCompile(performanceCounterToken, paths.Count());

           // maxRecompilationsFieldInfo.SetValue(null, oldValue);
        }
    }
}
