/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;
using System.IO;
using Composite.Core.IO;


namespace Composite.Core.WebClient
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class StyleCompiler
    {
        /// <exclude />
        public static void Compile(string sourceFile, string targetFile)
        {
            using (var outputFile = C1File.Open(targetFile, FileMode.Create))
            {
                using (var writer = new C1StreamWriter(outputFile))
                {
                    IncludeRecursive(sourceFile, writer, new HashSet<string>());
                }
            }
        }

        private static void IncludeRecursive(string fileToInclude, C1StreamWriter writer, HashSet<string> alreadyIncludedFiles)
        {
            fileToInclude = fileToInclude.ToLowerInvariant();
            alreadyIncludedFiles.Add(fileToInclude);

            bool includingEnabled = true;

            string[] lines = C1File.ReadAllLines(fileToInclude);
            foreach (string line in lines)
            {
                var trimmedline = line.TrimStart();

                if (includingEnabled)
                {
                    if (trimmedline.StartsWith("@import url"))
                    {
                        int fileNameBegin = trimmedline.IndexOf("\"");
                        int fileNameEnd = trimmedline.LastIndexOf("\"");
                        if (fileNameBegin > 0 && fileNameEnd > 0 && fileNameBegin < fileNameEnd)
                        {
                            string relativeIncludedFilePath =
                                trimmedline.Substring(fileNameBegin + 1, fileNameEnd - fileNameBegin - 1).Replace('/', '\\');
                            string directoryPath = Path.GetDirectoryName(fileToInclude);

                            string includedFilePath = Path.Combine(directoryPath, relativeIncludedFilePath);
                            if (alreadyIncludedFiles.Contains(includedFilePath))
                            {
                                continue;
                            }

                            if (C1File.Exists(includedFilePath))
                            {
                                IncludeRecursive(includedFilePath, writer, alreadyIncludedFiles);
                                continue;
                            }
                        }
                    }
                    else if (trimmedline.StartsWith("#region"))
                    {
                        includingEnabled = false;
                    }
                }

                writer.WriteLine(trimmedline); // or just "trimmedline"
            }
        }
    }
}
