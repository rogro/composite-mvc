/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.ComponentModel;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Web.Hosting;
using Composite.Core.Extensions;


namespace Composite.Core.IO
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [EditorBrowsable(EditorBrowsableState.Never)] 
    public static class PathUtil
    {
        private static readonly string _appBasePath;

        /// <exclude />
        static PathUtil()
        {
            if (HostingEnvironment.IsHosted)
            {
                _appBasePath = HostingEnvironment.ApplicationPhysicalPath;
            }
            else
            {
                _appBasePath = AppDomain.CurrentDomain.BaseDirectory;
            }
        }

        
        /// <summary>
        /// Root directory of website 
        /// </summary>
        /// <exclude />
        public static string BaseDirectory
        {
            get
            {
                return _appBasePath;
            }
        }

        /// <summary>
        /// Resolves a (tilde based) partial path to a full file system path. 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string Resolve(string path)
        {
            if (String.IsNullOrEmpty(path)) throw new ArgumentNullException("path");
            if (path.StartsWith("~"))
            {
                if (path == "~")
                {
                    return BaseDirectory;
                }

                string tildeLessPath = path.Remove(0, 1);
                if (Path.IsPathRooted(tildeLessPath) == false) throw new ArgumentException("Tilde based paths must start with tilde (~) and then a directory separator , like ~/folder/file.txt", "path");

                string appRootRelativePath = tildeLessPath.Remove(0, 1);
                if (Path.IsPathRooted(appRootRelativePath)) throw new ArgumentException("Invalid path", "path");

                appRootRelativePath = appRootRelativePath.Replace( '/', '\\' );

                return Path.Combine(_appBasePath, appRootRelativePath);
            }

            if (Path.IsPathRooted(path) == false) throw new ArgumentException("Relative paths must start with tilde (~), like ~/folder/file.txt", "path");

            return path.Replace('/', '\\');
        }

        /// <exclude />
        public static string CleanFileName(string s)
        {
            return CleanFileName(s, false);
        }


        /// <exclude />
        public static string CleanFileName(string s, bool allowUnicodeLetters)
        {
            var sb = new StringBuilder();

            foreach (var c in s)
            {
                if ("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ -_.1234567890".IndexOf(c) > -1
                    || (allowUnicodeLetters && Char.IsLetter(c)))
                {
                    sb.Append(c);
                }
            }

            return sb.Length > 0 ? sb.ToString() : null;
        }

        /// <exclude />
        public static string GetWebsitePath(string path)
        {
            string s = path.Remove(0, BaseDirectory.Length);
            s = s.Replace('\\', '/');
            if (s.StartsWith("'/'") == false)
            {
                s = "/" + s;
            }

            return s;
        }

        /// <summary>
        /// Indicates whether current Windows user has the NTFS write permission to a file or a folder
        /// </summary>
        /// <param name="fileOrDirectoryPath">Path to a file or a folder</param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Composite.IO", "Composite.DoNotUseFileClass:DoNotUseFileClass")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Composite.IO", "Composite.DoNotUseDirectoryClass:DoNotUseDirectoryClass")]
        public static bool WritePermissionGranted(string fileOrDirectoryPath)
        {
            try
            {
                AuthorizationRuleCollection rules;

                if (C1File.Exists(fileOrDirectoryPath))
                {
                    FileSystemSecurity security = File.GetAccessControl(fileOrDirectoryPath);

                    rules = security.GetAccessRules(true, true, typeof(NTAccount));
                }
                else if (C1Directory.Exists(fileOrDirectoryPath))
                {
                    DirectorySecurity security = Directory.GetAccessControl(fileOrDirectoryPath);

                    rules = security.GetAccessRules(true, true, typeof(NTAccount));
                }
                else
                {
                    throw new FileNotFoundException("File or directory '{0}' does not exist".FormatWith(fileOrDirectoryPath));
                }

                var currentuser = new WindowsPrincipal(WindowsIdentity.GetCurrent());
                bool result = false;
                foreach (FileSystemAccessRule rule in rules)
                {
                    if ((rule.FileSystemRights & (FileSystemRights.WriteData | FileSystemRights.Write)) == 0)
                    {
                        continue;
                    }

                    if (rule.IdentityReference.Value.StartsWith("S-1-"))
                    {
                        var sid = new SecurityIdentifier(rule.IdentityReference.Value);
                        if (!currentuser.IsInRole(sid))
                        {
                            continue;
                        }
                    }
                    else
                    {
                        if (!currentuser.IsInRole(rule.IdentityReference.Value))
                        {
                            continue;
                        }
                    }

                    if (rule.AccessControlType == AccessControlType.Deny)
                        return false;
                    if (rule.AccessControlType == AccessControlType.Allow)
                        result = true;
                }
                return result;
            }
            catch
            {
                return false;
            }
        }
    }
}
