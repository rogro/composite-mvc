/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using Composite.Core.Implementation;


namespace Composite.Core.IO
{
    /// <summary>
    /// This class is a almost one to one version of System.IO.StreamReader. Using this implementation instead 
    /// of System.IO.StreamReader, will ensure that your code will work both on Standard Windows deployment 
    /// and Windows Azure deployment.
    /// See System.IO.StreamReader for more documentation on the methods of this class.
    /// See <see cref="Composite.Core.IO.Plugins.IOProvider.IC1StreamReader"/>.
    /// </summary>
    public class C1StreamReader : TextReader, IDisposable
    {
        private ImplementationContainer<C1StreamReaderImplementation> _implementation;


        /// <summary>
        /// Creates a C1StreamReader.
        /// </summary>
        /// <param name="path">Path to file.</param>
        public C1StreamReader(string path)
            : this(path, Encoding.UTF8, true, 1024)
        {
        }



        /// <summary>
        /// Creates a C1StreamReader.
        /// </summary>
        /// <param name="path">Path to file.</param>
        /// <param name="detectEncodingFromByteOrderMarks">If true, the encoding will be deteced by the content of the file.</param>
        public C1StreamReader(string path, bool detectEncodingFromByteOrderMarks)
            : this(path, Encoding.UTF8, detectEncodingFromByteOrderMarks, 1024)
        {
        }



        /// <summary>
        /// Creates a C1StreamReader.
        /// </summary>
        /// <param name="path">Path to the file.</param>
        /// <param name="encoding">Encoding to use.</param>
        public C1StreamReader(string path, Encoding encoding)
            : this(path, encoding, true, 1024)
        {
        }



        /// <summary>
        /// Creates a C1StreamReader.
        /// </summary>
        /// <param name="path">Path to the file.</param>
        /// <param name="encoding">Encoding to use.</param>
        /// <param name="detectEncodingFromByteOrderMarks">If true, the encoding will be deteced by the content of the file.</param>
        public C1StreamReader(string path, Encoding encoding, bool detectEncodingFromByteOrderMarks)
            : this(path, encoding, detectEncodingFromByteOrderMarks, 1024)
        {
        }



        /// <summary>
        /// Creates a C1StreamReader.
        /// </summary>
        /// <param name="path">Path to the file.</param>
        /// <param name="encoding">Encoding to use.</param>
        /// <param name="detectEncodingFromByteOrderMarks">If true, the encoding will be deteced by the content of the file.</param>
        /// <param name="bufferSize">Buffer size to use.</param>
        public C1StreamReader(string path, Encoding encoding, bool detectEncodingFromByteOrderMarks, int bufferSize)
        {
            _implementation = new ImplementationContainer<C1StreamReaderImplementation>(() => ImplementationFactory.CurrentFactory.CreateC1StreamReader(path, encoding, detectEncodingFromByteOrderMarks, bufferSize));
        }



        /// <summary>
        /// Creates a C1StreamReader.
        /// </summary>
        /// <param name="stream">Stream to ream from.</param>
        public C1StreamReader(Stream stream)
            : this(stream, Encoding.UTF8, true, 1024)
        {
        }



        /// <summary>
        /// Creates a C1StreamReader.
        /// </summary>
        /// <param name="stream">Stream to ream from.</param>
        /// <param name="detectEncodingFromByteOrderMarks">If true, the encoding will be deteced by the content of the file.</param>
        public C1StreamReader(Stream stream, bool detectEncodingFromByteOrderMarks)
            : this(stream, Encoding.UTF8, detectEncodingFromByteOrderMarks, 1024)
        {
        }



        /// <summary>
        /// Creates a C1StreamReader.
        /// </summary>
        /// <param name="stream">Stream to ream from.</param>
        /// <param name="encoding">Encoding to use.</param>
        public C1StreamReader(Stream stream, Encoding encoding)
            : this(stream, encoding, true, 1024)
        {
        }



        /// <summary>
        /// Creates a C1StreamReader.
        /// </summary>
        /// <param name="stream">Stream to ream from.</param>
        /// <param name="encoding">Encoding to use.</param>
        /// <param name="detectEncodingFromByteOrderMarks">If true, the encoding will be deteced by the content of the file.</param>
        public C1StreamReader(Stream stream, Encoding encoding, bool detectEncodingFromByteOrderMarks)
            : this(stream, encoding, detectEncodingFromByteOrderMarks, 1024)
        {
        }



        /// <summary>
        /// Creates a C1StreamReader.
        /// </summary>
        /// <param name="stream">Stream to ream from.</param>
        /// <param name="encoding">Encoding to use.</param>
        /// <param name="detectEncodingFromByteOrderMarks">If true, the encoding will be deteced by the content of the file.</param>
        /// <param name="bufferSize">Buffer size to use.</param>
        public C1StreamReader(Stream stream, Encoding encoding, bool detectEncodingFromByteOrderMarks, int bufferSize)
        {
            _implementation = new ImplementationContainer<C1StreamReaderImplementation>(() => ImplementationFactory.CurrentFactory.CreateC1StreamReader(stream, encoding, detectEncodingFromByteOrderMarks, bufferSize));
        }



        /// <summary>
        /// Reads a byte from the stream.
        /// </summary>
        /// <returns>Returns the read byte.</returns>
        public override int Read()
        {
            return _implementation.Implementation.Read();
        }



        /// <summary>
        /// Reads a block from the file.
        /// </summary>
        /// <param name="buffer">Buffer to read into.</param>
        /// <param name="index">Index in buffer to start storing bytes.</param>
        /// <param name="count">Number of bytes to read.</param>
        /// <returns>Returns the number read bytes.</returns>
        public override int Read(char[] buffer, int index, int count)
        {
            return _implementation.Implementation.Read(buffer, index, count);
        }



        /// <summary>
        /// Read a line from the file.
        /// </summary>
        /// <returns>Returns the read line.</returns>
        public override string ReadLine()
        {
            return _implementation.Implementation.ReadLine();
        }



        /// <summary>
        /// Read all the content of the file into a strng.
        /// </summary>
        /// <returns>The content of the file.</returns>
        public override string ReadToEnd()
        {
            return _implementation.Implementation.ReadToEnd();
        }



        /// <summary>
        /// Reads a block from the file.
        /// </summary>
        /// <param name="buffer">Buffer to store read chars.</param>
        /// <param name="index">Index in buffer to start storing chars.</param>
        /// <param name="count">Number of chars to read.</param>
        /// <returns>Returns the number of read chars.</returns>
        public override int ReadBlock(char[] buffer, int index, int count)
        {
            return _implementation.Implementation.ReadBlock(buffer, index, count);
        }



        /// <summary>
        /// Peeks the current byte.
        /// </summary>
        /// <returns>The current byte.</returns>
        public override int Peek()
        {
            return _implementation.Implementation.Peek();
        }



        /// <summary>
        /// Returns true if the stream is at the end of stream.
        /// </summary>
        public bool EndOfStream
        {
            get
            {
                return _implementation.Implementation.EndOfStream;
            }
        }



        /// <summary>
        /// Closes the stream.
        /// </summary>
        public override void Close()
        {
            _implementation.Implementation.Close();
        }



        /// <summary>
        /// Returns the base stream.
        /// </summary>
        public virtual Stream BaseStream
        {
            get
            {
                return _implementation.Implementation.BaseStream;
            }
        }



        /// <summary>
        /// Returns the current encoding of the stream.
        /// </summary>
        public virtual Encoding CurrentEncoding
        {
            get
            {
                return _implementation.Implementation.CurrentEncoding;
            }
        }



        /// <summary>
        /// Destructor.
        /// </summary>
        ~C1StreamReader()
        {
            Dispose(false);
        }



        /// <summary>
        /// Disposes the stream.
        /// </summary>
        /// <param name="disposing">Ttrue if the stream is disposing.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _implementation.DisposeImplementation();
            }
        }



        //public void DiscardBufferedData()
        //{ 
        //    throw new NotImplementedException(); 
        //}
    }
}
