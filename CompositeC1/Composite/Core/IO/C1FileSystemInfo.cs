/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.IO;
using System.Runtime.Serialization;


namespace Composite.Core.IO
{
    /// <summary>
    /// This class is a almost one to one version of System.IO.FileSystemInfo. Using this implementation instead 
    /// of System.IO.FileSystemInfo, will ensure that your code will work both on Standard Windows deployment 
    /// and Windows Azure deployment.
    /// See System.IO.FileSystemInfo for more documentation on the methods of this class.
    /// </summary>
    public abstract class C1FileSystemInfo
    {
        /// <summary>
        /// Full path name.
        /// </summary>
        public abstract string FullName { get; }



        /// <summary>
        /// Extension.
        /// </summary>
        public abstract string Extension { get; }



        /// <summary>
        /// Returns true if the file system item exitst.
        /// </summary>
        public abstract bool Exists { get; }



        /// <summary>
        /// File attributes of the file system item.
        /// </summary>
        public abstract FileAttributes Attributes { get; set; }



        /// <summary>
        /// Deletes the file system item.
        /// </summary>
        public abstract void Delete();



        /// <summary>
        /// Gets or sets the creation time of the file system item.
        /// </summary>
        public abstract DateTime CreationTime { get; set; }



        /// <summary>
        /// Gets or sets the creation utc time of the file system item.
        /// </summary>
        public abstract DateTime CreationTimeUtc { get; set; }



        /// <summary>
        /// Gets or sets the last access time of the file system item.
        /// </summary>
        public abstract DateTime LastAccessTime { get; set; }



        /// <summary>
        /// Gets or sets the last access utc time of the file system item.
        /// </summary>
        public abstract DateTime LastAccessTimeUtc { get; set; }



        /// <summary>
        /// Gets or sets the last write time of the file system item.
        /// </summary>
        public abstract DateTime LastWriteTime { get; set; }



        /// <summary>
        /// Gets or sets the last write utc time of the file system item.
        /// </summary>
        public abstract DateTime LastWriteTimeUtc { get; set; }



        /// <summary>
        /// </summary>
        public abstract void GetObjectData(SerializationInfo info, StreamingContext context);



        /// <summary>
        /// </summary>
        public abstract void Refresh();
    }
}
