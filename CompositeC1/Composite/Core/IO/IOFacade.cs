/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.IO;
using System.Text;
using Composite.Core.IO.Plugins.IOProvider;
using Composite.Plugins.IO.IOProviders.LocalIOProvider;
using Composite.Core.IO.Foundation.PluginFacades;


namespace Composite.Core.IO
{
    internal static class IOFacade
    {
        public static IC1Directory C1Directory
        {
            get
            {
                return IOProviderPluginFacade.C1Directory;
            }
        }



        public static IC1File C1File
        {
            get
            {
                return IOProviderPluginFacade.C1File;
            }
        }



        public static IC1FileInfo CreateC1FileInfo(string path)
        {
            return IOProviderPluginFacade.CreateFileInfo(path);
        }



        public static IC1DirectoryInfo CreateC1DirectoryInfo(string path)
        {
            return IOProviderPluginFacade.CreateDirectoryInfo(path);
        }



        public static IC1FileStream CreateC1FileStream(string path, FileMode mode, FileAccess access, FileShare share, int bufferSize, FileOptions options)
        {
            return IOProviderPluginFacade.CreateFileStream(path, mode, access, share, bufferSize, options);
        }



        public static IC1FileSystemWatcher CreateC1FileSystemWatcher(string path, string filter)
        {
            return IOProviderPluginFacade.CreateFileSystemWatcher(path, filter);
        }



        public static IC1StreamReader CreateC1StreamReader(string path, Encoding encoding, bool detectEncodingFromByteOrderMarks, int bufferSize)
        {
            return IOProviderPluginFacade.CreateStreamReader(path, encoding, detectEncodingFromByteOrderMarks, bufferSize);
        }



        public static IC1StreamReader CreateC1StreamReader(Stream stream, Encoding encoding, bool detectEncodingFromByteOrderMarks, int bufferSize)
        {
            return IOProviderPluginFacade.CreateStreamReader(stream, encoding, detectEncodingFromByteOrderMarks, bufferSize);
        }



        public static IC1StreamWriter CreateC1StreamWriter(string path, bool append, Encoding encoding, int bufferSize)
        {
            return IOProviderPluginFacade.CreateStreamWriter(path, append, encoding, bufferSize);
        }



        public static IC1StreamWriter CreateC1StreamWriter(Stream stream, Encoding encoding, int bufferSize)
        {
            return IOProviderPluginFacade.CreateStreamWriter(stream, encoding, bufferSize);
        }



        public static IC1Configuration CreateC1Configuration(string path)
        {
            return IOProviderPluginFacade.CreateConfiguration(path);
        }
    }
}
