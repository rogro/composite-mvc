/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.IO;
using System.Runtime.InteropServices;


namespace Composite.Core.IO
{
    /// <summary>
    /// This class is a almost one to one version of System.IO.WaitForChangedResult. Using this implementation instead 
    /// of System.IO.WaitForChangedResult, will ensure that your code will work both on Standard Windows deployment 
    /// and Windows Azure deployment.
    /// See System.IO.WaitForChangedResult for more documentation on the methods of this class.
    /// Used by <see cref="C1FileSystemWatcher"/>
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct C1WaitForChangedResult
    {
        /// <summary>
        /// Gets or sets the name of the changed file system item.
        /// </summary>
        public string Name { get; set; }


        /// <summary>
        /// Gets or sets the old name of the changed file system item.
        /// </summary>
        public string OldName { get; set; }

        
        /// <summary>
        /// Gets or sets the type of the change.
        /// </summary>
        public WatcherChangeTypes ChangeType { get; set; }

        
        /// <summary>
        /// Gets or sets whether the operation timed out.
        /// </summary>
        public bool TimedOut { get; set; }
    }
}
