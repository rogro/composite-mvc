/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.IO;
using System.Text;


namespace Composite.Core.IO.Plugins.IOProvider
{
    /// <summary>
    /// Implementations of this interface is used by C1 through <see cref="IIOProvider"/> 
    /// to provide the behavior of <see cref="Composite.Core.IO.C1StreamReader"/>.
    /// See <see cref="Composite.Core.IO.C1StreamReader"/> for more information.
    /// </summary>
    public interface IC1StreamReader : IDisposable
    {
        /// <summary>
        /// Reads a byte from the stream.
        /// </summary>
        /// <returns>Returns the read byte.</returns>
        int Read();


        /// <summary>
        /// Reads a block from the file.
        /// </summary>
        /// <param name="buffer">Buffer to read into.</param>
        /// <param name="index">Index in buffer to start storing bytes.</param>
        /// <param name="count">Number of bytes to read.</param>
        /// <returns>Returns the number read bytes.</returns>
        int Read(char[] buffer, int index, int count);


        /// <summary>
        /// Read a line from the file.
        /// </summary>
        /// <returns>Returns the read line.</returns>
        string ReadLine();


        /// <summary>
        /// Read all the content of the file into a strng.
        /// </summary>
        /// <returns>The content of the file.</returns>
        string ReadToEnd();


        /// <summary>
        /// Reads a block from the file.
        /// </summary>
        /// <param name="buffer">Buffer to store read chars.</param>
        /// <param name="index">Index in buffer to start storing chars.</param>
        /// <param name="count">Number of chars to read.</param>
        /// <returns>Returns the number of read chars.</returns>
        int ReadBlock(char[] buffer, int index, int count);


        /// <summary>
        /// Peeks the current byte.
        /// </summary>
        /// <returns>The current byte.</returns>
        int Peek();


        /// <summary>
        /// Returns true if the stream is at the end of stream.
        /// </summary>
        bool EndOfStream { get; }


        /// <summary>
        /// Closes the stream.
        /// </summary>
        void Close();


        /// <summary>
        /// Returns the base stream.
        /// </summary>
        Stream BaseStream { get; }


        /// <summary>
        /// Returns the current encoding of the stream.
        /// </summary>
        Encoding CurrentEncoding { get; }
    }
}
