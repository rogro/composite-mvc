/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.IO;


namespace Composite.Core.IO.Plugins.IOProvider
{
    /// <summary>
    /// Implementations of this interface is used by C1 through <see cref="IIOProvider"/> 
    /// to provide the behavior of <see cref="Composite.Core.IO.C1FileSystemWatcher"/>.
    /// See <see cref="Composite.Core.IO.C1FileSystemWatcher"/> for more information.
    /// </summary>
    public interface IC1FileSystemWatcher
    {
        /// <summary>
        /// Gets or sets if events should be raised or not.
        /// </summary>
        bool EnableRaisingEvents { get; set; }


        /// <summary>
        /// Path to watch.
        /// </summary>
        string Path { get; set; }


        /// <summary>
        /// Filter to use.
        /// </summary>
        string Filter { get; set; }


        /// <summary>
        /// Gets or sets of subdirectories should also be watched.
        /// </summary>
        bool IncludeSubdirectories { get; set; }


        /// <summary>
        /// Adds or removes an event handler when new items are created.
        /// </summary>
        event FileSystemEventHandler Created;


        /// <summary>
        /// Adds or removes an event handler when new items changed.
        /// </summary>
        event FileSystemEventHandler Changed;


        /// <summary>
        /// Adds or removes an event handler when new items are renamed.
        /// </summary>
        event RenamedEventHandler Renamed;


        /// <summary>
        /// Adds or removes an event handler when new items are deleted.
        /// </summary>
        event FileSystemEventHandler Deleted;


        /// <summary>
        /// Adds or removes an event handler when an error occure.
        /// </summary>
        event ErrorEventHandler Error;


        /// <summary>
        /// Gets or sets the notify filter.
        /// </summary>        
        NotifyFilters NotifyFilter { get; set; }


        /// <summary>
        /// Begins the initialization.
        /// </summary>
        void BeginInit();


        /// <summary>
        /// Ends the initialization.
        /// </summary>
        void EndInit();


        /// <summary>
        /// </summary>
        /// <param name="changeType"></param>
        /// <returns></returns>
        C1WaitForChangedResult WaitForChanged(WatcherChangeTypes changeType);


        /// <summary>
        /// </summary>
        /// <param name="changeType"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        C1WaitForChangedResult WaitForChanged(WatcherChangeTypes changeType, int timeout);
    }
}
