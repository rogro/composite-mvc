/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.IO;


namespace Composite.Core.IO.Plugins.IOProvider
{
    /// <summary>
    /// Implementations of this interface is used by C1 through <see cref="IIOProvider"/> 
    /// to provide the behavior of <see cref="Composite.Core.IO.C1FileStream"/>.
    /// See <see cref="Composite.Core.IO.C1FileStream"/> for more information.
    /// </summary>
    public interface IC1FileStream : IDisposable
    {
        /// <summary>
        /// Name of the file.
        /// </summary>
        string Name { get; }


        /// <summary>
        /// Size of the file in bytes.
        /// </summary>
        long Length { get; }


        /// <summary>
        /// Sets the length of the file in bytes.
        /// </summary>
        /// <param name="value">New length of file stream.</param>
        void SetLength(long value);


        /// <summary>
        /// Gets or sets the current read/write position in the file stream.
        /// </summary>
        long Position { get; set; }


        /// <summary>
        /// Reads a block of bytes from the file stream.
        /// </summary>
        /// <param name="array">Target buffer of read bytes.</param>
        /// <param name="offset">Offset in the buffer to put read bytes.</param>
        /// <param name="count">Number of bytes to read.</param>
        /// <returns>Number of bytes read.</returns>
        int Read(byte[] array, int offset, int count);


        /// <summary>
        /// Reads a byte form the file stream.
        /// </summary>
        /// <returns>The read byte value.</returns>
        int ReadByte();


        /// <summary>
        /// Writes a block of bytes to the file stream.
        /// </summary>
        /// <param name="array">Bytes to write to the file.</param>
        /// <param name="offset">Offset in buffer to write from.</param>
        /// <param name="count">Number of bytes to write.</param>
        void Write(byte[] array, int offset, int count);


        /// <summary>
        /// Writes a byte to the file stream.
        /// </summary>
        /// <param name="value">Byte value to write.</param>
        void WriteByte(byte value);


        /// <summary>
        /// Seeks to a position in the file stream.
        /// </summary>
        /// <param name="offset">Offset to seek.</param>
        /// <param name="origin">Origin to seek from.</param>
        /// <returns>The new position in the stream.</returns>
        long Seek(long offset, SeekOrigin origin);


        /// <summary>
        /// Returns true if its possible to read from the stream.
        /// </summary>
        bool CanRead { get; }


        /// <summary>
        /// Returns true if its possible to seek in the stream.
        /// </summary>
        bool CanSeek { get; }


        /// <summary>
        /// Returns true if its possible to write to the stream.
        /// </summary>
        bool CanWrite { get; }


        /// <summary>
        /// Flushes the buffered bytes to the file.
        /// </summary>
        void Flush();


        /// <summary>
        /// Flushes the buffered bytes to the file.
        /// </summary>
        /// <param name="flushToDisk"></param>
        void Flush(bool flushToDisk);


        /// <summary>
        /// Closes the file stream.
        /// </summary>
        void Close();
    }
}
