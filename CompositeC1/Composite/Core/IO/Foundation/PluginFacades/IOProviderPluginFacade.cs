/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Configuration;
using System.IO;
using System.Text;
using Composite.C1Console.Events;
using Composite.Core.IO.Plugins.IOProvider;
using Composite.Core.IO.Plugins.IOProvider.Runtime;


namespace Composite.Core.IO.Foundation.PluginFacades
{
    internal static class IOProviderPluginFacade
    {
        private static IOProviderFactory _factory;
        private static IIOProvider _ioProvider;
        private static object _lock = new object();


        static IOProviderPluginFacade()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlushEvent);
        }



        public static IC1Directory C1Directory
        {
            get
            {
                IIOProvider ioProvider = GetIOProvider();

                return ioProvider.C1Directory;
            }
        }



        public static IC1File C1File
        {
            get
            {
                IIOProvider ioProvider = GetIOProvider();

                return ioProvider.C1File;
            }
        }



        public static IC1FileInfo CreateFileInfo(string path)
        {
            IIOProvider ioProvider = GetIOProvider();

            return ioProvider.CreateFileInfo(path);
        }



        public static IC1DirectoryInfo CreateDirectoryInfo(string path)
        {
            IIOProvider ioProvider = GetIOProvider();

            return ioProvider.CreateDirectoryInfo(path);
        }



        public static IC1FileStream CreateFileStream(string path, FileMode mode, FileAccess access, FileShare share, int bufferSize, FileOptions options)
        {
            IIOProvider ioProvider = GetIOProvider();

            return ioProvider.CreateFileStream(path, mode, access, share, bufferSize, options);
        }



        public static IC1FileSystemWatcher CreateFileSystemWatcher(string path, string filter)
        {
            IIOProvider ioProvider = GetIOProvider();

            return ioProvider.CreateFileSystemWatcher(path, filter);
        }



        public static IC1StreamReader CreateStreamReader(string path, Encoding encoding, bool detectEncodingFromByteOrderMarks, int bufferSize)
        {
            IIOProvider ioProvider = GetIOProvider();

            return ioProvider.CreateStreamReader(path, encoding, detectEncodingFromByteOrderMarks, bufferSize);
        }



        public static IC1StreamReader CreateStreamReader(Stream stream, Encoding encoding, bool detectEncodingFromByteOrderMarks, int bufferSize)
        {
            IIOProvider ioProvider = GetIOProvider();

            return ioProvider.CreateStreamReader(stream, encoding, detectEncodingFromByteOrderMarks, bufferSize);
        }



        public static IC1StreamWriter CreateStreamWriter(string path, bool append, Encoding encoding, int bufferSize)
        {
            IIOProvider ioProvider = GetIOProvider();

            return ioProvider.CreateStreamWriter(path, append, encoding, bufferSize);
        }



        public static IC1StreamWriter CreateStreamWriter(Stream stream, Encoding encoding, int bufferSize)
        {
            IIOProvider ioProvider = GetIOProvider();

            return ioProvider.CreateStreamWriter(stream, encoding, bufferSize);
        }



        public static IC1Configuration CreateConfiguration(string path)
        {
            IIOProvider ioProvider = GetIOProvider();

            return ioProvider.CreateConfiguration(path);
        }



        private static IIOProvider GetIOProvider()
        {
            if (_factory == null || _ioProvider == null)
            {
                lock (_lock)
                {
                    if (_factory == null || _ioProvider == null)
                    {
                        try
                        {
                            _factory = new IOProviderFactory();
                            _ioProvider = _factory.CreateDefault();
                        }
                        catch (ArgumentException ex)
                        {
                            HandleConfigurationError(ex);
                        }
                        catch (ConfigurationErrorsException ex)
                        {
                            HandleConfigurationError(ex);
                        }

                    }
                }
            }

            return _ioProvider;
        }



        private static void Flush()
        {
            _factory = null;
            _ioProvider = null;
        }



        private static void OnFlushEvent(FlushEventArgs args)
        {
            Flush();
        }



        private static void HandleConfigurationError(Exception ex)
        {
            Flush();

            throw new ConfigurationErrorsException(string.Format("Failed to load the configuration section '{0}' from the configuration.", IOProviderSettings.SectionName), ex);
        }
    }
}
