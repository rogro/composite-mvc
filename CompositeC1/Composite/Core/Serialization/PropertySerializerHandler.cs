/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Composite.Core.Types;


namespace Composite.Core.Serialization
{
	class PropertySerializerHandler : ISerializerHandler
	{
        public string Serialize(object objectToSerialize)
        {
            if (objectToSerialize == null) throw new ArgumentNullException("objectToSerialize");

            StringBuilder sb = new StringBuilder();

            StringConversionServices.SerializeKeyValuePair(sb, "_Type_", TypeManager.SerializeType(objectToSerialize.GetType()));

            IEnumerable<PropertyInfo> propertyInfos =
                from prop in objectToSerialize.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
                where prop.CanRead && prop.CanWrite 
                select prop;

            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                MethodInfo methodInfo =
                        (from mi in typeof(StringConversionServices).GetMethods(BindingFlags.Public | BindingFlags.Static)
                         where mi.Name == "SerializeKeyValuePair" &&
                               mi.IsGenericMethodDefinition &&
                               mi.GetParameters().Length == 3 &&
                               mi.GetParameters()[2].ParameterType.IsGenericParameter 
                         select mi).SingleOrDefault();
                
                methodInfo = methodInfo.MakeGenericMethod(new Type[] { propertyInfo.PropertyType });

                object propertyValue = propertyInfo.GetValue(objectToSerialize, null);

                methodInfo.Invoke(null, new object[] { sb, propertyInfo.Name, propertyValue });
            }

            return sb.ToString();
        }

        public object Deserialize(string serializedObject)
        {
            Dictionary<string, string> dic = StringConversionServices.ParseKeyValueCollection(serializedObject);

            if (dic.ContainsKey("_Type_") == false) throw new ArgumentException("serializedObject is of wrong format");

            string typeString = StringConversionServices.DeserializeValueString(dic["_Type_"]);
            Type type = TypeManager.GetType(typeString);

            object obj = Activator.CreateInstance(type);

            IEnumerable<PropertyInfo> propertyInfos =
                from prop in type.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                where prop.CanRead && prop.CanWrite 
                select prop;

            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                if (dic.ContainsKey(propertyInfo.Name) == false) throw new ArgumentException("serializedObject is of wrong format");

                MethodInfo methodInfo =
                        (from mi in typeof(StringConversionServices).GetMethods(BindingFlags.Public | BindingFlags.Static)
                         where mi.Name == "DeserializeValue" &&
                               mi.IsGenericMethodDefinition &&
                               mi.GetParameters().Length == 2 &&
                               mi.GetParameters()[1].ParameterType.IsGenericParameter 
                         select mi).SingleOrDefault();

                object defaultValue;
                if (propertyInfo.PropertyType == typeof(Guid)) defaultValue = default(Guid);
                else if (propertyInfo.PropertyType == typeof(string)) defaultValue = default(string);
                else if (propertyInfo.PropertyType == typeof(int)) defaultValue = default(int);
                else if (propertyInfo.PropertyType == typeof(DateTime)) defaultValue = default(DateTime);
                else if (propertyInfo.PropertyType == typeof(bool)) defaultValue = default(bool);
                else if (propertyInfo.PropertyType == typeof(decimal)) defaultValue = default(decimal);
                else if (propertyInfo.PropertyType == typeof(long)) defaultValue = default(long);
                else defaultValue = null;

                methodInfo = methodInfo.MakeGenericMethod(new Type[] { propertyInfo.PropertyType });
               
                object propertyValue = methodInfo.Invoke(null, new object[] { dic[propertyInfo.Name], defaultValue });

                propertyInfo.SetValue(obj, propertyValue, null);
            }

            return obj;
        }
    }
}
