/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Text;
using Composite.Core.Collections.Generic;
using Composite.C1Console.Events;
using Composite.Core.Extensions;
using Composite.Core.Serialization.CodeGeneration;
using Composite.Core.Serialization.CodeGeneration.Foundation;
using Composite.Core.Types;
using Composite.Core.Logging;


namespace Composite.Core.Serialization
{
    internal static class SerializationFacade
    {
        private static ResourceLocker<Resources> _resourceLocker = new ResourceLocker<Resources>(new Resources(), Resources.Initialize);



        static SerializationFacade()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlushEvent);
        }


        public static string Serialize(object propertyClass)
        {
            ISerializer serializer = GetSerializer(propertyClass.GetType());

            StringBuilder sb = new StringBuilder();

            serializer.Serialize(propertyClass, sb);

            return sb.ToString();
        }



        public static object Deserialize(Type propertyClassType, string serializedProperties)
        {
            ISerializer serializer = GetSerializer(propertyClassType);

            Dictionary<string, string> objectState =
                StringConversionServices.ParseKeyValueCollection(serializedProperties);

            return serializer.Deserialize(objectState);
        }



        public static T Deserialize<T>(Type propertyClassType, string serializedProperties)
        {
            ISerializer serializer = GetSerializer(propertyClassType);

            Dictionary<string, string> objectState =
                StringConversionServices.ParseKeyValueCollection(serializedProperties);

            return (T)serializer.Deserialize(objectState);
        }



        private static ISerializer GetSerializer(Type propertyClassType)
        {
            var reusourceLocker = _resourceLocker;
            var serializerCache = reusourceLocker.Resources.SerializerCache;

            ISerializer serializer;

            if (serializerCache.TryGetValue(propertyClassType, out serializer))
            {
                return serializer;
            }


            using (reusourceLocker.Locker)
            {
                if (!serializerCache.TryGetValue(propertyClassType, out serializer))
                {
                    serializer = PropertySerializerManager.GetPropertySerializer(propertyClassType);

                    serializerCache.Add(propertyClassType, serializer);
                }
            }

            return serializer;
        }



        private static void Flush()
        {
            _resourceLocker.ResetInitialization();
        }



        private static void OnFlushEvent(FlushEventArgs args)
        {
            Flush();
        }



        private sealed class Resources
        {
            public CompiledTypeCache<ISerializer> SerializerCache { get; set; }

            public static void Initialize(Resources resources)
            {
                resources.SerializerCache = new CompiledTypeCache<ISerializer>();
            }
        }
    }
}
