/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Xml.Linq;
using Composite.Core.Types;


namespace Composite.Core.Serialization
{
    internal sealed class SystemPrimitivValueXmlSerializer : IValueXmlSerializer
    {
        public bool TrySerialize(Type objectToSerializeType, object objectToSerialize, IXmlSerializer xmlSerializer, out XElement serializedObject)
        {
            if (objectToSerializeType == null) throw new ArgumentNullException("objectToSerializeType");

            if ((objectToSerializeType != typeof(int)) &&
                (objectToSerializeType != typeof(long)) &&
                (objectToSerializeType != typeof(bool)) &&
                (objectToSerializeType != typeof(string)) &&
                (objectToSerializeType != typeof(double)) &&
                (objectToSerializeType != typeof(decimal)) &&
                (objectToSerializeType != typeof(Guid)) &&
                (objectToSerializeType != typeof(DateTime)))
            {
                serializedObject = null;
                return false;
            }

            serializedObject = new XElement("Value");

            string serializedType = TypeManager.SerializeType(objectToSerializeType);

            serializedObject.Add(new XAttribute("type", serializedType));

            if (objectToSerialize != null)
            {
                serializedObject.Add(new XAttribute("value", objectToSerialize));
            }

            return true;
        }



        public bool TryDeserialize(XElement serializedObject, IXmlSerializer xmlSerializer, out object deserializedObject)
        {
            if (serializedObject == null) throw new ArgumentNullException("serializedObject");

            deserializedObject = null;

            if (serializedObject.Name.LocalName != "Value") return false;

            XAttribute typeAttribute = serializedObject.Attribute("type");
            if (typeAttribute == null) return false;

            XAttribute valueAttribute = serializedObject.Attribute("value");
            if (valueAttribute == null) return true;

            Type type = TypeManager.GetType(typeAttribute.Value);

            if (type == typeof(int)) deserializedObject = (int)valueAttribute;
            else if (type == typeof(long)) deserializedObject = (long)valueAttribute;
            else if (type == typeof(bool)) deserializedObject = (bool)valueAttribute;
            else if (type == typeof(string)) deserializedObject = (string)valueAttribute;
            else if (type == typeof(double)) deserializedObject = (double)valueAttribute;
            else if (type == typeof(decimal)) deserializedObject = (decimal)valueAttribute;
            else if (type == typeof(Guid)) deserializedObject = (Guid)valueAttribute;
            else if (type == typeof(DateTime)) deserializedObject = (DateTime)valueAttribute;
            else
            {
                return false;
            }

            return true;
        }
    }
}
