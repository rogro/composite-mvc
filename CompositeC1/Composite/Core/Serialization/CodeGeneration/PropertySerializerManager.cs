/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Composite.C1Console.Events;
using Composite.Core.Collections.Generic;
using Composite.Core.Serialization.CodeGeneration.Foundation;
using Composite.Core.Types;


namespace Composite.Core.Serialization.CodeGeneration
{
    internal static class PropertySerializerManager
    {
        private static readonly ResourceLocker<Resources> ResourceLocker = new ResourceLocker<Resources>(new Resources(), Resources.Initialize, false);


        static PropertySerializerManager()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlushEvent);
        }

        /// <summary>
        /// Returns a property serializer for the given property type.
        /// If no serializer exists, one will be runtime code generated.
        /// </summary>
        /// <param name="propertyClassType"></param>
        /// <returns></returns>
        public static ISerializer GetPropertySerializer(Type propertyClassType)
        {
            ISerializer serializer;

            if (!ResourceLocker.Resources.SerializersTypesCache.TryGetValue(propertyClassType, out serializer))
            {
                using (ResourceLocker.Locker)
                {
                    if (!ResourceLocker.Resources.SerializersTypesCache.TryGetValue(propertyClassType, out serializer))
                    {
                        Type propertySerializerType = GetPropertySerializerType(propertyClassType);

                        serializer = (ISerializer)Activator.CreateInstance(propertySerializerType);

                        ResourceLocker.Resources.SerializersTypesCache.Add(propertySerializerType, serializer);
                    }
                }
            }

            return serializer;
        }



        private static Type GetPropertySerializerType(Type propertyClassType)
        {
            string propertySerializerTypeName = PropertySerializerTypeCodeGenerator.CreateSerializerClassFullName(propertyClassType);

            Type propertySerializerType = TypeManager.TryGetType(propertySerializerTypeName);

            if (propertySerializerType == null)
            {
                propertySerializerType = CodeGeneratePropertySerializer(propertyClassType);
            }

            return propertySerializerType;
        }




        private static Type CodeGeneratePropertySerializer(Type propertyClassType)
        {
            CodeGenerationBuilder codeGenerationBuilder = new CodeGenerationBuilder("PropertySerializer: " + propertyClassType.FullName);

            PropertySerializerTypeCodeGenerator.AddPropertySerializerTypeCode(codeGenerationBuilder, propertyClassType);

            IEnumerable<Type> types = CodeGenerationManager.CompileRuntimeTempTypes(codeGenerationBuilder);

            return types.Single();
        }



        private static void Flush()
        {
            ResourceLocker.ResetInitialization();
        }



        private static void OnFlushEvent(FlushEventArgs args)
        {
            Flush();
        }



        private sealed class Resources
        {
            public Dictionary<Type, ISerializer> SerializersTypesCache;

            public static void Initialize(Resources resources)
            {
                resources.SerializersTypesCache = new Dictionary<Type, ISerializer>();
            }
        }
    }
}
