/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Composite.Core.Types;


namespace Composite.Core.Serialization
{
    internal sealed class SerializerHandlerFacadeImpl : ISerializerHandlerFacade
	{
        private Dictionary<Type, ISerializerHandler> _serializeHandlers = new Dictionary<Type, ISerializerHandler>();
        private readonly object _lock = new object();



        public bool TrySerialize(object objectToSerialize, out string serializedObject, out string errorMessage)
        {
            if (objectToSerialize == null) throw new ArgumentNullException("objectToSerialize");

            serializedObject = null;

            IEnumerable<SerializerHandlerAttribute> serializerHandlerAttributes = objectToSerialize.GetType().GetCustomAttributesRecursively<SerializerHandlerAttribute>();
            if (serializerHandlerAttributes.Count() == 0)
            {
                errorMessage = string.Format("The type '{0}' has no '{1}' defined in its inherit tree", objectToSerialize.GetType(), typeof(SerializerHandlerAttribute));
                return false;
            }

            SerializerHandlerAttribute serializerHandlerAttribute = serializerHandlerAttributes.First();
            if (serializerHandlerAttribute.SerializerHandlerType == null)
            {
                errorMessage = string.Format("The type '{0}' has specified a null argument to the '{1}'", objectToSerialize.GetType(), typeof(SerializerHandlerAttribute));
                return false;
            }

            if (typeof(ISerializerHandler).IsAssignableFrom(serializerHandlerAttribute.SerializerHandlerType) == false)
            {
                errorMessage = string.Format("The type '{0}' has specified a type that does not implement the '{1}' argument to the '{2}'", objectToSerialize.GetType(), typeof(ISerializerHandler), typeof(SerializerHandlerAttribute));
                return false;
            }

            Type serializeHandlerType = serializerHandlerAttribute.SerializerHandlerType;

            ISerializerHandler serializerHandler;
            lock (_lock)
            {
                if (_serializeHandlers.TryGetValue(serializeHandlerType, out serializerHandler) == false)
                {
                    serializerHandler = (ISerializerHandler)Activator.CreateInstance(serializeHandlerType);

                    _serializeHandlers.Add(serializeHandlerType, serializerHandler);
                }
            }

            string serializedObj = serializerHandler.Serialize(objectToSerialize);

            StringBuilder sb = new StringBuilder();
            StringConversionServices.SerializeKeyValuePair(sb, "SerializerHandlerType", TypeManager.SerializeType(serializerHandler.GetType()));
            StringConversionServices.SerializeKeyValuePair(sb, "SerializedObject", serializedObj);

            errorMessage = null;
            serializedObject = sb.ToString();
            return true;
        }



        public object Deserialize(string serializedObject)
        {
            if (serializedObject == null) throw new ArgumentNullException("serializedObject");

            Dictionary<string, string> dic = StringConversionServices.ParseKeyValueCollection(serializedObject);

            if ((dic.ContainsKey("SerializerHandlerType") == false) || (dic.ContainsKey("SerializedObject") == false)) throw new InvalidOperationException("serializedObject is of wrong format");

            string serilizerHandlerTypeString = StringConversionServices.DeserializeValueString(dic["SerializerHandlerType"]);
            Type serilizerHandlerType = TypeManager.GetType(serilizerHandlerTypeString);

            ISerializerHandler serializerHandler;
            lock (_lock)
            {
                if (_serializeHandlers.TryGetValue(serilizerHandlerType, out serializerHandler) == false)
                {
                    serializerHandler = (ISerializerHandler)Activator.CreateInstance(serilizerHandlerType);

                    _serializeHandlers.Add(serilizerHandlerType, serializerHandler);
                }
            }

            string serializedObjectString = StringConversionServices.DeserializeValueString(dic["SerializedObject"]);
            object resultObject = serializerHandler.Deserialize(serializedObjectString);

            return resultObject;
        }



        public void OnFlush()
        {
            _serializeHandlers = new Dictionary<Type, ISerializerHandler>();
        }
	}
}
