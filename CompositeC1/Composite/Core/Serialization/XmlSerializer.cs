/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;


namespace Composite.Core.Serialization
{
    internal sealed class XmlSerializer : IXmlSerializer
    {
        IEnumerable<IValueXmlSerializer> _valueXmlSerializers;



        public XmlSerializer(IEnumerable<IValueXmlSerializer> valueXmlSerializers)
        {
            if (valueXmlSerializers == null) throw new ArgumentNullException("valueXmlSerializers");

            _valueXmlSerializers = valueXmlSerializers.ToList();
        }



        public static IXmlSerializer CreateStandardSerializer()
        {
            return new XmlSerializer(new IValueXmlSerializer[]
                {
                    new SystemPrimitivValueXmlSerializer(),
                    new SystemCollectionValueXmlSerializer()
                });
        }



        public XElement Serialize(Type objectToSerializeType, object objectToSerialize)
        {
            if (objectToSerializeType == typeof(object)) 
            {
                if (objectToSerialize != null)
                {
                    objectToSerializeType = objectToSerialize.GetType();
                }
                else
                {
                    return new XElement("Null");
                }
            }


            XElement serializedObject;
            foreach (IValueXmlSerializer valueXmlSerializer in _valueXmlSerializers)
            {
                if (valueXmlSerializer.TrySerialize(objectToSerializeType, objectToSerialize, this, out serializedObject))
                {
                    return serializedObject;
                }
            }

            throw new InvalidOperationException(string.Format("Could not serialize object of type '{0}'", objectToSerializeType));
        }



        public object Deserialize(XElement serializedObject)
        {
            if (serializedObject.Name.LocalName == "Null")
            {
                return null;
            }

            object deserializedObject;
            foreach (IValueXmlSerializer valueXmlSerializer in _valueXmlSerializers)
            {
                if (valueXmlSerializer.TryDeserialize(serializedObject, this, out deserializedObject))
                {
                    return deserializedObject;
                }
            }

            string serializedObjectStr = serializedObject.ToString();


            // If necessary, cutting the size, since otherwise it may lead to megabyte long exception text
            if(serializedObjectStr.Length > 10000)
            {
                serializedObjectStr = serializedObjectStr.Substring(0, 10000);
            }
            throw new InvalidOperationException(string.Format("Could not deserialize '{0}'", serializedObjectStr));
        }
    }
}
