/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Threading;

namespace Composite.Core.Threading
{
    /// <summary>
    /// Will set the threads Culture and reset it to the original value when this is disposed of.
    /// <example>
    /// using( var cultureScope = new ThreadCultureScope( new CultureInfo("da-DK") ) )
    /// {
    ///   // Code here will run in da-DK scope. 
    ///   // Culture in effect before the using statement will be reset when exiting the using.
    /// }
    /// </example>
    /// </summary>
    public sealed class ThreadCultureScope : IDisposable
    {
        private CultureInfo _originalCulture = null;
        private CultureInfo _originalUiCulture = null;
        private CultureInfo _desiredCulture = null;
        private CultureInfo _desiredUiCulture = null;
        private bool _disposed = false;

        /// <summary>
        /// Constructs a new culture scope, setting the threads CurrentCulture and resetting when this is disposed. The CurrentUiCulture is not affected.
        /// </summary>
        /// <param name="culture">Desired culture to be in effect</param>
        public ThreadCultureScope(CultureInfo culture)
        {
            _originalCulture = Thread.CurrentThread.CurrentCulture;
            _desiredCulture = culture;

            Thread.CurrentThread.CurrentCulture = culture;
        }


        /// <summary>
        /// Constructs a new culture scope, setting the threads CurrentCulture and CurrentUiCulture and resetting when this is disposed.
        /// </summary>
        /// <param name="culture">Desired culture to be in effect</param>
        /// <param name="uiCulture">Desired UI culture to be in effect</param>
        public ThreadCultureScope(CultureInfo culture, CultureInfo uiCulture)
            : this(culture)
        {
            _originalUiCulture = Thread.CurrentThread.CurrentUICulture;
            _desiredUiCulture = uiCulture;

            Thread.CurrentThread.CurrentUICulture = uiCulture;
        }


        /// <summary>
        /// The culture this class was constructed to use
        /// </summary>
        public CultureInfo Culture
        {
            get { return _desiredCulture; }
        }


        /// <summary>
        /// The UI culture this class was constructed to use or null if none were specified.
        /// </summary>
        public CultureInfo UiCulture
        {
            get { return _desiredUiCulture; }
        }

        
        /// <summary>
        /// Return thread culture settings to their original values.
        /// </summary>
        public void Dispose()
        {
            if (!_disposed)
            {
                if (_originalCulture!=null)
                    Thread.CurrentThread.CurrentCulture = _originalCulture;

                if (_originalUiCulture != null)
                    Thread.CurrentThread.CurrentUICulture = _originalUiCulture;

                _disposed = true;                
            }
        }
    }
}
