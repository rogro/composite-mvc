/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Threading;
using Composite.Core.Collections.Generic;
using System;


namespace Composite.Core.Threading
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class ThreadDataManagerData: IDisposable
    {
        /// <exclude />
        public ThreadDataManagerData Parent { get; set; }
        private Hashtable<object, object> Data { get; set; }
        private bool _disposed = false;


        /// <exclude />
        public ThreadDataManagerData()
            : this(null)
        {
        }

        internal delegate void OnThreadDataDisposedDelegate();

        /// <exclude />
        public event ThreadStart OnDispose;

        /// <exclude />
        public ThreadDataManagerData(ThreadDataManagerData parentThreadData)
        {
            this.Parent = parentThreadData;
            this.Data = new Hashtable<object, object>();
        }



        /// <summary>
        /// This method will find the first one that contains the key and return the value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool TryGetParentValue(object key, out object value)
        {
            CheckNotDisposed();

            ThreadDataManagerData current = this;
            while ((current != null) && (current.Data.ContainsKey(key) == false))
            {
                current = current.Parent;
            }

            if (current == null)
            {
                value = null;
                return false;
            }

            value = current.Data[key];
            return true;
        }


        /// <exclude />
        public void SetValue(object key, object value)
        {
            CheckNotDisposed();

            if (this.Data.ContainsKey(key) == false)
            {
                this.Data.Add(key, value);
            }
            else
            {
                this.Data[key] = value;
            }
        }

        /// <exclude />
        public object GetValue(object key)
        {
            CheckNotDisposed();

            return Data[key];
        }

        /// <exclude />
        public bool HasValue(object key)
        {
            CheckNotDisposed();

            return this.Data.ContainsKey(key);
        }

        /// <exclude />
        public object this[object key]
        {
            get { return GetValue(key); }
        }

        /// <exclude />
        public void CheckNotDisposed()
        {
            if(_disposed) throw new ObjectDisposedException("TheadDataManagerData");
        }

        #region IDisposable Members


        /// <exclude />
        public void Dispose()
        {
            if (OnDispose != null)
            {
                OnDispose();
            }
            _disposed = true;
        }

        #endregion
    }
}
