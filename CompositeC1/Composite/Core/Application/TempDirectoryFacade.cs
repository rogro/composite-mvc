/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.IO;
using Composite.Core.Configuration;
using Composite.Core.IO;
using Composite.Core.WebClient;


namespace Composite.Core.Application
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class TempDirectoryFacade
    {
        private static TimeSpan TemporaryFileExpirationTimeSpan = TimeSpan.FromHours(24.0);

        /// <exclude />
        public static string CreateTempDirectory()
        {
            string directory = Path.Combine(TempDirectoryPath, UrlUtils.CompressGuid(Guid.NewGuid()));

            C1Directory.CreateDirectory(directory);

            return directory;
        }


        internal static string GetTempFileName(string extension)
        {
            return Path.Combine(TempDirectoryPath, UrlUtils.CompressGuid(Guid.NewGuid()).Substring(0, 8)) + (extension ?? "");
        }

        /// <exclude />
        public static void OnApplicationStart()
        {
            string tempDirectoryName = TempDirectoryPath;

            if (!C1Directory.Exists(tempDirectoryName))
            {
                C1Directory.CreateDirectory(tempDirectoryName);
            }
        }


        /// <exclude />
        public static void OnApplicationEnd()
        {
            // Deleting everything that is older than 24 hours
            string tempDirectoryName = TempDirectoryPath;

            if (!C1Directory.Exists(tempDirectoryName))
            {
                return;
            }

            

            foreach (string filename in C1Directory.GetFiles(tempDirectoryName))
            {
                try
                {
                    if (DateTime.Now > C1File.GetLastWriteTime(filename) + TemporaryFileExpirationTimeSpan)
                    {
                        C1File.Delete(filename);
                    }
                }
                catch 
                {
                }
            }

            foreach (string directoryPath in C1Directory.GetDirectories(tempDirectoryName))
            {
                try
                {
                    if (DateTime.Now > C1Directory.GetCreationTime(directoryPath) + TemporaryFileExpirationTimeSpan)
                    {
                        C1Directory.Delete(directoryPath, true);
                    }
                }
                catch 
                {
                }
            }
        }

        internal static string TempDirectoryPath
        {
            get { return PathUtil.Resolve(GlobalSettingsFacade.TempDirectory); }
        }
    }
}
