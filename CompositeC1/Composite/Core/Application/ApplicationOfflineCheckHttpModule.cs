/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Web;
using Composite.Core.Extensions;
using Composite.Core.IO;


namespace Composite.Core.Application
{
	internal class ApplicationOfflineCheckHttpModule: IHttpModule
	{
	    private static bool _isOffline;
        private static string _responceHtml;

        public void Init(HttpApplication context)
        {
            context.BeginRequest += HttpApplication_BeginRequest;
        }

        protected virtual void HttpApplication_BeginRequest(object sender, EventArgs e)
        {
            var context = ((HttpApplication)sender).Context;

            if (!IsOffline ||
                context.Request.FilePath.EndsWith("/Composite/services/LogService/LogService.svc", StringComparison.OrdinalIgnoreCase))
            {
                return;
            }

            context.Response.Clear();
            context.Response.Write(_responceHtml);
            context.Response.StatusCode = 480; /* Temporary unavailable*/
            context.Response.Flush();

            context.ApplicationInstance.CompleteRequest();
        }

	    public static bool IsOffline
	    {
	        get
	        {
	            return _isOffline;
	        }
            set
            {
                _isOffline = value;
                if(!_isOffline)
                {
                    return;
                }

                string filePath = FilePath;
                Verify.That(!filePath.IsNullOrEmpty(), "Path to 'app_offline.html' has not been set");
                try
                {
                    _responceHtml = C1File.ReadAllText(filePath);
                }
                catch(Exception e)
                {
                    _responceHtml = string.Empty;
                    Core.Logging.LoggingService.LogWarning("Failed to load file '{0}'".FormatWith(filePath), e);
                }
            }

	    }

        public static string FilePath { get; set; }

        public void Dispose()
        {
        }
    }
}
