/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;

namespace Composite.Core.Application
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class ApplicationOnlineHandlerFacade
    {
        private static IApplicationOnlineHandlerFacade _applicationOnlineHandlerFacade = new ApplicationOnlineHandlerFacadeImpl();


        internal static IApplicationOnlineHandlerFacade Implementation { get { return _applicationOnlineHandlerFacade; } set { _applicationOnlineHandlerFacade = value; } }



        /// <summary>
        /// Turns application offline
        /// </summary>
        /// <param name="softTurnOff">
        /// Setting softTurnOff to true will only make the application offline to the client, but 
        /// not actually turning off the application. Setting this to false will turn off the 
        /// application.
        /// </param>
        public static void TurnApplicationOffline(bool softTurnOff)
        {
            _applicationOnlineHandlerFacade.TurnApplicationOffline(softTurnOff, true);
        }



        /// <exclude />
        public static void TurnApplicationOffline(bool softTurnOff, bool recompileCompositeGenerated)
        {
            _applicationOnlineHandlerFacade.TurnApplicationOffline(softTurnOff, recompileCompositeGenerated);
        }


        /// <exclude />
        public static bool CanPutApplicationOffline(bool softTurnOff, out string errorMessage)
        {
            return _applicationOnlineHandlerFacade.CanPutApplicationOffline(softTurnOff, out errorMessage);
        }


        /// <exclude />
        public static void TurnApplicationOnline()
        {
            _applicationOnlineHandlerFacade.TurnApplicationOnline();
        }



        /// <exclude />
        public static IDisposable TurnOffScope(bool softTurnOff)
        {
            TurnApplicationOffline(softTurnOff);
            return new TurnOffToken();
        }


        /// <exclude />
        public static bool IsApplicationOnline
        {
            get
            {
                return _applicationOnlineHandlerFacade.IsApplicationOnline;
            }
        }



        private sealed class TurnOffToken : IDisposable
        {
            public void Dispose()
            {
                ApplicationOnlineHandlerFacade.TurnApplicationOnline();
            }
        }
    }
}
