/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Reflection;
using System.Web;
using System.Web.Hosting;

namespace Composite.Core.Application
{
    /// <summary>
    /// Postpones raising of shutdown event for ASP.NET
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public class ShutdownGuard : IDisposable
    {
        private readonly HttpRuntime _runtime;
        private readonly FieldInfo _shutdownWebEventRaised_FieldInfo;

        private readonly HostingEnvironment _hostingEnvironment;
        private readonly FieldInfo _shutdownInitiated_FieldInfo;

        //private readonly object _fileChangesManager;
        //private readonly FieldInfo _callbackFieldInfo;
        //private readonly FieldInfo _isFCNDisabledFieldInfo;
        //private readonly object _savedCallbackValue;


        /// <exclude />
        public ShutdownGuard()
        {
            if (!HostingEnvironment.IsHosted)
            {
                return;
            }

            const BindingFlags getStaticFieldValue = BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.GetField;
            _runtime = (HttpRuntime)typeof(HttpRuntime).InvokeMember("_theRuntime", getStaticFieldValue, null, null, null);
            _hostingEnvironment = (HostingEnvironment)typeof(HostingEnvironment).InvokeMember("_theHostingEnvironment", getStaticFieldValue, null, null, null);


            const BindingFlags privateField = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetField;
            _shutdownWebEventRaised_FieldInfo = typeof(HttpRuntime).GetField("_shutdownWebEventRaised", privateField);

            // In .NET 3.5 the field is called "_shutdownInitated"
            //    .NET 4.0 the field is called "_shutdownInitiated"
            _shutdownInitiated_FieldInfo = typeof(HostingEnvironment).GetField("_shutdownInitiated", privateField)
                                        ?? typeof(HostingEnvironment).GetField("_shutdownInitated",  privateField);

            // Simulating situation, when all events to unload current AppDomain were already raised.
            _shutdownWebEventRaised_FieldInfo.SetValue(_runtime, true);
            _shutdownInitiated_FieldInfo.SetValue(_hostingEnvironment, true);



            //_fileChangesManager = runtime.GetType().GetField("_fcm", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetField).GetValue(runtime);

            //_callbackFieldInfo = _fileChangesManager.GetType().GetField("_callbackRenameOrCriticaldirChange",
            //                                                                    BindingFlags.NonPublic |
            //                                                                    BindingFlags.Instance |
            //                                                                    BindingFlags.GetField);

            //_isFCNDisabledFieldInfo = _fileChangesManager.GetType().GetField("_FCNMode",
            //                                                                    BindingFlags.NonPublic |
            //                                                                    BindingFlags.Instance |
            //                                                                    BindingFlags.GetField);

            //_savedCallbackValue = _callbackFieldInfo.GetValue(_fileChangesManager);
            //_callbackFieldInfo.SetValue(_fileChangesManager, null);

            //// Turning off file change notifications. http://support.microsoft.com/kb/911272
            //_isFCNDisabledFieldInfo.SetValue(_fileChangesManager, (Int32)1);


        }



        /// <exclude />
        public void Dispose()
        {
            if (!HostingEnvironment.IsHosted)
            {
                return;
            }

            _shutdownWebEventRaised_FieldInfo.SetValue(_runtime, false);
            _shutdownInitiated_FieldInfo.SetValue(_hostingEnvironment, false);

            //_callbackFieldInfo.SetValue(_fileChangesManager, _savedCallbackValue);
            //_isFCNDisabledFieldInfo.SetValue(_fileChangesManager, (Int32)0);
        }
    }
}
