/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Threading;
using System.ComponentModel;


namespace Composite.Core.Application
{
    /// <summary>
    /// This is a OS sytem wide named semaphore.
    /// </summary>
    [EditorBrowsable(EditorBrowsableState.Never)]
    internal class SystemGlobalSemaphore
    {
        private readonly string _id;
        private readonly Semaphore _eventWaitHandle;


        /// <summary>
        /// </summary>
        /// <param name="id">The system wide id of the semaphore</param>
        public SystemGlobalSemaphore(string id)
        {
            _id = id;
            if (!_id.StartsWith(@"Global\", StringComparison.InvariantCultureIgnoreCase))
            {
                _id = @"Global\" + _id;
            }

            _eventWaitHandle = new Semaphore(1, 1, _id);
        }



        /// <summary>
        /// Enter the semaphore. Blocking. Returns false if entering the semaphore failed due to timeout.
        /// </summary>
        /// <param name="timeout">Timeout in milliseconds</param>
        /// <param name="throwOnTimeout">If this is true, the method will throw an excepion on timeout</param>
        /// <returns>True if entering successed. False if entering timed out.</returns>
        public bool Enter(int timeout, bool throwOnTimeout = false)
        {
            bool entered = _eventWaitHandle.WaitOne(timeout);

            if (!entered && throwOnTimeout) throw new TimeoutException(string.Format("Failed to obtain the system global semaphore with id '{0}'", _id));

            return entered;
        }


        /// <summary>
        /// Leave the semaphore.
        /// </summary>
        public void Leave()
        {
            _eventWaitHandle.Release();
        }



        /// <summary>
        /// The used id used for naming the semaphore.
        /// </summary>
        public string Id
        {
            get
            {
                return _id;
            }
        }
    }
}
