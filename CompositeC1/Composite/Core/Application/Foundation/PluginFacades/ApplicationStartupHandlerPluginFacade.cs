/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;
using System.Configuration;
using Composite.Core.Application.Plugins.ApplicationOnlineHandler.Runtime;
using Composite.Core.Application.Plugins.ApplicationStartupHandler;
using Composite.Core.Application.Plugins.ApplicationStartupHandler.Runtime;
using Composite.Core.Collections.Generic;


namespace Composite.Core.Application.Foundation.PluginFacades
{
    internal static class ApplicationStartupHandlerPluginFacade
    {
        private static ResourceLocker<Resources> _resourceLocker = new ResourceLocker<Resources>(new Resources(), Resources.Initialize);


        public static void OnBeforeInitialize(string handlerName)
        {
            Verify.ArgumentNotNullOrEmpty(handlerName, handlerName);

            using (_resourceLocker.Locker)
            {
                IApplicationStartupHandler provider = GetApplicationStartupHandler(handlerName);

                provider.OnBeforeInitialize();
            }
        }

        public static void OnInitialized(string handlerName)
        {
            Verify.ArgumentNotNullOrEmpty(handlerName, handlerName);

            using (_resourceLocker.Locker)
            {
                IApplicationStartupHandler provider = GetApplicationStartupHandler(handlerName);

                provider.OnInitialized();
            }
        }



        private static IApplicationStartupHandler GetApplicationStartupHandler(string handlerName)
        {
            IApplicationStartupHandler applicationStartupHandler;

            using (_resourceLocker.Locker)
            {
                if (_resourceLocker.Resources.ProviderCache.TryGetValue(handlerName, out applicationStartupHandler) == false)
                {
                    try
                    {
                        applicationStartupHandler = _resourceLocker.Resources.Factory.Create(handlerName);

                        _resourceLocker.Resources.ProviderCache.Add(handlerName, applicationStartupHandler);
                    }
                    catch (ArgumentException ex)
                    {
                        HandleConfigurationError(ex);
                    }
                    catch (ConfigurationErrorsException ex)
                    {
                        HandleConfigurationError(ex);
                    }
                }
            }

            return applicationStartupHandler;
        }



        private static void Flush()
        {
            _resourceLocker.ResetInitialization();
        }


        private static void HandleConfigurationError(Exception ex)
        {
            Flush();

            throw new ConfigurationErrorsException(string.Format("Failed to load the configuration section '{0}' from the configuration.", ApplicationOnlineHandlerSettings.SectionName), ex);
        }



        private sealed class Resources
        {
            public ApplicationStartupHandlerFactory Factory { get; set; }
            public Dictionary<string, IApplicationStartupHandler> ProviderCache { get; set; }

            public static void Initialize(Resources resources)
            {
                try
                {
                    resources.Factory = new ApplicationStartupHandlerFactory();
                }
                catch (NullReferenceException ex)
                {
                    HandleConfigurationError(ex);
                }
                catch (ConfigurationErrorsException ex)
                {
                    HandleConfigurationError(ex);
                }

                resources.ProviderCache = new Dictionary<string, IApplicationStartupHandler>();
            }
        }
    }
}
