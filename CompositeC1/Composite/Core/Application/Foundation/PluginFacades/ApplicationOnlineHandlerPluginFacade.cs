/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Configuration;
using Composite.Core.Application.Plugins.ApplicationOnlineHandler;
using Composite.Core.Application.Plugins.ApplicationOnlineHandler.Runtime;
using Composite.Core.Collections.Generic;
using Composite.C1Console.Events;


namespace Composite.Core.Application.Foundation.PluginFacades
{
    internal static class ApplicationOnlineHandlerPluginFacade
    {
        private static ResourceLocker<Resources> _resourceLocker = new ResourceLocker<Resources>(new Resources(), Resources.Initialize);



        static ApplicationOnlineHandlerPluginFacade()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlushEvent);
        }



        public static void TurnApplicationOffline()
        {
            IApplicationOnlineHandler applicationOnlineHandler = GetApplicationOnlineHandler();

            applicationOnlineHandler.TurnApplicationOffline();
        }



        public static void TurnApplicationOnline()
        {
            IApplicationOnlineHandler applicationOnlineHandler = GetApplicationOnlineHandler();

            applicationOnlineHandler.TurnApplicationOnline();
        }



        public static bool IsApplicationOnline()
        {
            IApplicationOnlineHandler applicationOnlineHandler = GetApplicationOnlineHandler();

            return applicationOnlineHandler.IsApplicationOnline();
        }



        public static bool CanPutApplicationOffline(out string errorMessage)
        {
            return GetApplicationOnlineHandler().CanPutApplicationOffline(out errorMessage);
        }



        private static IApplicationOnlineHandler GetApplicationOnlineHandler()
        {
            IApplicationOnlineHandler handler = null;

            using (_resourceLocker.Locker)
            {
                if (_resourceLocker.Resources.HandlerCache == null)
                {
                    try
                    {
                        handler = _resourceLocker.Resources.Factory.CreateDefault();

                        _resourceLocker.Resources.HandlerCache = handler;
                    }
                    catch (ArgumentException ex)
                    {
                        HandleConfigurationError(ex);
                    }
                    catch (ConfigurationErrorsException ex)
                    {
                        HandleConfigurationError(ex);
                    }
                }
                else
                {
                    handler = _resourceLocker.Resources.HandlerCache;
                }
            }

            return handler;
        }



        private static void Flush()
        {
            _resourceLocker.ResetInitialization();
        }



        private static void OnFlushEvent(FlushEventArgs args)
        {
            Flush();
        }



        private static void HandleConfigurationError(Exception ex)
        {
            Flush();

            throw new ConfigurationErrorsException(string.Format("Failed to load the configuration section '{0}' from the configuration.", ApplicationOnlineHandlerSettings.SectionName), ex);
        }



        private sealed class Resources
        {
            public ApplicationOnlineHandlerFactory Factory { get; set; }
            public IApplicationOnlineHandler HandlerCache { get; set; }

            public static void Initialize(Resources resources)
            {
                try
                {
                    resources.Factory = new ApplicationOnlineHandlerFactory();
                }
                catch (NullReferenceException ex)
                {
                    HandleConfigurationError(ex);
                }
                catch (ConfigurationErrorsException ex)
                {
                    HandleConfigurationError(ex);
                }

                resources.HandlerCache = null;
            }
        }
    }
}
