/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Threading;
using System.Web.Hosting;
using Composite.C1Console.Events;
using Composite.Core.Application.Foundation.PluginFacades;
using Composite.Core.Types;


namespace Composite.Core.Application
{
    internal class ApplicationOnlineHandlerFacadeImpl : IApplicationOnlineHandlerFacade
    {
        private static readonly string LogTitle = typeof (ApplicationOnlineHandlerFacadeImpl).Name;

        private bool _isApplicationOnline = true;
        private bool _wasLastTurnOffSoft = false;
        private bool _recompileCompositeGenerated;
        private ShutdownGuard _shutdownGuard;


        public void TurnApplicationOffline(bool softTurnOff, bool recompileCompositeGenerated)
        {
            Verify.IsTrue(this.IsApplicationOnline, "The application is already offline");

            Log.LogVerbose("ApplicationOnlineHandlerFacade", string.Format("Turning off the application ({0})", softTurnOff ? "Soft" : "Hard"));


            _recompileCompositeGenerated = recompileCompositeGenerated;

            _shutdownGuard = new ShutdownGuard();

            try
            {
                if (softTurnOff == false)
                {
                    ApplicationOnlineHandlerPluginFacade.TurnApplicationOffline();
                    Verify.IsFalse(ApplicationOnlineHandlerPluginFacade.IsApplicationOnline(), "Plugin failed to turn the application offline");
                }
                else
                {
                    ConsoleMessageQueueFacade.Enqueue(new LockSystemConsoleMessageQueueItem(), "");
                }
            }
            catch(Exception)
            {
                _shutdownGuard.Dispose();
                _shutdownGuard = null;

                throw;
            }

            _isApplicationOnline = false;
            _wasLastTurnOffSoft = softTurnOff;
        }



        public void TurnApplicationOnline()
        {
            Verify.IsFalse(this.IsApplicationOnline, "The application is already online");

            Log.LogVerbose("ApplicationOnlineHandlerFacade", "Turning on the application");

            if (_recompileCompositeGenerated)
            {
                try
                {
                    CodeGenerationManager.GenerateCompositeGeneratedAssembly();
                }
                catch (Exception ex)
                {
                    Log.LogError(LogTitle, "Failed to recompile Composite.Generated.dll");
                    Log.LogError(LogTitle, ex);
                }
            }            

            try
            {
                if (_wasLastTurnOffSoft == false)
                {
                    ApplicationOnlineHandlerPluginFacade.TurnApplicationOnline();
                    Verify.IsTrue(ApplicationOnlineHandlerPluginFacade.IsApplicationOnline(), "Plugin failed to turn the application online");
                }
            }
            finally
            {
                // Adding a sleep, so delayed notification from FileWatcher will not kill a newly spawned AppDomain
                Thread.Sleep(250);

                _shutdownGuard.Dispose();
                _shutdownGuard = null;

                if (HostingEnvironment.IsHosted)
                {
                    HostingEnvironment.InitiateShutdown();
                }
            }

            _isApplicationOnline = true;
        }



        public bool IsApplicationOnline
        {
            get
            {
                return _isApplicationOnline;
            }
        }


        public bool CanPutApplicationOffline(bool softTurnOff, out string errorMessage)
        {
            if(softTurnOff)
            {
                errorMessage = null;
                return true;
            }

            return ApplicationOnlineHandlerPluginFacade.CanPutApplicationOffline(out errorMessage);
        }
    }
}
