/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;


namespace Composite.Core.Extensions
{
	internal static class ByteArrayExtensionMethods
	{
        public static IEnumerable<byte[]> Split(this byte[] bytes, byte splitByte)
        {
            int lastIndex = 0;

            int index = IndexOf(bytes, splitByte, lastIndex);
            while (index != -1)
            {
                int size = index - lastIndex;
                byte[] subBytes = new byte[size];

                Array.Copy(bytes, lastIndex, subBytes, 0, size);

                lastIndex = index + 1;
                index = IndexOf(bytes, splitByte, lastIndex);

                yield return subBytes;
            }

            if (lastIndex < bytes.Length)
            {
                int size = bytes.Length - lastIndex;
                byte[] subBytes = new byte[size];

                Array.Copy(bytes, lastIndex, subBytes, 0, size);

                yield return subBytes;
            }
        }



        public static int IndexOf(this byte[] bytes, byte byteToFind)
        {
            return IndexOf(bytes, byteToFind, 0);
        }



        public static int IndexOf(this byte[] bytes, byte byteToFind, int startIndex)
        {
            if (bytes == null) throw new ArgumentNullException("bytes");
            if (startIndex > bytes.Length) throw new ArgumentNullException("startIndex exceeds the length of the bytes");

            for (int i = startIndex; i < bytes.Length; i++)
            {
                if (bytes[i] == byteToFind) return i;
            }

            return -1;
        }
	}
}
