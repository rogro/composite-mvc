/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Reflection;
using System.Text;


namespace Composite.Core.Extensions
{
    internal static class MethodInfoExtensionMethods
    {
        public static string ToExceptionString(this MethodInfo methodInfo)
        {
            if (methodInfo == null) throw new ArgumentNullException("methodInfo");

            StringBuilder sb = new StringBuilder();
            sb.Append(methodInfo.DeclaringType.FullName);
            sb.Append(".");
            sb.Append(methodInfo.Name);

            Type[] genericArguments = methodInfo.GetGenericArguments();
            if (genericArguments.Length > 0)
            {
                sb.Append("[");
                bool firstGenericParameter = true;
                foreach (Type type in genericArguments)
                {
                    if (firstGenericParameter == false)
                    {
                        sb.Append(", ");
                    }
                    else
                    {
                        firstGenericParameter = false;
                    }

                    sb.Append(type.Name);
                }
                sb.Append("]");
            }

            sb.Append("(");

            bool firstParameter = true;
            foreach (ParameterInfo parameterInfo in methodInfo.GetParameters())
            {
                if (firstParameter == false)
                {
                    sb.Append(", ");
                }
                else
                {
                    firstParameter = false;
                }

                sb.Append(parameterInfo.ParameterType.Name);
                sb.Append(" ");
                sb.Append(parameterInfo.Name);
            }

            sb.Append(")");

            return sb.ToString();
        }
    }
}
