/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;


namespace Composite.Core.Logging
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
	public sealed class DebugLoggingScope : IDisposable
	{
        private static IDisposable _noActionDisposable = new NoActionDisposable();

        /// <exclude />
        public static IDisposable CompletionTime( Type callingType, string actionInfo )
        {
            if (RuntimeInformation.IsDebugBuild)
            {
                return new DebugLoggingScope(callingType.Name, actionInfo, false, TimeSpan.MinValue);
            }
            else
            {
                return _noActionDisposable;
            }
        }


        /// <exclude />
        public static IDisposable CompletionTime(Type callingType, string actionInfo, TimeSpan loggingThreshold)
        {
            if (RuntimeInformation.IsDebugBuild)
            {
                return new DebugLoggingScope(callingType.Name, actionInfo, false, loggingThreshold);
            }
            else
            {
                return _noActionDisposable;
            }
        }


        /// <exclude />
        public static IDisposable MethodInfoScope
        {
            get
            {
                if (RuntimeInformation.IsDebugBuild)
                {
                    StackTrace stackTrace = new StackTrace();
                    StackFrame stackFrame = stackTrace.GetFrame(1);
                    string scopeName = string.Format("{0}.{1}", stackFrame.GetMethod().DeclaringType.Name, stackFrame.GetMethod().Name);

                    return new DebugLoggingScope(scopeName, "Method", true, TimeSpan.MinValue);
                }
                else
                {
                    return _noActionDisposable;
                }
            }
        }


        private int _startTickCount;
        private string _scopeName;
        private string _actionInfo;
        private TimeSpan _threshold;

        private DebugLoggingScope(string scopeName, string actionInfo, bool logStart, TimeSpan threshold)
        {
            _startTickCount = Environment.TickCount;
            _scopeName = scopeName;
            _actionInfo = actionInfo;
            _threshold = threshold;

            if (logStart==true)
            {
                LoggingService.LogVerbose(_scopeName, string.Format("Starting {0}", _actionInfo));
            }
        }


        /// <exclude />
        public void Dispose()
        {
            int endTickCount = Environment.TickCount;
            if ((endTickCount - _startTickCount) >= _threshold.Milliseconds)
            {
                LoggingService.LogVerbose(_scopeName, string.Format("Finished {0} ({1} ms)", _actionInfo, endTickCount - _startTickCount));
            }
        }


        private class NoActionDisposable : IDisposable
        {
            public void Dispose()
            {
            }
        }

    }
}
