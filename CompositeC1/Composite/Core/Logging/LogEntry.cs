/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Globalization;
using System.Runtime.Serialization;
using System.Text;
using System.Diagnostics;


namespace Composite.Core.Logging
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    public sealed class LogEntry
    {
        const char NonBreakingSpace = (char)160;
        private static readonly string DateTimeFormat = "yyyyMMdd" + NonBreakingSpace + "HH:mm:ss.ffff";

        /// <exclude />
        public DateTime TimeStamp { get; set; }

        /// <exclude />
        public int ApplicationDomainId { get; set; }

        /// <exclude />
        public int ThreadId { get; set; }

        /// <exclude />
        public string Severity { get; set; }

        /// <exclude />
        public string Title { get; set; }

        /// <exclude />
        public string DisplayOptions { get; set; }

        /// <exclude />
        public string Message { get; set; }


        /// <exclude />
        public override string ToString()
        {
            int applicationDomainId = AppDomain.CurrentDomain.Id;
            int threadId = System.Threading.Thread.CurrentThread.ManagedThreadId;

            var sb = new StringBuilder();
            sb.Append(TimeStamp.ToString(DateTimeFormat)); // It has one NonBreakingSpace inside
            sb.Append(NonBreakingSpace).Append(applicationDomainId);
            sb.Append(NonBreakingSpace).Append(threadId < 10 ? " " : string.Empty).Append(threadId);
            sb.Append(NonBreakingSpace).Append(Severity);
            sb.Append(NonBreakingSpace).Append(Title);
            sb.Append(NonBreakingSpace).Append(DisplayOptions);
            sb.Append(NonBreakingSpace).Append(Message/*.Replace("\n", @"\n")*/);

            return sb.ToString();
        }



        /// <exclude />
        [DebuggerStepThrough]
        public static LogEntry Parse(string serializedLogEntry)
        {
            Verify.ArgumentNotNull(serializedLogEntry, "serializedLogEntry");
            if(serializedLogEntry.IndexOf((char)65533) == -1
                && serializedLogEntry.IndexOf((char)160) == -1)
            {
                return null;
            }

            string[] parts = serializedLogEntry.Split((char)65533);

            if(parts.Length != 8)
            {
                parts = serializedLogEntry.Split((char)160);
            }

            if(parts.Length != 8)
            {
                return null;
            }

            var result = new LogEntry();

            try
            {
                string date = parts[0] + parts[1];

                DateTime timeStamp;

                if (!DateTime.TryParseExact(date, "yyyyMMddHH:mm:ss.ffff", 
                                            CultureInfo.InvariantCulture.DateTimeFormat, DateTimeStyles.None, 
                                            out timeStamp))
                {
                    return null;
                }

                result.TimeStamp = timeStamp;

                result.ApplicationDomainId = int.Parse(parts[2]);
                result.ThreadId = int.Parse(parts[3]);
                result.Severity = parts[4];
                result.Title = parts[5];
                result.DisplayOptions = parts[6];
                result.Message = parts[7];
            }
            catch(Exception)
            {
                return null;
            }

            return result;
        }
    }
}
