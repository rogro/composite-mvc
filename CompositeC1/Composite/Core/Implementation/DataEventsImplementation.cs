/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Globalization;

using Composite.Data;


namespace Composite.Core.Implementation
{
    /// <summary>
    /// Implementation pending
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    public class DataEventsImplementation<TData>
        where TData : class, IData
    {
        /// <summary>
        /// Implementation pending
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly", Justification = "We had to be backwards compatible")]
        public virtual event DataEventHandler OnBeforeAdd 
        { 
            add 
            {
                DataEventSystemFacade.SubscribeToDataBeforeAdd<TData>(value, true);
            } 
            remove 
            {
                DataEventSystemFacade.UnsubscribeToDataBeforeAdd(typeof(TData), value);
            } 
        }



        /// <summary>
        /// Implementation pending
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly", Justification = "We had to be backwards compatible")]
        public virtual event DataEventHandler OnAfterAdd
        {
            add
            {
                DataEventSystemFacade.SubscribeToDataAfterAdd<TData>(value, true);
            }
            remove
            {
                DataEventSystemFacade.UnsubscribeToDataAfterAdd(typeof(TData), value);
            }
        }



        /// <summary>
        /// Implementation pending
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly", Justification = "We had to be backwards compatible")]
        public virtual event DataEventHandler OnBeforeUpdate
        {
            add
            {
                DataEventSystemFacade.SubscribeToDataBeforeUpdate<TData>(value, true);
            }
            remove
            {
                DataEventSystemFacade.UnsubscribeToDataBeforeUpdate(typeof(TData), value);
            }
        }



        /// <summary>
        /// Implementation pending
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly", Justification = "We had to be backwards compatible")]
        public virtual event DataEventHandler OnAfterUpdate
        {
            add
            {
                DataEventSystemFacade.SubscribeToDataAfterUpdate<TData>(value, true);
            }
            remove
            {
                DataEventSystemFacade.UnsubscribeToDataAfterUpdate(typeof(TData), value);
            }
        }



        /// <summary>
        /// Implementation pending
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly", Justification = "We had to be backwards compatible")]
        public virtual event DataEventHandler OnDeleted
        {
            add
            {
                DataEventSystemFacade.SubscribeToDataDeleted<TData>(value, true);
            }
            remove
            {
                DataEventSystemFacade.UnsubscribeToDataDeleted(typeof(TData), value);
            }
        }



        /// <summary>
        /// Implementation pending
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly", Justification = "We had to be backwards compatible")]
        public virtual event StoreEventHandler OnStoreChanged
        {
            add
            {
                DataEventSystemFacade.SubscribeToStoreChanged<TData>(value, true);
            }
            remove
            {
                DataEventSystemFacade.UnsubscribeToStoreChanged(typeof(TData), value);
            }
        }



        /// <summary>
        /// Implementation pending
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly", Justification = "We had to be backwards compatible")]
        public virtual event DataEventHandler OnNew
        {
            add
            {
                DataEventSystemFacade.SubscribeToDataAfterBuildNew<TData>(value, true);
            }
            remove
            {
                DataEventSystemFacade.UnsubscribeToDataAfterBuildNew(typeof(TData), value);
            }
        }


        /// <summary>
        /// Implementation pending
        /// </summary>
        public virtual void FireExternalStoreChangeEvent(PublicationScope publicationScope, CultureInfo locale)
        {
            DataEventSystemFacade.FireExternalStoreChangedEvent(typeof(TData), publicationScope, locale);
        }
    }    
}
