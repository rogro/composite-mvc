/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Globalization;
using Composite.Core.Extensions;
using Composite.Data;


namespace Composite.Core.Implementation
{
    /// <summary>
    /// Documentation pending
    /// </summary>
    public class DataConnectionBase
    {
        /// <summary>
        /// Documentation pending
        /// </summary>
        protected void InitializeScope()
        {
            this.PublicationScope = Data.PublicationScope.Unpublished;
            this.DataScopeIdentifier = DataScopeIdentifier.Administrated;
            this.Locale = null;
        }


        /// <summary>
        /// Documentation pending
        /// </summary>
        /// <param name="scope"></param>
        /// <param name="locale"></param>
        protected void InitializeScope(PublicationScope scope, CultureInfo locale)
        {
            this.PublicationScope = scope;
            SetDataScopeIdentifier(scope);
            this.Locale = locale;
        }


        /// <summary>
        /// Documentation pending
        /// </summary>
        protected PublicationScope PublicationScope { get; private set; }

        /// <summary>
        /// Documentation pending
        /// </summary>
        protected DataScopeIdentifier DataScopeIdentifier { get; private set; }

        /// <summary>
        /// Documentation pending
        /// </summary>
        protected CultureInfo Locale { get; private set; }


        private void SetDataScopeIdentifier(PublicationScope scope)
        {
            switch (scope)
            {
                case PublicationScope.Published:
                    this.DataScopeIdentifier = DataScopeIdentifier.Public;
                    break;

                case PublicationScope.Unpublished:
                    this.DataScopeIdentifier = DataScopeIdentifier.Administrated;
                    break;

                default:
                    throw new ArgumentException("PublicationScope {0} not supported".FormatWith(scope), "scope");
            }
        }
    }
}
