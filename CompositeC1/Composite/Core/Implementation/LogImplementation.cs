/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Globalization;
using Composite.Core.Logging;


namespace Composite.Core.Implementation
{
    /// <summary>
    /// Implementation pending
    /// </summary>
    public class LogImplementation
    {
        /// <summary>
        /// Stateless constructor. This is used when implementations of static methods needs to be called        
        /// </summary>
        public LogImplementation()
        {
        }


        /// <summary>
        /// Implementation pending
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public virtual void LogInformation(string title, string message)
        {
            LoggingService.LogInformation(title, message);
        }



        /// <summary>
        /// Implementation pending
        /// </summary>
        /// <param name="title"></param>
        /// <param name="messageFormat"></param>
        /// <param name="args"></param>
        public virtual void LogInformation(string title, string messageFormat, params object[] args) 
        {
            LoggingService.LogInformation(title, string.Format(CultureInfo.InvariantCulture, messageFormat, args));
        }



        /// <summary>
        /// Implementation pending
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public virtual void LogVerbose(string title, string message)
        {
            LoggingService.LogVerbose(title, message);
        }



        /// <summary>
        /// Implementation pending
        /// </summary>
        /// <param name="title"></param>
        /// <param name="messageFormat"></param>
        /// <param name="args"></param>
        public virtual void LogVerbose(string title, string messageFormat, params object[] args) 
        {
            LoggingService.LogVerbose(title, string.Format(CultureInfo.InvariantCulture, messageFormat, args));
        }



        /// <summary>
        /// Implementation pending
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public virtual void LogWarning(string title, string message)
        {
            LoggingService.LogWarning(title, message);
        }



        /// <summary>
        /// Implementation pending
        /// </summary>
        /// <param name="title"></param>
        /// <param name="messageFormat"></param>
        /// <param name="args"></param>
        public virtual void LogWarning(string title, string messageFormat, params object[] args) 
        {
            LogWarning(title, string.Format(CultureInfo.InvariantCulture, messageFormat, args));
        }



        /// <summary>
        /// Implementation pending
        /// </summary>
        /// <param name="title"></param>
        /// <param name="exception"></param>
        public virtual void LogWarning(string title, Exception exception) 
        {
            LoggingService.LogWarning(title, exception);
        }



        /// <summary>
        /// Implementation pending
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public virtual void LogError(string title, string message)
        {
            LoggingService.LogError(title, message);
        }



        /// <summary>
        /// Implementation pending
        /// </summary>
        /// <param name="title"></param>
        /// <param name="messageFormat"></param>
        /// <param name="args"></param>
        public virtual void LogError(string title, string messageFormat, params object[] args) 
        {
            LogError(title, string.Format(CultureInfo.InvariantCulture, messageFormat, args));
        }



        /// <summary>
        /// Implementation pending
        /// </summary>
        /// <param name="title"></param>
        /// <param name="exception"></param>
        public virtual void LogError(string title, Exception exception) 
        {
            LoggingService.LogError(title, exception);
        }



        /// <summary>
        /// Implementation pending
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public virtual void LogCritical(string title, string message)
        {
            LoggingService.LogCritical(title, message);
        }



        /// <summary>
        /// Implementation pending
        /// </summary>
        /// <param name="title"></param>
        /// <param name="messageFormat"></param>
        /// <param name="args"></param>
        public virtual void LogCritical(string title, string messageFormat, params object[] args) 
        {
            LoggingService.LogCritical(title, string.Format(CultureInfo.InvariantCulture, messageFormat, args));
        }



        /// <summary>
        /// Implementation pending
        /// </summary>
        /// <param name="title"></param>
        /// <param name="exception"></param>
        public virtual void LogCritical(string title, Exception exception) 
        {
            LoggingService.LogCritical(title, exception);
        }        
    }
}
