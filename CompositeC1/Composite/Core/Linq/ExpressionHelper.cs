/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Composite.Data;
using Composite.Core.Linq;


namespace Composite.Core.Linq
{
	internal static class ExpressionHelper
	{
        public static List<object> GetObjects(Type interfaceType, Expression sourceExpression)
        {
            List<object> result = DataFacade.GetData(interfaceType).Provider.CreateQuery(sourceExpression).ToListOfObjects();

            return result;
        }



        public static List<T> GetCastedObjects<T>(Type interfaceType, Expression sourceExpression)
        {
            List<T> result =
                DataFacade.GetData(interfaceType).Provider.CreateQuery(sourceExpression).
                    ToEnumerableOfObjects().
                    Cast<T>().
                    ToList();

            return result;
        }



        public static Expression CreatePropertyExpression(string fieldName, Expression parameterExpression)
        {
            Type interfaceType = parameterExpression.Type;

            if (interfaceType.GetProperties().Any(f => f.Name == fieldName))
            {
                return Expression.Property(parameterExpression, fieldName);
            }


            foreach (Type superInterfaceType in interfaceType.GetInterfaces())
            {
                if (superInterfaceType.GetProperties().Any(f => f.Name == fieldName))
                {
                    return Expression.Property(Expression.Convert(parameterExpression, superInterfaceType), fieldName);
                }
            }

            throw new InvalidOperationException(string.Format("The interface '{0}' or any of its superinterfaces does not contain a field named '{1}'", parameterExpression.Type, fieldName));
        }



        public static Expression CreatePropertyExpression(Type currentInterfaceType, Type actualPropertyDeclaringType, string fieldName, ParameterExpression parameterExpression)
        {
            Expression fieldParameterExpression = GetPropertyParameterExpression(currentInterfaceType, actualPropertyDeclaringType, parameterExpression);

            Expression expression = ExpressionHelper.CreatePropertyExpression(fieldName, fieldParameterExpression);

            return expression;
        }



        public static Expression GetPropertyParameterExpression(Type currentInterfaceType, Type actualPropertyDeclaringType, ParameterExpression parameterExpression)
        {
            if (currentInterfaceType == actualPropertyDeclaringType) return parameterExpression;

            return Expression.Convert(parameterExpression, actualPropertyDeclaringType);
        }



        public static Expression CreateWhereExpression(Expression sourceExpression, ParameterExpression parameterExpression, Expression filterExpression)
        {
            if (filterExpression == null) return sourceExpression;

            LambdaExpression whereLambdaExpression = Expression.Lambda(filterExpression, parameterExpression);
            Expression whereExpression = ExpressionCreator.Where(sourceExpression, whereLambdaExpression);

            return whereExpression;
        }



        public static Expression CreateSelectExpression(Expression sourceExpression, Expression bodyExpression, ParameterExpression parameterExpression)
        {
            LambdaExpression selectLambdaExpression = Expression.Lambda(bodyExpression, parameterExpression);

            Expression selectExpression = ExpressionCreator.Select(sourceExpression, selectLambdaExpression);

            return selectExpression;
        }



        public static Expression CreateDistinctExpression(Expression sourceExpression)
        {
            Expression distinctExpression = ExpressionCreator.Distinct(sourceExpression);

            return distinctExpression;
        }



        public static Expression CreateOrderByExpression(Expression sourceExpression, LambdaExpression keySelector)
        {
            Expression orderByExpression = ExpressionCreator.OrderBy(sourceExpression, keySelector);

            return orderByExpression;
        }


        public static Expression CreateOrderByDescendingExpression(Expression sourceExpression, LambdaExpression keySelector)
        {
            return ExpressionCreator.OrderByDescending(sourceExpression, keySelector);
        }


        public static Expression ThenByExpression(Expression sourceExpression, LambdaExpression keySelector)
        {
            return ExpressionCreator.ThenBy(sourceExpression, keySelector);
        }


        public static Expression ThenByDescendingExpression(Expression sourceExpression, LambdaExpression keySelector)
        {
            return ExpressionCreator.ThenByDescending(sourceExpression, keySelector);
        }


        public static Expression CreateJoinExpression(Expression outerSource, Expression innerSource, LambdaExpression outerKeySelector, LambdaExpression innerKeySelector, LambdaExpression resultSelector)
        {
            Expression joinExpression = ExpressionCreator.Join(outerSource, innerSource, outerKeySelector, innerKeySelector, resultSelector);

            return joinExpression;
        }
	}
}
