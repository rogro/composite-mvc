/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;


namespace Composite.Core.Linq
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class ExpressionCreator
    {
        /// <exclude />
        public static Expression Select(Expression source, LambdaExpression selector)
        {
            Type type = TypeHelpers.FindElementType(source);

            return Expression.Call(
                    typeof(Queryable), 
                    "Select",
                    new Type[] { type, selector.Body.Type },
                    source,
                    Expression.Quote(selector)
                );
        }



        /// <exclude />
        public static Expression Where(Expression source, LambdaExpression predicate)
        {
            Type type = TypeHelpers.FindElementType(source);

            return Expression.Call(
                    typeof(Queryable),
                    "Where",
                    new Type[] { type },
                    source,
                    Expression.Quote(predicate)
                );
        }



        /// <exclude />
        public static Expression Count(Expression source, LambdaExpression predicate)
        {
            Type type = TypeHelpers.FindElementType(source);

            return Expression.Call(
                    typeof(Queryable),
                    "Count",
                    new Type[] { type },
                    source,
                    Expression.Quote(predicate)
                );
        }



        /// <exclude />
        public static Expression Distinct(Expression source)
        {
            Type type = TypeHelpers.FindElementType(source);

            return Expression.Call(
                    typeof(Queryable),
                    "Distinct",
                    new Type[] { type },
                    source
                );
        }



        /// <exclude />
        public static Expression OrderBy(Expression source, LambdaExpression keySelector)
        {
            Type type = TypeHelpers.FindElementType(source);

            return Expression.Call(
                    typeof(Queryable),
                    "OrderBy",
                    new Type[] { type, keySelector.Body.Type },
                    source,
                    keySelector
                );
        }



        /// <exclude />
        public static Expression OrderByDescending(Expression source, LambdaExpression keySelector)
        {
            Type type = TypeHelpers.FindElementType(source);            

            return Expression.Call(
                    typeof(Queryable),
                    "OrderByDescending",
                    new Type[] { type, keySelector.Body.Type },
                    source,
                    keySelector
                );
        }



        /// <exclude />
        public static Expression ThenBy(Expression source, LambdaExpression keySelector)
        {
            Type type = TypeHelpers.FindElementType(source);

            return Expression.Call(
                    typeof(Queryable),
                    "ThenBy",
                    new Type[] { type, keySelector.Body.Type },
                    source,
                    keySelector
                );
        }



        /// <exclude />
        public static Expression ThenByDescending(Expression source, LambdaExpression keySelector)
        {
            Type type = TypeHelpers.FindElementType(source);

            return Expression.Call(
                    typeof(Queryable),
                    "ThenByDescending",
                    new Type[] { type, keySelector.Body.Type },
                    source,
                    keySelector
                );
        }



        /// <exclude />
        public static Expression Join(Expression outerSource, Expression innerSource, LambdaExpression outerKeySelector, LambdaExpression innerKeySelector, LambdaExpression resultSelector)
        {
            Type outerType = TypeHelpers.FindElementType(outerSource);
            Type innerType = TypeHelpers.FindElementType(innerSource);
            Type keyType = outerKeySelector.Body.Type;
            Type resultType = resultSelector.ReturnType;

            // public static IEnumerable<TResult> Join<TOuter, TInner, TKey, TResult>(
            //      IEnumerable<TOuter> outer, 
            //      IEnumerable<TInner> inner, 
            //      Func<TOuter, TKey> outerKeySelector, 
            //      Func<TInner, TKey> innerKeySelector, 
            //      Func<TOuter, TInner, TResult> resultSelector);

            Type outerFullType = typeof(IEnumerable<>).MakeGenericType(outerType);
            Type innerFullType = typeof(IEnumerable<>).MakeGenericType(innerType);
            Type outerKeySelectorFullType = typeof(Func<,>).MakeGenericType(outerType, keyType);
            Type innerKeySelectorFullType = typeof(Func<,>).MakeGenericType(innerType, keyType);
            Type resultSelectorFullType = typeof(Func<,,>).MakeGenericType(outerType, innerType, resultType);

#if DEBUG

            var b1 = outerFullType.IsAssignableFrom(outerSource.Type);
            var b2 = innerFullType.IsAssignableFrom(innerSource.Type);
            var b3 = outerKeySelector.Type == outerKeySelectorFullType;
            var b4 = innerKeySelector.Type == innerKeySelectorFullType;
            var b5 = resultSelector.Type == resultSelectorFullType;
#endif

            return Expression.Call(
                    typeof(Queryable),
                    "Join",
                    new Type[] { outerType, innerType, keyType, resultType },
                    outerSource,
                    innerSource,
                    Expression.Quote(outerKeySelector),
                    Expression.Quote(innerKeySelector),
                    Expression.Quote(resultSelector)
                );
        }
    }
}
