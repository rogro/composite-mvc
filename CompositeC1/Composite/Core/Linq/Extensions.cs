/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


namespace Composite.Core.Linq
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class DictionaryExtensions
    {
        /// <exclude />
        public static int GetContentHashCode(this IDictionary dictionary)
        {
            int hash = 0;

            foreach (DictionaryEntry entry in dictionary)
            {
                hash = hash ^ entry.Key.GetHashCode() ^ entry.Value.GetHashCode();
            }

            return hash;
        }
    }


    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class IEnumerableExtensions
    {
        /// <summary>
        /// Returns an evaluated collection. It allows avoiding of multiple calculations for the same enumerator.
        /// </summary>
        /// <typeparam name="T">Element type.</typeparam>
        /// <param name="enumerable">Enumerable object to be evaluated.</param>
        /// <returns>Evaluated collection.</returns>
        public static ICollection<T> Evaluate<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable is T[])
            {
                return enumerable as T[];
            }

            if (enumerable is ICollection<T>)
            {
                return enumerable as ICollection<T>;
            }

            return new List<T>(enumerable);
        }



        /// <exclude />
        public static IEnumerable<T> EvaluateOrNull<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable == null) return null;

            if (enumerable is T[] || enumerable is List<T>)
            {
                return enumerable;
            }
            return new List<T>(enumerable);
        }    

        /// <summary>
        /// Extends standard IQueryable<typeparamref name="T"/>.Single method, allows specifying exception text.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="exceptionOnEmpty">Exception format for not a single row found</param>
        /// <param name="exceptionOnMultipleResults">Exception format for multiple rows found</param>
        /// <param name="formatArgs"></param>
        /// <returns></returns>
        public static T SingleOrException<T>(this IQueryable<T> query, string exceptionOnEmpty, string exceptionOnMultipleResults, params object[] formatArgs) 
        {
            var result = query.ToList();

            if (result.Count == 0) throw new InvalidOperationException(string.Format(exceptionOnEmpty, formatArgs));
            
            if (result.Count == 1) return result[0];

            throw new InvalidOperationException(string.Format(exceptionOnMultipleResults, formatArgs));
        }

        /// <summary>
        /// Extends standard IQueryable<typeparamref name="T"/>.Single method, allows specifying exception text.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="exceptionOnEmpty">Exception format for not a single row found</param>
        /// <param name="exceptionOnMultipleResults">Exception format for multiple rows found</param>
        /// <param name="formatArgs"></param>
        /// <returns></returns>
        public static T SingleOrException<T>(this IEnumerable<T> query, string exceptionOnEmpty, string exceptionOnMultipleResults, params object[] formatArgs)
        {
            var result = query.ToList();

            if (result.Count == 0) throw new InvalidOperationException(string.Format(exceptionOnEmpty, formatArgs));

            if (result.Count == 1) return result[0];

            throw new InvalidOperationException(string.Format(exceptionOnMultipleResults, formatArgs));
        }

        /// <summary>
        /// Extends standard IQueryable<typeparamref name="T"/>.Single method, allows specifying exception text.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="exceptionOnMultipleResults">Exception format for multiple rows found</param>
        /// <param name="formatArgs">Format arguments</param>
        /// <returns></returns>
        public static T SingleOrDefaultOrException<T>(this IEnumerable<T> query, string exceptionOnMultipleResults, params object[] formatArgs)
        {
            var result = query.ToList();

            if (result.Count == 0) return default(T);

            if (result.Count == 1) return result[0];

            throw new InvalidOperationException(string.Format(exceptionOnMultipleResults, formatArgs));
        }

        /// <summary>
        /// Extends standard IQueryable<typeparamref name="T"/>.First() method, allows specifying exception text.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="exceptionOnEmpty">Exception format for not a single row found</param>
        /// <param name="formatArgs">Format arguments</param>
        /// <returns></returns>
        public static T FirstOrException<T>(this IQueryable<T> query, string exceptionOnEmpty, params object[] formatArgs) where T: class
        {
            var result = query.FirstOrDefault();

            if (result == null) throw new InvalidOperationException(string.Format(exceptionOnEmpty, formatArgs));

            return result;
        }
    }



    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class ListExtensions
    {
        /// <exclude />
        public static List<U> ToList<T, U>(this IEnumerable<T> source, Func<T, U> convertor)
        {
            List<U> result = new List<U>();

            foreach (T item in source)
            {
                result.Add(convertor(item));
            }

            return result;
        }



        /// <exclude />
        public static List<object> ToListOfObjects(this IEnumerable enumerable)
        {
            var result = new List<object>();

            foreach (object o in enumerable)
            {
                result.Add(o);
            }

            return result;
        }



        /// <exclude />
        public static IEnumerable<object> ToEnumerableOfObjects(this IEnumerable enumerable)
        {
            foreach (object o in enumerable)
            {
                yield return o;
            }
        }
    }
}
