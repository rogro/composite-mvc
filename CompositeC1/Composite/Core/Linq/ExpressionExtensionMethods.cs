/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq.Expressions;
using Composite.Core.Linq.ExpressionVisitors;
using Composite.Core.Logging;


namespace Composite.Core.Linq
{
    internal static class ExpressionExtensionMethods
    {
        public static Expression NestedAnd(this Expression leftExpression, Expression rightExpression)
        {
            if ((leftExpression == null) && (rightExpression == null)) throw new ArgumentNullException("rightExpression");

            if (leftExpression == null) return rightExpression;
            if (rightExpression == null) return leftExpression;            

            return Expression.And(leftExpression, rightExpression);
        }



        public static Expression NestedOr(this Expression leftExpression, Expression rightExpression)
        {
            if ((leftExpression == null) && (rightExpression == null)) throw new ArgumentNullException("rightExpression");

            if (leftExpression == null) return rightExpression;
            if (rightExpression == null) return leftExpression;

            return Expression.Or(leftExpression, rightExpression);
        }



        public static void DebugLogExpression(this Expression expression, string title, string label = "Expression")
        {
            if (RuntimeInformation.IsDebugBuild)
            {
                if (expression != null)
                {
                    Core.Logging.LoggingService.LogVerbose(title, label + " = " + expression.ToString());
                }
                else
                {
                    Core.Logging.LoggingService.LogVerbose(title, label + " =  null");
                }
            }
        }

        public static string BuildCacheKey(this Expression expression)
        {
            try
            {
                return CacheKeyBuilderExpressionVisitor.ExpressionToString(expression);
            }
            catch (Exception e)
            {
                var exeptionToLog = new InvalidOperationException("Failed while building a cache key for expression " + expression, e);
                LoggingService.LogError(typeof(ExpressionExtensionMethods).FullName, exeptionToLog);

                return null;
            }
        }
    }
}
