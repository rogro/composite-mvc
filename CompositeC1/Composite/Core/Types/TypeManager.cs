/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.C1Console.Events;


namespace Composite.Core.Types
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class TypeManager
    {
        private static ITypeManager _implementation = new TypeManagerImpl();

        internal static ITypeManager Implementation { get { return _implementation; } set { _implementation = value; } }


        static TypeManager()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlushEvent);
        }



        /// <summary>
        /// Gets a type by name, throws an exception if no type found.
        /// </summary>
        /// <param name="fullName">The full name of the type.</param>
        /// <returns>A <see cref="System.Type"/> instance.</returns>
        public static Type GetType(string fullName)
        {
            return _implementation.GetType(fullName);
        }



        /// <summary>
        /// Returns the type with the provided fullName (or null).
        /// </summary>
        /// <returns>A type or null</returns>
        public static Type TryGetType(string fullName)
        {
            return _implementation.TryGetType(fullName);
        }



        /// <exclude />
        public static string GetRuntimeFullName(string fullName)
        {
            if (fullName.StartsWith("DynamicType:")) return fullName.Remove(0, "DynamicType:".Length);

            return fullName;
        }

        /// <summary>
        /// In C1 2.1 and older dynamic types had a "DynamicType:" prefix in references. 
        /// This method removes this no longer used prefix.
        /// </summary>
        internal static string FixLegasyTypeName(string typeManagerTypeName)
        {
            if (typeManagerTypeName.StartsWith("DynamicType:", StringComparison.InvariantCultureIgnoreCase))
            {
                return typeManagerTypeName.Remove(0, "DynamicType:".Length);
            }

            return typeManagerTypeName;
        }

        /// <exclude />
        public static string SerializeType(Type type)
        {
            return _implementation.SerializeType(type);
        }



        /// <exclude />
        public static string TrySerializeType(Type type)
        {
            return _implementation.TrySerializeType(type);
        }
      


        /// <summary>
        /// This method return true if there is type with the fullname <para>typeFullname</para> anywhere in the system.
        /// </summary>
        /// <param name="typeFullname">Full name: namespace+name. X.Y.Z where X.Y is the namespace and Z is the type.</param>
        /// <returns></returns>
        public static bool HasTypeWithName(string typeFullname)
        {
            return _implementation.HasTypeWithName(typeFullname);
        }



        private static void OnFlushEvent(FlushEventArgs args)
        {
            _implementation.OnFlush();
        }
    }
}

