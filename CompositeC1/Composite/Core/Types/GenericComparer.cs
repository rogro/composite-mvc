/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Reflection;

namespace Composite.Core.Types
{
    internal class GenericComparer<T> : IComparer, IComparer<T> 
    {
        public static GenericComparer<T> Build(Type type, string propertyName)
        {
            return new GenericComparer<T>(type, propertyName, true);
        }



        public static GenericComparer<T> Build(Type type, string propertyName, bool ascDesc)
        {
            return new GenericComparer<T>(type, propertyName, ascDesc);
        }



        private MemberInfo _field;
        private bool _ascDesc;

        public GenericComparer(Type type, string propertyName, bool ascDesc)
        {
            this._field = type.GetPropertiesRecursively(f => f.Name == propertyName)[0];
            this._ascDesc = ascDesc;
        }



        object GetValue(object obj)
        {

            if (_field is FieldInfo)

                return ((FieldInfo)_field).GetValue(obj);

            else

                return ((PropertyInfo)_field).GetValue(obj, new object[0]);

        }



        int IComparer.Compare(object obj1, object obj2)
        {
            IComparable comparer = (IComparable)GetValue(obj1);

            return comparer.CompareTo(GetValue(obj2)) * (_ascDesc ? 1 : -1);

        }


        public int Compare(T obj1, T obj2)
        {
            IComparable comparer = (IComparable)GetValue(obj1);
            object value = GetValue(obj2);

            if ((comparer == null) && (value == null)) return 0;

            if (comparer == null) return (_ascDesc ? 1 : -1);
            if (value == null) return (_ascDesc ? -1 : 1);

            return comparer.CompareTo(value) * (_ascDesc ? 1 : -1);
        }
    }
}
