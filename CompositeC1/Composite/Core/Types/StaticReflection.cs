/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq.Expressions;
using System.Reflection;
using Composite.Core.Extensions;

namespace Composite.Core.Types
{
    internal static class StaticReflection
    {
        public static MethodInfo GetGenericMethodInfo(Expression<Action<object>> expression)
        {
            Verify.ArgumentNotNull(expression, "expression");

            return GetMethodInfo(expression.Body).GetGenericMethodDefinition();
        }

        public static MethodInfo GetGenericMethodInfo(Expression<Func<object>> expression)
        {
            Verify.ArgumentNotNull(expression, "expression");

            return GetMethodInfo(expression.Body).GetGenericMethodDefinition();
        }

        public static MethodInfo GetMethodInfo<T, S>(Expression<Func<T, S>>  expression)
        {
            return GetMethodInfo(expression.Body as MethodCallExpression);
        }

        public static MethodInfo GetMethodInfo<T>(Expression<Func<T>> expression)
        {
            return GetMethodInfo(expression.Body as MethodCallExpression);
        }

        public static MethodInfo GetMethodInfo(Expression expression)
        {
            Verify.ArgumentNotNull(expression, "expression");

            if (expression is UnaryExpression
                && (expression as UnaryExpression).NodeType == ExpressionType.Convert)
            {
                expression = (expression as UnaryExpression).Operand;
            }

            Verify.ArgumentCondition(expression is MethodCallExpression, "expressionBody", 
                                     "Expression body should be of type '{0}'".FormatWith(typeof(MethodCallExpression).Name));

            return (expression as MethodCallExpression).Method;
        }
    }
}
