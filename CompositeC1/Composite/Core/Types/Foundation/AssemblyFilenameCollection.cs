/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.IO;


namespace Composite.Core.Types.Foundation
{
    internal sealed class AssemblyFilenameCollection
    {
        private Dictionary<string, string> _assemblyFilenames = new Dictionary<string, string>();



        public bool ContainsAssemblyFilename(string assemblyFilename)
        {
            if (string.IsNullOrEmpty(assemblyFilename)) throw new ArgumentNullException("assemblyFilename");

            string assemblyName = GetAssemblyName(assemblyFilename);

            return _assemblyFilenames.ContainsKey(assemblyName);
        }



        public bool ContainsAssemblyName(string assemblyName)
        {
            if (string.IsNullOrEmpty(assemblyName)) throw new ArgumentNullException("assemblyName");

            return _assemblyFilenames.ContainsKey(assemblyName);
        }



        public void Add(string assemblyFilename)
        {
            if (string.IsNullOrEmpty(assemblyFilename)) throw new ArgumentNullException("assemblyFilename");

            string assemblyName = GetAssemblyName(assemblyFilename);

            _assemblyFilenames[assemblyName] = assemblyFilename;
        }



        public string GetFilenameByAssemblyName(string assemblyName)
        {
            if (string.IsNullOrEmpty(assemblyName)) throw new ArgumentNullException("assemblyName");

            string assemblyFilename;
            if (_assemblyFilenames.TryGetValue(assemblyName, out assemblyFilename) == false) throw new ArgumentException(string.Format("Does not contain the assembly name '{0}'", assemblyName));

            return assemblyFilename;
        }



        public static string GetAssemblyName(string assemblyFilename)
        {
            string filename = Path.GetFileName(assemblyFilename);

            string extension = Path.GetExtension(filename);

            filename = filename.Remove(filename.Length - extension.Length);

            return filename;
        }
    }
}
