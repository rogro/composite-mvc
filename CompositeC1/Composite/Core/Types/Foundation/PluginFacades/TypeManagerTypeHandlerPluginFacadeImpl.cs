/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Concurrent;
using Composite.Core.Types.Plugins.TypeManagerTypeHandler;
using Composite.Core.Types.Plugins.TypeManagerTypeHandler.Runtime;


namespace Composite.Core.Types.Foundation.PluginFacades
{
    internal sealed class TypeManagerTypeHandlerPluginFacadeImpl : ITypeManagerTypeHandlerPluginFacade
    {
        private static readonly object _syncRoot = new object();
        private static Resources _resources;

        public Type GetType(string providerName, string fullName)
        {
            Verify.ArgumentNotNullOrEmpty(providerName, "providerName");
            Verify.ArgumentNotNullOrEmpty(fullName, "fullName");

            ITypeManagerTypeHandler typeManagerTypeHandler = GetTypeManagerTypeHandler(providerName);
            return typeManagerTypeHandler.GetType(fullName);
        }



        public string SerializedType(string providerName, Type type)
        {
            Verify.ArgumentNotNullOrEmpty(providerName, "providerName");
            Verify.ArgumentNotNull(type, "type");

            ITypeManagerTypeHandler typeManagerTypeHandler = GetTypeManagerTypeHandler(providerName);
            return typeManagerTypeHandler.SerializeType(type);
        }



        public bool HasTypeWithName(string providerName, string typeFullname)
        {
            Verify.ArgumentNotNullOrEmpty(providerName, "providerName");
            Verify.ArgumentNotNullOrEmpty(typeFullname, "typeFullname");

            ITypeManagerTypeHandler typeManagerTypeHandler = GetTypeManagerTypeHandler(providerName);
            return typeManagerTypeHandler.HasTypeWithName(typeFullname);
        }


        public void OnFlush()
        {
            _resources = null;
        }

        private static ITypeManagerTypeHandler GetTypeManagerTypeHandler(string providerName)
        {
            Resources resources = GetResources();

            return resources.TypeHandlerCache.GetOrAdd(providerName,
                provider =>
                {
                    ITypeManagerTypeHandler typeHandler = resources.Factory.Create(provider);

                    return typeHandler;
                });
        }


        private static Resources GetResources()
        {
            Resources result = _resources;
            if(result == null)
            {
                lock(_syncRoot)
                {
                    result = _resources;
                    if(result == null)
                    {
                        result = BuildResources();
                        _resources = result;
                    }
                }
            }
            return result;
        }

        private static Resources BuildResources()
        {
            return new Resources
                       {
                           Factory = new TypeManagerTypeHandlerFactory(),
                           TypeHandlerCache = new ConcurrentDictionary<string, ITypeManagerTypeHandler>()
                       };
        }

        private sealed class Resources
        {
            public TypeManagerTypeHandlerFactory Factory { get; set; }
            public ConcurrentDictionary<string, ITypeManagerTypeHandler> TypeHandlerCache { get; set; }
        }
    }
}
