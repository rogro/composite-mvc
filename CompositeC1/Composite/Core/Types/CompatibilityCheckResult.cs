/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.CodeDom.Compiler;
using Composite.Core.Extensions;


namespace Composite.Core.Types
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
	public class CompatibilityCheckResult
	{
        internal CompatibilityCheckResult()
        {
            Successful = true;
        }

        internal CompatibilityCheckResult(CompilerResults compilerResults)
        {
            Successful = !compilerResults.Errors.HasErrors;
            CompilerResults = compilerResults;
        }


        /// <exclude />
        public bool Successful { get; private set; }


        /// <exclude />
        public CompilerResults CompilerResults { get; private set; }


        /// <exclude />
	    public string ErrorMessage
	    {
	        get
	        {
                var compilationErrors = CompilerResults.Errors;
                if (compilationErrors.HasErrors)
                {
                    for(int i=0; i<compilationErrors.Count; i++)
                    {
                        if(!compilationErrors[i].IsWarning)
                        {
                            CompilerError firstError = compilationErrors[i];
                            return "(File name: '{0}', line: {1}, column: {2}, message: {3})".FormatWith(firstError.FileName, firstError.Line, firstError.Column, firstError.ErrorText);
                        }
                    }
                    throw new InvalidOperationException("Failed to parse error");
                    
                }
	            return string.Empty;
	        }
	    }
	}
}
