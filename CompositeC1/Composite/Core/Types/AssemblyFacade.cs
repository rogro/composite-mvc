/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using Composite.Core.IO;
using Composite.Core.Configuration;


namespace Composite.Core.Types
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [EditorBrowsable(EditorBrowsableState.Never)] 
	public static class AssemblyFacade
    {
        private static readonly string LogTitle = typeof (AssemblyFacade).Name;
        private static readonly Type RuntimeModuleType = typeof(Module).Assembly.GetType("System.Reflection.RuntimeModule");

        private static bool _compositeGeneratedErrorLogged;

        /// <exclude />
        public static IEnumerable<Assembly> GetLoadedAssembliesFromBin()
        {
            string binDirectory = PathUtil.Resolve(GlobalSettingsFacade.BinDirectory).ToLowerInvariant().Replace('\\', '/');

            List<Assembly> assemblies = new List<Assembly>();
            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                if (assembly.IsDynamic) continue;

                try
                {
                    string codebase = assembly.CodeBase.ToLowerInvariant();

                    if (codebase.Contains(binDirectory))
                    {
                        assemblies.Add(assembly);
                    }
                }
                catch
                {
                    // Ignore exceptions
                }
            }

            return assemblies;
        }


        /// <summary>
        /// Gets list of file pathes of .NET dll files from "~/Bin" folder, excluding Composite.Generated.dll.
        /// </summary>
        /// <returns></returns>
        /// <exclude />
        public static IEnumerable<string> GetAssembliesFromBin()
        {
            return GetAssembliesFromBin(false);
        }

        /// <summary>
        /// Gets list of file pathes of .NET dll files from "~/Bin" folder.
        /// </summary>
        /// <param name="includeCompositeGenerated">if set to <c>true</c> Composite.Generated.dll will also be included.</param>
        /// <returns></returns>
        /// <exclude />
        public static IEnumerable<string> GetAssembliesFromBin(bool includeCompositeGenerated)
        {
            var assembliesFromBin = new List<string>();

            foreach (string binFilePath in C1Directory.GetFiles(PathUtil.Resolve(GlobalSettingsFacade.BinDirectory), "*.dll")) {
                string assemblyFileName = Path.GetFileName(binFilePath);

                if (!includeCompositeGenerated)
                {
                    if (assemblyFileName.IndexOf(CodeGenerationManager.CompositeGeneratedFileName, StringComparison.OrdinalIgnoreCase) >= 0) continue;
                }

                if (IsDotNetAssembly(binFilePath)) {
                    assembliesFromBin.Add(binFilePath);
                }
            }

            return assembliesFromBin;
        }

        /// <summary>
        /// Gets the Composite.Generated assembly from the  "~/Bin" folder
        /// </summary>
        /// <exclude />
        public static Assembly GetGeneratedAssemblyFromBin()
        {
            foreach (string binFilePath in C1Directory.GetFiles(PathUtil.Resolve(GlobalSettingsFacade.BinDirectory), "*.dll"))
            {
                string assemblyFileName = Path.GetFileName(binFilePath);

                if (assemblyFileName.IndexOf(CodeGenerationManager.CompositeGeneratedFileName, StringComparison.OrdinalIgnoreCase) < 0)
                {
                    continue;
                }

                try
                {
                    return Assembly.LoadFrom(binFilePath);
                }
                catch(Exception ex)
                {
                    if (!_compositeGeneratedErrorLogged)
                    {
                        Log.LogInformation(LogTitle, "Failed to load ~/Bin/Composite.Generated.dll ");
                        Log.LogWarning(LogTitle, ex);

                        _compositeGeneratedErrorLogged = true;
                    }
                }
            }

            return null;
        }

        private static bool IsDotNetAssembly(string dllFilePath)
        {
            try {
                AssemblyName.GetAssemblyName(dllFilePath);
            }
            catch (BadImageFormatException) {
                return false;
            }
            catch (Exception) {
            }

            return true;
        }


        /// <exclude />
        public static Assembly GetAppCodeAssembly()
        {
            return AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(IsAppCodeDll);
        }


        /// <exclude />
        public static bool IsInMemoryAssembly(Assembly asm)
        {
            // Checking 
            // (asm.ManifestModule as System.Reflection.RuntimeModule).GetFullyQualifiedName() == "<In Memory Module>"

            if (!RuntimeModuleType.IsInstanceOfType(asm.ManifestModule)) return false;

            var method = RuntimeModuleType.GetMethod("GetFullyQualifiedName", BindingFlags.NonPublic | BindingFlags.Instance);
            return (method.Invoke(asm.ManifestModule, new object[0]) as string) == "<In Memory Module>";
        }


        /// <exclude />
        public static bool IsAppCodeDll(Assembly assembly)
        {
            string fullName = assembly.FullName;
            return fullName != null && (fullName.StartsWith("App_Code.") || fullName.StartsWith("App_Code,"));
        }

        /// <summary>
        /// Gets assemblies from bin and app code assembly
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Assembly> GetAllAssemblies()
        {
            List<Assembly> assemblies = GetLoadedAssembliesFromBin().ToList();

            Assembly appCodeAssembly = GetAppCodeAssembly();

            if (appCodeAssembly != null)
            {
                assemblies.Add(appCodeAssembly);
            }

            return assemblies;
        }
	}
}
