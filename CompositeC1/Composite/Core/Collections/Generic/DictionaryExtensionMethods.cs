/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;


namespace Composite.Core.Collections.Generic
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
	public static class DictionaryExtensionMethods
	{
        /// <exclude />
        public static void AddDictionary<TKey, TValue>(this Dictionary<TKey, TValue> targetDictionary, Dictionary<TKey, TValue> sourceDictionary)
        {
            if (targetDictionary == null) throw new ArgumentNullException("targetDictionary");
            if (sourceDictionary == null) throw new ArgumentNullException("sourceDictionary");

            foreach (KeyValuePair<TKey, TValue> kvp in sourceDictionary)
            {
                targetDictionary.Add(kvp.Key, kvp.Value);
            }
        }


        /// <exclude />
        public static TValue EnsureValue<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key, Func<TValue> createValue)
        {
            // TODO: Syncronization logic here???
            TValue value;
            if (dictionary.TryGetValue(key, out value) == false)
            {
                value = createValue();
                dictionary.Add(key, value);
            }

            return value;
        }


        /// <exclude />
        public static TValue EnsureValue<TKey, TValue>(this Hashtable<TKey, TValue> dictionary, TKey key, Func<TValue> createValue)
        {
            TValue value;
            if (!dictionary.TryGetValue(key, out value))
            {
                lock(dictionary)
                {
                    if (!dictionary.TryGetValue(key, out value))
                    {

                        value = createValue();
                        dictionary.Add(key, value);
                    }
                }
            }

            return value;
        }

	}
}
