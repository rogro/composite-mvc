/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections;
using System.Collections.Generic;


namespace Composite.Core.Collections.Generic
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [Obsolete("Use standard System.Collections.ObjectModel.ReadOnlyCollection<T>")]
    public sealed class ReadOnlyList<T> : IList<T>
    {
        private List<T> _list;


        /// <exclude />
        public ReadOnlyList(List<T> list)
        {
            if (list == null) throw new ArgumentNullException("list");

            _list = list;
        }


        /// <exclude />
        public int IndexOf(T item)
        {
            return _list.IndexOf(item);
        }


        /// <exclude />
        public void Insert(int index, T item)
        {
            throw new InvalidOperationException("This is not allowed on a ReadOnlyList");
        }


        /// <exclude />
        public void RemoveAt(int index)
        {
            throw new InvalidOperationException("This is not allowed on a ReadOnlyList");
        }


        /// <exclude />
        public T this[int index]
        {
            get
            {
                return _list[index];
            }
            set
            {
                throw new InvalidOperationException("This is not allowed on a ReadOnlyList");
            }
        }


        /// <exclude />
        public void Add(T item)
        {
            throw new InvalidOperationException("This is not allowed on a ReadOnlyList");
        }


        /// <exclude />
        public void Clear()
        {
            throw new InvalidOperationException("This is not allowed on a ReadOnlyList");
        }


        /// <exclude />
        public bool Contains(T item)
        {
            return _list.Contains(item);
        }


        /// <exclude />
        public void CopyTo(T[] array, int arrayIndex)
        {
            _list.CopyTo(array, arrayIndex);
        }


        /// <exclude />
        public int Count
        {
            get { return _list.Count; }
        }


        /// <exclude />
        public bool IsReadOnly
        {
            get { return true; }
        }


        /// <exclude />
        public bool Remove(T item)
        {
            throw new InvalidOperationException("This is not allowed on a ReadOnlyList");
        }


        /// <exclude />
        public IEnumerator<T> GetEnumerator()
        {
            return _list.GetEnumerator();
        }


        /// <exclude />
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)_list).GetEnumerator();
        }
    }
}
