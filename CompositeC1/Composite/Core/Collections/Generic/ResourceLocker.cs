/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Threading;


namespace Composite.Core.Collections.Generic
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class ResourceLocker<T>
        where T : class
    {
        /// <exclude />
        public delegate void InitializerDelegate(T resources);

        private readonly T _resources;
        private readonly InitializerDelegate _initializerDelegate;
        private readonly bool _requireCoreReaderLock;

        private bool _initialized;
        private bool _initializing;
        private Exception _initializationException;
        
        private readonly object _lock = new object();


        /// <exclude />
        public ResourceLocker(T resources, InitializerDelegate initializerDelegate, bool requireCoreReaderLock)
        {
            _resources = resources;
            _initializerDelegate = initializerDelegate;
            _requireCoreReaderLock = requireCoreReaderLock;
        }

        /// <exclude />
        public ResourceLocker(T resources, InitializerDelegate initializerDelegate): this(resources, initializerDelegate, true)
        {
        }


        /// <exclude />
        public T Resources
        {
            get
            {
                if (!_initialized)
                {
                    Initialize();
                }

                if (_initializationException != null)
                {
                    throw new InvalidOperationException("Error initializing resources", _initializationException);
                }

                return _resources;
            }
        }


        /// <exclude />
        public bool IsInitialized
        {
            get
            {
                return _initialized;
            }
        }


        /// <exclude />
        public void Initialize()
        {
            if (_initialized)
            	return;


            IDisposable globalReaderLock = null;

            try
            {
                if(_requireCoreReaderLock)
                {
                    globalReaderLock = GlobalInitializerFacade.CoreNotLockedScope;
                }
                
                lock (_lock)
				{
					Verify.IsFalse(_initializing, "Initialize is already being executed on this thread! Please examine the stack!");

					if (_initialized)
						return;

					_initializing = true;
                    _initializationException = null;

				    try
				    {
				        _initializerDelegate(_resources);
				    }
				    catch (Exception exception)
				    {
				        _initializationException = exception;
				        throw;
				    }
					finally
					{
						_initialized = true;
						_initializing = false;
					}
				}

            }
            finally
            {
                if(globalReaderLock != null)
                {
                    globalReaderLock.Dispose();
                }
            }
		}


        /// <exclude />
        public IDisposable Locker
        {
            get
            {
                return new ResourceLockerToken(this);
            }
        }


        /// <exclude />
        public IDisposable ReadLocker
        {
            get
            {
                return new ResourceLockerToken(this);
            }
        }


        /// <exclude />
        public void ResetInitialization()
        {
            lock (_lock)
            {
                _initialized = false;
            }
        }


        private void TryEnterLock(int timeoutInMilliseconds, ref bool success)
        {
            Monitor.TryEnter(_lock, timeoutInMilliseconds, ref success);
        }


        private void Exit()
        {
            Monitor.Exit(_lock);
        }


        private sealed class ResourceLockerToken : IDisposable
        {
            private readonly ResourceLocker<T> _resourceLocker;

            internal ResourceLockerToken(ResourceLocker<T> resourceLocker)
            {
                if (resourceLocker == null) throw new ArgumentNullException("resourceLocker");

                _resourceLocker = resourceLocker;

                int tires = 120;

                bool success = false;
                while (!success && tires-- > 0)
                {
                    IDisposable coreLock = null;

                    try
                    {
                        if (_resourceLocker._requireCoreReaderLock)
                        {
                            coreLock = GlobalInitializerFacade.CoreNotLockedScope;
                        }

                        _resourceLocker.TryEnterLock(500, ref success);
                    }
                    finally
                    {
                        if (coreLock != null)
                        {
                            coreLock.Dispose();
                        }
                    }

                    if (!success)
                    {
                        Thread.Sleep(1);
                    }
                }

                if (!success)
                {
                    throw new TimeoutException("Failed to obtain a required resource lock. Aborting to avoid system deadlocks.");
                }
            }

            public void Dispose()
            {
                _resourceLocker.Exit();
            }
        }
    }
}
