/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Diagnostics;
using System.Collections.Generic;


namespace Composite.Core.Collections
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [DebuggerDisplay("Name = {Name}, Namespace = {Namespace}")]
    public sealed class NamespaceTreeBuilderFolder
    {
        internal NamespaceTreeBuilderFolder(string name, string namespaceName)
        {
            this.Name = name;
            this.Namespace = namespaceName;
            this.Leafs = new List<INamespaceTreeBuilderLeafInfo>();
            this.SubFolders = new List<NamespaceTreeBuilderFolder>();
        }


        /// <exclude />
        public string Name
        {
            get;
            private set;
        }


        /// <exclude />
        public string Namespace
        {
            get;
            private set;
        }


        /// <exclude />
        public List<INamespaceTreeBuilderLeafInfo> Leafs
        {
            get;
            private set;
        }


        /// <exclude />
        public List<NamespaceTreeBuilderFolder> SubFolders
        {
            get;
            private set;
        }
    }
}
