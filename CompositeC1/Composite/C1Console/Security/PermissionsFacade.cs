/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Composite.C1Console.Elements;
using Composite.Core.Linq;


namespace Composite.C1Console.Security
{
    /// <summary>
    /// This class can be use to get allowed permissions for current user or given UserToken and an EntityToken.
    /// </summary>
    [EditorBrowsable(EditorBrowsableState.Never)]
    public static class PermissionsFacade
    {
        /// <summary>
        /// This method will return all allowed permission for the current logged in user given the <paramref name="entityToken"/>.
        /// </summary>
        /// <param name="entityToken">EntityToken to get permissions for.</param>
        /// <returns>Allowed permission types</returns>
        public static IEnumerable<PermissionType> GetPermissionsForCurrentUser(EntityToken entityToken)
        {
            UserToken userToken = UserValidationFacade.GetUserToken();

            return GetPermissions(userToken, entityToken);
        }


        /// <summary>
        /// This method will return all allowed permission for the given <paramref name="userToken"/> and given the <paramref name="entityToken"/>.
        /// </summary>
        /// <param name="userToken">UserToken to get permissions for.</param>
        /// <param name="entityToken">EntityToken to get permissions for.</param>
        /// <returns>Allowed permission types</returns>
        public static IEnumerable<PermissionType> GetPermissions(UserToken userToken, EntityToken entityToken)
        {
            IEnumerable<UserPermissionDefinition> userPermissionDefinitions = PermissionTypeFacade.GetUserPermissionDefinitions(userToken.Username);
            IEnumerable<UserGroupPermissionDefinition> userGroupPermissionDefinitions = PermissionTypeFacade.GetUserGroupPermissionDefinitions(userToken.Username);

            IEnumerable<PermissionType> permissions = PermissionTypeFacade.GetCurrentPermissionTypes(userToken, entityToken, userPermissionDefinitions, userGroupPermissionDefinitions).Evaluate();

            return permissions;
        }



        /// <summary>
        /// This method returns true if the given username <paramref name="username"/> has admin rights on the root element.
        /// This is normal way of creating a administrator in C1.
        /// </summary>
        /// <param name="username">Username to test</param>
        /// <returns>True if the given username has admin rights on the root element.</returns>
        public static bool IsAdministrator(string username)
        {
            UserToken userToken = new UserToken(username);

            EntityToken rootEntityToken = ElementFacade.GetRootsWithNoSecurity().First().ElementHandle.EntityToken;

            IEnumerable<PermissionType> permissions = GetPermissions(userToken, rootEntityToken);

            return permissions.Contains(PermissionType.Administrate);
        }
    }
}
