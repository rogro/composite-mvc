/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using System.Collections.Generic;
using Composite.Core.Linq;


namespace Composite.C1Console.Security
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class SecurityResolver
    {
        /// <exclude />
        public static SecurityResult Resolve(UserToken userToken, ActionToken actionToken, EntityToken entityToken, IEnumerable<UserPermissionDefinition> userPermissionDefinitions, IEnumerable<UserGroupPermissionDefinition> userGroupPermissionDefinition)
        {
            if (userToken == null) throw new ArgumentNullException("userToken");
            if (actionToken == null) throw new ArgumentNullException("actionToken");


            return Resolve(userToken, actionToken.PermissionTypes, entityToken, userPermissionDefinitions, userGroupPermissionDefinition);
        }


        /// <exclude />
        public static SecurityResult Resolve(UserToken userToken, IEnumerable<PermissionType> requiredPermissions, EntityToken entityToken, IEnumerable<UserPermissionDefinition> userPermissionDefinitions, IEnumerable<UserGroupPermissionDefinition> userGroupPermissionDefinition)
        {
            if (userToken == null) throw new ArgumentNullException("userToken");
            if (requiredPermissions == null) throw new ArgumentNullException("requiredPermissions");

            if ((entityToken is NoSecurityEntityToken)) return SecurityResult.Allowed;

            requiredPermissions = requiredPermissions.Evaluate();

            if (!requiredPermissions.Any())
            {
                return SecurityResult.Allowed;
            }

            IEnumerable<PermissionType> currentPermissionTypes = PermissionTypeFacade.GetCurrentPermissionTypes(userToken, entityToken, userPermissionDefinitions, userGroupPermissionDefinition);

            if (!currentPermissionTypes.Any())
            {
                return SecurityResult.Disallowed;
            }

            // At least one of the permissions should be allowed
            foreach (PermissionType permissionType in currentPermissionTypes)
            {
                if (requiredPermissions.Contains(permissionType))
                {
                    return SecurityResult.Allowed;
                }
            }

            return SecurityResult.Disallowed;
        }



        /// <exclude />
        public static SecurityResult Resolve(SecurityToken securityToken)
        {
            if (securityToken == null) throw new ArgumentNullException("securityToken");

            IEnumerable<UserPermissionDefinition> userPermissionDefinitions = PermissionTypeFacade.GetUserPermissionDefinitions(securityToken.UserToken.Username);
            IEnumerable<UserGroupPermissionDefinition> userGroupPermissionDefinition = PermissionTypeFacade.GetUserGroupPermissionDefinitions(securityToken.UserToken.Username);

            return Resolve(securityToken.UserToken, securityToken.ActionToken, securityToken.EntityToken, userPermissionDefinitions, userGroupPermissionDefinition);
        }
    }
}
