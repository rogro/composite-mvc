/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Security;
using System.Text;
using Composite.Core.Serialization;
using Composite.Core.Types;
using Composite.Core;


namespace Composite.C1Console.Security
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class ActionTokenSerializer
    {
        /// <exclude />
        public static string Serialize(ActionToken actionToken)
        {
            return Serialize(actionToken, false);
        }



        /// <exclude />
        public static string Serialize(ActionToken actionToken, bool includeHashValue)
        {
            StringBuilder sb = new StringBuilder();

            StringConversionServices.SerializeKeyValuePair(sb, "actionTokenType", TypeManager.SerializeType(actionToken.GetType()));

            string serializedActionToken = actionToken.Serialize();
            StringConversionServices.SerializeKeyValuePair(sb, "actionToken", serializedActionToken);

            if (includeHashValue)
            {
                StringConversionServices.SerializeKeyValuePair(sb, "actionTokenHash", HashSigner.GetSignedHash(serializedActionToken).Serialize());
            }

            return sb.ToString();
        }



        /// <exclude />
        public static ActionToken Deserialize(string serialziedActionToken)
        {
            return Deserialize(serialziedActionToken, false);
        }



        /// <exclude />
        public static ActionToken Deserialize(string serialziedActionToken, bool includeHashValue)
        {
            if (string.IsNullOrEmpty(serialziedActionToken)) throw new ArgumentNullException("serialziedActionToken");

            Dictionary<string, string> dic = StringConversionServices.ParseKeyValueCollection(serialziedActionToken);

            if ((dic.ContainsKey("actionTokenType") == false) ||
                (dic.ContainsKey("actionToken") == false) ||
                ((includeHashValue) && (dic.ContainsKey("actionTokenHash") == false)))
            {
                throw new ArgumentException("Failed to deserialize the value. It has to be serialized with ActionTokenSerializer class.", "serialziedActionToken");
            }

            string actionTokenTypeString = StringConversionServices.DeserializeValueString(dic["actionTokenType"]);
            string actionTokenString = StringConversionServices.DeserializeValueString(dic["actionToken"]);

            if (includeHashValue)
            {
                string actionTokenHash = StringConversionServices.DeserializeValueString(dic["actionTokenHash"]);

                HashValue hashValue = HashValue.Deserialize(actionTokenHash);
                if (HashSigner.ValidateSignedHash(actionTokenString, hashValue) == false)
                {
                    throw new SecurityException("Serialized action token is tampered");
                }
            }

            Type actionType = TypeManager.GetType(actionTokenTypeString);

            MethodInfo methodInfo = actionType.GetMethod("Deserialize", BindingFlags.Public | BindingFlags.Static);
            if (methodInfo == null)
            {
                Log.LogWarning("ActionTokenSerializer", string.Format("The action token {0} is missing a public static Deserialize method taking a string as parameter and returning an {1}", actionType, typeof(ActionToken)));
                throw new InvalidOperationException(string.Format("The action token {0} is missing a public static Deserialize method taking a string as parameter and returning an {1}", actionType, typeof(ActionToken)));
            }


            ActionToken actionToken;
            try
            {
                actionToken = (ActionToken)methodInfo.Invoke(null, new object[] { actionTokenString });
            }
            catch (Exception ex)
            {
                Log.LogWarning("ActionTokenSerializer", string.Format("The action token {0} is missing a public static Deserialize method taking a string as parameter and returning an {1}", actionType, typeof(ActionToken)));
                Log.LogWarning("ActionTokenSerializer", ex);

                throw new InvalidOperationException(string.Format("The action token {0} is missing a public static Deserialize method taking a string as parameter and returning an {1}", actionType, typeof(ActionToken)), ex);
            }

            if (actionToken == null)
            {
                Log.LogWarning("ActionTokenSerializer", string.Format("public static Deserialize method taking a string as parameter and returning an {1} on the action token {0} did not return an object", actionType, typeof(ActionToken)));

                throw new InvalidOperationException(string.Format("public static Deserialize method taking a string as parameter and returning an {1} on the action token {0} did not return an object", actionType, typeof(ActionToken)));
            }

            return actionToken;
        }



        /// <exclude />
        public static T Deserialize<T>(string serialziedActionToken)
            where T : ActionToken
        {
            return (T)Deserialize(serialziedActionToken);
        }
    }
}
