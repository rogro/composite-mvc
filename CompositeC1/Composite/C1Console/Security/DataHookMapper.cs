/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Composite.Data;


namespace Composite.C1Console.Security
{
    internal sealed class DataHookMapper<T>
        where T : class, IData
    {
        private EntityTokenHook _currentEntityTokenHook;

        private EntityToken ParentEntityToken { get; set; }

        public DataHookMapper(EntityToken parentEntityToken)
        {
            if (parentEntityToken == null) throw new ArgumentNullException("parentEntityToken");

            DataEventSystemFacade.SubscribeToDataAfterAdd<T>(OnDataAddedOrDeleted, false);
            DataEventSystemFacade.SubscribeToDataDeleted<T>(OnDataAddedOrDeleted, false);
            DataEventSystemFacade.SubscribeToStoreChanged<T>(OnStoreChanged, false);

            this.ParentEntityToken = parentEntityToken;
        }



        public EntityTokenHook CurrentEntityTokenHook
        {
            get
            {
                if (_currentEntityTokenHook == null)
                {
                    UpdateCurrentEntityTokenHook();
                }

                return _currentEntityTokenHook;
            }
        }



        private void UpdateCurrentEntityTokenHook()
        {
            EntityTokenHook entityTokenHook = new EntityTokenHook(this.ParentEntityToken);

            List<T> datas = DataFacade.GetData<T>().ToList();
            foreach (T data in datas)
            {
                entityTokenHook.AddHookie(data.GetDataEntityToken());
            }

            _currentEntityTokenHook = entityTokenHook;
        }


        private void OnStoreChanged(object sender, StoreEventArgs dataEventArgs)
        {
            if (!dataEventArgs.DataEventsFired)
            {
                HookingFacade.RemoveHook(this.CurrentEntityTokenHook);
                UpdateCurrentEntityTokenHook();
                HookingFacade.AddHook(this.CurrentEntityTokenHook);
            }
        }      

        private void OnDataAddedOrDeleted(object sender, DataEventArgs dataEventArgs)
        {
            HookingFacade.RemoveHook(this.CurrentEntityTokenHook);

            UpdateCurrentEntityTokenHook();

            HookingFacade.AddHook(this.CurrentEntityTokenHook);
        }      
    }
}
