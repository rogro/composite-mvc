/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;
using System.Text;


namespace Composite.C1Console.Security
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class EntityTokenHtmlPrettyfierHelper
    {
        private StringBuilder _stringBuilder = new StringBuilder();


        /// <exclude />
        public void StartTable()
        {
            _stringBuilder.AppendLine(@"<table style=""empty-cells: show; padding: 10px; margin: 10px; border-collapse: collapse; border: 1px solid black;"">");
        }

        /// <exclude />
        public void EndTable()
        {
            _stringBuilder.AppendLine(@"</table>");
        }


        /// <exclude />
        public void StartRow()
        {
            _stringBuilder.AppendLine(@"<tr style=""padding: 0px; margin: 0px; border: 1px solid black;"">");
        }


        /// <exclude />
        public void EndRow()
        {
            _stringBuilder.AppendLine(@"</tr>");
        }


        /// <exclude />
        public void AddCell(string value)
        {
            AddCell(value, 1);
        }


        /// <exclude />
        public void AddCell(string value, int colspan, string backgroundColor = "#ffffff")
        {
            _stringBuilder.AppendLine(string.Format(@"<td colspan=""{0}"" style=""background-color: {1}; vertical-align: top; border: 1px solid black; padding: 2px; margin: 0px;"">{2}</td>", colspan, backgroundColor, value));
        }


        /// <exclude />
        public void AddFullRow(IEnumerable<string> values)
        {
            StartRow();

            foreach (string value in values)
            {
                AddCell(value);
            }

            EndRow();
        }


        /// <exclude />
        public void AddHeading(string value)
        {
            StartRow();

            _stringBuilder.AppendLine(string.Format(@"<td colspan=""2"" style=""vertical-align: top; border: 1px solid black; padding: 6px; margin: 0px; font-size: 110%"">{0}</td>", value));

            EndRow();
        }


        /// <exclude />
        public string GetResult()
        {
            return _stringBuilder.ToString();
        }
    }
}
