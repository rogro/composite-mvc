/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System.Configuration;

using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

using Composite.C1Console.Security.Plugins.LoginSessionStore;

namespace Composite.C1Console.Security.Plugins.LoginSessionStore.Runtime
{
    internal class LoginSessionStoreSettings : SerializableConfigurationSection
    {
        public const string SectionName = "Composite.C1Console.Security.Plugins.LoginSessionStoreConfiguration";

        private const string _sessionDataProvidersProperty = "LoginSessionStore";        
        [ConfigurationProperty(_sessionDataProvidersProperty, IsRequired = true)]
        public NameTypeConfigurationElementCollection<LoginSessionStoreData, LoginSessionStoreData> LoginSessionStores
        {
            get
            {
                return (NameTypeConfigurationElementCollection<LoginSessionStoreData, LoginSessionStoreData>)base[_sessionDataProvidersProperty];
            }
        }

        private const string _defaultProviderProperty = "defaultProvider";
        [ConfigurationProperty(_defaultProviderProperty, IsRequired = true)]
        public string DefaultLoginSessionStore
        {
            get{ return (string)base[_defaultProviderProperty];}
            set { base[_defaultProviderProperty] = value; }
        }                
    }
}
