/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */



namespace Composite.C1Console.Security.Plugins.LoginProvider
{
    /// <summary>
    /// Interface for form login providers - providers able to validate logins (username/password) and manage basic login information. 
    /// The provider is as a minimum expected to provide Validate( username, password ), UsersExists { get; } and AllUsernames { get; }. 
    /// If the provider support adding new users and changing a users password, this is declared via CanAddNewUser and CanSetUserPassword.
    /// </summary>
    public interface IFormLoginProvider : ILoginProvider
    {
        /// <summary>
        /// Validate a login (username and password combination)
        /// </summary>
        /// <param name="username">User name to validate</param>
        /// <param name="password">Password to validate</param>
        /// <returns>Emun describing the result of the validation</returns>
        LoginResult Validate(string username, string password);

        /// <summary>
        /// Updates a user password
        /// </summary>
        /// <param name="username">Name of user to update password for</param>
        /// <param name="password">Desired password</param>
        void SetUserPassword(string username, string password);

        /// <summary>
        /// Create a new login
        /// </summary>
        /// <param name="username">Name of user to update password for</param>
        /// <param name="password">Desired password</param>
        /// <param name="group">Name of group (simple container) for user</param>
        /// <param name="email">User email address</param>
        void AddNewUser(string username, string password, string group, string email);
    }
}
