/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Configuration;

using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;



namespace Composite.C1Console.Security.Plugins.LoginProvider.Runtime
{
    internal class LoginProviderSettings : SerializableConfigurationSection
    {
        public const string SectionName = "Composite.C1Console.Security.Plugins.LoginProviderConfiguration";

        private const string _loginProvidersProperty = "LoginProviderPlugins";               
        [ConfigurationProperty(_loginProvidersProperty, IsRequired = true)]
        public NameTypeConfigurationElementCollection<LoginProviderData, LoginProviderData> LoginProviderPlugins
        {
            get
            {
                return (NameTypeConfigurationElementCollection<LoginProviderData, LoginProviderData>)base[_loginProvidersProperty];
            }
        }

        private const string _defaultLoginProviderPluginProperty = "defaultLoginProviderPlugin";
        [ConfigurationProperty(_defaultLoginProviderPluginProperty, IsRequired = true)]
        public string DefaultLoginProviderPlugin
        {
            get { return (string)base[_defaultLoginProviderPluginProperty]; }
            set { base[_defaultLoginProviderPluginProperty] = value; }
        }

        private const string _maxLoginAttempts = "maxLoginAttempts";
        [ConfigurationProperty(_maxLoginAttempts, IsRequired = false, DefaultValue = int.MaxValue)]
        public int MaxLoginAttempts
        {
            get { return (int)base[_maxLoginAttempts]; }
            set { base[_maxLoginAttempts] = value; }
        }
    }
}
