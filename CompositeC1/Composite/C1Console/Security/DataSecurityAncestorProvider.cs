/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;
using Composite.Data.Hierarchy;
using Composite.Data;


namespace Composite.C1Console.Security
{
    internal sealed class DataSecurityAncestorProvider : ISecurityAncestorProvider
    {
        public IEnumerable<EntityToken> GetParents(EntityToken entityToken)
        {
            if (entityToken == null) throw new ArgumentNullException("entityToken");

            DataEntityToken dataEntityToken = entityToken as DataEntityToken;

            if (dataEntityToken == null) throw new ArgumentException(string.Format("The type of the entityToken should be of type {0}", typeof(DataEntityToken)), "entityToken");

            IData data = dataEntityToken.Data;
            if(data == null) return null; // A piece of data may be deleted to this point

            using (new DataScope(data.DataSourceId.DataScopeIdentifier, data.DataSourceId.LocaleScope))
            {
                IData parent = DataAncestorFacade.GetParent(dataEntityToken.Data);
                if (parent == null)
                {
                    return null;
                }

                return new List<EntityToken> { parent.GetDataEntityToken() };                
            }
        }
    }
}
