/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;


namespace Composite.C1Console.Security
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
	public sealed class HashValue
	{
        private int _value;



        /// <exclude />
        public HashValue(int value)
        {
            _value = value;
        }



        /// <exclude />
        public string Serialize()
        {
            return _value.ToString();
        }



        /// <exclude />
        public static HashValue Deserialize(string serializedHashValue)
        {            
            int value;

            if (int.TryParse(serializedHashValue, out value) == false)
            {
                throw new ArgumentException("The string is not a valid serialized hash value");
            }

            return new HashValue(value);
        }



        /// <exclude />
        public override bool Equals(object obj)
        {
            return Equals(obj as HashValue);
        }



        /// <exclude />
        public bool Equals(HashValue hashValue)
        {
            if (hashValue == null) return false;

            return _value == hashValue._value;
        }



        /// <exclude />
        public override string ToString()
        {
            return _value.ToString();
        }



        /// <exclude />
        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }
	}
}
