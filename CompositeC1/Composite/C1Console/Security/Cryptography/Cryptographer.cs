/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;


namespace Composite.C1Console.Security.Cryptography
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
	public static class Cryptographer
	{
        private static string _secretKey;
        private static Encoding _encoding = Encoding.Unicode;

        static Cryptographer()
        {
            _secretKey = "u?Ccr?";
            GCHandle gch = GCHandle.Alloc(_secretKey, GCHandleType.Pinned);
            
            //ZeroMemory(gch.AddrOfPinnedObject(), _secretKey.Length * 2);
            //gch.Free();
        }



        /// <exclude />
        public static string Encrypt(this string data)
        {
            byte[] byteData = _encoding.GetBytes(data);
            byte[] encrypted = Cryptographer.EncryptBytes(byteData);
            return Convert.ToBase64String(encrypted);
        }



        /// <exclude />
        public static string Decrypt(this string data)
        {
            byte[] byteData = _encoding.GetBytes(data);
            byte[] decrypted = Cryptographer.DecryptBytes(Convert.FromBase64String(data));
            return _encoding.GetString(decrypted);
        }



        /// <exclude />
        public static byte[] EncryptBytes(byte[] toEncrypt)
        {
            DESCryptoServiceProvider DES = new DESCryptoServiceProvider();
            DES.Key = ASCIIEncoding.ASCII.GetBytes(_secretKey);
            DES.IV = ASCIIEncoding.ASCII.GetBytes(_secretKey);
            ICryptoTransform desencrypt = DES.CreateEncryptor();

            byte[] result;
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cryptostream = new CryptoStream(ms, desencrypt, CryptoStreamMode.Write))
                {
                    cryptostream.Write(toEncrypt, 0, toEncrypt.Length);
                    cryptostream.Close();
                }
                result = ms.ToArray();
            }
            return result;
        }



        /// <exclude />
        public static byte[] DecryptBytes(byte[] toDecrypt)
        {
            DESCryptoServiceProvider DES = new DESCryptoServiceProvider();
            DES.Key = ASCIIEncoding.ASCII.GetBytes(_secretKey);
            DES.IV = ASCIIEncoding.ASCII.GetBytes(_secretKey);
            ICryptoTransform desdecrypt = DES.CreateDecryptor();

            byte[] result;
            using (MemoryStream memoryWriteStream = new MemoryStream())
            {
                memoryWriteStream.Write(toDecrypt, 0, toDecrypt.Length);
                memoryWriteStream.Position = 0;

                using (CryptoStream cryptostreamDecr = new CryptoStream(memoryWriteStream, desdecrypt, CryptoStreamMode.Read))
                {
                    using (Stream memoryReadStream = new MemoryStream())
                    {
                        int size = 2048;
                        byte[] data = new byte[2048];
                        while (true)
                        {
                            size = cryptostreamDecr.Read(data, 0, data.Length);
                            if (size > 0)
                            {
                                memoryReadStream.Write(data, 0, size);
                            }
                            else
                            {
                                break;
                            }
                        }

                        result = new byte[memoryReadStream.Length];
                        memoryReadStream.Position = 0;
                        memoryReadStream.Read(result, 0, (int)memoryReadStream.Length);
                    }
                }
            }

            return result;
        }
	}
}
