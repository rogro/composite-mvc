/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Composite.Core.Extensions;
using Composite.Core.Linq;


namespace Composite.C1Console.Security
{
    internal sealed class AuxiliarySecurityAncestorFacadeImpl : IAuxiliarySecurityAncestorFacade
    {
        private Dictionary<Type, List<IAuxiliarySecurityAncestorProvider>> _auxiliarySecurityAncestorProviders = new Dictionary<Type, List<IAuxiliarySecurityAncestorProvider>>();
        private Dictionary<Type, List<IAuxiliarySecurityAncestorProvider>> _flushPersistentAuxiliarySecurityAncestorProviders = new Dictionary<Type, List<IAuxiliarySecurityAncestorProvider>>();


        public IEnumerable<EntityToken> GetParents(EntityToken entityToken)
        {
            if (entityToken == null) throw new ArgumentNullException("entityToken");


            List<IAuxiliarySecurityAncestorProvider> auxiliarySecurityAncestorProviders;
            List<IAuxiliarySecurityAncestorProvider> flushPersistentAuxiliarySecurityAncestorProviders;

            _auxiliarySecurityAncestorProviders.TryGetValue(entityToken.GetType(), out auxiliarySecurityAncestorProviders);
            _flushPersistentAuxiliarySecurityAncestorProviders.TryGetValue(entityToken.GetType(), out flushPersistentAuxiliarySecurityAncestorProviders);

            IEnumerable<IAuxiliarySecurityAncestorProvider> resultSecurityAncestorProviders = auxiliarySecurityAncestorProviders.ConcatOrDefault(flushPersistentAuxiliarySecurityAncestorProviders);

            if (resultSecurityAncestorProviders == null) return null;


            IEnumerable<EntityToken> totalResult = null;
            foreach (IAuxiliarySecurityAncestorProvider auxiliarySecurityAncestorProvider in resultSecurityAncestorProviders)
            {
                Dictionary<EntityToken, IEnumerable<EntityToken>> result = auxiliarySecurityAncestorProvider.GetParents(new EntityToken[] { entityToken });

                if (result.Count > 0)
                {
                    var firstValue = result.Values.First();

                    if (totalResult == null)
                    {
                        totalResult = firstValue;
                    }
                    else
                    {
                        totalResult = totalResult.Concat(firstValue);
                    }
                }
            }

            return totalResult;
        }



        public void AddAuxiliaryAncestorProvider(Type entityTokenType, IAuxiliarySecurityAncestorProvider auxiliarySecurityAncestorProvider, bool flushPersistent)
        {
            Dictionary<Type, List<IAuxiliarySecurityAncestorProvider>> providers;
            if (!flushPersistent)
            {
                providers = _auxiliarySecurityAncestorProviders;
            }
            else
            {
                providers = _flushPersistentAuxiliarySecurityAncestorProviders;
            }

            List<IAuxiliarySecurityAncestorProvider> auxiliarySecurityAncestorProviders;

            if (providers.TryGetValue(entityTokenType, out auxiliarySecurityAncestorProviders) == false)
            {
                auxiliarySecurityAncestorProviders = new List<IAuxiliarySecurityAncestorProvider>();
                providers.Add(entityTokenType, auxiliarySecurityAncestorProviders);
            }

            if (auxiliarySecurityAncestorProviders.Contains(auxiliarySecurityAncestorProvider))
            {
                throw new ArgumentNullException("The given auxiliarySecurityAncestorProvider has already been added with the given entity token");
            }

            auxiliarySecurityAncestorProviders.Add(auxiliarySecurityAncestorProvider);
        }



        public void RemoveAuxiliaryAncestorProvider(Type entityTokenType, IAuxiliarySecurityAncestorProvider auxiliarySecurityAncestorProvider)
        {
            List<IAuxiliarySecurityAncestorProvider> auxiliarySecurityAncestorProviders;

            if (_auxiliarySecurityAncestorProviders.TryGetValue(entityTokenType, out auxiliarySecurityAncestorProviders))
            {
                auxiliarySecurityAncestorProviders.Remove(auxiliarySecurityAncestorProvider);
            }

            if (_flushPersistentAuxiliarySecurityAncestorProviders.TryGetValue(entityTokenType, out auxiliarySecurityAncestorProviders))
            {
                auxiliarySecurityAncestorProviders.Remove(auxiliarySecurityAncestorProvider);
            }
        }



        public IEnumerable<IAuxiliarySecurityAncestorProvider> GetAuxiliaryAncestorProviders(Type entityTokenType)
        {
            List<IAuxiliarySecurityAncestorProvider> auxiliarySecurityAncestorProviders;

            if (_auxiliarySecurityAncestorProviders.TryGetValue(entityTokenType, out auxiliarySecurityAncestorProviders))
            {
                foreach (IAuxiliarySecurityAncestorProvider auxiliarySecurityAncestorProvider in auxiliarySecurityAncestorProviders)
                {
                    yield return auxiliarySecurityAncestorProvider;
                }
            }


            if (_flushPersistentAuxiliarySecurityAncestorProviders.TryGetValue(entityTokenType, out auxiliarySecurityAncestorProviders))
            {
                foreach (IAuxiliarySecurityAncestorProvider auxiliarySecurityAncestorProvider in auxiliarySecurityAncestorProviders)
                {
                    yield return auxiliarySecurityAncestorProvider;
                }
            }

            yield break;
        }



        public void Flush()
        {
            _auxiliarySecurityAncestorProviders = new Dictionary<Type, List<IAuxiliarySecurityAncestorProvider>>();
        }
    }
}
