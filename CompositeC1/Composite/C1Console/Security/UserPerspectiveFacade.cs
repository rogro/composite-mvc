/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Composite.Data;
using Composite.Data.Types;


namespace Composite.C1Console.Security
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
	public static class UserPerspectiveFacade
	{
        /// <exclude />
        public static IEnumerable<string> GetSerializedEntityTokens(string username)
        {
            return
                from activePerspective in DataFacade.GetData<IUserActivePerspective>()
                where activePerspective.Username == username
                select activePerspective.SerializedEntityToken;
        }



        /// <exclude />
        public static IEnumerable<EntityToken> GetEntityTokens(string username )
        {
            return
                GetSerializedEntityTokens(username).Select(f => EntityTokenSerializer.Deserialize(f));
        }



        /// <exclude />
        public static void SetEntityTokens(string username, IEnumerable<EntityToken> entityTokens)
        {
            SetSerializedEntityTokens(username, entityTokens.Select(f => EntityTokenSerializer.Serialize(f)));
        }



        /// <exclude />
        public static void SetSerializedEntityTokens(string username, IEnumerable<string> serializedEntityTokens)
        {
            DataFacade.Delete<IUserActivePerspective>(f => f.Username == username);

            foreach (string serializedEntityToken in serializedEntityTokens)
            {
                IUserActivePerspective activePerspective = DataFacade.BuildNew<IUserActivePerspective>();

                activePerspective.Username = username;
                activePerspective.SerializedEntityToken = serializedEntityToken;
                activePerspective.Id = Guid.NewGuid();

                DataFacade.AddNew<IUserActivePerspective>(activePerspective);
            }
        }



        /// <exclude />
        public static void DeleteAll(string username)
        {
            DataFacade.Delete<IUserActivePerspective>(f => f.Username == username);
        }
    }
}
