/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;
using System;
using Composite.Core.Types;
using System.Linq;


namespace Composite.C1Console.Security
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public abstract class ActionToken
    {
        /// <exclude />
        public abstract IEnumerable<PermissionType> PermissionTypes { get; }

        /// <exclude />
        public virtual string Serialize() { return ""; }

        /// <exclude />
        public virtual bool IgnoreEntityTokenLocking { get { return false; } }
    }



    internal static class ActionTokenExtensionMethods
    {
        private static Dictionary<Type, bool> _ignoreEntityTokenLockingCache = new Dictionary<Type, bool>();
        private static readonly object _lock = new object();

        public static bool IsIgnoreEntityTokenLocking(this ActionToken actionToken)
        {
            if (actionToken == null) throw new ArgumentNullException("actionToken");
            
            Type type = actionToken.GetType();

            lock (_lock)
            {
                bool ignoreLocking;
                if (_ignoreEntityTokenLockingCache.TryGetValue(type, out ignoreLocking) == false)
                {
                    ignoreLocking = type.GetCustomAttributesRecursively<IgnoreEntityTokenLocking>().Any() ;

                    _ignoreEntityTokenLockingCache.Add(type, ignoreLocking);
                }

                return ignoreLocking | actionToken.IgnoreEntityTokenLocking;
            }
        }
    }
}
