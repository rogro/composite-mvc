/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Composite.Data;
using Composite.Data.Types;


namespace Composite.C1Console.Security
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class UserGroupPerspectiveFacade
	{
        /// <exclude />
        public static IEnumerable<string> GetSerializedEntityTokens(string username)
        {
            foreach (Guid userGroupId in UserGroupFacade.GetUserGroupIds(username))
            {
                foreach (string serializedEntityToken in GetSerializedEntityTokens(userGroupId))
                {
                    yield return serializedEntityToken;
                }
            }
        }



        /// <exclude />
        public static IEnumerable<string> GetSerializedEntityTokens(Guid userGroupId)
        {
            return
                (from activePerspective in DataFacade.GetData<IUserGroupActivePerspective>()
                 where activePerspective.UserGroupId == userGroupId
                 select activePerspective.SerializedEntityToken).ToList();
        }



        /// <exclude />
        public static IEnumerable<EntityToken> GetEntityTokens(Guid userGroupId)
        {
            return
                GetSerializedEntityTokens(userGroupId).Select(f => EntityTokenSerializer.Deserialize(f));
        }



        /// <exclude />
        public static void SetEntityTokens(Guid userGroupId, IEnumerable<EntityToken> entityTokens)
        {
            SetSerializedEntityTokens(userGroupId, entityTokens.Select(f => EntityTokenSerializer.Serialize(f)));
        }



        /// <exclude />
        public static void SetSerializedEntityTokens(Guid userGroupId, IEnumerable<string> serializedEntityTokens)
        {
            DataFacade.Delete<IUserGroupActivePerspective>(f => f.UserGroupId == userGroupId);

            foreach (string serializedEntityToken in serializedEntityTokens)
            {
                IUserGroupActivePerspective activePerspective = DataFacade.BuildNew<IUserGroupActivePerspective>();

                activePerspective.Id = Guid.NewGuid();
                activePerspective.UserGroupId = userGroupId;
                activePerspective.SerializedEntityToken = serializedEntityToken;
                
                DataFacade.AddNew<IUserGroupActivePerspective>(activePerspective);
            }
        }



        /// <exclude />
        public static void DeleteAll(Guid userGroupId)
        {
            DataFacade.Delete<IUserGroupActivePerspective>(f => f.UserGroupId == userGroupId);
        }
	}
}
