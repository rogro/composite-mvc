/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using Composite.C1Console.Events;
using Composite.Core;



namespace Composite.C1Console.Security
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [EditorBrowsable(EditorBrowsableState.Never)] 
    public delegate void DirtyHooksCallbackDelegate();


    /// <summary>    
    /// </summary>
    /// <exclude />
    [EditorBrowsable(EditorBrowsableState.Never)] 
    public static class HookingFacade
    {
        /// <exclude />
        public delegate void NewElementProviderRootEntitiesDelegate(HookingFacadeEventArgs hookingFacadeEventArgs);


        private static IHookingFacade _hookingFacade = new HookingFacadeImpl();



        static HookingFacade()
        {

            GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlushEvent);
        }



        /// <exclude />
        public static void EnsureInitialization()
        {
            _hookingFacade.EnsureInitialization();
        }



        /// <exclude />
        public static IEnumerable<EntityToken> GetHookies(EntityToken hooker)
        {
            try
            {
                using (GlobalInitializerFacade.CoreIsInitializedScope)
                {
                    return _hookingFacade.GetHookies(hooker);
                }
            }
            catch (Exception ex)
            {
                Log.LogCritical("HookingFacade", ex);

                GlobalInitializerFacade.FatalResetTheSytem();

                throw;
            }
        }



        /// <exclude />
        public static IEnumerable<EntityToken> GetParentHookers()
        {
            try
            {
                using (GlobalInitializerFacade.CoreIsInitializedScope)
                {
                    return _hookingFacade.GetParentHookers();
                }
            }
            catch (Exception ex)
            {
                Log.LogCritical("HookingFacade", ex);

                GlobalInitializerFacade.FatalResetTheSytem();

                throw;
            }
        }



        /// <exclude />
        public static IEnumerable<EntityToken> GetParentToChildHooks(EntityToken parentEntityToken)
        {
            try
            {
                using (GlobalInitializerFacade.CoreIsInitializedScope)
                {
                    return _hookingFacade.GetParentToChildHooks(parentEntityToken);
                }
            }
            catch (Exception ex)
            {
                Log.LogCritical("HookingFacade", ex);

                GlobalInitializerFacade.FatalResetTheSytem();

                throw;
            }
        }



        /// <exclude />
        public static void RemoveHook(EntityTokenHook entityTokenHook)
        {
            try
            {
                using (GlobalInitializerFacade.CoreIsInitializedScope)
                {
                    _hookingFacade.RemoveHook(entityTokenHook);
                }
            }
            catch (Exception ex)
            {
                Log.LogCritical("HookingFacade", ex);

                GlobalInitializerFacade.FatalResetTheSytem();

                throw;
            }

        }



        /// <exclude />
        public static void RemoveHooks(IEnumerable<EntityTokenHook> entityTokenHooks)
        {
            try
            {
                using (GlobalInitializerFacade.CoreIsInitializedScope)
                {
                    _hookingFacade.RemoveHooks(entityTokenHooks);
                }
            }
            catch (Exception ex)
            {
                Log.LogCritical("HookingFacade", ex);

                GlobalInitializerFacade.FatalResetTheSytem();

                throw;
            }
        }



        /// <exclude />
        public static void AddHook(EntityTokenHook entityTokenHook)
        {
            try
            {
                using (GlobalInitializerFacade.CoreIsInitializedScope)
                {
                    _hookingFacade.AddHook(entityTokenHook);
                }
            }
            catch (Exception ex)
            {
                Log.LogCritical("HookingFacade", ex);

                GlobalInitializerFacade.FatalResetTheSytem();

                throw;
            }
        }




        /// <exclude />
        public static void AddHooks(IEnumerable<EntityTokenHook> entityTokenHooks)
        {
            try
            {
                using (GlobalInitializerFacade.CoreIsInitializedScope)
                {
                    _hookingFacade.AddHooks(entityTokenHooks);
                }
            }
            catch (Exception ex)
            {
                Log.LogCritical("HookingFacade", ex);

                GlobalInitializerFacade.FatalResetTheSytem();

                throw;
            }
        }


        /// <summary>
        /// Use this method to register a callback method that will be called when fully updated hooks are needed.
        /// </summary>
        /// <param name="id">
        /// The id of the callback method. This ensures that only one callback is registered
        /// for each id.
        /// </param>
        /// <param name="dirtyHooksCallbackDelegate"></param>
        /// <exclude />
        public static void RegisterDirtyCallback(string id, DirtyHooksCallbackDelegate dirtyHooksCallbackDelegate)
        {
            using (GlobalInitializerFacade.CoreIsInitializedScope)
            {
                _hookingFacade.RegisterDirtyCallback(id, dirtyHooksCallbackDelegate);
            }
        }



        /// <exclude />
        public static void SubscribeToNewElementProviderRootEntitiesEvent(NewElementProviderRootEntitiesDelegate newElementProviderRootEntitiesDelegate)
        {
            using (GlobalInitializerFacade.CoreIsInitializedScope)
            {
                _hookingFacade.SubscribeToNewElementProviderRootEntitiesEvent(newElementProviderRootEntitiesDelegate);
            }
        }



        /// <exclude />
        public static void UnsubscribeFromNewElementProviderRootEntitiesEvent(NewElementProviderRootEntitiesDelegate newElementProviderRootEntitiesDelegate)
        {
            using (GlobalInitializerFacade.CoreIsInitializedScope)
            {
                _hookingFacade.UnsubscribeFromNewElementProviderRootEntitiesEvent(newElementProviderRootEntitiesDelegate);
            }
        }



        /// <exclude />
        public static void FireNewElementProviderRootEntitiesEvent(string providerName)
        {
            using (GlobalInitializerFacade.CoreIsInitializedScope)
            {
                _hookingFacade.FireNewElementProviderRootEntitiesEvent(providerName);
            }
        }



        private static void OnFlushEvent(FlushEventArgs args)
        {
            _hookingFacade.Flush();
        }
    }
}
