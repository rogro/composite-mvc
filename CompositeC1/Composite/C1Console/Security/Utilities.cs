/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;

namespace Composite.C1Console.Security
{
    internal static class Utilities
    {
        public static void ParseUserLoginString(string windowsAuthenticatedFullName, out string userName, out string domainName)
        {
            ParseUserLoginString(windowsAuthenticatedFullName, out userName, out domainName, true);
        }


        public static bool TryParseUserLoginString(string windowsAuthenticatedFullName, out string userName, out string domainName)
        {
            return ParseUserLoginString(windowsAuthenticatedFullName, out userName, out domainName, false);
        }


        private static bool ParseUserLoginString(string windowsAuthenticatedFullName, out string userName, out string domainName, bool throwException)
        {
            // Format: [domain]\[username] or [username]
            if (string.IsNullOrEmpty(windowsAuthenticatedFullName))
            {
                userName = null;
                domainName = null;
                return HandleError("Authenticated user name can not be null or empty", "windowsAuthenticatedFullName", throwException);
            }

            if (windowsAuthenticatedFullName.IndexOf(@"\") == -1)
            {
                domainName = "";
                userName = windowsAuthenticatedFullName;
            }
            else
            {
                char[] userLoginSeparator = { '\\' };
                string[] userLoginParts = windowsAuthenticatedFullName.Split(userLoginSeparator);

                if (2 == userLoginParts.Length && string.IsNullOrEmpty(userLoginParts[0]) == false && string.IsNullOrEmpty(userLoginParts[1]) == false)
                {
                    domainName = userLoginParts[0];
                    userName = userLoginParts[1];
                }
                else
                {
                    userName = null;
                    domainName = null;
                    return HandleError("Unexpected user login format", "windowsAuthenticatedFullName", throwException);
                }
            }

            return true;
        }


        private static bool HandleError(string message, string paramName, bool throwException)
        {
            if (throwException)
            {
                throw new ArgumentException(message, paramName);
            }

            return false;
        }        
    }
}
