/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.C1Console.Events;
using Composite.Core.Collections.Generic;

namespace Composite.C1Console.Security.Foundation
{
    internal static class SecurityAncestorProviderCache
    {
        private static Hashtable<Type, ISecurityAncestorProvider> _securityAncestorProviderCache = new Hashtable<Type, ISecurityAncestorProvider>();
        private static object _lock = new object();


        static SecurityAncestorProviderCache()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlush);
        }


        public static ISecurityAncestorProvider GetSecurityAncestorProvider(EntityToken entityToken)
        {
            Verify.ArgumentNotNull(entityToken, "entityToken");

            ISecurityAncestorProvider securityAncestorProvider;

            Type entityTokenType = entityToken.GetType();

            if (_securityAncestorProviderCache.TryGetValue(entityTokenType, out securityAncestorProvider) == false)
            {
                lock (_lock)
                {
                    if (_securityAncestorProviderCache.TryGetValue(entityTokenType, out securityAncestorProvider) == false)
                    {
                        object[] attributes = entityTokenType.GetCustomAttributes(typeof(SecurityAncestorProviderAttribute), true);

                        Verify.That(attributes.Length > 0, "Missing {0} attribute on the entity token {1}", typeof(SecurityAncestorProviderAttribute), entityTokenType);

                        var attribute = (SecurityAncestorProviderAttribute)attributes[0];

                        Verify.IsNotNull(attribute.SecurityAncestorProviderType, "Security ancestor provider type can not be null on the entity token {0}", entityTokenType);
                        Verify.That(typeof(ISecurityAncestorProvider).IsAssignableFrom(attribute.SecurityAncestorProviderType), "Security ancestor provider '{0}' should implement the interface '{1}'", attribute.SecurityAncestorProviderType, typeof(ISecurityAncestorProvider));

                        securityAncestorProvider = (ISecurityAncestorProvider)Activator.CreateInstance(attribute.SecurityAncestorProviderType);

                        _securityAncestorProviderCache.Add(entityTokenType, securityAncestorProvider);
                    }
                }
            }


            return securityAncestorProvider;
        }



        private static void Flush()
        {
            lock (_lock)
            {
                _securityAncestorProviderCache = new Hashtable<Type, ISecurityAncestorProvider>();
            }
        }



        private static void OnFlush(FlushEventArgs args)
        {
            Flush();
        }
    }
}
