/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Configuration;
using Composite.Core.Collections.Generic;
using Composite.C1Console.Events;
using Composite.C1Console.Security.Plugins.LoginProvider;
using Composite.C1Console.Security.Plugins.LoginProvider.Runtime;


namespace Composite.C1Console.Security.Foundation.PluginFacades
{
    internal class LoginProviderPluginFacade
    {
        private static readonly ResourceLocker<Resources> _resourceLocker = new ResourceLocker<Resources>(new Resources(), Resources.DoInitializeResources);



        static LoginProviderPluginFacade()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(args => Flush());
        }



        public static IEnumerable<string> AllUsernames
        {
            get
            {
                return _resourceLocker.Resources.Plugin.AllUsernames;
            }
        }


        public static bool CanSetUserPassword
        {
            get
            {
                return _resourceLocker.Resources.Plugin.CanSetUserPassword;
            }
        }


        public static bool CanAddNewUser
        {
            get
            {
                return _resourceLocker.Resources.Plugin.CanAddNewUser;
            }
        }


        public static bool UsersExists
        {
            get
            {
                return _resourceLocker.Resources.Plugin.UsersExists;
            }
        }


        public static LoginResult FormValidateUser(string userName, string password)
        {
            IFormLoginProvider validator = _resourceLocker.Resources.Plugin as IFormLoginProvider;

            return validator.Validate(userName, password);
        }


        public static void FormSetUserPassword(string userName, string password)
        {
            IFormLoginProvider validator = _resourceLocker.Resources.Plugin as IFormLoginProvider;

            validator.SetUserPassword(userName, password);
        }


        public static void FormAddNewUser(string userName, string password, string group, string email)
        {
            IFormLoginProvider validator = _resourceLocker.Resources.Plugin as IFormLoginProvider;

            if (validator.CanAddNewUser == false) throw new InvalidOperationException("Login provider does not support adding users");

            validator.AddNewUser(userName, password, group, email);
        }


        public static bool WindowsValidateUser(string userName, string domainName)
        {
            IWindowsLoginProvider validator = _resourceLocker.Resources.Plugin as IWindowsLoginProvider;

            return validator.Validate(userName, domainName);
        }


        /// <summary>
        /// Returns a Type object containing the type of the current plugin
        /// </summary>
        /// <returns></returns>
        public static bool CheckType<T>()
        {
            return _resourceLocker.Resources.Plugin is T;
        }



        public static Type GetValidationPluginType()
        {
            return _resourceLocker.Resources.Plugin.GetType();
        }



        private static void Flush()
        {
            _resourceLocker.ResetInitialization();
        }



        private static void HandleConfigurationError(Exception ex)
        {
            Flush();

            throw new ConfigurationErrorsException(string.Format("Failed to load the configuration section '{0}' from the configuration.", LoginProviderSettings.SectionName), ex);
        }



        private sealed class Resources
        {
            public LoginProviderFactory Factory { get; set; }
            public ILoginProvider Plugin { get; set; }

            public static void DoInitializeResources(Resources resources)
            {
                try
                {
                    resources.Factory = new LoginProviderFactory();
                    resources.Plugin = resources.Factory.CreateDefault();                    
                }
                catch (ArgumentException ex)
                {
                    HandleConfigurationError(ex);
                }
                catch (ConfigurationErrorsException ex)
                {
                    HandleConfigurationError(ex);
                }
            }
        }
    }
}