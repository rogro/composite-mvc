/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Configuration;
using Composite.Core.Collections.Generic;
using Composite.C1Console.Events;
using Composite.C1Console.Security.Plugins.UserGroupPermissionDefinitionProvider;
using Composite.C1Console.Security.Plugins.UserGroupPermissionDefinitionProvider.Runtime;
using Composite.Core.Extensions;


namespace Composite.C1Console.Security.Foundation.PluginFacades
{
    internal static class UserGroupPermissionDefinitionProviderPluginFacade
    {
        private static ResourceLocker<Resources> _resourceLocker = new ResourceLocker<Resources>(new Resources(), Resources.DoInitializeResources);



        static UserGroupPermissionDefinitionProviderPluginFacade()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlushEvent);
        }


        
        public static IEnumerable<UserGroupPermissionDefinition> AllUserGroupPermissionDefinitions
        {
            get
            {
                    return _resourceLocker.Resources.Plugin.AllUserGroupPermissionDefinitions;
            }
        }


        
        public static bool CanAlterDefinitions
        {
            get
            {
                    return _resourceLocker.Resources.Plugin.CanAlterDefinitions;
            }
        }



        public static IEnumerable<UserGroupPermissionDefinition> GetPermissionsByUserGroup(Guid userGroupId)
        {
                return _resourceLocker.Resources.Plugin.GetPermissionsByUserGroup(userGroupId);
        }


        
        public static void SetUserGroupPermissionDefinition(UserGroupPermissionDefinition userGroupPermissionDefinition)
        {
            Verify.ArgumentNotNull(userGroupPermissionDefinition, "userGroupPermissionDefinition");
            Verify.ArgumentCondition(!userGroupPermissionDefinition.SerializedEntityToken.IsNullOrEmpty(), "userGroupPermissionDefinition", "SerializedEntityToken is empty");
            Verify.ArgumentCondition(userGroupPermissionDefinition.UserGroupId != Guid.Empty, "userGroupPermissionDefinition", "Is Guid.Empty");

             _resourceLocker.Resources.Plugin.SetUserGroupPermissionDefinition(userGroupPermissionDefinition);
        }



        public static void RemoveUserGroupPermissionDefinition(Guid userGroupID, string serializedEntityToken)
        {
            if (userGroupID == Guid.Empty) throw new ArgumentNullException("userGroupID");
            if (string.IsNullOrEmpty(serializedEntityToken)) throw new ArgumentNullException("serializedEntityToken");

            _resourceLocker.Resources.Plugin.RemoveUserGroupPermissionDefinition(userGroupID, serializedEntityToken);
        }



        private static void Flush()
        {
            _resourceLocker.ResetInitialization();
        }



        private static void OnFlushEvent(FlushEventArgs args)
        {
            Flush();
        }



        private static void HandleConfigurationError(Exception ex)
        {
            Flush();

            throw new ConfigurationErrorsException(string.Format("Failed to load the configuration section '{0}' from the configuration.", UserGroupPermissionDefinitionProviderSettings.SectionName), ex);
        }



        private sealed class Resources
        {
            public UserGroupPermissionDefinitionProviderFactory Factory { get; set; }
            public IUserGroupPermissionDefinitionProvider Plugin { get; set; }

            public static void DoInitializeResources(Resources resources)
            {
                try
                {
                    resources.Factory = new UserGroupPermissionDefinitionProviderFactory();
                    resources.Plugin = resources.Factory.CreateDefault();
                }
                catch (ArgumentException ex)
                {
                    HandleConfigurationError(ex);
                }
                catch (ConfigurationErrorsException ex)
                {
                    HandleConfigurationError(ex);
                }
            }
        }
    }
}
