/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Configuration;
using System.Net;
using Composite.Core.Collections.Generic;
using Composite.Core.Configuration;
using Composite.C1Console.Events;
using Composite.C1Console.Security.BuildinPlugins.BuildinLoginSessionStore;
using Composite.C1Console.Security.Plugins.LoginSessionStore;
using Composite.C1Console.Security.Plugins.LoginSessionStore.Runtime;


namespace Composite.C1Console.Security.Foundation.PluginFacades
{
    internal class LoginSessionStorePluginFacade
    {
        private static ResourceLocker<Resources> _resourceLocker = new ResourceLocker<Resources>(new Resources(), Resources.DoInitializeResources);



        static LoginSessionStorePluginFacade()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(args => Flush());
        }



        public static bool HasConfiguration()
        {
            return (ConfigurationServices.ConfigurationSource != null) &&
                   (ConfigurationServices.ConfigurationSource.GetSection(LoginSessionStoreSettings.SectionName) != null);
        }



        public static bool CanPersistAcrossSessions
        {
            get
            {
                using (_resourceLocker.ReadLocker)
                {
                    return _resourceLocker.Resources.Provider.CanPersistAcrossSessions;
                }
            }
        }


        public static void StoreUsername(string userName, bool persistAcrossSessions)
        {
            using (_resourceLocker.ReadLocker)
            {
                _resourceLocker.Resources.Provider.StoreUsername(userName, persistAcrossSessions);
            }
        }


        public static void FlushUsername()
        {
            using (_resourceLocker.ReadLocker)
            {
                _resourceLocker.Resources.Provider.FlushUsername();
            }
        }



        public static string StoredUsername
        {
            get
            {
                 return _resourceLocker.Resources.Provider.StoredUsername;
            }
        }



        public static IPAddress UserIpAddress
        {
            get
            {
                using (_resourceLocker.ReadLocker)
                {
                    return _resourceLocker.Resources.Provider.UserIpAddress;
                }
            }
        }



        private static void Flush()
        {
            _resourceLocker.ResetInitialization();
        }



        private static void HandleConfigurationError(Exception ex)
        {
            Flush();

            throw new ConfigurationErrorsException(string.Format("Failed to load the configuration section '{0}' from the configuration.", LoginSessionStoreSettings.SectionName), ex);
        }



        private sealed class Resources
        {
            public LoginSessionStoreFactory Factory { get; set; }
            public ILoginSessionStore Provider { get; set; }

            public static void DoInitializeResources(Resources resources)
            {
                if (LoginSessionStorePluginFacade.HasConfiguration())
                {
                    try
                    {
                        resources.Factory = new LoginSessionStoreFactory();
                        resources.Provider = resources.Factory.CreateDefault();
                    }
                    catch (ArgumentException ex)
                    {
                        HandleConfigurationError(ex);
                    }
                    catch (ConfigurationErrorsException ex)
                    {
                        HandleConfigurationError(ex);
                    }
                }
                else if (RuntimeInformation.IsUnittest)
                {
                    // This is a fall bakc for unittests
                    resources.Provider = new BuildinLoginSessionStore();
                }
            }
        }
    }
}
