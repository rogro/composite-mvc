/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Configuration;
using Composite.Core.Collections.Generic;
using Composite.Core.Configuration;
using Composite.C1Console.Events;
using Composite.C1Console.Security.BuildinPlugins.BuildinUserPermissionDefinitionProvider;
using Composite.C1Console.Security.Plugins.UserPermissionDefinitionProvider;
using Composite.C1Console.Security.Plugins.UserPermissionDefinitionProvider.Runtime;


namespace Composite.C1Console.Security.Foundation.PluginFacades
{
    internal static class UserPermissionDefinitionProviderPluginFacade
    {
        private static ResourceLocker<Resources> _resourceLocker = new ResourceLocker<Resources>(new Resources(), Resources.DoInitializeResources);



        static UserPermissionDefinitionProviderPluginFacade()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlushEvent);
        }



        public static bool HasConfiguration()
        {
            return (ConfigurationServices.ConfigurationSource != null) &&
                   (ConfigurationServices.ConfigurationSource.GetSection(UserPermissionDefinitionProviderSettings.SectionName) != null);
        }



        public static IEnumerable<UserPermissionDefinition> AllUserPermissionDefinitions
        {
            get
            {
                return _resourceLocker.Resources.Plugin.AllUserPermissionDefinitions;
            }
        }



        public static bool CanAlterDefinitions
        {
            get
            {
                return _resourceLocker.Resources.Plugin.CanAlterDefinitions;
            }
        }



        public static void SetUserPermissionDefinition(UserPermissionDefinition userPermissionDefinition)
        {
            if (userPermissionDefinition == null) throw new ArgumentNullException("userPermissionDefinition");
            if (string.IsNullOrEmpty(userPermissionDefinition.SerializedEntityToken)) throw new ArgumentNullException("userPermissionDefinition");
            if (string.IsNullOrEmpty(userPermissionDefinition.Username)) throw new ArgumentNullException("userPermissionDefinition");

            _resourceLocker.Resources.Plugin.SetUserPermissionDefinition(userPermissionDefinition);
        }



        public static void RemoveUserPermissionDefinition(UserToken userToken, string serializedEntityToken)
        {
            if (userToken == null) throw new ArgumentNullException("userToken");
            if (string.IsNullOrEmpty(serializedEntityToken)) throw new ArgumentNullException("serializedEntityToken");

            _resourceLocker.Resources.Plugin.RemoveUserPermissionDefinition(userToken, serializedEntityToken);
        }



        public static IEnumerable<UserPermissionDefinition> GetPermissionsByUser(string userName)
        {
            return _resourceLocker.Resources.Plugin.GetPermissionsByUser(userName);
        }



        private static void Flush()
        {
            _resourceLocker.ResetInitialization();
        }



        private static void OnFlushEvent(FlushEventArgs args)
        {
            Flush();
        }



        private static void HandleConfigurationError(Exception ex)
        {
            Flush();

            throw new ConfigurationErrorsException(string.Format("Failed to load the configuration section '{0}' from the configuration.", UserPermissionDefinitionProviderSettings.SectionName), ex);
        }



        private sealed class Resources
        {
            public UserPermissionDefinitionProviderFactory Factory { get; set; }
            public IUserPermissionDefinitionProvider Plugin { get; set; }

            public static void DoInitializeResources(Resources resources)
            {
                if (HasConfiguration())
                {
                    try
                    {
                        resources.Factory = new UserPermissionDefinitionProviderFactory();
                        resources.Plugin = resources.Factory.CreateDefault();
                    }
                    catch (ArgumentException ex)
                    {
                        HandleConfigurationError(ex);
                    }
                    catch (ConfigurationErrorsException ex)
                    {
                        HandleConfigurationError(ex);
                    }
                }
                else
                {
                    resources.Plugin = new BuildinUserPermissionDefinitionProvider();
                }
            }
        }
    }
}
