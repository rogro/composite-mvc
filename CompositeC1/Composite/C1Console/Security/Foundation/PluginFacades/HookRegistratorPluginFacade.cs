/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;
using System.Configuration;
using Composite.Core.Collections.Generic;
using Composite.C1Console.Events;
using Composite.C1Console.Security.Plugins.HookRegistrator;
using Composite.C1Console.Security.Plugins.HookRegistrator.Runtime;


namespace Composite.C1Console.Security.Foundation.PluginFacades
{
    internal static class HookRegistratorPluginFacade
    {
        private static ResourceLocker<Resources> _resourceLocker = new ResourceLocker<Resources>(new Resources(), Resources.Initialize);



        private static object _lock = new object();


        static HookRegistratorPluginFacade()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlushEvent);
        }



        public static IEnumerable<EntityTokenHook> GetHooks(string pluginName)
        {
                using (_resourceLocker.Locker)
                {
                    return GetHookRegistratorProvider(pluginName).GetHooks();
                }
        }



        private static IHookRegistrator GetHookRegistratorProvider(string pluginName)
        {
            using (_resourceLocker.Locker)
            {
                IHookRegistrator hookRegistrator;

                if (_resourceLocker.Resources.HookRegistratorCache.TryGetValue(pluginName, out hookRegistrator) == false)
                {
                    try
                    {
                        hookRegistrator = _resourceLocker.Resources.Factory.Create(pluginName);

                        _resourceLocker.Resources.HookRegistratorCache.Add(pluginName, hookRegistrator);
                    }
                    catch (ArgumentException ex)
                    {
                        HandleConfigurationError(ex);
                    }
                    catch (ConfigurationErrorsException ex)
                    {
                        HandleConfigurationError(ex);
                    }
                }

                return hookRegistrator;
            }
        }



        private static void Flush()
        {
            _resourceLocker.ResetInitialization();
        }



        private static void OnFlushEvent(FlushEventArgs args)
        {
            Flush();
        }






        private static void HandleConfigurationError(Exception ex)
        {
            Flush();

            throw ex;

            /*            ConfigurationErrorsException newEx = new ConfigurationErrorsException(string.Format("Failed to load the configuration section '{0}' from the configuration.", LoginProviderSettings.SectionName), ex);
                        if (ExceptionService.HandleException(newEx, Policy.Plugin) == HandleResult.Rethrow)
                        {
                            throw newEx;
                        }*/
        }



        private sealed class Resources
        {
            public HookRegistratorFactory Factory { get; set; }
            public Dictionary<string, IHookRegistrator> HookRegistratorCache { get; set; }

            public static void Initialize(Resources resources)
            {
                try
                {
                    resources.Factory = new HookRegistratorFactory();
                    resources.HookRegistratorCache = new Dictionary<string, IHookRegistrator>();
                }
                catch (NullReferenceException ex)
                {
                    HandleConfigurationError(ex);
                }
                catch (ConfigurationErrorsException ex)
                {
                    HandleConfigurationError(ex);
                }
            }
        }
    }
}
