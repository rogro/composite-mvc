/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;
using System.Linq;
using Composite.Core.Caching;


namespace Composite.C1Console.Security.Foundation
{
	internal static class PermissionTypeFacadeCaching
	{
        private static readonly string CurrentPermissionTypeCachingKey = "PermissionTypeFacadeCaching_CurrentPermissionTypeCachingKey";
        private static readonly string InheritedPermissionsTypeCachingKey = "PermissionTypeFacadeCaching_InheritedPermissionsTypeCachingKey";
        private static readonly string UserPermissionTypeCachingKey = "PermissionTypeFacadeCaching_UserPermissionTypeCachingKey";
        private static readonly string UserGroupPermissionTypeCachingKey = "UserGroupPermissionTypeCachingKey_UserPermissionTypeCachingKey";


        public static IReadOnlyCollection<PermissionType> GetCurrentPermissionTypes(UserToken userToken, EntityToken entityToken)
        {
            return GetFromCache(userToken, entityToken, CurrentPermissionTypeCachingKey);
        }



        public static void SetCurrentPermissionTypes(UserToken userToken, EntityToken entityToken, IReadOnlyCollection<PermissionType> permissionTypes)
        {
            SetToCache(userToken, entityToken, permissionTypes, CurrentPermissionTypeCachingKey);
        }



        public static IReadOnlyCollection<PermissionType> GetInheritedPermissionsTypes(UserToken userToken, EntityToken entityToken)
        {
            return GetFromCache(userToken, entityToken, InheritedPermissionsTypeCachingKey);
        }



        public static void SetInheritedPermissionsTypes(UserToken userToken, EntityToken entityToken, IReadOnlyCollection<PermissionType> permissionTypes)
        {
            SetToCache(userToken, entityToken, permissionTypes, InheritedPermissionsTypeCachingKey);
        }



        public static IReadOnlyCollection<PermissionType> GetUserPermissionTypes(UserToken userToken, EntityToken entityToken)
        {
            return GetFromCache(userToken, entityToken, UserPermissionTypeCachingKey);
        }



        public static void SetUserPermissionTypes(UserToken userToken, EntityToken entityToken, IReadOnlyCollection<PermissionType> permissionTypes)
        {
            SetToCache(userToken, entityToken, permissionTypes, UserPermissionTypeCachingKey);
        }



        public static IReadOnlyCollection<PermissionType> GetUserGroupPermissionTypes(UserToken userToken, EntityToken entityToken)
        {
            return GetFromCache(userToken, entityToken, UserGroupPermissionTypeCachingKey);
        }



        public static void SetUserGroupPermissionTypes(UserToken userToken, EntityToken entityToken, IReadOnlyCollection<PermissionType> permissionTypes)
        {
            SetToCache(userToken, entityToken, permissionTypes, UserGroupPermissionTypeCachingKey);
        }



        public static bool CachingWorking
        {
            get
            {
                return RequestLifetimeCache.HasKey(UserPermissionTypeCachingKey) && RequestLifetimeCache.HasKey(UserGroupPermissionTypeCachingKey);
            }
        }


        private static void SetToCache(UserToken userToken, EntityToken entityToken, IReadOnlyCollection<PermissionType> permissionTypes, object cachingKey)
        {
            // Using RequestLifetimeCache and there for no thread locking /MRJ

            Dictionary<UserToken, Dictionary<EntityToken, IReadOnlyCollection<PermissionType>>> permissionTypeCache;

            if (RequestLifetimeCache.HasKey(cachingKey))
            {
                permissionTypeCache = RequestLifetimeCache.TryGet<Dictionary<UserToken, Dictionary<EntityToken, IReadOnlyCollection<PermissionType>>>>(cachingKey);
            }
            else
            {
                permissionTypeCache = new Dictionary<UserToken, Dictionary<EntityToken, IReadOnlyCollection<PermissionType>>>();

                RequestLifetimeCache.Add(cachingKey, permissionTypeCache);
            }

            Dictionary<EntityToken, IReadOnlyCollection<PermissionType>> entityTokenPermissionTypes;
            if (!permissionTypeCache.TryGetValue(userToken, out entityTokenPermissionTypes))
            {
                entityTokenPermissionTypes = new Dictionary<EntityToken, IReadOnlyCollection<PermissionType>>();
                permissionTypeCache.Add(userToken, entityTokenPermissionTypes);
            }

            if (!entityTokenPermissionTypes.ContainsKey(entityToken))
            {
                entityTokenPermissionTypes.Add(entityToken, permissionTypes);
            }
            else
            {
                entityTokenPermissionTypes[entityToken] = entityTokenPermissionTypes[entityToken].Concat(permissionTypes).Distinct().ToList();
            }
        }



        private static IReadOnlyCollection<PermissionType> GetFromCache(UserToken userToken, EntityToken entityToken, object cachingKey)
        {
            // Using RequestLifetimeCache and there for no thread locking /MRJ

            Dictionary<UserToken, Dictionary<EntityToken, IReadOnlyCollection<PermissionType>>> permissionTypeCache;

            if (RequestLifetimeCache.HasKey(cachingKey))
            {
                permissionTypeCache = RequestLifetimeCache.TryGet<Dictionary<UserToken, Dictionary<EntityToken, IReadOnlyCollection<PermissionType>>>>(cachingKey);
            }
            else
            {
                permissionTypeCache = new Dictionary<UserToken, Dictionary<EntityToken, IReadOnlyCollection<PermissionType>>>();

                RequestLifetimeCache.Add(cachingKey, permissionTypeCache);
            }

            Dictionary<EntityToken, IReadOnlyCollection<PermissionType>> entityTokenPermissionTypes;
            if (!permissionTypeCache.TryGetValue(userToken, out entityTokenPermissionTypes))
            {
                entityTokenPermissionTypes = new Dictionary<EntityToken, IReadOnlyCollection<PermissionType>>();
                permissionTypeCache.Add(userToken, entityTokenPermissionTypes);
            }

            IReadOnlyCollection<PermissionType> permissionTypes;

            entityTokenPermissionTypes.TryGetValue(entityToken, out permissionTypes);

            return permissionTypes;
        }
	}
}
