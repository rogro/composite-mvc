/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using Composite.Core.Linq;


namespace Composite.C1Console.Security
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [DebuggerDisplay("Hooker = {Hooker}")]
	public sealed class EntityTokenHook
	{
        private List<EntityToken> _hooks = new List<EntityToken>();


        /// <exclude />
        public EntityTokenHook(EntityToken hooker)
        {
            Verify.ArgumentNotNull(hooker, "hooker");

            this.Hooker = hooker;
        }



        /// <exclude />
        public EntityToken Hooker
        {
            get;
            private set;
        }



        /// <exclude />
        public IEnumerable<EntityToken> Hookies
        {
            get
            {
                return _hooks;
            }
        }



        /// <exclude />
        public void AddHookie(EntityToken hook)
        {
            Verify.ArgumentNotNull(hook, "hook");

            _hooks.Add(hook);
        }



        /// <exclude />
        public void AddHookies(IEnumerable<EntityToken> hooks)
        {
            Verify.ArgumentNotNull(hooks, "hooks");

            IEnumerable<EntityToken> resolvedHooks = hooks.Evaluate();

            Verify.ArgumentCondition(!resolvedHooks.Contains(null), "hooks", "The collection contains one or more null values");

            _hooks.AddRange(resolvedHooks);
        }
	}
}
