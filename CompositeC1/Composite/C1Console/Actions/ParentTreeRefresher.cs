/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using Composite.C1Console.Events;
using Composite.Core.Logging;
using Composite.C1Console.Security;


namespace Composite.C1Console.Actions
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class ParentTreeRefresher
	{
        private bool _postRefreshMessegesCalled = false;
        private FlowControllerServicesContainer _flowControllerServicesContainer;


        /// <exclude />
        public ParentTreeRefresher(FlowControllerServicesContainer flowControllerServicesContainer)
        {
            if (flowControllerServicesContainer == null) throw new ArgumentNullException("flowControllerServicesContainer");

            _flowControllerServicesContainer = flowControllerServicesContainer;
        }


        /// <exclude />
        public void PostRefreshMesseges(EntityToken childEntityToken)
        {
            PostRefreshMesseges(childEntityToken, 1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="childEntityToken"></param>
        /// <param name="parentLevel">1 means the first parent, 2 means the second, etc.</param>
        public void PostRefreshMesseges(EntityToken childEntityToken, int parentLevel)
        {
            if (childEntityToken == null) throw new ArgumentNullException("childEntityToken");


            if (_postRefreshMessegesCalled)
            {
                throw new InvalidOperationException("Only one PostRefreshMesseges call is allowed");
            }
            else
            {
                _postRefreshMessegesCalled = true;

                RelationshipGraph relationshipGraph = new RelationshipGraph(childEntityToken, RelationshipGraphSearchOption.Both);

                if (relationshipGraph.Levels.Count() > parentLevel)
                {
                    RelationshipGraphLevel relationshipGraphLevel = relationshipGraph.Levels.ElementAt(parentLevel);

                    IManagementConsoleMessageService messageService = _flowControllerServicesContainer.GetService<IManagementConsoleMessageService>();

                    foreach (EntityToken entityToken in relationshipGraphLevel.AllEntities)
                    {
                        messageService.RefreshTreeSection(entityToken);
                        LoggingService.LogVerbose("FirstParentTreeRefresher", string.Format("Refreshing EntityToken: Type = {0}, Source = {1}, Id = {2}, EntityTokenType = {3}", entityToken.Type, entityToken.Source, entityToken.Id, entityToken.GetType()));
                    }
                }                
            }
        }
	}
}
