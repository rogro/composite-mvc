/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using Composite.C1Console.Security;


namespace Composite.C1Console.Actions.Workflows
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    // This is a dummy token, no elements using this token exists!
    public sealed class EntityTokenLockedEntityToken : EntityToken
	{
        private string _lockedByUsername;
        private string _serializedLockedActionToken;
        private string _serializedLockedEntityToken;

        private ActionToken _lockedActionToken = null;
        private EntityToken _lockedEntityToken = null;


        /// <exclude />
        public EntityTokenLockedEntityToken(string lockedByUsername, string serializedLockedActionToken, string serializedLockedEntityToken)
        {
            _lockedByUsername = lockedByUsername;
            _serializedLockedActionToken = serializedLockedActionToken;
            _serializedLockedEntityToken = serializedLockedEntityToken;            
        }


        /// <exclude />
        public string LockedByUsername
        {
            get { return _lockedByUsername; }
        }


        /// <exclude />
        public ActionToken LockedActionToken
        {
            get
            {
                if (_lockedActionToken == null)
                {
                    _lockedActionToken = ActionTokenSerializer.Deserialize(_serializedLockedActionToken);
                }

                return _lockedActionToken;
            }
        }


        /// <exclude />
        public EntityToken LockedEntityToken
        {
            get
            {
                if (_lockedEntityToken == null)
                {
                    _lockedEntityToken = EntityTokenSerializer.Deserialize(_serializedLockedEntityToken);
                }

                return _lockedEntityToken;
            }
        }


        /// <exclude />
        public override string Type
        {
            get { return _lockedByUsername; }
        }


        /// <exclude />
        public override string Source
        {
            get { return _serializedLockedActionToken; }
        }


        /// <exclude />
        public override string Id
        {
            get { return _serializedLockedEntityToken; }
        }


        /// <exclude />
        public override string Serialize()
        {
            return DoSerialize();
        }


        /// <exclude />
        public static EntityToken Deserialize(string serializedEntityToken)
        {
            string type, source, id;

            DoDeserialize(serializedEntityToken, out type, out source, out id);

            return new EntityTokenLockedEntityToken(type, source, id);
        }
    }
}
