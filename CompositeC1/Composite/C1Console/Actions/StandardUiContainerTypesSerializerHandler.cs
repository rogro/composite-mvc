/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.Core.Serialization;


namespace Composite.C1Console.Actions
{
    internal sealed class StandardUiContainerTypesSerializerHandler : ISerializerHandler
    {
        public string Serialize(object objectToSerialize)
        {
            IFlowUiContainerType flowUiContainerType = (IFlowUiContainerType)objectToSerialize;

            if ((flowUiContainerType.ContainerName != "Document") &&
                (flowUiContainerType.ContainerName != "EmptyDocument") &&
                (flowUiContainerType.ContainerName != "Wizard") &&
                (flowUiContainerType.ContainerName != "DataDialog") &&
                (flowUiContainerType.ContainerName != "ConfirmDialog") &&
                (flowUiContainerType.ContainerName != "WarningDialog") &&                
                (flowUiContainerType.ContainerName != "Null"))
            {
                throw new NotSupportedException();
            }


            return flowUiContainerType.ContainerName;
        }

        public object Deserialize(string serializedObject)
        {
            switch (serializedObject)
            {
                case "Document": return StandardUiContainerTypes.Document;
                case "EmptyDocument": return StandardUiContainerTypes.EmptyDocument;
                case "Wizard": return StandardUiContainerTypes.Wizard;
                case "DataDialog": return StandardUiContainerTypes.DataDialog;
                case "ConfirmDialog": return StandardUiContainerTypes.ConfirmDialog;
                case "WarningDialog": return StandardUiContainerTypes.WarningDialog;
                case "Null": return StandardUiContainerTypes.Null;
                default: throw new NotSupportedException();
            }
        }
    }
}
