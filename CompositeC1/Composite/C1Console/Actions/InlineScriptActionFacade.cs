/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Text;
using Composite.C1Console.Security;
using Composite.Core.WebClient.FlowMediators;
using Composite.Core.Serialization;
using Composite.C1Console.Elements;


namespace Composite.C1Console.Actions
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class InlineScriptActionFacade
    {
        /// <exclude />
        public static string GetInlineElementActionScriptCode(EntityToken entityToken, ActionToken actionToken)
        {
            return GetInlineElementActionScriptCode(entityToken, actionToken, new Dictionary<string, string>());
        }


        /// <exclude />
        public static string GetInlineElementActionScriptCode(EntityToken entityToken, ActionToken actionToken, Dictionary<string, string> piggyBag)
        {
            StringBuilder sb = new StringBuilder();

            StringConversionServices.SerializeKeyValuePair(sb, "EntityToken", EntityTokenSerializer.Serialize(entityToken, false));
            StringConversionServices.SerializeKeyValuePair(sb, "ActionToken", ActionTokenSerializer.Serialize(actionToken, true));
            StringConversionServices.SerializeKeyValuePair(sb, "PiggyBag", PiggybagSerializer.Serialize(piggyBag));

            string scriptAction = string.Format(@"SystemAction.invokeInlineAction(""{0}"");", Convert.ToBase64String(Encoding.UTF8.GetBytes(sb.ToString())));

            return scriptAction;
        }


        /// <exclude />
        public static void ExecuteElementScriptAction(string serializedScriptAction, string consoleId)
        {
            string scriptAction = Encoding.UTF8.GetString(Convert.FromBase64String(serializedScriptAction));

            Dictionary<string, string> dic = StringConversionServices.ParseKeyValueCollection(scriptAction);

            if ((dic["EntityToken"] == null) || (dic["ActionToken"] == null) || (dic["PiggyBag"] == null)) throw new ArgumentException("Wrong format", "serializedScriptAction");

            string serializedEntityToken = StringConversionServices.DeserializeValueString(dic["EntityToken"]);
            string serializedActionToken = StringConversionServices.DeserializeValueString(dic["ActionToken"]);
            string serializedPiggyBag = StringConversionServices.DeserializeValueString(dic["PiggyBag"]);

            TreeServicesFacade.ExecuteElementAction(
                "DUMMY",
                serializedEntityToken,
                serializedPiggyBag,
                serializedActionToken,
                consoleId);
        }
    }
}
