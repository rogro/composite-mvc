/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;
using System.Linq;
using Composite.C1Console.Actions.Foundation;
using Composite.C1Console.Actions.Foundation.PluginFacades;
using Composite.Data;
using Composite.Core.Linq;



namespace Composite.C1Console.Actions
{
    internal static class DataActionFacade
    {
        public static Dictionary<DataSourceId, List<DataActionSourceId>> GetActions<T>(IQueryable<T> data)
            where T : class, IData
        {
            var dataSourceIds =
                from d in data
                select d.DataSourceId;

            Dictionary<DataSourceId, List<DataActionSourceId>> result = dataSourceIds.ToDictionary(d => d, d => new List<DataActionSourceId>());

            Type dataInterfaceType = data.GetType().GetGenericArguments()[0];

            foreach (string providerName in DataActionProviderPluginRegistry.GetProviderNames())
            {
                AddActionsByType(result, providerName, dataInterfaceType);

                AddActionsByData<T>(result, providerName, data);
            }

            return result;
        }



        private static void AddActionsByType(Dictionary<DataSourceId, List<DataActionSourceId>> result, string providerName, Type dataInterfaceType)
        {
            IEnumerable<IDataActionId> dataActionIdsByType = DataActionProviderPluginFacade.GetActionsByType(providerName, dataInterfaceType);

            List<DataActionSourceId> dataActionSourceIdsByType = dataActionIdsByType.ToList<IDataActionId, DataActionSourceId>(id => new DataActionSourceId(id, providerName));


            foreach (List<DataActionSourceId> actionList in result.Values)
            {
                actionList.AddRange(dataActionSourceIdsByType);
            }
        }



        private static void AddActionsByData<T>(Dictionary<DataSourceId, List<DataActionSourceId>> result, string providerName, IQueryable<T> data)
            where T : class, IData
        {
            Dictionary<DataSourceId, List<IDataActionId>> dataActionIdsByData = DataActionProviderPluginFacade.GetActionsByData<T>(providerName, data);

            if (dataActionIdsByData != null)
            {
                foreach (KeyValuePair<DataSourceId, List<IDataActionId>> kvp in dataActionIdsByData)
                {
                    List<DataActionSourceId> dataActionSourceIds;
                    if (result.TryGetValue(kvp.Key, out dataActionSourceIds) == false)
                    {
                        throw new InvalidOperationException(string.Format("The DataActionProvider {0} returned a DataSourceId that was not a part of the data query", providerName));
                    }

                    List<DataActionSourceId> dataActionSourceIdsByData = kvp.Value.ToList<IDataActionId, DataActionSourceId>(id => new DataActionSourceId(id, providerName));

                    dataActionSourceIds.AddRange(dataActionSourceIdsByData);
                }
            }
        }
    }
}
