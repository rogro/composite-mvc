/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Composite.C1Console.Security;
using Composite.C1Console.Events;
using Composite.Core.Serialization;

namespace Composite.C1Console.Actions
{

    internal sealed class MessageBoxActionTokenActionExecutor : IActionExecutor
    {
        public FlowToken Execute(EntityToken entityToken, ActionToken actionToken, FlowControllerServicesContainer flowControllerServicesContainer)
        {
            MessageBoxActionToken messageBoxActionToken = (MessageBoxActionToken)actionToken;

            IManagementConsoleMessageService managementConsoleMessageService = flowControllerServicesContainer.GetService<IManagementConsoleMessageService>();

            managementConsoleMessageService.ShowMessage(messageBoxActionToken.DialogType, messageBoxActionToken.Title, messageBoxActionToken.Message);

            return null;
        }
    }



    [ActionExecutor(typeof(MessageBoxActionTokenActionExecutor))]
    internal sealed class MessageBoxActionToken : ActionToken
    {
        private List<PermissionType> _permissionTypes;


        public MessageBoxActionToken(string title, string message, DialogType dialogType)
            :this(title, message, dialogType, new List<PermissionType>() { PermissionType.Add, PermissionType.Administrate, PermissionType.Approve, PermissionType.Delete, PermissionType.Edit, PermissionType.Publish, PermissionType.Read })
        {            
        }


        public MessageBoxActionToken(string title, string message, DialogType dialogType, List<PermissionType> permissionTypes)
        {
            _permissionTypes = permissionTypes;
            this.Title = title;
            this.Message = message;
            this.DialogType = dialogType;
        }


        public string Title { get; private set; }
        public string Message { get; private set; }
        public DialogType DialogType { get; private set; }


        public override IEnumerable<PermissionType> PermissionTypes
        {
            get { return _permissionTypes; }
        }


        public override string Serialize()
        {
            StringBuilder sb = new StringBuilder();

            StringConversionServices.SerializeKeyValuePair(sb, "Title", this.Title);
            StringConversionServices.SerializeKeyValuePair(sb, "Message", this.Message);
            StringConversionServices.SerializeKeyValuePair(sb, "DialogType", this.DialogType.ToString());
            StringConversionServices.SerializeKeyValuePair(sb, "PermissionTypes", _permissionTypes.SerializePermissionTypes());

            return sb.ToString();
        }


        public static ActionToken Deserialize(string serializedData)
        {
            Dictionary<string, string> dic = StringConversionServices.ParseKeyValueCollection(serializedData);

            return new MessageBoxActionToken
            (
                StringConversionServices.DeserializeValueString(dic["Title"]),
                StringConversionServices.DeserializeValueString(dic["Message"]),
                (DialogType)Enum.Parse(typeof(DialogType), StringConversionServices.DeserializeValueString(dic["DialogType"])),
                StringConversionServices.DeserializeValueString(dic["PermissionTypes"]).DesrializePermissionTypes().ToList()
            );
        }
    }

}
