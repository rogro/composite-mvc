/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using Composite.Core.Serialization;


namespace Composite.C1Console.Actions
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class StandardUiContainerTypes
    {
        /// <exclude />
        public static IFlowUiContainerType Document { get { return _document; } }

        /// <exclude />
        public static IFlowUiContainerType EmptyDocument { get { return _emptyDocument; } }

        /// <exclude />
        public static IFlowUiContainerType Wizard { get { return _wizard; } }

        /// <exclude />
        public static IFlowUiContainerType DataDialog { get { return _dataDialog; } }

        /// <exclude />
        public static IFlowUiContainerType ConfirmDialog { get { return _confirmDialog; } }

        /// <exclude />
        public static IFlowUiContainerType WarningDialog { get { return _warningDialog; } }

        /// <exclude />
        public static IFlowUiContainerType Null { get { return _null; } }


        private static DocumentUiContainer _document = new DocumentUiContainer();
        private static EmptyDocumentUiContainer _emptyDocument = new EmptyDocumentUiContainer();
        private static WizardUiContainer _wizard = new WizardUiContainer();
        private static DataDialogUiContainer _dataDialog = new DataDialogUiContainer();
        private static ConfirmDialogUiContainer _confirmDialog = new ConfirmDialogUiContainer();
        private static WarningDialogUiContainer _warningDialog = new WarningDialogUiContainer();
        private static NullUiContainer _null = new NullUiContainer();


        [SerializerHandler(typeof(StandardUiContainerTypesSerializerHandler))]
        private class DocumentUiContainer : IFlowUiContainerType
        {
            internal DocumentUiContainer() { }
            string IFlowUiContainerType.ContainerName { get { return "Document"; } }
            ActionResultResponseType IFlowUiContainerType.ActionResultResponseType { get { return ActionResultResponseType.OpenDocument; } }
        }


        [SerializerHandler(typeof(StandardUiContainerTypesSerializerHandler))]
        private class EmptyDocumentUiContainer : IFlowUiContainerType
        {
            internal EmptyDocumentUiContainer() { }
            string IFlowUiContainerType.ContainerName { get { return "EmptyDocument"; } }
            ActionResultResponseType IFlowUiContainerType.ActionResultResponseType { get { return ActionResultResponseType.OpenDocument; } }
        }


        [SerializerHandler(typeof(StandardUiContainerTypesSerializerHandler))]
        private class WizardUiContainer : IFlowUiContainerType
        {
            internal WizardUiContainer() { }
            string IFlowUiContainerType.ContainerName { get { return "Wizard"; } }
            ActionResultResponseType IFlowUiContainerType.ActionResultResponseType { get { return ActionResultResponseType.OpenModalDialog; } }
        }


        [SerializerHandler(typeof(StandardUiContainerTypesSerializerHandler))]
        private class DataDialogUiContainer : IFlowUiContainerType
        {
            internal DataDialogUiContainer() { }
            string IFlowUiContainerType.ContainerName { get { return "DataDialog"; } }
            ActionResultResponseType IFlowUiContainerType.ActionResultResponseType { get { return ActionResultResponseType.OpenModalDialog; } }
        }


        [SerializerHandler(typeof(StandardUiContainerTypesSerializerHandler))]
        private class ConfirmDialogUiContainer : IFlowUiContainerType
        {
            internal ConfirmDialogUiContainer() { }
            string IFlowUiContainerType.ContainerName { get { return "ConfirmDialog"; } }
            ActionResultResponseType IFlowUiContainerType.ActionResultResponseType { get { return ActionResultResponseType.OpenModalDialog; } }
        }


        [SerializerHandler(typeof(StandardUiContainerTypesSerializerHandler))]
        private class WarningDialogUiContainer : IFlowUiContainerType
        {
            internal WarningDialogUiContainer() { }
            string IFlowUiContainerType.ContainerName { get { return "WarningDialog"; } }
            ActionResultResponseType IFlowUiContainerType.ActionResultResponseType { get { return ActionResultResponseType.OpenModalDialog; } }
        }


        [SerializerHandler(typeof(StandardUiContainerTypesSerializerHandler))]
        private class NullUiContainer : IFlowUiContainerType
        {
            internal NullUiContainer() { }
            string IFlowUiContainerType.ContainerName { get { return "Null"; } }
            ActionResultResponseType IFlowUiContainerType.ActionResultResponseType { get { return ActionResultResponseType.None; } }
        }
    }
}
