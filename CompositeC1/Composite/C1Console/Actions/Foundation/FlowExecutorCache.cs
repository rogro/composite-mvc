/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Concurrent;
using Composite.C1Console.Events;


namespace Composite.C1Console.Actions.Foundation
{
    internal static class FlowControllerCache
    {
        private static readonly ConcurrentDictionary<Type, IFlowController> _flowControllerCache = new ConcurrentDictionary<Type, IFlowController>();


        static FlowControllerCache()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(args => Flush());
        }


        public static IFlowController GetFlowController(FlowToken flowToken, FlowControllerServicesContainer flowControllerServicesContainer)
        {
            Verify.ArgumentNotNull(flowToken, "flowToken");

            Type flowTokenType = flowToken.GetType();

            return _flowControllerCache.GetOrAdd(flowTokenType, type =>
            {
                object[] attributes = type.GetCustomAttributes(typeof(FlowControllerAttribute), true);
                Verify.That(attributes.Length > 0, "Missing '{0}' attribute on the flow token '{1}'", typeof(FlowControllerAttribute), type);

                FlowControllerAttribute attribute = (FlowControllerAttribute) attributes[0];
                Verify.IsNotNull(attribute.FlowControllerType, "Flow controller type can not be null on the action token '{0}'", type);
                Verify.That(typeof(IFlowController).IsAssignableFrom(attribute.FlowControllerType), "Flow controller '{0}' should implement the interface '{1}'", attribute.FlowControllerType, typeof(IFlowController));

                var flowController = (IFlowController) Activator.CreateInstance(attribute.FlowControllerType);
                flowController.ServicesContainer = flowControllerServicesContainer;

                return flowController;
            });
        }


        private static void Flush()
        {
            _flowControllerCache.Clear();
        }
    }
}
