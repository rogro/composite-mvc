/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System.Collections.Generic;
using System.Configuration;
using Composite.C1Console.Actions.Plugins.DataActionProvider;
using Composite.C1Console.Actions.Plugins.DataActionProvider.Runtime;
using Composite.Core.Collections.Generic;
using Composite.Core.Configuration;
using Composite.C1Console.Events;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Composite.Core.Logging;


namespace Composite.C1Console.Actions.Foundation
{
    internal static class DataActionProviderPluginRegistry
    {
        private static ResourceLocker<Resources> _resourceLocker = new ResourceLocker<Resources>(new Resources(), Resources.Initialize);



        static DataActionProviderPluginRegistry()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlushEvent);
        }



        public static IEnumerable<string> GetProviderNames()
        {
            using (_resourceLocker.ReadLocker)
            {
                return _resourceLocker.Resources.ProviderNames;
            }
        }



        private static IConfigurationSource GetConfiguration()
        {
            IConfigurationSource source = ConfigurationServices.ConfigurationSource;

            if (null == source)
            {
                throw new ConfigurationErrorsException(string.Format("No configuration source specified"));
            }

            return source;
        }



        private static void Flush()
        {
            _resourceLocker.ResetInitialization();
        }



        private static void OnFlushEvent(FlushEventArgs args)
        {
            Flush();
        }


        private sealed class Resources
        {
            public List<string> ProviderNames { get; set; }

            public static void Initialize(Resources resources)
            {
                IConfigurationSource source = GetConfiguration();

                DataActionProviderSettings settings = source.GetSection(DataActionProviderSettings.SectionName) as DataActionProviderSettings;
                if (null == settings)
                {
                    throw new ConfigurationErrorsException(string.Format("Failed to load the configuration section '{0}' from the configuration", DataActionProviderSettings.SectionName));
                }


                resources.ProviderNames = new List<string>();

                foreach (DataActionProviderData data in settings.DataActionProviderPlugins)
                {
                    resources.ProviderNames.Add(data.Name);
                }
            }
        }
    }
}
