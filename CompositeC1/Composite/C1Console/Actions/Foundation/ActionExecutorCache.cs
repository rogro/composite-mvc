/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;
using Composite.C1Console.Events;
using Composite.Core.Logging;
using Composite.C1Console.Security;


namespace Composite.C1Console.Actions.Foundation
{
    internal static class ActionExecutorCache
    {
        private static Dictionary<Type, IActionExecutor> _actionExecutorCache = new Dictionary<Type, IActionExecutor>();

        private static object _lock = new object();



        static ActionExecutorCache()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlush);
        }



        public static IActionExecutor GetActionExecutor(ActionToken actionToken)
        {
            if (actionToken == null) throw new ArgumentNullException("actionToken");


            IActionExecutor actionExecutor;


            if (_actionExecutorCache.TryGetValue(actionToken.GetType(), out actionExecutor) == false)
            {
                object[] attributes = actionToken.GetType().GetCustomAttributes(typeof(ActionExecutorAttribute), true);

                if (attributes.Length == 0) throw new InvalidOperationException(string.Format("Missing {0} attribute on the flow token {1}", typeof(ActionExecutorAttribute), actionToken.GetType()));

                ActionExecutorAttribute attribute = (ActionExecutorAttribute)attributes[0];

                if (attribute.ActionExecutorType == null) throw new InvalidOperationException(string.Format("Action executor type can not be null on the action token {0}", actionToken.GetType()));
                if (typeof(IActionExecutor).IsAssignableFrom(attribute.ActionExecutorType) == false) throw new InvalidOperationException(string.Format("Action executor {0} should implement the interface {1}", attribute.ActionExecutorType, typeof(IActionExecutor)));

                actionExecutor = (IActionExecutor)Activator.CreateInstance(attribute.ActionExecutorType);

                _actionExecutorCache.Add(actionToken.GetType(), actionExecutor);
            }


            return actionExecutor;
        }



        private static void Flush()
        {
            _actionExecutorCache = new Dictionary<Type, IActionExecutor>();
        }



        private static void OnFlush(FlushEventArgs args)
        {
            Flush();
        }
    }
}
