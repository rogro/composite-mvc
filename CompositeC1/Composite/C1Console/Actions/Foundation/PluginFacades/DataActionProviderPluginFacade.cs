/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Composite.C1Console.Actions.Plugins.DataActionProvider;
using Composite.C1Console.Actions.Plugins.DataActionProvider.Runtime;
using Composite.Core.Collections.Generic;
using Composite.Data;
using Composite.C1Console.Events;


namespace Composite.C1Console.Actions.Foundation.PluginFacades
{
    internal static class DataActionProviderPluginFacade
    {
        private static ResourceLocker<Resources> _resourceLocker = new ResourceLocker<Resources>(new Resources(), Resources.Initialize);



        static DataActionProviderPluginFacade()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlushEvent);
        }



        public static IDataAction GetAction(string providerName, IDataActionId dataActionId)
        {
            if (string.IsNullOrEmpty(providerName)) throw new ArgumentNullException("providerName");
            if (dataActionId == null) throw new ArgumentNullException("dataActionId");

            using (_resourceLocker.Locker)
            {
                return GetDataActionProvider(providerName).GetAction(dataActionId);
            }
        }



        public static IEnumerable<IDataActionId> GetActionsByType(string providerName, Type dataInterfaceType)
        {
            if (string.IsNullOrEmpty(providerName)) throw new ArgumentNullException("providerName");
            if (dataInterfaceType == null) throw new ArgumentNullException("dataInterfaceType");

            using (_resourceLocker.Locker)
            {
                return GetDataActionProvider(providerName).GetActionsByType(dataInterfaceType);
            }
        }



        public static Dictionary<DataSourceId, List<IDataActionId>> GetActionsByData<T>(string providerName, IQueryable<T> data)
            where T : class, IData
        {
            if (string.IsNullOrEmpty(providerName)) throw new ArgumentNullException("providerName");
            if (data == null) throw new ArgumentNullException("data");

            using (_resourceLocker.Locker)
            {
                return GetDataActionProvider(providerName).GetActionsByData<T>(data);
            }
        }



        private static IDataActionProvider GetDataActionProvider(string providerName)
        {
            IDataActionProvider provider;

            using (_resourceLocker.Locker)
            {
                if (_resourceLocker.Resources.ProviderCache.TryGetValue(providerName, out provider) == false)
                {
                    try
                    {
                        provider = _resourceLocker.Resources.Factory.Create(providerName);

                        provider.Context = new DataActionProviderContext(providerName);

                        _resourceLocker.Resources.ProviderCache.Add(providerName, provider);
                    }
                    catch (ArgumentException ex)
                    {
                        HandleConfigurationError(ex);
                    }
                    catch (ConfigurationErrorsException ex)
                    {
                        HandleConfigurationError(ex);
                    }
                }
            }

            return provider;
        }



        private static void Flush()
        {
            _resourceLocker.ResetInitialization();
        }



        private static void OnFlushEvent(FlushEventArgs args)
        {
            Flush();
        }



        private static void HandleConfigurationError(Exception ex)
        {
            Flush();

            throw new ConfigurationErrorsException(string.Format("Failed to load the configuration section '{0}' from the configuration.", DataActionProviderSettings.SectionName), ex);
        }



        private sealed class Resources
        {
            public DataActionProviderFactory Factory { get; set; }
            public Dictionary<string, IDataActionProvider> ProviderCache { get; set; }

            public static void Initialize(Resources resources)
            {
                try
                {
                    resources.Factory = new DataActionProviderFactory();
                }
                catch (NullReferenceException ex)
                {
                    HandleConfigurationError(ex);
                }
                catch (ConfigurationErrorsException ex)
                {
                    HandleConfigurationError(ex);
                }

                resources.ProviderCache = new Dictionary<string, IDataActionProvider>();
            }
        }
    }
}
