/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.C1Console.Events;
using Composite.Core;
using Composite.C1Console.Security;


namespace Composite.C1Console.Actions
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class AddNewTreeRefresher
    {
        private readonly FlowControllerServicesContainer _flowControllerServicesContainer;
        private readonly RelationshipGraph _beforeGraph;
        private RelationshipGraph _afterGraph;
        private bool _postRefreshMessegesCalled;


        /// <exclude />
        public AddNewTreeRefresher(EntityToken parentEntityToken, FlowControllerServicesContainer flowControllerServicesContainer)
        {
            Verify.ArgumentNotNull(parentEntityToken, "parentEntityToken");
            Verify.ArgumentNotNull(flowControllerServicesContainer, "flowControllerServicesContainer");

            _beforeGraph = new RelationshipGraph(parentEntityToken, RelationshipGraphSearchOption.Both, false, false);
            _flowControllerServicesContainer = flowControllerServicesContainer;
        }



        /// <exclude />
        public void PostRefreshMesseges(EntityToken newChildEntityToken)
        {
            Verify.ArgumentNotNull(newChildEntityToken, "newChildEntityToken");
            Verify.That(!_postRefreshMessegesCalled, "Only one PostRefreshMesseges call is allowed");

            _postRefreshMessegesCalled = true;

            _afterGraph = new RelationshipGraph(newChildEntityToken, RelationshipGraphSearchOption.Both, false, false);

            IManagementConsoleMessageService messageService = _flowControllerServicesContainer.GetService<IManagementConsoleMessageService>();

            foreach (EntityToken entityToken in RefreshBeforeAfterEntityTokenFinder.FindEntityTokens(_beforeGraph, _afterGraph))
            {
                messageService.RefreshTreeSection(entityToken);
                Log.LogVerbose("AddNewTreeRefresher", string.Format("Refreshing EntityToken: Type = {0}, Source = {1}, Id = {2}, EntityTokenType = {3}", entityToken.Type, entityToken.Source, entityToken.Id, entityToken.GetType()));
            }
        }
    }
}
