/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;


namespace Composite.C1Console.Actions
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class FlowControllerServicesContainer
    {
        private Dictionary<Type, List<object>> _services = new Dictionary<Type, List<object>>();


        /// <exclude />
        public FlowControllerServicesContainer()
        { }


        // Creates a new service container and initialized it with the services from servicesContainerToClone.
        /// <exclude />
        public FlowControllerServicesContainer(FlowControllerServicesContainer servicesContainerToClone)
        { 
           _services = new Dictionary<Type,List<object>>(servicesContainerToClone._services);
        }



        /// <exclude />
        public void AddService(IFlowControllerService flowControllerService)
        {
            Type type = flowControllerService.GetType();

            foreach (Type interfaceType in type.GetInterfaces())
            {
                List<object> list;

                if (_services.TryGetValue(interfaceType, out list) == false)
                {
                    list = new List<object>();

                    _services.Add(interfaceType, list);
                }

                list.Add(flowControllerService);
            }
        }



        /// <exclude />
        public void RemoveService(IFlowControllerService flowControllerService)
        {
            Type type = flowControllerService.GetType();

            foreach (Type interfaceType in type.GetInterfaces())
            {
                List<object> list;
                if (_services.TryGetValue(interfaceType, out list) == false) throw new InvalidOperationException();

                list.Remove(flowControllerService);
            }
        }


        /// <summary>
        /// <para>Returns a concrete service interface, if available. Sample service interfaces are:</para>
        /// <para> - Composite.C1Console.Events.IManagementConsoleMessageService</para>
        /// <para> - Composite.C1Console.Actions.IActionExecutionService</para>
        /// <para> - Composite.C1Console.Forms.Flows.IFormFlowRenderingService</para>
        /// <para> - Composite.C1Console.Actions.IElementInformationService</para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetService<T>() where T : IFlowControllerService
        {
            return (T)GetService(typeof(T));
        }



        /// <exclude />
        public IFlowControllerService GetService(Type serviceType)
        {
            List<object> list;

            if (_services.TryGetValue(serviceType, out list) == false)
            {
                return null;
            }

            if (list.Count > 1)
            {
                throw new InvalidOperationException(string.Format("More than one services of type '{0}' is added", serviceType));
            }

            return (IFlowControllerService)list[0];
        }
    }
}
