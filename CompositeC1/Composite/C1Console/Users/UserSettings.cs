/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;
using System.Globalization;
using System.Net;
using Composite.C1Console.Security;
using System.Threading;


namespace Composite.C1Console.Users
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class UserSettings
    {
        private static IUserSettingsFacade _implementation = new UserSettingsImpl();

        internal static IUserSettingsFacade Implementation { get { return _implementation; } set { _implementation = value; } }


        /// <exclude />
        public static string Username
        {
            get
            {
                return _implementation.Username;
            }
        }

        /// <exclude />
        public static CultureInfo C1ConsoleUiLanguage
        {
            get
            {
                return _implementation.C1ConsoleUiLanguage;
            }
            set
            {
                _implementation.C1ConsoleUiLanguage = value;
            }
        }

        /// <exclude />
        public static CultureInfo CultureInfo
        {
            get
            {
                return _implementation.CultureInfo;
            }
            set
            {
                _implementation.CultureInfo = value;
            }
        }


        /// <exclude />
        public static CultureInfo GetUserCultureInfo(string username)
        {
            return _implementation.GetUserCultureInfo(username);
        }


        /// <exclude />
        public static CultureInfo GetUserC1ConsoleUiLanguage(string username)
        {
            return _implementation.GetUserC1ConsoleUiLanguage(username);
        }


        /// <exclude />
        public static void SetUserCultureInfo(string username, CultureInfo cultureInfo)
        {
            _implementation.SetUserCultureInfo(username, cultureInfo);
        }



        /// <exclude />
        public static void SetUserC1ConsoleUiLanguage(string username, CultureInfo cultureInfo)
        {
            _implementation.SetUserC1ConsoleUiLanguage(username, cultureInfo);
        }



        // Overload
        /// <summary>
        /// This is an overload for GetCurrentActiveLocaleCultureInfo(string username)
        /// using the current username.
        /// </summary>
        public static CultureInfo ActiveLocaleCultureInfo
        {
            get
            {
                return GetCurrentActiveLocaleCultureInfo(UserSettings.Username);
            }
            set
            {
                SetCurrentActiveLocaleCultureInfo(UserSettings.Username, value);
            }
        }


        /// <exclude />
        public static CultureInfo GetCurrentActiveLocaleCultureInfo(string username)
        {
            return _implementation.GetCurrentActiveLocaleCultureInfo(username);
        }


        /// <exclude />
        public static void SetCurrentActiveLocaleCultureInfo(string username, CultureInfo cultureInfo)
        {
            _implementation.SetCurrentActiveLocaleCultureInfo(username, cultureInfo);
        }



        // Overload
        /// <summary>
        /// This is an overload for GetForeignLocaleCultureInfo(string username)
        /// using the current username.
        /// </summary>
        public static CultureInfo ForeignLocaleCultureInfo
        {
            get
            {
                return GetForeignLocaleCultureInfo(UserSettings.Username);
            }
            set
            {
                SetForeignLocaleCultureInfo(UserSettings.Username, value);
            }
        }


        /// <exclude />
        public static CultureInfo GetForeignLocaleCultureInfo(string username)
        {
            return _implementation.GetForeignLocaleCultureInfo(username);
        }


        /// <exclude />
        public static void SetForeignLocaleCultureInfo(string username, CultureInfo cultureInfo)
        {
            _implementation.SetForeignLocaleCultureInfo(username, cultureInfo);
        }


        /// <exclude />
        public static void AddActiveLocaleCultureInfo(string username, CultureInfo cultureInfo)
        {
            _implementation.AddActiveLocaleCultureInfo(username, cultureInfo);
        }


        /// <exclude />
        public static void RemoveActiveLocaleCultureInfo(string username, CultureInfo cultureInfo)
        {
            _implementation.RemoveActiveLocaleCultureInfo(username, cultureInfo);
        }



        // Overload
        /// <summary>
        /// This is an overload for GetActiveLocaleCultureInfos(string username)
        /// using the current username.
        /// </summary>
        public static IEnumerable<CultureInfo> ActiveLocaleCultureInfos
        {
            get
            {
                return GetActiveLocaleCultureInfos(UserSettings.Username);
            }
        }


        /// <exclude />
        public static IEnumerable<CultureInfo> GetActiveLocaleCultureInfos(string username)
        {
            return _implementation.GetActiveLocaleCultureInfos(username);
        }


        /// <exclude />
        public static string LastSpecifiedNamespace
        {
            get
            {
                return _implementation.LastSpecifiedNamespace;
            }
            set
            {
                _implementation.LastSpecifiedNamespace = value;
            }
        }


        /// <exclude />
        public static IPAddress UserIPAddress
        {
            get
            {
                return _implementation.UserIPAddress;
            }
        }
    }
}

