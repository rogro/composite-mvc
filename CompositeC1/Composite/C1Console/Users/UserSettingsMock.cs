/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using Composite.Core.Configuration;
using Composite.C1Console.Security;

namespace Composite.C1Console.Users
{
	internal class UserSettingsMock: IUserSettingsFacade
	{
        #region IUserSettingsFacade Members

        public string Username
        {
            get { return UserValidationFacade.GetUsername(); }
        }

        public System.Globalization.CultureInfo CultureInfo
        {
            get
            {
                return GlobalSettingsFacade.DefaultCultureInfo;
            }
            set
            {
            }
        }

        public System.Globalization.CultureInfo C1ConsoleUiLanguage
        {
            get
            {
                return GlobalSettingsFacade.DefaultCultureInfo;
            }
            set
            {
            }
        }

        public System.Globalization.CultureInfo GetUserCultureInfo(string username)
        {
            throw new NotImplementedException();
        }

        public void SetUserCultureInfo(string username, System.Globalization.CultureInfo cultureInfo)
        {
            throw new NotImplementedException();
        }

        public System.Globalization.CultureInfo GetCurrentActiveLocaleCultureInfo(string username)
        {
            throw new NotImplementedException();
        }

        public void SetCurrentActiveLocaleCultureInfo(string username, System.Globalization.CultureInfo cultureInfo)
        {
            throw new NotImplementedException();
        }

        public void AddActiveLocaleCultureInfo(string username, System.Globalization.CultureInfo cultureInfo)
        {
            throw new NotImplementedException();
        }

        public void RemoveActiveLocaleCultureInfo(string username, System.Globalization.CultureInfo cultureInfo)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<System.Globalization.CultureInfo> GetActiveLocaleCultureInfos(string username)
        {
            throw new NotImplementedException();
        }

        public System.Globalization.CultureInfo GetForeignLocaleCultureInfo(string username)
        {
            throw new NotImplementedException();
        }

        public void SetForeignLocaleCultureInfo(string username, System.Globalization.CultureInfo cultureInfo)
        {
        }

        public string LastSpecifiedNamespace
        {
            get
            {
                return string.Empty;
            }
            set
            {
            }
        }

        public System.Net.IPAddress UserIPAddress
        {
            get { throw new NotImplementedException(); }
        }

        #endregion


        public System.Globalization.CultureInfo GetUserC1ConsoleUiLanguage(string username)
        {
            throw new NotImplementedException();
        }

        public void SetUserC1ConsoleUiLanguage(string username, System.Globalization.CultureInfo cultureInfo)
        {
            throw new NotImplementedException();
        }
    }
}
