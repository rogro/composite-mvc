/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;
using Composite.C1Console.Elements.Plugins.ElementAttachingProvider.Runtime;
using Composite.C1Console.Security;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ObjectBuilder;


namespace Composite.C1Console.Elements.Plugins.ElementAttachingProvider
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public enum ElementAttachingProviderPosition
    {
        /// <summary>
        /// At the top
        /// </summary>
        Top = 0,

        /// <summary>
        /// At the bottom
        /// </summary>
        Bottom = 1
    }



    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class ElementAttachingProviderResult
    {
        /// <exclude />
        public ElementAttachingProviderResult()
        {
            this.Position = ElementAttachingProviderPosition.Bottom;
        }

        /// <summary>
        /// IF this is null, then the hole result is ignored
        /// </summary>
        public IEnumerable<Element> Elements { get; set; }


        /// <exclude />
        public ElementAttachingProviderPosition Position { get; set; }


        /// <summary>
        /// This is used if more than one element attaching provider is adding elements.
        /// Bigger is higher.
        /// </summary>
        public int PositionPriority { get; set; }
    }



    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [CustomFactory(typeof(ElementAttachingProviderCustomFactory))]
    [ConfigurationNameMapper(typeof(ElementAttachingProviderDefaultNameRetriever))]
	public interface IElementAttachingProvider
	{
        /// <summary>
        /// The system will supply an ElementProviderContext to the provider
        /// to use for creating ElementHandles
        /// </summary>
        ElementProviderContext Context { set; }


        /// <summary>
        /// This is only called when rendering root nodes. Used to switch HasChildren from false to true.
        /// </summary>
        /// <param name="parentEntityToken"></param>
        /// /// <param name="piggybag"></param>
        /// <returns></returns>
        bool HaveCustomChildElements(EntityToken parentEntityToken, Dictionary<string, string> piggybag);


        /// <summary>
        /// If null is returned, the result is ignored
        /// </summary>
        /// <param name="parentEntityToken"></param>
        /// <param name="piggybag"></param>
        /// <returns></returns>
        ElementAttachingProviderResult GetAlternateElementList(EntityToken parentEntityToken, Dictionary<string, string> piggybag);


        /// <exclude />
        IEnumerable<Element> GetChildren(EntityToken parentEntityToken, Dictionary<string, string> piggybag);
	}
}
