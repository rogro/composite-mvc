/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Composite.C1Console.Elements.Plugins.ElementProvider.Runtime;
using Composite.Core.Configuration;


namespace Composite.C1Console.Elements.Plugins.ElementProvider
{
    internal sealed class ElementProviderConfigurationServices
    {
        public static HooklessElementProviderData GetElementProviderConfiguration(string providerName)
        {
            ElementProviderSettings settings = (ElementProviderSettings)ConfigurationServices.ConfigurationSource.GetSection(ElementProviderSettings.SectionName);
            
            if (settings.ElementProviderPlugins.Contains(providerName) == false) throw new ArgumentException("Unknown provider name", "providerName");
            
            return settings.ElementProviderPlugins.Get(providerName);
        }



        public static bool DeleteElementProviderConfiguration(string providerName)
        {
            ElementProviderSettings settings = (ElementProviderSettings)ConfigurationServices.ConfigurationSource.GetSection(ElementProviderSettings.SectionName);

            if (settings.ElementProviderPlugins.Contains(providerName))
            {
                settings.ElementProviderPlugins.Remove(providerName);

                SaveElementProviderSettings(settings);

                return true;
            }

            return false;
        }



        public static void SaveElementProviderConfiguration(HooklessElementProviderData elementProviderData)
        {
            ElementProviderSettings settings = (ElementProviderSettings)ConfigurationServices.ConfigurationSource.GetSection(ElementProviderSettings.SectionName);

            SaveElementProviderSettings(settings, elementProviderData);
        }



        private static void SaveElementProviderSettings(ElementProviderSettings settings)
        {
            SaveElementProviderSettings(settings, null);
        }



        private static void SaveElementProviderSettings(ElementProviderSettings settings, HooklessElementProviderData elementProviderData)
        {
            ElementProviderSettings newSettings = new ElementProviderSettings();
            newSettings.RootProviderName = settings.RootProviderName;

            if (elementProviderData != null)
            {
                newSettings.ElementProviderPlugins.Add(elementProviderData);

                foreach (var plugin in settings.ElementProviderPlugins.Where(p => p.Name != elementProviderData.Name))
                {
                    newSettings.ElementProviderPlugins.Add(plugin);
                }
            }
            else
            {
                foreach (var plugin in settings.ElementProviderPlugins)
                {
                    newSettings.ElementProviderPlugins.Add(plugin);
                }
            }

            ConfigurationServices.SaveConfigurationSection(ElementProviderSettings.SectionName, newSettings);
        }
    }
}
