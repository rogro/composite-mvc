/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;
using Composite.C1Console.Elements.Plugins.ElementProvider.Runtime;
using Composite.C1Console.Security;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ObjectBuilder;


namespace Composite.C1Console.Elements.Plugins.ElementProvider
{
    /// <summary>    
    /// This interface is implemented by element providers - plug-ins that can decorate the C1 Console tree structure with elements.
    /// </summary>
    [CustomFactory(typeof(HooklessElementProviderCustomFactory))]
    [ConfigurationNameMapper(typeof(HooklessElementProviderDefaultNameRetriever))]
    public interface IHooklessElementProvider
    {
        /// <summary>
        /// The system will supply an ElementProviderContext to the provider
        /// to use for creating <see cref="ElementHandle"/>-s
        /// </summary>
        ElementProviderContext Context { set; }


        /// <summary>
        /// Gets the provider's root elements 
        /// </summary>
        /// <param name="seachToken">
        /// If this is null the provider should not do any filtering. If this is not null the provider
        /// should do the appropriate filtering on its elements. 
        /// If the provider does not want to be a part of a search and this variable is not null,
        /// the provider should return an empty list
        /// </param>
        /// <returns>Root elements</returns>
        IEnumerable<Element> GetRoots(SearchToken seachToken);


        /// <summary>
        /// Gets the children of a given element
        /// </summary>
        /// <param name="entityToken">The parent element of the elements to return</param>
        /// <param name="seachToken">
        /// If this is <value>null</value> the provider should not do any filtering. If this is not null the provider
        /// should do the appropriate filtering on its elements. 
        /// If the provider does not want to be a part of a search and this variable is not null,
        /// the provider should return an empty list
        /// </param>
        /// <returns>Child elements</returns>
        IEnumerable<Element> GetChildren(EntityToken entityToken, SearchToken seachToken);        
    }
}
