/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Composite.C1Console.Security;
using Composite.C1Console.Elements.Foundation.PluginFacades;
using Composite.C1Console.Elements.Plugins.ElementAttachingProvider;

namespace Composite.C1Console.Elements.Foundation
{
	internal static class ElementAttachingProviderFacade
	{
        public static bool HaveCustomChildElements(EntityToken parentEntityToken, Dictionary<string, string> piggybag)
        {
            foreach (string providerName in ElementAttachingProviderRegistry.ElementAttachingProviderNames)
            {
                bool result = ElementAttachingProviderPluginFacade.HaveCustomChildElements(providerName, parentEntityToken, piggybag);
                if (result)
                {
                    return true;
                }
            }

            return false;
        }



        public static IEnumerable<Element> AttachElements(EntityToken parentEntityToken, Dictionary<string, string> piggybag, IEnumerable<Element> currentElements)
        {
            List<ElementAttachingProviderResult> topResults = new List<ElementAttachingProviderResult>();
            List<ElementAttachingProviderResult> bottomResults = new List<ElementAttachingProviderResult>();

            foreach (string providerName in ElementAttachingProviderRegistry.ElementAttachingProviderNames)
            {
                if (ElementAttachingProviderPluginFacade.IsMultibleResultElementAttachingProvider(providerName) == false)
                {
                    ElementAttachingProviderResult result = ElementAttachingProviderPluginFacade.GetAlternateElementList(providerName, parentEntityToken, piggybag);

                    if ((result == null) || (result.Elements == null)) continue;

                    if (result.Position == ElementAttachingProviderPosition.Top)
                    {
                        topResults.Add(result);
                    }
                    else if (result.Position == ElementAttachingProviderPosition.Bottom)
                    {
                        bottomResults.Add(result);
                    }
                }
                else
                {
                    IEnumerable<ElementAttachingProviderResult> results = ElementAttachingProviderPluginFacade.GetAlternateElementLists(providerName, parentEntityToken, piggybag);

                    if (results == null) continue;

                    foreach (ElementAttachingProviderResult result in results)
                    {
                        if ((result == null) || (result.Elements == null)) continue;

                        if (result.Position == ElementAttachingProviderPosition.Top)
                        {
                            topResults.Add(result);
                        }
                        else if (result.Position == ElementAttachingProviderPosition.Bottom)
                        {
                            bottomResults.Add(result);
                        }
                    }
                }
            }

            Comparison<ElementAttachingProviderResult> sortMethod = delegate(ElementAttachingProviderResult r1, ElementAttachingProviderResult r2) { return r2.PositionPriority - r1.PositionPriority; };
            topResults.Sort(sortMethod);
            bottomResults.Sort(sortMethod);


            IEnumerable<Element> topElements = null;
            foreach (ElementAttachingProviderResult result in topResults)
            {
                if (topElements == null)
                {
                    topElements = result.Elements;
                }
                else
                {
                    topElements = topElements.Concat(result.Elements);
                }
            }


            IEnumerable<Element> bottomElements = null;
            foreach (ElementAttachingProviderResult result in bottomResults)
            {
                if (bottomElements == null)
                {
                    bottomElements = result.Elements;
                }
                else
                {
                    bottomElements = bottomElements.Concat(result.Elements);
                }
            }

            IEnumerable<Element> resultElements = topElements;

            if (resultElements == null)
            {
                resultElements = currentElements;
            }
            else
            {
                resultElements = resultElements.Concat(currentElements);
            }

            if (bottomElements != null)
            {
                resultElements = resultElements.Concat(bottomElements);
            }

            return resultElements;
        }
	}
}
