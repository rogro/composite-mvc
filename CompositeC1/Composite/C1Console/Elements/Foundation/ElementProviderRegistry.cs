/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Configuration;
using Composite.Core.Collections.Generic;
using Composite.Core.Configuration;
using Composite.C1Console.Elements.Plugins.ElementProvider;
using Composite.C1Console.Elements.Plugins.ElementProvider.Runtime;
using Composite.C1Console.Events;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;


namespace Composite.C1Console.Elements.Foundation
{
    internal static class ElementProviderRegistry
    {
        private static ResourceLocker<Resources> _resourceLocker = new ResourceLocker<Resources>(new Resources(), Resources.Initialize);



        static ElementProviderRegistry()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlushEvent);
        }



        public static string RootElementProviderName
        {
            get
            {
                using (_resourceLocker.ReadLocker)
                {
                    return _resourceLocker.Resources.RootElementProviderName;
                }
            }
        }



        public static IEnumerable<string> ElementProviderNames
        {
            get
            {
                using (_resourceLocker.ReadLocker)
                {
                    return _resourceLocker.Resources.ElementProviderNames.Keys;
                }
            }
        }



        public static Type GetElementProviderType(string elementProviderName)
        {
            if (string.IsNullOrEmpty(elementProviderName)) throw new ArgumentNullException("elementProviderName");

            Type type;
            using (_resourceLocker.ReadLocker)
            {
                if (_resourceLocker.Resources.ElementProviderNames.TryGetValue(elementProviderName, out type) == false)
                {
                    throw new ArgumentException(string.Format("The element provider named '{0}' does not exist", elementProviderName));
                }

                return type;
            }
        }



        public static bool IsProviderHookingProvider(string elementProviderName)
        {
            using (_resourceLocker.ReadLocker)
            {
                return _resourceLocker.Resources.HookingProviderNames.Contains(elementProviderName);
            }
        }



        private static IConfigurationSource GetConfiguration()
        {
            IConfigurationSource source = ConfigurationServices.ConfigurationSource;

            if (null == source)
            {
                throw new ConfigurationErrorsException(string.Format("No configuration source specified"));
            }

            return source;
        }



        private static void Flush()
        {
            _resourceLocker.ResetInitialization();
        }



        private static void OnFlushEvent(FlushEventArgs args)
        {
            Flush();
        }



        private sealed class Resources
        {
            public string RootElementProviderName;
            public Dictionary<string, Type> ElementProviderNames;
            public List<string> HookingProviderNames;

            public static void Initialize(Resources resources)
            {
                IConfigurationSource configurationSource = GetConfiguration();

                ElementProviderSettings settings = configurationSource.GetSection(ElementProviderSettings.SectionName) as ElementProviderSettings;
                if (null == settings)
                {
                    throw new ConfigurationErrorsException(string.Format("Failed to load the configuration section '{0}' from the configuration", ElementProviderSettings.SectionName));
                }

                resources.RootElementProviderName = settings.RootProviderName;


                resources.ElementProviderNames = new Dictionary<string, Type>();
                resources.HookingProviderNames = new List<string>();

                foreach (HooklessElementProviderData data in settings.ElementProviderPlugins)
                {
                    resources.ElementProviderNames.Add(data.Name, data.Type);

#pragma warning disable 612
                    if ((data is ElementProviderData))
                    {
                        resources.HookingProviderNames.Add(data.Name);
                    }
#pragma warning restore 612
                }
            }
        }
    }
}
