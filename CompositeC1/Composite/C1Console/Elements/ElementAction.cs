/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;


namespace Composite.C1Console.Elements
{
    /// <summary>    
    /// Define an action you can attach to an <see cref="Element"/>.
    /// </summary>
    public sealed class ElementAction
    {
        private readonly ActionHandle _actionHandle;


        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        /// <param name="actionHandle"></param>
        public ElementAction(ActionHandle actionHandle)
        {
            if (actionHandle == null) throw new ArgumentNullException("actionHandle");

            _actionHandle = actionHandle;            
        }



        /// <summary>
        /// The action handle
        /// </summary>
        public ActionHandle ActionHandle
        {
            get { return _actionHandle; }
        }



        /// <summary>
        /// The visual representation (label, icon) of the action
        /// </summary>
        public ActionVisualizedData VisualData { get; set; }



        /// <exclude />
        public string TagValue { get; set; }

        

        /// <exclude />
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }



        /// <exclude />
        public bool Equals(ElementAction elementAction)
        {
            if (elementAction == null) return false;

            return this.ActionHandle.Equals(elementAction);
        }



        /// <exclude />
        public override int GetHashCode()
        {
            return this.ActionHandle.GetHashCode();
        }
    }
}
