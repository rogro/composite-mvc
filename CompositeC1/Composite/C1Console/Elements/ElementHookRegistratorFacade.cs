/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;
using Composite.C1Console.Security;
using Composite.C1Console.Elements.Foundation;
using Composite.C1Console.Elements.Foundation.PluginFacades;
using Composite.Core.Instrumentation;


namespace Composite.C1Console.Elements
{
    internal static class ElementHookRegistratorFacade
    {
        public static IEnumerable<EntityTokenHook> GetHooks()
        {
            int tt1 = System.Environment.TickCount;

            List<EntityTokenHook> hooks = new List<EntityTokenHook>();

            foreach (string providerName in ElementProviderRegistry.ElementProviderNames)
            {
                if (ElementProviderRegistry.IsProviderHookingProvider(providerName) == false) continue;

                int t1 = System.Environment.TickCount;
                hooks.AddRange(ElementProviderPluginFacade.GetHooks(providerName));
                int t2 = System.Environment.TickCount;

                Composite.Core.Logging.LoggingService.LogVerbose("ElementHookRegistratorFacade", string.Format("Time for {0}: {1} ms", providerName, t2 - t1));
            }

            int tt2 = System.Environment.TickCount;

            Composite.Core.Logging.LoggingService.LogVerbose("ElementHookRegistratorFacade", string.Format("Total time for: {0} ms", tt2 - tt1));

            return hooks;
        }
    }
}
