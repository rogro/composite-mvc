/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Text;
using System.Reflection;
using System.Collections.Generic;
using Composite.Core.Types;
using Composite.C1Console.Security;
using Composite.Core.Serialization;


namespace Composite.C1Console.Elements
{
    /// <summary>    
    /// A handle to an action - a unique and serializable way to identify actions.
    /// </summary>
    public sealed class ActionHandle
    {
        private ActionToken _actionToken;
        private string _serializedActionToken;



        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        /// <param name="actionToken"></param>
        public ActionHandle(ActionToken actionToken)
        {
            _actionToken = actionToken;
        }



        /// <summary>
        /// <see cref="ActionToken"/> represented by this handle.
        /// </summary>
        public ActionToken ActionToken
        {
            get { return _actionToken; }
        }



        private string SerializedActionToken
        {
            get
            {
                if (_serializedActionToken == null)
                {
                    _serializedActionToken = _actionToken.Serialize();
                }

                return _serializedActionToken;
            }
        }



        /// <exclude />
        public override bool Equals(object obj)
        {
            return Equals(obj as ActionHandle);
        }



        /// <exclude />
        public bool Equals(ActionHandle actionHandle)
        {
            if (actionHandle == null) return false;

            return this.SerializedActionToken == actionHandle.SerializedActionToken;
        }



        /// <exclude />
        public override int GetHashCode()
        {
            return this.SerializedActionToken.GetHashCode();
        }
    }
}
