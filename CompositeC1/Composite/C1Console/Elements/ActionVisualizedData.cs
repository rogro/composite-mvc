/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using Composite.Core.ResourceSystem;


namespace Composite.C1Console.Elements
{
    /// <summary>    
    /// Describe how an action should appear visually (label, icon).
    /// </summary>
    public sealed class ActionVisualizedData
    {
        /// <summary>
        /// Constructs a new instance
        /// </summary>
        public ActionVisualizedData() 
        {
            this.ActionCheckedStatus = ActionCheckedStatus.Uncheckable;
            ActivePositions = ElementActionActivePosition.NavigatorTree;
        }

        /// <summary>
        /// Constructs a new instance, cloning an existing instance.
        /// </summary>
        /// <param name="copy"></param>
        public ActionVisualizedData(ActionVisualizedData copy) 
        {
            this.ActionLocation = copy.ActionLocation;
            this.Disabled = copy.Disabled;
            this.Icon = copy.Icon;
            this.Label = copy.Label;
            this.ToolTip = copy.ToolTip;
            this.ActionCheckedStatus = copy.ActionCheckedStatus;
            this.ActivePositions = copy.ActivePositions;
        }


        /// <summary>
        /// Controls if a check box should be shown next to the action command, and if this checkbox is checked.
        /// </summary>
        public ActionCheckedStatus ActionCheckedStatus { get; set; }

        /// <summary>
        /// Label of the action
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// When disabled, the action will be shown but grayed out and cannot be invoked.
        /// </summary>
        public bool Disabled { get; set; }

        /// <summary>
        /// The action's icon
        /// </summary>
        public ResourceHandle Icon { get; set; }

        /// <summary>
        /// The action's tool tip
        /// </summary>
        public string ToolTip { get; set; }

        /// <summary>
        /// Where the action should show up, relative to other actions.
        /// </summary>
        public ActionLocation ActionLocation { get; set; }

        /// <summary>
        /// Where the action should be shown - when elements are shown in a selection tree (as opposed to the normal navigation tree) some actions may be desired.
        /// Default is <value>ElementActionActivePosition.NavigatorTree</value> only.
        /// </summary>
        public ElementActionActivePosition ActivePositions { get; set; }


        /// <exclude />
        public override string ToString()
        {
            return Label;
        }
    }
}
