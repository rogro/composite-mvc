/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Text;
using System.Collections.Generic;
using Composite.Core.Serialization;


namespace Composite.C1Console.Elements
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class ElementProviderHandle
    {
        private string _serializedData = null;


        /// <exclude />
        public ElementProviderHandle(string providerName)
        {
            Verify.ArgumentNotNullOrEmpty(providerName, "providerName");

            this.ProviderName = providerName;
        }



        /// <exclude />
        public string ProviderName
        {
            get;
            private set;
        }



        /// <exclude />
        public string Serialize()
        {
            return ToString();
        }



        /// <exclude />
        public static ElementProviderHandle Deserialize(string serializedElementHandle)
        {
            Dictionary<string, string> dic = StringConversionServices.ParseKeyValueCollection(serializedElementHandle);

            if (dic.ContainsKey("_providerName_") == false)
            {
                throw new ArgumentException("The serializedElementProviderHandle is not a serialized element provider handle", "serializedElementProviderHandle");
            }

            string providerName = StringConversionServices.DeserializeValueString(dic["_providerName_"]);

            return new ElementProviderHandle(providerName);
        }



        /// <exclude />
        public override string ToString()
        {
            if (_serializedData == null)
            {
                StringBuilder sb = new StringBuilder();
                StringConversionServices.SerializeKeyValuePair(sb, "_providerName_", ProviderName);

                _serializedData = sb.ToString();
            }

            return _serializedData;
        }
    }
}
