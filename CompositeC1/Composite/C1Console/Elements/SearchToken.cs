/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.Core.Extensions;
using Composite.Core.Serialization;
using Composite.Core.Types;


namespace Composite.C1Console.Elements
{
    /// <summary>
    /// Class that describe a element provider search. Sub class this for more specific fields. As a minimum a Keyword is present.
    /// </summary>
    public class SearchToken
    {
        /// <summary>
        /// Keyword to search for
        /// </summary>
        public string Keyword { get; set; }

        /// <summary>
        /// Serializes this instance
        /// </summary>
        /// <returns>String representation</returns>
        public string Serialize()
        {
            string serializedSearchToken = SerializationFacade.Serialize(this);
            string serializedClassName = TypeManager.SerializeType(this.GetType());

            string serializedSearchTokenWithClass = string.Format("{0}|{1}", serializedClassName, serializedSearchToken);

            return serializedSearchTokenWithClass;
        }


        /// <summary>
        /// Deserializes a search token
        /// </summary>
        /// <param name="serializedSearchToken">String representation of searchtoken</param>
        /// <returns>Deserialized SearchToken</returns>
        public static SearchToken Deserialize( string serializedSearchToken )
        {
            Verify.ArgumentNotNullOrEmpty("serializedSearchToken", serializedSearchToken);
            Verify.ArgumentCondition(serializedSearchToken.IndexOf('|') > -1, "serializedSearchToken", "Malformed serializedSearchToken - must be formated like '<class name>|<serialized values>'");

            string[] parts = serializedSearchToken.Split('|');

            string className = parts[0];
            string serializedSearchTokenWithoutClassName = parts[1];

            Type searchTokenType = TypeManager.GetType(className);

            SearchToken searchToken = (SearchToken)SerializationFacade.Deserialize(searchTokenType, serializedSearchTokenWithoutClassName);

            return searchToken;
        }
    }



    internal static class SeachTokenExtensionMethods
    {
        /// <summary>
        /// This method return <value>true</value> if the <paramref name="searchToken"/> is NOT null and the keyword is NOT null or empty
        /// </summary>
        /// <param name="searchToken"></param>
        /// <returns></returns>
        public static bool IsValidKeyword(this SearchToken searchToken)
        {
            return searchToken != null && !searchToken.Keyword.IsNullOrEmpty();
        }
    }
}
