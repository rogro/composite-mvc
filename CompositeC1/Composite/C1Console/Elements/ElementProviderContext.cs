/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using Composite.C1Console.Security;


namespace Composite.C1Console.Elements
{
    /// <summary>   
    /// Context assigned to element providers when they are constructed. Contains a helper method for constructing a provider specific <see cref="ElementHandle"/> and the configuation based name of the provider.
    /// </summary>
    public sealed class ElementProviderContext
    {
        private string _providerName;



        /// <summary>
        /// Constructs a new instance of <see cref="ElementProviderContext"/>
        /// </summary>
        /// <param name="providerName">Name of the provider</param>
        public ElementProviderContext(string providerName)
        {
            _providerName = providerName;
        }



        /// <summary>
        /// Created a provider instance (based on name) specific <see cref="ElementHandle"/> for a given <see cref="EntityToken"/>, making it possible to tie an entiry token to a specific provider instance. 
        /// </summary>
        /// <param name="entityToken"></param>
        /// <returns></returns>
        public ElementHandle CreateElementHandle(EntityToken entityToken)
        {
            return new ElementHandle(_providerName, entityToken);
        }



        /// <summary>
        /// The name if the provider instance. This name typically originate from configuration. A given provider type may exist as multiple instances, but all with have a unique name.
        /// </summary>
        public string ProviderName
        {
            get { return _providerName; }
        }
    }
}
