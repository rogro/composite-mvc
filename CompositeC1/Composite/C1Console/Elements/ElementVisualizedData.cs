/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using Composite.Core.ResourceSystem;


namespace Composite.C1Console.Elements
{
    /// <summary>    
    /// Describe an elements visual appearance
    /// </summary>
    public sealed class ElementVisualizedData
    {
        /// <summary>
        /// Label to be shown in tree
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// If this element has (may have) chrildren. When true navigation for opening the element will be provided.
        /// </summary>
        public bool HasChildren { get; set; }

        /// <summary>
        /// When true the element will be shown in the UI but in a grayed out way with all actions disabled
        /// </summary>
        public bool IsDisabled { get; set; }

        /// <summary>
        /// The icon of the element (when closed)
        /// </summary>
        public ResourceHandle Icon { get; set; }

        /// <summary>
        /// The icon of the element when open (when children are shown)
        /// </summary>
        public ResourceHandle OpenedIcon { get; set; }

        /// <summary>
        /// Tooltip for the element - typically shown when hovering the element
        /// </summary>
        public string ToolTip { get; set; }
    }
}
