/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;


namespace Composite.C1Console.Elements
{
    /// <summary>    
    /// Details describing how this element may be dragged or accept dragged elements
    /// </summary>
    public sealed class ElementDragAndDropInfo
    {
        private List<string> _dropHashTypeIdentifiers = null;

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        internal ElementDragAndDropInfo()
        {
            this.DragType = null;
            this.SupportsIndexedPosition = false;
        }


        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        /// <param name="movabilityType">Declare what this element is in terms of drag and drop by declaring a <see cref="System.Type"/>. 
        /// Element providers accepting drag and drop of this type will accept this.</param>
        public ElementDragAndDropInfo(Type movabilityType)
        {
            this.DragType = movabilityType;
        }

        /// <summary>
        /// Identifies a type of element in relation to movability. Used in conjunction with the AdoptTypeAcceptList.
        /// Use the MovabilitySubType to distinguish sub types.
        /// </summary>
        public Type DragType { get; set; }


        /// <summary>
        /// Optional. If the element provider presents data from multiple sources that can not exchange data and you 
        /// can not communicate a unique source using a Type alone, use this field to distinguish the source.
        /// </summary>
        public string DragSubType { get; set; }


        /// <exclude />
        public bool SupportsIndexedPosition { get; set; }


        /// <exclude />
        public void AddDropType(Type acceptMovabilityType)
        {
            this.AddDropType(acceptMovabilityType, "");
        }


        /// <exclude />
        public void AddDropType(Type acceptMovabilityType, string acceptMovabilitySubType)
        {
            if (_dropHashTypeIdentifiers == null) _dropHashTypeIdentifiers = new List<string>();

            _dropHashTypeIdentifiers.Add(BuildHashedTypeIdentifier(acceptMovabilityType, acceptMovabilitySubType));
        }



        internal string GetHashedTypeIdentifier()
        {
            return BuildHashedTypeIdentifier(this.DragType, this.DragSubType);
        }


        internal List<string> GetDropHashTypeIdentifiers()
        {
            return _dropHashTypeIdentifiers;
        }



        private static string BuildHashedTypeIdentifier(Type type, string subType)
        {
            return string.Format("{0}:{1}:{2}", type.Name, subType, type.AssemblyQualifiedName.GetHashCode());
        }
    }
}
