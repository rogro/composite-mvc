/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using Composite.C1Console.Security;


namespace Composite.C1Console.Elements
{
    /// <summary>    
    /// An "identity token" which identifies a specific element from a specific provider. 
    /// </summary>
    public sealed class ElementHandle
    {
        private readonly string _providerName;
        private readonly EntityToken _entityToken;
        private string _serializedPiggyback = null;
        Dictionary<string, string> _piggyback = null;


        /// <summary>
        /// Constructs a new ElementHandle
        /// </summary>
        /// <param name="providerName"></param>
        /// <param name="entityToken"></param>
        public ElementHandle(string providerName, EntityToken entityToken)
            : this(providerName, entityToken, (string)null)
        {
        }



        /// <summary>
        /// Constructs a new ElementHandle
        /// </summary>
        /// <param name="providerName"></param>
        /// <param name="entityToken"></param>
        /// <param name="piggyback"></param>
        public ElementHandle(string providerName, EntityToken entityToken, Dictionary<string, string> piggyback)
        {
            Verify.ArgumentNotNull(piggyback, "piggyback");

            _providerName = providerName;
            _entityToken = entityToken;
            _piggyback = piggyback;
        }



        /// <summary>
        /// Constructs a new ElementHandle
        /// </summary>
        /// <param name="providerName"></param>
        /// <param name="entityToken"></param>
        /// <param name="piggyback"></param>
        public ElementHandle(string providerName, EntityToken entityToken, string piggyback)
        {
            if (piggyback == null)
            {
                piggyback = string.Empty;
            }

            _providerName = providerName;
            _entityToken = entityToken;

            _serializedPiggyback = piggyback;
        }



        /// <summary>
        /// Name of provider this ElementHandle originate from
        /// </summary>
        public string ProviderName
        {
            get { return _providerName; }
        }



        /// <summary>
        /// The entity token of the element
        /// </summary>
        public EntityToken EntityToken
        {
            get { return _entityToken; }
        }



        /// <summary>
        /// Custom values appended to this ElementHandle
        /// </summary>
        public Dictionary<string, string> Piggyback
        {
            get
            {
                if (_piggyback == null)
                {
                    string serialized = _serializedPiggyback;
                    Verify.IsNotNull(serialized, "Serialized state is NULL");

                    _piggyback = PiggybagSerializer.Deserialize(_serializedPiggyback);
                }

                return _piggyback;
            }
        }



        /// <exclude />
        public string SerializedPiggyback
        {
            get
            {
                if (_serializedPiggyback == null)
                {
                    _serializedPiggyback = PiggybagSerializer.Serialize(this.Piggyback);
                }

                return _serializedPiggyback;
            }
        }



        /// <exclude />
        public override bool Equals(object obj)
        {
            return Equals(obj as ElementHandle);
        }



        /// <exclude />
        public bool Equals(ElementHandle elementHandle)
        {
            if (elementHandle == null) return false;

            if ((elementHandle.EntityToken.Equals(this.EntityToken) == false) || (elementHandle.ProviderName != this.ProviderName)) return false;

            if (elementHandle.Piggyback.Count != this.Piggyback.Count) return false;

            foreach (KeyValuePair<string, string> kvp in elementHandle.Piggyback)
            {
                string value;
                if (this.Piggyback.TryGetValue(kvp.Key, out value) == false) return false;

                if (kvp.Value != value) return false;
            }

            return true;
        }



        /// <exclude />
        public override int GetHashCode()
        {
            int hashCode = this._providerName.GetHashCode() ^ _entityToken.GetHashCode();

            foreach (KeyValuePair<string, string> kvp in this.Piggyback)
            {
                hashCode ^= kvp.Key.GetHashCode();
                hashCode ^= kvp.Value.GetHashCode();
            }

            return hashCode;
        }
    }
}
