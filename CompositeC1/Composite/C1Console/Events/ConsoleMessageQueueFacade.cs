/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using Composite.C1Console.Events.Foundation;
using Composite.Core.Configuration;


namespace Composite.C1Console.Events
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class ConsoleMessageQueueFacade
    {
        private static ConsoleMessageQueue _messageQeueue = new ConsoleMessageQueue(GlobalSettingsFacade.ConsoleMessageQueueItemSecondToLive);

        // Dont add flush / initialize to this class. We do not want console messages to get lost.

        /// <summary>
        /// 
        /// </summary>
        /// <param name="consoleMessageQueueItem"></param>
        /// <param name="receiverConsoleId">null or empty string is a broardcast</param>
        public static void Enqueue(IConsoleMessageQueueItem consoleMessageQueueItem, string receiverConsoleId)
        {
            if (consoleMessageQueueItem == null) throw new ArgumentNullException("consoleMessageQueueItem");

            _messageQeueue.Enqueue(consoleMessageQueueItem, receiverConsoleId);
        }



        /// <exclude />
        public static IEnumerable<ConsoleMessageQueueElement> GetQueueElements(int currentConsoleCounter, string consoleId)
        {
            return _messageQeueue.GetQueueElements(currentConsoleCounter, consoleId);
        }



        /// <exclude />
        public static int GetLatestMessageNumber(string consoleId)
        {
            return _messageQeueue.GetLatestMessageNumber(consoleId);
        }



        /// <exclude />
        public static int CurrentChangeNumber
        {
            get
            {
                return _messageQeueue.CurrentQueueItemNumber;
            }
        }



        /// <exclude />
        public static void DoDebugSerializationToFileSystem()
        {
            _messageQeueue.DoDebugSerializationToFileSystem();
        }
    }
}
