/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Linq;
using System.Xml.Linq;
using Composite.Core.Serialization;
using System.Collections.Generic;
using Composite.Core.Logging;


namespace Composite.C1Console.Events.Foundation
{
    internal sealed class ConsoleMessageQueueElementListValueXmlSerializer : IValueXmlSerializer
    {
        public bool TrySerialize(Type objectToSerializeType, object objectToSerialize, IXmlSerializer xmlSerializer, out XElement serializedList)
        {
            if (objectToSerializeType == null) throw new ArgumentNullException("objectToSerializeType");
            if (xmlSerializer == null) throw new ArgumentNullException("xmlSerializer");

            serializedList = null;
            if (objectToSerializeType != typeof(List<ConsoleMessageQueueElement>)) return false;

            List<ConsoleMessageQueueElement> queueElements = objectToSerialize as List<ConsoleMessageQueueElement>;

            serializedList = new XElement("ConsoleMessageQueueElements");

            if (queueElements != null)
            {
                foreach (ConsoleMessageQueueElement queueElement in queueElements)
                {
                    XElement serializedQueueItem = xmlSerializer.Serialize(queueElement.QueueItem.GetType(), queueElement.QueueItem);

                    XElement serializedElement = new XElement("QueueElement",
                        new XAttribute("time", queueElement.EnqueueTime),
                        new XAttribute("number", queueElement.QueueItemNumber),
                        new XAttribute("itemtype", queueElement.QueueItem.GetType()),
                        serializedQueueItem);

                    if (string.IsNullOrEmpty(queueElement.ReceiverConsoleId) == false)
                    {
                        serializedElement.Add(new XAttribute("console", queueElement.ReceiverConsoleId));
                    }

                    serializedList.Add(serializedElement);
                }
            }

            return true;
        }



        public bool TryDeserialize(XElement serializedObject, IXmlSerializer xmlSerializer, out object deserializedObject)
        {
            if (serializedObject == null) throw new ArgumentNullException("serializedObject");
            if (xmlSerializer == null) throw new ArgumentNullException("xmlSerializer");

            deserializedObject = null;

            if (serializedObject.Name.LocalName != "ConsoleMessageQueueElements") return false;

            List<ConsoleMessageQueueElement> queueElements = new List<ConsoleMessageQueueElement>();

            foreach (XElement queueElement in serializedObject.Elements())
            {
                ConsoleMessageQueueElement result = new ConsoleMessageQueueElement();
                result.EnqueueTime = DateTime.Parse(queueElement.Attribute("time").Value);
                result.QueueItemNumber = Int32.Parse(queueElement.Attribute("number").Value);
                if (queueElement.Attribute("console") != null)
                    result.ReceiverConsoleId = queueElement.Attribute("console").Value;

                try
                {
                    result.QueueItem = (IConsoleMessageQueueItem)xmlSerializer.Deserialize(queueElement.Elements().First());
                }
                catch (Exception ex)
                {
                    string errorInfo = string.Format("Deserialization of message #{0} failed with an '{1}' exception - the message has been dropped. Details: '{2}'", queueElement.Attribute("number").Value, ex.GetType().Name, ex.Message);
                    // pad queue to ensure sequence.
                    LoggingService.LogWarning("ConsoleMessageQueue", errorInfo);
                    result.QueueItem = new LogEntryMessageQueueItem { Level = LogLevel.Error, Message = errorInfo, Sender = this.GetType() }; 
                }

                queueElements.Add(result);
            }

            deserializedObject = queueElements;
            return true;
        }
    }
}

