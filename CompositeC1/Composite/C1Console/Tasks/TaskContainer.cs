/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using Composite.C1Console.Actions;
using Composite.Core;
using Composite.Core.Types;
using Composite.Data;
using Composite.Data.Types;


namespace Composite.C1Console.Tasks
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class TaskContainer : IDisposable
    {
        private static readonly string LogTitle = typeof (TaskContainer).Name;

        private List<Task> _tasks;
        private bool _disposed = false;



        internal TaskContainer(List<Task> tasks, TaskManagerEvent taskManagerEvent)
        {
            _tasks = tasks;

            foreach (Task task in _tasks)
            {
                task.TaskManager.OnRun(task.Id, taskManagerEvent);
            }
        }



        /// <exclude />
        public void UpdateTasksWithFlowToken(FlowToken flowToken)
        {
            foreach (Task task in _tasks)
            {
                task.FlowToken = flowToken.Serialize();
            }
        }



        /// <exclude />
        public void OnStatus(TaskManagerEvent taskManagerEvent)
        {
            foreach (Task task in _tasks)
            {
                task.TaskManager.OnStatus(task.Id, taskManagerEvent);
            }
        }



        /// <exclude />
        public void SetOnIdleTaskManagerEvent(TaskManagerEvent taskManagerEvent)
        {
            _onIdleTaskManagerEvent = taskManagerEvent;
        }


        private TaskManagerEvent _onIdleTaskManagerEvent;


        /// <summary>
        /// Saves tasks to database
        /// </summary>
        public void SaveTasks()
        {
            foreach (Task task in _tasks)
            {
                try
                {
                    ITaskItem taskItem = DataFacade.BuildNew<ITaskItem>();
                    taskItem.Id = Guid.NewGuid();
                    taskItem.TaskId = task.Id;
                    taskItem.TaskManagerType = TypeManager.SerializeType(task.TaskManagerType);
                    taskItem.SerializedFlowToken = task.FlowToken;
                    taskItem.StartTime = task.StartTime;

                    DataFacade.AddNew<ITaskItem>(taskItem);
                }
                catch (Exception ex)
                {
                    Log.LogError(LogTitle, "Error while attempt to persist a task");
                    Log.LogError(LogTitle, ex);
                }
            }
        }


        /// <exclude />
        ~TaskContainer()
        {
            Dispose();
        }



        /// <exclude />
        public void Dispose()
        {
            if (_disposed == false)
            {
                _disposed = true;

                foreach (Task task in _tasks)
                {
                    task.TaskManager.OnIdle(task.Id, _onIdleTaskManagerEvent);
                }
            }
        }
    }
}
