/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.C1Console.Actions;
using Composite.C1Console.Security;


namespace Composite.C1Console.Tasks
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class TaskManagerFacade
    {
        private static ITaskManagerFacade _implementation;
        private static readonly object _syncRoot = new object();

        internal static ITaskManagerFacade Implementation
        {
            get
            {
                // Avoiding initialization in a static constructor so sql timeouts won't cause the type initialization exception
                var implementation = _implementation;
                if(implementation == null)
                {
                    lock(_syncRoot)
                    {
                        implementation = _implementation;
                        if(implementation == null)
                        {
                            _implementation = implementation = new TaskManagerFacadeImpl();
                        }
                    }
                }

                return implementation;
            } 
            set
            {
                _implementation = value;
            }
        }       



        /// <exclude />
        public static void AttachTaskCreator(Func<EntityToken, ActionToken, Task> taskCreator)
        {
            Implementation.AttachTaskCreator(taskCreator);
        }



        internal static TaskContainer CreateNewTasks(EntityToken entityToken, ActionToken actionToken, TaskManagerEvent taskManagerEvent)
        {
            return Implementation.CreateNewTasks(entityToken, actionToken, taskManagerEvent);
        }



        /// <exclude />
        public static TaskContainer RuntTasks(FlowToken flowToken, TaskManagerEvent taskManagerEvent)
        {
            return Implementation.RuntTasks(flowToken, taskManagerEvent);
        }



        internal static void CompleteTasks(FlowToken flowToken)
        {
            Implementation.CompleteTasks(flowToken);
        }
    }
}
