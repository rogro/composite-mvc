/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System.Collections.Generic;
using System.Workflow.Runtime;
using Composite.C1Console.Actions;
using Composite.C1Console.Security;


namespace Composite.C1Console.Workflow
{
    internal sealed class WorkflowActionExecutor : IActionExecutorSerializedParameters
    {
        public FlowToken Execute(string serializedEntityToken, string serializedActionToken, ActionToken actionToken, FlowControllerServicesContainer flowControllerServicesContainer)
        {
            WorkflowActionToken workflowActionToken = (WorkflowActionToken)actionToken;

            WorkflowInstance workflowInstance = WorkflowFacade.CreateNewWorkflow(
                    workflowActionToken.WorkflowType,
                    new Dictionary<string, object> { 
                            { "SerializedEntityToken", serializedEntityToken },
                            { "SerializedActionToken", serializedActionToken },
                            { "ParentWorkflowInstanceId", workflowActionToken.ParentWorkflowInstanceId }
                        }
                );

            workflowInstance.Start();

            WorkflowFacade.SetFlowControllerServicesContainer(workflowInstance.InstanceId, flowControllerServicesContainer);
            WorkflowFacade.RunWorkflow(workflowInstance);

            WorkflowFacade.SetEventHandlerFilter(workflowInstance.InstanceId, workflowActionToken.EventHandleFilterType);

            return new WorkflowFlowToken(workflowInstance.InstanceId);
        }



        public FlowToken Execute(EntityToken entityToken, ActionToken actionToken, FlowControllerServicesContainer flowControllerServicesContainer)
        {
            return Execute(EntityTokenSerializer.Serialize(entityToken), ActionTokenSerializer.Serialize(actionToken), actionToken, flowControllerServicesContainer);
        }
    }
}
