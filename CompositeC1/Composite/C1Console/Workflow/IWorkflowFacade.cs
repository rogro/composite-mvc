/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Threading;
using System.Workflow.Activities;
using System.Workflow.Runtime;
using Composite.C1Console.Actions;
using Composite.C1Console.Security;
using Composite.C1Console.Workflow.Foundation;
using Composite.C1Console.Events;


namespace Composite.C1Console.Workflow
{
	internal interface IWorkflowFacade
	{
        void EnsureInitialization();
        WorkflowRuntime WorkflowRuntime { get; }

        void RunWhenInitialized(Action action);


        #region Workflow methods
        WorkflowInstance CreateNewWorkflow(Type workflowType);
        WorkflowInstance CreateNewWorkflow(Type workflowType, Dictionary<string, object> arguments);
        WorkflowFlowToken StartNewWorkflow(Type workflowType, FlowControllerServicesContainer flowControllerServicesContainer, EntityToken entityToken, ActionToken actionToken);
        WorkflowInstance GetWorkflow(Guid instanceId);
        StateMachineWorkflowInstance GetStateMachineWorkflowInstance(Guid instanceId);
        void RunWorkflow(Guid instanceId);
        void RunWorkflow(WorkflowInstance workflowInstance);
        void AbortWorkflow(Guid instanceId);
        void AcquireLock(Guid isntanceId, EntityToken entityToken);
        #endregion


        #region FlowControllerServices methods
        void SetFlowControllerServicesContainer(Guid instanceId, FlowControllerServicesContainer flowControllerServicesContainer);
        FlowControllerServicesContainer GetFlowControllerServicesContainer(Guid instanceId);
        void RemoveFlowControllerServicesContainer(Guid instanceId);
        #endregion


        #region Workflow status methods
        bool WorkflowExists(Guid instanceId);
        Semaphore WaitForIdleStatus(Guid instanceId);
        #endregion


        #region Form workflow methods
        void SetEventHandlerFilter(Guid instanceId, Type eventHandlerFilterType);
        IEventHandleFilter GetEventHandleFilter(Guid instanceId);
        IEnumerable<string> GetCurrentFormEvents(Guid instanceId);
        IEnumerable<string> GetCurrentFormEvents(WorkflowInstance workflowInstance);
        void FireSaveEvent(Guid instanceId, Dictionary<string, object> bindings);
        void FireSaveAndPublishEvent(Guid instanceId, Dictionary<string, object> bindings);
        void FireNextEvent(Guid instanceId, Dictionary<string, object> bindings);
        void FirePreviousEvent(Guid instanceId, Dictionary<string, object> bindings);
        void FireFinishEvent(Guid instanceId, Dictionary<string, object> bindings);
        void FireCancelEvent(Guid instanceId, Dictionary<string, object> bindings);
        void FirePreviewEvent(Guid instanceId, Dictionary<string, object> bindings);
        void FireCustomEvent(int eventNumber, Guid instanceId, Dictionary<string, object> bindings);
        void FireChildWorkflowDoneEvent(Guid parentInstanceId, string workflowResult);
        #endregion


        #region FormData methods
        void AddFormData(Guid instanceId, FormData formData);
        bool TryGetFormData(Guid instanceId, out FormData formData);
        FormData GetFormData(Guid instanceId, bool allowCreationIfNotExisting);
        #endregion


        void Flush();
        void ShutDown();
        void ConsoleClosed(ConsoleClosedEventArgs args);

    }
}
