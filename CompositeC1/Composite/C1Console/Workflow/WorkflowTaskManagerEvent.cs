/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using Composite.C1Console.Tasks;
using System;
using Composite.C1Console.Actions;


namespace Composite.C1Console.Workflow
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public class WorkflowTaskManagerEvent : FlowTaskManagerEvent
    {
        /// <exclude />
        public WorkflowTaskManagerEvent(FlowToken flowToken, Guid workflowInstanceId)
            : base(flowToken)
        {
            this.WorkflowInstanceId = workflowInstanceId;
            this.EventName = "";
        }


        /// <exclude />
        public string EventName { get; set ;}


        /// <exclude />
        public Guid WorkflowInstanceId { get; private set; }
    }



    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public class WorkflowCreationTaskManagerEvent : TaskManagerEvent
    {
        /// <exclude />
        public WorkflowCreationTaskManagerEvent(Guid parentWorkflowInstanceId)
        {
            this.ParentWorkflowInstanceId = parentWorkflowInstanceId;
        }

        /// <exclude />
        public Guid ParentWorkflowInstanceId { get; private set; }
    }



    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public class SaveWorklowTaskManagerEvent : WorkflowTaskManagerEvent
    {
        /// <exclude />
        public SaveWorklowTaskManagerEvent(FlowToken flowToken, Guid workflowInstanceId, bool succeeded)
            : base(flowToken, workflowInstanceId)
        {
            this.Succeeded = succeeded;
        }


        /// <exclude />
        public bool Succeeded { get; private set; }


        /// <exclude />
        public override string ToString()
        {
            return string.Format("WorkflowInstanceId: {0}, SaveStaus: {1}", this.WorkflowInstanceId, this.Succeeded);
        }
    }
}
