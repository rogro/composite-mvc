/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.ComponentModel;
using System.Workflow.Activities;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Compiler;


namespace Composite.C1Console.Workflow.Activities
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class ConditionalSetStateActivity : Activity
    {
        /// <exclude />
        public static readonly DependencyProperty ConditionProperty = DependencyProperty.Register("Condition", typeof(ActivityCondition), typeof(ConditionalSetStateActivity), new PropertyMetadata(DependencyPropertyOptions.Metadata));

        /// <exclude />
        public static readonly DependencyProperty TrueTargetStateNameProperty = DependencyProperty.Register("TrueTargetStateName", typeof(string), typeof(ConditionalSetStateActivity), new PropertyMetadata("", DependencyPropertyOptions.Metadata, new Attribute[] { new ValidationOptionAttribute(ValidationOption.Optional) }));

        /// <exclude />
        public static readonly DependencyProperty FalseTargetStateNameProperty = DependencyProperty.Register("FalseTargetStateName", typeof(string), typeof(ConditionalSetStateActivity), new PropertyMetadata("", DependencyPropertyOptions.Metadata, new Attribute[] { new ValidationOptionAttribute(ValidationOption.Optional) }));



        /// <exclude />
        public ConditionalSetStateActivity()
        {
        }


        /// <exclude />
        public ConditionalSetStateActivity(string name) 
            : base(name)
        {
        }



        /// <exclude />
        protected override ActivityExecutionStatus Execute(ActivityExecutionContext executionContext)
        {
            StateMachineWorkflowInstance instance = WorkflowFacade.GetStateMachineWorkflowInstance(this.WorkflowInstanceId);

            bool result = Condition.Evaluate(this, executionContext);

            SetStateEventArgs args;
            if (result)
            {
                args = new SetStateEventArgs(this.TrueTargetStateName);
            }
            else
            {
                args = new SetStateEventArgs(this.FalseTargetStateName);
            }

          //  instance.EnqueueItemOnIdle("SetStateQueue", args, null, null);

            return ActivityExecutionStatus.Closed;
        }



        /// <exclude />
        [Browsable(true)]
        public ActivityCondition Condition
        {
            get { return (base.GetValue(ConditionProperty) as ActivityCondition); }
            set { base.SetValue(ConditionProperty, value); }
        }


        /// <exclude />
        [DefaultValue((string)null)]
        public string TrueTargetStateName
        {
            get { return (base.GetValue(TrueTargetStateNameProperty) as string); }
            set { base.SetValue(TrueTargetStateNameProperty, value); }
        }


        /// <exclude />
        [DefaultValue((string)null)]
        public string FalseTargetStateName
        {
            get { return (base.GetValue(FalseTargetStateNameProperty) as string); }
            set { base.SetValue(FalseTargetStateNameProperty, value); }
        }
    }
}
