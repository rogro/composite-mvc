/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;
using System.Configuration;
using Composite.Core.Collections.Generic;
using Composite.C1Console.Events;
using Composite.C1Console.Forms.Plugins.FunctionFactory;
using Composite.C1Console.Forms.Plugins.FunctionFactory.Runtime;


namespace Composite.C1Console.Forms.Foundation.PluginFacades
{
    internal static class FunctionFactoryPluginFacade
    {
        private static ResourceLocker<Resources> _resourceLocker = new ResourceLocker<Resources>(new Resources(), Resources.Initialize);



        static FunctionFactoryPluginFacade()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlushEvent);
        }



        public static IFormFunction GetFunction(string namespaceName, string name)
        {
            string compositeName = string.Format("{0}->{1}", namespaceName, name);

            if (false == _resourceLocker.Resources.FactoryCache.ContainsKey(compositeName))
            {
                try
                {
                    IFormFunctionFactory formFunctionFactory = _resourceLocker.Resources.Factory.Create(compositeName);

                    using (_resourceLocker.Locker)
                    {
                        if (_resourceLocker.Resources.FactoryCache.ContainsKey(compositeName) == false)
                        {
                            _resourceLocker.Resources.FactoryCache.Add(compositeName, formFunctionFactory);
                        }
                    }
                }
                catch (ArgumentException ex)
                {
                    HandleConfigurationError(ex);
                }
                catch (ConfigurationErrorsException ex)
                {
                    HandleConfigurationError(ex);
                }
            }


                return _resourceLocker.Resources.FactoryCache[compositeName].CreateFunction();
        }



        private static void Flush()
        {
            _resourceLocker.ResetInitialization();
        }



        private static void OnFlushEvent(FlushEventArgs args)
        {
            Flush();
        }



        private static void HandleConfigurationError(Exception ex)
        {
            Flush();

            throw new ConfigurationErrorsException(string.Format("Failed to load the configuration section '{0}' from the configuration.", FunctionFactorySettings.SectionName), ex);
        }



        private sealed class Resources
        {
            public FunctionFactoryFactory Factory { get; set; }
            public Dictionary<string, IFormFunctionFactory> FactoryCache { get; set; }

            public static void Initialize(Resources resources)
            {
                try
                {
                    resources.Factory = new FunctionFactoryFactory();
                }
                catch (NullReferenceException ex)
                {
                    HandleConfigurationError(ex);
                }
                catch (ConfigurationErrorsException ex)
                {
                    HandleConfigurationError(ex);
                }


                resources.FactoryCache = new Dictionary<string, IFormFunctionFactory>();
            }
        }
    }
}
