/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using Composite.C1Console.Forms.Foundation.FormTreeCompiler.CompileTreeNodes;


namespace Composite.C1Console.Forms.Foundation.FormTreeCompiler
{
    internal sealed class CompilerGlobals
    {
        public static readonly string DefaultPropertyName = ":default:";
        public static readonly string FormDefinition_TagName = "formdefinition";
        public static readonly string Layout_TagName = "layout";
        public static readonly string Bindings_TagName = "bindings";
        public static readonly string Binding_TagName = "binding";
        public static readonly string Bind_TagName = "bind";
        public static readonly string Read_TagName = "read";
        public static readonly string IfCondition_TagName = "ifCondition";
        public static readonly string IfWhenTrue_TagName = "ifWhenTrue";
        public static readonly string IfWhenFalse_TagName = "ifWhenFalse";
        public static readonly string RootNamespaceURI = "http://www.composite.net/ns/management/bindingforms/1.0";


        public static bool IsProducerTag(CompileTreeNode node)
        {
            ElementCompileTreeNode element = node as ElementCompileTreeNode;

            if (null == element) return false;

            if (IsElementEmbeddedProperty(element)) return false;
            if (IsReadTag(element)) return false;
            if (IsBindTag(element)) return false;
            if (IsBindingTag(element)) return false;            
            if (IsBindingsTag(element)) return false;
            if (IsLayoutTag(element)) return false;
            if (IsFormDefinitionTag(element)) return false;

            return true;
        }

        public static bool IsElementEmbeddedProperty(ElementCompileTreeNode element)
        {
            return element.XmlSourceNodeInformation.Name.Contains(".");
        }

        public static bool IsReadTag(ElementCompileTreeNode element)
        {
            return (element.XmlSourceNodeInformation.Name == Read_TagName) && (element.XmlSourceNodeInformation.NamespaceURI == RootNamespaceURI);
        }

        public static bool IsBindTag(ElementCompileTreeNode element)
        {
            return (element.XmlSourceNodeInformation.Name == Bind_TagName) && (element.XmlSourceNodeInformation.NamespaceURI == RootNamespaceURI);
        }

        public static bool IsBindingTag(ElementCompileTreeNode element)
        {
            return (element.XmlSourceNodeInformation.Name == Binding_TagName) && (element.XmlSourceNodeInformation.NamespaceURI == RootNamespaceURI);
        }

        public static bool IsBindingsTag(ElementCompileTreeNode element)
        {
            return (element.XmlSourceNodeInformation.Name == Bindings_TagName) && (element.XmlSourceNodeInformation.NamespaceURI == RootNamespaceURI);
        }

        public static bool IsLayoutTag(ElementCompileTreeNode element)
        {
            return (element.XmlSourceNodeInformation.Name == Layout_TagName) && (element.XmlSourceNodeInformation.NamespaceURI == RootNamespaceURI);
        }

        public static bool IsFormDefinitionTag(ElementCompileTreeNode element)
        {
            return (element.XmlSourceNodeInformation.Name == FormDefinition_TagName) && (element.XmlSourceNodeInformation.NamespaceURI == RootNamespaceURI);
        }

        public static bool IsElementIfConditionTag(ElementCompileTreeNode element)
        {
            return (element.XmlSourceNodeInformation.Name == IfCondition_TagName) && (element.XmlSourceNodeInformation.NamespaceURI == RootNamespaceURI);
        }

        public static bool IsElementIfWhenTrueTag(ElementCompileTreeNode element)
        {
            return (element.XmlSourceNodeInformation.Name == IfWhenTrue_TagName) && (element.XmlSourceNodeInformation.NamespaceURI == RootNamespaceURI);
        }

        public static bool IsElementIfWhenFalseTag(ElementCompileTreeNode element)
        {
            return (element.XmlSourceNodeInformation.Name == IfWhenFalse_TagName) && (element.XmlSourceNodeInformation.NamespaceURI == RootNamespaceURI);
        }

        public static void GetSplittedPropertyNameFromCompositeName(CompileTreeNode node, out string producerName, out string propertyName)
        {
            string[] split = node.XmlSourceNodeInformation.Name.Split('.');
            if (2 != split.Length) throw new FormCompileException("Wrong tag format", node.XmlSourceNodeInformation);

            producerName = split[0];
            propertyName = split[1];
        }
    }

}
