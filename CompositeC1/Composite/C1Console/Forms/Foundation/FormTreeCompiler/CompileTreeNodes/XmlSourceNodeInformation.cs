/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

namespace Composite.C1Console.Forms.Foundation.FormTreeCompiler.CompileTreeNodes
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class XmlSourceNodeInformation
    {
        private int _depth;
        private string _name;
        private string _tagName;
        private string _namespaceURI;
        private string _xPath;

        /// <exclude />
        public XmlSourceNodeInformation(int depth, string name, string tagName, string namespaceURI)
        {
            _depth = depth;
            _name = name;
            _tagName = tagName;
            _namespaceURI = namespaceURI;
            _xPath = "";
        }

        /// <exclude />
        public int Depth
        {
            get { return _depth; }
        }

        /// <exclude />
        public string Name
        {
            get { return _name; }
        }

        /// <exclude />
        public string TagName
        {
            get { return _tagName; }
        }

        /// <exclude />
        public string NamespaceURI
        {
            get { return _namespaceURI; }
            set { _namespaceURI = value; }
        }

        /// <exclude />
        public string XPath
        {
            get { return _xPath; }
            set { _xPath = value; }
        }
    }
}
