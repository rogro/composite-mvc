/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Diagnostics;
using Composite.Data.Validation.ClientValidationRules;
using System.Collections.Generic;


namespace Composite.C1Console.Forms.Foundation.FormTreeCompiler.CompileTreeNodes
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    [DebuggerDisplay("PropertyCompileTreeNode: {Name} = {Value}")]
    public sealed class PropertyCompileTreeNode : CompileTreeNode
    {
        private string _name = "";        
        private object _value = null;
        private string _inclosingProducerName = "";

        /// <exclude />
        public PropertyCompileTreeNode(string name, XmlSourceNodeInformation sourceInformation, bool isNamespaceDeclaration = false)
            : base(sourceInformation)
        {
            _name = name;
            IsNamespaceDeclaration = isNamespaceDeclaration;
        }


        /// <exclude />
        public string Name
        {
            get { return _name; }
        }


        /// <exclude />
        public bool IsNamespaceDeclaration
        {
            get;
            private set;
        }

        /// <exclude />
        public object Value
        {
            get { return _value; }
            set { _value = value; }
        }


        /// <exclude />
        public string InclosingProducerName
        {
            get { return _inclosingProducerName; }
            set { _inclosingProducerName = value; }
        }


        /// <exclude />
        public List<ClientValidationRule> ClientValidationRules
        {
            get;
            set;
        }
    }
}
