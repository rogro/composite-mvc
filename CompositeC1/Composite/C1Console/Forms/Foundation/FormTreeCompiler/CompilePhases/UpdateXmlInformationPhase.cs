/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;

using Composite.C1Console.Forms.Foundation.FormTreeCompiler.CompileTreeNodes;


namespace Composite.C1Console.Forms.Foundation.FormTreeCompiler.CompilePhases
{
    internal sealed class UpdateXmlInformationPhase
    {
        public void UpdateInformation(CompileTreeNode startNode)
        {
            UpdateInformation(startNode, "", null);
        }


        private void UpdateInformation(CompileTreeNode node, string currentXPath, int? childNumber)
        {
            if (childNumber.HasValue)
            {
                currentXPath = string.Format("{0}/{1}[{2}]", currentXPath, node.XmlSourceNodeInformation.TagName, childNumber.Value);
            }
            else
            {
                currentXPath = string.Format("{0}/{1}", currentXPath, node.XmlSourceNodeInformation.TagName);
            }

            node.XmlSourceNodeInformation.XPath = currentXPath;

            Dictionary<string, int> tagOccursCount = new Dictionary<string, int>();
            Dictionary<string, int> xPathCounter = new Dictionary<string, int>();

            foreach (CompileTreeNode child in node.Children)
            {
                if (false == tagOccursCount.ContainsKey(child.XmlSourceNodeInformation.Name)) tagOccursCount.Add(child.XmlSourceNodeInformation.Name, 0);

                tagOccursCount[child.XmlSourceNodeInformation.Name]++;
            }

            foreach (CompileTreeNode child in node.Children)
            {
                if (tagOccursCount[child.XmlSourceNodeInformation.Name] > 1)
                {
                    if (false == xPathCounter.ContainsKey(child.XmlSourceNodeInformation.Name)) xPathCounter.Add(child.XmlSourceNodeInformation.Name, 1);

                    UpdateInformation(child, currentXPath, xPathCounter[child.XmlSourceNodeInformation.Name]++);
                }
                else
                {
                    UpdateInformation(child, currentXPath, null);
                }
            }

            foreach (PropertyCompileTreeNode nameProperty in node.AllNamedProperties)
            {
                nameProperty.XmlSourceNodeInformation.NamespaceURI = node.XmlSourceNodeInformation.NamespaceURI;

                UpdateInformation(nameProperty, currentXPath, null);
            }

            if (node.DefaultProperties.Count == 1)
            {
                node.DefaultProperties[0].XmlSourceNodeInformation.NamespaceURI = node.XmlSourceNodeInformation.NamespaceURI;

                UpdateInformation(node.DefaultProperties[0], currentXPath, null);
            }
            else if (node.DefaultProperties.Count > 1)
            {
                int counter = 1;
                foreach (CompileTreeNode defaultProperty in node.DefaultProperties)
                {
                    defaultProperty.XmlSourceNodeInformation.NamespaceURI = node.XmlSourceNodeInformation.NamespaceURI;

                    UpdateInformation(defaultProperty, currentXPath, counter++);
                }
            }
        }
    }
}
