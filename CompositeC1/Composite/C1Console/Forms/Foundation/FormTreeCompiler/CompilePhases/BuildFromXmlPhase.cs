/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Linq;
using System.Xml.Linq;
using Composite.C1Console.Forms.Foundation.FormTreeCompiler.CompileTreeNodes;
using Composite.Core.ResourceSystem;

namespace Composite.C1Console.Forms.Foundation.FormTreeCompiler.CompilePhases
{
    /// <summary>
    /// Converts form markup xml into tree of <see cref="CompileTreeNode"/>. Translates properties
    /// </summary>
    internal static class BuildFromXmlPhase
    {
        public static CompileTreeNode BuildTree(XDocument document)
        {
            return BuildRec(document.Root);
        }

        public static ElementCompileTreeNode BuildRec(XElement element)
        {
            int depth = element.Ancestors().Count();

            var debugInfo = new XmlSourceNodeInformation(depth, element.Name.LocalName, element.Name.LocalName, element.Name.NamespaceName);

            var result = new ElementCompileTreeNode(debugInfo);
            foreach (var attribute in element.Attributes())
            {
                if (attribute.Name.LocalName == "xmlns") continue;

                bool isNamespaceDeclaration = attribute.Name.Namespace == "http://www.w3.org/2000/xmlns/";

                var property = new PropertyCompileTreeNode(attribute.Name.LocalName, debugInfo, isNamespaceDeclaration);
                property.Value = StringResourceSystemFacade.ParseString(attribute.Value);

                result.AddNamedProperty(property);
            }

            foreach (var node in element.Nodes())
            {
                if (node is XElement)
                {
                    result.Children.Add(BuildRec(node as XElement));
                    continue;
                }

                if (node is XText)
                {
                    string text = (node as XText).Value;

                    if (string.IsNullOrWhiteSpace(text))
                    {
                        continue;
                    }

                    var textProperty = new PropertyCompileTreeNode(CompilerGlobals.DefaultPropertyName, debugInfo);
                    textProperty.Value = StringResourceSystemFacade.ParseString(text);

                    result.DefaultProperties.Add(textProperty);
                    continue;
                }
            }

            return result;
        }
    }
}
