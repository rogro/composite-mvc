/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using Composite.C1Console.Forms.Foundation.PluginFacades;
using Composite.C1Console.Forms.Foundation.FormTreeCompiler.CompileTreeNodes;


namespace Composite.C1Console.Forms.Foundation.FormTreeCompiler.CompilePhases
{
    internal sealed class CreateProducersPhase
    {
        private CompileContext _compileContext;


        public CreateProducersPhase(CompileContext compileContext)
        {
            _compileContext = compileContext;
        }



        public void CreateProducers(CompileTreeNode node)
        {
            if (node is ElementCompileTreeNode)
            {
                ElementCompileTreeNode element = node as ElementCompileTreeNode;

                if ((CompilerGlobals.IsProducerTag(element)) ||
                    (CompilerGlobals.IsReadTag(element)) ||
                    (CompilerGlobals.IsBindTag(element)) ||
                    (CompilerGlobals.IsBindingTag(element)) ||
                    (CompilerGlobals.IsLayoutTag(element)))
                {
                    element.Producer = ProducerMediatorPluginFacade.CreateProducer(_compileContext.CurrentChannel, element.XmlSourceNodeInformation.NamespaceURI, element.XmlSourceNodeInformation.Name);

                    if (element.Producer is IUiControl)
                    {
                        (element.Producer as IUiControl).UiControlID = _compileContext.GetNextControlId(element.XmlSourceNodeInformation.Name);
                        
                    }
                }
                else if (CompilerGlobals.IsBindingsTag(element))
                {
                    if (null == _compileContext.BindingsProducer)
                    {
                        _compileContext.BindingsProducer = ProducerMediatorPluginFacade.CreateProducer(_compileContext.CurrentChannel, element.XmlSourceNodeInformation.NamespaceURI, element.XmlSourceNodeInformation.Name);
                    }

                    element.Producer = _compileContext.BindingsProducer;
                }
            }

            foreach (CompileTreeNode subNode in node.AllSubNodes) CreateProducers(subNode);
        }
    }
}
