/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;
using Composite.Data.Validation.ClientValidationRules;


namespace Composite.C1Console.Forms.Foundation
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public abstract class UiControl : IUiControl
    {
        private string _label = "";
        private string _help = "";


        /// <exclude />
        public UiControl()
        {
            this.SourceBindingPaths = new List<string>();
        }

        /// <summary>
        /// The unique and permanent ID of the UiControl instance.
        /// </summary>
        public string UiControlID { get; set; }

        /// <summary>
        /// The channel name of the UiControl instance.
        /// </summary>
        public IFormChannelIdentifier UiControlChannel { get; set; }

        /// <summary>
        /// The label of the UiControl. Containers may use this value when applying layout to a list of UiControls.
        /// </summary>
        [FormsProperty()]
        public virtual string Label
        {
            get { return _label; }
            set { _label = value; }
        }

        /// <summary>
        /// Short context sensitive help relevant to the control.
        /// </summary>
        [FormsProperty()]
        public virtual string Help
        {
            get { return _help; }
            set { _help = value; }
        }


        /// <exclude />
        public List<ClientValidationRule> ClientValidationRules
        {
            get;
            set;
        }

        /// <summary>
        /// When invoked, UiControl Properies that expose bindable data must be updated to reflect user induced state.
        /// I.e. the Text property of a TextBox UiControl should be assigned the Text property of it's inner control, so
        /// the Composite.C1Console.Forms Manager can access the users input. 
        /// </summary>
        public virtual void BindStateToControlProperties()
        { }


        /// <summary>
        /// A (short) message to the user relating to this control, typically information about missing or 
        /// bad input.
        /// </summary>
        public List<string> SourceBindingPaths { get; set; }

    }
}
