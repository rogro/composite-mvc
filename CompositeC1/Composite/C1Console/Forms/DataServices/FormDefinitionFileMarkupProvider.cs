/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using Composite.C1Console.Forms.Flows;
using Composite.Data;
using Composite.Data.Types;


namespace Composite.C1Console.Forms.DataServices
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public class FormDefinitionFileMarkupProvider : IFormMarkupProvider
    {
        private string _formPath;


        /// <exclude />
        public FormDefinitionFileMarkupProvider(string formPath)
        {
            if (string.IsNullOrEmpty(formPath)) throw new ArgumentException("Path can not be empty", formPath);
            _formPath = formPath;
        }


        /// <exclude />
        public XmlReader GetReader()
        {
            string folderPath = Path.GetDirectoryName(_formPath).ToLowerInvariant();
            string fileName = Path.GetFileName(_formPath).ToLowerInvariant();

            List<IFormDefinitionFile> formFiles =
                (from file in DataFacade.GetData<IFormDefinitionFile>()
                 where file.FolderPath.ToLowerInvariant() == folderPath && file.FileName.ToLowerInvariant() == fileName
                 select file).ToList();

            if (formFiles.Count == 0) throw new InvalidOperationException(string.Format("No form definition with path '{0}' was found. Please use a virtual Form Path", _formPath));
            if (formFiles.Count > 1) throw new InvalidOperationException(string.Format("Multiple form definitions with path '{0}' was found", _formPath));

            return new XmlTextReader(formFiles[0].GetReadStream());
        }
    }
}