/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using Composite.C1Console.Forms.Foundation;
using System;
using System.Collections.Generic;
using Composite.Core.Types;
using System.ComponentModel;
using System.Globalization;

namespace Composite.C1Console.Forms.CoreUiControls
{
    [Flags]
    [TypeConverter(typeof(UiControlTypeSelectorModeConverter))]
    internal enum UiControlTypeSelectorMode 
    {
        // Composite selector types
        /// <summary>
        /// Include interfaces in the result.
        /// </summary>
        InterfaceTypes = 0x01,
        /// <summary>
        /// Include concrete types in the result.
        /// </summary>
        ConcreteTypes = 0x02,
        /// <summary>
        /// Include primitives in the result.
        /// </summary>
        PrimitiveTypes = 0x04,
    }

    internal class UiControlTypeSelectorModeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            string val = value as string;

            val = val.ToLowerInvariant();
            string[] options = val.Split('|');
            UiControlTypeSelectorMode mode = new UiControlTypeSelectorMode();
            foreach (string opt in options)
            {
                string option = opt.Trim();
                if (option == TypeIncludes.ConcreteTypes.ToString().ToLowerInvariant())
                {
                    mode = mode | UiControlTypeSelectorMode.ConcreteTypes;
                }
                else if (option == TypeIncludes.InterfaceTypes.ToString().ToLowerInvariant())
                {
                     mode = mode | UiControlTypeSelectorMode.InterfaceTypes;
                }
                else if (option == TypeIncludes.PrimitiveTypes.ToString().ToLowerInvariant())
                {
                     mode = mode | UiControlTypeSelectorMode.PrimitiveTypes;
                }
                else
                {
                    throw new FormatException(String.Format("{0} is not a valid TextSelector Mode value - use ConcreteTypes, InterfaceTypes, PrimitiveTypes", value));
                }
            }
            return mode;
        }
    }

    [ControlValueProperty("SelectedType")]
    internal abstract class TypeSelectorUiControl : UiControl
    {


        [BindableProperty()]
        [FormsProperty()]
        public Type SelectedType { get; set; }

        [FormsProperty()]
        public IEnumerable<Type> TypeOptions { get; set; }
        [FormsProperty()]
        public Type AssignableTo { get; set; }
        [FormsProperty()]
        public UiControlTypeSelectorMode Mode { get; set; }

        private static bool IsSet(UiControlTypeSelectorMode flags, UiControlTypeSelectorMode compareFlag)
        {
            return ((flags & compareFlag) == compareFlag);
        }



        private IEnumerable<Type> EnumerateWithCompositeSelector()
        {
            TypeIncludes includes = new TypeIncludes();
            if (IsSet(Mode, UiControlTypeSelectorMode.ConcreteTypes))
            {
                includes = TypeIncludes.ConcreteTypes;
            }
            if (IsSet(Mode, UiControlTypeSelectorMode.InterfaceTypes))
            {
                includes = includes | TypeIncludes.InterfaceTypes;
            }
            if (IsSet(Mode, UiControlTypeSelectorMode.PrimitiveTypes))
            {
                includes = includes | TypeIncludes.PrimitiveTypes;
            }
            return TypeLocator.FindTypes(includes, this.AssignableTo);
        }



        protected IEnumerable<Type> GetTypeOptions()
        {
            if (this.AssignableTo != null && this.TypeOptions != null) 
                throw new InvalidOperationException("Both TypeOptions and AssignableTo has been set. Only one may be set.");

            if (this.TypeOptions != null) 
                return this.TypeOptions;

            if (this.AssignableTo == null)
            {
                throw new ArgumentNullException("AssignableTo");
            }

            return EnumerateWithCompositeSelector();
        }
    }
}
