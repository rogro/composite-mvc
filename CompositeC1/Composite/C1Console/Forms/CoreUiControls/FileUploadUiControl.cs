/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Composite.C1Console.Forms.Foundation;
using Composite.Core.Serialization;


namespace Composite.C1Console.Forms.CoreUiControls
{
    /// <summary>
    /// </summary>
    [ControlValueProperty("UploadedFile")]
    internal abstract class FileUploadUiControl : UiControl
    {
        /// <summary>
        /// </summary>
        [RequiredValue()]
        [BindableProperty]
        [FormsProperty()]
        public UploadedFile UploadedFile { get; set; }
    }


    internal sealed class UploadedFileSerializerHandler : ISerializerHandler
    {
        public string Serialize(object objectToSerialize)
        {
            if (objectToSerialize == null) throw new ArgumentNullException("objectToSerialize");

            StringBuilder sb = new StringBuilder();
            UploadedFile uploadedFile = (UploadedFile)objectToSerialize;

            StringConversionServices.SerializeKeyValuePair(sb, "HasFile", uploadedFile.HasFile);

            if (uploadedFile.FileName != null)
            {
                StringConversionServices.SerializeKeyValuePair(sb, "FileName", uploadedFile.FileName);
            }

            if (uploadedFile.ContentType != null)
            {
                StringConversionServices.SerializeKeyValuePair(sb, "ContentType", uploadedFile.ContentType);
            }

            StringConversionServices.SerializeKeyValuePair(sb, "ContentLength", uploadedFile.ContentLength);

            if (uploadedFile.FileStream != null)
            {
                long position = uploadedFile.FileStream.Position;

                uploadedFile.FileStream.Seek(0, SeekOrigin.Begin);
                
                byte[] buffer = new byte[uploadedFile.FileStream.Length];
                uploadedFile.FileStream.Read(buffer, 0, (int)uploadedFile.FileStream.Length);

                StringConversionServices.SerializeKeyValueArrayPair<byte>(sb, "FileStream", buffer);

                uploadedFile.FileStream.Seek(position, SeekOrigin.Begin);
            }

            return sb.ToString();
        }



        public object Deserialize(string serializedObject)
        {
            if (serializedObject == null) throw new ArgumentNullException("serializedObject");

            Dictionary<string, string> dic = StringConversionServices.ParseKeyValueCollection(serializedObject);

            UploadedFile uploadedFile = new UploadedFile();

            if (dic.ContainsKey("HasFile") == false) throw new InvalidOperationException("Not correct serialized format");
            uploadedFile.HasFile = StringConversionServices.DeserializeValueBool(dic["HasFile"]);

            if (dic.ContainsKey("FileName"))
            {
                uploadedFile.FileName = StringConversionServices.DeserializeValueString(dic["FileName"]);
            }

            if (dic.ContainsKey("ContentType"))
            {
                uploadedFile.ContentType = StringConversionServices.DeserializeValueString(dic["ContentType"]);
            }

            if (dic.ContainsKey("ContentLength") == false) throw new InvalidOperationException("Not correct serialized format");
            uploadedFile.ContentLength = StringConversionServices.DeserializeValueInt(dic["ContentLength"]);

            if (dic.ContainsKey("FileStream"))
            {
                byte[] bytes = StringConversionServices.DeserializeValueArray<byte>(dic["FileStream"]);

                uploadedFile.FileStream = new MemoryStream(bytes);
            }

            return uploadedFile;
        }
    }


    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [SerializerHandler(typeof(UploadedFileSerializerHandler))]
    public sealed class UploadedFile
    {
        /// <exclude />
        public UploadedFile()
        {
            this.HasFile = false;
        }

        /// <exclude />
        public bool HasFile { get; set; }

        /// <exclude />
        public string FileName { get; set; }

        /// <exclude />
        public string ContentType { get; set; }

        /// <exclude />
        public int ContentLength { get; set; }

        /// <exclude />
        public Stream FileStream { get; set; }
    }
}
