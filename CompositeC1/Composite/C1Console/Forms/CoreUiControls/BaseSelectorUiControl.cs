/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections;
using System.ComponentModel;
using System.Globalization;
using Composite.C1Console.Forms.Foundation;

namespace Composite.C1Console.Forms.CoreUiControls
{
    internal abstract class BaseSelectorUiControl : UiControl
    {
        public BaseSelectorUiControl()
        {
            this.OptionsKeyField = ".";
            this.OptionsLabelField = ".";
            this.Required = true;
        }

        [RequiredValue()]
        [FormsProperty()]
        public IEnumerable Options { get; set; }

        [FormsProperty()]
        public string OptionsKeyField { get; set; }

        [FormsProperty()]
        public string OptionsLabelField { get; set; }

        [FormsProperty()]
        public SelectorBindingType BindingType { get; set; }

        [FormsProperty()]
        public bool Required { get; set; }

        [BindableProperty()]
        [FormsProperty()]
        public EventHandler SelectedIndexChangedEventHandler { get; set; }
    }


    [TypeConverter(typeof(SelectorBindingTypeConverter))]
    internal enum SelectorBindingType
    {
        BindToObject,
        BindToKeyFieldValue
    }


    internal class SelectorBindingTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            string val = value as string;

            val = val.ToLowerInvariant();

            switch (val)
            {
                case "bindtoobject":
                    return SelectorBindingType.BindToObject;

                case "bindtokeyfieldvalue":
                    return SelectorBindingType.BindToKeyFieldValue;

                default:
                    throw new FormatException(String.Format("{0} is not a valid Selector BindingType value - use BindToObject or BindToKeyFieldValue", value));
            }
        }
    }
}
