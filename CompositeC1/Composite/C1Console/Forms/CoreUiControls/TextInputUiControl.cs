/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Globalization;
using System.ComponentModel;
using Composite.C1Console.Forms.Foundation;


namespace Composite.C1Console.Forms.CoreUiControls
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public enum TextBoxType
    {
        /// <exclude />
        String = 0,

        /// <exclude />
        Integer = 1,

        /// <exclude />
        Decimal = 2,

        /// <exclude />
        Password = 3,

        /// <exclude />
        ProgrammingIdentifier = 4,
        
        /// <exclude />
        ProgrammingNamespace = 5,

        /// <exclude />
        ReadOnly = 6,

        /// <exclude />
        Guid = 7
    }



    [ControlValueProperty("Text")]
    internal abstract class TextInputUiControl : UiControl
    {
        public TextInputUiControl()
        {
            this.Text = "";
            this.Type = TextBoxType.String;
            this.Required = false;
            this.SpellCheck = true;
        }

        [BindableProperty]
        [FormsProperty]
        public string Text { get; set; }


        [FormsProperty]
        public TextBoxType Type { get; set; }


        [FormsProperty]
        public bool Required { get; set; }


        [FormsProperty]
        public bool SpellCheck { get; set; }
    }

}
