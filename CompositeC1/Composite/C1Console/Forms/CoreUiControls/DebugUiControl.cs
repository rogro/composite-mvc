/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;

using Composite.C1Console.Forms.Foundation;


namespace Composite.C1Console.Forms.CoreUiControls
{
    [ControlValueProperty("UiControl")]
    internal abstract class DebugUiControl : UiControl
    {
        private IUiControl _uiControl;
        private string _tagName;
        private List<BindingInformation> _bindings = new List<BindingInformation>();
        private string _sourceElementXPath;


        public IUiControl UiControl
        {
            get { return _uiControl; }
            set { _uiControl = value; }
        }

        public string TagName
        {
            get { return _tagName; }
            set { _tagName = value; }
        }

        [FormsProperty()]
        public override string Label
        {
            get
            {
                return _uiControl.Label;
            }
            set
            {
                throw new InvalidOperationException( "Debug.Label may not be assigned" );
            }
        }

        public List<BindingInformation> Bindings
        {
            get { return _bindings; }
        }

        public string SourceElementXPath
        {
            get { return _sourceElementXPath; }
            set { _sourceElementXPath = value; }
        }


        internal class BindingInformation
        {
            private string _bindingObjectName;
            private Type _bindingObjectType;
            private string _bindingPropertyName;

            public BindingInformation(string bindingObjectName, Type bindingObjectType, string bindingPropertyName)
            {
                _bindingObjectName = bindingObjectName;
                _bindingObjectType = bindingObjectType;
                _bindingPropertyName = bindingPropertyName;
            }

            public string BindingObjectName
            {
                get { return _bindingObjectName; }
            }

            public Type BindingObjectType
            {
                get { return _bindingObjectType; }
            }

            public string BindingPropertyName
            {
                get { return _bindingPropertyName; }
            }
        }
    }
}
