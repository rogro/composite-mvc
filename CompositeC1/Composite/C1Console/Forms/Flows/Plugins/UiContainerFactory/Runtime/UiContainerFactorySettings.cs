/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System.Configuration;

using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Composite.Core.Configuration;


namespace Composite.C1Console.Forms.Flows.Plugins.UiContainerFactory.Runtime
{
    internal sealed class UiContainerFactorySettings : SerializableConfigurationSection
    {
        public const string SectionName = "Composite.C1Console.Forms.Flows.Plugins.UiContainerFactoryConfiguration";


        private const string _channelsProperty = "Channels";
        [ConfigurationProperty(_channelsProperty, IsRequired = true)]
        public ChannelConfigurationElementCollection Channels
        {
            get
            {
                return (ChannelConfigurationElementCollection)base[_channelsProperty];
            }
        }
    }



    internal sealed class ChannelConfigurationElement : NamedConfigurationElement
    {
        private const string _factoriesPropertyName = "Factories";
        [ConfigurationProperty(_factoriesPropertyName, IsRequired = true)]
        public NameTypeManagerTypeConfigurationElementCollection<UiContainerFactoryData> Factories
        {
            get { return (NameTypeManagerTypeConfigurationElementCollection<UiContainerFactoryData>)base[_factoriesPropertyName]; }
        }
    }



    internal sealed class ChannelConfigurationElementCollection : ConfigurationElementCollection
    {
        public ChannelConfigurationElementCollection()
            : base()
        {
            AddElementName = "Channel";
        }

        public void Add(ChannelConfigurationElement channelConfigurationElement)
        {
            BaseAdd(channelConfigurationElement);
        }

        new public ChannelConfigurationElement this[string Name]
        {
            get
            {
                return (ChannelConfigurationElement)BaseGet(Name);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new ChannelConfigurationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as ChannelConfigurationElement).Name;
        }
    }

}
