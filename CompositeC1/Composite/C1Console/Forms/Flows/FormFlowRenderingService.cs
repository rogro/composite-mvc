/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Web;
using System.Collections.Generic;
using Composite.Core.WebClient;


namespace Composite.C1Console.Forms.Flows
{
    internal class FormFlowRenderingService : IFormFlowRenderingService
    {
        private Dictionary<string, string> _bindingPathedMessages;

        public void RerenderView()
        {
            this.RerenderViewRequested = true;
        }

        public bool RerenderViewRequested { get; private set; }


        public bool HasFieldMessages
        {
            get { return (_bindingPathedMessages != null && _bindingPathedMessages.Count > 0); }
        }


        public void ShowFieldMessages(Dictionary<string, string> bindingPathedMessages)
        {
            if (_bindingPathedMessages == null)
            {
                _bindingPathedMessages = new Dictionary<string, string>();
            }

            foreach (var msgElement in bindingPathedMessages)
            {
                _bindingPathedMessages.Add(msgElement.Key, msgElement.Value);
            }
        }


        internal Dictionary<string, string> BindingPathedMessages
        {
            get { return _bindingPathedMessages; }
        }


        public void ShowFieldMessage(string fieldBindingPath, string message)
        {
            Verify.ArgumentNotNullOrEmpty(fieldBindingPath, "fieldBindingPath");
            Verify.ArgumentNotNullOrEmpty(message, "message");

            if (_bindingPathedMessages == null)
            {
                _bindingPathedMessages = new Dictionary<string, string>();
            }

            if (_bindingPathedMessages.ContainsKey(fieldBindingPath))
            {
                _bindingPathedMessages[fieldBindingPath] += "\n" + message;
            }
            else
            {
                _bindingPathedMessages.Add(fieldBindingPath, message);
            }
        }


        public void SetSaveStatus(bool succeeded)
        {
            var httpContext = HttpContext.Current;

            (httpContext.Handler as FlowPage).SaveStepSucceeded = succeeded;
        }
    }
}
