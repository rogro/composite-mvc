/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System.Collections.Generic;
using System.Xml;
using Composite.C1Console.Actions;
using Composite.Data.Validation.ClientValidationRules;


namespace Composite.C1Console.Forms.Flows
{
    internal delegate Dictionary<string, object> DelegatedBindingsProvider();

    internal delegate XmlReader DelegatedMarkupProvider();


    internal class FormFlowUiDefinition : VisualFlowUiDefinitionBase
    {
        private Dictionary<IFormEventIdentifier, FormFlowEventHandler> _eventHandlers = new Dictionary<IFormEventIdentifier, FormFlowEventHandler>();


        public FormFlowUiDefinition(IFormMarkupProvider markupProvider, IFlowUiContainerType containerType, string containerLabel, IBindingsProvider bindingsProvider, Dictionary<string, List<ClientValidationRule>> bindingsValidationRules)
        {
            this.MarkupProvider = markupProvider;
            this.UiContainerType = containerType;
            this.ContainerLabel = containerLabel;
            this.BindingsProvider = bindingsProvider;
            this.BindingsValidationRules = bindingsValidationRules;
        }


        public FormFlowUiDefinition(IFormMarkupProvider markupProvider, IFlowUiContainerType containerType, string containerLabel, Dictionary<string, object> bindings)
            : this(markupProvider, containerType, containerLabel, new PreLoadedBindingsProvider(bindings), null)
        { }


        public FormFlowUiDefinition(IFormMarkupProvider markupProvider, IFlowUiContainerType containerType, string containerLabel, Dictionary<string, object> bindings, Dictionary<string, List<ClientValidationRule>> bindingsValidationRules)
            : this(markupProvider, containerType, containerLabel, new PreLoadedBindingsProvider(bindings), bindingsValidationRules)
        { }        

        public FormFlowUiDefinition(XmlReader markupReader, IFlowUiContainerType containerType, string containerLabel, Dictionary<string, object> bindings)
            : this(new PreLoadedMarkupProvider(markupReader), containerType, containerLabel, new PreLoadedBindingsProvider(bindings), null)
        { }

        public FormFlowUiDefinition(XmlReader markupReader, IFlowUiContainerType containerType, string containerLabel, Dictionary<string, object> bindings, Dictionary<string, List<ClientValidationRule>> bindingsValidationRules)
            : this(new PreLoadedMarkupProvider(markupReader), containerType, containerLabel, new PreLoadedBindingsProvider(bindings), bindingsValidationRules)
        { }

        public FormFlowUiDefinition(IFormMarkupProvider markupProvider, IFlowUiContainerType containerType, string containerLabel, DelegatedBindingsProvider delegatedBindingsProvider)
            : this(markupProvider, containerType, containerLabel, new DelegateBasedBindingsProvider(delegatedBindingsProvider), null)
        { }


        public FormFlowUiDefinition(DelegatedMarkupProvider delegatedMarkupProvider, IFlowUiContainerType containerType, string containerLabel, DelegatedBindingsProvider delegatedBindingsProvider)
            : this(new DelegateBasedMarkupProvider(delegatedMarkupProvider), containerType, containerLabel, new DelegateBasedBindingsProvider(delegatedBindingsProvider), null)
        { }


        public IFormMarkupProvider MarkupProvider { get; private set; }

        public IFormMarkupProvider CustomToolbarItemsMarkupProvider { get; private set; }

        public void SetCustomToolbarMarkupProvider(XmlReader markupReader)
        {
            this.CustomToolbarItemsMarkupProvider = new PreLoadedMarkupProvider(markupReader);
        }

        public void SetCustomToolbarMarkupProvider(IFormMarkupProvider markupProvider)
        {
            this.CustomToolbarItemsMarkupProvider = markupProvider;
        }

        public override IFlowUiContainerType UiContainerType { get; protected set; }

        public IBindingsProvider BindingsProvider { get; private set; }

        public Dictionary<IFormEventIdentifier, FormFlowEventHandler> EventHandlers
        {
            get { return _eventHandlers; }
        }

        public Dictionary<string, List<ClientValidationRule>> BindingsValidationRules
        {
            get;
            private set;
        }


        private class PreLoadedMarkupProvider : IFormMarkupProvider
        {
            private XmlReader _xmlReader;

            public PreLoadedMarkupProvider(XmlReader xmlReader)
            {
                _xmlReader = xmlReader;
            }

            public XmlReader GetReader()
            {
                return _xmlReader;
            }
        }



        private class PreLoadedBindingsProvider : IBindingsProvider
        {
            private Dictionary<string, object> _bindings;

            public PreLoadedBindingsProvider(Dictionary<string, object> bindings)
            {
                _bindings = bindings;
            }

            public Dictionary<string, object> GetBindings()
            {
                return _bindings;
            }
        }



        private class DelegateBasedBindingsProvider : IBindingsProvider
        {
            private DelegatedBindingsProvider _delegatedBindingsProvider;

            internal DelegateBasedBindingsProvider(DelegatedBindingsProvider delegatedBindingsProvider)
            {
                _delegatedBindingsProvider = delegatedBindingsProvider;
            }

            public Dictionary<string, object> GetBindings()
            {
                return _delegatedBindingsProvider.Invoke();
            }
        }



        private class DelegateBasedMarkupProvider : IFormMarkupProvider
        {
            private DelegatedMarkupProvider _delegatedMarkupProvider;

            internal DelegateBasedMarkupProvider(DelegatedMarkupProvider delegatedMarkupProvider)
            {
                _delegatedMarkupProvider = delegatedMarkupProvider;
            }

            public XmlReader GetReader()
            {
                return _delegatedMarkupProvider.Invoke();
            }
        }
    }
}
