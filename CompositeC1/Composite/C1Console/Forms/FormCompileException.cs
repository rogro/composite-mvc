/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;

using Composite.C1Console.Forms.Foundation.FormTreeCompiler.CompileTreeNodes;


namespace Composite.C1Console.Forms
{
    internal class FormCompileException : ApplicationException
    {
        private readonly XmlSourceNodeInformation _xmlInformation;
        private readonly XmlSourceNodeInformation _propertyXmlInformation;
        private readonly XmlSourceNodeInformation _extraPropertyXmlInformation;

        public FormCompileException(string message, XmlSourceNodeInformation xmlInformation)
            : base(message)
        {
            _xmlInformation = xmlInformation;
        }

        public FormCompileException(string message, XmlSourceNodeInformation xmlInformation, XmlSourceNodeInformation propertyXmlInformation)
            : base(message)
        {
            _xmlInformation = xmlInformation;
            _propertyXmlInformation = propertyXmlInformation;
        }

        public FormCompileException(string message, CompileTreeNode treeNode, CompileTreeNode propertyTreeNode)
            : this(message, treeNode.XmlSourceNodeInformation, propertyTreeNode.XmlSourceNodeInformation)
        {
        }

        public FormCompileException(string message, XmlSourceNodeInformation xmlInformation, XmlSourceNodeInformation propertyXmlInformation, XmlSourceNodeInformation extraPropertyXmlInformation)
            : base(message)
        {
            _xmlInformation = xmlInformation;
            _propertyXmlInformation = propertyXmlInformation;
            _extraPropertyXmlInformation = extraPropertyXmlInformation;
        }

        public FormCompileException(string message, XmlSourceNodeInformation xmlInformation, Exception inner)
            : base(message, inner)
        {
            _xmlInformation = xmlInformation;
        }

        public string TagName
        {
            get { return _xmlInformation.TagName; }
        }

        public string XPath
        {
            get { return _xmlInformation.XPath; }
        }

        public override string ToString()
        {
            string result = Message;

            if (null != _xmlInformation)
            {
                result = string.Format("{0}\n\nTag = {1}\nXPath = {2}", result, _xmlInformation.TagName, _xmlInformation.XPath);
            }

            if (null != _propertyXmlInformation)
            {
                result = string.Format("{0}\n\nProperty:\nTag = {1}\nXPath = {2}", result, _propertyXmlInformation.TagName, _propertyXmlInformation.XPath);
            }

            if (null != _extraPropertyXmlInformation)
            {
                result = string.Format("{0}\n\nProperty:\nTag = {1}\nXPath = {2}", result, _extraPropertyXmlInformation.TagName, _extraPropertyXmlInformation.XPath);
            }

            return result;
        }
    }

}
