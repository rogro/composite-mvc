/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using Composite.Data.Validation.ClientValidationRules;
using System.Collections.Generic;


namespace Composite.C1Console.Forms
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public interface IUiControl
    {
        /// <summary>
        /// UiControls are automatically assigned a unique and permanent UiControlID by the Composite.C1Console.Forms Manager. 
        /// Use the UiControlID to uniquely identify a UiControl instance between build ups.
        /// </summary>
        string UiControlID { get; set; }

        /// <summary>
        /// UiControls are automatically assigned the name of the channel they are executed within. 
        /// You can use the channel name when compiling embedded forms.
        /// </summary>
        IFormChannelIdentifier UiControlChannel { get; set; }

        /// <summary>
        /// UiControl labels are used by most containers to label the control.
        /// </summary>
        string Label { get; set; }

        /// <summary>
        /// UiControl help strings are used by most containers to add context sensitive help to the control.
        /// </summary>
        string Help { get; set; }

        /// <summary>
        /// UiControls can use these validation rules to perform client side validaion
        /// </summary>
        List<ClientValidationRule> ClientValidationRules { get; set; }

        /// <summary>
        /// When invoked, UiControl Properies that expose bindable data must be updated to reflect user induced state.
        /// I.e. the Text property of a TextBox UiControl should be assigned the Text property of it's inner control, so
        /// the Composite.C1Console.Forms Manager can access the users input. 
        /// </summary>
        void BindStateToControlProperties();

        /// <summary>
        /// The "path to the source" that is bound to this control - i.e. the value written in the "source" property in the form markup.
        /// </summary>
        List<string> SourceBindingPaths { get; set; }
    }
}
