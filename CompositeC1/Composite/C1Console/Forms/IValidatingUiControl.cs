/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Composite.C1Console.Forms
{
    /// <summary>
    /// UiControls can be in a situation where user input can not be converted to the type the control is binding to. 
    /// Rather than throwing an exception, the control can declare that is is in an invalid state and give a message. 
    /// This allow the core to abort saving etc. 
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    public interface IValidatingUiControl : IUiControl
    {
        /// <summary>
        /// This field declare is your control is in a valid state. 
        /// If this returns true, actions like Save and Preview will be canceled.
        /// </summary>
        bool IsValid { get; }

        /// <summary>
        /// When in an invalid state this field is expected to describe the reason for this invalid state. The text is intended
        /// for the end-user and should explain what they did wrong, like "Date format is invalid, use 'yyyy-mm-dd'".
        /// </summary>
        string ValidationError { get; }
    }
}
