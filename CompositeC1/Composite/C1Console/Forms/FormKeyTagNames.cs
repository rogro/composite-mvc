/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using Composite.C1Console.Forms.Foundation.FormTreeCompiler;


namespace Composite.C1Console.Forms
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
	public static class FormKeyTagNames
	{
        /// <exclude />
        public static readonly string DefaultPropertyName = CompilerGlobals.DefaultPropertyName;

        /// <exclude />
        public static readonly string FormDefinition = CompilerGlobals.FormDefinition_TagName;

        /// <exclude />
        public static readonly string Layout = CompilerGlobals.Layout_TagName;

        /// <exclude />
        public static readonly string Bindings = CompilerGlobals.Bindings_TagName;

        /// <exclude />
        public static readonly string Binding = CompilerGlobals.Binding_TagName;

        /// <exclude />
        public static readonly string Bind = CompilerGlobals.Bind_TagName;

        /// <exclude />
        public static readonly string Read = CompilerGlobals.Read_TagName;

        /// <exclude />
        public static readonly string IfCondition = CompilerGlobals.IfCondition_TagName;

        /// <exclude />
        public static readonly string IfWhenTrue = CompilerGlobals.IfWhenTrue_TagName;

        /// <exclude />
        public static readonly string IfWhenFalse = CompilerGlobals.IfWhenFalse_TagName;
	}
}
