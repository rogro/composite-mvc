/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Xml.Linq;
using Composite.Core.Xml;


namespace Composite.C1Console.Trees
{
    /// <summary>    
    /// Defines the way an element is shown depending on presense of the child elements
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public enum LeafDisplayMode
    {
        /// <summary>
        /// Only shows the elements that has child elements; hides all the elements without child elements.
        /// </summary>
        Compact = 0,

        /// <summary>
        /// Element is always shown, as well as the the "+" sign even when element doesn't have any child items. Default value, has the best performance
        /// </summary>
        Lazy = 1,

        /// <summary>
        /// Element is always shown, the "+" sign is shown only if there're child elements.
        /// </summary>
        Auto = 2
    }



    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public static class LeafDisplayModeHelper
    {
        /// <exclude />
        public static LeafDisplayMode ParseDisplayMode(XAttribute attribute, Tree tree)
        {
            if (attribute != null)
            {
                LeafDisplayMode parsedValue;

                if (Enum.TryParse(attribute.Value, out parsedValue))
                {
                    return parsedValue;
                }

                tree.AddValidationError(attribute.GetXPath(), "TreeValidationError.Common.WrongAttributeValue", attribute.Value);
            }

            return LeafDisplayMode.Lazy;
        }
    }
}