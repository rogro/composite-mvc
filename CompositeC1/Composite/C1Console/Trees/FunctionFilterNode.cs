/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using System.Linq.Expressions;
using System.Xml.Linq;
using Composite.Data;
using Composite.Functions;
using Composite.Core;
using Composite.Core.ResourceSystem;


namespace Composite.C1Console.Trees
{
    internal sealed class FunctionFilterNode : FilterNode
    {
        public XElement FunctionMarkup { get; set; } // Required


        private AttributeDynamicValuesHelper FunctionMarkupDynamicValuesHelper { get; set; }


        public override Expression CreateDownwardsFilterExpression(ParameterExpression parameterExpression, TreeNodeDynamicContext dynamicContext)
        {
            var replaceContext = new DynamicValuesHelperReplaceContext(dynamicContext.CurrentEntityToken, dynamicContext.Piggybag);

            XElement markup = this.FunctionMarkupDynamicValuesHelper.ReplaceValues(replaceContext);

            BaseRuntimeTreeNode baseRuntimeTreeNode = FunctionTreeBuilder.Build(markup);
            LambdaExpression expression = GetLambdaExpression(baseRuntimeTreeNode);

            if (expression.Parameters.Count != 1)
            {
                throw new InvalidOperationException("Only 1 parameter lamdas supported when calling function: " + markup);
            }


            ParameterChangerExpressionVisitor expressionVisitor = new ParameterChangerExpressionVisitor(expression.Parameters.Single(), parameterExpression);

            Expression resultExpression = expressionVisitor.Visit(expression.Body);

            return resultExpression;
        }        



        public override Expression CreateUpwardsFilterExpression(ParameterExpression parameterExpression, TreeNodeDynamicContext dynamicContext)
        {
            DataEntityToken currentEntityToken = dynamicContext.CurrentEntityToken as DataEntityToken;
            IData filteredDataItem = null;

            Func<IData, bool> upwardsFilter = dataItem =>
            {
                var ancestorEntityToken = dataItem.GetDataEntityToken();

                var replaceContext = new DynamicValuesHelperReplaceContext
                {
                    CurrentDataItem = dataItem,
                    CurrentEntityToken = ancestorEntityToken,
                    PiggybagDataFinder = new PiggybagDataFinder(dynamicContext.Piggybag, ancestorEntityToken)
                };

                XElement markup = this.FunctionMarkupDynamicValuesHelper.ReplaceValues(replaceContext);

                BaseRuntimeTreeNode baseRuntimeTreeNode = FunctionTreeBuilder.Build(markup);
                LambdaExpression expression = GetLambdaExpression(baseRuntimeTreeNode);

                if (expression.Parameters.Count != 1)
                {
                    throw new InvalidOperationException("Only 1 parameter lamdas supported when calling function: " + markup);
                }

                Delegate compiledExpression = expression.Compile();

                if (filteredDataItem == null)
                {
                    filteredDataItem = currentEntityToken.Data;
                }

                return (bool)compiledExpression.DynamicInvoke(filteredDataItem);
            };

            
            return upwardsFilter.Target != null 
                ? Expression.Call(Expression.Constant(upwardsFilter.Target), upwardsFilter.Method, parameterExpression)
                : Expression.Call(upwardsFilter.Method, parameterExpression);

        }


        internal override void Initialize()
        {
            try
            {
                FunctionTreeBuilder.Build(this.FunctionMarkup);
            }
            catch 
            {
                AddValidationError("TreeValidationError.FunctionFilter.WrongFunctionMarkup");
                return;
            }
                       

            this.FunctionMarkupDynamicValuesHelper = new AttributeDynamicValuesHelper(this.FunctionMarkup);
            this.FunctionMarkupDynamicValuesHelper.Initialize(this.OwnerNode);
        }



        private LambdaExpression GetLambdaExpression(BaseRuntimeTreeNode baseRuntimeTreeNode)
        {
            LambdaExpression expression = (LambdaExpression)baseRuntimeTreeNode.GetValue();

            if (expression == null)
            {
                string message = string.Format(StringResourceSystemFacade.GetString("Composite.C1Console.Trees", "TreeValidationError.FunctionFilter.WrongReturnValue"), string.Format("Expression<Func<{0}, bool>>", this.OwnerNode.InterfaceType));

                Log.LogError("TreeFacade", message);
                Log.LogError("TreeFacade", "In tree " + this.OwnerNode.Tree.TreeId + " in function " + this.XPath);

                throw new InvalidOperationException(message);
            }


            if (expression.ReturnType != typeof(bool))
            {
                string message = string.Format(StringResourceSystemFacade.GetString("Composite.C1Console.Trees", "TreeValidationError.FunctionFilter.WrongFunctionReturnType"), expression.ReturnType, typeof(bool));
                
                Log.LogError("TreeFacade", message);
                Log.LogError("TreeFacade", "In tree " + this.OwnerNode.Tree.TreeId + " in function " + this.XPath);
                
                throw new InvalidOperationException(message);
            }

            if (expression.Parameters.Count() != 1)
            {
                string message = string.Format(StringResourceSystemFacade.GetString("Composite.C1Console.Trees", "TreeValidationError.FunctionFilter.WrongFunctionParameterCount"), expression.Parameters.Count());
                
                Log.LogError("TreeFacade", message);
                Log.LogError("TreeFacade", "In tree " + this.OwnerNode.Tree.TreeId + " in function " + this.XPath);

                throw new InvalidOperationException(message);
            }

            ParameterExpression parameterExpression = expression.Parameters.Single();
            if (this.OwnerNode.InterfaceType.IsAssignableFrom(parameterExpression.Type) == false)
            {
                string message = string.Format(StringResourceSystemFacade.GetString("Composite.C1Console.Trees", "TreeValidationError.FunctionFilter.WrongFunctionParameterType"), parameterExpression.Type, this.OwnerNode.InterfaceType);
                
                Log.LogError("TreeFacade", message);
                Log.LogError("TreeFacade", "In tree " + this.OwnerNode.Tree.TreeId + " in function " + this.XPath);

                throw new InvalidOperationException(message);
            }

            return expression;
        }



        private sealed class ParameterChangerExpressionVisitor : ExpressionVisitor
        {
            private readonly ParameterExpression _parameterExpressionToChange;
            private readonly ParameterExpression _newParameterExpression;


            public ParameterChangerExpressionVisitor(ParameterExpression parameterExpressionToChange, ParameterExpression newParameterExpression)
            {
                _parameterExpressionToChange = parameterExpressionToChange;
                _newParameterExpression = newParameterExpression;
            }


            protected override Expression VisitParameter(ParameterExpression node)
            {
                if (node == _parameterExpressionToChange)
                    return _newParameterExpression;
                    
                return node;
            }
        }
    }
}
