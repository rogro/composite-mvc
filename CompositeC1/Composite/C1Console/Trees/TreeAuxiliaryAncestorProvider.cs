/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Composite.C1Console.Security;
using Composite.C1Console.Trees.Foundation;
using Composite.Core;
using Composite.Core.Extensions;
using Composite.Data;


namespace Composite.C1Console.Trees
{
    internal sealed class TreeAuxiliaryAncestorProvider : IAuxiliarySecurityAncestorProvider
    {
        public Dictionary<EntityToken, IEnumerable<EntityToken>> GetParents(IEnumerable<EntityToken> entityTokens)
        {
            Dictionary<EntityToken, IEnumerable<EntityToken>> result = new Dictionary<EntityToken, IEnumerable<EntityToken>>();

            foreach (EntityToken entityToken in entityTokens)
            {
                TreeNodeDynamicContext dynamicContext = new TreeNodeDynamicContext(TreeNodeDynamicContextDirection.Up)
                {
                    CurrentEntityToken = entityToken
                };


                if (entityToken is TreeSimpleElementEntityToken
                    || entityToken is TreeFunctionElementGeneratorEntityToken)
                {
                    var parentEntityTokenSource = entityToken as IEntityTokenContainingParentEntityToken;

                    try
                    {
                        result.Add(entityToken, new[] { parentEntityTokenSource.GetParentEntityToken() });
                    }
                    catch (Exception ex)
                    {
                        string treeId;

                        if (entityToken is TreeSimpleElementEntityToken)
                        {
                            treeId = (entityToken as TreeSimpleElementEntityToken).TreeNodeId;
                        }
                        else if (entityToken is TreeFunctionElementGeneratorEntityToken)
                        {
                            treeId = (entityToken as TreeFunctionElementGeneratorEntityToken).TreeNodeId;
                        }
                        else
                        {
                            throw new InvalidOperationException("This code should not be reachable.");
                        }

                        Log.LogError("TreeFacade", "The tree '{0}' failed to return parent entity tokens and are ignored", treeId);
                        Log.LogError("TreeFacade", ex);
                    }
                }
                else if (entityToken is TreeDataFieldGroupingElementEntityToken)
                {
                    TreeDataFieldGroupingElementEntityToken treeDataFieldGroupingElementEntityToken = entityToken as TreeDataFieldGroupingElementEntityToken;
                    string treeId = entityToken.Source;

                    try
                    {
                        Tree tree = TreeFacade.GetTree(treeId);

                        string treeNodeId = treeDataFieldGroupingElementEntityToken.TreeNodeId;
                        TreeNode treeNode = tree.GetTreeNode(treeNodeId);

                        dynamicContext.FieldGroupingValues = treeDataFieldGroupingElementEntityToken.GroupingValues;
                        dynamicContext.FieldFolderRangeValues = treeDataFieldGroupingElementEntityToken.FolderRangeValues;
                        dynamicContext.CurrentTreeNode = treeNode;

                        result.Add(entityToken, treeNode.ParentNode.GetEntityTokens(entityToken, dynamicContext));
                    }
                    catch (Exception ex)
                    {
                        Log.LogError("TreeFacade", string.Format("The tree '{0}' failed to return parent entity tokens and are ignored", treeId));
                        Log.LogError("TreeFacade", ex);
                    }
                }
                else if (entityToken is DataEntityToken)
                {
                    DataEntityToken dataEntityToken = entityToken as DataEntityToken;

                    Type interfaceType = dataEntityToken.InterfaceType;

                    foreach (Tree tree in TreeFacade.AllTrees)
                    {
                        List<TreeNode> treeNodes;
                        if (!tree.BuildProcessContext.DataInteraceToTreeNodes.TryGetValue(interfaceType, out treeNodes)) continue;
                        
                        IEnumerable<EntityToken> concatList = null;                        

                        foreach (TreeNode treeNode in treeNodes)
                        {
                            try
                            {                              
                                dynamicContext.CurrentTreeNode = treeNode;

                                concatList = concatList.ConcatOrDefault(treeNode.ParentNode.GetEntityTokens(entityToken, dynamicContext));
                            }
                            catch (Exception ex)
                            {
                                Log.LogError("TreeFacade", "The tree '{0}' failed to return parent entity tokens and are ignored", treeNode.Tree.TreeId);
                                Log.LogError("TreeFacade", ex);
                            }
                        }

                        if (concatList != null)
                        {
                            IEnumerable<EntityToken> existingList;
                            if (result.TryGetValue(entityToken, out existingList))
                            {
                                result[entityToken] = existingList.Concat(concatList);
                            }
                            else
                            {
                                result.Add(entityToken, concatList);
                            }
                        }
                    }
                }
                else if (entityToken is TreePerspectiveEntityToken)
                {
                    result.Add(entityToken, new[] { TreeSharedRootsFacade.SharedRootFolders[entityToken.Id].AttachmentPoint.AttachingPoint.EntityToken });
                }
            }

            return result;
        }
    }
}
