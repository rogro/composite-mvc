/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Composite.C1Console.Actions;
using Composite.C1Console.Events;
using Composite.C1Console.Elements;
using Composite.C1Console.Security;
using Composite.Core.Serialization;



namespace Composite.C1Console.Trees
{
    internal class MessageBoxActionNode : ActionNode
    {
        public string Title { get; set; }                                   // Required
        public string Message { get; set; }                                 // Required
        public DialogType DialogType { get; set; }                          // Optional        

        // Cached values
        private DynamicValuesHelper TitleDynamicValuesHelper { get; set; }
        private DynamicValuesHelper MessageDynamicValuesHelper { get; set; }


        protected override void OnAddAction(Action<ElementAction> actionAdder, EntityToken entityToken, TreeNodeDynamicContext dynamicContext, DynamicValuesHelperReplaceContext dynamicValuesHelperReplaceContext)
        {
            ActionToken actionToken = new MessageBoxActionNodeActionToken(
                this.TitleDynamicValuesHelper.ReplaceValues(dynamicValuesHelperReplaceContext),
                this.MessageDynamicValuesHelper.ReplaceValues(dynamicValuesHelperReplaceContext),
                this.Serialize(), 
                this.PermissionTypes
                );

            ElementAction elementAction = new ElementAction(new ActionHandle(actionToken))
            {
                VisualData = this.CreateActionVisualizedData(dynamicValuesHelperReplaceContext)
            };

            elementAction.VisualData.ActionLocation = this.Location;

            actionAdder(elementAction);
        }



        protected override void OnInitialize()
        {
            this.TitleDynamicValuesHelper = new DynamicValuesHelper(this.Title);
            this.TitleDynamicValuesHelper.Initialize(this.OwnerNode);

            this.MessageDynamicValuesHelper = new DynamicValuesHelper(this.Message);
            this.MessageDynamicValuesHelper.Initialize(this.OwnerNode);
        }
    }



    internal sealed class MessageBoxActionNodeActionExecutor : IActionExecutor
    {
        public FlowToken Execute(EntityToken entityToken, ActionToken actionToken, FlowControllerServicesContainer flowControllerServicesContainer)
        {
            MessageBoxActionNodeActionToken messageBoxActionNodeActionToken = (MessageBoxActionNodeActionToken)actionToken;

            MessageBoxActionNode messageBoxActionNode = (MessageBoxActionNode)ActionNode.Deserialize(messageBoxActionNodeActionToken.SerializedActionNode);            

            IManagementConsoleMessageService managementConsoleMessageService = flowControllerServicesContainer.GetService<IManagementConsoleMessageService>();

            managementConsoleMessageService.ShowMessage(
                messageBoxActionNode.DialogType, 
                messageBoxActionNodeActionToken.Title, 
                messageBoxActionNodeActionToken.Message);

            return null;
        }
    }



    [ActionExecutor(typeof(MessageBoxActionNodeActionExecutor))]
    internal sealed class MessageBoxActionNodeActionToken : ActionToken
    {
        private List<PermissionType> _permissionTypes;


        public MessageBoxActionNodeActionToken(string title, string message, string serializedActionNode, List<PermissionType> permissionTypes)
        {
            this.Title = title;
            this.Message = message;
            _permissionTypes = permissionTypes;
            this.SerializedActionNode = serializedActionNode;
        }

        public string Title { get; private set; }
        public string Message { get; private set; }
        public string SerializedActionNode { get; private set; }        

        public override IEnumerable<PermissionType> PermissionTypes
        {
            get { return _permissionTypes; }
        }


        public override string Serialize()
        {
            StringBuilder sb = new StringBuilder();

            StringConversionServices.SerializeKeyValuePair(sb, "Title", this.Title);
            StringConversionServices.SerializeKeyValuePair(sb, "Message", this.Message);
            StringConversionServices.SerializeKeyValuePair(sb, "SerializedActionNode", this.SerializedActionNode);
            StringConversionServices.SerializeKeyValuePair(sb, "PermissionTypes", _permissionTypes.SerializePermissionTypes());            

            return sb.ToString();
        }


        public static ActionToken Deserialize(string serializedData)
        {
            Dictionary<string, string> dic = StringConversionServices.ParseKeyValueCollection(serializedData);

            return new MessageBoxActionNodeActionToken
            (
                StringConversionServices.DeserializeValueString(dic["Title"]),
                StringConversionServices.DeserializeValueString(dic["Message"]),
                StringConversionServices.DeserializeValueString(dic["SerializedActionNode"]),
                StringConversionServices.DeserializeValueString(dic["PermissionTypes"]).DesrializePermissionTypes().ToList()
            );
        }
    }
}
