/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;
using Composite.C1Console.Elements;
using Composite.Functions;
using Composite.Core.ResourceSystem;
using Composite.C1Console.Security;
using Composite.Core.Serialization;
using Composite.C1Console.Workflow;


namespace Composite.C1Console.Trees
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
	public sealed class ReportFunctionActionNode : ActionNode
	{
        /// <exclude />
        public XElement FunctionMarkup { get; set; }                            // Required
        
        /// <exclude />
        public string DocumentLabel { get; set; }                               // Optional, defaults to Label

        /// <exclude />
        public ResourceHandle DocumentIcon { get; set; }                        // Optional, defaults to Icon


        /// <exclude />
        public AttributeDynamicValuesHelper FunctionMarkupDynamicValuesHelper { get; private set; }

        /// <exclude />
        public DynamicValuesHelper DocumentLabelDynamicValueHelper { get; private set; }


        /// <exclude />
        protected override void OnAddAction(Action<ElementAction> actionAdder, EntityToken entityToken, TreeNodeDynamicContext dynamicContext, DynamicValuesHelperReplaceContext dynamicValuesHelperReplaceContext)
        {
            StringBuilder payload = new StringBuilder();
            StringConversionServices.SerializeKeyValuePair(payload, "TreeId", this.OwnerNode.Tree.TreeId);
            StringConversionServices.SerializeKeyValuePair(payload, "ActionId", this.Id);

            WorkflowActionToken actionToken = new WorkflowActionToken(
                WorkflowFacade.GetWorkflowType("Composite.C1Console.Trees.Workflows.ReportFunctionActionWorkflow"), 
                this.PermissionTypes)                
            {
                Payload = this.Serialize(),
                ExtraPayload = PiggybagSerializer.Serialize(dynamicContext.Piggybag.PreparePiggybag(dynamicContext.CurrentTreeNode, dynamicContext.CurrentEntityToken)),
                DoIgnoreEntityTokenLocking = true
            };
            

            actionAdder(new ElementAction(new ActionHandle(actionToken))
            {
                VisualData = CreateActionVisualizedData(dynamicValuesHelperReplaceContext)
            });
        }



        /// <exclude />
        protected override void OnInitialize()
        {
            try
            {
                FunctionTreeBuilder.Build(this.FunctionMarkup);
            }
            catch
            {
                AddValidationError("TreeValidationError.Common.WrongFunctionMarkup");
                return;
            }            

            this.FunctionMarkupDynamicValuesHelper = new AttributeDynamicValuesHelper(this.FunctionMarkup);
            this.FunctionMarkupDynamicValuesHelper.Initialize(this.OwnerNode);

            this.DocumentLabelDynamicValueHelper = new DynamicValuesHelper(this.DocumentLabel);
            this.DocumentLabelDynamicValueHelper.Initialize(this.OwnerNode);
        }



        /// <exclude />
        public override string ToString()
        {
            return string.Format("ReportFunctionActionNode, Label = {0}", this.Label);
        }
    }
}
