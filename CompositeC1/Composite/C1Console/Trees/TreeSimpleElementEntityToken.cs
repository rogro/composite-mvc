/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Diagnostics;
using Composite.C1Console.Security;
using Composite.C1Console.Security.SecurityAncestorProviders;


namespace Composite.C1Console.Trees
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    [SecurityAncestorProvider(typeof(NoAncestorSecurityAncestorProvider))]
    [DebuggerDisplay("Id = {Id}, TreeId = {Source}, ParentEntityToken = {Type}")]
    public sealed class TreeSimpleElementEntityToken : EntityToken, IEntityTokenContainingParentEntityToken
	{
        private EntityToken _parentEntityToken;
        private readonly string _treeNodeId;
        private readonly string _treeId;
        private readonly string _serializedParentEntityToken;

        /// <exclude />
        public TreeSimpleElementEntityToken(string treeNodeId, string treeId, string serializedParentEntityToken)
        {
            _treeNodeId = treeNodeId;
            _treeId = treeId;
            _serializedParentEntityToken = serializedParentEntityToken;
        }

        /// <exclude />
        public override string Type
        {
            get { return _serializedParentEntityToken; }
        }


        /// <exclude />
        public override string Source
        {
            get { return _treeId; }
        }


        /// <exclude />
        public override string Id
        {
            get { return _treeNodeId; }
        }

        /// <exclude />
        public string TreeNodeId
        {
            get
            {
                return this.Id;
            }
        }

        /// <exclude />
        public string SerializedParentEntityToken
        {
            get
            {
                return _serializedParentEntityToken;
            }
        }

        /// <exclude />
        public EntityToken ParentEntityToken
        {
            get
            {
                if (_parentEntityToken == null)
                {
                    _parentEntityToken = EntityTokenSerializer.Deserialize(_serializedParentEntityToken);
                }

                return _parentEntityToken;
            }
        }


        /// <exclude />
        public EntityToken GetParentEntityToken()
        {
            return this.ParentEntityToken;
        }


        /// <exclude />
        public override string Serialize()
        {
            return DoSerialize();
        }

        /// <exclude />
        public static EntityToken Deserialize(string serializedEntityToken)
        {
            string type, source, id;

            DoDeserialize(serializedEntityToken, out type, out source, out id);

            return new TreeSimpleElementEntityToken(id, source, type);
        }

        /// <exclude />
        public override void OnGetPrettyHtml(EntityTokenHtmlPrettyfier prettyfier)
        {
            EntityToken parentEntityToken = this.ParentEntityToken;

            prettyfier.OnWriteType = (token, helper) => helper.AddFullRow(new string[] { "<b>Type</b>", string.Format("<b>ParentEntityToken:</b><br /><b>Type:</b> {0}<br /><b>Source:</b> {1}<br /><b>Id:</b>{2}<br />", parentEntityToken.Type, parentEntityToken.Source, parentEntityToken.Id) });
        }


        /// <exclude />
        public override string OnGetTypePrettyHtml()
        {
            EntityToken parentEntityToken = this.ParentEntityToken;

            string type;
            if ((parentEntityToken is TreeSimpleElementEntityToken))
            {
                type = string.Format(@"<div style=""border: 1px solid blue;"">{0}</div>", parentEntityToken.OnGetTypePrettyHtml());
            }
            else
            {                
                type = parentEntityToken.Type;
            }

            return string.Format("<b>ParentEntityToken:</b><br /><b>Type:</b> {0}<br /><b>Source:</b> {1}<br /><b>Id:</b>{2}<br />", type, parentEntityToken.Source, parentEntityToken.Id);
        }        
    }
}
