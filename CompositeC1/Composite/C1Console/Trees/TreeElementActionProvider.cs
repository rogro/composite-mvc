/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;
using Composite.C1Console.Elements;
using Composite.C1Console.Elements.Plugins.ElementActionProvider;
using Composite.Core.ResourceSystem;
using Composite.C1Console.Security;
using Composite.C1Console.Workflow;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;


namespace Composite.C1Console.Trees
{
    [ConfigurationElementType(typeof(NonConfigurableElementActionProvider))]
    internal sealed class TreeElementActionProvider : IElementActionProvider
    {
        private static readonly ResourceHandle AddApplicationIcon = ResourceHandle.BuildIconFromDefaultProvider("tree-add-application");
        private static readonly ResourceHandle RemoveApplicationIcon = ResourceHandle.BuildIconFromDefaultProvider("tree-remove-application");

        private static readonly ActionGroup ApplicationsActionGroup = new ActionGroup("Applications", ActionGroupPriority.TargetedAppendMedium);

        private static readonly List<PermissionType> AddPermissionTypes = new List<PermissionType> { PermissionType.Administrate };
        private static readonly List<PermissionType> RemovePermissionTypes = new List<PermissionType> { PermissionType.Administrate };


        public IEnumerable<ElementAction> GetActions(EntityToken entityToken)
        {
            if (TreeFacade.HasPossibleAttachmentPoints(entityToken))
            {
                yield return new ElementAction(new ActionHandle(new WorkflowActionToken(WorkflowFacade.GetWorkflowType("Composite.C1Console.Trees.Workflows.AddApplicationWorkflow"), AddPermissionTypes) { DoIgnoreEntityTokenLocking = true }))
                {
                    VisualData = new ActionVisualizedData
                    {
                        Label = StringResourceSystemFacade.GetString("Composite.C1Console.Trees", "AddApplicationWorkflow.AddApplication.Label"),
                        ToolTip = StringResourceSystemFacade.GetString("Composite.C1Console.Trees", "AddApplicationWorkflow.AddApplication.ToolTip"),
                        Icon = AddApplicationIcon,
                        Disabled = false,
                        ActionLocation = new ActionLocation
                        {
                            ActionType = ActionType.Other,
                            IsInFolder = false,
                            IsInToolbar = false,
                            ActionGroup = ApplicationsActionGroup
                        }
                    }
                };


                yield return new ElementAction(new ActionHandle(new WorkflowActionToken(WorkflowFacade.GetWorkflowType("Composite.C1Console.Trees.Workflows.RemoveApplicationWorkflow"), RemovePermissionTypes) { DoIgnoreEntityTokenLocking = true }))
                {
                    VisualData = new ActionVisualizedData
                    {
                        Label = StringResourceSystemFacade.GetString("Composite.C1Console.Trees", "RemoveApplicationWorkflow.RemoveApplication.Label"),
                        ToolTip = StringResourceSystemFacade.GetString("Composite.C1Console.Trees", "RemoveApplicationWorkflow.RemoveApplication.ToolTip"),
                        Icon = RemoveApplicationIcon,
                        Disabled = false,
                        ActionLocation = new ActionLocation
                        {
                            ActionType = ActionType.Other,
                            IsInFolder = false,
                            IsInToolbar = false,
                            ActionGroup = ApplicationsActionGroup
                        }
                    }
                };
            }


            List<ElementAction> elementActions = new List<ElementAction>();
            foreach (Tree tree in TreeFacade.GetTreesByEntityToken(entityToken))
            {
                TreeNodeDynamicContext dynamicContext = new TreeNodeDynamicContext(TreeNodeDynamicContextDirection.Down)
                {
                    CurrentTreeNode = tree.RootTreeNode,
                    CurrentEntityToken = entityToken,
                    Piggybag = new Dictionary<string, string>()
                };

                foreach (ActionNode actionNode in tree.RootTreeNode.ActionNodes)
                {
                    actionNode.AddAction(f => elementActions.Add(f), entityToken, dynamicContext);
                }
            }


            foreach (ElementAction elementAction in elementActions)
            {
                yield return elementAction;
            }
        }
    }
}
