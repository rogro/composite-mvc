/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using Composite.C1Console.Security;
using System.Reflection;


namespace Composite.C1Console.Trees
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public enum TreeNodeDynamicContextDirection
    {
        /// <exclude />
        Down, // Creating elements

        /// <exclude />
        Up // Getting ancestor entity tokens
    }



    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    [DebuggerDisplay("TreeNodeDynamicContext. Direction: {Direction}")]
    public sealed class TreeNodeDynamicContext
    {
        private Dictionary<string, object> _fieldGroupingValues;
        private Dictionary<string, int> _fieldFolderRangeValues;


        /// <exclude />
        public TreeNodeDynamicContext(TreeNodeDynamicContextDirection treeNodeDynamicContextDirection)
        {
            this.Direction = treeNodeDynamicContextDirection;
            this.CustomData = new Dictionary<string, object>();
        }


        /// <exclude />
        public string ElementProviderName { get; set; }


        /// <exclude />
        public TreeNodeDynamicContextDirection Direction { get; internal set; }


        /// <exclude />
        public Dictionary<string, object> FieldGroupingValues
        {
            get
            {
                if (_fieldGroupingValues == null)
                {
                    _fieldGroupingValues = new Dictionary<string, object>();
                }

                return _fieldGroupingValues;
            }
            set
            {
                _fieldGroupingValues = value;
            }
        }


        /// <exclude />
        public Dictionary<string, int> FieldFolderRangeValues
        {
            get
            {
                if (_fieldFolderRangeValues == null)
                {
                    _fieldFolderRangeValues = new Dictionary<string, int>();
                }

                return _fieldFolderRangeValues;
            }
            set
            {
                _fieldFolderRangeValues = value;
            }
        }


        /// <exclude />
        public Dictionary<string, string> Piggybag { get; set; }


        /// <exclude />
        public EntityToken CurrentEntityToken { get; set; }

        /// <exclude />
        public TreeNode CurrentTreeNode { get; set; }


        internal Dictionary<string, object> CustomData { get; set; }


        internal bool IsRoot { get; set; }
    }    
}
