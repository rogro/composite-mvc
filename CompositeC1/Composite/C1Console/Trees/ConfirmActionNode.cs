/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Xml.Linq;
using Composite.C1Console.Elements;
using Composite.C1Console.Security;
using Composite.C1Console.Workflow;
using Composite.Functions;


namespace Composite.C1Console.Trees
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class ConfirmActionNode : ActionNode
    {
        /// <exclude />
        public string ConfirmTitle { get; internal set; }                           // Requried

        /// <exclude />
        public string ConfirmMessage { get; internal set; }                         // Requried

        /// <exclude />
        public XElement FunctionMarkup { get; internal set; }                       // Requried

        /// <exclude />
        public bool RefreshTree { get; internal set; }                              // Optional


        // Cached values
        /// <exclude />
        public DynamicValuesHelper ConfirmTitleDynamicValuesHelper { get; set; }

        /// <exclude />
        public DynamicValuesHelper ConfirmMessageDynamicValuesHelper { get; set; }

        /// <exclude />
        public AttributeDynamicValuesHelper FunctionMarkupDynamicValuesHelper { get; private set; }


        /// <exclude />
        protected override void OnAddAction(Action<ElementAction> actionAdder, EntityToken entityToken, TreeNodeDynamicContext dynamicContext, DynamicValuesHelperReplaceContext dynamicValuesHelperReplaceContext)
        {
            WorkflowActionToken actionToken = new WorkflowActionToken(
                WorkflowFacade.GetWorkflowType("Composite.C1Console.Trees.Workflows.ConfirmActionWorkflow"),
                this.PermissionTypes)
            {
                Payload = this.Serialize(),
                ExtraPayload = PiggybagSerializer.Serialize(dynamicContext.Piggybag.PreparePiggybag(this.OwnerNode, dynamicContext.CurrentEntityToken)),
                DoIgnoreEntityTokenLocking = true
            };


            actionAdder(new ElementAction(new ActionHandle(actionToken))
            {
                VisualData = CreateActionVisualizedData(dynamicValuesHelperReplaceContext)
            });
        }


        /// <exclude />
        protected override void OnInitialize()
        {
            try
            {
                FunctionTreeBuilder.Build(this.FunctionMarkup);
            }
            catch
            {
                AddValidationError("TreeValidationError.Common.WrongFunctionMarkup");
                return;
            }


            this.FunctionMarkupDynamicValuesHelper = new AttributeDynamicValuesHelper(this.FunctionMarkup);
            this.FunctionMarkupDynamicValuesHelper.Initialize(this.OwnerNode);

            this.ConfirmTitleDynamicValuesHelper = new DynamicValuesHelper(this.ConfirmTitle);
            this.ConfirmTitleDynamicValuesHelper.Initialize(this.OwnerNode);

            this.ConfirmMessageDynamicValuesHelper = new DynamicValuesHelper(this.ConfirmMessage);
            this.ConfirmMessageDynamicValuesHelper.Initialize(this.OwnerNode);
        }
    }
}
