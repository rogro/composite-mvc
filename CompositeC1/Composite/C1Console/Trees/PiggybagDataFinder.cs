/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Composite.C1Console.Security;
using Composite.Data;


namespace Composite.C1Console.Trees
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class PiggybagDataFinder
    {
        private List<EntityToken> _parentEntityTokens;

        private readonly Dictionary<string, string> _piggybag;
        private readonly EntityToken _currentEntityToken;



        /// <exclude />
        public PiggybagDataFinder(Dictionary<string, string> piggybag, EntityToken currentEntityToken)
        {
            _piggybag = piggybag;
            _currentEntityToken = currentEntityToken;
        }


         
        /// <exclude />
        public Dictionary<string, string> Piggybag
        {
            get
            {
                return _piggybag;
            }
        }



        /// <exclude />
        public IData GetData(Type interfaceType, IData currentData = null)
        {
            if ((currentData != null) && (currentData.DataSourceId.InterfaceType == interfaceType)) return currentData;

            DataEntityToken dataEntityToken = GetDataEntityToken(interfaceType);

            return dataEntityToken.Data;
        }



        /// <exclude />
        public IData TryGetData(Type interfaceType, IData currentData = null)
        {
            if ((currentData != null) && (currentData.DataSourceId.InterfaceType == interfaceType)) return currentData;

            DataEntityToken dataEntityToken = TryGetDataEntityToken(interfaceType);            
            if (dataEntityToken == null) return null;

            return dataEntityToken.Data;
        }




        private DataEntityToken GetDataEntityToken(Type interfaceType)
        {
            DataEntityToken dataEntityToken = TryGetDataEntityToken(interfaceType);

            if ((dataEntityToken == null) || (dataEntityToken.InterfaceType != interfaceType)) throw new InvalidOperationException("Could not find data entity token that match the given interface type");            

            return dataEntityToken;
        }



        private DataEntityToken TryGetDataEntityToken(Type interfaceType)
        {
            DataEntityToken dataEntityToken = _currentEntityToken as DataEntityToken;
            if ((dataEntityToken == null) || (dataEntityToken.InterfaceType != interfaceType))
            {
                dataEntityToken = this.ParentEntityTokens.FindDataEntityToken(interfaceType);
            }            

            return dataEntityToken;
        }



        private IEnumerable<EntityToken> ParentEntityTokens
        {
            get
            {
                if (_parentEntityTokens == null)
                {
                    _parentEntityTokens = _piggybag.GetParentEntityTokens().ToList();
                }

                return _parentEntityTokens;
            }
        }
    }
}
