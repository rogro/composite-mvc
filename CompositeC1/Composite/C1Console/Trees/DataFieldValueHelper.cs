/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using Composite.Data;
using Composite.Core.Types;
using Composite.C1Console.Trees.Foundation.AttachmentPoints;


namespace Composite.C1Console.Trees
{
    internal sealed class DataFieldValueHelper
    {
        private const string PreFix = "${C1:Data:";
        private static Regex TemplateRegex = new Regex(@"\$\{(?<string>[^\}]+)}", RegexOptions.Compiled);

        private List<DataFieldValueHelperEntry> _entries = new List<DataFieldValueHelperEntry>();

        public string Template { get; private set; }



        public DataFieldValueHelper(string template)
        {
            this.Template = template;
        }



        public static bool ContainsDataField(string value)
        {
            return value.Contains(PreFix);
        }



        public IEnumerable<Type> InterfaceTypes
        {
            get { return _entries.Select(e => e.InterfaceType);}
        }



        public string ReplaceValues(string currentValue, PiggybagDataFinder piggybagDataFinder, IData currentDataItem = null, bool useUrlEncode = false)
        {
            string result = currentValue;

            foreach (DataFieldValueHelperEntry entry in _entries)
            {
                IData data = piggybagDataFinder.GetData(entry.InterfaceType, currentDataItem);

                object value;
                if (entry.IsReference == false)
                {
                    value = entry.PropertyInfo.GetValue(data, null);

                    if (value == null)
                    {
                        value = "(NULL)";
                    }
                    else if (entry.PropertyInfo.PropertyType == typeof(DateTime))
                    {
                        value = ((DateTime)value).ToString("yyyy MM dd");
                    }
                }
                else
                {
                    IData referencedData = data.GetReferenced(entry.PropertyInfo.Name);

                    value = referencedData.GetLabel();
                }

                string stringValue = value.ToString();

                if (useUrlEncode)
                {
                    stringValue = HttpUtility.UrlEncode(stringValue);
                }

                result = result.Replace(entry.Match, stringValue);
            }

            return result;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="ownerTreeNode">This is only used to add validation errors</param>
        public void Initialize(TreeNode ownerTreeNode)
        {
            foreach (Match match in TemplateRegex.Matches(this.Template))
            {
                if (match.Value.StartsWith(PreFix) == false) continue;

                string value = match.Groups["string"].Value;
                string[] values = value.Split(':');
                if (values.Length != 4)
                {
                    ownerTreeNode.AddValidationError("TreeValidationError.DataFieldValueHelper.WrongFormat", match.Value, string.Format(@"{0}[InterfaceType]:[FieldName]}}", PreFix));
                    return;
                }


                string typeName = values[2];
                Type interfaceType = TypeManager.TryGetType(typeName);
                if (interfaceType == null)
                {
                    ownerTreeNode.AddValidationError("TreeValidationError.Common.UnkownInterfaceType", typeName);
                    return;
                }

                if (typeof(IData).IsAssignableFrom(interfaceType) == false)
                {
                    ownerTreeNode.AddValidationError("TreeValidationError.Common.NotImplementingIData", interfaceType, typeof(IData));
                    return;
                }


                string fieldName = values[3];
                PropertyInfo propertyInfo = interfaceType.GetPropertiesRecursively(f => f.Name == fieldName).SingleOrDefault();
                if (propertyInfo == null)
                {
                    ownerTreeNode.AddValidationError("TreeValidationError.Common.MissingProperty", interfaceType, fieldName);
                    return;
                }

                bool possibleAttachmentPointsExist = ownerTreeNode.Tree.PossibleAttachmentPoints.OfType<IDataItemAttachmentPoint>().Any(f => f.InterfaceType == interfaceType);
                bool attachmentPointsExist = ownerTreeNode.Tree.AttachmentPoints.OfType<IDataItemAttachmentPoint>().Any(f => f.InterfaceType == interfaceType);
                if (!possibleAttachmentPointsExist 
                    && !attachmentPointsExist 
                    && !ownerTreeNode.SelfAndParentsHasInterface(interfaceType))
                {
                    ownerTreeNode.AddValidationError("TreeValidationError.DataFieldValueHelper.InterfaceNotInParentTree", interfaceType);
                    return;
                }

                bool isReferencingProperty = DataReferenceFacade.GetForeignKeyProperties(interfaceType).Any(f => f.SourcePropertyInfo.Equals(propertyInfo));

                DataFieldValueHelperEntry entry = new DataFieldValueHelperEntry(
                    match.Value,
                    interfaceType,
                    propertyInfo,
                    isReferencingProperty
                );

                if (_entries.Contains(entry) == false)
                {
                    _entries.Add(entry);
                }
            }
        }




        private sealed class DataFieldValueHelperEntry
        {
            public DataFieldValueHelperEntry(string match, Type interfaceType, PropertyInfo propertyInfo, bool isReference)
            {
                this.Match = match;
                this.InterfaceType = interfaceType;
                this.PropertyInfo = propertyInfo;
                this.IsReference = isReference;
            }


            public string Match { get; private set; }
            public Type InterfaceType { get; private set; }
            public PropertyInfo PropertyInfo { get; private set; }
            public bool IsReference { get; private set; }


            public override bool Equals(object obj)
            {
                return Equals(obj as DataFieldValueHelperEntry);
            }


            public bool Equals(DataFieldValueHelperEntry entry)
            {
                return entry != null && object.Equals(this.Match, entry.Match);
            }


            public override int GetHashCode()
            {
                return this.Match.GetHashCode();
            }
        }
    }
}
