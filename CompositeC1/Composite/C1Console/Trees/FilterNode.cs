/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Linq.Expressions;
using System.Collections.Generic;


namespace Composite.C1Console.Trees
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public abstract class FilterNode
    {
        /// <exclude />
        public string XPath { get; internal set; }

        /// <exclude />
        public int Id { get; internal set; }

        /// <exclude />
        public DataElementsTreeNode OwnerNode { get; internal set; }


        /// <summary>
        /// This is used when finding data to create elements from (elements going down)
        /// </summary>
        /// <param name="parameterExpression"></param>
        /// <param name="dynamicContext"></param>
        /// <returns></returns>
        public virtual Expression CreateDownwardsFilterExpression(ParameterExpression parameterExpression, TreeNodeDynamicContext dynamicContext) { return null; }



        /// <summary>
        /// This is used when finding data to create entity tokens (security going up)
        /// </summary>
        /// <param name="parameterExpression"></param>
        /// <param name="dynamicContext"></param>
        /// <returns></returns>
        public virtual Expression CreateUpwardsFilterExpression(ParameterExpression parameterExpression, TreeNodeDynamicContext dynamicContext) { return null; }



        /// <summary>
        /// Use this method to do initializing and validation
        /// </summary>
        internal virtual void Initialize() { }



        /// <exclude />
        public void SetOwnerNode(TreeNode treeNode)
        {
            this.OwnerNode = (DataElementsTreeNode)treeNode;
        }



        /// <exclude />
        protected void AddValidationError(ValidationError validationError)
        {
            this.OwnerNode.Tree.BuildResult.AddValidationError(validationError);
        }



        /// <exclude />
        protected void AddValidationError(string stringName, params object[] args)
        {
            this.OwnerNode.Tree.BuildResult.AddValidationError(ValidationError.Create(this.XPath, stringName, args));
        }
    }
}
