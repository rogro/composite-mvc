/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;
using System.Xml.Linq;


namespace Composite.C1Console.Trees
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class AttributeDynamicValuesHelper
    {
        private readonly Dictionary<string, DynamicValuesHelper> _dynamicValuesHelpers = new Dictionary<string, DynamicValuesHelper>();

        /// <exclude />
        public XElement Element { get; private set; }


        /// <exclude />
        public AttributeDynamicValuesHelper(XElement element)
        {
            this.Element = element;
        }



        /// <exclude />
        public XElement ReplaceValues(DynamicValuesHelperReplaceContext context)
        {
            XElement result = new XElement(this.Element);

            foreach (XAttribute attribute in GetAttributes(result))
            {
                DynamicValuesHelper dynamicValuesHelper = _dynamicValuesHelpers[GetKeyValue(attribute)];

                string newValue = dynamicValuesHelper.ReplaceValues(context);

                attribute.SetValue(newValue);
            }

            return result;
        }



        /// <exclude />
        public void Initialize(TreeNode ownerTreeNode)
        {
            foreach (XAttribute attribute in GetAttributes(this.Element))
            {
                string key = GetKeyValue(attribute);
                if (_dynamicValuesHelpers.ContainsKey(key)) continue;

                DynamicValuesHelper dynamicValuesHelper = new DynamicValuesHelper(attribute.Value);
                dynamicValuesHelper.Initialize(ownerTreeNode);

                _dynamicValuesHelpers.Add(key, dynamicValuesHelper);
            }
        }



        private static IEnumerable<XAttribute> GetAttributes(XElement element)
        {
            return element.DescendantsAndSelf().Attributes();
        }



        private static string GetKeyValue(XAttribute attribute)
        {
            return attribute.Name + "·" + attribute.Value;
        }
    }
}
