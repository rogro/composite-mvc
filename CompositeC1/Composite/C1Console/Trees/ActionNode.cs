/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Text;
using Composite.Data;
using Composite.C1Console.Elements;
using Composite.Core.ResourceSystem;
using Composite.C1Console.Security;
using Composite.Core.Serialization;


namespace Composite.C1Console.Trees
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
	public abstract class ActionNode
	{
        private static string TreeIdSerializedKeyName = "_TreeId_";
        private static string ActionNodeIdSerializedKeyName = "_ActionNodeId_";


        /// <exclude />
        public string XPath { get; internal set; }

        /// <exclude />
        public int Id { get; internal set; }


        /// <exclude />
        public string Label { get; set; }                                   // Requried

        /// <exclude />
        public string ToolTip { get; set; }                                 // Defaults to Label

        /// <exclude />
        public ResourceHandle Icon { get; internal set; }                   // Requried

        /// <exclude />
        public ActionLocation Location { get; internal set; }               // Optional

        /// <exclude />
        public List<PermissionType> PermissionTypes { get; internal set; }  // Optional

        /// <exclude />
        public TreeNode OwnerNode { get; internal set; }

        // Cached values
        private DynamicValuesHelper LabelDynamicValuesHelper { get; set; }
        private DynamicValuesHelper ToolTipDynamicValuesHelper { get; set; }


        /// <exclude />
        protected abstract void OnAddAction(Action<ElementAction> actionAdder, EntityToken parentEntityToken, TreeNodeDynamicContext dynamicContext, DynamicValuesHelperReplaceContext dynamicValuesHelperReplaceContext);

        /// <exclude />
        protected virtual void OnInitialize() { }


        /// <exclude />
        protected ActionVisualizedData CreateActionVisualizedData(DynamicValuesHelperReplaceContext dynamicValuesHelperReplaceContext)
        {
            return new ActionVisualizedData
            {
                Label = this.LabelDynamicValuesHelper.ReplaceValues(dynamicValuesHelperReplaceContext),
                ToolTip = this.ToolTipDynamicValuesHelper.ReplaceValues(dynamicValuesHelperReplaceContext),
                Icon = this.Icon,
                Disabled = false,
                ActionLocation = this.Location
            };
        }



        /// <summary>
        /// Use this method to do initializing and validation
        /// </summary>
        internal void Initialize() 
        {
            this.LabelDynamicValuesHelper = new DynamicValuesHelper(this.Label);
            this.LabelDynamicValuesHelper.Initialize(this);

            this.ToolTipDynamicValuesHelper = new DynamicValuesHelper(this.ToolTip);
            this.ToolTipDynamicValuesHelper.Initialize(this);

            OnInitialize();
        }



        /// <exclude />
        public void AddAction(Action<ElementAction> actionAdder, EntityToken entityToken, TreeNodeDynamicContext dynamicContext)
        {
            var piggybag = dynamicContext.Piggybag;
            if (!entityToken.Equals(dynamicContext.CurrentEntityToken))
            {
                piggybag = piggybag.PreparePiggybag(dynamicContext.CurrentTreeNode, dynamicContext.CurrentEntityToken);
            }

            var replaceContext = new DynamicValuesHelperReplaceContext(entityToken, piggybag);

            OnAddAction(actionAdder, entityToken, dynamicContext, replaceContext);
        }



        /// <exclude />
        public string Serialize()
        {
            var sb = new StringBuilder();
            Serialize(sb);
            return sb.ToString();
        }



        /// <exclude />
        public void Serialize(StringBuilder sb)
        {
            StringConversionServices.SerializeKeyValuePair(sb, TreeIdSerializedKeyName, this.OwnerNode.Tree.TreeId);
            StringConversionServices.SerializeKeyValuePair(sb, ActionNodeIdSerializedKeyName, this.Id);
        }



        /// <exclude />
        public static ActionNode Deserialize(string serializedString)
        {
            Dictionary<string, string> serializedValueCollection = StringConversionServices.ParseKeyValueCollection(serializedString);

            return Deserialize(serializedValueCollection);
        }



        /// <exclude />
        public static ActionNode Deserialize(Dictionary<string, string> serializedValueCollection, bool removeEntiresFromCollection = false)
        {
            string treeId = StringConversionServices.DeserializeValueString(serializedValueCollection[TreeIdSerializedKeyName]);
            int actionNodeId = StringConversionServices.DeserializeValueInt(serializedValueCollection[ActionNodeIdSerializedKeyName]);

            if (removeEntiresFromCollection)
            {
                serializedValueCollection.Remove(TreeIdSerializedKeyName);
                serializedValueCollection.Remove(ActionNodeIdSerializedKeyName);
            }

            Tree tree = TreeFacade.GetTree(treeId);
            if (tree == null) return null;

            return tree.GetActionNode(actionNodeId);
        }



        /// <exclude />
        protected void AddValidationError(ValidationError validationError)
        {
            this.OwnerNode.Tree.BuildResult.AddValidationError(validationError);
        }



        /// <exclude />
        protected void AddValidationError(string stringName, params object[] args)
        {
            this.OwnerNode.Tree.BuildResult.AddValidationError(ValidationError.Create(this.XPath, stringName, args));
        }
	}
}
