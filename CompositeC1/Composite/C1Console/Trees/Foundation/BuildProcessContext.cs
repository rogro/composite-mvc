/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;


namespace Composite.C1Console.Trees.Foundation
{
    internal sealed class BuildProcessContext
    {
        private int _nodeIdCounter;
        private List<string> _usedIds = new List<string>();

        public BuildProcessContext()
        {
            _nodeIdCounter = 0;
            this.ActionIdCounter = 0;
            this.FilterIdCounter = 0;
        }

        
        public int ActionIdCounter;
        public int FilterIdCounter;



        public string CreateNewNodeId()
        {
            return string.Format("NodeAutoId_{0}", _nodeIdCounter++);
        }




        public bool AlreadyUsed(string id)
        {
            return _usedIds.Contains(id);
        }



        public void AddUsedId(string id)
        {
            _usedIds.Add(id);
        }



        public Dictionary<Type, List<TreeNode>> DataInteraceToTreeNodes = new Dictionary<Type, List<TreeNode>>();
    }
}
