/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using Composite.Data;
using Composite.C1Console.Security;
using Composite.Data.ProcessControlled;


namespace Composite.C1Console.Trees.Foundation.AttachmentPoints
{
    /// <summary>
    /// This class is used when the user adds trees dynamicly
    /// Used as a dual with Composite.Data.Types.IDataItemTreeAttachmentPoint
    /// </summary>    
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    [DebuggerDisplay("DynamicDataItemAttachmentPoint. Type = '{InterfaceType}', Key = '{KeyValue}'")]
    public sealed class DynamicDataItemAttachmentPoint : BaseAttachmentPoint, IDataItemAttachmentPoint
    {
        /// <exclude />
        public Type InterfaceType { get; set; }

        /// <exclude />
        public object KeyValue { get; set; }


        /// <exclude />
        public override bool IsAttachmentPoint(EntityToken parentEntityToken)
        {
            DataEntityToken dataEntityToken = parentEntityToken as DataEntityToken;
            if (dataEntityToken == null) return false;

            if (dataEntityToken.InterfaceType != this.InterfaceType) return false;

            // The data item has not been localized, so down attach the tree.
            if (dataEntityToken.DataSourceId.LocaleScope.Equals(LocalizationScopeManager.CurrentLocalizationScope) == false) return false;

            object keyValue = dataEntityToken.DataSourceId.GetKeyValue();

            return object.Equals(keyValue, this.KeyValue);
        }


        /// <exclude />
        public override IEnumerable<EntityToken> GetEntityTokens(EntityToken childEntityToken, TreeNodeDynamicContext dynamicContext)
        {
            if (typeof(ILocalizedControlled).IsAssignableFrom(this.InterfaceType))
            {
                foreach (CultureInfo cultureInfo in DataLocalizationFacade.ActiveLocalizationCultures)
                {
                    using (new DataScope(cultureInfo))
                    {
                        EntityToken entityToken = GetEntityTokensImpl();
                        if (entityToken != null)
                        {
                            yield return entityToken;
                        }
                    }
                }
            }
            else
            {
                yield return GetEntityTokensImpl();
            }
        }



        private EntityToken GetEntityTokensImpl()
        {
            IData data = DataFacade.TryGetDataByUniqueKey(this.InterfaceType, KeyValue);
            return data == null ? null : data.GetDataEntityToken();
        }


        /// <exclude />
        public override void Log(string title, string indention = "")
        {
            Core.Log.LogVerbose(title, string.Format("{0}DynamicDataType: Position = {1}, Type = {2}, KeyValue = {3}", indention, this.Position, this.InterfaceType, this.KeyValue));
        }
    }
}
