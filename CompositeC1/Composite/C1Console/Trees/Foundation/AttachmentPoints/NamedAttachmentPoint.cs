/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System.Collections.Generic;
using Composite.C1Console.Elements;
using Composite.Core.Logging;
using Composite.C1Console.Security;


namespace Composite.C1Console.Trees.Foundation.AttachmentPoints
{
    internal sealed class NamedAttachmentPoint : BaseAttachmentPoint, INamedAttachmentPoint
    {
        public AttachingPoint AttachingPoint { get; set; }


        public override IEnumerable<EntityToken> GetEntityTokens(EntityToken childEntityToken, TreeNodeDynamicContext dynamicContext) 
        { 
            yield return this.AttachingPoint.EntityToken; 
        }


        public override bool IsAttachmentPoint(EntityToken parentEntityToken)
        {
            return ElementAttachingPointFacade.IsAttachingPoint(parentEntityToken, this.AttachingPoint);
        }


        public override void Log(string title, string indention = "")
        {
            LoggingService.LogVerbose(title, string.Format("{0}Named: Position = {1}, Id = {2}, EntityTokenType = {3}, EntityToken = {4}", indention, this.Position, this.AttachingPoint.Id, this.AttachingPoint.EntityTokenType, this.AttachingPoint.EntityToken));
        }
    }




    internal sealed class EntityTokenAttachmentPoint : BaseAttachmentPoint
    {
        public EntityToken EntityToken { get; set; }


        public override IEnumerable<EntityToken> GetEntityTokens(EntityToken childEntityToken, TreeNodeDynamicContext dynamicContext)
        {
            yield return this.EntityToken;
        }


        public override bool IsAttachmentPoint(EntityToken parentEntityToken)
        {
            return ElementAttachingPointFacade.IsAttachingPoint(parentEntityToken, this.EntityToken);
        }


        public override void Log(string title, string indention = "")
        {
            LoggingService.LogVerbose(title, string.Format("{0}EntityToken: Position = {1}, EntityToken = {2}", indention, this.Position, this.EntityToken));
        }
    }    
}
