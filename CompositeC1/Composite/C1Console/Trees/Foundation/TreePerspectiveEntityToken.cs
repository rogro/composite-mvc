/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using Composite.C1Console.Security;
using System.Collections.Generic;
using Composite.Core.Serialization;
using System.Text;


namespace Composite.C1Console.Trees.Foundation
{
    [SecurityAncestorProvider(typeof(Composite.C1Console.Security.SecurityAncestorProviders.NoAncestorSecurityAncestorProvider))]
    internal class TreePerspectiveEntityToken : EntityToken
    {
        private readonly string _id;
        private List<string> _childTrees = new List<string>();


        public TreePerspectiveEntityToken(string id)
        {
            _id = id;
        }


        public override string Id
        {
            get { return _id; }
        }


        public override string Type
        {
            get { return "C1Trees"; }
        }


        public override string Source
        {
            get { return "C1Trees"; }
        }


        public void AddChildTree(string treeId)
        {
            _childTrees.Add(treeId);
        }


        public override string Serialize()
        {
            return Id;
            /*StringBuilder sb = new StringBuilder();
            
            StringConversionServices.SerializeKeyValuePair(sb, "Id", Id);
            
            int counter = 0;
            foreach (string treeId in _childTrees)
            {
                string key = "TreeId" + (counter++);
                StringConversionServices.SerializeKeyValuePair(sb, key, treeId);
            }

            return sb.ToString();*/
        }


        public static EntityToken Deserialize(string serializedEntityToken)
        {
           /* Dictionary<string, string> dic = StringConversionServices.ParseKeyValueCollection(serializedEntityToken);

            string id = StringConversionServices.DeserializeValueString(dic["Id"]);

            int count = 0;
            while (true)
            {
                string key = "TreeId" + (counter++);
            }*/

            return new TreePerspectiveEntityToken(serializedEntityToken);
        }
    }
}
