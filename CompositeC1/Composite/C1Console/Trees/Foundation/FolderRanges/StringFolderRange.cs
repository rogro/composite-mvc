/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Diagnostics;
using Composite.Core.ResourceSystem;


namespace Composite.C1Console.Trees.Foundation.FolderRanges
{
    [DebuggerDisplay("{Index} : ({MinValue}-{MaxValue}) : {Label}")]
    internal sealed class StringFolderRange : IFolderRange
    {
        private int _index;
        private string _minValue;
        private string _maxValue;
        private bool _isMinOpenEnded;
        private bool _isMaxOpenEnded;


        public StringFolderRange(int index, string minValue, string maxValue, bool isMinOpenEnded, bool isMaxOpenEnded)
        {
            _index = index;
            _minValue = minValue;
            _maxValue = maxValue;
            _isMinOpenEnded = isMinOpenEnded;
            _isMaxOpenEnded = isMaxOpenEnded;
        }



        public int Index
        {
            get { return _index; }
        }



        public object MinValue
        {
            get { return _minValue; }
        }



        public object MaxValue
        {
            get { return _maxValue; }
        }



        public bool IsMinOpenEnded
        {
            get { return _isMinOpenEnded; }
        }



        public bool IsMaxOpenEnded
        {
            get { return _isMaxOpenEnded; }
        }



        public string Label
        {
            get
            {
                if (this.Index == -1)
                {
                    return StringResourceSystemFacade.GetString("Composite.C1Console.Trees", "TreeRanges.StringRange.Other");
                }
                else if (this.IsMinOpenEnded)
                {
                    return string.Format(StringResourceSystemFacade.GetString("Composite.C1Console.Trees", "TreeRanges.StringRange.MinOpenEnded"), this.MaxValue);
                }
                else if (this.IsMaxOpenEnded)
                {
                    return string.Format(StringResourceSystemFacade.GetString("Composite.C1Console.Trees", "TreeRanges.StringRange.MaxOpenEnded"), this.MinValue);
                }
                else 
                {
                    return string.Format(StringResourceSystemFacade.GetString("Composite.C1Console.Trees", "TreeRanges.StringRange.Closed"), this.MinValue, this.MaxValue);
                }
            }
        }



        public object DefaultValue
        {
            get
            {
                return "";
            }
        }
    }
}
