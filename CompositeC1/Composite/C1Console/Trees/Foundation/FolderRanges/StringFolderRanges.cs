/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Composite.Core.Linq;


namespace Composite.C1Console.Trees.Foundation.FolderRanges
{
    internal sealed class StringFolderRanges : BaseFolderRanges
    {
        private readonly MethodInfo StringCompare_MethodInfo = typeof(string).GetMethods().First(f => f.Name == "Compare");
        private readonly MethodInfo StringToUpper_MethodInfo = typeof(string).GetMethods().First(f => f.Name == "ToUpper");
        private readonly MethodInfo StringStartsWith_MethodInfo = typeof(string).GetMethods().First(f => f.Name == "StartsWith");


        public override Expression CreateContainsListSelectBodyExpression(Expression fieldExpression, ParameterExpression parameterExpression)
        {
            Expression currentExpression = null;

            foreach (IFolderRange folderRange in this.Ranges)
            {
                string minValue = GetMinValue(folderRange);
                string maxValue = GetMaxValue(folderRange);

                if (currentExpression == null)
                {
                    currentExpression = Expression.Constant(-1);
                }

                var fieldToUpper = Expression.Call(fieldExpression, this.StringToUpper_MethodInfo);

                currentExpression = Expression.Condition(
                    Expression.AndAlso(
                        Expression.NotEqual(
                            fieldExpression,
                            Expression.Constant(null, typeof(string))
                        ),
                        Expression.AndAlso(
                            Expression.GreaterThanOrEqual(
                                Expression.Call(
                                    this.StringCompare_MethodInfo,
                                    fieldToUpper,
                                    Expression.Constant(minValue.ToUpperInvariant())
                                ),
                                Expression.Constant(0)
                            ),
                            Expression.OrElse(
                                Expression.LessThanOrEqual(
                                    Expression.Call(
                                        this.StringCompare_MethodInfo,
                                        fieldToUpper,
                                        Expression.Constant(maxValue.ToUpperInvariant())
                                    ),
                                    Expression.Constant(0)
                                ),
                                Expression.Call(fieldToUpper, StringStartsWith_MethodInfo, Expression.Constant(maxValue.ToUpperInvariant()))
                            )
                            
                        )
                    ),
                    Expression.Constant(folderRange.Index),
                    currentExpression
                );
            }

            return currentExpression;
        }



        public override Expression CreateFilterExpression(int folderRangeIndex, Expression fieldExpression)
        {
            if (folderRangeIndex != -1)
            {
                IFolderRange folderRange = this.GetFolderRange(folderRangeIndex);

                return CreateFolderRangeExpression(folderRange, fieldExpression);
            }
            
            Expression currentExpression = null;
            foreach (IFolderRange folderRange in this.Ranges.Where(f => f.Index != -1))
            {
                Expression expression = CreateFolderRangeExpression(folderRange, fieldExpression);

                expression = Expression.Not(expression);

                currentExpression = currentExpression.NestedAnd(expression);
            }

            return currentExpression;
        }



        private Expression CreateFolderRangeExpression(IFolderRange folderRange, Expression fieldExpression)
        {
            string minValue = GetMinValue(folderRange);
            string maxValue = GetMaxValue(folderRange);

            var fieldToUpper = Expression.Call(fieldExpression, this.StringToUpper_MethodInfo);

            Expression expression = Expression.AndAlso(
                Expression.NotEqual(
                    fieldExpression,
                    Expression.Constant(null, typeof(string))
                ),
                Expression.AndAlso(
                    Expression.GreaterThanOrEqual(
                        Expression.Call(
                            this.StringCompare_MethodInfo,
                            fieldToUpper,
                            Expression.Constant(minValue.ToUpperInvariant())
                        ),
                        Expression.Constant(0)
                    ),

                    Expression.OrElse(
                        Expression.LessThanOrEqual(
                            Expression.Call(
                                this.StringCompare_MethodInfo,
                                fieldToUpper,
                                Expression.Constant(maxValue.ToUpperInvariant())
                            ),
                            Expression.Constant(0)
                        ),

                        Expression.Call(fieldToUpper, StringStartsWith_MethodInfo, Expression.Constant(maxValue.ToUpperInvariant()))
                    )
                    
                )
            );

            return expression;
        }



        private static string GetMinValue(IFolderRange folderRange)
        {
            if (folderRange.IsMinOpenEnded)
            {
                return "" + (char.MinValue + 1) + (char.MinValue + 1) + (char.MinValue + 1);
            }

            return (string) folderRange.MinValue;
        }


        private static string GetMaxValue(IFolderRange folderRange)
        {
            if (folderRange.IsMaxOpenEnded)
            {
                return "" + char.MaxValue + char.MaxValue + char.MaxValue;
            }

            return (string) folderRange.MaxValue;
        }
    }
}
