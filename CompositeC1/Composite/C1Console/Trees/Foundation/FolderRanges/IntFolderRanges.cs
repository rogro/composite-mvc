/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Linq;
using System.Linq.Expressions;
using Composite.Core.Linq;


namespace Composite.C1Console.Trees.Foundation.FolderRanges
{
    internal sealed class IntFolderRanges : BaseFolderRanges
    {
        public override Expression CreateContainsListSelectBodyExpression(Expression fieldExpression, ParameterExpression parameterExpression)
        {
            Expression currentExpression = null;

            foreach (IFolderRange folderRange in this.Ranges)
            {
                int minValue = (int)folderRange.MinValue;
                int maxValue = (int)folderRange.MaxValue;


                if (currentExpression == null)
                {
                    currentExpression = Expression.Constant(-1);
                }

                Expression minExpression = Expression.GreaterThanOrEqual(
                    fieldExpression,
                    Expression.Constant(minValue)
                );

                Expression maxExpression = Expression.LessThanOrEqual(
                    fieldExpression,
                    Expression.Constant(maxValue)
                );

                if (folderRange.IsMinOpenEnded)
                {
                    currentExpression = Expression.Condition(
                        maxExpression,
                        Expression.Constant(folderRange.Index),
                        currentExpression
                    );
                }
                else if (folderRange.IsMaxOpenEnded)
                {
                    currentExpression = Expression.Condition(
                        minExpression,
                        Expression.Constant(folderRange.Index),
                        currentExpression
                    );
                }
                else
                {
                    currentExpression = Expression.Condition(
                        Expression.AndAlso(
                            minExpression,
                            maxExpression
                        ),
                        Expression.Constant(folderRange.Index),
                        currentExpression
                    );
                }
            }

            return currentExpression;
        }



        public override Expression CreateFilterExpression(int folderRangeIndex, Expression fieldExpression)
        {
            if (folderRangeIndex != -1)
            {
                IFolderRange folderRange = this.GetFolderRange(folderRangeIndex);

                int minValue = (int)folderRange.MinValue;
                int maxValue = (int)folderRange.MaxValue;

                Expression minExpression = Expression.GreaterThanOrEqual(
                    fieldExpression,
                    Expression.Constant(minValue)
                );

                Expression maxExpression = Expression.LessThanOrEqual(
                    fieldExpression,
                    Expression.Constant(maxValue)
                );

                Expression expression;
                if (folderRange.IsMinOpenEnded)
                {
                    expression = maxExpression;
                }
                else if (folderRange.IsMaxOpenEnded)
                {
                    expression = minExpression;
                }
                else
                {
                    expression = Expression.And(minExpression, maxExpression);
                }

                return expression;
            }
            else
            {
                Expression currentExpression = null;
                foreach (IFolderRange folderRange in this.Ranges.Where(f => f.Index != -1))
                {
                    int minValue = (int)folderRange.MinValue;
                    int maxValue = (int)folderRange.MaxValue;

                    Expression minExpression = Expression.GreaterThanOrEqual(
                        fieldExpression,
                        Expression.Constant(minValue)
                    );

                    Expression maxExpression = Expression.LessThanOrEqual(
                        fieldExpression,
                        Expression.Constant(maxValue)
                    );

                    if (folderRange.IsMinOpenEnded)
                    {
                        currentExpression = currentExpression.NestedAnd(Expression.Not(maxExpression));
                    }
                    else if (folderRange.IsMaxOpenEnded)
                    {
                        currentExpression = currentExpression.NestedAnd(Expression.Not(minExpression));
                    }
                    else
                    {
                        currentExpression = currentExpression.NestedAnd(
                            Expression.Not(
                                Expression.And(
                                    minExpression,
                                    maxExpression
                                )
                            )
                        );
                    }
                }

                return currentExpression;
            }
        }
    }
}
