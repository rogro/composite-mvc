/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;


namespace Composite.C1Console.Trees.Foundation
{
    internal sealed class TupleIndexer
    {
        private dynamic _tuple;

        public TupleIndexer(dynamic tuple)
        {
            _tuple = tuple;
        }


        public int this[int index]
        {
            get
            {
                if (index == 1) return (int)_tuple.Item1;
                else if (index == 2) return (int)_tuple.Item2;
                else if (index == 3) return (int)_tuple.Item3;
                else if (index == 4) return (int)_tuple.Item4;
                else if (index == 5) return (int)_tuple.Item5;
                else if (index == 6) return (int)_tuple.Item6;
                else if (index == 7) return (int)_tuple.Item7;
                else if (index == 8) return (int)_tuple.Item8;
                else throw new IndexOutOfRangeException();
            }
        }



        public object GetAtIndex(int index)
        {
            if (index == 1) return _tuple.Item1;
            else if (index == 2) return _tuple.Item2;
            else if (index == 3) return _tuple.Item3;
            else if (index == 4) return _tuple.Item4;
            else if (index == 5) return _tuple.Item5;
            else if (index == 6) return _tuple.Item6;
            else if (index == 7) return _tuple.Item7;
            else if (index == 8) return _tuple.Item8;
            else throw new IndexOutOfRangeException();
        }
    }
}
