/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Composite.Core.Linq;
using Composite.Core.Types;


namespace Composite.C1Console.Trees
{
    internal sealed class FieldOrderByNode : OrderByNode
    {
        private PropertyInfo PropertyInfo { get; set; }


        public string FieldName { get; internal set; } // Required
        public string Direction { get; internal set; } // Optional


        public override Expression CreateOrderByExpression(Expression sourceExpression, ParameterExpression parameterExpression, bool first)
        {
            Expression fieldExpression = ExpressionHelper.CreatePropertyExpression(this.OwnerNode.InterfaceType, this.PropertyInfo.DeclaringType, this.FieldName, parameterExpression);

            LambdaExpression lambdaExpression = Expression.Lambda(fieldExpression, parameterExpression);

            if (first)
            {
                return this.Direction == "ascending"
                    ? ExpressionCreator.OrderBy(sourceExpression, lambdaExpression)
                    : ExpressionCreator.OrderByDescending(sourceExpression, lambdaExpression);
            }

            return this.Direction == "ascending"
                    ? ExpressionCreator.ThenBy(sourceExpression, lambdaExpression)
                    : ExpressionCreator.ThenByDescending(sourceExpression, lambdaExpression);
        }



        internal override void Initialize()
        {
            if ((this.Direction != "ascending") && (this.Direction != "descending"))
            {
                AddValidationError("TreeValidationError.FieldOrderBy.UnknownDirection", this.Direction);
            }

            this.PropertyInfo = this.OwnerNode.InterfaceType.GetPropertiesRecursively().SingleOrDefault(f => f.Name == this.FieldName);

            if (this.PropertyInfo == null)
            {
                AddValidationError("TreeValidationError.FieldOrderBy.UnknownField", this.OwnerNode.InterfaceType, this.FieldName);
            }
        }



        public override string ToString()
        {
            return string.Format("OrderByNode, FieldName = {0}, Direction = {1}", this.FieldName, this.Direction);
        }
    }
}
