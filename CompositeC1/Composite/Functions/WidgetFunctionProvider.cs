/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using System.Xml.Linq;
using System.Collections.Generic;


namespace Composite.Functions
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
	public sealed class WidgetFunctionProvider
	{
        /// <exclude />
        public static WidgetFunctionProvider BuildNoWidgetProvider()
        {
            return new WidgetFunctionProvider();
        }

        private string _widgetFunctionName = null;
        private IWidgetFunction _widgetFunction = null;
        private IEnumerable<BaseParameterRuntimeTreeNode> _setParameters = null;
        private XElement _serializedWidgetFunction = null;
        private object _lock = new object();

        private WidgetFunctionProvider()
        {
        }



        internal WidgetFunctionProvider(string widgetName)
        {
            if (string.IsNullOrEmpty(widgetName)) throw new ArgumentNullException("widgetName");

            _widgetFunctionName = widgetName;
        }



        internal WidgetFunctionProvider(string widgetName, IEnumerable<BaseParameterRuntimeTreeNode> parameters)
        {
            if (string.IsNullOrEmpty(widgetName)) throw new ArgumentNullException("widgetName");
            if (parameters == null) throw new ArgumentNullException("parameters");

            _widgetFunctionName = widgetName;
            _setParameters = parameters;
        }



        /// <exclude />
        public WidgetFunctionProvider(IWidgetFunction widgetFunction)
        {
            if (widgetFunction == null) throw new ArgumentNullException("widgetFunction");

            _widgetFunction = widgetFunction;
        }



        /// <exclude />
        public WidgetFunctionProvider(XElement serializedWidgetFunction)
        {
            _serializedWidgetFunction = serializedWidgetFunction;
        }



        /// <exclude />
        public string WidgetFunctionCompositeName
        {
            get
            {
                if (string.IsNullOrEmpty(_widgetFunctionName) == false)
                {
                    return _widgetFunctionName;
                }

                EnsureWidgetFunction();

                if (_widgetFunction != null)
                {
                    return _widgetFunction.CompositeName();
                }

                throw new InvalidOperationException("Neither name nor IWidgetFunction found");
            }
        }



        /// <exclude />
        public IWidgetFunction WidgetFunction
        {
            get
            {
                EnsureWidgetFunction();

                if (_widgetFunction == null)
                {
                    if (string.IsNullOrEmpty(_widgetFunctionName) == false)
                    {
                        _widgetFunction = FunctionFacade.GetWidgetFunction(_widgetFunctionName);
                    }
                }

                
                return _widgetFunction;
            }
        }



        /// <exclude />
        public IEnumerable<BaseParameterRuntimeTreeNode> WidgetFunctionParameters
        {
            get
            {
                if (_setParameters == null)
                {
                    yield break;
                }
                else
                {
                    foreach (BaseParameterRuntimeTreeNode param in _setParameters)
                    {
                        yield return param;
                    }
                }
            }
        }



        /// <exclude />
        public XElement SerializedWidgetFunction
        {
            get 
            {
                EnsureWidgetFunction();

                WidgetFunctionRuntimeTreeNode widgetRuntimeTreeNode = new WidgetFunctionRuntimeTreeNode(this.WidgetFunction, this.WidgetFunctionParameters.ToList());

                return widgetRuntimeTreeNode.Serialize();
            }
        }



        private void EnsureWidgetFunction()
        {
            lock (_lock)
            {
                if (_widgetFunction == null && _serializedWidgetFunction != null)
                {
                    WidgetFunctionRuntimeTreeNode functionNode = (WidgetFunctionRuntimeTreeNode)FunctionFacade.BuildTree(_serializedWidgetFunction);
                    _setParameters = functionNode.GetSetParameters().ToList();
                    _widgetFunction = functionNode.GetWidgetFunction();
                }
            }
        }
	}
}
