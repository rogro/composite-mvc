/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Xml.Linq;
using Composite.Functions.Foundation;


namespace Composite.Functions
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class WidgetFunctionRuntimeTreeNode : BaseFunctionRuntimeTreeNode
    {
        private IWidgetFunction _widgetFunction;


        /// <exclude />
        public WidgetFunctionRuntimeTreeNode(IWidgetFunction widgetFunction)
            : this(widgetFunction, "", new HelpDefinition(""), "", new List<BaseParameterRuntimeTreeNode>())
        {
        }


        /// <exclude />
        public WidgetFunctionRuntimeTreeNode(IWidgetFunction widgetFunction, List<BaseParameterRuntimeTreeNode> parameters)
            : this(widgetFunction, "", new HelpDefinition(""), "", parameters)
        {
        }


        /// <exclude />
        public WidgetFunctionRuntimeTreeNode(IWidgetFunction widgetFunction, string label, HelpDefinition helpDefinition, string bindingSourceName)
            : this(widgetFunction, label, helpDefinition, bindingSourceName, new List<BaseParameterRuntimeTreeNode>())
        {
        }


        /// <exclude />
        public string Label { get; set; }

        /// <exclude />
        public HelpDefinition HelpDefinition { get; set; }

        /// <exclude />
        public string BindingSourceName { get; set; }


        internal WidgetFunctionRuntimeTreeNode(IWidgetFunction widgetFunction, string label, HelpDefinition helpDefinition, string bindingSourceName, List<BaseParameterRuntimeTreeNode> parameters)
        {
            _widgetFunction = widgetFunction;
            this.Label = label;
            this.HelpDefinition = helpDefinition;
            this.BindingSourceName = bindingSourceName;
            this.Parameters = parameters;
        }


        /// <exclude />
        protected override IMetaFunction HostedFunction
        {
            get { return _widgetFunction; }
        }


        /// <exclude />
        public IWidgetFunction GetWidgetFunction()
        {
            return _widgetFunction;
        }


        /// <exclude />
        public override object GetValue(FunctionContextContainer contextContainer)
        {
            if (contextContainer == null) throw new ArgumentNullException("contextContainer");

            ValidateNotSelfCalling();

            ParameterList parameters = new ParameterList(contextContainer);

            foreach (ParameterProfile parameterProfile in _widgetFunction.ParameterProfiles)
            {
                BaseParameterRuntimeTreeNode parameterTreeNode = this.Parameters.Where(ptn => ptn.Name == parameterProfile.Name).SingleOrDefault();

                if (parameterTreeNode == null)
                {
                    BaseValueProvider valueProvider = parameterProfile.FallbackValueProvider;

                    object value = valueProvider.GetValue(contextContainer);

                    parameters.AddConstantParameter(parameterProfile.Name, value, parameterProfile.Type);
                }
                else
                {
                    parameters.AddLazyParameter(parameterProfile.Name, parameterTreeNode, parameterProfile.Type);
                }
            }

            return _widgetFunction.GetWidgetMarkup(parameters, this.Label, this.HelpDefinition, this.BindingSourceName);
        }


        /// <exclude />
        public override IEnumerable<string> GetAllSubFunctionNames()
        {
            List<string> names = new List<string>();

            foreach (BaseParameterRuntimeTreeNode parameter in this.Parameters)
            {
                names.AddRange(parameter.GetAllSubFunctionNames());
            }

            return names.Distinct();
        }


        /// <exclude />
        public override bool ContainsNestedFunctions
        {
            get
            {
                foreach (var parameter in this.Parameters)
                {
                    if (parameter.ContainsNestedFunctions)
                    {
                        return true;
                    }
                }

                return false;
            }
        }


        /// <exclude />
        public override XElement Serialize()
        {
            XElement element = XElement.Parse(string.Format(@"<f:{0} xmlns:f=""{1}"" />", FunctionTreeConfigurationNames.WidgetFunctionTagName, FunctionTreeConfigurationNames.NamespaceName));

            element.Add(new XAttribute(FunctionTreeConfigurationNames.NameAttributeName, _widgetFunction.CompositeName()));

            if (!string.IsNullOrEmpty(this.Label))
                element.Add(new XAttribute(FunctionTreeConfigurationNames.LabelAttributeName, this.Label));

            if (!string.IsNullOrEmpty(this.BindingSourceName))
                element.Add(new XAttribute(FunctionTreeConfigurationNames.BindingSourceNameAttributeName, this.BindingSourceName));

            if (this.HelpDefinition != null && !string.IsNullOrEmpty(this.HelpDefinition.HelpText))
            {
                element.Add(this.HelpDefinition.Serialize());
            }

            foreach (ParameterProfile parameterProfile in _widgetFunction.ParameterProfiles)
            {
                BaseParameterRuntimeTreeNode parameterRuntimeTreeNode = this.Parameters.Where(ptn => ptn.Name == parameterProfile.Name).FirstOrDefault();

                if (parameterRuntimeTreeNode != null)
                {
                    element.Add(parameterRuntimeTreeNode.Serialize());
                }
            }

            return element;
        }
    }
}
