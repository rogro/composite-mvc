/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Composite.Core.Collections.Generic;
using Composite.Core.Logging;


namespace Composite.Functions.Foundation
{
    internal sealed class MetaFunctionProviderRegistryImpl : IMetaFunctionProviderRegistry
    {
        private ResourceLocker<Resources> _resourceLocker = new ResourceLocker<Resources>(new Resources(), Resources.DoInitialize);



        public List<string> FunctionNames
        {
            get
            {
                using (GlobalInitializerFacade.CoreIsInitializedScope)
                {
                    using (_resourceLocker.ReadLocker)
                    {
                        return _resourceLocker.Resources.FunctionContainer.FunctionNames.ToList();
                    }
                }
            }
        }



        public List<string> WidgetFunctionNames
        {
            get
            {
                using (GlobalInitializerFacade.CoreIsInitializedScope)
                {
                    using (_resourceLocker.ReadLocker)
                    {
                        return _resourceLocker.Resources.WidgetFunctionContainer.FunctionNames.ToList();
                    }
                }
            }
        }



        public IEnumerable<string> FunctionNamesByProviderName(string providerName)
        {
            if (string.IsNullOrEmpty(providerName)) throw new ArgumentNullException("providerName");

            using (GlobalInitializerFacade.CoreIsInitializedScope)
            {
                using (_resourceLocker.ReadLocker)
                {
                    return _resourceLocker.Resources.FunctionContainer.FunctionNamesByProviderName(providerName);
                }
            }
        }



        public IEnumerable<string> WidgetFunctionNamesByProviderName(string providerName)
        {
            if (string.IsNullOrEmpty(providerName)) throw new ArgumentNullException("providerName");

            using (GlobalInitializerFacade.CoreIsInitializedScope)
            {
                using (_resourceLocker.ReadLocker)
                {
                    return _resourceLocker.Resources.WidgetFunctionContainer.FunctionNamesByProviderName(providerName);
                }
            }
        }



        public IEnumerable<string> GetFunctionNamesByType(Type supportedType)
        {
            if (supportedType == null) throw new ArgumentNullException("supportedType");

            using (GlobalInitializerFacade.CoreIsInitializedScope)
            {
                using (_resourceLocker.ReadLocker)
                {
                    return _resourceLocker.Resources.FunctionContainer.GetFunctionNamesByType(supportedType);
                }
            }
        }



        public IEnumerable<string> GetWidgetFunctionNamesByType(Type supportedType)
        {
            if (supportedType == null) throw new ArgumentNullException("supportedType");

            using (GlobalInitializerFacade.CoreIsInitializedScope)
            {
                using (_resourceLocker.ReadLocker)
                {
                    return _resourceLocker.Resources.WidgetFunctionContainer.GetFunctionNamesByType(supportedType);
                }
            }
        }



        public IFunction GetFunction(string name)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException("name");

            using (GlobalInitializerFacade.CoreIsInitializedScope)
            {
                using (_resourceLocker.ReadLocker)
                {
                    return (IFunction)_resourceLocker.Resources.FunctionContainer.GetFunction(name);
                }
            }
        }



        public IWidgetFunction GetWidgetFunction(string name)
        {
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException("name");

            using (GlobalInitializerFacade.CoreIsInitializedScope)
            {
                using (_resourceLocker.ReadLocker)
                {
                    return (IWidgetFunction)_resourceLocker.Resources.WidgetFunctionContainer.GetFunction(name);
                }
            }
        }



        public IEnumerable<Type> FunctionSupportedTypes
        {
            get
            {
                using (GlobalInitializerFacade.CoreIsInitializedScope)
                {
                    using (_resourceLocker.ReadLocker)
                    {
                        return _resourceLocker.Resources.FunctionContainer.FunctionSupportedTypes;
                    }
                }
            }
        }



        public IEnumerable<Type> WidgetFunctionSupportedTypes
        {
            get
            {
                using (GlobalInitializerFacade.CoreIsInitializedScope)
                {
                    using (_resourceLocker.ReadLocker)
                    {
                        return _resourceLocker.Resources.WidgetFunctionContainer.FunctionSupportedTypes;
                    }
                }
            }
        }



        public void ReinitializeFunctionFromProvider(string providerName)
        {
            if (string.IsNullOrEmpty(providerName)) throw new ArgumentNullException("providerName");

            using (GlobalInitializerFacade.CoreIsInitializedScope)
            {
                using (_resourceLocker.Locker)
                {
                    _resourceLocker.Resources.FunctionContainer.ReInitializeFunctionsFromProvider(providerName);
                }
            }
        }



        public void ReinitializeWidgetFunctionFromProvider(string providerName)
        {
            if (string.IsNullOrEmpty(providerName)) throw new ArgumentNullException("providerName");

            using (GlobalInitializerFacade.CoreIsInitializedScope)
            {
                using (_resourceLocker.Locker)
                {
                    _resourceLocker.Resources.WidgetFunctionContainer.ReInitializeFunctionsFromProvider(providerName);
                }
            }
        }



        public void Initialize_PostDataTypes()
        {
            _resourceLocker.ResetInitialization();

            using (_resourceLocker.Locker)
            {
                // Keeping static/dynamic function initialization order for backward compatibility
                _resourceLocker.Resources.FunctionContainer.Initialize(false);
                _resourceLocker.Resources.WidgetFunctionContainer.Initialize(false);
                _resourceLocker.Resources.FunctionContainer.Initialize(true);
                _resourceLocker.Resources.WidgetFunctionContainer.Initialize(true);
            }
        }       



        public void Flush()
        {
            _resourceLocker.ResetInitialization();
        }



        private sealed class Resources
        {
            public static void DoInitialize(Resources resources)
            {
                resources.ExcludedFunctionNames = new List<string>();
                resources.FunctionContainer = new FunctionContainer(resources.ExcludedFunctionNames);
                resources.WidgetFunctionContainer = new WidgetFunctionContainer(resources.ExcludedFunctionNames);
            }

            public FunctionContainer FunctionContainer { get; set; }
            public WidgetFunctionContainer WidgetFunctionContainer { get; set; }
            public List<string> ExcludedFunctionNames { get; set; }
        }
    }
}
