/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using Composite.C1Console.Events;


namespace Composite.Functions.Foundation
{
	internal static class MetaFunctionProviderRegistry
	{
        private static IMetaFunctionProviderRegistry _metaFunctionProviderRegistry = new MetaFunctionProviderRegistryImpl();
        

        static MetaFunctionProviderRegistry()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlushEvent);
        }


        internal static IMetaFunctionProviderRegistry Implementation { get { return _metaFunctionProviderRegistry; } set { _metaFunctionProviderRegistry = value; } }



        public static List<string> FunctionNames
        {
            get
            {
                return _metaFunctionProviderRegistry.FunctionNames;
            }
        }



        public static List<string> WidgetFunctionNames
        {
            get
            {
                return _metaFunctionProviderRegistry.WidgetFunctionNames;
            }
        }



        public static IEnumerable<string> FunctionNamesByProviderName(string providerName)
        {
            return _metaFunctionProviderRegistry.FunctionNamesByProviderName(providerName);
        }



        public static IEnumerable<string> WidgetFunctionNamesByProviderName(string providerName)
        {
            return _metaFunctionProviderRegistry.WidgetFunctionNamesByProviderName(providerName);
        }



        public static IEnumerable<string> GetFunctionNamesByType(Type supportedType)
        {
            return _metaFunctionProviderRegistry.GetFunctionNamesByType(supportedType);
        }



        public static IEnumerable<string> GetWidgetFunctionNamesByType(Type supportedType)
        {
            return _metaFunctionProviderRegistry.GetWidgetFunctionNamesByType(supportedType);
        }



        public static IFunction GetFunction(string name)
        {
            return _metaFunctionProviderRegistry.GetFunction(name);
        }



        public static IWidgetFunction GetWidgetFunction(string name)
        {
            return _metaFunctionProviderRegistry.GetWidgetFunction(name);
        }



        public static IEnumerable<Type> FunctionSupportedTypes
        {
            get
            {
                return _metaFunctionProviderRegistry.FunctionSupportedTypes;
            }
        }



        public static IEnumerable<Type> WidgetFunctionSupportedTypes
        {
            get
            {
                return _metaFunctionProviderRegistry.WidgetFunctionSupportedTypes;
            }
        }



        internal static void ReinitializeFunctionFromProvider(string providerName)
        {
            _metaFunctionProviderRegistry.ReinitializeFunctionFromProvider(providerName);
        }



        internal static void ReinitializeWidgetFunctionFromProvider(string providerName)
        {
            _metaFunctionProviderRegistry.ReinitializeWidgetFunctionFromProvider(providerName);
        }



        internal static void Initialize_PostDataTypes()
        {
            if (RuntimeInformation.IsDebugBuild)
            {
                GlobalInitializerFacade.ValidateIsOnlyCalledFromGlobalInitializerFacade(new StackTrace());
            }

            _metaFunctionProviderRegistry.Initialize_PostDataTypes();
        }



        private static void OnFlushEvent(FlushEventArgs args)
        {
            _metaFunctionProviderRegistry.Flush();
        }
	}
}
