/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.ComponentModel;
using System.Xml.Linq;


namespace Composite.Functions.Foundation
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [EditorBrowsable(EditorBrowsableState.Never)] 
    public static class FunctionTreeConfigurationNames
    {
        private static readonly XName _paramTag = (XNamespace) NamespaceName + ParamTagName;
        private static readonly XName _paramElementTag = (XNamespace) NamespaceName + ParamElementTagName;
        private static readonly XName _valueAttribute = ValueAttributeName;
        private static readonly XName _nameAttribute = NameAttributeName;

        /// <exclude />
        public static XName ParamTag { get { return _paramTag; } }

        /// <exclude />
        public static XName ParamElementTag { get { return _paramElementTag; } }

        /// <exclude />
        public static XName NameAttribute { get { return _nameAttribute; } }

        /// <exclude />
        public static XName ValueAttribute { get { return _valueAttribute; } }

        /// <exclude />
        public static string NamespaceName { get { return "http://www.composite.net/ns/function/1.0"; } }

        /// <exclude />
        public static string ParamTagName { get { return "param"; } }

        /// <exclude />
        public static string ParamElementTagName { get { return "paramelement"; } }

        /// <exclude />
        public static string FunctionTagName { get { return "function"; } }

        /// <exclude />
        public static string WidgetFunctionTagName { get { return "widgetfunction"; } }

        /// <exclude />
        public static string HelpDefinitionTagName { get { return "helpdefinition"; } }

        /// <exclude />
        public static string NameAttributeName { get { return "name"; } }

        /// <exclude />
        public static string ValueAttributeName { get { return "value"; } }

        /// <exclude />
        public static string LabelAttributeName { get { return "label"; } }

        /// <exclude />
        public static string BindingSourceNameAttributeName { get { return "bindingsourcename"; } }

        /// <exclude />
        public static string HelpTextAttributeName { get { return "helptext"; } }
	}
}
