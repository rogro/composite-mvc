/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Composite.Core;
using Composite.Core.Configuration;
using Composite.Functions.Foundation.PluginFacades;
using Composite.Functions.Plugins.FunctionProvider.Runtime;


namespace Composite.Functions.Foundation
{
    internal sealed class FunctionContainer : MetaFunctionContainer
    {
        internal FunctionContainer(List<string> excludedFunctionNames)
            : base(excludedFunctionNames)
        {
        }


        protected override IEnumerable<string> OnGetProviderNames()
        {
            if ((ConfigurationServices.ConfigurationSource == null) ||
                (ConfigurationServices.ConfigurationSource.GetSection(FunctionProviderSettings.SectionName) == null))
            {
                Log.LogError("FunctionProviderRegistry", "Failed to load the configuration section '{0}' from the configuration", FunctionProviderSettings.SectionName);

                return new List<string>();
            }

            var functionProviderSettings = ConfigurationServices.ConfigurationSource.GetSection(FunctionProviderSettings.SectionName) as FunctionProviderSettings;

            return functionProviderSettings.FunctionProviderPlugins.Select(plugin => plugin.Name);
        }



        protected override IEnumerable<IMetaFunction> OnGetFunctionsFromProvider(string providerName, FunctionTypesToReturn functionTypesToReturn)
        {
            switch (functionTypesToReturn)
            {
                case FunctionTypesToReturn.StaticDependentFunctions:
                    return FunctionProviderPluginFacade.Functions(providerName);
                    
                case FunctionTypesToReturn.DynamicDependentOnlyFunctions:
                    return FunctionProviderPluginFacade.DynamicTypeDependentFunctions(providerName);
                    
                case FunctionTypesToReturn.AllFunctions:
                    IEnumerable<IMetaFunction> functions = FunctionProviderPluginFacade.Functions(providerName);
                    return functions.Concat(FunctionProviderPluginFacade.DynamicTypeDependentFunctions(providerName));
            }

            throw new NotImplementedException(string.Format("Unexpected FunctionTypesToReturn enumeration value '{0}' from provider '{1}'", functionTypesToReturn, providerName));
        }

        protected override void OnFunctionsAdded(List<string> functionNames, bool fireEvents)
        {
            if (fireEvents)
            {
                FunctionEventSystemFacade.FireFunctionAddedEvent(new FunctionsAddedEventArgs(functionNames));
            }
        }



        protected override void OnFunctionsRemoved(List<string> functionNames)
        {
            FunctionEventSystemFacade.FireFunctionRemovedEvent(new FunctionsRemovedEventArgs(functionNames));
        }



        protected override string FunctionType
        {
            get { return "Function"; }
        }
    }
}
