/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Configuration;
using Composite.Core.Collections.Generic;
using Composite.C1Console.Events;
using Composite.Functions.Plugins.FunctionProvider;
using Composite.Functions.Plugins.FunctionProvider.Runtime;


namespace Composite.Functions.Foundation.PluginFacades
{
    internal static class FunctionProviderPluginFacade
    {
        private static ResourceLocker<Resources> _resourceLocker = new ResourceLocker<Resources>(new Resources(), Resources.Initialize);



        static FunctionProviderPluginFacade()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlushEvent);
        }



        public static IEnumerable<IFunction> Functions(string providerName)
        {
            using (_resourceLocker.Locker)
            {
                List<IFunction> functions = new List<IFunction>();

                IFunctionProvider provider = GetFunctionProvider(providerName);
                var functionsFromProvider = provider.Functions;

                Verify.IsNotNull(functionsFromProvider, "Provider '{0}' has returned null function colleciton", providerName);

                foreach (IFunction function in functionsFromProvider)
                {
                    functions.Add(new FunctionWrapper(function));
                }

                return functions;
            }
        }



        public static IEnumerable<IFunction> DynamicTypeDependentFunctions(string providerName)
        {
            using (_resourceLocker.Locker)
            {
                List<IFunction> functions = new List<IFunction>();

                IDynamicTypeFunctionProvider provider = GetFunctionProvider(providerName) as IDynamicTypeFunctionProvider;

                if (provider != null)
                {
                    foreach (IFunction function in provider.DynamicTypeDependentFunctions)
                    {
                        functions.Add(new FunctionWrapper(function));
                    }
                }

                return functions;
            }
        }



        internal static IFunctionProvider GetFunctionProvider(string providerName)
        {
            IFunctionProvider functionProvider;

            var resources = _resourceLocker;

            using (resources.Locker)
            {
                if (resources.Resources.ProviderCache.TryGetValue(providerName, out functionProvider) == false)
                {
                    try
                    {
                        functionProvider = resources.Resources.Factory.Create(providerName);

                        functionProvider.FunctionNotifier = new FunctionNotifier(providerName);

                        resources.Resources.ProviderCache.Add(providerName, functionProvider);
                    }
                    catch (ArgumentException ex)
                    {
                        HandleConfigurationError(ex);
                    }
                    catch (ConfigurationErrorsException ex)
                    {
                        HandleConfigurationError(ex);
                    }
                }
            }

            return functionProvider;
        }



        private static void Flush()
        {
            _resourceLocker.ResetInitialization();
        }



        private static void OnFlushEvent(FlushEventArgs args)
        {
            Flush();
        }



        private static void HandleConfigurationError(Exception ex)
        {
            Flush();

            throw new ConfigurationErrorsException(string.Format("Failed to load the configuration section '{0}' from the configuration.", FunctionProviderSettings.SectionName), ex);
        }


        private sealed class Resources
        {
            public FunctionProviderFactory Factory { get; set; }
            public Dictionary<string, IFunctionProvider> ProviderCache { get; set; }

            public static void Initialize(Resources resources)
            {
                try
                {
                    resources.Factory = new FunctionProviderFactory();
                }
                catch (NullReferenceException ex)
                {
                    HandleConfigurationError(ex);
                }
                catch (ConfigurationErrorsException ex)
                {
                    HandleConfigurationError(ex);
                }

                resources.ProviderCache = new Dictionary<string, IFunctionProvider>();
            }
        }
    }
}
