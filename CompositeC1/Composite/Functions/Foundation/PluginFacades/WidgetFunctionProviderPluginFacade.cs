/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;
using System.Configuration;
using Composite.Core.Collections.Generic;
using Composite.C1Console.Events;
using Composite.Functions.Plugins.WidgetFunctionProvider;
using Composite.Functions.Plugins.WidgetFunctionProvider.Runtime;


namespace Composite.Functions.Foundation.PluginFacades
{
    internal static class WidgetFunctionProviderPluginFacade
    {
        private static ResourceLocker<Resources> _resourceLocker = new ResourceLocker<Resources>(new Resources(), Resources.Initialize);



        static WidgetFunctionProviderPluginFacade()
        {
            GlobalEventSystemFacade.SubscribeToFlushEvent(OnFlushEvent);
        }



        public static IEnumerable<IWidgetFunction> Functions(string providerName)
        {
            using (_resourceLocker.Locker)
            {
                List<IWidgetFunction> widgetFunctions = new List<IWidgetFunction>();

                IWidgetFunctionProvider provider = GetFunctionProvider(providerName);

                foreach (IWidgetFunction widgetFunction in provider.Functions)
                {
                    widgetFunctions.Add(new WidgetFunctionWrapper(widgetFunction));
                }

                return widgetFunctions;
            }
        }



        public static IEnumerable<IWidgetFunction> DynamicTypeDependentFunctions(string providerName)
        {
            using (_resourceLocker.Locker)
            {
                List<IWidgetFunction> widgetFunctions = new List<IWidgetFunction>();

                IDynamicTypeWidgetFunctionProvider provider = GetFunctionProvider(providerName) as IDynamicTypeWidgetFunctionProvider;

                if (provider != null)
                {
                    foreach (IWidgetFunction widgetFunction in provider.DynamicTypeDependentFunctions)
                    {
                        widgetFunctions.Add(new WidgetFunctionWrapper(widgetFunction));
                    }
                }

                return widgetFunctions;
            }
        }



        private static IWidgetFunctionProvider GetFunctionProvider(string providerName)
        {
            IWidgetFunctionProvider widgetFunctionProvider;

            using (_resourceLocker.Locker)
            {
                if (_resourceLocker.Resources.ProviderCache.TryGetValue(providerName, out widgetFunctionProvider) == false)
                {
                    try
                    {
                        widgetFunctionProvider = _resourceLocker.Resources.Factory.Create(providerName);

                        widgetFunctionProvider.WidgetFunctionNotifier = new WidgetFunctionNotifier(providerName);

                        _resourceLocker.Resources.ProviderCache.Add(providerName, widgetFunctionProvider);
                    }
                    catch (ArgumentException ex)
                    {
                        HandleConfigurationError(ex);
                    }
                    catch (ConfigurationErrorsException ex)
                    {
                        HandleConfigurationError(ex);
                    }
                }
            }

            return widgetFunctionProvider;
        }



        private static void Flush()
        {
            _resourceLocker.ResetInitialization();
        }



        private static void OnFlushEvent(FlushEventArgs args)
        {
            Flush();
        }



        private static void HandleConfigurationError(Exception ex)
        {
            Flush();

            throw new ConfigurationErrorsException(string.Format("Failed to load the configuration section '{0}' from the configuration.", WidgetFunctionProviderSettings.SectionName), ex);
        }



        private sealed class Resources
        {
            public WidgetFunctionProviderFactory Factory { get; set; }
            public Dictionary<string, IWidgetFunctionProvider> ProviderCache { get; set; }

            public static void Initialize(Resources resources)
            {
                try
                {
                    resources.Factory = new WidgetFunctionProviderFactory();
                }
                catch (NullReferenceException ex)
                {
                    HandleConfigurationError(ex);
                }
                catch (ConfigurationErrorsException ex)
                {
                    HandleConfigurationError(ex);
                }


                resources.ProviderCache = new Dictionary<string, IWidgetFunctionProvider>();
            }
        }
    }
}
