/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Composite.C1Console.Security;


namespace Composite.Functions.Foundation.PluginFacades
{
    /// <summary>
    /// This class is used for caching exceptions from plugins and hadling them correcty
    /// </summary>
    internal sealed class WidgetFunctionWrapper : IWidgetFunction, IFunctionInitializationInfo
    {
        private IWidgetFunction _widgetFunctionToWrap;


        internal WidgetFunctionWrapper(IWidgetFunction widgetFunctionToWrap)
        {
            _widgetFunctionToWrap = widgetFunctionToWrap;
        }



        public string Name
        {
            get
            {
                return _widgetFunctionToWrap.Name;
            }
        }



        public string Namespace
        {
            get
            {
                return _widgetFunctionToWrap.Namespace;
            }
        }


        public string Description { get { return _widgetFunctionToWrap.Description; } }


        public Type ReturnType
        {
            get
            {
                return _widgetFunctionToWrap.ReturnType;
            }
        }



        public IEnumerable<ParameterProfile> ParameterProfiles
        {
            get
            {
                if (_widgetFunctionToWrap.ParameterProfiles != null)
                {
                    return _widgetFunctionToWrap.ParameterProfiles;
                }

                return new ParameterProfile[] { };
            }
        }



        public XElement GetWidgetMarkup(ParameterList parameters, string label, HelpDefinition help, string bindingSourceName)
        {
            return _widgetFunctionToWrap.GetWidgetMarkup(parameters, label, help, bindingSourceName);
        }



        public EntityToken EntityToken
        {
            get
            {
                return _widgetFunctionToWrap.EntityToken;
            }
        }


        bool IFunctionInitializationInfo.FunctionInitializedCorrectly
        {
            get
            {
                if (!(_widgetFunctionToWrap is IFunctionInitializationInfo))
                {
                    return true;
                }
                return ((IFunctionInitializationInfo)_widgetFunctionToWrap).FunctionInitializedCorrectly;
            }
        }
    }
}
