/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System.Collections.Generic;
using Composite.Core.Collections.Generic;
using Composite.Functions.Foundation.PluginFacades.Runtime;
using Composite.Functions.Plugins.XslExtensionsProvider;
using Composite.Core.Types;

namespace Composite.Functions.Foundation.PluginFacades
{
	internal static class XslExtensionsProviderPluginFacade
	{
        static Resources _resources = new Resources();

        static XslExtensionsProviderPluginFacade()
        {
            Resources.Initialize(_resources);
        }


	    public static List<Pair<string, object>> CreateExtensions(string providerName)
	    {
	        return GetProvider(providerName).CreateExtensions();
	    }

        private static IXslExtensionsProvider GetProvider(string providerName)
        {
            Resources resources = _resources;

            IXslExtensionsProvider provider = resources.ProviderCache[providerName];
            if(provider != null)
            {
                return provider;
            }

            lock(resources.SyncRoot)
            {
                provider = resources.ProviderCache[providerName];
                if (provider != null)
                {
                    return provider;
                }

                provider = resources.Factory.Create(providerName);
                resources.ProviderCache[providerName] = provider;
            }

            return provider;
        }

        private sealed class Resources
        {
            public readonly object SyncRoot = new object();
            public readonly XslExtensionsProviderFactory Factory = new XslExtensionsProviderFactory();
            public Hashtable<string, IXslExtensionsProvider> ProviderCache { get; set; }

            public static void Initialize(Resources resources)
            {
                resources.ProviderCache = new Hashtable<string, IXslExtensionsProvider>();
            }
        }
	}
}
