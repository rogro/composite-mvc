/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Composite.Core.Configuration;
using Composite.Functions.Foundation.PluginFacades;
using Composite.Functions.Plugins.WidgetFunctionProvider.Runtime;
using Composite.Core.Logging;


namespace Composite.Functions.Foundation
{
    internal sealed class WidgetFunctionContainer : MetaFunctionContainer
    {
        internal WidgetFunctionContainer(List<string> excludedFunctionNames)
            : base(excludedFunctionNames)
        {
        }


        protected override IEnumerable<string> OnGetProviderNames()
        {
            if ((ConfigurationServices.ConfigurationSource != null) &&
                (ConfigurationServices.ConfigurationSource.GetSection(WidgetFunctionProviderSettings.SectionName) != null))
            {
                WidgetFunctionProviderSettings functionProviderSettings = ConfigurationServices.ConfigurationSource.GetSection(WidgetFunctionProviderSettings.SectionName) as WidgetFunctionProviderSettings;

                return functionProviderSettings.WidgetFunctionProviderPlugins.Select(plugin => plugin.Name);
            }
            else
            {
                LoggingService.LogError("FunctionProviderRegistry", string.Format("Failed to load the configuration section '{0}' from the configuration", WidgetFunctionProviderSettings.SectionName));

                return new List<string>();
            }
        }



        protected override IEnumerable<IMetaFunction> OnGetFunctionsFromProvider(string providerName, FunctionTypesToReturn functionTypesToReturn)
        {
            IEnumerable<IMetaFunction> functions = new List<IMetaFunction>();

            switch (functionTypesToReturn)
            {
                case FunctionTypesToReturn.StaticDependentFunctions:
                    try
                    {
                        functions = WidgetFunctionProviderPluginFacade.Functions(providerName).Cast<IMetaFunction>();
                    }
                    catch (Exception ex)
                    {
                        LoggingService.LogCritical("FunctionProviderRegistry", ex);
                    }
                    break;
                case FunctionTypesToReturn.DynamicDependentOnlyFunctions:
                    try
                    {
                        functions = WidgetFunctionProviderPluginFacade.DynamicTypeDependentFunctions(providerName).Cast<IMetaFunction>();
                    }
                    catch (Exception ex)
                    {
                        LoggingService.LogCritical("FunctionProviderRegistry", ex);
                    }
                    break;
                case FunctionTypesToReturn.AllFunctions:
                    try
                    {
                        functions = WidgetFunctionProviderPluginFacade.Functions(providerName).Cast<IMetaFunction>();
                    }
                    catch (Exception ex)
                    {
                        LoggingService.LogCritical("FunctionProviderRegistry", ex);
                    }

                    try
                    {
                        functions = functions.Concat(WidgetFunctionProviderPluginFacade.DynamicTypeDependentFunctions(providerName).Cast<IMetaFunction>());
                    }
                    catch (Exception ex)
                    {
                        LoggingService.LogCritical("FunctionProviderRegistry", ex);
                    }
                    break;
                default:
                    throw new NotImplementedException();
            }

            return functions;
        }



        protected override void OnFunctionsAdded(List<string> functionNames, bool fireEvents)
        {
            if (fireEvents)
            {
                FunctionEventSystemFacade.FireWidgetFunctionAddedEvent(new WidgetFunctionsAddedEventArgs(functionNames));
            }
        }



        protected override void OnFunctionsRemoved(List<string> functionNames)
        {
            FunctionEventSystemFacade.FireWidgetFunctionRemovedEvent(new WidgetFunctionsRemovedEventArgs(functionNames));
        }



        protected override string FunctionType
        {
            get { return "WidgetFunction"; }
        }
    }
}
