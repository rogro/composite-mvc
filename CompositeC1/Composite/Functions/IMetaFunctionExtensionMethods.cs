/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;
using Composite.Core.Extensions;
using Composite.Core.ResourceSystem;


namespace Composite.Functions
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
	public static class IMetaFunctionExtensionMethods
	{
        /// <exclude />
        public static string CompositeName(this IMetaFunction metaFunction)
        {
            return CompositeName(metaFunction.Namespace, metaFunction.Name);
        }



        /// <exclude />
        public static string CompositeName(string namespaceName, string name)
        {
            return StringExtensionMethods.CreateNamespace(namespaceName, name, '.');
        }



        /// <exclude />
        public static bool IsNamespaceCorrectFormat(this IMetaFunction metaFunction)
        {
            if (metaFunction.Namespace == "") return true;

            if (metaFunction.Namespace.StartsWith(".")
                || metaFunction.Namespace.EndsWith("."))
            {
                return false;
            }

            string[] splits = metaFunction.Namespace.Split('.');
            foreach (string split in splits)
            {
                if (split == "") return false;
            }

            return true;
        }



        /// <exclude />
        public static bool ValidateParameterProfiles(this IMetaFunction metaFunction)
        {
            List<string> names = new List<string>();

            foreach (ParameterProfile parameterProfile in metaFunction.ParameterProfiles)
            {
                if (names.Contains(parameterProfile.Name))
                {
                    return false;
                }
                names.Add(parameterProfile.Name);
            }

            return true;
        }



        /// <exclude />
        public static string DescriptionLocalized(this IMetaFunction function)
        {
            if (function.Description != null && function.Description.Contains("${"))
            {
                return StringResourceSystemFacade.ParseString(function.Description);
            }
            
            return function.Description;
        }

	}
}
