/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Xml.Linq;
using System.Collections.Generic;
using Composite.Core.Types;


namespace Composite.Functions
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public abstract class BaseRuntimeTreeNode
    {
        internal BaseRuntimeTreeNode() { }

        /// <exclude />
        public object GetValue()
        {
            FunctionContextContainer internalContextContainer = new FunctionContextContainer();
            return GetValue(internalContextContainer);
        }


        /// <exclude />
        public abstract object GetValue(FunctionContextContainer contextContainer);


        /// <exclude />
        public T GetValue<T>()
        {
            FunctionContextContainer internalContextContainer = new FunctionContextContainer();
            return GetValue<T>(internalContextContainer);
        }


        /// <exclude />
        public virtual object GetValue(FunctionContextContainer contextContainer, Type type)
        {
            Verify.ArgumentNotNull(contextContainer, "contextContainer");

            object value = this.GetValue(contextContainer);

            if (value == null || type.IsInstanceOfType(value))
            {
                return value;
            }
            
            return ValueTypeConverter.Convert(value, type);
        }


        /// <exclude />
        public T GetValue<T>(FunctionContextContainer contextContainer)
        {
            return (T) GetValue(contextContainer, typeof(T));
        }

        /// <exclude />
        public object GetCachedValue()
        {
            FunctionContextContainer internalContextContainer = new FunctionContextContainer();
            return GetValue(internalContextContainer);
        }

        /// <exclude />
        public abstract IEnumerable<string> GetAllSubFunctionNames();

        /// <exclude />
        public abstract bool ContainsNestedFunctions { get; }

        /// <exclude />
        public abstract XElement Serialize();
    }
}
