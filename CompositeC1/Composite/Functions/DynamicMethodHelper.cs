/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Reflection;
using System.Reflection.Emit;

namespace Composite.Functions
{
    internal class DynamicMethodHelper
    {
        public delegate object FunctionCall(Func<object> body);

        private static readonly MethodInfo FuncObject_InvokeMethodInfo = typeof(Func<object>).GetMethod("Invoke");

        /// <summary>
        /// Creates a dynamic method with the specified name. To be used for adding information into StackTrace.
        /// </summary>
        public static FunctionCall GetDynamicMethod(string methodName)
        {
            var wrapper = new DynamicMethod(
                methodName,
                typeof(object),
                new[] { typeof(Func<object>) },
                typeof(FunctionFacade).Module);

            var wrapperBody = wrapper.GetILGenerator();

            wrapperBody.Emit(OpCodes.Ldarg_0);
            wrapperBody.EmitCall(OpCodes.Callvirt, FuncObject_InvokeMethodInfo, null);
            wrapperBody.Emit(OpCodes.Ret);

            return (FunctionCall)wrapper.CreateDelegate(typeof(FunctionCall));
        }
    }
}
