/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using Composite.Core;
using Composite.Core.Types;
using Composite.Data;
using Composite.Core.ResourceSystem;
using Composite.Core.Extensions;


namespace Composite.Functions
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
	public sealed class ParameterProfile
	{
        private readonly WidgetFunctionProvider _widgetFuntionProvider;
        private Dictionary<string, object> _widgetFunctionRuntimeParameters;

        /// <exclude />
        public ParameterProfile(string name, Type type, bool isRequired, BaseValueProvider fallbackValueProvider, WidgetFunctionProvider widgetFunctionProvider, string label, HelpDefinition helpDefinition)
            : this(name, type, isRequired, fallbackValueProvider, widgetFunctionProvider, label, helpDefinition, false)
        {
        }

        /// <exclude />
        public ParameterProfile(
            string name, 
            Type type, 
            bool isRequired, 
            BaseValueProvider fallbackValueProvider, 
            WidgetFunctionProvider widgetFunctionProvider, 
            string label, 
            HelpDefinition helpDefinition, 
            bool hideInSimpleView)
        {
            Verify.ArgumentNotNull(name, "name");
            Verify.ArgumentNotNull(type, "type");
            Verify.ArgumentNotNull(fallbackValueProvider, "fallbackValueProvider");
            Verify.ArgumentCondition(!label.IsNullOrEmpty(), "label", "label may not be null or an empty string");
            Verify.ArgumentNotNull(helpDefinition, "helpDefinition");

            this.Name = name;
            this.Type = type;
            this.IsRequired = isRequired && (!type.IsGenericType || type.GetGenericTypeDefinition() != typeof(NullableDataReference<>));
            this.FallbackValueProvider = fallbackValueProvider;
            _widgetFuntionProvider = widgetFunctionProvider;
            this.Label = label;
            this.HelpDefinition = helpDefinition;
            this.HideInSimpleView = hideInSimpleView;
        }


        /// <exclude />
        public ParameterProfile(
            string name, Type type, bool isRequired, BaseValueProvider fallbackValueProvider, WidgetFunctionProvider widgetFunctionProvider, 
            Dictionary<string,object> widgetFunctionRuntimeParameters, string label, HelpDefinition helpDefinition)
            : this( name, type, isRequired, fallbackValueProvider, widgetFunctionProvider, label, helpDefinition )
        {
            _widgetFunctionRuntimeParameters = widgetFunctionRuntimeParameters;
        }


        /// <exclude />
        public string Name { get; private set; }

        /// <exclude />
        public Type Type { get; private set; }


        /// <exclude />
        public bool IsRequired { get; private set; }

        /// <exclude />
        public BaseValueProvider FallbackValueProvider { get; private set; }

        /// <exclude />
        public bool HideInSimpleView { get; internal set; }

        /// <exclude />
        public IWidgetFunction WidgetFunction 
        {
            get
            {
                return _widgetFuntionProvider != null ? _widgetFuntionProvider.WidgetFunction : null;
            }
        }


        /// <exclude />
        public IEnumerable<BaseParameterRuntimeTreeNode> WidgetFunctionParameters
        {
            get
            {
                return _widgetFuntionProvider.WidgetFunctionParameters;
            }
        }

//#warning Kill this?
//        public Dictionary<string,object> WidgetFunctionRuntimeParameters 
//        {
//            get
//            {
//                if (_widgetFunctionRuntimeParameters == null) return null;

//                Dictionary<string, object> parameters = new Dictionary<string, object>(_widgetFunctionRuntimeParameters);
//                foreach (BaseParameterRuntimeTreeNode param in _widgetFuntionProvider.WidgetFunctionParameters)
//                {
//                    if (parameters.ContainsKey(param.Name) == false)
//                    {
//                        parameters.Add(param.Name, param.GetValue());
//                    }
//                }

//                return parameters;
//            }
//        }

        /// <exclude />
        public string Label{ get; private set; }

        /// <exclude />
        public HelpDefinition HelpDefinition { get; private set; }


        /// <exclude />
        public object GetDefaultValue()
        {
            // Initializing the binding
            object value = null;

            try
            {
                var fallbackValueProvider = FallbackValueProvider;

                if (!(fallbackValueProvider is NoValueValueProvider))
                {
                    object defaultValue = fallbackValueProvider.GetValue();

                    if (defaultValue != null)
                    {
                        value = ValueTypeConverter.Convert(defaultValue, this.Type);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.LogWarning(typeof(ParameterProfile).Name, ex);
            }

            if (value == null)
            {
                if (this.Type == typeof(bool))
                {
                    value = false;
                }
            }
            return value;
        }

        /// <exclude />
        public string LabelLocalized
        {
            get
            {
                return this.Label.StartsWith("${") ? StringResourceSystemFacade.ParseString(this.Label) : this.Label;
            }
        }
	}
}
