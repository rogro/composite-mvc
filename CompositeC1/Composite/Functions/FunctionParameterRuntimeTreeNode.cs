/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Threading;
using System.Xml.Linq;
using System.Collections.Generic;
using Composite.Functions.Foundation;


namespace Composite.Functions
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public sealed class FunctionParameterRuntimeTreeNode : BaseParameterRuntimeTreeNode
    {
        private FunctionRuntimeTreeNode _functionNode;

        /// <exclude />
        public FunctionParameterRuntimeTreeNode(string name, FunctionRuntimeTreeNode functionNode)
            : base(name)
        {
            _functionNode = functionNode;
        }



        /// <exclude />
        public FunctionRuntimeTreeNode GetHostedFunction()
        {
            return _functionNode;
        }



        /// <exclude />
        public override bool ContainsNestedFunctions
        {
            get
            {
                return _functionNode.ContainsNestedFunctions;
            }
        }


        /// <exclude />
        public override object GetValue(FunctionContextContainer contextContainer)
        {
            Verify.ArgumentNotNull(contextContainer, "contextContainer");

            return _functionNode.GetValue(contextContainer);
        }


        /// <exclude />
        public override IEnumerable<string> GetAllSubFunctionNames()
        {
            return _functionNode.GetAllSubFunctionNames();
        }


        /// <exclude />
        public override XElement Serialize()
        {
            // ensure "f:function" naming:
            XElement element = XElement.Parse(string.Format(@"<f:{0} xmlns:f=""{1}"" />", FunctionTreeConfigurationNames.ParamTagName, FunctionTreeConfigurationNames.NamespaceName));

            element.Add(new XAttribute(FunctionTreeConfigurationNames.NameAttributeName, this.Name));

            element.Add(_functionNode.Serialize());

            return element;
        }
    }
}
