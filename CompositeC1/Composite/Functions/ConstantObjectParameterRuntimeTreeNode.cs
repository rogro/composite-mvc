/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using Composite.Core.Xml;
using Composite.Functions.Foundation;


namespace Composite.Functions
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
	public sealed class ConstantObjectParameterRuntimeTreeNode : BaseParameterRuntimeTreeNode
    {
        private readonly object _constantValue;



        /// <exclude />
        public ConstantObjectParameterRuntimeTreeNode(string name, object constantValue)
            : base(name)
        {
            _constantValue = constantValue;
        }


        /// <exclude />
        public override object GetValue(FunctionContextContainer contextContainer)
        {
            return _constantValue;
        }


        /// <exclude />
        public override IEnumerable<string> GetAllSubFunctionNames()
        {
            return new string[0];
        }


        /// <exclude />
        public override bool ContainsNestedFunctions
        {
            get
            {
                return false;
            }
        }


        /// <exclude />
        public override XElement Serialize()
        {
            var element = new XElement(FunctionTreeConfigurationNames.ParamTag, 
                new XAttribute(FunctionTreeConfigurationNames.NameAttribute, this.Name));

            if (_constantValue is IEnumerable && !(_constantValue is string))
            {
                foreach (object obj in (IEnumerable)_constantValue)
                {
                    element.Add(new XElement(FunctionTreeConfigurationNames.ParamElementTag,
                            new XAttribute(FunctionTreeConfigurationNames.ValueAttribute, 
                                           XmlSerializationHelper.GetSerializableObject(obj))));
                }

                return element;
            }

            object xValue;
            if (_constantValue is XNode)
            {
                if (_constantValue is XDocument)
                {
                    xValue = ((XDocument) _constantValue).Root;
                }
                else
                {
                    xValue = _constantValue;
                }
            }
            else
            {
                xValue = new XAttribute(FunctionTreeConfigurationNames.ValueAttributeName, 
                        XmlSerializationHelper.GetSerializableObject(_constantValue) ?? string.Empty);
            }

            element.Add(xValue);

            return element;
        }
    }
}
