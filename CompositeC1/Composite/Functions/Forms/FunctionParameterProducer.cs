/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Composite.C1Console.Forms;


namespace Composite.Functions.Forms
{
    [ControlValueProperty("Producers")]
	internal sealed class FunctionParameterProducer : IFunctionProducer 
	{
        public FunctionParameterProducer()
        {
            this.Producers = new List<BaseRuntimeTreeNode>();
        }


        [RequiredValue]
        public string name { get; set; }

        
        public object value { get; set; }


        [FormsProperty]
        public List<BaseRuntimeTreeNode> Producers { get; private set; }



        public object GetResult()
        {
            if (this.value != null)
            {
                if (this.value is FunctionRuntimeTreeNode)
                {
                    return new FunctionParameterRuntimeTreeNode(this.name, this.value as FunctionRuntimeTreeNode);
                }
                if (this.value is string)
                {
                    return new ConstantParameterRuntimeTreeNode(this.name, this.value as string);
                }

                throw new NotImplementedException();
            }

            if (Producers.Count == 1)
            {
                var functionNode = Producers[0] as FunctionRuntimeTreeNode;
                if (functionNode != null)
                {
                    return new FunctionParameterRuntimeTreeNode(this.name, functionNode);
                }

                var constantObjectNode = Producers[0] as ConstantObjectParameterRuntimeTreeNode;
                if (constantObjectNode != null)
                {
                    return new ConstantObjectParameterRuntimeTreeNode(this.name, constantObjectNode.GetValue());
                }
            }

            if (Producers.All(p => p is ConstantParameterRuntimeTreeNode))
            {
                IEnumerable<string> values = Producers.OfType<ConstantParameterRuntimeTreeNode>().Select(f => (string)f.GetValue());

                return new ConstantParameterRuntimeTreeNode(this.name, values);                
            }

            throw new NotImplementedException();
        }
	}
}
