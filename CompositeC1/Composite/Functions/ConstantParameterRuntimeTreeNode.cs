/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Composite.Core.Types;
using Composite.Core.Xml;
using Composite.Functions.Foundation;

namespace Composite.Functions
{
    internal sealed class ConstantParameterRuntimeTreeNode : BaseParameterRuntimeTreeNode
    {
        private readonly object _constantValue;
        private readonly bool _isEnumerable;
        private readonly XAttribute _attribute;


        public ConstantParameterRuntimeTreeNode(string name, string constantValue)
            : base(name)
        {
            _constantValue = constantValue;
        }

        public ConstantParameterRuntimeTreeNode(string name, XAttribute valueAttribute)
            : base(name)
        {
            _attribute = valueAttribute;
        }


        public ConstantParameterRuntimeTreeNode(string name, IEnumerable<string> constantValue)
            : base(name)
        {
            _constantValue = constantValue;
            _isEnumerable = true;
        }



        public override object GetValue(FunctionContextContainer contextContainer)
        {
            Verify.ArgumentNotNull(contextContainer, "contextContainer");

            return _attribute != null ? _attribute.Value : _constantValue;
        }

        public override object GetValue(FunctionContextContainer contextContainer, Type type)
        {
            Verify.ArgumentNotNull(contextContainer, "contextContainer");
            Verify.ArgumentNotNull(type, "type");

            if (_attribute != null)
            {
                return XmlSerializationHelper.Deserialize(_attribute, type);
            }

            return ValueTypeConverter.Convert(_constantValue, type);
        }


        public override IEnumerable<string> GetAllSubFunctionNames()
        {
            yield break;
        }


        public override bool ContainsNestedFunctions
        {
            get
            {
                return false;
            }
        }



        public override XElement Serialize()
        {
            var element = new XElement(FunctionTreeConfigurationNames.ParamTag,
                        new XAttribute(FunctionTreeConfigurationNames.NameAttribute, this.Name));

            if (_isEnumerable)
            {
                var strings = (IEnumerable<string>) _constantValue;
                foreach (string s in strings)
                {
                    element.Add(new XElement(FunctionTreeConfigurationNames.ParamElementTag,
                            new XAttribute(FunctionTreeConfigurationNames.ValueAttribute, s)));
                }

                return element;
            }

            if (_attribute != null)
            {
                element.Add(new XAttribute(FunctionTreeConfigurationNames.ValueAttribute, _attribute.Value));
            }

            if (_constantValue != null)
            {
                element.Add(new XAttribute(FunctionTreeConfigurationNames.ValueAttribute, _constantValue));
            }

            return element;
        }
    }
}
