/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;


namespace Composite.Functions
{
    /// <summary>
    /// Add information about parameters to functions callable via the "C# Function" feature. 
    /// </summary>
    /// <remarks>
    /// This class has been marked as obsolete. Use <see cref="FunctionParameterAttribute" />.
    /// </remarks>
    /// <example>
    /// Here is an example of how to use <see cref="FunctionParameterDescriptionAttribute" />
    /// <code>
    /// [FunctionParameterDescription("searchTerm", "Search term", "One or more keywords to search for")]
    /// [FunctionParameterDescription("filter", "Filter", "Filter to apply to data before searching for search term", null)]
    /// public static int GetItemCount( string searchTerm, Expression&lt;Func&lt;IMyDataType,bool&gt;&gt; filter ) 
    /// { 
    ///     if (filter == null ) filter = _defaultFilter;
    ///     // more code here
    /// }
    /// </code>
    /// </example>
    /// <exclude />
    [Obsolete("Use FunctionParameterAttribute instead ...", false)]
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
	public sealed class FunctionParameterDescriptionAttribute : Attribute
	{
        /// <summary>
        /// Describe a function parameter for use in the C1 Function system.
        /// </summary>
        /// <param name="parameterName">The programmatic name of the parameter</param>
        /// <param name="parameterLabel">Human readable label</param>
        /// <param name="parameterHelpText">Human readable help text</param>
        public FunctionParameterDescriptionAttribute(string parameterName, string parameterLabel, string parameterHelpText)
        {
            this.ParameterName = parameterName;
            this.ParameterLabel = parameterLabel;
            this.ParameterHelpText = parameterHelpText;
            this.HasDefaultValue = false;
        }

        /// <summary>
        /// Describe a function parameter for use in the C1 Function system.
        /// </summary>
        /// <param name="parameterName">The programmatic name of the parameter</param>
        /// <param name="parameterLabel">Human readable label</param>
        /// <param name="parameterHelpText">Human readable help text</param>
        /// <param name="defaultValue">Optional. Default value that should be assigned to the parameter if not specified by the caller. You can use 'null' for complex objects that can not be expressed in attribute code and the check for null in the code.</param>
        public FunctionParameterDescriptionAttribute(string parameterName, string parameterLabel, string parameterHelpText, object defaultValue)
            : this(parameterName, parameterLabel, parameterHelpText)
        {
            this.DefaultValue = defaultValue;
            this.HasDefaultValue = true;
        }


        /// <summary>
        /// The name of the function parameter being described. This should match the parameter name in the method.
        /// </summary>
        public string ParameterName
        {
            get;
            private set;
        }

        /// <summary>
        /// Optional. Default value that should be assigned to the parameter if not specified by the caller. You can use 'null' for complex objects that can not be expressed in attribute code and the check for null in the code.
        /// </summary>
        public object DefaultValue
        {
            get;
            private set;
        }


        /// <summary>
        /// Human readable label for this parameter
        /// </summary>
        public string ParameterLabel
        {
            get;
            private set;
        }


        /// <summary>
        /// Human readable help for this parameter
        /// </summary>
        public string ParameterHelpText
        {
            get;
            private set;
        }


        /// <summary>
        /// Indicate if this parameter definition has a default value or not.
        /// </summary>
        public bool HasDefaultValue
        {
            get;
            private set;
        }
    }
}
