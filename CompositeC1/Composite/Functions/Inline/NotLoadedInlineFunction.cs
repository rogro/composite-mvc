/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.IO;
using Composite.Core.Configuration;
using Composite.Core.Extensions;
using Composite.Core.IO;
using Composite.Core.Xml;
using Composite.Data;
using Composite.Data.Types;

namespace Composite.Functions.Inline
{
    internal class NotLoadedInlineFunction : IFunction, IFunctionInitializationInfo
    {
        private readonly IInlineFunction _function;
        private string[] _sourceCode;

        private readonly StringInlineFunctionCreateMethodErrorHandler _errors;

        public NotLoadedInlineFunction(IInlineFunction functionInfo, StringInlineFunctionCreateMethodErrorHandler errors)
        {
            Verify.ArgumentCondition(errors.HasErrors, "errors", "No errors information provided");

            _function = functionInfo;
            _errors = errors;
        }

        object IFunction.Execute(ParameterList parameters, FunctionContextContainer context)
        {
            if (_errors.CompileErrors.Count > 0)
            {
                var error = _errors.CompileErrors[0];
                var exception = new InvalidOperationException("{1} Line {0}: {2}".FormatWith(error.Item1, error.Item2, error.Item3));

                if (_sourceCode == null)
                {
                    string filepath = Path.Combine(PathUtil.Resolve(GlobalSettingsFacade.InlineCSharpFunctionDirectory), _function.CodePath);

                    if (C1File.Exists(filepath))
                    {
                        _sourceCode = C1File.ReadAllLines(filepath);
                    }
                    else
                    {
                        _sourceCode = new string[0];
                    }
                }
                
                if (_sourceCode.Length > 0)
                {
                    XhtmlErrorFormatter.EmbedSouceCodeInformation(exception, _sourceCode, error.Item1);
                }

                throw exception;
            }

            if (_errors.LoadingException != null)
            {
                throw _errors.LoadingException;
            }

            throw new InvalidOperationException("Function wasn't loaded due to compilation errors");
        }

        public string Name
        {
            get { return _function.Name; }
        }


        public string Namespace
        {
            get { return _function.Namespace; }
        }

        public string Description
        {
            get
            {
                if (_errors.CompileErrors.Count > 0)
                {
                    var error = _errors.CompileErrors[0];
                    return "{1} Line {0}: {2}".FormatWith(error.Item1, error.Item2, error.Item3);
                }

                if (_errors.LoadingException != null)
                {
                    return _errors.LoadingException.Message;
                }

                return _function.Description;
            }
        }

        Type IMetaFunction.ReturnType
        {
            get { return typeof(void); }
        }

        IEnumerable<ParameterProfile> IMetaFunction.ParameterProfiles
        {
            get
            {
                return new ParameterProfile[0];
            }
        }

        bool IFunctionInitializationInfo.FunctionInitializedCorrectly
        {
            get { return false; }
        }


        public C1Console.Security.EntityToken EntityToken
        {
            get { return _function.GetDataEntityToken(); }
        }
    }
}
