/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;


namespace Composite.Functions.Inline
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    public class StringInlineFunctionCreateMethodErrorHandler : InlineFunctionCreateMethodErrorHandler
    {
        bool _hasErrors;

        /// <exclude />
        public StringInlineFunctionCreateMethodErrorHandler()
        {
            _hasErrors = false;
            this.CompileErrors = new List<Tuple<int, string, string>>();
        }


        /// <exclude />
        public List<Tuple<int, string, string>> CompileErrors { get; set; }

        /// <exclude />
        public string MissingContainerType { get; set; }

        /// <exclude />
        public string NamespaceMismatch { get; set; }

        /// <exclude />
        public string MissionMethod { get; set; }

        /// <exclude />
        public Exception LoadingException { get; set; }

        /// <exclude />
        public override bool HasErrors { get { return _hasErrors; } }


        /// <exclude />
        public override void OnCompileError(int line, string errorNumber, string message)
        {
            _hasErrors = true;
            this.CompileErrors.Add(new Tuple<int, string, string>(line, errorNumber, message));
        }


        /// <exclude />
        public override void OnMissingContainerType(string message)
        {
            _hasErrors = true;
            this.MissingContainerType = message;
        }


        /// <exclude />
        public override void OnNamespaceMismatch(string message)
        {
            _hasErrors = true;
            this.NamespaceMismatch = message;
        }


        /// <exclude />
        public override void OnMissionMethod(string message)
        {
            _hasErrors = true;
            this.MissionMethod = message;
        }


        /// <exclude />
        public override void OnLoadSourceError(Exception exception)
        {
            _hasErrors = true;
            LoadingException = exception;
        }
    }
}
