/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;
using Composite.Functions.Foundation;
using Composite.Core.Xml;


namespace Composite.Functions
{
	internal sealed class XElementParameterRuntimeTreeNode : BaseParameterRuntimeTreeNode
    {
        private XElement _element;

        private XElement ExecuteInnerFunctions(FunctionContextContainer contextContainer)
        {
            XElement resultRoot = new XElement(_element);
            int loopCount = 0;

            while (true)
            {
                XName functionXName = Namespaces.Function10 + FunctionTreeConfigurationNames.FunctionTagName;
                IEnumerable<XElement> nestedFunctionCalls = resultRoot.Descendants(functionXName).Where(f => !f.Ancestors(functionXName).Any());
                var evaluatedListOfInnerFunctions = nestedFunctionCalls.ToList();

                if (!evaluatedListOfInnerFunctions.Any())
                {
                    break;
                }

                if (loopCount++ > 1000)
                {
                    throw new InvalidOperationException("One or more function seems to be returning markup generating endless recursion. The following markup seems to generate the problem: " + evaluatedListOfInnerFunctions.First().ToString());
                }

                var functionCallResults = new object[evaluatedListOfInnerFunctions.Count];

                
                for(int i=0; i<evaluatedListOfInnerFunctions.Count; i++)
                {
                    XElement functionCallDefinition = evaluatedListOfInnerFunctions[i];

                    BaseRuntimeTreeNode runtimeTreeNode = FunctionTreeBuilder.Build(functionCallDefinition);

                    functionCallResults[i] = runtimeTreeNode.GetValue(contextContainer);
                };

                for (int i = 0; i < evaluatedListOfInnerFunctions.Count; i++)
                {
                    object embedableResult = contextContainer.MakeXEmbedable(functionCallResults[i]);

                    if (embedableResult is XAttribute)
                    {
                        evaluatedListOfInnerFunctions[i].Parent.Add(embedableResult);
                        evaluatedListOfInnerFunctions[i].Remove();
                    }
                    else
                    {
                        evaluatedListOfInnerFunctions[i].ReplaceWith(embedableResult);
                    }
                }
            }

            return resultRoot;
        }


        public XElementParameterRuntimeTreeNode(string name, XElement element)
            : base(name)
        {
            _element = element;
        }


        public override object GetValue(FunctionContextContainer contextContainer)
        {
            Verify.ArgumentNotNull(contextContainer, "contextContainer");

            return ExecuteInnerFunctions(contextContainer);
        }


        public override IEnumerable<string> GetAllSubFunctionNames()
        {
            return
                from nameAttribute in _element.Elements(Namespaces.Function10 + FunctionTreeConfigurationNames.FunctionTagName).Attributes( FunctionTreeConfigurationNames.NameAttributeName )
                select nameAttribute.Value;
        }



        public override bool ContainsNestedFunctions
        {
            get
            {
                return GetAllSubFunctionNames().Any();
            }
        }



        public XElement GetHostedXElement()
        {
            return _element;
        }

        public override XElement Serialize()
        {
            XElement element =
                new XElement(XName.Get(FunctionTreeConfigurationNames.ParamTagName, FunctionTreeConfigurationNames.NamespaceName),
                    new XAttribute(FunctionTreeConfigurationNames.NameAttributeName, this.Name),
                    _element
                );

            return element;
        }
    }
}
