/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;


namespace Composite.Functions
{
    /// <summary>
    /// Adds a desctiotion about a C1 Function.
    /// 
    /// Different C1 Function providers let developers create new C1 Functions by creating artifacts like static funtions in C#,
    /// Razor Web Pages, User Controls etc. These C1 Functions can have descriptions and this attribute let you control this
    /// description.
    /// 
    /// The use of this attribute is relative to the C1 Function provider being used.
    /// </summary>
    /// <example>
    /// Here is an example of how to use <see cref="FunctionAttribute" />:
    /// <code>
    /// [Function(Description="The description goes here")]
    /// public static int GetItemCount() 
    /// { 
    ///     // more code here
    /// }
    /// </code>
    /// </example>
    /// <example>
    /// Here is an example of how to use <see cref="FunctionAttribute" /> to annotate a class:
    /// <code>
    /// [FunctionParameter(Label="Item count", DefaultValue=10)]
    /// public int ItemCount { get; set; } 
    /// </code>
    /// Note that <see cref="P:Composite.Functions.FunctionParameterAttribute.Name" /> is not expected when <see cref="FunctionParameterAttribute" /> is used this way.
    /// </example>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = false)]
	public sealed class FunctionAttribute : Attribute
	{
        /// <summary>
        /// Describe a function for use in the C1 Function system. 
        /// </summary>
        public FunctionAttribute()
        {
        }


        /// <summary>
        /// The description of the function.
        /// </summary>
        public string Description
        {
            get;
            set;
        }
    }
}
