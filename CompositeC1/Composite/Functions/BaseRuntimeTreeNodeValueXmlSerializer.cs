/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using System.Xml.Linq;
using Composite.Core.Serialization;


namespace Composite.Functions
{
    internal sealed class BaseRuntimeTreeNodeValueXmlSerializer : IValueXmlSerializer
    {
        public bool TrySerialize(Type objectToSerializeType, object objectToSerialize, IXmlSerializer xmlSerializer, out XElement serializedObject)
        {
            if (objectToSerializeType == null) throw new ArgumentNullException("objectToSerializeType");

            serializedObject = null;

            if (typeof(BaseRuntimeTreeNode).IsAssignableFrom(objectToSerializeType))
            {
                serializedObject = new XElement("BaseRuntimeTreeNode");

                if (objectToSerialize != null)
                {
                    BaseRuntimeTreeNode baseRuntimeTreeNode = objectToSerialize as BaseRuntimeTreeNode;

                    serializedObject.Add(new XElement("Value", baseRuntimeTreeNode.Serialize()));
                }

                return true;
            }
            else
            {
                return false;
            }
        }



        public bool TryDeserialize(XElement serializedObject, IXmlSerializer xmlSerializer, out object deserializedObject)
        {
            if (serializedObject == null) throw new ArgumentNullException("serializedObject");

            deserializedObject = null;

            if (serializedObject.Name.LocalName != "BaseRuntimeTreeNode") return false;

            XElement valueElement = serializedObject.Element("Value");
            if (valueElement != null)
            {
                if (valueElement.Elements().Count() != 1) return false;

                deserializedObject = FunctionFacade.BuildTree(valueElement.Elements().Single());
            }

            return true;
        }
    }
}
