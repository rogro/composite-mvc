/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Composite.Plugins.Functions.FunctionProviders.StandardFunctionProvider.Constant;
using Composite.Core.Xml;
using Composite.Data;
using Composite.Core.Extensions;

namespace Composite.Functions
{
	/// <summary>    
	/// </summary>
	/// <exclude />
	[System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
	public static class StandardFunctions
	{

		/// <exclude />
		public static IFunction GetDefaultFunctionByType(Type type)
		{
			if (type == typeof (string)) return StandardFunctions.StringFunction;
			if (type == typeof (int) || type == typeof (int?)) return StandardFunctions.IntegerFunction;
			if (type == typeof (Decimal) || type == typeof (Decimal?)) return StandardFunctions.DecimalFunction;
			if (type == typeof (DateTime) || type == typeof (DateTime?)) return StandardFunctions.DateTimeFunction;
			if (type == typeof (Guid) || type == typeof (Guid?)) return StandardFunctions.GuidFunction;
			if (type == typeof (bool) || type == typeof (bool?)) return StandardFunctions.BooleanFunction;

			if (type == typeof (XhtmlDocument)) return StandardFunctions.XhtmlDocumentFunction;

			if (type.IsGenericType)
			{
				if (type.GetGenericTypeDefinition() == typeof (NullableDataReference<>))
				{
					var referenceType = type.GetGenericArguments().First();
					var functionName = StringExtensionMethods.CreateNamespace(referenceType.FullName, "GetNullableDataReference");
					IFunction function;
					if (FunctionFacade.TryGetFunction(out function, functionName))
						return function;
				}
				else if (type.GetGenericTypeDefinition() == typeof (DataReference<>))
				{
					var referenceType = type.GetGenericArguments().First();
					var functionName = StringExtensionMethods.CreateNamespace(referenceType.FullName, "GetDataReference");
					IFunction function;
					if (FunctionFacade.TryGetFunction(out function, functionName))
						return function;
				}
			}

			return null;
		}

		/// <exclude />
		public static IFunction StringFunction { get { return FunctionFacade.GetFunction("Composite.Constant.String"); } }

		/// <exclude />
		public static IFunction DateTimeFunction { get { return FunctionFacade.GetFunction("Composite.Utils.Date.Now"); } }

		/// <exclude />
		public static IFunction BooleanFunction { get { return FunctionFacade.GetFunction("Composite.Constant.Boolean"); } }

		/// <exclude />
		public static IFunction DecimalFunction { get { return FunctionFacade.GetFunction("Composite.Constant.Decimal"); } }

		/// <exclude />
		public static IFunction IntegerFunction { get { return FunctionFacade.GetFunction("Composite.Constant.Integer"); } }

		/// <exclude />
		public static IFunction GuidFunction { get { return FunctionFacade.GetFunction("Composite.Constant.Guid"); } }

		/// <exclude />
		public static IFunction XhtmlDocumentFunction { get { return FunctionFacade.GetFunction("Composite.Constant.XhtmlDocument"); } }
	}
}
