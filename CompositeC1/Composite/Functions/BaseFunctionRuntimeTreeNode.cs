/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Composite.Core.Extensions;

namespace Composite.Functions
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public abstract class BaseFunctionRuntimeTreeNode : BaseRuntimeTreeNode
    {
        /// <exclude />
        protected List<BaseParameterRuntimeTreeNode> Parameters { get; set; }

        /// <exclude />
        public void SetParameter(BaseParameterRuntimeTreeNode parameterRuntimeTreeNode)
        {
            if (parameterRuntimeTreeNode == null) throw new ArgumentNullException("parameterRuntimeTreeNode");

            BaseParameterRuntimeTreeNode node = this.Parameters.Find(n => n.Name == parameterRuntimeTreeNode.Name);

            if (node != null)
            {
                this.Parameters.Remove(node);
            }

            this.Parameters.Add(parameterRuntimeTreeNode);
        }



        /// <exclude />
        public void RemoveParameter(string parameterName)
        {
            if (string.IsNullOrEmpty(parameterName)) throw new ArgumentException("parameterName can not be null or an empty string");

            BaseParameterRuntimeTreeNode toRemove = this.Parameters.Where(f => f.Name == parameterName).FirstOrDefault();

            if (toRemove != null)
            {
                this.Parameters.Remove(toRemove);
            }
        }

        /// <summary>
        /// Returns information about parameters that have been set.
        /// </summary>
        public IEnumerable<BaseParameterRuntimeTreeNode> GetSetParameters()
        {
            return this.Parameters;
        }




        /// <exclude />
        public override bool ContainsNestedFunctions
        {
            get
            {
                foreach (var parameter in this.Parameters)
                {

                    if (parameter.GetAllSubFunctionNames().Count() > 0)
                    {
                        return true;
                    }
                }

                return false;
            }
        }


        /// <exclude />
        abstract protected IMetaFunction HostedFunction { get; }



        /// <summary>
        /// Returns the composite name of the hosted function
        /// </summary>
        /// <returns></returns>
        public string GetCompositeName()
        {
            return this.HostedFunction.CompositeName();
        }



        /// <summary>
        /// Returns the name (without namespace) of the hosted function
        /// </summary>
        /// <returns></returns>
        public string GetName()
        {
            return this.HostedFunction.Name;
        }



        /// <summary>
        /// Returns the namespace of the hosted function
        /// </summary>
        /// <returns></returns>
        public string GetNamespace()
        {
            return this.HostedFunction.Namespace;
        }



        /// <summary>
        /// Returns the description of the hosted function
        /// </summary>
        /// <returns></returns>
        public string GetDescription()
        {
            return this.HostedFunction.Description;
        }



        /// <exclude />
        protected void ValidateNotSelfCalling()
        {
            var function = HostedFunction;
            if (function is ICompoundFunction
                && (function as ICompoundFunction).AllowRecursiveCall)
            {
                return;
            }

            string functionName = GetCompositeName();

            foreach (BaseParameterRuntimeTreeNode parameterRuntimeTreeNode in Parameters)
            {
                if (IsSelfCalling(functionName, parameterRuntimeTreeNode))
                {
                    throw new InvalidOperationException("The function '{0}' is calling itself. A function should implement '{1}' interface in order not to be affected by that limitation."
                        .FormatWith(functionName, typeof(ICompoundFunction).FullName));
                }
            }
        }


        private static bool IsSelfCalling(string functionName, BaseRuntimeTreeNode runtimeTreeNode)
        {            
            if (runtimeTreeNode is FunctionParameterRuntimeTreeNode)
            {
                FunctionParameterRuntimeTreeNode functionParameterRuntimeTreeNode = runtimeTreeNode as FunctionParameterRuntimeTreeNode;

                if (functionParameterRuntimeTreeNode.GetHostedFunction().GetCompositeName() == functionName)
                {
                    return true;
                }
                return IsSelfCalling(functionName, functionParameterRuntimeTreeNode.GetHostedFunction());
            }

            if (runtimeTreeNode is BaseFunctionRuntimeTreeNode)
            {
                BaseFunctionRuntimeTreeNode functionRuntimeTreeNode = runtimeTreeNode as BaseFunctionRuntimeTreeNode;

                if (functionName == functionRuntimeTreeNode.GetCompositeName())
                {
                    return true;
                }

                foreach (BaseParameterRuntimeTreeNode parameterRuntimeTreeNode in functionRuntimeTreeNode.Parameters)
                {
                    if (IsSelfCalling(functionName, parameterRuntimeTreeNode))
                    {
                        return true;
                    }
                }

                return false;
            }

            return false;
        }
    }
}
