/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Xml.Linq;
using System;
using Composite.Functions.Foundation;
using Composite.Core.ResourceSystem;


namespace Composite.Functions
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
	public sealed class HelpDefinition
	{
        /// <exclude />
        public HelpDefinition GetLocalized()
        {
            if (this.HelpText.StartsWith("${"))
            {
                return new HelpDefinition(StringResourceSystemFacade.ParseString(this.HelpText));
            }
            else
            {
                return new HelpDefinition(this.HelpText);
            }
        }


        /// <exclude />
        public HelpDefinition(string helpText)
        {
            this.HelpText = helpText;
        }


        /// <exclude />
        public string HelpText
        {
            get;
            private set;
        }



        /// <exclude />
        public XElement Serialize()
        {
            XElement element = XElement.Parse(string.Format(@"<f:{0} xmlns:f=""{1}"" />", FunctionTreeConfigurationNames.HelpDefinitionTagName, FunctionTreeConfigurationNames.NamespaceName));

            element.Add(new XAttribute(FunctionTreeConfigurationNames.HelpTextAttributeName, this.HelpText));

            return element;
        }



        /// <exclude />
        public static HelpDefinition Deserialize(XElement serializedHelpDefinition)
        {
            if (serializedHelpDefinition == null) throw new ArgumentNullException("serializedHelpDefinition");

            if (serializedHelpDefinition.Name.LocalName != FunctionTreeConfigurationNames.HelpDefinitionTagName) throw new ArgumentException("Wrong serialized format");

            XAttribute helpTextAttribute = serializedHelpDefinition.Attribute(FunctionTreeConfigurationNames.HelpTextAttributeName);
            if (helpTextAttribute == null) throw new ArgumentException("Wrong serialized format");

            return new HelpDefinition(helpTextAttribute.Value);
        }
	}
}
