/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Collections.Generic;
using System.Linq;
using Composite.C1Console.Security;
using Composite.Plugins.Elements.ElementProviders.BaseFunctionProviderElementProvider;


namespace Composite.Functions
{
    /// <summary>
    /// Use this security ancestor provider for custom functions that
    /// will be an element under the 'All Functions' section in the C1 console.
    /// This provider assumes that the full name of the function, including namespace,
    /// is stored in the Id property of the entity token.   
    /// Example:
    /// entityToken.Id = "Composite.Forms.Renderer"    
    /// NOTE: That this might have changed if someone changes this in the provider code!
    /// </summary>
    public class StandardFunctionSecurityAncestorProvider : ISecurityAncestorProvider
    {
        /// <summary>
        /// Returns a parent entity token for the given function entity token.
        /// </summary>
        /// <param name="entityToken"></param>
        /// <returns></returns>
        public IEnumerable<EntityToken> GetParents(EntityToken entityToken)
        {
            string fullname = entityToken.Id;
            string providerName = entityToken.Source;

            if (fullname.Contains('.'))
            {
                fullname = fullname.Remove(fullname.LastIndexOf('.'));
            }

            string id = BaseFunctionProviderElementProvider.CreateId(fullname, "AllFunctionsElementProvider");

            yield return new BaseFunctionFolderElementEntityToken(id);
        }
    }
}
