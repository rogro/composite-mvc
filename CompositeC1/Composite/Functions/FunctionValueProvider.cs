/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Xml.Linq;
using System.Collections.Generic;


namespace Composite.Functions
{
    internal sealed class FunctionValueProvider : BaseValueProvider
    {
        private FunctionRuntimeTreeNode _functionFunctionRuntimeNode = null;

        private string _functionName = null;
        private List<BaseParameterRuntimeTreeNode> _parameters = null;

        private XElement _serializedFunction = null;

        private object _lock = new object();


        public FunctionValueProvider(FunctionRuntimeTreeNode functionFunctionRuntimeNode)
        {
            if (functionFunctionRuntimeNode == null) throw new ArgumentNullException("functionFunctionRuntimeNode");

            _functionFunctionRuntimeNode = functionFunctionRuntimeNode;
        }


        public FunctionValueProvider(string functionName, List<BaseParameterRuntimeTreeNode> parameters)
        {
            if (string.IsNullOrEmpty(functionName)) throw new ArgumentException("functionName may not be null or empty");
            if (parameters == null) throw new ArgumentNullException("parameters");

            _functionName = functionName;
            _parameters = parameters;
        }


        public FunctionValueProvider(XElement serializedFunction)
        {
            if (serializedFunction == null) throw new ArgumentNullException("serializedFunction");

            _serializedFunction = serializedFunction;
        }



        public override object GetValue(FunctionContextContainer contextContainer)
        {
            if (contextContainer == null) throw new ArgumentNullException("contextContainer");

            Initialize();

            return _functionFunctionRuntimeNode.GetValue(contextContainer);
        }



        public override XObject Serialize()
        {
            Initialize();

            return _functionFunctionRuntimeNode.Serialize();
        }


        private void Initialize()
        {
            if (_functionFunctionRuntimeNode == null)
            {
                lock (_lock)
                {
                    if (_functionFunctionRuntimeNode == null)
                    {
                        if (_serializedFunction == null)
                        {
                            IFunction function = FunctionFacade.GetFunction(_functionName);
                            _functionFunctionRuntimeNode = new FunctionRuntimeTreeNode(function, _parameters);
                        }
                        else
                        {
                            _functionFunctionRuntimeNode = (FunctionRuntimeTreeNode)FunctionTreeBuilder.Build(_serializedFunction);
                        }
                    }
                }
            }
        }
    }
}
