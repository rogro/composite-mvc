/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Xml.Xsl;
using Composite.Functions.Foundation;
using Composite.Functions.Foundation.PluginFacades;
using Composite.Core.Logging;
using Composite.Core.Extensions;
using Composite.Core.Types;

namespace Composite.Functions
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
	public static class XslExtensionsManager
	{
        /// <exclude />
        public static void Register(XsltArgumentList argumentList)
        {
            foreach(string providerName in XslExtensionsProviderRegistry.XslExtensionsProviderNames)
            {
                List<Pair<string, object>> extensions;
                try
                {
                    extensions = XslExtensionsProviderPluginFacade.CreateExtensions(providerName);
                }
                catch(Exception ex)
                {
                    string message = "Failed to get xsl extensions from provider '{0}'".FormatWith(providerName);
                    LoggingService.LogError("XslExtensionsManager", new InvalidOperationException(message, ex));
                    continue;
                }

                if(extensions != null)
                {
                    foreach (Pair<string, object> pair in extensions)
                    {
                        argumentList.AddExtensionObject(pair.First, pair.Second);
                    }
                }
            }
        }
	}
}
