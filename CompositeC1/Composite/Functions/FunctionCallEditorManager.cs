/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System.Linq;
using Composite.Core.Collections.Generic;
using Composite.Data;
using Composite.Data.Types;
using Composite.Core.WebClient;

namespace Composite.Functions
{
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    public static class FunctionCallEditorManager
    {
        private static readonly Hashtable<string, FunctionCallEditorSettings> _customEditorSettings = new Hashtable<string, FunctionCallEditorSettings>();

        /// <summary>
        /// Returns a relative path a to a custom funtion call editor for the specified function.
        /// </summary>
        /// <param name="functionName">The function name</param>
        /// <returns>A relative path to a custom call editor, or <value>null</value> if no custom editor was defined.</returns>
        public static FunctionCallEditorSettings GetCustomEditorSettings(string functionName)
        {
            var settings = _customEditorSettings[functionName];
            if (settings != null)
            {
                return settings;
            }

            using (var c = new DataConnection())
            {
                var mapping = c.Get<ICustomFunctionCallEditorMapping>().FirstOrDefault(e => e.FunctionName == functionName);

                if (mapping != null)
                {
                    return new FunctionCallEditorSettings
                    {
                        Url = UrlUtils.ResolvePublicUrl(mapping.CustomEditorPath),
                        Width = mapping.Width,
                        Height = mapping.Height
                    };
                }
            }

            return null;
        }

        /// <summary>
        /// Registers the custom call editor.
        /// </summary>
        /// <param name="functionName">Name of the function.</param>
        /// <param name="tildeBasedPath">A tilde based website path, like ~/Composite/custom.aspx</param>
        public static void RegisterCustomCallEditor(string functionName, string tildeBasedPath)
        {
            RegisterCustomCallEditor(functionName, tildeBasedPath, null, null);
        }

        /// <summary>
        /// Registers the custom call editor.
        /// </summary>
        /// <param name="functionName">Name of the function.</param>
        /// <param name="tildeBasedPath">A tilde based website path, like ~/Composite/custom.aspx</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        public static void RegisterCustomCallEditor(string functionName, string tildeBasedPath, int? width, int? height)
        {
            Verify.ArgumentCondition(tildeBasedPath.StartsWith("~"), "tildeBasedPath", "Must start with tilde (~)");

            lock (_customEditorSettings)
            {
                _customEditorSettings[functionName] = new FunctionCallEditorSettings
                    {
                        Url = UrlUtils.ResolvePublicUrl(tildeBasedPath),
                        Width = width,
                        Height = height
                    };
            }
        }
    }
}
