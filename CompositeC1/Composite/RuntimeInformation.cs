/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.Core.IO;
using System.Reflection;


namespace Composite
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
	public static class RuntimeInformation
	{
        private static bool _isUnitTestDetermined = false;
        private static bool _isUnitTest = false;
        private static string _uniqueInstanceName = null;
        private static string _uniqueInstanceNameSafe = null;

        /// <exclude />
        public static bool IsDebugBuild
        {
            get
            {
#if DEBUG
                return true;
#else
                return false;
#endif
            }
        }


        /// <exclude />
	    public static bool AppDomainLockingDisabled
	    {
            get
            {
                return IsUnittest;
            }
	    }


        /// <exclude />
        public static bool IsUnittest
        {
            get
            {
                if (_isUnitTestDetermined==false)
                {
                    _isUnitTest = RuntimeInformation.IsUnittestImpl;
                    _isUnitTestDetermined = true;
                }

                return _isUnitTest;
            }
        }


        private static bool IsUnittestImpl
        {
            get
            {
                if (AppDomain.CurrentDomain.SetupInformation.ApplicationName == null)
                {
                    return true;
                }

                if (AppDomain.CurrentDomain.SetupInformation.ApplicationName == "vstesthost.exe")
                {
                    return true;
                }

                return false;
            }
        }



        /// <exclude />
        public static Version ProductVersion
        {
            get
            {
                return typeof(RuntimeInformation).Assembly.GetName().Version;
            }
        }



        /// <exclude />
        public static string ProductTitle
        {
            get
            {
                Assembly asm = typeof(RuntimeInformation).Assembly;
                string assemblyTitle = ((AssemblyTitleAttribute)asm.GetCustomAttributes(typeof(AssemblyTitleAttribute), false)[0]).Title;

                return assemblyTitle;
            }
        }



        /// <exclude />
        public static string UniqueInstanceName
        {
            get
            {
                if (_uniqueInstanceName == null)
                {
                    string baseString = PathUtil.BaseDirectory.ToLowerInvariant();
                    _uniqueInstanceName = string.Format("C1@{0}", PathUtil.CleanFileName(baseString));
                }

                return _uniqueInstanceName;
            }
        }



        /// <exclude />
        public static string UniqueInstanceNameSafe
        {
            get
            {
                if (_uniqueInstanceNameSafe == null)
                {
                    string baseString = PathUtil.BaseDirectory.ToLowerInvariant().Replace(@"\", "-").Replace("/", "-");
                    _uniqueInstanceNameSafe = string.Format("C1@{0}", PathUtil.CleanFileName(baseString));
                }

                return _uniqueInstanceNameSafe;
            }
        }
	}
}
