/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */


if (document.location.protocol.toString() == "https:") {
    setTimeout(function () {
        document.location = document.location.toString().replace("unsecure.aspx", "top.aspx");
    }, 100);
} else {
    if (document.location.protocol.toString() == "http:") {
        var mySecureUrl = getMySecureUrl();
        var localWatermark = document.getElementById("watermark").getAttribute("content");
        document.write("<script type='text/javascript' src='" + mySecureUrl + "?jsprobe=true&watermark=" + encodeURIComponent(localWatermark) + "'></script>");
    }
}


window.onload = function () {
    document.body.className += " loaded";

    if (getParameterByName("fallback") == "true") {
        document.getElementById("splash").className += " fallbackallowed";

        setTimeout(function () {
            var start = document.getElementById("start");
            start.className += " active";
            start.onclick = function () {
                var adminPath = document.location.pathname.replace("unsecure.aspx", "");
                document.cookie = "avoidc1consolehttps=true; path=" + adminPath;
                var topUrl = document.location.toString().split("?")[0].replace("unsecure.aspx", "top.aspx");
                document.location = topUrl;
            }
        }, 2500);
    }
}


function getMySecureUrl() {
    var location = document.location;
    var customPort = getParameterByName("httpsport");

    var mySecureUrl = "https://" + location.hostname;

    if (customPort.length > 0) {
        mySecureUrl += ":" + parseInt(customPort, 10);
    }

    mySecureUrl += location.pathname;

    return mySecureUrl;
}


function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.search);
    if (results == null)
        return "";
    else
        return decodeURIComponent(results[1].replace(/\+/g, " "));
}