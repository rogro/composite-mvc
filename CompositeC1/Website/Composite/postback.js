/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

/**
 * Inspectors can recognize the postback document by looking for this global boolean.
 * @type {boolean}
 */
window.isPostBackDocument = true;

/**
 * Track postback map.
 * @type {List<object>} 
 */
window.postBackList = null;

/**
 * Track postback URL.
 * @type {string} 
 */
window.postBackURL = null;


/**
 * Populate form and submit to specified url. Note that we use a List instead 
 * of a Map, which may sound more appealing, because server could expect multiple 
 * fields with same name but different value; as may be the case for checkboxes.
 * @param {List<object>} map
 * @param {string} url
 */
function submit ( list, url ) {
	
	if ( !list instanceof List ) {
		alert ( "POSTBACK.JS REFACTORED!" );
	}
	
	var form = document.forms [ 0 ];
	form.action = top.Resolver.resolve ( url );
	var debug = "Posting to: " + form.action +"\n\n";

	list.each(function (object) {
		if (object.value != null && object.value.indexOf('\n') > -1) {
			var textarea = document.createElement("textarea");
			textarea.name = object.name;
			textarea.value = object.value;

			form.appendChild(textarea);
		}
		else {
			var input = document.createElement("input");
			input.name = object.name;
			input.value = object.value;

			// DELETE THIS NOW???
			input.setAttribute("name", new String(object.name)); // FF4.0 beta bug!!!
			input.setAttribute("value", new String(object.value)); // FF4.0 beta bug!!!

			form.appendChild(input);
		}
		debug += object.name + ": " + object.value + "\n";
	});
	
	/*
	 * Debug form post.
	 */
	top.SystemLogger.getLogger ( document.title ).debug ( debug );
	
	/*
	 * You can read these on window.unload in case you need to resubmit the data. 
	 */
	window.postBackList = list;
	window.postBackURL = url;
	
	/*
	 * Submit the form. Because other parties may have an interest in our local 
	 * window.load event, we submit on a short timeout. Otherwise this document  
	 * may cease to exist before they get a chance to handle it...
	 */
// maw: sry, but this was too annoying :)
//	alert ( form.method )
	setTimeout ( function () {
		top.Application.logger.debug ( DOMSerializer.serialize ( document.documentElement, true ));
		form.submit ();
	}, 100 );
}