/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

var DOMEvents = top.DOMEvents;
var Node = top.Node;
var CSSUtil = top.CSSUtil;
var Client = top.Client;

/**
 * @class
 */ 
var ViewSourceContent = new function () {
	
	var logger = top.SystemLogger.getLogger ( "ViewSourceContent" );
	var listen = true;
	
	/**
	 * Timeout fixes a strange bug in exploder.
	 * @param {DOMElement} e
	 */ 
	function twist ( element ) {
		if ( listen ) {
			element.className = element.className == "open" ? "closed" : "open";
			listen = false;
			setTimeout ( function () {
				listen = true;
			}, 0 );	
		} 
	}
	
	/**
	 * @implements {IEventListener}
	 * @param {MouseEvent} e
	 */
	this.handleEvent = function ( e ) {
	
		if ( e.button == Client.isExplorer ? 1 : 0 ) {
			var target = DOMEvents.getTarget ( e );
			while ( target.nodeType == Node.ELEMENT_NODE ) {
				if ( CSSUtil.hasClassName ( target, "haschildren" )) {
					DOMEvents.stopPropagation ( e );
					twist ( target.parentNode );
					break;
				} else {
					target = target.parentNode;
				}
			}
		}
	}
	
	DOMEvents.addEventListener ( 
		document, 
		DOMEvents.MOUSEDOWN, 
		this
	);
}