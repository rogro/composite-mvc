/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

FieldsButtonDataBinding.prototype = new DataBinding;
FieldsButtonDataBinding.prototype.constructor = FieldsButtonDataBinding;
FieldsButtonDataBinding.superclass = DataBinding.prototype;

/**
 * @class
 */
function FieldsButtonDataBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "FieldsButtonDataBinding" );
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
FieldsButtonDataBinding.prototype.toString = function () {

	return "[FieldsButtonDataBinding]";
}

/**
 * @overloads {DataBinding#onBindingRegister}
 */
FieldsButtonDataBinding.prototype.onBindingRegister = function () {
	
	FieldsButtonDataBinding.superclass.onBindingRegister.call ( this );
	
	this.propertyMethodMap [ "image" ] = function ( image ) {
		var button = this._buttonBinding;
		if ( button != null ) { 
			if ( button.imageProfile != null ) {
				button.imageProfile.setDefaultImage ( image ); // DAMMIT!!!
			}
			button.setImage ( image );
		}
	} 
	
	this.propertyMethodMap [ "label" ] = function ( label ) {
		var button = this._buttonBinding;
		if ( button != null ) {
			button.setLabel ( label );
		}
	}
	
	this.propertyMethodMap [ "tooltip" ] = function ( tooltip ) {
		var button = this._buttonBinding;
		if ( button != null ) {
			button.setToolTip ( tooltip );
		}
	}
	
	this.propertyMethodMap [ "isdisabled" ] = function ( isDisabled ) {
		var button = this._buttonBinding;
		if ( button != null ) {
			button.setDisabled ( isDisabled );
		}
	}
}

/**
 * @overloads {DataBinding#onBindingAttach}
 */
FieldsButtonDataBinding.prototype.onBindingAttach = function () {
	
	FieldsButtonDataBinding.superclass.onBindingAttach.call ( this );
	
	var button = this.add ( ClickButtonBinding.newInstance ( this.bindingDocument ));
	
	button.isFocusable = false;
	button.setProperty ( "image", this.getProperty ( "image" ));
	button.setProperty ( "label", this.getProperty ( "label" ));
	
	var isDisabled = this.getProperty ( "isdisabled" );
	if ( isDisabled ) {
		button.setProperty ( "isdisabled", true );
		this.isFocusable = false;
	}
	
	button.attach ();
	this._buttonBinding = button;
	this.addActionListener ( ButtonBinding.ACTION_COMMAND );
	
	// garbage going on here!
	var callbackid = this.getProperty ( "callbackid" );
	if ( callbackid != null ) {
		Binding.dotnetify ( this ); 
	}
}

/**
 * @implements {IActionListener}
 * @overloads {Binding#handleAction}
 * @param {Action} action
 */
FieldsButtonDataBinding.prototype.handleAction = function ( action ) {
	
	FieldsButtonDataBinding.superclass.handleAction.call ( this, action );
	
	switch ( action.type ) {
		case ButtonBinding.ACTION_COMMAND :
			this.focus ();
			this.dispatchAction ( PageBinding.ACTION_DOPOSTBACK );
			break;
	}
}

//ABSTRACT METHODS ............................................................

/**
 * Validate.
 * @implements {IData}
 * @return {boolean}
 */
FieldsButtonDataBinding.prototype.validate = function () {
	
	return true;
};

/**
 * Manifest. This will write form elements into page DOM 
 * so that the server recieves something on form submit.
 * @implements {IData}
 */
FieldsButtonDataBinding.prototype.manifest = function () {};

/**
 * Get value. This is intended for serversice processing.
 * @implements {IData}
 * @return {string}
 */
FieldsButtonDataBinding.prototype.getValue = function () {};

/**
 * Set value.
 * @implements {IData}
 * @param {string} value
 */
FieldsButtonDataBinding.prototype.setValue = function () {};

/**
 * Get result. This is intended for clientside processing.
 * @implements {IData}
 * @return {object}
 */
FieldsButtonDataBinding.prototype.getResult = function () {};

/**
 * Set result.
 * @implements {IData}
 * @param {object} value
 */
FieldsButtonDataBinding.prototype.setResult = function () {};