/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

ToolBarButtonDataBindingAddNew.prototype = new ToolBarButtonBinding;
ToolBarButtonDataBindingAddNew.prototype.constructor = ToolBarButtonDataBindingAddNew;
ToolBarButtonDataBindingAddNew.superclass = ToolBarButtonBinding.prototype;

/**
 * @class
 */
function ToolBarButtonDataBindingAddNew () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "ToolBarButtonDataBindingAddNew" );
	
	/**
	 * @type {string}
	 */
	this._dialoglabel = null;
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
ToolBarButtonDataBindingAddNew.prototype.toString = function () {

	return "[ToolBarButtonDataBindingAddNew]";
}

ToolBarButtonDataBindingAddNew.prototype.oncommand = function () {
	
	// TODO: move the view handle to binding markup as a property... 
    var showWidget = this.getProperty ( "selectwidget" );
	var def = ViewDefinitions [ showWidget ? "Composite.Management.WidgetFunctionSelectorDialog" : "Composite.Management.FunctionSelectorDialog" ];
	
	this._dialoglabel = def._label;
	def.argument.label = this.getProperty ( "dialoglabel" );
	def.argument.nodes [ 0 ].search = this.getProperty ( "providersearch" );
	
	var self = this;
    def.handler = {
        handleDialogResponse : function ( response, result ) {
    		delete def.argument.nodes [ 0 ].search;
    		def.argument.label = self._dialoglabel;
            if ( response == Dialog.RESPONSE_ACCEPT ) {
                self.shadowTree.dotnetinput.value = result.getFirst ();
                setTimeout ( function () {
                	self.dispatchAction ( Binding.ACTION_DIRTY )
                    self.dispatchAction ( PageBinding.ACTION_DOPOSTBACK );
                }, 0 );
            }
        }
    }

    Dialog.invokeDefinition ( def );
}