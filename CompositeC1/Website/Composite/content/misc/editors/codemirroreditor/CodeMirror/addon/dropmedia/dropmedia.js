/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

(function () {
	"use strict";

	var UPLOAD_SERVICE_URL = Resolver.resolve("${root}/services/Media/Upload.ashx");

	CodeMirror.defineInitHook(function (cm) {
		cm.on("drop", onDrop);
	});

	function onDrop(cm, e) {
		var files = e.dataTransfer.files;
		if (files && files.length && window.FileReader && window.File) {
			CodeMirror.e_preventDefault(e);

			dispatchMouseEvent(e, "mousedown");
			dispatchMouseEvent(e, "mouseup");

			var def = ViewDefinitions["Composite.Management.MediaWritableFolderSelectorDialog"];
			def.handler = {
				handleDialogResponse: function (response, result) {
					if (response == Dialog.RESPONSE_ACCEPT) {
						var folder = result.getFirst();
						var content = "";
						for (var i = 0; i < files.length; i++) {
							var file = files[i];
							var request = new XMLHttpRequest();
							request.open("post", UPLOAD_SERVICE_URL, false);
							request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
							request.setRequestHeader("X-FileName", file.name);
							request.setRequestHeader("X-Folder", folder);
							request.send(file);
							content += request.responseText;
						}
						cm.replaceSelection(content);
						// save last selected folder
						def.argument.selectedToken = folder;
					}
				}
			};
			Dialog.invokeDefinition(def);
		}
	}

	function dispatchMouseEvent(event, name) {
		if (event.initMouseEvent) {
			var mouseEvent = document.createEvent("MouseEvent");
			mouseEvent.initMouseEvent(name, true, true, window, 0,
										event.screenX, event.screenY, event.clientX, event.clientY,
										event.ctrlKey, event.altKey, event.shiftKey, event.metaKey,
										0, null);
			event.target.dispatchEvent(mouseEvent);
		}
	}
})();
