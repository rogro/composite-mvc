/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

ImageDialogPageBinding.prototype = new TinyDialogPageBinding;
ImageDialogPageBinding.prototype.constructor = ImageDialogPageBinding;
ImageDialogPageBinding.superclass = TinyDialogPageBinding.prototype;

/**
 * @class
 */
function ImageDialogPageBinding () {
	
	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "ImageDialogPageBinding" );
	
	/**
	 * @type {string}
	 */
	this._tinyAction = null;
}

/**
 * Identifies binding.
 */
ImageDialogPageBinding.prototype.toString = function () {

	return "[ImageDialogPageBinding]";
}

/**
 * @overloads {PageBinding#setPageArgument}
 * @param {object} arg
 */
ImageDialogPageBinding.prototype.setPageArgument = function ( arg ) {

	this._tinyAction = arg.tinyAction;
	this.label = this._tinyAction == "insert" ? "Insert Image" : "Image Properties";
	
	ImageDialogPageBinding.superclass.setPageArgument.call ( this, arg );
}

/**
 * @overloads {DialogPageBinding#onBeforePageIntialize}
 */
ImageDialogPageBinding.prototype.onBeforePageInitialize = function () {
	
	ImageDialogPageBinding.superclass.onBeforePageInitialize.call ( this );
	
	this._populateClassNameSelector ( "img" );
	if ( this._tinyAction == "update" ) {
		this._populateDataBindingsFromDOM ();
	}
}

/**
 * On "insert" action, launch dialog automatically.
 * @overloads {DialogPageBinding#onAfterPageIntialize}
 */
ImageDialogPageBinding.prototype.onAfterPageInitialize = function () {
	
	if ( this._tinyAction == "insert" ) {
		var dialoginput = this.bindingWindow.DataManager.getDataBinding ( "src" );
		dialoginput.oncommand ();
	}
	
	ImageDialogPageBinding.superclass.onAfterPageInitialize.call ( this );
}

/**
 * Initialize databindings.
 * @overloads {TinyDialogPageBinding#_populateDataBindingsFromDOM}
 */
ImageDialogPageBinding.prototype._populateDataBindingsFromDOM = function () {
	
	ImageDialogPageBinding.superclass._populateDataBindingsFromDOM.call ( this );
	
	var img 	= this._tinyElement;
	var src 	= img.getAttribute ( "src" );
	var alt 	= img.getAttribute ( "alt" );
	var title 	= img.getAttribute ( "title" );
	var manager	= this.bindingWindow.DataManager;
	
	if ( src ) {
		manager.getDataBinding ( "src" ).setValue ( src );
	}
	if ( alt ) {
		manager.getDataBinding ( "alt" ).setValue ( alt );
	}
	if ( title ) {
		manager.getDataBinding ( "title" ).setValue ( title );
	}
}