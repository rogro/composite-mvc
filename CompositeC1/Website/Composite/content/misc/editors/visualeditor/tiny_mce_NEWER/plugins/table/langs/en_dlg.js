/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

tinyMCE.addI18n('en.table_dlg',{
general_tab:"General",
advanced_tab:"Advanced",
general_props:"General properties",
advanced_props:"Advanced properties",
rowtype:"Row in table part",
title:"Insert/Modify table",
width:"Width",
height:"Height",
cols:"Cols",
rows:"Rows",
cellspacing:"Cellspacing",
cellpadding:"Cellpadding",
border:"Border",
align:"Alignment",
align_default:"Default",
align_left:"Left",
align_right:"Right",
align_middle:"Center",
row_title:"Table row properties",
cell_title:"Table cell properties",
cell_type:"Cell type",
valign:"Vertical alignment",
align_top:"Top",
align_bottom:"Bottom",
bordercolor:"Border color",
bgcolor:"Background color",
merge_cells_title:"Merge table cells",
id:"Id",
style:"Style",
langdir:"Language direction",
langcode:"Language code",
mime:"Target MIME type",
ltr:"Left to right",
rtl:"Right to left",
bgimage:"Background image",
summary:"Summary",
td:"Data",
th:"Header",
cell_cell:"Update current cell",
cell_row:"Update all cells in row",
cell_all:"Update all cells in table",
row_row:"Update current row",
row_odd:"Update odd rows in table",
row_even:"Update even rows in table",
row_all:"Update all rows in table",
thead:"Table Head",
tbody:"Table Body",
tfoot:"Table Foot",
scope:"Scope",
rowgroup:"Row Group",
colgroup:"Col Group",
col_limit:"You've exceeded the maximum number of columns of {$cols}.",
row_limit:"You've exceeded the maximum number of rows of {$rows}.",
cell_limit:"You've exceeded the maximum number of cells of {$cells}.",
missing_scope:"Are you sure you want to continue without specifying a scope for this table header cell. Without it, it may be difficult for some users with disabilities to understand the content or data displayed of the table.",
caption:"Table caption",
frame:"Frame",
frame_none:"none",
frame_groups:"groups",
frame_rows:"rows",
frame_cols:"cols",
frame_all:"all",
rules:"Rules",
rules_void:"void",
rules_above:"above",
rules_below:"below",
rules_hsides:"hsides",
rules_lhs:"lhs",
rules_rhs:"rhs",
rules_vsides:"vsides",
rules_box:"box",
rules_border:"border"
});