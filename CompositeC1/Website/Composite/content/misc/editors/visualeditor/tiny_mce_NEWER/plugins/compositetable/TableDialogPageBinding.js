/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

TableDialogPageBinding.prototype = new TinyDialogPageBinding;
TableDialogPageBinding.prototype.constructor = TableDialogPageBinding;
TableDialogPageBinding.superclass = TinyDialogPageBinding.prototype;

/**
 * @class
 */
function TableDialogPageBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "TableDialogPageBinding" );

	/**
	 * @type {string}
	 */
	this._tinyAction = null;
}

/**
 * Identifies binding.
 */
TableDialogPageBinding.prototype.toString = function () {

	return "[TableDialogPageBinding]";
}

/**
 * @overloads {DialogPageBinding#onBeforePageInitialize}
 */
TableDialogPageBinding.prototype.onBeforePageInitialize = function () {
	
	this._tinyAction = this.pageArgument.tinyAction;
	this._populateClassNameSelector ( "table" );
	this._invokeInsertVersusUpdateLayout ();
	
	TableDialogPageBinding.superclass.onBeforePageInitialize.call ( this );
}

/**
 * Same dialog is used for both "insert" and "update" table.
 */
TableDialogPageBinding.prototype._invokeInsertVersusUpdateLayout = function () {
	
	if ( this._tinyAction == "update" ) {
		this.label = StringBundle.getString ( "Composite.Web.VisualEditor", "Tables.Table.TitleUpdate" );
		this._populateDataBindingsFromDOM ();
	} else {
		this.label = StringBundle.getString ( "Composite.Web.VisualEditor", "Tables.Table.TitleInsert" );
	}
}

/**
 * Initialize databindings (only  in update scenario}.
 * @overloads {TinyDialogPageBinding#_populateDataBindingsFromDOM}
 */
TableDialogPageBinding.prototype._populateDataBindingsFromDOM = function () {
	
	TableDialogPageBinding.superclass._populateDataBindingsFromDOM.call ( this );
	
	var docManager = this.bindingWindow.DataManager;
	var table = this._tinyElement;
	
	var layoutFieldGroupBinding = UserInterface.getBinding ( 
		this.bindingDocument.getElementById ( "layoutfieldgroup" )
	);
	if ( layoutFieldGroupBinding ) {
		layoutFieldGroupBinding.hide ();
	}
	if ( table.summary ) {
		docManager.getDataBinding ( "summary" ).setValue ( table.summary );
	}
}