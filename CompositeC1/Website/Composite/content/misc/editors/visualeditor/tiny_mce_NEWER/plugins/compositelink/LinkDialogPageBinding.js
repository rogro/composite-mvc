/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

LinkDialogPageBinding.prototype = new TinyDialogPageBinding;
LinkDialogPageBinding.prototype.constructor = LinkDialogPageBinding;
LinkDialogPageBinding.superclass = TinyDialogPageBinding.prototype;

/**
 * @class
 */
function LinkDialogPageBinding () {
	
	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "LinkDialogPageBinding" );
}

/**
 * Identifies binding.
 */
LinkDialogPageBinding.prototype.toString = function () {

	return "[LinkDialogPageBinding]";
}

/**
 * @overloads {PageBinding#setPageArgument}
 * @param {object} arg
 */
LinkDialogPageBinding.prototype.setPageArgument = function ( arg ) {
	
	LinkDialogPageBinding.superclass.setPageArgument.call ( this, arg );
	this.label = this._tinyAction == "update" ? "Link Properties" : "Insert Link";
}

/**
 * @overloads {DialogPageBinding#onBeforePageIntialize}
 */
LinkDialogPageBinding.prototype.onBeforePageInitialize = function () {
	
	LinkDialogPageBinding.superclass.onBeforePageInitialize.call ( this );
	
	this._populateClassNameSelector ( "a" );
	if ( this._tinyAction == "update" ) {
		this._populateDataBindingsFromDOM ();
	} else {
		var manager = this.bindingWindow.DataManager;
		manager.getDataBinding ( "href" ).setValue ( "http://" );
	}
}

/**
 * Initialize databindings.
 * @overloads {TinyDialogPageBinding#_populateDataBindingsFromDOM}
 */
LinkDialogPageBinding.prototype._populateDataBindingsFromDOM = function () {
	
	LinkDialogPageBinding.superclass._populateDataBindingsFromDOM.call ( this );
	
	var a = this._tinyElement, manager = this.bindingWindow.DataManager;
	
	if ( a.href ) {
		manager.getDataBinding ( "href" ).setValue ( a.href );
	}
	if ( a.rel ) {
		manager.getDataBinding ( "rel" ).setValue ( a.rel );
	}
	if ( a.title ) {
		manager.getDataBinding ( "title" ).setValue ( a.title );
	}
	
	var target = a.getAttribute ( "tinymcetargetalias" );
	if ( target &&  target == "_blank" ) {
		manager.getDataBinding ( "blank" ).check ( true );
	}
}