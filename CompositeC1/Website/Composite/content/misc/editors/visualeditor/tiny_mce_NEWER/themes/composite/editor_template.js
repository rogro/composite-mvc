/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

new function () {
	
	/**
	 * Node change handlers.
	 * @type {List<IWysiwygEditorNodeChangeHandler>}
	 */
	var nodeChangeHandlers = new List ();
	
	/**
	 * Content change handlers.
	 * @type {List<IWysiwygEditorContentChangeHandler>}
	 */
	var contentChangeHandlers = new List ();
	
	/*
	 * Shortcut.
	 */
	var DOM = tinymce.DOM;
	
	/**
	 * Info
	 */
	this.getInfo = function() {
		return {
			longname : 'Composite theme',
			author : 'Composite A/S',
			authorurl : 'http://www.composite.net',
			version : tinymce.majorVersion + "." + tinymce.minorVersion
		}
	}

	/*
	 * This would be the actual theme.
	 */
	tinymce.create ( "tinymce.themes.CompositeTheme", {
		
		/**
		 * @type {VisualEditorBinding}
		 */
		editorBinding : null,
		
		/**
		 * The TinyMCE engine.
		 * @type {TinyMCE_Engine} 
		 */
		tinyEngine : null,
		
		/**
		 * The TinyMCE instance.
		 * @type {tinymce.Editor}
		 */
		tinyInstance : null,
		
		/**
		 * Invoked by the VisualEditorBinding.
		 * @param {VisualEditorBinding} editor
		 * @param {TinyMCE_Engine} engine
		 * @param {tinymce.Editor} instance
		 */
		initC1 : function ( binding, engine, instance ) {
			
			this.editorBinding = binding;
			this.tinyEngine = engine;
			this.tinyInstance = instance;
			
			/*
			 * Inject stylesheets.
			 */
			var css0 = this.editorBinding.defaultStylesheet;
			var css1 = this.editorBinding.presentationStylesheet;
			var css2 = this.editorBinding.configurationStylesheet;
			
			if ( css0 != null ) {
				this.tinyInstance.dom.loadCSS ( css0 );
			}
			if ( css1 != null ) {
				this.tinyInstance.dom.loadCSS ( css1 );
			}
			if ( css2 != null ) {
				this.tinyInstance.dom.loadCSS ( css2 );
			}
		},
		
		/**
		 * @param {IWysiwygEditorNodeChangeHandler} handler
		 */
		registerNodeChangeHandler : function ( handler ) {
			if ( handler && handler.handleNodeChange != null ) {
				nodeChangeHandlers.add ( handler );
			}
		},
		
		/**
		 * @param {IWysiwygEditorContentChangeHandler} handler
		 */
		registerContentChangeHandler : function ( handler ) {
			if ( handler && handler.handleContentChange != null ) {
				contentChangeHandlers.add ( handler );
			}
		},
		
		/**
		 * Init.
		 */
		init : function ( ed, url ) {
		
			var t = this;
			t.editor = ed;
			
			ed.onInit.add ( function () {
				
				/*
				 * Register node change handler.
				 */
				ed.onNodeChange.add ( 
					
					/*
					 * @param {tinymce.Editor} ed
					 * @param {tinymce.ControlManager} cm
					 * @param {HTMLElement} e
					 */
					function ( ed, cm, e ) {
						if ( e != null ) {
							if ( nodeChangeHandlers.hasEntries ()) {
								nodeChangeHandlers.each ( function ( handler ) {
									handler.handleNodeChange ( e );
								});
							}
						}
				});
				
				/*
				 * Register content change handler.
				 */
				ed.onChange.add ( 
					
					/*
					 * @param {tinymce.Editor} ed
					 * @param {int} l Undo lavel that got added
					 * @param {object} um Undo manager that level was added to
					 */
					function ( ed, l, um ) {
						if ( contentChangeHandlers.hasEntries ()) {
							contentChangeHandlers.each ( function ( handler ) {
								handler.handleContentChange ();
							});
						}
				});
			});
		},
		
		/**
		 * @param {what?} o
		 */
		renderUI : function(o) {
			
			var t = this, n = o.targetNode, ic, tb, ed = t.editor, cf = ed.controlManager;

			n = DOM.insertAfter ( DOM.create ( 'div', { id : ed.id + '_container' }), n );
			n = ic = DOM.add ( n, 'div' );

			return {
				iframeContainer : ic,
				editorContainer : ed.id + '_container',
				sizeContainer : null,
				deltaHeight : -20
			};
		},
		
		/**
		 * Enable dialog mode.
		 */
		enableDialogMode : function () {
			
			this.editorBinding.enableDialogMode ();
		},
		
		/**
		 * Disable dialog mode.
		 */
		disableDialogMode : function () {
			
			this.editorBinding.disableDialogMode ();
			
		}
	});

	tinymce.ThemeManager.add ( "composite", tinymce.themes.CompositeTheme );
}