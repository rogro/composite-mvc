/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

tinyMCE.addI18n('en.xhtmlxtras_dlg',{
attribute_label_title:"Title",
attribute_label_id:"ID",
attribute_label_class:"Class",
attribute_label_style:"Style",
attribute_label_cite:"Cite",
attribute_label_datetime:"Date/Time",
attribute_label_langdir:"Text Direction",
attribute_option_ltr:"Left to right",
attribute_option_rtl:"Right to left",
attribute_label_langcode:"Language",
attribute_label_tabindex:"TabIndex",
attribute_label_accesskey:"AccessKey",
attribute_events_tab:"Events",
attribute_attrib_tab:"Attributes",
general_tab:"General",
attrib_tab:"Attributes",
events_tab:"Events",
fieldset_general_tab:"General Settings",
fieldset_attrib_tab:"Element Attributes",
fieldset_events_tab:"Element Events",
title_ins_element:"Insertion Element",
title_del_element:"Deletion Element",
title_acronym_element:"Acronym Element",
title_abbr_element:"Abbreviation Element",
title_cite_element:"Citation Element",
remove:"Remove",
insert_date:"Insert current date/time",
option_ltr:"Left to right",
option_rtl:"Right to left",
attribs_title:"Insert/Edit Attributes"
});