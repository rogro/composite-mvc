/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

/**
 * Composite image plugin.
 */
new function () {
	
	var URL_IMAGETREESELECTOR = "${tiny}/plugins/compositeimage/image.aspx";
	
	tinymce.create ( "tinymce.plugins.CompositeImagePlugin", {
		
		/**
		 * @type {tinymce.Editor}
		 */
		editor : null,
		
		/**
		 * Hacking a bug where explorer does not recognize  
		 * double-click on a newly inserted image...
		 */
		_img : null,
		
		/**
		 * Get info
		 */
		getInfo : function() {
			return {
				longname : "Composite Image Plugin",
				author : "Composite A/S",
				authorurl : "http://www.composite.net",
				infourl : null,
				version : tinymce.majorVersion + "." + tinymce.minorVersion
			};
		},
		
		/**
		 * @param {tinymce.Editor} ed
		 * @param {string} url
		 */
		init : function ( ed, url ) {
			
			this.editor = ed;
			
			/*
			 * Doubleclick feature enabled for Mozilla only. 
			 * IE looses the selection-bookmark when IMG gets  
			 * doubleclicked immediately after closing the dialog.
			 */
			if ( Client.isMozilla ) {
				var self = this;
				ed.onDblClick.add ( function ( editor, e ) {
					if ( e.target.nodeName.toLowerCase () == "img" ) {
						if ( 
							CSSUtil.hasClassName ( e.target, VisualEditorBinding.FUNCTION_CLASSNAME ) ||
							CSSUtil.hasClassName ( e.target, VisualEditorBinding.FIELD_CLASSNAME )) {
						} else {
							self._img = e.target;
							self.execCommand ( "compositeInsertImage", true, "update" );
							self._img = null;
						}
						/*
						switch ( e.target.className ) {
							case VisualEditorBinding.FUNCTION_CLASSNAME :
							case VisualEditorBinding.FIELD_CLASSNAME :
								break;
							default :
								self._img = e.target;
								self.execCommand ( "compositeInsertImage", true, "update" );
								self._img = null;
								break;
						}
						*/
					}
				});
			}
		},
			
		/**
		 * @param {string} cmd
		 * @param {boolean} ui
		 * @param {string} value
		 */
		execCommand : function ( cmd, ui, value ) {
			
			var result = false;
			var self = this;
			var editor = this.editor;
			var editorBinding = editor.theme.editorBinding;
			
			if ( cmd == "compositeInsertImage" ) {
				
				var img = null;
				if ( this._img != null ) {
					img = this._img;
				} else {
					img = self.editor.selection.getNode ();
				}
				if ( img.nodeName.toLowerCase () != "img" ) {
					img = null;
				}
				
				this.editor.theme.enableDialogMode ();
				
				var self = this;
				var handler = {
					handleDialogResponse : function ( response, result ) {
					
						self.editor.theme.disableDialogMode ();
						
						if ( response == Dialog.RESPONSE_ACCEPT ) {
							
							switch ( value ) {
								
								case "insert" :
									
									var html = '<img';
									html += makeAttrib ( 'id', result.get ( "id" ) );
									html += makeAttrib ( 'class', result.get ( "classname" ));
									html += makeAttrib ( 'src', result.get ( "src" ));
									html += makeAttrib ( 'alt', result.get ( "alt" ));
									html += makeAttrib ( 'title', result.get ( "title" ));
									html += '"/>';
									
									var inst = tinyMCE.selectedInstance;
									inst.execCommand ( "mceInsertContent", false, html );
									break;
									
								case "update" :
									
									if ( result.get ( "src" ) != null ) {
										img.src = result.get ( "src" );
									} else {
										img.src = "";
										img.removeAttribute ( "src" );
									}
									if ( result.get( "alt" ) != null ) {
										img.alt = result.get ( "alt" );
									} else {
										img.alt = "";
									}
									if ( result.get( "title" ) != null ) {
										img.title = result.get ( "title" );
									} else {
										img.title = "";
										img.removeAttribute ( "title" );
									}
									if ( result.get( "id" ) != null ) {
										img.id = result.get ( "id" );
									} else {
										img.id = "";
										img.removeAttribute ( "id" );	
									}
									if ( result.get( "classname" ) != null ) {
										img.className = result.get ( "classname" );
										img.setAttribute ( "class", result.get ( "classname" ));
									} else {
										img.className = "";
										img.removeAttribute ( "class" );
									}
									break;
							}
							
							editorBinding.checkForDirty ();
						}
					}
				};
				
				var arg = {
					tinyAction 		: value,
					tinyWindow 		: window,
					tinyElement 	: img,
					tinyEngine 		: tinymce,
					tinyInstance 	: this.editor,
					tinyTheme 		: this.editor.theme,
					editorBinding 	: this.editor.theme.editorBinding
				}
				
				Dialog.invokeModal ( URL_IMAGETREESELECTOR, handler, arg );
				result = true;
			}
			
			return result;
		}
	});

	// Register plugin
	tinymce.PluginManager.add("compositeimage", tinymce.plugins.CompositeImagePlugin);
};