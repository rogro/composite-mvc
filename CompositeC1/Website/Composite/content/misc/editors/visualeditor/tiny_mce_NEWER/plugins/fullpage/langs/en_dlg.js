/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

tinyMCE.addI18n('en.fullpage_dlg',{
title:"Document properties",
meta_tab:"General",
appearance_tab:"Appearance",
advanced_tab:"Advanced",
meta_props:"Meta information",
langprops:"Language and encoding",
meta_title:"Title",
meta_keywords:"Keywords",
meta_description:"Description",
meta_robots:"Robots",
doctypes:"Doctype",
langcode:"Language code",
langdir:"Language direction",
ltr:"Left to right",
rtl:"Right to left",
xml_pi:"XML declaration",
encoding:"Character encoding",
appearance_bgprops:"Background properties",
appearance_marginprops:"Body margins",
appearance_linkprops:"Link colors",
appearance_textprops:"Text properties",
bgcolor:"Background color",
bgimage:"Background image",
left_margin:"Left margin",
right_margin:"Right margin",
top_margin:"Top margin",
bottom_margin:"Bottom margin",
text_color:"Text color",
font_size:"Font size",
font_face:"Font face",
link_color:"Link color",
hover_color:"Hover color",
visited_color:"Visited color",
active_color:"Active color",
textcolor:"Color",
fontsize:"Font size",
fontface:"Font family",
meta_index_follow:"Index and follow the links",
meta_index_nofollow:"Index and don't follow the links",
meta_noindex_follow:"Do not index but follow the links",
meta_noindex_nofollow:"Do not index and don\'t follow the links",
appearance_style:"Stylesheet and style properties",
stylesheet:"Stylesheet",
style:"Style",
author:"Author",
copyright:"Copyright",
add:"Add new element",
remove:"Remove selected element",
moveup:"Move selected element up",
movedown:"Move selected element down",
head_elements:"Head elements",
info:"Information",
add_title:"Title element",
add_meta:"Meta element",
add_script:"Script element",
add_style:"Style element",
add_link:"Link element",
add_base:"Base element",
add_comment:"Comment node",
title_element:"Title element",
script_element:"Script element",
style_element:"Style element",
base_element:"Base element",
link_element:"Link element",
meta_element:"Meta element",
comment_element:"Comment",
src:"Src",
language:"Language",
href:"Href",
target:"Target",
type:"Type",
charset:"Charset",
defer:"Defer",
media:"Media",
properties:"Properties",
name:"Name",
value:"Value",
content:"Content",
rel:"Rel",
rev:"Rev",
hreflang:"Href lang",
general_props:"General",
advanced_props:"Advanced"
});