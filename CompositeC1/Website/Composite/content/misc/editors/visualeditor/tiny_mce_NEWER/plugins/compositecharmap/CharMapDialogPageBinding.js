/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

CharMapDialogPageBinding.prototype = new DialogPageBinding;
CharMapDialogPageBinding.prototype.constructor = CharMapDialogPageBinding;
CharMapDialogPageBinding.superclass = DialogPageBinding.prototype;

/**
 * @class
 */
function CharMapDialogPageBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "CharMapDialogPageBinding" );
}

/**
 * Identifies binding.
 */
CharMapDialogPageBinding.prototype.toString = function () {

	return "[CharMapDialogPageBinding]";
}

/**
 *
 */
CharMapDialogPageBinding.prototype.onBindingAttach = function () {

	CharMapDialogPageBinding.superclass.onBindingAttach.call ( this );
	
	this.addEventListener ( DOMEvents.MOUSEOVER );
	this.addEventListener ( DOMEvents.MOUSEOUT );
	this.addEventListener ( DOMEvents.CLICK );
}

/**
 * @overloads {PageBinding#handleEvent}
 * @param {MouseEvent} e
 */
CharMapDialogPageBinding.prototype.handleEvent = function ( e ) {
	
	CharMapDialogPageBinding.superclass.handleEvent.call ( this, e );
	
	var e = e ? e : this.bindingWindow.event;
	var node = e.target ? e.target : e.srcElement;
	var p = this.bindingDocument.getElementById ( "selection" );
	
	if ( DOMUtil.getLocalName ( node ) == "a" ) {
		switch ( e.type ) {
			case "click" :
				this.response = Dialog.RESPONSE_ACCEPT;
				this.result = String.fromCharCode ( node.getAttribute ( "code" ));
				this.dispatchAction ( DialogPageBinding.ACTION_RESPONSE );
				break;
			default :
				p.firstChild.nodeValue = e.type == DOMEvents.MOUSEOVER ? node.getAttribute ( "text" ) : "";
				break;
		}
	}
}