/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

/**
 * Composite plugin.
 */
new function () {
	
	//var URL_UPDATERENDERING = "${root}/content/dialogs/functions/editFunctionCall.aspx?type=Composite.Core.Xml.XhtmlDocument,Composite&functionmarkup=${functionmarkup}";
	var URL_UPDATERENDERING = "${root}/content/dialogs/functions/editFunctionCall.aspx?type=Composite.Core.Xml.XhtmlDocument,Composite";
	
	tinymce.create ( "tinymce.plugins.CompositeRenderingPlugin", {
		
		/**
		 * @type {tinymce.Editor}
		 */
		editor : null,
		
		/**
		 * Get info
		 */
		getInfo : function() {
			return {
				longname : "Composite Rendering Plugin",
				author : "Composite A/S",
				authorurl : "http://www.composite.net",
				infourl : null,
				version : tinymce.majorVersion + "." + tinymce.minorVersion
			};
		},
		
		/**
		 * @param {tinymce.Editor} ed
		 * @param {string} url
		 */
		init : function ( ed, url ) {
			
			this.editor = ed;
			
			/*
			 * Doubleclick feature enabled for Mozilla only. 
			 * IE looses the selection-bookmark when IMG gets  
			 * doubleclicked immediately after closing the dialog.
			 */
			if ( Client.isMozilla ) {
				var self = this;
				ed.onDblClick.add ( function ( editor, e ) {
					if ( e.target.nodeName.toLowerCase () == "img" ) {
						if ( CSSUtil.hasClassName ( e.target, VisualEditorBinding.FUNCTION_CLASSNAME )) {
							self._img = e.target;
							self.execCommand ( "compositeInsertRendering", true, "update" );
							self._img = null;
						}
						/*
						switch ( e.target.className ) {
							case VisualEditorBinding.FUNCTION_CLASSNAME :
								self._img = e.target;
								self.execCommand ( "compositeInsertRendering", true, "update" );
								self._img = null;
								break;
						}
						*/
					}
				});
			}
		},
			
		/**
		 * @param {string} cmd
		 * @param {boolean} ui
		 * @param {string} value
		 */
		execCommand : function ( cmd, ui, value ) {
			
			var result = false;
			var self = this;
			var editor = this.editor;
			var editorBinding = editor.theme.editorBinding;
			
			if ( cmd == "compositeInsertRendering" ) {	
				if ( value == "update" ) {
					this._updateRendering ();
				} else {
					this._insertRendering ();
				}
				editorBinding.checkForDirty ();
				result = true;
			} 
			return result;
		},
		
		/**
		 * Insert rendering.
		 */
		_insertRendering : function () {
			
			this.editor.theme.enableDialogMode ();
			
			var def = ViewDefinitions [ "Composite.Management.XhtmlDocumentFunctionSelectorDialog" ];
			
			var self = this;
			def.handler = {
				handleDialogResponse : function ( response, result ) {
					if ( response == Dialog.RESPONSE_ACCEPT ) {
					
						var functionName = result.getFirst ();
						var functionInfo = top.XhtmlTransformationsService.GetFunctionInfo ( functionName );
						
						if ( functionInfo.RequireConfiguration ) {
							self._launchUpdateDialog ( functionInfo.FunctionMarkup );
						} else {
							self.editor.theme.disableDialogMode ()
							self._insertImgTag ( functionInfo.FunctionMarkup );
						}
					} else {
						self.editor.theme.disableDialogMode ()
					}
				}
			}
			Dialog.invokeDefinition ( def );
		},
		
		/**
		 * Update rendering.
		 */
		_updateRendering : function () {
			
			var img = null;
			if ( this._img != null ) {
				img = this._img;
			} else {
				img = this.editor.selection.getNode ();
			}
			if ( img.nodeName.toLowerCase () == "img" ) {
				var markup = img.alt;
				this._launchUpdateDialog ( markup );
			}
		},
		
		/**
		 * Launch update dialog.
		 * @param {string} markup
		 */
		_launchUpdateDialog : function ( markup ) {
			
			this.editor.theme.enableDialogMode ();
			
			var self = this;
			var dialogHandler = {
				handleDialogResponse : function ( response, result ) {
				
					self.editor.theme.disableDialogMode ();
					
					if ( response == Dialog.RESPONSE_ACCEPT ) {
						self._insertImgTag ( result );
					}
				}
			}
			
			/*
			URL_UPDATERENDERING = "${root}/content/dialogs/functions/editFunctionCall.aspx?type=Composite.Core.Xml.XhtmlDocument,Composite&functionmarkup=${functionmarkup}";
			var url = URL_UPDATERENDERING.replace ( 
				"${functionmarkup}", 
				markup
			);
			Dialog.invokeModal ( 
				url,
				dialogHandler, 
				null 
			);
			*/
			
			/*
			 * TODO: Implement this instead...
			 */
			var def = ViewDefinitions [ "Composite.Management.PostBackDialog" ];
			def.width = 880; //760;
			def.height = 520;
			def.label = "Function Properties";
			def.image = "${icon:parameter_overloaded}";
			def.handler = dialogHandler;
			def.argument = {
				url : URL_UPDATERENDERING,
				list : new List ([{ name: "functionmarkup", value : markup }])
			}
			StageBinding.presentViewDefinition ( def );
		},
		
		/**
		 * Insert image tag from function markup.
		 * @param {string} markup
		 */
		_insertImgTag : function ( markup ) {
			
			if ( markup != "" ) {
				var html = top.XhtmlTransformationsService.GetImageTagForFunctionCall ( markup );
				this.editor.execCommand ( "mceInsertContent", false, html );
			} else {
				this.editor.execCommand ( "mceInsertContent", false, "" );
			}
			
			this.editor.theme.editorBinding.checkForDirty ();
		}
	});

	// Register plugin
	tinymce.PluginManager.add("compositerendering", tinymce.plugins.CompositeRenderingPlugin);
};
