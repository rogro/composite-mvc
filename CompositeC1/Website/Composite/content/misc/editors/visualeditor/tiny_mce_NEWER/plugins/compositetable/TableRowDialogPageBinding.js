/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

TableRowDialogPageBinding.prototype = new TinyDialogPageBinding;
TableRowDialogPageBinding.prototype.constructor = TableRowDialogPageBinding;
TableRowDialogPageBinding.superclass = TinyDialogPageBinding.prototype;

/**
 * @class
 */
function TableRowDialogPageBinding () {
	
	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "TableRowDialogPageBinding" );
}

/**
 * Identifies binding.
 */
TableRowDialogPageBinding.prototype.toString = function () {

	return "[TableRowDialogPageBinding]";
}

/**
 * @overloads {DialogPageBinding#onBeforePageInitialize}
 */
TableRowDialogPageBinding.prototype.onBeforePageInitialize = function () {
	
	TableRowDialogPageBinding.superclass.onBeforePageInitialize.call ( this );
	
	this._populateClassNameSelector ();
	this._populateDataBindingsFromDOM ();
}

/**
 * Initialize databindings.
 * @overloads {TinyDialogPageBinding#_populateDataBindingsFromDOM}
 */
TableRowDialogPageBinding.prototype._populateDataBindingsFromDOM = function () {
	
	TableRowDialogPageBinding.superclass._populateDataBindingsFromDOM.call ( this );
	
	var manager = this.bindingWindow.DataManager;
	var tr = this._tinyElement;

	var rowtype = manager.getDataBinding ( "rowtype" );
	var position = DOMUtil.getLocalName ( tr.parentNode );
	rowtype.selectByValue ( position, true );
	
	if ( tr.getAttribute ( "align" )) {
		manager.getDataBinding ( "align" ).selectByValue ( tr.align );
	}
	if ( tr.getAttribute ( "valign" )) {
		manager.getDataBinding ( "valign" ).selectByValue ( tr.valign );
	}
	
	var selector = DataManager.getDataBinding ( "rowtype" );
	switch ( tr.parentNode.nodeName.toLowerCase ()) {
		case "thead" :
			selector.selectByValue ( "thead" );
			break;
		case "tfoot" :
			selector.selectByValue ( "tfoot" );
			break;
	}
}