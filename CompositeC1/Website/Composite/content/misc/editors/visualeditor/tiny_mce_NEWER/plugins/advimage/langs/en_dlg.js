/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

tinyMCE.addI18n('en.advimage_dlg',{
tab_general:"General",
tab_appearance:"Appearance",
tab_advanced:"Advanced",
general:"General",
title:"Title",
preview:"Preview",
constrain_proportions:"Constrain proportions",
langdir:"Language direction",
langcode:"Language code",
long_desc:"Long description link",
style:"Style",
classes:"Classes",
ltr:"Left to right",
rtl:"Right to left",
id:"Id",
map:"Image map",
swap_image:"Swap image",
alt_image:"Alternative image",
mouseover:"for mouse over",
mouseout:"for mouse out",
misc:"Miscellaneous",
example_img:"Appearance preview image",
missing_alt:"Are you sure you want to continue without including an Image Description? Without it the image may not be accessible to some users with disabilities, or to those using a text browser, or browsing the Web with images turned off.",
dialog_title:"Insert/edit image",
src:"Image URL",
alt:"Image description",
list:"Image list",
border:"Border",
dimensions:"Dimensions",
vspace:"Vertical space",
hspace:"Horizontal space",
align:"Alignment",
align_baseline:"Baseline",
align_top:"Top",
align_middle:"Middle",
align_bottom:"Bottom",
align_texttop:"Text top",
align_textbottom:"Text bottom",
align_left:"Left",
align_right:"Right",
image_list:"Image list"
});