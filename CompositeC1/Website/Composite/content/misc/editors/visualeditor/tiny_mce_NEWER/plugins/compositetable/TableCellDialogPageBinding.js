/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

TableCellDialogPageBinding.prototype = new TinyDialogPageBinding;
TableCellDialogPageBinding.prototype.constructor = TableCellDialogPageBinding;
TableCellDialogPageBinding.superclass = TinyDialogPageBinding.prototype;

/**
 * @class
 */
function TableCellDialogPageBinding () {
	
	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "TableCellDialogPageBinding" );
}

/**
 * Identifies binding.
 */
TableCellDialogPageBinding.prototype.toString = function () {

	return "[TableCellDialogPageBinding]";
}

/**
 * @overloads {DialogPageBinding#onBeforePageIntialize}
 */
TableCellDialogPageBinding.prototype.onBeforePageInitialize = function () {
	
	this._populateClassNameSelector ();
	this._populateDataBindingsFromDOM ();
	
	TableCellDialogPageBinding.superclass.onBeforePageInitialize.call ( this );
}

/**
 * Initialize databindings.
 * @overloads {TinyDialogPageBinding#_populateDataBindingsFromDOM}
 */
TableCellDialogPageBinding.prototype._populateDataBindingsFromDOM = function () {
	
	TableCellDialogPageBinding.superclass._populateDataBindingsFromDOM.call ( this );
	
	var td = this._tinyElement;
	var manager = this.bindingWindow.DataManager;
	
	manager.getDataBinding ( "cellType" ).selectByValue ( 
		DOMUtil.getLocalName ( td ),
		true
	);
	if ( td.getAttribute ( "width" )) {
		manager.getDataBinding ( "width" ).setValue ( td.width );
	}
	if ( td.getAttribute ( "align" )) {
		manager.getDataBinding ( "align" ).selectByValue ( td.align );
	}
	if ( td.getAttribute ( "valign" )) {
		manager.getDataBinding ( "valign" ).selectByValue ( td.valign );
	}
}