/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

tinyMCE.addI18n('en.style_dlg',{
title:"Edit CSS Style",
apply:"Apply",
text_tab:"Text",
background_tab:"Background",
block_tab:"Block",
box_tab:"Box",
border_tab:"Border",
list_tab:"List",
positioning_tab:"Positioning",
text_props:"Text",
text_font:"Font",
text_size:"Size",
text_weight:"Weight",
text_style:"Style",
text_variant:"Variant",
text_lineheight:"Line height",
text_case:"Case",
text_color:"Color",
text_decoration:"Decoration",
text_overline:"overline",
text_underline:"underline",
text_striketrough:"strikethrough",
text_blink:"blink",
text_none:"none",
background_color:"Background color",
background_image:"Background image",
background_repeat:"Repeat",
background_attachment:"Attachment",
background_hpos:"Horizontal position",
background_vpos:"Vertical position",
block_wordspacing:"Word spacing",
block_letterspacing:"Letter spacing",
block_vertical_alignment:"Vertical alignment",
block_text_align:"Text align",
block_text_indent:"Text indent",
block_whitespace:"Whitespace",
block_display:"Display",
box_width:"Width",
box_height:"Height",
box_float:"Float",
box_clear:"Clear",
padding:"Padding",
same:"Same for all",
top:"Top",
right:"Right",
bottom:"Bottom",
left:"Left",
margin:"Margin",
style:"Style",
width:"Width",
height:"Height",
color:"Color",
list_type:"Type",
bullet_image:"Bullet image",
position:"Position",
positioning_type:"Type",
visibility:"Visibility",
zindex:"Z-index",
overflow:"Overflow",
placement:"Placement",
clip:"Clip"
});