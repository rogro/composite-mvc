/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

tinyMCE.addI18n('en.advlink_dlg',{
title:"Insert/edit link",
url:"Link URL",
target:"Target",
titlefield:"Title",
is_email:"The URL you entered seems to be an email address, do you want to add the required mailto: prefix?",
is_external:"The URL you entered seems to external link, do you want to add the required http:// prefix?",
list:"Link list",
general_tab:"General",
popup_tab:"Popup",
events_tab:"Events",
advanced_tab:"Advanced",
general_props:"General properties",
popup_props:"Popup properties",
event_props:"Events",
advanced_props:"Advanced properties",
popup_opts:"Options",
anchor_names:"Anchors",
target_same:"Open in this window / frame",
target_parent:"Open in parent window / frame",
target_top:"Open in top frame (replaces all frames)",
target_blank:"Open in new window",
popup:"Javascript popup",
popup_url:"Popup URL",
popup_name:"Window name",
popup_return:"Insert 'return false'",
popup_scrollbars:"Show scrollbars",
popup_statusbar:"Show status bar",
popup_toolbar:"Show toolbars",
popup_menubar:"Show menu bar",
popup_location:"Show location bar",
popup_resizable:"Make window resizable",
popup_dependent:"Dependent (Mozilla/Firefox only)",
popup_size:"Size",
popup_position:"Position (X/Y)",
id:"Id",
style:"Style",
classes:"Classes",
target_name:"Target name",
langdir:"Language direction",
target_langcode:"Target language",
langcode:"Language code",
encoding:"Target character encoding",
mime:"Target MIME type",
rel:"Relationship page to target",
rev:"Relationship target to page",
tabindex:"Tabindex",
accesskey:"Accesskey",
ltr:"Left to right",
rtl:"Right to left",
link_list:"Link list"
});