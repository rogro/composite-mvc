/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

VisualEditorBoxBinding.prototype = new Binding;
VisualEditorBoxBinding.prototype.constructor = VisualEditorBoxBinding;
VisualEditorBoxBinding.superclass = Binding.prototype;

/**
 * @class
 */
function VisualEditorBoxBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "VisualEditorBoxBinding" );
	
	/**
	 * @type {VisualEditorBinding}
	 */
	this._editorBinding = null;
	
	/**
	 * Subtree binding attachment delayed until editor has loaded.
	 * @overwrites {Binding#isLazy}
	 */
	this.isLazy = true;
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
VisualEditorBoxBinding.prototype.toString = function () {

	return "[VisualEditorBoxBinding]";
}


/**
 * Register for initialization when TinyMCE is loaded.
 * @overloads {ToolBarBinding#onBindingAttach}
 */
VisualEditorBoxBinding.prototype.onBindingAttach = function () {
	
	VisualEditorBoxBinding.superclass.onBindingAttach.call ( this );
	
	/*
	 * Rigup editor component.
	 */
	var tinywindow = this.bindingWindow.bindingMap.tinywindow;
	EditorBinding.registerComponent ( this, tinywindow );
	
	/*
	 * Size the damn cover for IE.
	 */
	if ( Client.isExplorer ) {
		var cover = this.bindingWindow.bindingMap.toolbarscover;
		cover.setHeight ( this.boxObject.getDimension ().h );
	}
}

/**
 * Setup to initialize when TinyMCE is loaded and containng EditorBinding initializes.
 * @implements {IWysiwygEditorComponent}
 * @param {WysiwygEditorBinding} editor
 * @param {TinyMCE_Engine} engine
 * @param {TinyMCE_Control} instance
 * @param {TinyMCE_CompositeTheme} theme
 */
VisualEditorBoxBinding.prototype.initializeComponent = function ( editor, engine, instance, theme ) {

	this._editorBinding = editor;
	this._editorBinding.addActionListener ( VisualEditorBinding.ACTION_INITIALIZED, this );
}

/**
 * @implements {IActionListener}
 * @overloads {Binding#handleAction}
 * @param {Action} action
 */
VisualEditorBoxBinding.prototype.handleAction = function ( action ) {

	VisualEditorBoxBinding.superclass.handleAction.call ( this, action );
	
	var binding = action.target;
	
	/*
	 * This binding is LAZY. We attach toolbar content 
	 * only when editor content has finished loading. 
	 * This will be percieved as a faster load. 
	 */
	switch ( action.type ) {
		case VisualEditorBinding.ACTION_INITIALIZED :
			if ( binding == this._editorBinding ) {
				var self = this;
				setTimeout ( function () {
					self._initialize ();
				}, 100 );
			}
			break;
	}
}

/**
 * Initialize. By default, this simply attaches subtree bindings.
 */
VisualEditorBoxBinding.prototype._initialize = function () {
	
	this._editorBinding.removeActionListener ( VisualEditorBinding.ACTION_INITIALIZED, this );
	this.attachRecursive ();
	//this.bindingWindow.bindingMap.toolbarscover.hide ();
	CoverBinding.fadeOut ( this.bindingWindow.bindingMap.toolbarscover );
}