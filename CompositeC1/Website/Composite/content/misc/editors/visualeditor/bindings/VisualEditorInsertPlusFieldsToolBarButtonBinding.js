/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

VisualEditorInsertPlusFieldsToolBarButtonBinding.prototype = new VisualEditorInsertToolBarButtonBinding;
VisualEditorInsertPlusFieldsToolBarButtonBinding.prototype.constructor = VisualEditorInsertPlusFieldsToolBarButtonBinding;
VisualEditorInsertPlusFieldsToolBarButtonBinding.superclass = VisualEditorInsertToolBarButtonBinding.prototype;

/**
 * @class
 */
function VisualEditorInsertPlusFieldsToolBarButtonBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "VisualEditorInsertPlusFieldsToolBarButtonBinding" );
	
	/**
	 * @type {boolean}
	 */
	this._isFieldsConfigured = false;
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
VisualEditorInsertPlusFieldsToolBarButtonBinding.prototype.toString = function () {

	return "[VisualEditorInsertPlusFieldsToolBarButtonBinding]";
}

/**
 * @overloads {VisualEditorInsertToolBarButtonBinding#onBindingAttach}
 */
VisualEditorInsertPlusFieldsToolBarButtonBinding.prototype.onBindingAttach = function () {
	
	VisualEditorInsertPlusFieldsToolBarButtonBinding.superclass.onBindingAttach.call ( this );
	this.addActionListener ( ButtonBinding.ACTION_COMMAND );
}

/**
 * Configure fields insertion.
 */
VisualEditorInsertPlusFieldsToolBarButtonBinding.prototype._configureFields = function ( config ) {
	
	this.popupBinding._indexMenuContent ();
	var item = this.popupBinding.getMenuItemForCommand ( "compositeInsertFieldParent" );
	var doc = this.bindingDocument;
	
	if ( item ) {
		item.dispose ();
	}
	
	item = MenuItemBinding.newInstance ( doc );
	item.setLabel( "${string:Composite.Web.VisualEditor:ContextMenu.LabelField}" );
	item.image = "${icon:fields}";
	item.imageDisabled = "${icon:fields-disabled}";
	item.setProperty ( "cmd", "compositeInsertFieldParent" );
			
	var groupnames = config.getGroupNames ();
	if ( groupnames.hasEntries ()) {
	
		var popup 	= MenuPopupBinding.newInstance ( doc );
		var body 	= popup.add ( MenuBodyBinding.newInstance ( doc ));
		var group 	= body.add ( MenuGroupBinding.newInstance ( doc ));
		
		groupnames.each ( function ( groupname ) {
			var fields = config.getFieldNames ( groupname );
			fields.each ( function ( fieldname ) {
				var i = group.add ( MenuItemBinding.newInstance ( doc ));
				i.setLabel ( fieldname );
				i.setImage ( "${icon:field}" );
				i.setProperty ( "cmd", "compositeInsertField" );
				i.setProperty ( "val", groupname + ":" + fieldname );
				group.add ( i );
			});
		});
		item.add ( popup );
	}
	
	this.popupBinding._menuGroups [ "insertions" ].getFirst ().add ( item );
	item.attachRecursive ();
	this.popupBinding._menuItems [ "compositeInsertFieldParent" ] = item;
}

/**
 * @overloads {VisualEditorInsertToolBarButtonBinding#handleAction}
 * @implements {IActionListener}
 * @param {Action} action
 */
VisualEditorInsertPlusFieldsToolBarButtonBinding.prototype.handleAction = function ( action ) {
	
	VisualEditorInsertPlusFieldsToolBarButtonBinding.superclass.handleAction.call ( this, action );
	
	var binding = action.target;
	
	switch ( action.type ) {
	
		case ButtonBinding.ACTION_COMMAND :
			if ( !this._isFieldsConfigured ) {
				if ( binding == this ) {
					var config = this._editorBinding.embedableFieldConfiguration;
					if ( config ) {
						this._configureFields ( config );
					}
				}
				this._isFieldsConfigured = true;
			}
			break;
	}
}