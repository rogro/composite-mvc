/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

VisualEditorInsertToolBarButtonBinding.prototype = new EditorToolBarButtonBinding;
VisualEditorInsertToolBarButtonBinding.prototype.constructor = VisualEditorInsertToolBarButtonBinding;
VisualEditorInsertToolBarButtonBinding.superclass = EditorToolBarButtonBinding.prototype;

/**
 * @class
 */
function VisualEditorInsertToolBarButtonBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "VisualEditorInsertToolBarButtonBinding" );
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
VisualEditorInsertToolBarButtonBinding.prototype.toString = function () {
	
	return "[VisualEditorInsertToolBarButtonBinding]";
}

/**
 * @overloads {ButtonBinding#onBindingAttach}
 */
VisualEditorInsertToolBarButtonBinding.prototype.onBindingAttach = function () {
	
	VisualEditorInsertToolBarButtonBinding.superclass.onBindingAttach.call ( this );
	this.popupBinding.addActionListener ( MenuItemBinding.ACTION_COMMAND, this );
}

/**
 * Configure fields insertion.
 */
VisualEditorInsertToolBarButtonBinding.prototype._configureFields = function ( config ) {
	
	this.popupBinding._indexMenuContent ();
	var item = this.popupBinding.getMenuItemForCommand ( "compositeInsertFieldParent" );
	var doc = this.bindingDocument;
	
	if ( item ) {
		item.dispose ();
	}
	
	item = MenuItemBinding.newInstance ( doc );
	item.setLabel ( "Field" );
	item.image = "${icon:fields}";
	item.imageDisabled = "${icon:fields-disabled}";
	item.setProperty ( "cmd", "compositeInsertFieldParent" );
			
	var groupnames = config.getGroupNames ();
	if ( groupnames.hasEntries ()) {
	
		var popup 	= MenuPopupBinding.newInstance ( doc );
		var body 	= popup.add ( MenuBodyBinding.newInstance ( doc ));
		var group 	= body.add ( MenuGroupBinding.newInstance ( doc ));
		
		groupnames.each ( function ( groupname ) {
			var fields = config.getFieldNames ( groupname );
			fields.each ( function ( fieldname ) {
				var i = group.add ( MenuItemBinding.newInstance ( doc ));
				i.setLabel ( fieldname );
				i.setImage ( "${icon:field}" );
				i.setProperty ( "cmd", "compositeInsertField" );
				i.setProperty ( "val", groupname + ":" + fieldname );
				group.add ( i );
			});
		});
		item.add ( popup );
	}
	
	this.popupBinding._menuGroups [ "insertions" ].getFirst ().add ( item );
	item.attachRecursive ();
	this.popupBinding._menuItems [ "compositeInsertFieldParent" ] = item;
}

/**
 * @implements {IActionListener}
 * @overloads {Binding#handleAction}
 * @param {Action} action
 */
VisualEditorInsertToolBarButtonBinding.prototype.handleAction = function ( action ) {
	
	VisualEditorInsertToolBarButtonBinding.superclass.handleAction.call ( this, action );
	
	var binding = action.target;
	
	switch ( action.type ) {
			
		case MenuItemBinding.ACTION_COMMAND :
		
			var cmd = binding.getProperty ( "cmd" );
			var gui = binding.getProperty ( "gui" );
			var val = binding.getProperty ( "val" );
			
			/*
			if ( this._editorBinding.hasBookmark ()) {
				this._editorBinding.restoreBookmark ();
			}
			*/
			
			this._editorBinding.handleCommand ( cmd, gui ? gui : false, val );
			
			/*
			 * Temp!!!!!!!!!!!!!!!!!!!!!!!
			 */
			var handler = this._editorBinding.getEditorWindow ().standardEventHandler;
			setTimeout ( function () {
				handler.enableNativeKeys ( true );
			}, 100 );
			break;
	}
}