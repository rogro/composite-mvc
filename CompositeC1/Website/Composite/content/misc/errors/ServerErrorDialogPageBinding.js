/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

ServerErrorDialogPageBinding.prototype = new DialogPageBinding;
ServerErrorDialogPageBinding.prototype.constructor = ServerErrorDialogPageBinding;
ServerErrorDialogPageBinding.superclass = DialogPageBinding.prototype;

/**
 * @class
 */
function ServerErrorDialogPageBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "ServerErrorDialogPageBinding" );
}

/**
 * Identifies binding.
 */
ServerErrorDialogPageBinding.prototype.toString = function () {
	
	return "[ServerErrorDialogPageBinding]";
}

/**
 * Detach the containing ViewBinding from all server control.
 * @overloads {DialogPageBinding#onBindingAttach}
 */
ServerErrorDialogPageBinding.prototype.onBindingAttach = function () {
	
	ServerErrorDialogPageBinding.superclass.onBindingAttach.call ( this );
	this.dispatchAction ( ViewBinding.ACTION_DETACH );
	this.dispatchAction ( DockTabBinding.ACTION_FORCE_CLEAN );
}

/**
 * Force unlock application.
 * @overloads {PageBinding#onAfterPageInitialize}
 */
ServerErrorDialogPageBinding.prototype.onAfterPageInitialize = function () {
	
	ServerErrorDialogPageBinding.superclass.onAfterPageInitialize.call ( this );
	
	/*
	 * Better force-unlock the GUI.
	 */
	Application.unlock ( this, true );
	
	function hideCover () {
		var cover = bindingMap.cover;
		if ( Binding.exists ( cover )) {
			CoverBinding.fadeOut ( cover );
		}
	}
	
	/* 
	 * This fade-in stuff is just for show.
	 */
	setTimeout ( hideCover, 500 );
}