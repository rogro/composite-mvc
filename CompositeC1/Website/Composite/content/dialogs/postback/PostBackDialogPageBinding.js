/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

PostBackDialogPageBinding.prototype = new DialogPageBinding;
PostBackDialogPageBinding.prototype.constructor = PostBackDialogPageBinding;
PostBackDialogPageBinding.superclass = DialogPageBinding.prototype;

/**
 * @class
 */
function PostBackDialogPageBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "PostBackDialogPageBinding" );

	/**
	 * @type {string}
	 */
	this._url = null;
	
	/**
	 * @type {List<object>}
	 */
	this._list = null;
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
PostBackDialogPageBinding.prototype.toString = function () {

	return "[PostBackDialogPageBinding]";
}

/**
 * @param {object} arg
 */
PostBackDialogPageBinding.prototype.setPageArgument = function ( arg ) {
	
	PostBackDialogPageBinding.superclass.setPageArgument.call ( this, arg );
	
	this._url = arg.url;
	this._list = arg.list;
}

/**
 * Submit on ready. Investigations should be instigated  
 * to further study how we can make this go a lot faster.
 * @overloads {DialogPageBinding#onAfterPageInitialize}
 */
PostBackDialogPageBinding.prototype.onAfterPageInitialize = function () {
	
	PostBackDialogPageBinding.superclass.onAfterPageInitialize.call ( this );
	this._submit ();
}

/**
 * Submit the data to the specified URL.
 */
PostBackDialogPageBinding.prototype._submit = function () {
	
	var form = this.bindingDocument.forms [ 0 ];
	form.action = top.Resolver.resolve ( this._url );
	
	var isDebugging = true;
	var debug = "Posting to: " + form.action +"\n\n";
	
	this._list.reset ();
	while ( this._list.hasNext ()) {
	
		var entry = this._list.getNext ();
		var input = this.bindingDocument.createElement ( "input" );
		
		input.name = entry.name;
		input.value =  entry.value;
		
		// DELETE THIS NOW???
		input.setAttribute ( "name", new String ( entry.name )); // FF4.0 beta bug!!!
		input.setAttribute ( "value", new String ( entry.value )); // FF4.0 beta bug!!!
		
		input.type = "hidden";
		form.appendChild ( input );
		
		if ( isDebugging ) {
			debug += entry.name + ": " + entry.value  + "\n";
		}
	}
	if ( isDebugging ) {
		this.logger.debug ( debug );
	}
	form.submit ();
}