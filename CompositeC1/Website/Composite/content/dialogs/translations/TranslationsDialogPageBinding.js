/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

TranslationsDialogPageBinding.prototype = new DialogPageBinding;
TranslationsDialogPageBinding.prototype.constructor = TranslationsDialogPageBinding;
TranslationsDialogPageBinding.superclass = DialogPageBinding.prototype;


/**
 * @class
 */
function TranslationsDialogPageBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "TranslationsDialogPageBinding" );
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
TranslationsDialogPageBinding.prototype.toString = function () {

	return "[TranslationsDialogPageBinding]";
}

/**
 * @overloads {DialogPageBindong#onBeforePageInitialize}
 */
TranslationsDialogPageBinding.prototype.onBeforePageInitialize = function () {

	TranslationsDialogPageBinding.superclass.onBeforePageInitialize.call ( this );
	
	var list = new List ();
	Localization.locales.each ( function ( locale ) {
		list.add ( 
			new SelectorBindingSelection ( locale, locale, false, null )
		);
	});
	
	DataManager.getDataBinding ( "sourcelanguage" ).populateFromList ( list );
	DataManager.getDataBinding ( "targetlanguage" ).populateFromList ( list );
}

/**
 * @overloads {DialogPageBindong#onDialogAccept}
 */
TranslationsDialogPageBinding.prototype.onDialogAccept = function () {
	
	var isContinue = true;
	
	var source = DataManager.getDataBinding ( "sourcelanguage" );
	var target = DataManager.getDataBinding ( "targetlanguage" );
	var deck = window.bindingMap.decks.getSelectedDeckBinding ();
	
	if ( deck.getID () == "fieldsdeck" ) {
		if ( target.isDirty ) {
			window.bindingMap.decks.select ( "warningdeck" );
			isContinue = false;
		}
	}
	if ( isContinue == true ) {
		if ( source.isDirty == true ) {
			Localization.setSourceLanguage ( source.getValue ());
		}
		if ( target.isDirty == true ) {
			Localization.setTargetLanguage ( source.getValue ());
		}
		TranslationsDialogPageBinding.superclass.onDialogAccept.call ( this );
	}
}