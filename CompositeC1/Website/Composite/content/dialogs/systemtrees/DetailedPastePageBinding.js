/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

DetailedPastePageBinding.prototype = new DialogPageBinding;
DetailedPastePageBinding.prototype.constructor = DetailedPastePageBinding;
DetailedPastePageBinding.superclass = DialogPageBinding.prototype;

/**
 * @class
 */
function DetailedPastePageBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "DetailedPastePageBinding" );
	
	/**
	 * List<SystemNode>
	 */
	this._list = null;
}

/**
 * Identifies binding.
 */
DetailedPastePageBinding.prototype.toString = function () {
	
	return "[StandardDialogPageBinding]";

}

/**
 * @overloads {PageBinding#setPageArgument}
 * @param {List<SystemNode>} nodes
 */
DetailedPastePageBinding.prototype.setPageArgument = function ( nodes ) {
	
	DetailedPastePageBinding.superclass.setPageArgument.call ( this, nodes );

	var list = new List ();
	var iterate = 0;
	nodes.each ( function ( node ) {
	    if ( node.hasDragAccept ()) { // TODO: implement highly advanced check for accept type...
		    list.add ( 
			    new SelectorBindingSelection ( 
				    node.getLabel (),
				    String ( iterate ),
				    false,
				    node.getImageProfile ()
			    )
		    );
		    iterate ++;
		}
	});
	this._list = list;
}

/**
 * @overloads {DialogPageBinding#onBeforePageInitialize}
 */
DetailedPastePageBinding.prototype.onBeforePageInitialize = function () {
	
	var manager = this.bindingWindow.DataManager;
	
	/*
	 * Fix selector.
	 */
	var selector = manager.getDataBinding ( "sibling" );
	selector.populateFromList ( this._list );
	if ( this._list.getLength () == 1 ) {
		selector.disable ();
	}
	
	/*
	 * Fix radiogroup.					
	 */
	var radiogroup = manager.getDataBinding ( "switch" );
	radiogroup.addActionListener ( RadioGroupBinding.ACTION_SELECTIONCHANGED, {
		handleAction : function ( action ) {
			switch ( radiogroup.getValue ()) {
				case "before" :
					bindingMap.insertlabel.setLabel (
						StringBundle.getString ( "ui", "Website.Dialogs.SystemTree.DetailedPaste.LabelInsertBefore" )
					);
					break;
				case "after" :
					bindingMap.insertlabel.setLabel (
						StringBundle.getString ( "ui", "Website.Dialogs.SystemTree.DetailedPaste.LabelInsertAfter" )
					);
					break;
			}
			action.consume ();	
		}
	});
	
	DetailedPastePageBinding.superclass.onBeforePageInitialize.call ( this );
}