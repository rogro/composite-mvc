/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

WebServiceErrorDialogPageBinding.prototype = new DialogPageBinding;
WebServiceErrorDialogPageBinding.prototype.constructor = WebServiceErrorDialogPageBinding;
WebServiceErrorDialogPageBinding.superclass = DialogPageBinding.prototype;

/**
 * @class
 */
function WebServiceErrorDialogPageBinding () {
	
	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "WebServiceErrorDialogPageBinding" );
	
	/**
	 * @type {SOAPFault}
	 */
	this._soapFault = null;
	
	/**
	 * @type {SOAPRequest}
	 */
	this._soapRequest = null;
	
	/**
	 * @type {SOAPRequestResponse}
	 */
	this._soapResponse = null;
}

/**
 * Identifies binding.
 */
WebServiceErrorDialogPageBinding.prototype.toString = function () {

	return "[WebServiceErrorDialogPageBinding]";
}

/**
 * @overloads {DialogPageBinding#setPageArgument}
 * @param {SOAPFault} arg
 */
WebServiceErrorDialogPageBinding.prototype.setPageArgument = function ( arg ) {
	
	this._soapFault 	= arg.soapFault;
	this._soapRequest 	= arg.soapRequest;
	this._soapResponse 	= arg.soapResponse;
	
	WebServiceErrorDialogPageBinding.superclass.setPageArgument.call ( this, arg );
}

/**
 * @overloads {DialogPageBinding#onBeforePageInitialize}
 */
WebServiceErrorDialogPageBinding.prototype.onBeforePageInitialize = function () {

	var span = this.bindingDocument.getElementById ( "operationname" );
	span.firstChild.data = this._soapFault.getOperationName ();
	
	var textarea = this.bindingDocument.getElementById ( "faultstring" );
	textarea.value = this._soapFault.getFaultString ();
	
	WebServiceErrorDialogPageBinding.superclass.onBeforePageInitialize.call ( this );
}