/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

ContentErrorDialogPageBinding.prototype = new DialogPageBinding;
ContentErrorDialogPageBinding.prototype.constructor = ContentErrorDialogPageBinding;
ContentErrorDialogPageBinding.superclass = DialogPageBinding.prototype;

/**
 * @class
 */
function ContentErrorDialogPageBinding () {
	
	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "ContentErrorDialogPageBinding" );
	
	/**
	 * Supplied as page argument.
	 * @type {SOAPFault}
	 */
	this._soapFault = null;
}

/**
 * Identifies binding.
 */
ContentErrorDialogPageBinding.prototype.toString = function () {

	return "[ContentErrorDialogPageBinding]";
}

/**
 * @param {SOAPFault} soapFault
 */
ContentErrorDialogPageBinding.prototype.setPageArgument = function ( soapFault ) {
	
	ContentErrorDialogPageBinding.superclass.setPageArgument.call ( this, soapFault );
	
	this._soapFault = soapFault;
}

/**
 * @param {SOAPFault} soapFault
 */
ContentErrorDialogPageBinding.prototype.onBeforePageInitialize = function ( soapFault ) {
	
	var string = this._soapFault.getFaultString ();
	var key = "Failed to parse html:\n\n";
	var error = null;
	
	/*
	 * Parse the HTMLTidy error message and extract the relevant part.
	 */
	if ( string.indexOf ( key ) >-1 ) {
		try {
			var split = string.split ( key )[ 1 ];
			error = split.split ( "\n" )[ 0 ];
			error = error.replace ( " - Error:", ":" );
		} catch ( exception ) {
			error = "Unknown error.";
			this.logger.error ( "Error could not be parsed!" );
		}
	} else {
		error = "Unknown error.";
	}
	
	this.bindingDocument.getElementById ( "error" ).firstChild.data = error;
	ContentErrorDialogPageBinding.superclass.onBeforePageInitialize.call ( this );
}

/**
 * Force-unlock application interface. This is 
 * needed when a switch to Preview tab was made 
 * using invalid markup.
 * @overloads {PageBinding#onAfterPageInitialize}
 */
ContentErrorDialogPageBinding.prototype.onAfterPageInitialize = function () {

	ContentErrorDialogPageBinding.superclass.onAfterPageInitialize.call ( this );
	if ( Application.isLocked ) {
		Application.unlock ( this, true );
	}
}