/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

VisualEditorDialogPageBinding.prototype = new DialogPageBinding;
VisualEditorDialogPageBinding.prototype.constructor = VisualEditorDialogPageBinding;
VisualEditorDialogPageBinding.superclass = DialogPageBinding.prototype;

/**
 * @class
 */
function VisualEditorDialogPageBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "VisualEditorDialogPageBinding" );
}

/**
 * Identifies binding.
 */
VisualEditorDialogPageBinding.prototype.toString = function () {
	
	return "[VisualEditorDialogPageBinding]";
}

/**
 * Transfer all argument properties to editor instance.
 * @param {object} arg
 */
VisualEditorDialogPageBinding.prototype.setPageArgument = function ( arg ) {
	
	/*
	 * Dialog title.
	 */
	this.label = arg.label;
	
	/*
	 * Deprecated!
	 *
	var textarea = this.bindingDocument.getElementById ( "visualeditortextarea" );
	textarea.value = arg.value;
	*/
	
	/*
	 * Editor value.
	 */
	this.bindingWindow.bindingMap.visualeditor.setValue ( arg.value );
	
	/*
	 * Configuration.
	 */ 
	var editor = this.bindingWindow.bindingMap.visualeditor;
	if ( editor && !editor.isAttached ) {
		for ( var property in arg.configuration ) {
			editor.setProperty ( property, arg.configuration [ property ]);
		}
	} else {
		throw "VisualEditorDialogPageBinding dysfunction!";
	}
	
	VisualEditorDialogPageBinding.superclass.setPageArgument.call ( this, arg );
}

/**
 * Must return a simple string.
 * @overloads {DialogPageBinding#onBindingAccept}
 */
VisualEditorDialogPageBinding.prototype.onDialogAccept = function () {
	
	this.response  = Dialog.RESPONSE_ACCEPT;
	this.result = this.bindingWindow.bindingMap.visualeditor.getValue ();

    VisualEditorDialogPageBinding.superclass.onDialogAccept.call ( this );	
}