/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

ScaleImageDialogPageBinding.prototype = new DialogPageBinding;
ScaleImageDialogPageBinding.prototype.constructor = ScaleImageDialogPageBinding;
ScaleImageDialogPageBinding.superclass = DialogPageBinding.prototype;

/**
 * @class
 */
function ScaleImageDialogPageBinding () {
	
	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "ScaleImageDialogPageBinding" );
}

/**
 * @overloads {DialogPageBinding#onBeforePageInitialize}
 */
ScaleImageDialogPageBinding.prototype.onBeforePageInitialize = function () {

	var arg = this.pageArgument;

	var ratio = arg.width / arg.height;
	var isFixedRatio = true;
	
	bindingMap.width.setValue ( arg.width );
	bindingMap.height.setValue ( arg.height );
	
	/*
	 * Dimensions.
	 */
	bindingMap.dimensions.onValueChange = function () {
		isFixedRatio = this.getValue () == "fixed";
		bindingMap.width.onValueChange ();
	}
	
	/*
	 * Unit.
	 */
	bindingMap.unit.onValueChange = function () {
		switch ( this.getValue ()) {
			case "pixels" :
				bindingMap.width.setValue ( arg.width );
				bindingMap.height.setValue ( arg.height );	
				break;
			case "percent" :
				bindingMap.width.setValue ( "100" );
				bindingMap.height.setValue ( "100" );	
				break;
		}
	}
	
	/*
	 * Width.
	 */
	bindingMap.width.onValueChange = function () {
		if ( isFixedRatio ) {
			var val = this.getValue ();
			if ( bindingMap.unit.getValue () ==  "pixels" ) {
				val = Math.round ( val / ratio )
			}
			bindingMap.height.setValue ( val );
		}
	}
	
	/*
	 * Height.
	 */
	bindingMap.height.onValueChange = function () {
		if ( isFixedRatio ) {
			bindingMap.width.setValue ( 
				Math.round ( this.getValue () * ratio )
			);
		}
	}
	
	// superduper
	ScaleImageDialogPageBinding.superclass.onBeforePageInitialize.call ( this );
}

/**
 * Set result on dialog accept.
 * @overloads {DialogPageBinding#onDialogAccept}
 */
ScaleImageDialogPageBinding.prototype.onDialogAccept = function () {
	
	var width = bindingMap.width.getResult ();
	var height = bindingMap.height.getResult ();
	
	this.result = {
		unit : bindingMap.unit.getValue () == "percent" ? "%" : "px",
		width : width,
		height : height
	}
	
	ScaleImageDialogPageBinding.superclass.onDialogAccept.call ( this );
}
