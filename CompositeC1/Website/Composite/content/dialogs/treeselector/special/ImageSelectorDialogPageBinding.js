/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

ImageSelectorDialogPageBinding.prototype = new TreeSelectorDialogPageBinding;
ImageSelectorDialogPageBinding.prototype.constructor = ImageSelectorDialogPageBinding;
ImageSelectorDialogPageBinding.superclass = TreeSelectorDialogPageBinding.prototype;

/**
 * @class
 */
function ImageSelectorDialogPageBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "ImageSelectorDialogPageBinding" );
}

/**
 * Identifies binding.
 */
ImageSelectorDialogPageBinding.prototype.toString = function () {
	
	return "[ImageSelectorDialogPageBinding]";
}

/**
 * Update image preview.
 * @overloads {TreeSelectorDialogPageBinding#_updateDisplayAndResult}
 */
ImageSelectorDialogPageBinding.prototype._updateDisplayAndResult = function () {
	
	ImageSelectorDialogPageBinding.superclass._updateDisplayAndResult.call ( this );
	
	var image 	= document.getElementById ( "previewimage" );
	var info 	= document.getElementById ( "info" );
	var disk 	= bindingMap.sizeondisk;
	var size 	= bindingMap.sizeonscreen;
	
	var url = this.result.getFirst ();
	
	// Page id is in brackets, so getting subscring from opening bracket to closing bracket
	var mediaItemId = new String(String(url).split("(")[1]).split(")")[0];
	s = Resolver.resolve("${root}/../media/") + mediaItemId;
	s += "?&action=fit&mw=220&mh=288";
	
	image.style.backgroundImage = "url('" + s + "')";	
	info.className = "image";
}

/**
 * Reset image preview.
 * @overloads {TreeSelectorDialogPageBinding#_clearDisplayAndResult}
 */
ImageSelectorDialogPageBinding.prototype._clearDisplayAndResult = function () {
	
	ImageSelectorDialogPageBinding.superclass._clearDisplayAndResult.call ( this );
	
	var image = document.getElementById ( "previewimage" );
	var info = document.getElementById ( "info" );
	
	image.style.backgroundImage = 'url("imageselector.png")';
	info.className = "";
}