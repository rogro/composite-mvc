/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

TreeSelectorToolBarBinding.prototype = new SystemToolBarBinding;
TreeSelectorToolBarBinding.prototype.constructor = TreeSelectorToolBarBinding;
TreeSelectorToolBarBinding.superclass = SystemToolBarBinding.prototype;


/**
 * @class
 * This would be the giant toolbar at the top of the main window.
 */
function TreeSelectorToolBarBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "TreeSelectorToolBarBinding" );

	/**
	* Tree position 
	* @type {int}
	*/
	this._activePosition = SystemAction.activePositions.SelectorTree;

	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
TreeSelectorToolBarBinding.prototype.toString = function () {

	return "[TreeSelectorToolBarBinding]";
}

/**
 * Contain all buttons. Overflowing buttons are moved to a popup. 
 * The margin between buttons and groups are not accounted for...
 */
TreeSelectorToolBarBinding.prototype._containAllButtons = function () {
	
	var mores = this.bindingWindow.bindingMap.moreactionstoolbargroup;
	var avail = this.bindingWindow.bindingMap.toolbar.boxObject.getDimension().w - this._moreActionsWidth - 6;
	var total = 0;
	var hides = new List ();
	
	var button, buttons = this._toolBarBodyLeft.getDescendantBindingsByLocalName ( "toolbarbutton" );
	while (( button = buttons.getNext ()) != null ) {
		if ( !button.isVisible ) {
			button.show ();
		}
		total += button.boxObject.getDimension ().w;
		if ( total >= avail ) {
			hides.add ( button );
			button.hide ();
		}
	}
	
	if ( hides.hasEntries ()) {
		
		var group = hides.getFirst ().bindingElement.parentNode;
		UserInterface.getBinding ( group ).setLayout ( ToolBarGroupBinding.LAYOUT_LAST );
		
		this._moreActions = new List ();
		while (( button = hides.getNext ()) != null ) {
			this._moreActions.add ( button.associatedSystemAction );
		}
		mores.show ();
		
	} else {
		this._moreActions = null;
		mores.hide ();
	}
}

/**
 * TreeSelectorToolBarBinding factory.
 * @param {DOMDocument} ownerDocument
 * @return {TreeSelectorToolBarBinding}
 */
TreeSelectorToolBarBinding.newInstance = function ( ownerDocument ) {

	var element = DOMUtil.createElementNS ( Constants.NS_UI, "ui:toolbar", ownerDocument );
	return UserInterface.registerBinding ( element, TreeSelectorToolBarBinding );
}