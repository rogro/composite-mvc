/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

SaveAllDialogPageBinding.prototype = new DialogPageBinding;
SaveAllDialogPageBinding.prototype.constructor = SaveAllDialogPageBinding;
SaveAllDialogPageBinding.superclass = DialogPageBinding.prototype;

/**
 * @class
 */
function SaveAllDialogPageBinding () {
	
	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "SaveAllDialogPageBinding" );
	
	/**
	 * List<string><DockTabBinding>
	 */
	this._dirtyTabs = null;
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
SaveAllDialogPageBinding.prototype.toString = function () {

	return "[SaveAllDialogPageBinding]";
}

/**
 * @overloads {PageBinding#setPageArgument}
 * @param {Map<string><DockTabBinding>} list
 */
SaveAllDialogPageBinding.prototype.setPageArgument = function ( map ) {
	
	SaveAllDialogPageBinding.superclass.setPageArgument.call ( this, map );
	this._dirtyTabs = map;
}

/**
 * @overloads {DialogPageBinding#onBeforePageInitialize}
 */
SaveAllDialogPageBinding.prototype.onBeforePageInitialize = function () {

	SaveAllDialogPageBinding.superclass.onBeforePageInitialize.call ( this );
	
	/*
	 * Sort tabs according to associated perspective.
	 */
	var perspectives = new Map ();
	this._dirtyTabs.each ( function ( key, tab ) {
		var label = tab.perspectiveNode.getLabel ();
		if ( !perspectives.has ( label )) {
			perspectives.set ( label, new List ());
		}
		perspectives.get ( label ).add ( tab );
	});
	
	/*
	 * Build fields.
	 */
	var doc = this.bindingDocument;
	var self = this;
	perspectives.each ( function ( label, list ) {
		self._buildField ( label, list );
	});
}

/**
 * @param {string} label
 * @param {List<DockTabBinding>} list
 */
SaveAllDialogPageBinding.prototype._buildField = function ( label, list ) {
	
	var doc = this.bindingDocument;
	var field = FieldBinding.newInstance ( doc );
	var desc = FieldDescBinding.newInstance ( doc );
	var data = FieldDataBinding.newInstance ( doc );
	var group = CheckBoxGroupBinding.newInstance ( doc );
	
	list.each ( function ( tab ) {
		var box = CheckBoxBinding.newInstance ( doc );
		box.setLabel ( tab.getLabel ());
		box.setResult ( tab );
		box.check ( true );
		group.add ( box );
	});
	
	desc.setLabel ( label );
	data.add ( group );
	field.add ( desc );
	field.add ( data );
	
	this.bindingWindow.bindingMap.fieldgroup.add ( field );
	field.attachRecursive ();
}