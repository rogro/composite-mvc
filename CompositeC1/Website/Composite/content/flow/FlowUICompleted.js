/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

try {
	if ( top.Application != null ) {
		
		/*
		 * force unlock all - just in case.
		 */
		top.Application.unlock ( document.title, true );
		
		/*
		 * Instructions to close the dialog are stacked on the 
		 * MessageQueue. A timeout value of zero is not enough... 
		 */
		setTimeout ( function () {
			top.MessageQueue.update ();
		}, 50 );
	}
} catch ( exception ) {
	var browser = window.navigator.userAgent;
	if ( browser.toLowerCase ().indexOf ( "gecko" ) >-1 ) {
		throw exception;
	} else {
		/*
		 * Explorer may be so cornered in its pityful 
		 * disgrace that it cannot even resolve "top"   
		 * around here. Better do nothing about this 
		 * exception and hope for the best. The joke  
		 * is that it seems to do just fine even when 
		 * the exception is encountered. Ridiculous.
		 */
	}
}