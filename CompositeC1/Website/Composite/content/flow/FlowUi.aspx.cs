/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Web;
using System.Web.UI;

using Composite;
using Composite.C1Console.Actions;
using Composite.C1Console.Events;
using Composite.Core;
using Composite.Core.WebClient;
using Composite.Core.WebClient.FlowMediators;


public partial class Composite_Management_FlowUi : FlowPage
{
    private int _startTickCount = Environment.TickCount;
    private string _consoleId = null;
    private string _uiContainerName = null;

    private const string _consoleIdParameterName = "consoleId";
    private const string _flowHandleParameterName = "flowHandle";
    private const string _elementProviderNameParameterName = "elementProvider";

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Error += new EventHandler(Composite_Management_FlowUi_Error);
        _consoleId = Request.QueryString[_consoleIdParameterName];
        string flowHandleSerialized = Request.QueryString[_flowHandleParameterName];
        string elementProviderName = Request.QueryString[_elementProviderNameParameterName];

        if (string.IsNullOrEmpty(_consoleId) == true) throw new ArgumentNullException(_consoleIdParameterName, "Missing query string parameter");
        if (string.IsNullOrEmpty(flowHandleSerialized) == true) throw new ArgumentNullException(_flowHandleParameterName, "Missing query string parameter");
        if (string.IsNullOrEmpty(elementProviderName) == true) throw new ArgumentNullException(_elementProviderNameParameterName, "Missing query string parameter");

        FlowHandle flowHandle = FlowHandle.Deserialize(flowHandleSerialized);

        try
        {
            using (GlobalInitializerFacade.CoreIsInitializedScope)
            {
                Control webControl = WebFlowUiMediator.GetFlowUi(flowHandle, elementProviderName, _consoleId, out _uiContainerName);

                if (webControl == null)
                {
                    // TODO: check if "httpContext.ApplicationInstance.CompleteRequest();" can be used
                    Response.Redirect(UrlUtils.ResolveAdminUrl("content/flow/FlowUiCompleted.aspx"), true);
                }
                else
                {
                    this.Controls.Add(webControl);
                }
            }
        }
        catch (Exception ex)
        {
            IConsoleMessageQueueItem errorLogEntry = new LogEntryMessageQueueItem { Sender = typeof(System.Web.UI.Page), Level = Composite.Core.Logging.LogLevel.Error, Message = ex.Message };
            ConsoleMessageQueueFacade.Enqueue(errorLogEntry, _consoleId);
            throw;
        }

        Response.Cache.SetCacheability(HttpCacheability.NoCache);
    }


    void Composite_Management_FlowUi_Error(object sender, EventArgs e)
    {
        Composite.Core.WebClient.ErrorServices.DocumentAdministrativeError(Server.GetLastError());
        Composite.Core.WebClient.ErrorServices.RedirectUserToErrorPage(_uiContainerName, Server.GetLastError());
    }


    protected override void OnUnload(EventArgs e)
    {
        if (Composite.RuntimeInformation.IsDebugBuild == true)
        {
            int endTickCount = Environment.TickCount;
            Log.LogVerbose("FlowUi.aspx", string.Format("Time spent serving request: {0} ms", endTickCount - _startTickCount));
            base.OnUnload(e);
        }
    }

}