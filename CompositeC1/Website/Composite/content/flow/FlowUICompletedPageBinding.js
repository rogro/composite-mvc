/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

FlowUICompletedPageBinding.prototype = new PageBinding;
FlowUICompletedPageBinding.prototype.constructor = FlowUICompletedPageBinding;
FlowUICompletedPageBinding.superclass = PageBinding.prototype;

/**
 * @class
 */
function FlowUICompletedPageBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "FlowUICompletedPageBinding" );
}

/**
 * Identifies binding.
 */
FlowUICompletedPageBinding.prototype.toString = function () {
	
	return "[FlowUICompletedPageBinding]";
}

/**
 * @overloads {Binding#onBindingDispose}
 */
FlowUICompletedPageBinding.prototype.onBindingDispose = function () {
	
	FlowUICompletedPageBinding.superclass.onBindingDispose.call ( this );
	
	if ( this._timeout ) {
		window.clearTimeout ( this._timeout );
		this._timeout = null;
	}
}

/**
 * Force unlock application and update message queue.
 * @overwrites {PageBinding#onAfterPageInitialize}
 */
FlowUICompletedPageBinding.prototype.onAfterPageInitialize = function () {
	
	/*
	 * Because of the terrible page-load driven interaction model, 
	 * this page is bound to appear at times we don't want to see it. 
	 * Show the message on a timeout to prevent weird images flashing.
	 */
	if ( window.bindingMap ) { // why not always?
		var cover = window.bindingMap.cover;
		this._timeout = window.setTimeout ( function () {
			if ( Binding.exists ( cover )) {
				CoverBinding.fadeOut ( cover );
			}
		}, 1500 );
	}
	
	/*
	 * Force unlock. Something may have gotten stuck if we are here...
	 */
	Application.unlock ( this, true );
	
	/*
	 * Fetch messages.
	 */
	setTimeout ( function () {
		MessageQueue.update ();
	}, 50 );
}