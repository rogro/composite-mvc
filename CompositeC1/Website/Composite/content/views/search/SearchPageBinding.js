/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

SearchPageBinding.prototype = new PageBinding;
SearchPageBinding.prototype.constructor = SearchPageBinding;
SearchPageBinding.superclass = PageBinding.prototype;

/**
 * @class
 */
function SearchPageBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "SearchPageBinding" );
	
	/**
	 * @type {string}
	 */
	this._provoderName = null;
	
	/**
	 * @type {string}
	 */
	this._entityToken = null;
	
	/**
	 * @type {string}
	 */
	this._searchToken = null;
}

/** 
 * Identifies binding.
 */
SearchPageBinding.prototype.toString = function () {
	
	return "[SearchPageBinding]";
}

/**
 * @overloads {PageBinding#setPageArgument}
 * @param {HashMap<string><string>} map
 */
SearchPageBinding.prototype.setPageArgument = function ( map ) {
	
	SearchPageBinding.superclass.setPageArgument.call ( this, map );
	
	if ( map ) {
	
		this._providerName	= map [ "ProviderName" ];
		this._entityToken	= map [ "EntityToken" ];
		this._searchToken	= map [ "SerializedSearchToken" ];
		
		if ( !this._isValidSearch ()) {
			throw "SearchPageBinding argument dysfunction.";
		} else if ( this._isPageBindingInitialized ) {
			bindingMap.tree.clear ();
			this._buildSearchResultTree ();
		}
	}
}

/**
 * @overloads {PageBinding#onBeforePageInitialize}
 */
SearchPageBinding.prototype.onBeforePageInitialize = function () {
	
	if ( this._isValidSearch ()) {
		this._buildSearchResultTree ();
	}
	SearchPageBinding.superclass.onBeforePageInitialize.call ( this );
}

/**
 * Is valid search?
 * @return {boolean}
 */
SearchPageBinding.prototype._isValidSearch = function () {

	return (
		this._providerName != null && 
		this._entityToken != null && 
		this._searchToken != null
	);
}

/**
 * Launch new search dialog, providing existing search token.
 */
SearchPageBinding.prototype.newSearch = function () {
	
	var url = Dialog.URL_TREESEARCH;
	
	if ( this._isValidSearch () ) {
		url += "?SearchToken=" + this._searchToken;
		url += "&EntityToken=" + this._entityToken;
		url += "&ProviderName=" + this._providerName;
		Dialog.invokeModal ( url );
	} else {
		Dialog.error ( "UARGH?", "How to handle fresh search?" );
	}
	
	this._providerName 	= null;
 	this._entityToken 	= null;
	this._searchToken 	= null;
}

/**
 * Build search result tree.
 */
SearchPageBinding.prototype._buildSearchResultTree = function () {
	
	if ( this._isValidSearch ()) {
	
		bindingMap.tree.searchToken = this._searchToken;
		
		alert ( "tree.searchToken REFACTORED!" );
		
		var list = new List ( 
				
			/*
			 * THIS WSMETHOD ALSO REFACTORED!
			 */
				
			TreeService.GetElementsBySearchToken ( 
				this._providerName,
				this._entityToken,
				this._searchToken	
			)
		);
		if ( list.hasEntries ()) {
			while ( list.hasNext ()) {
				var node = SystemTreeNodeBinding.newInstance ( 
					new SystemNode ( 
						list.getNext ()
					), 
					this.bindingDocument 
				)
				bindingMap.tree.add ( node ); 
				node.attach ();
			}
			bindingMap.decks.select ( "resultdeck" );
		} else {
			bindingMap.decks.select ( "noresultdeck" );		
		}
	}
}

/**
 * Executed when the page is shown.
 *
SearchPageBinding.prototype.onAfterPageInitialize = function () {
	
	SystemPageBinding.superclass.onAfterPageInitialize.call ( this );
	
	if ( this._isValidSearch ()) {
		setTimeout ( function () {
			bindingMap.tree.focus ();
			bindingMap.tree.selectDefault ();
		}, 500 );
	}
}
*/