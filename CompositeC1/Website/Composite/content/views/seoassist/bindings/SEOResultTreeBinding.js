/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

SEOResultTreeBinding.prototype = new TreeBinding;
SEOResultTreeBinding.prototype.constructor = SEOResultTreeBinding;
SEOResultTreeBinding.superclass = TreeBinding.prototype;

/**
 * @class
 */
function SEOResultTreeBinding () {
	
	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "SEOResultTreeBinding" );
	
	/**
	 * @type {string}
	 */
	this.keyword = null;
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
SEOResultTreeBinding.prototype.toString = function () {

	return "[SEOResultTreeBinding]";
};

/**
 * @overloads {TreeBinding#focusSingleTreeNodeBinding}
 * @param {TreeNodeBinding} binding
 */
SEOResultTreeBinding.prototype.focusSingleTreeNodeBinding = function (binding) {

	SEOResultTreeBinding.superclass.focusSingleTreeNodeBinding.call(this, binding);

	var key = null;
	if (binding instanceof SEOResultTreeNodeBinding) {
		key = binding.getLabel();
	} else {
		var treenode = binding.getAncestorBindingByLocalName("treenode");
		if (treenode != null)
			key = treenode.getLabel();
	}
	if (key != null && key != this.keyword) {
		this.keyword = key;
		EventBroadcaster.broadcast(BroadcastMessages.HIGHLIGHT_KEYWORDS,
			new List([key])
		);
	}
};