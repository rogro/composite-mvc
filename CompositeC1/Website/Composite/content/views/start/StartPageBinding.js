/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

StartPageBinding.prototype = new PageBinding;
StartPageBinding.prototype.constructor = StartPageBinding;
StartPageBinding.superclass = PageBinding.prototype;

/**
 * @class
 */
function StartPageBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "StartPageBinding" );
	
	/**
	 * This object gets loaded in the start page frame.
	 * @type {CompositeStart}
	 */
	this._starter = null;
}

/**
 * Identifies binding.
 */
StartPageBinding.prototype.toString = function () {
	
	return "[StartPageBinding]";
}

/**
 * @overloads {PageBinding#onBindingRegister}
 */
StartPageBinding.prototype.onBindingRegister = function () {
	
	StartPageBinding.superclass.onBindingRegister.call ( this );
	this.addActionListener ( WindowBinding.ACTION_ONLOAD );
	this.addActionListener ( ControlBinding.ACTION_COMMAND );
	EventBroadcaster.subscribe ( BroadcastMessages.START_COMPOSITE, this );
	EventBroadcaster.subscribe ( BroadcastMessages.STOP_COMPOSITE , this );
	EventBroadcaster.subscribe ( BroadcastMessages.COMPOSITE_START, this );
}

/**
 * Load start page with a random querystring to avoid client cache.
 * @overloads {PageBinding#onBindingAttach}
 */
StartPageBinding.prototype.onBindingAttach = function () {
	
	/*
	 * compositestart/CompositeStart.aspx
	 */
	StartPageBinding.superclass.onBindingAttach.call ( this );
	this.bindingWindow.bindingMap.start.setURL ( 
		"GetStartPage.ashx?random=" + KeyMaster.getUniqueKey ()
	);
}

/**
 * @implements {IActionListener}
 * @overloads {PageBinding#handleAction}
 * @param {Action} action
 */
StartPageBinding.prototype.handleAction = function ( action ) {

	StartPageBinding.superclass.handleAction.call ( this, action );
	
	var binding = action.target;
	
	switch ( action.type ) {
	
		case WindowBinding.ACTION_ONLOAD :
		
			if ( action.target == bindingMap.start ) {
				this._starter = bindingMap.start.getContentWindow ().CompositeStart;
				if ( this._starter ) {
					this._starter.start ();
				}
			}
			break;
			
		case ControlBinding.ACTION_COMMAND :
		
			if ( action.target == bindingMap.closecontrol ) {
				if ( bindingMap.cover ) {
					bindingMap.cover.show ();
				}
				if ( this._starter ) {
					this._starter.stop ();
				}
			}
			break;
	}
}

/**
 * @implements {IBroadcastListener}
 * @overloads {PageBinding#handleBroadcast}
 * @param {string} broadcast
 * @param {object} arg
 */
StartPageBinding.prototype.handleBroadcast = function ( broadcast, arg ) {

	StartPageBinding.superclass.handleBroadcast.call ( this, broadcast, arg );
	
	switch ( broadcast ) {
			
		case BroadcastMessages.START_COMPOSITE :
			if ( this._starter ) {
				this._starter.start ();
			}
			break;
			
		case BroadcastMessages.STOP_COMPOSITE :
			if ( this._starter ) {
				this._starter.stop ();
			}
			break;
			
		case BroadcastMessages.COMPOSITE_START : // broadcated by CompositeStart when ready
			if ( bindingMap.cover ) {
				bindingMap.cover.hide ();
			}
			break;
	}
}