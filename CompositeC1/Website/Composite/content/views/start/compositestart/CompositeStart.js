/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

/*
 * @class
 * This fellow is required around here. It provides a minimal interface 
 * for interaction with the Composite framework. Use the "start" method 
 * to start a Flash movie or whatever when the start page is shown. Use 
 * the "stop" method to stop processor-intensive stuff when page exits.
 */
var CompositeStart = new function () {
	
	/**
	 * Invoked by framework when page is shown. Do whatever you 
	 * like around here but REMEMBER to broadcast when done.
	 */
	this.start = function () {
		
		/*
		 * Notice that we import top level classes for use in this window. 
		 * Although we don't actually use this for anything...
		 */
		if ( top.Application ) {
			if ( !window.Application ) {
				top.Application.declareTopLocal ( window );
				customStartupStuff ();
			}
			EventBroadcaster.broadcast ( BroadcastMessages.COMPOSITE_START );
		}
	}
	
	/**
	 * Invoked by framework when page gets hidden. Do whatever 
	 * you like around here but REMEMBER to broadcast when done!
	 */
	this.stop = function () {
		
		if ( window.Application ) {
			EventBroadcaster.broadcast ( BroadcastMessages.COMPOSITE_STOP );
		}
	}
	
	/**
	 * Startup stuff.
	 */
	function customStartupStuff () {
		
		if ( document.getElementById ( "content" )) {
			sizeToFit ();
			window.onresize = sizeToFit;
		}
	}
	
	/**
	 * Size to fit.
	 */
	function sizeToFit () {
		
		var height = window.innerHeight ? window.innerHeight : document.body.clientHeight;
		var titlebar = document.getElementById ( "titlebar" );
		var content = document.getElementById ( "content" );
		content.style.height = ( height - titlebar.offsetHeight ) + "px";
	}
}