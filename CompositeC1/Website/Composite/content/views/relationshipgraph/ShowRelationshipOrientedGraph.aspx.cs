/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using Composite.C1Console.Security;
using Composite.Core.Configuration;
using Composite.Core.IO;
using Composite.Core.Types;


namespace Composite.content.views.relationshipgraph
{
    public partial class ShowRelationshipOrientedGraph : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string idString = Request.QueryString["Id"];
            if (string.IsNullOrEmpty(idString) == true)
            {
                return;
            }

            Guid id = new Guid(idString);
            string filename = Path.Combine(PathUtil.Resolve(GlobalSettingsFacade.TempDirectory), string.Format("{0}.RelationshipGraph", id));

            EntityToken startEntityToken = EntityTokenSerializer.Deserialize(C1File.ReadAllLines(filename)[0]);

            RelationshipOrientedGraph graph = new RelationshipOrientedGraph(startEntityToken);

            IEnumerable<IEnumerable<EntityToken>> paths = graph.Root.GetAllPaths();

            RelationshipOrientedGraphPlaceHolder.Controls.Add(new LiteralControl(string.Format("<div><b>Path count: {0}</b></div>", paths.Count())));

            int pathCounter = 1;
            foreach (IEnumerable<EntityToken> path in paths)
            {
                EntityTokenHtmlPrettyfierHelper helper = new EntityTokenHtmlPrettyfierHelper();
                helper.StartTable();
                                
                helper.AddHeading(string.Format("<b>Path: {0}</b>", pathCounter++));                

                int levelCounter = 0;
                foreach (EntityToken entityToken in path)
                {
                    helper.StartRow();
                    helper.AddCell(string.Format("<center><b>Level: {0}</b></center>", levelCounter++), 2, "#aaaaaa");
                    helper.EndRow();

                    helper.StartRow();
                    helper.AddCell("<b>Id</b>");
                    helper.AddCell(entityToken.OnGetIdPrettyHtml());
                    helper.EndRow();

                    helper.StartRow();
                    helper.AddCell("<b>Type</b>");
                    helper.AddCell(entityToken.OnGetTypePrettyHtml());
                    helper.EndRow();

                    helper.StartRow();
                    helper.AddCell("<b>Source</b>");
                    helper.AddCell(entityToken.OnGetSourcePrettyHtml());                    
                    helper.EndRow();

                    string extra = entityToken.OnGetExtraPrettyHtml();
                    if (string.IsNullOrEmpty(extra) == false)
                    {
                        helper.StartRow();
                        helper.AddCell("<b>Extra</b>");
                        helper.AddCell(extra);
                        helper.EndRow();
                    }

                    helper.StartRow();
                    helper.AddCell("<b>RTT</b>");
                    helper.AddCell(TypeManager.SerializeType(entityToken.GetType()));
                    helper.EndRow();
                }                                                
                
                helper.EndTable();

                RelationshipOrientedGraphPlaceHolder.Controls.Add(new LiteralControl(helper.GetResult()));
            }
        }        
    }
}