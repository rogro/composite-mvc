/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

ImageSelectionBinding.prototype = new Binding;
ImageSelectionBinding.prototype.constructor = ImageSelectionBinding;
ImageSelectionBinding.superclass = Binding.prototype;

/**
 * @class
 * Largely controlled by the {@link ImageStageBinding}.
 */
function ImageSelectionBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "ImageSelectionBinding" );
	
	/**
	 * @type {boolean}
	 */
	this._state = true;
	
	/**
	 * @type {object}
	 */
	this.geometry = {
		w : 0,
		h : 0,
		x : 0,
		y : 0
	}
}

/**
 * Identifies binding.
 */
ImageSelectionBinding.prototype.toString = function () {
	
	return "[ImageSelectionBinding]";
}

/**
 * Hide on startup.
 */
ImageSelectionBinding.prototype.onBindingAttach = function () {
	
	ImageSelectionBinding.superclass.onBindingAttach.call ( this );
	this.hide ();
}

/**
 * Repaint.
 */
ImageSelectionBinding.prototype.repaint = function () {
	
	this.setW ( this.geometry.w );
	this.setH ( this.geometry.h );
	this.setX ( this.geometry.x );
	this.setY ( this.geometry.y );
	
	// TODO: squares!
}

/**
 * Update statustext when hiding.
 * TODO: put this elsewhere?
 * @overloads {Binding#hide}
 */
ImageSelectionBinding.prototype.hide = function () {
	
	ImageSelectionBinding.superclass.hide.call ( this );
	if ( bindingMap.statustext ) {
		bindingMap.statustext.setLabel ( "", true );
	}
}

/**
 * Set width.
 * @param {int} w
 */
ImageSelectionBinding.prototype.setW = function ( w ) {
	
	this.bindingElement.style.width = new String ( w * ImageEditor.scale ) + "px";
	this.geometry.w = w;
}

/**
 * Set height.
 * @param {int} h
 */
ImageSelectionBinding.prototype.setH = function ( h ) {
	
	this.bindingElement.style.height = new String ( h * ImageEditor.scale ) + "px";
	this.geometry.h = h;
}

/**
 * Set x.
 * @param {int} x
 */
ImageSelectionBinding.prototype.setX = function ( x ) {
	
	this.bindingElement.style.left = new String ( x * ImageEditor.scale ) + "px";
	this.geometry.x = x;
}

/**
 * Set y.
 * @param {int} y
 */
ImageSelectionBinding.prototype.setY = function ( y ) {
	
	this.bindingElement.style.top = new String ( y * ImageEditor.scale ) + "px";
	this.geometry.y = y;
}