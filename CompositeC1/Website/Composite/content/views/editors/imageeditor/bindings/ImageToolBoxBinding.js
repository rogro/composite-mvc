/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

ImageToolBoxBinding.prototype = new Binding;
ImageToolBoxBinding.prototype.constructor = ImageToolBoxBinding;
ImageToolBoxBinding.superclass = Binding.prototype;

/**
 * @class
 */
function ImageToolBoxBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "ImageToolBoxBinding" );
	
	/**
	 * @type {Point}
	 */
	this._startPosition = null;
}

/**
 * Identifies binding.
 */
ImageToolBoxBinding.prototype.toString = function () {
	
	return "[ImageToolBoxBinding]";
}

ImageToolBoxBinding.prototype.onBindingAttach = function () {
	
	ImageToolBoxBinding.superclass.onBindingAttach.call ( this );
	EventBroadcaster.subscribe ( this.bindingWindow.WindowManager.WINDOW_RESIZED_BROADCAST, this );
}

/**
 * Implements {@link IBroadcastListener}
 * @param {string} broadcast
 * @param {object} arg
 */
ImageToolBoxBinding.prototype.handleBroadcast = function ( broadcast, arg ) {
	
	ImageToolBoxBinding.superclass.handleBroadcast.call ( this, broadcast, arg );
	
	switch ( broadcast ) {
		case this.bindingWindow.WindowManager.WINDOW_RESIZED_BROADCAST :
			this._startPosition = this.getPosition ();
			this._setComputedPosition ( 
				new Point ( 0, 0 )
			);
			break;
	}
}

/**
 * Implements {@link IDragHandler}
 * @param {Point} point
 */
ImageToolBoxBinding.prototype.onDragStart = function ( point ) {
	
	this._startPosition = this.getPosition ();
}

/**
 * Implements {@link IDragHandler}
 * @param {Point} diff
 */
ImageToolBoxBinding.prototype.onDrag = function ( diff ) {

	this._setComputedPosition ( diff );
}

/**
 * Implements {@link IDragHandler}
 * @param {Point} diff
 */
ImageToolBoxBinding.prototype.onDragStop = function ( diff ) {
	
	this.onDrag ( diff );
	this._startPosition = null;
}

/**
 * Keep toolbox on stage.
 * @param {Point} diff
 */
ImageToolBoxBinding.prototype._setComputedPosition = function ( diff ) {

	var dim1 = this.boxObject.getDimension ();
	var dim2 = bindingMap.imagestage.boxObject.getDimension ();

	var x = this._startPosition.x + diff.x;
	var y = this._startPosition.y + diff.y;
	
	x = x < 0 ? 0 : x + dim1.w > dim2.w ? dim2.w - dim1.w : x;
	y = y < 0 ? 0 : y + dim1.h > dim2.h ? dim2.h - dim1.h : y;
	
	this.setPosition ( 
		new Point ( x, y )
	)
}

/**
 * Set position.
 * @param {Point} point
 */
ImageToolBoxBinding.prototype.setPosition = function ( point ) {
	
	this.bindingElement.style.left = point.x + "px";
	this.bindingElement.style.top = point.y + "px";
}

/**
 * Get position.
 * @return {Point}
 */
ImageToolBoxBinding.prototype.getPosition = function () {
	
	return new Point ( 
		this.bindingElement.offsetLeft,
		this.bindingElement.offsetTop
	);
}

/**
 * Get position.
 * @return {Point}
 */
ImageToolBoxBinding.prototype.getDimension = function () {
	
	return this.boxObject.getDimension ();
}