/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

ImageEditorPageBinding.prototype = new EditorPageBinding;
ImageEditorPageBinding.prototype.constructor = ImageEditorPageBinding;
ImageEditorPageBinding.superclass = EditorPageBinding.prototype;


/**
 * @class
 */
function ImageEditorPageBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "ImageEditorPageBinding" );
	
	/**
	 * Handle of the view that contains us.
	 * @type {string}
	 */
	this.viewhandle = null;
}

/**
 * Identifies binding.
 */
ImageEditorPageBinding.prototype.toString = function () {
	
	return "[ImageEditorPageBinding]";
}

/**
 * Overloads.
 */
ImageEditorPageBinding.prototype.onBindingRegister = function () {
	
	ImageEditorPageBinding.superclass.onBindingRegister.call ( this );
	this.addActionListener ( ImageBoxBinding.ACTION_INITIALIZED, this );
}

/**
 * Overloads.
 */
ImageEditorPageBinding.prototype.onBindingAttach = function () {
	
	ImageEditorPageBinding.superclass.onBindingAttach.call ( this );
	
	/*
	 * Extract the view handle. Backend needs to know.
	 */
	var view = this.getAncestorBindingByLocalName ( "view", true );
	var def = view.getDefinition ();
	this.viewhandle = def.handle;
	
	/*
	 * Connect zoom selector.
	 */
	var selector = bindingMap.zoomselector;
	selector.onValueChange = function () {
		ImageEditor.zoomTo ( selector.getValue ());
	}
	
	/**
	 * Setup zoom menu.
	 */
	bindingMap.zoommenugroup.addActionListener ( 
		MenuItemBinding.ACTION_COMMAND, this 
	);
}

/**
 * Pause page initialization.
 * @overwrites {PageBinding#onBeforePageInitialize}
 */
ImageEditorPageBinding.prototype.onBeforePageInitialize = function () {
	
	/*
	 * Do nothing - waiting for the image to load. 
	 * @see {ImageEditor#PageBindinghandleAction}
	 */
}

/**
 * @implements {IActionListener}
 * @overloads {EditorPageBinding#handleAction}
 * @param {Action} action
 */
ImageEditorPageBinding.prototype.handleAction = function ( action ) {

	ImageEditorPageBinding.superclass.handleAction.call ( this, action );
	
	switch ( action.type ){
		case ImageBoxBinding.ACTION_INITIALIZED :
			
			/*
			 * This stunt is somewhat hacked...
			 */
			var src = String ( decodeURIComponent ( action.target.getImageSource ()));
			var temp1 = src.split ( "MediaArchive:" )[ 1 ];
			var temp2 = temp1.split ( "&" )[ 0 ];
			var temp3 = temp2.split ( "/" );
			var temp4 = temp3 [ temp3.length - 1 ]; 
			
			this.label = temp4;
			
			if ( !this._isPageBindingInitialized ) {
				ImageEditorPageBinding.superclass.onBeforePageInitialize.call ( this );
			}
			break;
		case MenuItemBinding.ACTION_COMMAND :
			var item = action.target;
			ImageEditor.zoomTo ( 
				item.getProperty ( "zoom" )
			);
			break;
	}
}

/**
 * @overwrites {EditorPageBinding#save}
 */
ImageEditorPageBinding.prototype._saveEditorPage = function () {
	
	ImageEditorActions.save ();
	
	/*
	 * TODO!
	 */
	this.logger.error ( "TODO: MessageQueue SaveStatus!!!!!!!!!!!!!!!!!!!!!!!!!!" );
	setTimeout ( function () {
		MessageQueue.update ();
	}, 50 );
}