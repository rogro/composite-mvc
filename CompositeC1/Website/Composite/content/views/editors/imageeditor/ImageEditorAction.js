/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

ImageEditorAction.TYPE_CROP 		= "c"; // x,y,w,h
ImageEditorAction.TYPE_SCALE 		= "s"; // 'px'|'%',w,h
ImageEditorAction.TYPE_ROTATE 		= "r"; // d
ImageEditorAction.TYPE_FLIP 		= "f"; // true|false (horizontal)
ImageEditorAction.TYPE_SELECT		= "x"; // x,y,w,h
ImageEditorAction.TYPE_SAVE			= "save";

/**
 * String values to be placed in quotes by this method.
 * @param {object} value
 * @return {string}
 */
ImageEditorAction.compute = function ( value ) {
	
	if ( parseInt ( value ).toString () == value || parseFloat ( value ).toString () == value || value == true || value == false ) {
		value = new String ( encodeURIComponent ( value ));
	} else {
		value = new String ( "\"" + encodeURIComponent ( value ) + "\"" );
	}
	return value;
}

/**
 * @class
 * @param {string} type
 * @param {array} args
 */
function ImageEditorAction ( type, args ) {
	
	this.type = type;
	this.args = args;
}

/**
 * @return {string}
 */ 
ImageEditorAction.prototype.toString = function () {

	var result = this.type;

	if ( this.args ) {
		result += "("
		var args = new List ( this.args );
		while ( args.hasNext ()) {
			result += ImageEditorAction.compute ( args.getNext ());
			if ( args.hasNext ()) {
				result += ",";
			}
		}
		result += ")"
	}
	return result;	
}
