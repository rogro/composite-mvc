/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

/**
 * @class
 */
var ImageEditor = new function () {
	
	var logger = SystemLogger.getLogger ( "ImageEditor" );
	var scales = [ 0.12, 0.25, 0.5, 1, 2, 4, 8 ];
	var index = 3;
		
	this.MODE_MOVE 		= "move";
	this.MODE_SELECT 	= "select";
	this.MODE_ZOOMIN 	= "zoomin";
	this.MODE_ZOOMOUT 	= "zoomout";
	
	/**
	 * @type {number}
	 */
	this.scale = scales [ index ];
	
	/**
	 * @type {string}
	 */
	this.mode = null;
	
	/**
	 * True while user is dragging a new selection.
	 * @type {boolean}
	 */
	this.isSelecting = false;
	 
	/**
	 * Set mode.
	 * @param {string} state
	 */
	this.setMode = function ( mode ) {
		
		if ( this.mode ) {
			bindingMap.imagestage.detachClassName ( this.mode );
		}
		bindingMap.imagestage.attachClassName ( mode );
		bindingMap.imagecursor.setMode ( mode );
		this.mode = mode;
	}
	
	/**
	 * Implements {@link IBroadcastListener}
	 * @param {string} broadcast
	 */
	this.handleBroadcast = function ( broadcast ) {
		
		switch ( broadcast ) {
			case BroadcastMessages.KEY_SHIFT_DOWN :
				if ( this.mode == ImageEditor.MODE_ZOOMIN ) {
					this.setMode ( ImageEditor.MODE_ZOOMOUT );
				}
				break;
			case BroadcastMessages.KEY_SHIFT_UP :
				if ( this.mode == ImageEditor.MODE_ZOOMOUT ) {
					this.setMode ( ImageEditor.MODE_ZOOMIN );
				}
				break;
			case WindowManager.WINDOW_UNLOADED_BROADCAST :
				EventBroadcaster.subscribe ( BroadcastMessages.KEY_SHIFT_DOWN, this );
				EventBroadcaster.subscribe ( BroadcastMessages.KEY_SHIFT_UP, this );
				break;
		}
	}
	
	EventBroadcaster.subscribe ( BroadcastMessages.KEY_SHIFT_DOWN, this );
	EventBroadcaster.subscribe ( BroadcastMessages.KEY_SHIFT_UP, this );
	EventBroadcaster.subscribe ( WindowManager.WINDOW_UNLOADED_BROADCAST, this );
	
	/**
	 * Zoom in.
	 */
	this.zoomIn = function () {
		
		if ( index + 1 < scales.length ) {
			this.scale = scales [ ++ index ];
			this._updateZoomBindings ();
			repaint ();
		}
		
	}
	
	/**
	 * Zoom out.
	 */
	this.zoomOut = function () {
		
		if ( index > 0 ) {
			this.scale = scales [ -- index ];
			this._updateZoomBindings ();
			repaint ();
		}
	}
	
	/**
	 * Zoom to specified index.
	 * @param {int} newIndex
	 */
	this.zoomTo = function ( newIndex ) {
		
		newIndex = Number ( newIndex );
		
		if ( newIndex != index ) {
			index = newIndex;
			this.scale = scales [ index ];
			this._updateZoomBindings ();
			repaint ();
		}
	}
	
	/**
	 * Update zoom broadcasters and zoom selector.
	 */
	this._updateZoomBindings = function () {
		
		// broadcasters
		var b1 = bindingMap.broadcasterCanZoomIn;
		var b2 = bindingMap.broadcasterCanZoomOut;
		
		var isMin = index == 0;
		var isMax = index == scales.length - 1;
		
		if ( isMin ) {
			b2.disable ();
			b1.enable ();
		} else if ( isMax ) {
			b2.enable ();
			b1.disable ();
		} else {
			b2.enable ();
			b1.enable ();
		}
		
		// zoom selector
		bindingMap.zoomselector.selectByValue ( 
			index, true 
		);
		
		// zoom menugroup
		var group = bindingMap.zoommenugroup;
		var items = group.getChildBindingsByLocalName ( "menuitem" );
		items.each ( function ( item ) {
			if ( item.getProperty ( "zoom" ) == index ) {
				item.check ( true );
			} else {
			 	item.uncheck ( true );
			}
		});
	}
	
	/**
	 * Snap value to nearest number.
	 * @param {number} value
	 * @param {number} number
	 */
	this.grid = function ( value, number ) {
		
		var ceil = Math.ceil ( value );
		var remainder = value % number;
		if ( remainder > 0 ) {
			value = value - remainder + number;
		}
	  	return value;
	}
		
	
	/**
	 * Repaint stuff.
	 */
	function repaint () {
		
		bindingMap.imagestagecontainer.bindingElement.style.visibility = "hidden";
		bindingMap.imagebox.repaint ();
		bindingMap.imageselection.repaint ();
		bindingMap.imagescrollbox.repaint ();
		bindingMap.imagestagecontainer.bindingElement.style.visibility = "visible";
	}
}
