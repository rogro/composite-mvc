/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

PermissionEditorHeadBinding.prototype = new Binding;
PermissionEditorHeadBinding.prototype.constructor = PermissionEditorHeadBinding;
PermissionEditorHeadBinding.superclass = Binding.prototype;

/**
 * @class
 */
function PermissionEditorHeadBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "PermissionEditorHeadBinding" );
	
	/**
	 * @type {Map<string><int>}
	 */
	this._indexes = new Map ();
	
	/**
	 * @type {int}
	 */
	this._count = 0;
}

/**
 * Identifies binding.
 */
PermissionEditorHeadBinding.prototype.toString = function () {
	
	return "[PermissionEditorHeadBinding]";
}

/** 
 * Set headings.
 * @param {List<object>}
 */
PermissionEditorHeadBinding.prototype.setHeadings = function ( list ) {
	
	var row = this.bindingElement.rows [ 0 ];
	var indexes = this._indexes;
	
	list.each ( function ( object ) {
		var th = DOMUtil.createElementNS ( Constants.NS_XHTML, "th", document );
		th.appendChild ( document.createTextNode ( object.Value ));
		row.appendChild ( th );
		indexes.set ( object.Key, th.cellIndex );
	});
}

/**
 * Get cell index for permission type key.
 * @param {string} key
 */
PermissionEditorHeadBinding.prototype.getIndexForPermission = function ( key ) {
	
	return this._indexes.get ( key );
}

/**
 * Get permission type key for cell index.
 * @param {string} key
 */
PermissionEditorHeadBinding.prototype.getPermissionForIndex = function ( index ) {
	
	if ( !this._keys ) {
		this._keys = this._indexes.inverse ();
	}
	return this._keys.get ( index );
}

/**
 * Get permission type count.
 * @return {int}
 */
PermissionEditorHeadBinding.prototype.getPermissionTypeCount = function () {
	
	if ( !this._count ) {
		this._count = this._indexes.countEntries ();
	}
	return this._count;
}