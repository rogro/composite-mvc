/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Web.UI;
using Composite.C1Console.Security;
using Composite.Core.Configuration;
using Composite.Core.IO;
using Composite.Core.Serialization;


public partial class ShowElementInformation_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["PiggyBagId"] == null)
        {
            ElementInformationPlaceHolder.Controls.Add(new LiteralControl("No entity token.... nothing to do.... "));

            return;
        }

        Guid piggybagId = new Guid(Request.QueryString["PiggyBagId"]);
        string filename = Path.Combine(PathUtil.Resolve(GlobalSettingsFacade.TempDirectory), string.Format("{0}.showinfo", piggybagId));

        string[] showinfo = C1File.ReadAllLines(filename);

        string serializedEntityToken = showinfo[0];
        string serializedPiggyBag = showinfo[1];

        EntityToken entityToken = EntityTokenSerializer.Deserialize(serializedEntityToken);

        Dictionary<string, string> dic = StringConversionServices.ParseKeyValueCollection(serializedPiggyBag);
        Dictionary<string, string> piggybag = new Dictionary<string, string>();
        foreach (var kvp in dic)
        {
            piggybag.Add(kvp.Key, StringConversionServices.DeserializeValueString(kvp.Value));
        }

        string entityTokenHtml = entityToken.GetPrettyHtml(piggybag);

        ElementInformationPlaceHolder.Controls.Add(new LiteralControl(entityTokenHtml));
    }
}
