/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

GenericPageBinding.prototype = new PageBinding;
GenericPageBinding.prototype.constructor = GenericPageBinding;
GenericPageBinding.superclass = PageBinding.prototype;

/**
 * @class
 */
function GenericPageBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "GenericPageBinding" );
	
	/**
	 * URL to either 1) load directly or 2) set as action on the form. 
	 * @type {string}
	 */
	this._url = null;
	
	/**
	 * Postback arguments.
	 * @type {Map<String><String>}
	 */
	this._map = null;
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
GenericPageBinding.prototype.toString = function () {

	return "[GenericPageBinding]";
}

/**
 * @overloads {PageBinding#setPageArgument}
 * @param {object} arg
 */
GenericPageBinding.prototype.setPageArgument = function ( arg ) {
	
	GenericPageBinding.superclass.setPageArgument.call ( this, arg );
	
	this._url = arg.url;
	this._map = arg.map;
	
	var win = this.bindingWindow.bindingMap.window;
	if ( this._list.hasEntries ()) {
		win.setURL ( WindowBinding.POSTBACK_URL );
		this.addActionListener ( WindowBinding.ACTION_LOADED );
	} else {
		win.setURL ( this._url );
	}
}

/**
 * @overloads {PageBinding#handleAction}
 * @implements {IActionListener}
 * @param {Action} action 
 */
GenericPageBinding.prototype.handleAction = function ( action ) {
	
	GenericPageBinding.superclass.handleAction.call ( this, action );
	
	switch ( action.type ) {
		case WindowBinding.ACTION_LOADED :
			var win = action.target.getContentWindow ();
			if ( win.isPostBackDocument ) {
				action.target.post ( this._list, this._url );
			}
			action.consume ();
			break;
	}
}

/**
 * @overloads {PageBinding#onBeforePageInitialize}
 */
GenericPageBinding.prototype.onBeforePageInitialize = function () {
	
	GenericPageBinding.superclass.onBeforePageInitialize.call ( this );
}