/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

SystemLogPageBinding.prototype = new PageBinding;
SystemLogPageBinding.prototype.constructor = SystemLogPageBinding;
SystemLogPageBinding.superclass = PageBinding.prototype;

/**
 * @class
 */
function SystemLogPageBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "SystemLogPageBinding" );
}

/**
 * Identifies binding.
 */
SystemLogPageBinding.prototype.toString = function () {
	
	return "[SystemLogPageBinding]";
}

/**
 * Broadcast log opened.
 * @overloads {PageBinding#onBindingRegister}
 */
SystemLogPageBinding.prototype.onBindingRegister = function () {

	SystemLogPageBinding.superclass.onBindingRegister.call ( this );
	
	this.addActionListener ( WindowBinding.ACTION_LOADED, {
		handleAction : function ( action ) {
			EventBroadcaster.broadcast (
				BroadcastMessages.SYSTEMLOG_OPENED, 
				action.target.getContentWindow ()
			);
		}
	});
}

/**
 * Broadcast log closed.
 * @overloads {PageBinding#onBindingDispose}
 */
SystemLogPageBinding.prototype.onBindingDispose = function () {

	EventBroadcaster.broadcast ( BroadcastMessages.SYSTEMLOG_CLOSED );
	SystemLogPageBinding.superclass.onBindingDispose.call ( this );
}



// BINDING COUNT DEBUGGING .................................................

/**
 * Display binding count.
 *
SystemLogPageBinding.prototype.countBindings = function () {
	
	this.logger.debug ( "Binding instance count: " + UserInterface.getBindingCount ());
}

/**
 * Set point.
 *
SystemLogPageBinding.prototype.setPoint = function () {
	
	UserInterface.setPoint ();
	this.logger.debug ( "Point at " + UserInterface.getBindingCount () + " bindings." );
}

/**
 * Clear point and log new bindings.
 *
SystemLogPageBinding.prototype.clearPoint = function () {
	
	var keys = UserInterface.getPoint ();
	if ( keys ) {
		if ( keys.hasEntries ()) {
			var debug = "";
			while ( keys.hasNext ()) {
				var key = keys.getNext ();
				debug += UserInterface.getBindingByKey ( key ).toString () + ( Client.isExplorer ? "\n\n" : "\n" );
			}
			this.logger.debug ( debug );
		} else {
			this.logger.debug ( "No new bindings!" );
		}
	} else {
		this.logger.error ( "No point set!" );
	}
	UserInterface.clearPoint ();
}

/**
 * @param {boolean} isTrack
 *
SystemLogPageBinding.prototype.autoTrack = function ( isTrack ) {
	
	if ( isTrack ) {
		this.logger.debug ( "Tracking every 10 seconds." );
	} else {
		this.logger.debug ( "Untracking." );
	}
	UserInterface.autoTrackDisposedBindings ( isTrack );
}
*/