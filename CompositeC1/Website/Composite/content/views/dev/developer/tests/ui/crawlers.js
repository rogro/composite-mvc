/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

var Crawlers = new function () {
	
	var directives = {
		"stop crawling" : NodeCrawler.STOP_CRAWLING,
		"skip children" : NodeCrawler.SKIP_CHILDREN,
		"skip node"	: NodeCrawler.SKIP_NODE
	}
	
	/**
	 * Test descending crawler.
	 */
	this.testDescending = function ( arg ) {
		
		this._clear ();
		var crawler = this._getCrawler ( arg );
		crawler.crawl ( document.getElementById ( "test1" ));
	}
	
	/**
	 * Test ascending crawler.
	 */
	this.testAscending = function ( arg ) {
		
		this._clear ();
		var crawler = this._getCrawler ( arg );
		crawler.type = NodeCrawler.TYPE_ASCENDING;
		crawler.crawl ( document.getElementById ( "test2" ), true );
	}
	
	/**
	 * Clear previous test.
	 */
	this._clear = function () {
		
		var out = document.getElementById ( "output" );
		while ( out.hasChildNodes () == true ) {
			out.removeChild ( out.lastChild );
		}
	}
	
	/**
	 * Build test result.
	 */
	function out ( string ) {
		
		var ul = document.getElementById ( "output" );
		var li = DOMUtil.createElementNS ( Constants.NS_XHTML, "li", document );
		li.appendChild ( document.createTextNode ( string ));
		ul.appendChild ( li );
	}
	
	/**
	 * Configure test crawler.
	 * @return {NodeCrawler}
	 */
	this._getCrawler = function ( arg ) {
		
		var crawler = null;
		switch ( arg ) {
			case 1 :
				crawler = new NodeCrawler ();
				break;
			case 2 :
				crawler = new ElementCrawler ();
				break;
			case 3 :
				crawler = new BindingCrawler ();
				break;
		}
		
		crawler.addFilter ( function ( node ) {
			if ( node.nodeType == Node.TEXT_NODE ) {
				out ( "#text" );
				return NodeCrawler.SKIP_NODE;
			}
		});
		
		crawler.addFilter ( function ( node ) {
			out ( "<" + node.nodeName.toLowerCase () + " f=\"" + node.getAttribute ( "f" ) + "\">" );
		});
		
		crawler.addFilter ( function ( node ) {
			var directive = node.getAttribute ( "directive" );
			if ( directive ) {
				return directives [ directive ];
			}
		});
		
		crawler.addFilter ( function ( node ) {
			var nextnode = node.getAttribute ( "nextnode" );
			if ( nextnode ) {
				crawler.nextNode = document.getElementById ( nextnode );
			}
		})
		
		return crawler;
	}
}