/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

UpdateManagerTestPageBinding.prototype = new PageBinding;
UpdateManagerTestPageBinding.prototype.constructor = UpdateManagerTestPageBinding;
UpdateManagerTestPageBinding.superclass = PageBinding.prototype;

/**
 * Notice this! Make sure that before and after  
 * markup gets send to the System Log.... 
 * @overwrites {DocumentUpdatePlugin.isDebugging}
 */
DocumentUpdatePlugin.isDebugging = true;

/**
 * UpdateManager test!
 */
function UpdateManagerTestPageBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "UpdateManagerTestPageBinding" );
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
UpdateManagerTestPageBinding.prototype.toString = function () {

	return "[UpdateManagerTestPageBinding]";
}

/**
 * Setup update listeners and handle potential errors.
 * @overloads {PageBinding#onBeforePageInitialize}
 */
UpdateManagerTestPageBinding.prototype.onBeforePageInitialize = function () {
	
	DOMEvents.addEventListener ( document, DOMEvents.BEFOREUPDATE, this );
	DOMEvents.addEventListener ( document, DOMEvents.AFTERUPDATE, this );
	DOMEvents.addEventListener ( document, DOMEvents.ERRORUPDATE, this );
	 
	bindingMap.menubar.addActionListener ( MenuItemBinding.ACTION_COMMAND, {
		handleAction : function ( action ) {
			var file = action.target.getProperty ( "example" );
			bindingMap.markup.setValue ( Templates.getPlainText ( file ));
			bindingMap.updatebutton.fireCommand ();
		}
	})
	
	UpdateManagerTestPageBinding.superclass.onBeforePageInitialize.call ( this );
}

/**
 * @implements {IEventHandler}
 * @overloads {PageBinding#handleEvent}
 * @param {Event} e
 */
UpdateManagerTestPageBinding.prototype.handleEvent = function ( e ) {
	
	UpdateManagerTestPageBinding.superclass.handleEvent.call ( this, e );
	
	if ( DOMEvents.getTarget ( e ) == document.documentElement ) {
		switch ( e.type ) {
			case DOMEvents.BEFOREUPDATE :
				this._beforeUpdate ();
				break;
			case DOMEvents.AFTERUPDATE :
				this._afterUpdate ();
				break;
			case DOMEvents.ERRORUPDATE :
				this._errorUpdate ();
				break;
		}
	}
}

/**
 * Before update.
 */
UpdateManagerTestPageBinding.prototype._beforeUpdate = function () {
	
	this.bindingWindow.bindingMap.reporttextbox.setValue ( "" );
}

/**
 * After update.
 */
UpdateManagerTestPageBinding.prototype._afterUpdate = function () {
	
	var summary = this.bindingWindow.UpdateManager.summary;
	if ( summary == "" ) {
		summary = "No updates :)"
	}
	this.bindingWindow.bindingMap.reporttextbox.setValue ( 	summary );
}

/**
 * After error.
 */
UpdateManagerTestPageBinding.prototype._errorUpdate = function () {
	
	this.bindingWindow.bindingMap.reporttextbox.setValue (
		"RELOAD THIS TEST! :)\n\n" + UpdateManager.errorsmessage 
	);
}