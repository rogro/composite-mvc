/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using Composite.Core.ResourceSystem;

public partial class Spikes_Icons_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var imageTags =
            from iconHandle in IconResourceSystemFacade.GetAllIconHandles()
            orderby iconHandle.ResourceNamespace, iconHandle.ResourceName
            select new XElement("div",
                new XAttribute("class", "iconcontainer"),
                new XElement("img",
                    new XAttribute("src", BuildImgUrl(iconHandle))),
                new XElement( "span",
                    new XAttribute( "class", "iconnamespace" ),
                    new XText( iconHandle.ResourceNamespace )),
                new XElement( "span",
                    new XAttribute( "class", "iconname" ),
                    new XText( iconHandle.ResourceName ))
                );

        XElement imageContainer = 
            new XElement("div", 
                new XAttribute( "class", "iconscontainer " + IconSize.SelectedValue ),
                imageTags);

        IconPlaceHolder.Controls.Add(new LiteralControl(imageContainer.ToString()));
    }

    private string BuildImgUrl(ResourceHandle iconHandle)
    {
        return
            string.Format("../../../../../services/Icon/GetIcon.ashx?resourceName={0}&resourceNamespace={1}&size={2}", iconHandle.ResourceName, iconHandle.ResourceNamespace, IconSize.SelectedValue);
    }


    protected void IconSize_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
