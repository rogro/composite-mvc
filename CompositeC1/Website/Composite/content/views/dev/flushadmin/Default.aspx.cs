/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Composite.Core.Types;
using Composite.C1Console.Events;
using System.Reflection;


public partial class FlushAdmin_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Request.QueryString["Type"] != null)
        {
            Type type = TypeManager.GetType(this.Request.QueryString["Type"]);

            FlushAttribute flushAttribute = type.GetCustomAttributes(false).Where(f => f.GetType() == typeof(FlushAttribute)).Single() as FlushAttribute;

            MethodInfo methodInfo =
                (from m in type.GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static)
                 where m.Name == flushAttribute.MethodName
                 select m).SingleOrDefault();

            methodInfo.Invoke(null, null);

            FlushAdminPlaceHolder.Controls.Add(new LiteralControl(string.Format(@"<b>{0} flushed</b>", this.Request.QueryString["Type"])));
            FlushAdminPlaceHolder.Controls.Add(new LiteralControl("<br /><br /><br />"));
        }
        
        FlushAdminPlaceHolder.Controls.Add(new LiteralControl(@"<table border=""0"">"));

        foreach (Type type in AssemblyFacade.GetAllAssemblies().GetTypes())
        {
            FlushAttribute flushAttribute = type.GetCustomAttributes(false).Where(f => f.GetType() == typeof(FlushAttribute)).SingleOrDefault() as FlushAttribute;


            if (flushAttribute == null) continue;            

            MethodInfo methodInfo =
                (from m in type.GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static)
                 where m.Name == flushAttribute.MethodName
                 select m).SingleOrDefault();

            if (methodInfo == null) continue;

            FlushAdminPlaceHolder.Controls.Add(new LiteralControl("<tr>"));
            FlushAdminPlaceHolder.Controls.Add(new LiteralControl("<td>" + type.FullName + "</td>"));
            FlushAdminPlaceHolder.Controls.Add(new LiteralControl(string.Format(@"<td><a href=""{0}?Type={1}"">Flush</a></td>", this.Request.CurrentExecutionFilePath, HttpUtility.UrlEncode(type.GetVersionNeutralName()))));
            FlushAdminPlaceHolder.Controls.Add(new LiteralControl("</tr>"));
        }

        FlushAdminPlaceHolder.Controls.Add(new LiteralControl("</table>"));
    }
}
