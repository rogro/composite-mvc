/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

BrowserTabBoxBinding.prototype = new TabBoxBinding;
BrowserTabBoxBinding.prototype.constructor = BrowserTabBoxBinding;
BrowserTabBoxBinding.superclass = TabBoxBinding.prototype;


/**
 * @class
 */
function BrowserTabBoxBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "BrowserTabBoxBinding" );
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
BrowserTabBoxBinding.prototype.toString = function () {

	return "[BrowserTabBoxBinding]";
}

/**
 * Open new tab (and show the URL). 
 * TODO: If URL is already displayed, find the tab and focus it instead?
 * @param @optional {string} url
 */
BrowserTabBoxBinding.prototype.newURL = function ( url ) {
	
	var tab = BrowserTabBinding.newInstance ( this.bindingDocument );
	var win = WindowBinding.newInstance ( this.bindingDocument );
	tab.browserwindow = win;
	this.appendTabByBindings ( tab, win );
	if ( url != null ) {
		this.setURL ( url );
	}
}

/**
 * Show URL in selected tab.
 * @param {string} url.
 */
BrowserTabBoxBinding.prototype.setURL = function ( url ) {
	
	var tab = this.getSelectedTabBinding ();
	var win = tab.browserwindow;
	win.setURL ( url );
}

/**
 * Get URL from selected tab.
 * @return {string}
 */
BrowserTabBoxBinding.prototype.getLocation = function () {
	
	var tab = this.getSelectedTabBinding ();
	var win = tab.browserwindow;
	return new String ( win.getContentDocument ().location );
}

/**
 * Get currently active document.
 * @return {DOMDocument}
 */
BrowserTabBoxBinding.prototype.getContentDocument = function () {

	var tab = this.getSelectedTabBinding ();
	var win = tab.browserwindow;
	return win.getContentDocument ();
}

/**
 * Get currently active window.
 * @return {DOMDocumentView}
 */
BrowserTabBoxBinding.prototype.getContentWindow = function () {

	var tab = this.getSelectedTabBinding ();
	var win = tab.browserwindow;
	return win.getContentWindow ();
}