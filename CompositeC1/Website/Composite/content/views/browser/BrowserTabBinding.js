/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

BrowserTabBinding.prototype = new TabBinding;
BrowserTabBinding.prototype.constructor = BrowserTabBinding;
BrowserTabBinding.superclass = TabBinding.prototype;

BrowserTabBinding.ACTIONVENT_CLOSE = "browsertabclose";
BrowserTabBinding.IMG_CLOSE_DEFAULT = Resolver.resolve ( "${root}/skins/system/tabboxes/tab-close-default.png" );
BrowserTabBinding.IMG_CLOSE_HOVER = Resolver.resolve ( "${root}/skins/system/tabboxes/tab-close-hover.png" );

/**
 * @class
 */
function BrowserTabBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "BrowserTabBinding" );
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
BrowserTabBinding.prototype.toString = function () {

	return "[BrowserTabBinding]";
}

/**
 * @overloads {TabBinding#buildDOMContent}
 */
BrowserTabBinding.prototype.buildDOMContent = function () {

	BrowserTabBinding.superclass.buildDOMContent.call ( this );
	
	var img = this.bindingDocument.createElement ( "img" );
	img.src = BrowserTabBinding.IMG_CLOSE_DEFAULT;
	img.style.display = "none";
	this.labelBinding.bindingElement.appendChild ( img );
	this.shadowTree.closeImage = img;
	
	setTimeout ( function () { // before label is set, img is oddly placed.
		img.style.display = "inline";
	}, 0 );
	
	var self = this;
	var handler = {
		handleEvent : function ( e ) {
			switch ( e.type ) {
				case DOMEvents.MOUSEOVER :
					img.src = BrowserTabBinding.IMG_CLOSE_HOVER;
					break;
				case DOMEvents.MOUSEOUT :
					img.src = BrowserTabBinding.IMG_CLOSE_DEFAULT;
					break;
				case DOMEvents.CLICK :
					self.dispatchAction ( BrowserTabBinding.ACTIONVENT_CLOSE );
					self.containingTabBoxBinding.removeTab ( self );
					break;
			}
		}
	}
	
	DOMEvents.addEventListener ( img, DOMEvents.MOUSEOVER, handler );
	DOMEvents.addEventListener ( img, DOMEvents.MOUSEOUT, handler );
	DOMEvents.addEventListener ( img, DOMEvents.CLICK, handler );
}

/**
 * BrowserTabBinding factory.
 * @param {DOMDocument} ownerDocument
 * @return {BrowserTabBinding}
 */
BrowserTabBinding.newInstance = function ( ownerDocument ) {

	var element = DOMUtil.createElementNS ( Constants.NS_UI, "ui:tab", ownerDocument );
	return UserInterface.registerBinding ( element, BrowserTabBinding );
}