/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

LanguageSelectorBinding.prototype = new SelectorBinding;
LanguageSelectorBinding.prototype.constructor = LanguageSelectorBinding;
LanguageSelectorBinding.superclass = SelectorBinding.prototype;

/**
 * @class
 */
function LanguageSelectorBinding () {
	
	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "LanguageSelectorBinding" );
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
LanguageSelectorBinding.prototype.toString = function () {

	return "[LanguageSelectorBinding]";
}

/**
 * @overloads {SelectorBinding#onBindingAttach}
 */
LanguageSelectorBinding.prototype.onBindingAttach = function () {
	
	LanguageSelectorBinding.superclass.onBindingAttach.call ( this );
	 
	var browser = bindingMap.browserpage;
	browser.addActionListener ( BrowserPageBinding.ACTION_ONLOAD, this );
	browser.addActionListener ( BrowserPageBinding.ACTION_TABSHIFT, this );
}

/**
 * @overloads {SelectorBinding#handleAction}
 * @implements {IActionListener}
 * @param {Action} action
 */
LanguageSelectorBinding.prototype.handleAction = function ( action ) {
	
	LanguageSelectorBinding.superclass.handleAction.call ( this, action );
	
	switch ( action.type ) {
		case BrowserPageBinding.ACTION_ONLOAD :
		case BrowserPageBinding.ACTION_TABSHIFT :
			this._updateLanguages ();	
			break;
	}
}

/**
 * The server returns an array of objects with props:
 *     Name
 *     IsoName
 *     UrlMappingName
 *     Url
 *     IsCurrent
 */
LanguageSelectorBinding.prototype._updateLanguages = function () {
	
	var doc = bindingMap.browserpage.getContentDocument ();
	
	if ( doc != null ) {
		
		var location = doc.location.toString ();
		
		WebServiceProxy.isFaultHandler = false;
		var response = LocalizationService.GetPageOtherLocales ( location );
		WebServiceProxy.isFaultHandler = true;
		
		if ( !response instanceof SOAPFault ) {
			var list = new List ( response );
			if ( list != null && list.hasEntries ()) {
				var selections = new List ();
				list.each ( function ( lang ) {
					selections.add ( 
						new SelectorBindingSelection (
							lang.Name,
							lang.Url,
							lang.IsCurrent,
							null
						)
					);
				});
				this.populateFromList ( selections );
				this.show ();
			} else {
				this.hide ();
			}
		}
	} else {
		this.hide ();
	}
}

/**
 * Update browser window when selector is handled.
 * @overwrites {Selector#onValueChange}
 */
LanguageSelectorBinding.prototype.onValueChange = function () {
	
	bindingMap.browserpage.setURL ( this.getValue ());
}