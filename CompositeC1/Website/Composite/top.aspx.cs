/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using Composite.Core.Configuration;



public partial class Composite_Management_Top : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        string myPathAndQuery = Request.Url.PathAndQuery;

        if (!myPathAndQuery.Contains("/Composite/"))
        {
            // not launching in /Composite/ folder (case sensitive)! The wysiwyg editor support code URL handling does case sensitive searches, so we fix it right here. Tnx to the sucky string features in xslt 1.0
            int badlyCaseIndex = myPathAndQuery.IndexOf("/composite/", StringComparison.OrdinalIgnoreCase);
            string fixedPathAndQuery = string.Format("{0}/Composite/{1}",
                                            myPathAndQuery.Substring(0, badlyCaseIndex),
                                            myPathAndQuery.Substring(badlyCaseIndex + 11));

            Response.Redirect(fixedPathAndQuery);
        }

        if (SystemSetupFacade.IsSystemFirstTimeInitialized == false)
        {
            if (System.Web.HttpContext.Current.Request.UserAgent == null)
            {
                Response.Redirect("unknownbrowser.aspx");
                return;
            }

            introholder.Visible = true;
            splashholder.Visible = false;
        }
        else
        {
            introholder.Visible = false;
            splashholder.Visible = true;
        }
    }
}
