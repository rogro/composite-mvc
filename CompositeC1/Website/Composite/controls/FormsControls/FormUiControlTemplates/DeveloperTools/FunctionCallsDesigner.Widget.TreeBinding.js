/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

FunctionTreeBinding.prototype = new TreeBinding;
FunctionTreeBinding.prototype.constructor = TreeBinding;
FunctionTreeBinding.superclass = TreeBinding.prototype;

/**
 * @type {ToolBarButtonBinding}
 */
FunctionTreeBinding.addNew = function ( binding ) {
    
    var def = ViewDefinitions [ "Composite.Management.WidgetFunctionSelectorDialog" ];
    
    def.argument.nodes [ 0 ].search = binding.getProperty("SerializedFunctionSearchToken");
    
    def.handler = {
        handleDialogResponse : function ( response, result ) {
            if ( response == Dialog.RESPONSE_ACCEPT ) {
                var input = document.getElementById ( binding.getProperty ( "AddNewPostBackArgsId" ));
                input.value = result.getFirst ();
                
                setTimeout ( function () {
                    binding.dispatchAction ( PageBinding.ACTION_DOPOSTBACK );
                }, 0 );
            }
        }
    }

    Dialog.invokeDefinition ( def );
}

/**
 * @class
 */
function FunctionTreeBinding () {

	/**
	 * @type {SystemLogger}
	 */
	 
	this.logger = SystemLogger.getLogger ( "FunctionTreeBinding" );
	
	/**
	 * @type {SystemTreeNodeBinding}
	 */
	this._defaultTreeNode = null;
}

/**
 * Identifies binding.
 */
FunctionTreeBinding.prototype.toString = function () {
	
	return "[FunctionTreeBinding]";
}

/**
 * TEMP!
 */
FunctionTreeBinding.prototype.onBindingAttach = function () {
	
	FunctionTreeBinding.superclass.onBindingAttach.call ( this );
	// alert ( "Hej Maw!\n\nKan du fixe det sådan at Query-noden er focused som default?\n\nEllers fjerner jeg ikke alerten." );
}

FunctionTreeBinding.prototype.handleAction = function ( action ) {
    
    switch ( action.type ) {
    
        case TreeNodeBinding.ACTION_ONFOCUS :
        
            var binding = action.target;
            var bindingHandle = binding.getHandle ();
            
            var input = document.getElementById ( this.getProperty ( "FunctionTreePostBackHandle" ));
            var previousHandle = input.value;
            
            if ( previousHandle != bindingHandle )
            {
                input.value = bindingHandle;

                var treeNodeType = ( bindingHandle.indexOf(":")>-1 ? bindingHandle.split(':')[0] : "" ) ;
                
                switch (treeNodeType)
                {
                    case "Function":
                        bindingMap.broadcasterFunctionTreeHasSelection.enable ();
                        binding.dispatchAction ( PageBinding.ACTION_DOPOSTBACK );
                        break;
                    case "Namespace":
                        bindingMap.broadcasterFunctionTreeHasSelection.enable ();
                        binding.dispatchAction ( PageBinding.ACTION_DOPOSTBACK );
                        break;
                    case "Parameter":
                        bindingMap.broadcasterFunctionTreeHasSelection.disable ();
                        binding.dispatchAction ( PageBinding.ACTION_DOPOSTBACK );
                        break;
                    default:
                        bindingMap.broadcasterFunctionTreeHasSelection.disable ();
                }
                
            }
            
            break;
    }
    
    FunctionTreeBinding.superclass.handleAction.call ( this, action );
}
