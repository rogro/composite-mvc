/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using Composite.Core.Xml;
using System.Collections.Generic;
using Composite.Core.Logging;
using Composite.Plugins.Forms.WebChannel.UiControlFactories;

namespace CompositeMultiContentXhtmlEditor
{
    public partial class MultiContentXhtmlEditor : MultiContentXhtmlEditorTemplateUserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.ContentsPlaceHolder.Controls.Count == 0)
            {
                SetUpTextAreas(false);
            }
        }

        protected override void BindStateToProperties()
        {
            Dictionary<string, string> newNamedXhtmlFragments = new Dictionary<string, string>();
            foreach (Control c in this.ContentsPlaceHolder.Controls)
            {
                if (IsRealContent(((TextBox)c).Text))
                {
                    newNamedXhtmlFragments.Add(c.ID, ((TextBox)c).Text.Replace("&nbsp;", "&#160;"));
                }
            }

            this.NamedXhtmlFragments = newNamedXhtmlFragments;
        }


        protected override void InitializeViewState()
        {
            SetUpTextAreas(true);
        }

        public override string GetDataFieldClientName()
        {
            return null;
        }


        private void SetUpTextAreas(bool flush)
        {
            List<string> handledIds = new List<string>();

            ContentsPlaceHolder.Controls.Clear();
			
			bool isFirst = true;
			
            foreach (string placeHolderId in this.PlaceholderDefinitions.Keys)
            {
                if (handledIds.Contains(placeHolderId) == false)
                {
                    TextBox contentTextBox = new Composite.Core.WebClient.UiControlLib.TextBox();
                    contentTextBox.TextMode = TextBoxMode.MultiLine;
                    contentTextBox.ID = placeHolderId;
                    contentTextBox.Attributes.Add("placeholderid", placeHolderId);
                    contentTextBox.Attributes.Add("placeholdername", this.PlaceholderDefinitions[placeHolderId]);
                    if ( isFirst )
                    {
                        contentTextBox.Attributes.Add("selected", "true");
                        isFirst = false;
                    }
                    if (flush == true)
                    {
                        if (this.NamedXhtmlFragments.ContainsKey(placeHolderId))
                        {
                            contentTextBox.Text = this.NamedXhtmlFragments[placeHolderId];
                        }
                        else
                        {
                            contentTextBox.Text = "";
                        }
                    }
                    ContentsPlaceHolder.Controls.Add(contentTextBox);
                    handledIds.Add(placeHolderId);
                }
            }
        }


        private bool IsRealContent(string content)
        {
            if (content.Length > 50) return true;
            string testContent = content.Replace("<p>", "");
            testContent = testContent.Replace("</p>", "");
            testContent = testContent.Replace("&nbsp;", "");
            testContent = testContent.Replace("&#160;", "");
            testContent = testContent.Replace(" ", "");
            testContent = testContent.Replace("<br/>", "");

            if (string.IsNullOrEmpty(testContent) == true)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

    }
}