/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Composite.Core.ResourceSystem;
using Composite.Core.WebClient.UiControlLib;
using Composite.Plugins.Forms.WebChannel.UiContainerFactories;


public partial class Composite_Forms_WarningDialogExecutionContainer : TemplatedUiContainerBase
{
    private PlaceHolder formPlaceHolder2 = new PlaceHolder();
    private PlaceHolder messagePlaceHolder2 = new PlaceHolder();

    protected string ContainerLabel { get; private set; }
    protected ResourceHandle ContainerIcon { get; private set; }

    protected string ContainerIconClientString
    {
        get
        {
            if (this.ContainerIcon != null)
            {
                return string.Format("${{icon:{0}:{1}}}", this.ContainerIcon.ResourceNamespace, this.ContainerIcon.ResourceName);
            }
            
            return "${icon:question}";
        }
    }

    public override Control GetFormPlaceHolder()
    {
        return formPlaceHolder2;
    }

    public override Control GetMessagePlaceHolder()
    {
        return messagePlaceHolder2;
    }

    public override void SetContainerTitle(string containerLabel)
    {
        this.ContainerLabel = containerLabel;
    }

    public override void SetContainerIcon(ResourceHandle icon)
    {
        this.ContainerIcon = icon;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        formPlaceHolder.Controls.Add(formPlaceHolder2);
        messagePlaceHolder.Controls.Add(messagePlaceHolder2);

//        Page.Items.Add("CustomBroadcasterSets", this.customBroadcasterSets);
    }

    public override void ShowFieldMessages(Dictionary<string, string> clientIDPathedMessages)
    {
        foreach (var msgElement in clientIDPathedMessages)
        {
            FieldMessage fieldMessage = new FieldMessage(msgElement.Key, msgElement.Value);
            messagePlaceHolder.Controls.Add(fieldMessage);
        }
    }

}
