/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Text;
using System.Web;
using System.Web.UI;

public partial class CodePressControl : System.Web.UI.UserControl
{
    protected override void Render(HtmlTextWriter writer)
    {
		
		Response.ContentType = "text/html";
		String _lang = HttpContext.Current.Request.QueryString [ "lang" ];
		bool _isMozilla = HttpContext.Current.Request.UserAgent.IndexOf ( "Gecko" ) > -1;
		StringBuilder _builder = new StringBuilder ();
		
		if ( null == _lang ) 
		{
			_lang = "text";
		}
		_builder.AppendLine ( @"<link type=""text/css"" href=""languages/codepress-" + _lang + @".css"" rel=""stylesheet"" id=""cp-lang-style"" />" );
		_builder.AppendLine ( @"<script type=""text/javascript"" src=""languages/codepress-" + _lang + @".js""></script>" );
		_builder.AppendLine ( @"<script type=""text/javascript"">CodePress.language=""" + _lang + @""";</script>" );
		_builder.AppendLine ( @"<script type=""text/javascript"" src=""extensions/" + ( _isMozilla ? "mozilla" : "explorer" ) + @".js""></script>" );
		
		writer.Write ( _builder.ToString ());
    }
}