/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Web;
using Composite;
using Composite.Core.Configuration;
using Composite.Core.WebClient;

/**
 * Simply store developermode on session. 
 */
public partial class AppInitializerControl : System.Web.UI.UserControl
{
    protected void Page_Init(object sender, EventArgs e)
    {

        /*
         * Set the browsing mode cookie.
         */
        string mode = Request.QueryString["mode"];

        if (String.IsNullOrEmpty(mode))
        {
            CookieHandler.Set("mode", "operate");
        }
        else
        {
            CookieHandler.Set("mode", mode == "develop" ? "develop" : "operate");
        }

        /*
         * Set the version cookie. If version doesn't match 
         * last session version, redirect to upgraded page.
         */
        string nowversion = Composite.RuntimeInformation.ProductVersion.ToString();

        bool isUpdated = false;
        if (CookieHandler.Get("CompositeVersionString") != null)
        {
            string oldversion = CookieHandler.Get("CompositeVersionString");
            if (nowversion != oldversion)
            {
                var installationAge = DateTime.Now - SystemSetupFacade.GetFirstTimeStart();
                isUpdated = installationAge.TotalMinutes > 5;
            }
        }

        CookieHandler.Set("CompositeVersionString", nowversion, DateTime.Now.AddYears(23));

        if ((RuntimeInformation.IsDebugBuild == false) && (isUpdated == true))
        {
            string url = "updated.aspx";
            if (CookieHandler.Get("mode") == "develop")
            {
                url += "?mode=develop";	// TODO: copy entire querystring (no intellisense here)!
            }
            Response.Redirect(url);
        }
    }
}