/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Text;
using System.Web;
using System.Web.UI;
using Composite.Core.IO;

public partial class IconRendererControl : System.Web.UI.UserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {
    	string size = Request.QueryString [ "size" ];
    	if ( String.IsNullOrEmpty ( size )) {
    		size = "24";
    	}

        string path = Server.MapPath ( "../../../../../images/icons/republic" );
        string [] dirEntries = C1Directory.GetDirectories ( path );
			
		StringBuilder builder = new StringBuilder (); 
        
        foreach(string dirName in dirEntries)
		{
		   // do something with fileName
		   if ( dirName.Contains ( "republic_" )) {
		   		string [] fileEntries = C1Directory.GetFiles ( dirName );
		   		foreach(string fileName in fileEntries) {
		   			if ( fileName.Contains ( "_" + size + "px_" )) {
		   				string string1 = fileName.Replace ( "\\", "/" ).ToLowerInvariant();
		   				string string2 = string1.Substring ( 
		   					string1.IndexOf ( "images" ) // Website
		   				);
		   				string string3 = string1.Substring ( 
		   					string1.LastIndexOf ( "/" ) + 1 
		   				);
		   				string3 = string3.Substring ( 0, 4 );

                        builder.AppendLine(string.Format(@"<div class=""img""><img src=""{0}/{1}"" /><span>{2}</span></div>", Composite.Core.WebClient.UrlUtils.AdminRootPath, HttpUtility.HtmlAttributeEncode(string2), HttpUtility.HtmlEncode(string3)));
			   		}
		   		}
		   }
		}

        dynamicOutputPlaceHolder.Controls.Add( new LiteralControl( builder.ToString () ));
    }
}