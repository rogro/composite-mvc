/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.IO;
using System.Text;
using System.Web.UI;
using Composite.Core.IO;


public partial class IconRendererHarmonyControl : System.Web.UI.UserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {
        string size = Request.QueryString["size"];
        if (String.IsNullOrEmpty(size))
        {
            size = "24";
        }

        string path = Path.Combine(PathUtil.Resolve(PathUtil.BaseDirectory), "Composite\\images\\icons\\harmony");
        string[] dirEntries = C1Directory.GetDirectories(path);

        StringBuilder builder = new StringBuilder();

        foreach (string dirName in dirEntries)
        {
            int index = dirName.LastIndexOf("\\");
            int length = dirName.Length - index;

            builder.AppendLine(
                "<ui:pageheading>" +
                dirName.Substring(index + 1, length - 1) +
                "</ui:pageheading>"
            );

            string[] fileEntries = C1Directory.GetFiles(dirName);
            foreach (string fileName in fileEntries)
            {
                if (fileName.Contains("_" + size))
                {
                    string url = fileName.Remove(0, PathUtil.Resolve(PathUtil.BaseDirectory).Length).Replace("\\", "/");

                    url = Composite.Core.WebClient.UrlUtils.ResolvePublicUrl(url);

                    string s = url.Substring(url.LastIndexOf("/") + 1);
                    s = s.Substring(0, (s.LastIndexOf("_")));
                    
                    

                    builder.AppendLine("<div class=\"img\"><img src=\"" + url + "\"/><span>" + s + "</span></div>");
                }
            }
        }

        dynamicOutputPlaceHolder.Controls.Add(new LiteralControl(builder.ToString()));
    }
}