/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Web.Hosting;
using System.Web.UI.HtmlControls;
using Composite.C1Console.Security;
using Composite.Core.Configuration;

public partial class Composite_Management_Login : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        if (SystemSetupFacade.IsSystemFirstTimeInitialized == false)
        {
            Response.Redirect("/Composite");
            return;
        }

        if (UserValidationFacade.IsLoggedIn())
        {
            RedirectToReturnUrl();
            return;
        }

        if (Request.RequestType == "POST")
        {
            OnPost();
        }
    }

    private void RedirectToReturnUrl()
    {
        var returnUrl = Request.QueryString["ReturnUrl"];

        try
        {
            Uri uri = new Uri(returnUrl);
            if (uri.Host != Request.Url.Host)
            {
                returnUrl = null; // prevent "Arbitrary Redirection" attacks
            }
        }
        catch (Exception) { }

        if (!string.IsNullOrEmpty(returnUrl))
        {
            Response.Redirect(returnUrl, false);
            return;
        }

        Response.Redirect(HostingEnvironment.ApplicationVirtualPath, false);
    }

    private void OnPost()
    {
        bool isValid = true;

        string username = Request.Form["txtUsername"];
        string password = Request.Form["txtPassword"];

        if (string.IsNullOrEmpty(username))
        {
            isValid = false;
            txtUsername.Attributes["class"] = "error";
        }

        if (string.IsNullOrEmpty(password) || password.Length < 6)
        {
            isValid = false;
            txtPassword.Attributes["class"] = "error";
        }

        txtUsername.Attributes.Add("value", username);

        if(isValid)
        {
            if(UserValidationFacade.FormValidateUser(username, password) == LoginResult.Success)
            {
                RedirectToReturnUrl();
                return;
            }

            divLoginFailed.Visible = true;
        }
    }
}
