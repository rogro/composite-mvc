/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

/*
 * If Prism, clear the cache and load top.aspx - else display message to user.
 */
window.onload = function () {
	
	var agent = navigator.userAgent.toLowerCase ();
	var isPrism = agent.indexOf ( "prism" ) >-1;
	var url = "top.aspx" + document.location.search;
	
	if ( isPrism ) {
		var event = document.createEvent ( "Events" );
		event.initEvent ( "contenttochrome-clearcache", true, true );
		window.dispatchEvent ( event );
		setTimeout ( function () {
			document.location = url;
		}, 250 );
	} else {
		var link = document.getElementById ( "continuelink" );
		var splash = document.getElementById ( "splash" );
		link.href = url;
		splash.style.display = "block";
		document.oncontextmenu = function ( e ) {
			return false;
		}
	}
}