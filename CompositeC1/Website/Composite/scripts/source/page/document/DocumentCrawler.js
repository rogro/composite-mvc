/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

DocumentCrawler.prototype = new ElementCrawler;
DocumentCrawler.prototype.constructor = DocumentCrawler;
DocumentCrawler.superclass = ElementCrawler.prototype;

DocumentCrawler.ID = "documentcrawler";
DocumentCrawler.MODE_REGISTER = "register";
DocumentCrawler.MODE_ATTACH = "attach";
DocumentCrawler.MODE_DETACH = "detach";

/**
 * @class
 * This be the crawler that attaches and detaches bindings. When a binding has 
 * the DocumentCrawler.ID included in it's "crawlerFilters" property, it means 
 * that the binding is not supposed to have descendant bindings (or that the  
 * binding will handle descendant bindings registration and attachment itself).
 */
function DocumentCrawler () {
	
	this.mode = DocumentCrawler.MODE_REGISTER;
	this.id = DocumentCrawler.ID;
	this._construct ();
	return this;
}

/**
 * @overloads {Crawler#_construct} 
 */
DocumentCrawler.prototype._construct = function () {
	
	DocumentCrawler.superclass._construct.call ( this );
	
	var self = this;
	this.addFilter ( function ( element, list ) {
		
		var binding = UserInterface.getBinding ( element );
		var result = null;
		
		switch ( self.mode ) {
			
			case DocumentCrawler.MODE_REGISTER :
				if ( binding == null ) {
					UserInterface.registerBinding ( element );
				}
				break;
				
			case DocumentCrawler.MODE_ATTACH :
				if ( binding != null ) {
					if ( !binding.isAttached ) {
						list.add ( binding );
					}
					if ( binding.isLazy == true ) {
						result = NodeCrawler.SKIP_CHILDREN;
					}
				}
				break;
				
			case DocumentCrawler.MODE_DETACH :
				if ( binding != null ) {
					list.add ( binding );
				}
				break;
		}
		return result;
	});
}