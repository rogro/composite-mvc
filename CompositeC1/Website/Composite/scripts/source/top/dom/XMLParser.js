/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

/**
 * @class
 */
function _XMLParser () {}

_XMLParser.prototype = {

	_logger: SystemLogger.getLogger("XMLParser"),
	_domParser: (window.DOMParser != null && window.XPathResult != null ? new DOMParser() : null),

	/**
	* @param {string} xml
	* @param @optional {boolean} If true, ignore all parse errors.
	* @return {DOMDocument}
	*/
	parse: function (xml, isIgnore) {

		var doc = null;

		if (xml != null) {
			if (this._domParser != null) {
				try {
					doc = this._domParser.parseFromString(xml, "text/xml");
				} catch (e) {
					alert(xml)
				}
				if (doc.documentElement.namespaceURI == Constants.NS_DOMPARSEERROR) {
					if (!isIgnore) {
						this._logger.error(DOMSerializer.serialize(doc.documentElement, true));
						if (Application.isDeveloperMode) {
							alert("XMLParser failed: \n\n" + DOMSerializer.serialize(doc.documentElement, true));
						}
					}
					doc = null;
				}
			} else {
				doc = DOMUtil.getDOMDocument();
				doc.setProperty("ProhibitDTD", false);
				doc.validateOnParse = false;
				doc.async = false;
				doc.loadXML(xml);
				if (doc.parseError.errorCode != 0) {
					if (!isIgnore) {
						this._logger.error("XMLParser failed!");
						if (Application.isDeveloperMode) {
							alert("XMLParser failed!");
						}
					}
					doc = null;
				}
			}
		} else {
			throw "XMLParser: No XML input to parse!";
		}

		return doc;
	},

	/**
	* Is xml parsable as full document? Note that we allow a string  
	* with no XML declaration. This may not be the best idea... 
	* @param {string} xml
	* @param @optional {boolean} hasDialog If true, automatically show a dialog
	*/
	isWellFormedDocument: function (xml, hasDialog, isConfirmDialog) {

		var result = true;
		var dec = '<?xml version="1.0" encoding="UTF-8"?>';
		if (xml.indexOf("<?xml ") == -1) {
			xml = dec + xml;
		}
		var string = SourceValidationService.IsWellFormedDocument(xml);

		if (string != "True") {
			result = false;
			if (hasDialog == true) {
				if (isConfirmDialog) {
					if (confirm("Not well-formed\n" + string + "\nContinue?")) {
						result = true;
					}
				}else {
					this._illFormedDialog(string);
				}
			}
		}

		return result;
	},

	/**
	* Is xml parsable as full document fragment?
	* @param {string} xml
	* @param @optional {boolean} hasDialog  If true, automatically show a dialog
	*/
	isWellFormedFragment: function (xml, hasDialog) {

		var result = true;
		var string = SourceValidationService.IsWellFormedFragment(xml);

		if (string != "True") {
			result = false;
			if (hasDialog == true) {
				this._illFormedDialog(string);
			}
		}

		return result;
	},

	/**
	* In case of malformed XML, analyze server parser 
	* exception and present a clarifying dialog. 
	* @param {String} string
	*/
	_illFormedDialog: function (string) {

		/*
		* Timeout allows any previous method to returnvalue first.
		*/
		setTimeout(function () {
			Dialog.error("Not well-formed", string);
		}, 0);
	}
}

/**
 * The instance that does it.
 * @type {_XMLParser}
 */
var XMLParser = new _XMLParser ();