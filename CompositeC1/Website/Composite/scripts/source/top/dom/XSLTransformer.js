/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

/**
 * @class
 * XSL transformers rule.
 */
function XSLTransformer () {
	
	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "XSLTransformer" );
	
	/**
	 * @type {MSXMLXSLTemplate}
	 */
	this._processor = null;
	
	/**
	 * @type {MSXMLXSLTemplate}
	 */
	this._cache = null;
}

/**
 * Import stylesheet.
 * @param {string} url
 */
XSLTransformer.prototype.importStylesheet = function ( url ) {
	
	var stylesheet = this._import ( 
		Resolver.resolve ( url )
	);
	
	if (Client.hasXSLTProcessor) {
		this._processor = new XSLTProcessor ();
		this._processor.importStylesheet ( stylesheet );
	} else {	
		this._cache = DOMUtil.getMSXMLXSLTemplate ();
		this._cache.stylesheet = stylesheet;
	}
}

/**
 * @param {string} url
 * @return {DOMDocument}
 */
XSLTransformer.prototype._import = function ( url ) {

	var result = null;

	if (Client.hasXSLTProcessor) {
	
		var request = DOMUtil.getXMLHTTPRequest ();
		request.open ( "get", Resolver.resolve ( url ),  false );
		request.send ( null );
		result = request.responseXML;
		
	} else {
		
		var result = DOMUtil.getDOMDocument ( true );
		result.async = false;
		result.load ( url );
	}
	
	return result;
}

/**
 * @param {DOMDocument} dom
 * @return {DOMDocument}
 */
XSLTransformer.prototype.transformToDocument = function ( dom ) {
	
	var result = null;
	if (Client.hasXSLTProcessor) {
		result = this._processor.transformToDocument ( dom );
	} else {
		alert ( "TODO!" );
	}
	return result;
	
}

/**
 * @param {DOMDocument} dom
 * @param {boolean} isPrettyPrint
 * @return {string}
 */
XSLTransformer.prototype.transformToString = function ( dom, isPrettyPrint ) {
	
	var result = null;
	if (Client.hasXSLTProcessor) {
		var doc = this.transformToDocument ( dom );
		result = DOMSerializer.serialize ( doc, isPrettyPrint ); 
	} else {
		var proc = this._cache.createProcessor ();
		proc.input = dom;
		proc.transform ();
		result = proc.output;
	}
	return result;
}