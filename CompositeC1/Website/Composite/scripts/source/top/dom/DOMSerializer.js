/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

/**
 * @class
 * DOMSerialzier.
 */
function _DOMSerializer () {}

_DOMSerializer.prototype = {

	_serializer: (window.XMLSerializer ? new XMLSerializer() : null),

	/**
	 * @param {DOMNode} node This should be an element or a document node.
	 * @param {boolean} isPrettyPrint Works in Mozilla only!
	 * @return {string}
	 */
	serialize : function ( node, isPrettyPrint ) {
		
		var result = null;
		var element = node;
		
		if ( node.nodeType == Node.DOCUMENT_NODE ) {
			element = node.documentElement;
		}

		if (element.xml != null)
		{
			return element.xml;
		}
		else if ( this._serializer != null) {
			if ( isPrettyPrint == true ) {
				element = element.cloneNode ( true );
				element = DOMFormatter.format ( element, DOMFormatter.INDENTED_TYPE_RESULT );
			}
			result = this._serializer.serializeToString ( element );
		}
		return result;
	}
}

/**
 * The instance that does it.
 * @type {_DOMSerializer}
 */
var DOMSerializer = new _DOMSerializer ();