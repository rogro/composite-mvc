/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

/**
 * @class
 * Debug that system.
 */
function _SystemDebug () {}

_SystemDebug.prototype = {
	
	_logger : SystemLogger.getLogger ( "SystemDebug" ),
	_stacklength : parseInt ( 5 ),

	/**
	 * Print sort of a strack trace.
	 * @param {object} args
	 * @param @optional {int} length
	 * @return
	 */
	stack : function ( args, length ) {
		
		this._stackMozilla ( args, length );
		/*
		if ( Client.isMozilla == true ) {
			this._stackMozilla ( args, length );
		} else {
			this._logger.debug ( "TODO!" );
		}
		*/
	},

	/**
	 * Stack Mozilla.
	 * @param {object} args
	 * @param @optional {int} length
	 */
	_stackMozilla : function ( args, length ) {
		
		length = length ? length : this._stacklength;
		if ( Client.isMozilla && args.callee || args.caller ) {
			var caller = Client.isMozilla ? args.callee.caller : args.caller.callee;
			var stack = "";
			
			var i = 0;
			while ( caller != null && i++ < length ) {
				stack += "\n#" + i + "\n";
				stack += caller.toString ();
				caller = caller.caller;
				stack += "\n";
			}
			
			this._logger.error ( stack );
		} else {
			this._logger.error ( "(Error stack unreachable!)" );
		}
	}

}

/*
 * The instance that does it.
 * @type {_SystemDebug}
 */
var SystemDebug = new _SystemDebug;
