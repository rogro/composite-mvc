/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

/**
 * Factory method in order to emulate SystemLogger syntax.
 * @param {object} object
 */
SystemTimer.getTimer = function ( object ) {
	
	return new SystemTimer ( object.toString ());
}

/**
 * @class
 * Simple stopwatch utility. Note that timing in 
 * Javascript should never be considered 100% reliable.
 */
function SystemTimer ( id ) {
	
	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "SystemTimer" );
	
	/**
	 * @type {string}
	 */
	this._id = id;
	
	/**
	 * @type {int}
	 */
	this._time = new Date ().getTime ();
}

/**
 * Reset timer.
 */
SystemTimer.prototype.reset = function () {
	
	this._time = new Date ().getTime ();
}

/**
 * Report time to system log.
 * @param {string} message
 */
SystemTimer.prototype.report = function ( message ) {
	
	this.logger.debug ( this._id +": " + this.getTime () + ( message ? ": " + message : "" ));
}

/**
 * Get time in milliseconds.
 * @return {int}
 */
SystemTimer.prototype.getTime = function () {
	
	return new Date ().getTime () - this._time;
}