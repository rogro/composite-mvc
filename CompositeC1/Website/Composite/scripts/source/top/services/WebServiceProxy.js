/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

/**
 * Logging SOAP? This has NO EFFECT in operational mode (only in developer mode)!
 * @type {boolean}
 */
WebServiceProxy.isLoggingEnabled = true;

/**
 * Flip this when webservice requests should return instances 
 * of DOMDocument instead of javascript objects. 
 * Remember to flip it back again!
 * @type {boolean}
 */
WebServiceProxy.isDOMResult = false;

/**
 * If set to true, the WebServiceProxy will display a special dialog on soap faults.
 * Whenever you adjust this property, remember to reset the value to true.
 * TODO: come up with some sort of SOAPFaultHandler to provide in webservice calls?
 * @type {boolean}
 */
WebServiceProxy.isFaultHandler = true;

/**
 * @class
 */
function WebServiceProxy () {
	
	this.logger = SystemLogger.getLogger ( "WebServiceProxy" );
}

/**
 * Create webservice proxy.
 * @param {string} url
 * @return {WebServiceProxy}
 */
WebServiceProxy.createProxy = function ( url ) {
	
	var wsdl = new WebServiceResolver ( url );
	var proxy = new WebServiceProxy ();
	
	var operations 	= wsdl.getOperations ();	
	operations.each ( function ( operation ) {
		proxy[operation.name] = WebServiceProxy.createProxyOperation(operation);
	});
	
	return proxy;
}

/** 
 * Logging SOAP in developermode.
 * @param {WebServiceOperation} operation
 * @param {SOAPMessage} soapMessage
 */
WebServiceProxy.prototype._log = function ( operation, soapMessage ) {
	
	if ( WebServiceProxy.isLoggingEnabled && Application.isDeveloperMode && soapMessage ) {
		var log = soapMessage instanceof SOAPRequest ? "SOAPRequest for " : "SOAPResponse from "; 
		log += operation.address + ": " + operation.name + "\n\n";
		log += DOMSerializer.serialize ( soapMessage.document, true )
		this.logger.fine ( log );
	}
}

/**
 * @param {WebServiceOperation} operation
 * @return {function}
 */
WebServiceProxy.createProxyOperation = function (operation) {

	/*
	* Method returns a function which in turn returns:
	* On request success, an {Object} or a {DOMDocument}.
	* On request error, a {SOAPFault}.
	*/
	return function () {
		var parameters = new List(arguments);
		var result = null;
		if (typeof (parameters.getLast()) == "function") {
			var onresponse = parameters.extractLast();
			var request = operation.encoder.encode(
				parameters
			);
			this._log(operation, request);
			var self = this;
			var response = request.asyncInvoke(operation.address, function (response) {
				self._log(operation, response);

				if (response) {
					if (response.fault) {
						result = SOAPFault.newInstance(operation, response.fault);
						if (WebServiceProxy.isFaultHandler) {
							WebServiceProxy.handleFault(result, request, response);
						}
					} else {
						if (WebServiceProxy.isDOMResult) {
							result = response.document;
						} else {
							result = operation.decoder.decode(response);
						}
					}
				}
				request.dispose();
				onresponse(result);
			});
		} else {
			var request = operation.encoder.encode(
				new List(arguments)
			);
			this._log(operation, request);
			var response = request.invoke(operation.address);
			this._log(operation, response);

			if (response) {
				if (response.fault) {
					result = SOAPFault.newInstance(operation, response.fault);
					if (WebServiceProxy.isFaultHandler) {
						WebServiceProxy.handleFault(result, request, response);
					}
				} else {
					if (WebServiceProxy.isDOMResult) {
						result = response.document;
					} else {
						result = operation.decoder.decode(response);
					}
				}
			}
			request.dispose();
			return result;
		}
	}
}

/**
 * Handle SOAP fault.
 * @param {SOAPFault} soapFault
 * @param {SOAPRequest} soapRequest
 * @param {SOAPRequestResponse} soapResponse
 */
WebServiceProxy.handleFault = function ( soapFault, soapRequest, soapResponse ) {
	
	try {
		Dialog.invokeModal ( 
			Dialog.URL_SERVICEFAULT,
			null, 
			{
				soapFault 		: soapFault,
				soapRequest 	: soapRequest,
				soapResponse 	: soapResponse
			}
		);
	} catch ( exception ) {
		alert ( 
			soapFault.getFaultString ()
		);
	}
}