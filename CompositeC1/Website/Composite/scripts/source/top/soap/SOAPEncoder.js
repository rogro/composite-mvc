/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

/**
 * @class
 * @param {WSDLParser} wsdl
 * @param {string} operationName
 */

function SOAPEncoder ( wsdl, operation ) {
	
	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "SOAPEncoder" );
	
	/** 
	 * @type {WSDLParser} 
	 * @private
	 */
	this._wsdl = wsdl;
	
	/** 
	 * @type {string} 
	 * @private
	 */
	this._operation = operation;
	
	/** 
	 * @type {string} 
	 * @private
	 */
	this._namespace = wsdl.getTargetNamespace ();
}

/**
 * @param {List} args
 * @return {SOAPMessage}
 */
SOAPEncoder.prototype.encode = function ( args ) {
	
	var message		= SOAPRequest.newInstance ( this._namespace, this._operation );
	var root 		= this._appendElement ( message.body, this._operation );
	var schema 		= this._wsdl.getSchema ();
	var schemaType 	= schema.lookup ( this._operation );
	var definitions	= schemaType.getListedDefinitions ();
	
	while ( definitions.hasNext ()) {
		var def = definitions.getNext ();
		var elm = this._appendElement ( root, def.name );
		var val = args.getNext ();
		this._resolve ( elm, def, val );
	}
	return message;
}

/**
 * @param {DOMElement} element
 * @param {SchemaDefinition} definition
 * @param {object} value
 */
SOAPEncoder.prototype._resolve = function ( element, definition, value ) {

	var schema = this._wsdl.getSchema ();
	
	if ( definition.isSimpleValue ) {
		this._appendText ( element, value, definition.type == "string" );
	} else {
	
		var schemaType 	= schema.lookup ( definition.type );
		if ( schemaType instanceof SchemaSimpleType ) {
			alert ( "SOAPEncoder: SchemaSimpleType support not implemented!" );
		} else {
			var defs = schemaType.getListedDefinitions ();
			if ( schemaType.isArray ) {
				var entries = new List ( value );
				var def = defs.getNext ();
				while ( entries.hasNext ()) {
					var elm = this._appendElement ( element, def.name );
					var val = entries.getNext ();
					this._resolve ( elm, def, val );
				}
			} else {

			    if (typeof value === "undefined") {
			        this.logger.error("SOAPEncoder: value is undefined");
			    } else {
			        while (defs.hasNext()) {

			            try {
			                var def = defs.getNext();
			                var elm = this._appendElement(element, def.name);

			                var val = value[def.name];
			                this._resolve(elm, def, val);
			            } catch(exception) {

			                // This can happen when opening dataitems in particular.
			                // Apparently, we recieve no OpenIcon but attempt to send it back...
			                this.logger.error("Mysterius malfunction in " + this._operation + ":\n\n" + def.name + ": " + value);
			            }
			        }
			    }
			}
		}
	}
}

/**
 * @param {DOMNode} node
 * @return {DOMElement}
 */
SOAPEncoder.prototype._appendElement = function ( node, name ) {
	
	var child = DOMUtil.createElementNS ( 
		this._namespace, name, node.ownerDocument 
	);
	node.appendChild ( child );
	return child;
}

/**
 * Text stripped according to http://www.w3.org/TR/REC-xml/#charsets because 
 * delicious Office applications may throw in all sorts of illegal characters. 
 * @param {DOMElement} element
 * @param {object} value
 * @param {boolean} isString - not really used...
 */
SOAPEncoder.prototype._appendText = function ( element, value, isString ) {
	
	if ( value != null ) {
		
		value = new String ( value );
		var safe = new String ( "" );
		var chars = value.split ( "" );
		var wasDeleted = false;
		var i = 0, c;
		
		while ( c = chars [ i++ ]) {
			
			var isAbort = true;
			var code = c.charCodeAt ( 0 );
			
			// case 0x10 :
			// case 0x13 :
			
			switch ( code ) {
				case 0x9 :
				case 0xA :
				case 0xD :
					isAbort = false;
					break;
				default :
					if (
						( code >= 0x20 && code <= 0xD7FF ) ||
						( code >= 0xE000 && code  <= 0xFFFD ) || 
						( code >= 0x10000 && code <= 0x10FFFF )
					) {
						isAbort = false;
					}
					break;
			}
			if ( !isAbort ) {
				safe += c;
			} else {
				wasDeleted = true;
			}			
		}
		
		if ( wasDeleted ) {
			this.logger.debug ( "Illegal XML character(s) was deleted from the string: " + value )
		}
		
		element.appendChild ( element.ownerDocument.createTextNode ( safe ));
	}
}