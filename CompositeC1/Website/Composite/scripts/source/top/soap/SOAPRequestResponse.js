/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

SOAPRequestResponse.prototype = new SOAPMessage;
SOAPRequestResponse.prototype.constructor = SOAPRequestResponse;
SOAPRequestResponse.superclass = SOAPMessage.prototype;

/**
 * @class
 * Please use static factory method, see below. The word "SOAPResponse" is reserved 
 * for a Mozilla native javascript object, unfortunately we cannot use it.
 * TODO: Soap has been discontinued in Firefox 3.0, so maybe we can use it soon...
 */
function SOAPRequestResponse () {}

/**
 * @type {SystemLogger}
 */
SOAPRequestResponse.logger = SystemLogger.getLogger ( "SOAPRequestResponse" );

/**
 * @type {XPathResolver}
 */
SOAPRequestResponse.resolver = new XPathResolver ();
SOAPRequestResponse.resolver.setNamespacePrefixResolver ({
	"soap" : Constants.NS_ENVELOPE
});

/**
 * @param {DOMDocument} doc
 */
SOAPRequestResponse.newInstance = function ( doc ) {
	
	var response = null;
	
	if ( doc && doc.documentElement ) {
	
		response = new SOAPRequestResponse ();
		var resolver = SOAPRequestResponse.resolver;
		
		response.document	= doc;
		response.envelope	= resolver.resolve ( "soap:Envelope", response.document );
		response.header		= resolver.resolve ( "soap:Header", response.envelope );
		response.body		= resolver.resolve ( "soap:Body", response.envelope );
		
		var fault = resolver.resolve ( "soap:Fault", response.body );
		if ( fault ) {
			SOAPRequestResponse.logger.fatal ( 
				DOMSerializer.serialize ( fault, true )
			);
			response.fault = {
				element 			: fault,
				faultNamespaceURI	: fault.namespaceURI,
				faultCode		 	: DOMUtil.getTextContent ( resolver.resolve ( "faultcode", fault )),
				faultString		 	: DOMUtil.getTextContent ( resolver.resolve ( "faultstring", fault )),
				detail				: fault.getElementsByTagName ( "detail" ).item ( 0 )
			}
		}
	}
	return response;
}