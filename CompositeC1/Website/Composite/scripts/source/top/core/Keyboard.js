/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

/**
 * @class
 * Broadcasting keys globally. This class is closely connected to the 
 * KeyGroupBinding going on in the root application file "top.aspx".
 */
function _Keyboard () {}

_Keyboard.prototype = {
	
	/**
	 * @type {SystemLogger}
	 */
	_logger : SystemLogger.getLogger ( "Keyboard" ),
	
	/** 
	 * @type {boolean}
	 */
	isShiftPressed : false,
	
	/** 
	 * @type {boolean}
	 */
	isControlPressed : false,
	
	/**
	 * Enter key pressed.
	 */
	keyEnter : function () {
		
		EventBroadcaster.broadcast ( BroadcastMessages.KEY_ENTER );
	},
	
	/**
	 * Escape key pressed.
	 */
	keyEscape : function () {
		
		EventBroadcaster.broadcast ( BroadcastMessages.KEY_ESCAPE );
	},
	
	/**
	 * Space key pressed.
	 */
	keySpace : function () {
		
		EventBroadcaster.broadcast ( BroadcastMessages.KEY_SPACE );
	},
	
	/**
	 * Shift key pressed. Another broadcast is 
	 * transmitted when the shift key is released.
	 * @see {Keyboard#keyUp} 
	 */
	keyShift : function () {
		
		this.isShiftPressed = true;
		EventBroadcaster.broadcast ( BroadcastMessages.KEY_SHIFT_DOWN );
	},
	
	/**
	 * Control key pressed. Another broadcast is 
	 * transmitted when the control key is released.
	 * @see {Keyboard#keyUp}
	 */
	keyControl : function () {
		
		this.isControlPressed = true;	
		EventBroadcaster.broadcast ( BroadcastMessages.KEY_CONTROL_DOWN );
	},
	
	/**
	 * Arrow key pressed.
	 */
	keyArrow : function ( key ) {
		
		EventBroadcaster.broadcast ( BroadcastMessages.KEY_ARROW, key );
	},
	
	/**
	 * Arrow key pressed.
	 */
	keyAlt : function () {
		
		EventBroadcaster.broadcast ( BroadcastMessages.KEY_ALT );
	},
	
	/**
	 * Tab key pressed.
	 */
	keyTab : function () {
		
		EventBroadcaster.broadcast ( BroadcastMessages.KEY_TAB );
	},
	
	/**
	 * Special broadcast whenever the shift or control key is released. 
	 * This method is invoked by the local {@link DocumentManager}.
	 * @param {KeyEvent} e
	 */
	keyUp : function ( e ) {
		
		if ( this.isShiftPressed && e.keyCode == window.KeyEventCodes.VK_SHIFT ) {
			this.isShiftPressed = false;
			EventBroadcaster.broadcast ( BroadcastMessages.KEY_SHIFT_UP );
		} else if ( this.isControlPressed && e.keyCode == window.KeyEventCodes.VK_CONTROL ) {
			this.isControlPressed = false;
			EventBroadcaster.broadcast ( BroadcastMessages.KEY_CONTROL_UP );
		}
	}
}

/**
 * The instance that does it.
 */
var Keyboard = new _Keyboard ();