/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

/**
*@class
* @param @url {string} url.
* @constructor
*/
//TODO Add escaping
function Uri(url) {

    var mediaExpr = /^(~?\/|(\.\.\/)+|https?:\/\/[\w\d-\.:]*\/)(media|page)(\(|%28)[\w\d-\:]+(\)|%29)/;
	var result = mediaExpr.exec(url?url:"");

	if (result) {
		if (result[3] == "media") {
			this.isMedia = true;
		} else if (result[3] == "page") {
			this.isPage = true;
		}
	}
	var queryString = {};
	url.replace(/^[^\?]*/g, "").replace(
		/([^?=&]+)(=([^&]*))?/g,
		function ($0, $1, $2, $3) { queryString[$1] = $3; }
	);

	this.queryString = queryString;

	this.path = url.replace(/\?.*/g, "");

	return this;
}


Uri.isMedia = function(url) {
	return new Uri(url).isMedia;
};

/**
* Get media path without parameters
* @return {string}
*/
Uri.prototype.getPath = function() {
	return this.path;
};

/**
* Get media path without parameters
* @return {string}
*/
Uri.prototype.getQueryString = function () {
	return new Map(this.queryString);
};

/**
* Has param in query string?
* @param {object} key
* @return {boolean}
*/
Uri.prototype.hasParam = function(key) {
	return this.queryString[key] != null;
};

/**
* Get param in query string
* @param {object} key
* @return {boolean}
*/
Uri.prototype.getParam = function(key) {
	return this.queryString[key];
};
/**
* Set param in query string
* @param {object} key
*/
Uri.prototype.setParam = function(key, value) {
	if (value == undefined) {
		delete this.queryString[key];
	} else {
		this.queryString[key] = value;
	}
};

/**
* @return {string}
*/
Uri.prototype.toString = function () {
	var url = this.path;

	var querystring = [];
	for (var key in this.queryString) {
		querystring.push(key + "=" + this.queryString[key]);
	}

	if (querystring.length > 0)
		url += "?" + querystring.join("&");

	return url;
}