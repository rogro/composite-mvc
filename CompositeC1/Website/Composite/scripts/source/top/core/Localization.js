/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

/**
 * @class
 * This fellow handles localisation of the public
 * website (and not the admininstration module).
 */
function _Localization () {
	
	EventBroadcaster.subscribe ( BroadcastMessages.APPLICATION_LOGIN, this );
	EventBroadcaster.subscribe ( BroadcastMessages.LANGUAGES_UPDATED, this );
	EventBroadcaster.subscribe ( BroadcastMessages.FROMLANGUAGE_UPDATED, this );
	EventBroadcaster.subscribe ( BroadcastMessages.TOLANGUAGE_UPDATED, this );
}

_Localization.prototype = {
	
	/**
	 * Available languages. Each entry in the list has the following properties: 
	 * Name
     * IsoName
     * UrlMappingName
     * IsCurrent
     * SerializedActionToken         
	 * @type {List<object>}
	 */
	languages : null,
		
	/**
	 * The source language.
	 * @type {string}
	 */
	source : null,
	
	/**
	 * The target language.
	 * @type {string}
	 */
	target : null,

	/**
	 * Is RTL tDirection.
	 * @type {Boolean}
	 */
	isRtl: false,
	
	/**
	 * @implements {IBroadcastListener}
	 * @param {string} broadcast
	 * @param {object} arg
	 */
	handleBroadcast : function ( broadcast, arg ) {
		
		/*
		 * Get list of languages.
		 */
		switch ( broadcast ) {
			case BroadcastMessages.APPLICATION_LOGIN :
			case BroadcastMessages.LANGUAGES_UPDATED:
			case BroadcastMessages.TOLANGUAGE_UPDATED:
				this.isRtl = LocalizationService.GetTextDirection(true) == "rtl";
				var languages = LocalizationService.GetActiveLocales ( true );
				if ( languages.length >= 1 ) {
					this.languages = new List ( languages );
				} else {
					this.languages = null;
				}
				EventBroadcaster.broadcast ( BroadcastMessages.UPDATE_LANGUAGES, this.languages );
				break;
		}
		
		/*
		 * Get current languages.
		 */
		switch ( broadcast ) {
			case BroadcastMessages.APPLICATION_LOGIN :
			case BroadcastMessages.FROMLANGUAGE_UPDATED :
				var locales = LocalizationService.GetLocales ( true );
				this.source = locales.ForeignLocaleName;
				this.target = locales.ActiveLocaleName;
				
				/*
				 * Who needs this? Delete?
				 */
				EventBroadcaster.broadcast ( BroadcastMessages.LOCALIZATION_CHANGED, {
					source : locales.ForeignLocaleName,
					target : locales.ActiveLocaleName
				});
				break;
		}
	},

	/**
	 * Return current lang in short format
	 */
	currentLang : function()
	{
		if (this.languages != null) {
			var languages = this.languages.copy();
			while (languages.hasNext()) {
				var lang = languages.getNext();
				if (lang.IsCurrent) {
					return lang.IsoName;
				}
			}
		}
		return null;
	}
}

/**
 * The instance that does it!
 * @type {_Localization}
 */
var Localization = new _Localization ();