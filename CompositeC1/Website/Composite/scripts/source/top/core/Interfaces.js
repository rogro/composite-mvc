/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

/**
 * @class
 * Primitively checks an object instance for an interface implementation. 
 * TODO: build up some sort of status repport for debugging.
 */
function _Interfaces () {
	
	/**
	 * This logger could probably report a detailed result.
	 * @type {SystemLogger}
	 */
	var logger = SystemLogger.getLogger ( "Interfaces" );
	
	/**
	 * Is interface implemented by instance object?
	 * @param {object} interfais (not using reserved keyword here)
	 * @param {object} instance
	 * @param {object} isLogging
	 * @return {boolean}
	 */
	this.isImplemented = function ( interfais, instance, isLogging ) {
		
		var isImplemented = true;
		for ( var property in interfais ) {
			if ( typeof instance [ property ] == Types.UNDEFINED ) {
				isImplemented = false;
			} else if ( typeof interfais [ property ] != typeof instance [ property ]) {
				isImplemented = false;
			}
			if ( !isImplemented ) {
				break;
			}
		}
		if ( !isImplemented ) {
			if ( isLogging ) {
				logger.fine ( instance + " invalid. Interface check abandoned at: " + property );
			}
		}
		return isImplemented;
	}
}

var Interfaces = new _Interfaces;