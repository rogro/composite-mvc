/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

/**
 * @class
 */
function _Types () {}

_Types.prototype = {
	
	/**
	 * @type {SystemLogger}
	 */
	_logger : SystemLogger.getLogger ( "Types" ),
	
	BOOLEAN 	: "boolean",
	STRING 	 	: "string",
	NUMBER 		: "number",
	FUNCTION 	: "function",
	UNDEFINED	: "undefined",
	
	/**
	 * Autocast string to an inferred type.
	 * To be used with caution and scepsis. 
	 * @param {string} string
	 * @return {object}
	 */
	castFromString : function ( string ) {
		
		var result = string;
		
		if ( parseInt ( result ).toString () === result ) {
			result = parseInt ( result );
		} else if ( parseFloat ( result ).toString () === result ) {
			result = parseFloat ( result );
		} else if ( result === "true" || result === "false" ) {
			result = ( result === "true" );
		}
		return result;
	},
	
	/**
	 * Is it defined?
	 * @param {object} arg
	 * @return {boolean}
	 */
	isDefined : function ( arg ) {
		
		return typeof arg != Types.UNDEFINED;
	},
	
	/**
	 * Is it a function?
	 * @param {object} arg
	 * @return {boolean}
	 */
	isFunction : function ( arg ) {
		
		return typeof arg == Types.FUNCTION;
	}
}

/**
 * The instance that does it!
 * @type {_Types}
 */
var Types = new _Types ();