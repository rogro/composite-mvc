/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

/**
 * @class
 */
window.StringBundle = new function () {
	
	var logger = SystemLogger.getLogger ( "StringBundle" );
	
	/*
	 * Provider shorthand.
	 */
	this.UI = "Composite.Management";
	
	/*
	 * Mapping providers.
	 * @type {HashMap<string><HashMap<string><string>>}
	 */
	var providers = {};
	
	/**
	 * Populate provider via webservice.
	 * @param {string} providername
	 * @param {HashMap} provider
	 * @param {HashMap}
	 */
	function resolve ( providername, provider ) {
		
		var list = new List (
			StringService.GetLocalisation ( providername )
		);
		if ( list.hasEntries ()) {
			list.each (
				function ( entry ) {
					provider [ entry.Key ] = entry.Value;
				}
			);
		} else {
			throw "No strings from provider: " + providername;
		}
	}
	
	/**
	 * Get string!
	 * @param {string} providername
	 * @param {string} stringkey
	 * @return {string}
	 */
	this.getString = function ( providername, stringkey ) {
		
		var result = null;
		
		if ( window.StringService != null ) {
			try {
				if ( providername == "ui" ) {
					providername = StringBundle.UI;
				}
				if ( !providers [ providername ] ) {
					var provider = providers [ providername ] = {};
					resolve ( providername, provider );
				}
				if ( providers [ providername ]) {
					result = providers [ providername ][ stringkey ]
				}
				if ( !result ) {
					throw "No such string: " + stringkey;
				}
			} catch ( exception ) {
				var cry = "StringBundle exception in string " + providername + ":" + stringkey;
				logger.error ( cry );
				if ( Application.isDeveloperMode ) {
					alert ( cry );
				}
			}
		}
		return result;
	}
}