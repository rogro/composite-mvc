/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

/**
 * @class
 * Search tokens are used to filter the content of a {@link SystemTreeBinding}.
 */
window.SearchTokens = new function () {
	
	/*
	 * Tokens indexed by key. This list should be manually maintained 
	 * to match available search tokens provided by the mighty server. 
	 * This will make sure that we don't misspell these strings.
	 */
	var tokens = {
		
		// searching gif, jpeg and png files.
		"MediaFileElementProvider.WebImages" : null,
		// searching flash, quicktime, director and windows media files.
		"MediaFileElementProvider.EmbeddableMedia": null,
		// searching only writable folders.
		"MediaFileElementProvider.WritableFolders": null,
		// searching functions that return XhtmlDocument (suitable for rendering) 
		"AllFunctionsElementProvider.VisualEditorFunctions": null,
		// searching functions that are sutable for Xslt function's function call section
		"AllFunctionsElementProvider.XsltFunctionCall": null
	}
	
	/**
	 * Get token by key.
	 * @param {string} key 
	 */
	this.getToken = function ( key ) {
		
		var result = null;
		if ( this.hasToken ( key )) {
			result = tokens [ key ];
		} else {
			throw "Unknown search token key: " + key;
		}
		return result;
	}
	
	/**
	 * Has token?
	 * @param {string} key
	 * @return {boolean}
	 */
	this.hasToken = function ( key ) {
		
		return typeof tokens [ key ] != Types.UNDEFINED;
	}
	
	/*
	 * Fetch tokens on login.
	 */
	EventBroadcaster.subscribe ( BroadcastMessages.APPLICATION_LOGIN, {
		handleBroadcast : function () {
			new List ( TreeService.GetSearchTokens ( true )).each ( 
				function ( token ) {
					if ( SearchTokens.hasToken ( token.Key )) {
						tokens [ token.Key ] = token.Value;
					} else {
						alert ( "SearchTokens need updating!" );
					}
				}
			);
		}
	});
}