/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

/**
 * @class
 * Allows basic communication between C1 and the Prism host. 
 * The Composite C1 extension must be installed in Prism.
 */
function _Prism () {}
_Prism.prototype = {
		
	/**
	 * @type {SystemLogger}
	 */
	_logger : SystemLogger.getLogger ( "Prism" ),
	
	/**
	 * This will clear the cache (in Prism only).
	 */
	clearCache : function () {
	
		this._logger.fine ( "Clearing the cache" );
		this._dispatchToPrism ( "contenttochrome-clearcache" );
	},
	
	/**
	 * This will disable forced cache in Prism. Forced. 
	 * cache is a setup were files are NEVER checked 
	 * for newer versions on server unless expired.
	 */
	disableCache : function () {
		
		this._logger.fine ( "Disabling cache" );
		this._dispatchToPrism ( "contenttochrome-cache-disable" );
	},
	
	/**
	 * This will enable forced cache in Prism (see note above).
	 */
	enableCache : function () {
		
		this._logger.fine ( "Enabling cache" );
		this._dispatchToPrism ( "contenttochrome-cache-enable" );
	},
	
	/**
	 * Dispatch event to Prism host.
	 * @param {string} type
	 */
	_dispatchToPrism : function ( type ) {
		
		if ( Client.isPrism ) {
			var event = document.createEvent ( "Events" );
			event.initEvent ( type, true, true );
			window.dispatchEvent ( event );
		} else {
			this._logger.warn ( "Prism methods should only be invoked in Prism! (" + type + ")" );
		}
	}
}

/**
 * The instance that does it.
 * @type {_Chrome}
 */
var Prism = new _Prism ();