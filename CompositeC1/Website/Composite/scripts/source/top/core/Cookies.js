/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

/**
 * @class
 * TODO: Dont use cookies! Define a functional Persistance instead.
 */
function _Cookies () {}

_Cookies.prototype = {
	
	/**
	 * Create new cookie.
	 * @param {string} name
	 * @param {string} value
	 * @param {string} days
	 */
	createCookie : function ( name, value, days ) {
	
		var expires = "";
		if ( days ) {
			var date = new Date ();
			date.setTime ( date.getTime () + ( days * 24 * 60 * 60 * 1000 ));
			expires = "; expires=" + date.toGMTString ();
		}
		document.cookie = name + "=" + escape ( value ) + expires + "; path=/";
		return this.readCookie ( name );
	},
	
	/**
	 * Read existing cookie.
	 * @param {string} name
	 * @return {string}
	 */
	readCookie : function ( name ) {
	
		var result = null;
		var nameEQ = name + "=";
		var ca = document.cookie.split ( ";" );
		for( var i=0; i < ca.length; i++ ) {
			var c = ca [i];
			while ( c.charAt ( 0 )== " " ) c = c.substring ( 1, c.length );
			if ( c.indexOf ( nameEQ ) == 0 ) {
				result = unescape ( c.substring ( nameEQ.length, c.length ));
			}
		}
		return result;
	},
	
	/**
	 * No more cookie.
	 * @param {string} name
	 */
	eraseCookie : function ( name ) {
	
		this.createCookie ( name, "", -1 );
	}
}
var Cookies = new _Cookies();