/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

/**
 * @class
 * @see {Options}
 */
window.Preferences = new function () {
	
	var logger = SystemLogger.getLogger ( "Preferences" );
	
	/*
	 * Preferrably using defined constants to avoid spelling mistakes.
	 */
	this.LOGIN = "login";
	
	/* 
	 * Default preferences.
	 */
	var preferences = {
		"login" : true
	};
	
	/* 
	 * Fetch preferences on startup.
	 */
	EventBroadcaster.subscribe ( BroadcastMessages.LOCALSTORE_INITIALIZED, {
		handleBroadcast : function () {
			if ( LocalStore.isEnabled ) {
				var store = LocalStore.getProperty ( LocalStore.PREFERENCES );
				if ( store ) {
					for ( var key in store ) { // "overloading" not replacing!
						preferences [ key ] = store [ key ];
					}
					debug ( true );
				} else {
					debug ( false );
				}
			} else {
				debug ( false );
			}
		}
	});
	
	/* 
	 * Store preferences on shutdown.
	 */
	EventBroadcaster.subscribe ( BroadcastMessages.APPLICATION_SHUTDOWN, {
		handleBroadcast : function () {
			if ( LocalStore.isEnabled ) {
				LocalStore.setProperty ( 
					LocalStore.PREFERENCES,
					preferences
				);
			}
		}
	});
	
	/**
	 * Get preference.
	 * @param {string} key
	 * @return {object}
	 */
	this.getPref = function ( key ) {
		
		var result = null;
		if ( key ) {
			result = preferences [ key ];
		} else {
			throw "No such preference.";
		}
		return result;
	}
	
	/**
	 * Set preference.
	 * @param {string} key
	 * @param {object} value
	 */
	this.setPref = function ( key, value ) {
		
		if ( key ) {
			preferences [ key ] = value;
		} else {
			throw "No such preference.";
		}
	}
	
	/**
	 * Logging preferences on startup.
	 * @param {boolean} hasStoredPreferences 
	 */
	function debug ( hasStoredPreferences ) {
		
		var output = hasStoredPreferences ? 
			"Persisted preferences" : 
			"No persisted preferences. Using defaults";
			
		output += ":\n";
		for ( var key in preferences ) {
			var pref = preferences [ key ];
			output += "\n\t" + 
				key + ": " + 
				pref + " [" + 
				typeof pref + "]";
		}
		logger.fine ( output );
	}
}