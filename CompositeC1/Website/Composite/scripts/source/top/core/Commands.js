/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

/**
 * @class
 * Simply because we need to wrap some code up in short method   
 * names that can be accessed easily in inline markup.
 */
function _Commands () {
	
	this._construct ();
}

/**
 * Code.
 */
_Commands.prototype = {
	
	_URL_ABOUTDIALOG : "${root}/content/dialogs/about/about.aspx",
	_URL_PREFERENCES : "${root}/content/dialogs/preferences/preferences.aspx",
	
	/**
	 * Construct.
	 */
	_construct : function () {
		
		var self = this;
		EventBroadcaster.subscribe ( BroadcastMessages.SAVE_ALL, {
			handleBroadcast : function ( broadcast, arg ) {
				self.saveAll ( arg );
			}
		})
	},

	/**
	 * Opens the About dialog.
	 */
	about : function () {
		
		this._dialog ( this._URL_ABOUTDIALOG );
	},
	
	/**
	 * Opens the Preferences dialog.
	 */
	preferences : function () {
		
		this._dialog ( this._URL_PREFERENCES );
	},
	
	/**
	 * Taking care to fadeout menus before opening dialogs, otherwise   
	 * fading may be jaggy. But what if nobody was using a menu?...
	 */
	_dialog : function ( url ) {
		
		if ( Client.hasTransitions ) {
			setTimeout ( function () {
				Dialog.invokeModal ( url );
			}, Animation.DEFAULT_TIME );
		} else {
			Dialog.invokeModal ( url );
		}
	},
	
	/**
	 * Close current editor. This broadcast is intercepted by the DockTabBinding. 
	 */
	close : function () {
		
		EventBroadcaster.broadcast ( BroadcastMessages.CLOSE_CURRENT );
	},
	
	/**
	 * Close all (editors). 
	 */
	closeAll : function () {
		
		this.saveAll ( true );
	},
	
	/**
	 * Save current editor [CTRL+S].  This broadcast is intercepted by the DockTabBinding.
	 */
	save: function () {
		EventBroadcaster.broadcast ( BroadcastMessages.SAVE_CURRENT );
	},
	
	/**
	 * Save all dirty tabs (in all perspectives), prompting a list.
	 * @param {boolean} isCloseAll
	 */
	saveAll : function ( isCloseAll ) {
		
		/*
		 * Invoke dialog and collect 
		 * selected tabs in a list.
		 */
		var self = this;
		var docktabs = Application.getDirtyDockTabsTabs ();
		if ( docktabs.hasEntries ()) {
			Dialog.invokeModal ( "${root}/content/dialogs/save/saveall.aspx", {
				handleDialogResponse : function ( response, result ) {
					switch ( response ) {
						case Dialog.RESPONSE_ACCEPT :
							self._handleSaveAllResult ( result, isCloseAll );
							break;
						case Dialog.RESPONSE_CANCEL :
							/*
							 * Needed for language-change scenario...
							 */
							EventBroadcaster.broadcast ( BroadcastMessages.SAVE_ALL_DONE );
							break;
					}
				}
			}, docktabs );
		} else if ( isCloseAll ){
			EventBroadcaster.broadcast ( BroadcastMessages.CLOSE_ALL );
		}
	},
	
	/**
	 * Handle result from "save all" dialog.
	 * @param {DataBindingResultMap} result
	 * @param {boolean} isCloseAll
	 * @return {boolean} True if something was dirty...
	 */
	_handleSaveAllResult : function ( result, isCloseAll ) {
	
		var returnable = false;
		
		var list = new List ();
		result.each ( function ( name, tab ) {
			if ( tab != false ) {
				list.add ( tab );
			}
		});
		/*
		 * Save tabs from list.
		 */
		if ( list.hasEntries ()) {
			returnable = true;
			var count = list.getLength ();
			var handler = { 
				handleBroadcast : function ( broadcast, tab ) {
					if ( --count == 0 ) {
						EventBroadcaster.unsubscribe ( BroadcastMessages.DOCKTAB_CLEAN, this );
						EventBroadcaster.broadcast ( BroadcastMessages.SAVE_ALL_DONE );
						if ( isCloseAll ) {
							EventBroadcaster.broadcast ( BroadcastMessages.CLOSE_ALL );
						}
					}
				}
			}
			EventBroadcaster.subscribe ( BroadcastMessages.DOCKTAB_CLEAN, handler );
			list.each ( function ( tab ) {
				tab.saveContainedEditor ();
			});
		} else {
			/*
			 * Needed for language-change scenario...
			 */
			EventBroadcaster.broadcast ( BroadcastMessages.SAVE_ALL_DONE );
		}
		
		return returnable;
	},
	
	/**
	 * Flip display of the system log [control+shift+L]. If an error prevented 
	 * the system from starting normally, you'll be happy to know that the log 
	 * is still able to open in a regular browser window.
	 */
	systemLog : function () {
		
		if ( Application.isOperational ) {
			StageBinding.handleViewPresentation ( "Composite.Management.SystemLog" );
		} else {
			var win = window.open ( Constants.APPROOT + "/content/views/dev/systemlog/systemlogoutput.html" );
			win.onload = function () {
				EventBroadcaster.broadcast ( BroadcastMessages.SYSTEMLOG_OPENED, this );
			}
		}
	},
	
	/**
	 * Launch the Help view.
	 */
	help : function () {
		
		var handle = "Composite.Management.Help";
		if ( !StageBinding.isViewOpen ( handle )) {
			StageBinding.handleViewPresentation ( handle );
		}
	}
}

/**
 * The instance that does it.
 */
var Commands = new _Commands ();