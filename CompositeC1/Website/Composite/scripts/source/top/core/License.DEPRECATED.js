/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

/**
 * @class
 */
window.License = new function () {
	
	/**
	 * @type {boolean}
	 */
	this.isRegistered = false;
	
	/**
	 * @type {boolean}
	 */
	this.isExpired = false;
	
	/**
	 * @type {string}
	 */
	this.registrationName = null;
	
	/**
	 * @type {string}
	 */
	this.registrationURL = null;
	
	/**
	 * @type {string}
	 */
	this.statusURL = null;
	
	/**
	 * Robot readable build version "1.2.3505.18361".
	 * @type {string}
	 */
	this.versionString = null;
	
	/**
	 * Human readable product version "C1 1.2 SP2".
	 * @type {string}
	 */
	this.versionPrettyString = null;
	
	/**
	 * @type {string}
	 */
	this.installationID = null;
	
	/**
	 * Refresh license. This is done at least once, on Application startup.
	 * @param {boolean} isHardUpdate This will trigger the server to discover any new lisense.
	 */
	this.refresh = function ( isHardUpdate ) {
		
		/*
		if ( isHardUpdate ) {
			LicensingService.InvokeLicenseFetch ( true );
		}
		*/
		
		this.isRegistered = true; // HARDCODED FOR NOW. WAS: LicensingService.Registered ( true );
		
		var self = this;
		new List ( InstallationService.GetLicenseInfo ( true )).each ( function ( entry ) {
			switch ( entry.Key ) {
				case "RegistrationURL" :
					self.registrationURL = entry.Value;
					break;
				case "StatusURL" :
					self.statusURL = entry.Value;
					break;
				case "ProductVersion" :
					self.versionString = entry.Value;
					break;
				case "ProductTitle" :
					self.versionPrettyString = entry.Value;
					break;
				case "RegisteredTo" :
					self.registrationName = entry.Value;
					break;
				case "Expired" :
					self.isExpired = entry.Value == "True";
					break;
				case "InstallationId" :
					self.installationID = entry.Value;
					break;
			}
		});
	};
}