/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

/**
 * @class
 * @implements {IFocusable}
 */
var IData = new function () {
	
	/**
	 * @implements {IFocusable}
	 * @type {boolean}
	 */
	this.isFocusable = true;
	
	/**
	 * Validate.
	 * @return {boolean}
	 */
	this.validate = function () {};
	
	/**
	 * Manifest. This will write form elements into page DOM 
	 * so that the server recieves something on form submit.
	 * @return {Binding} Although we probably return null...
	 */
	this.manifest = function () {};
	
	/**
	 * Mark binding dirty and set a flag in the local DataManager. 
	 */
	this.dirty = function () {};
	
	/**
	 * Remove the dirty mark.
	 */
	this.clean = function () {};
	
	/**
	 * Focus.
	 * @implements {IFocusable}
	 */
	this.focus = function () {};
	
	/**
	 * Blur.
	 * @implements {IFocusable}
	 */
	this.blur = function () {};
	
	/**
	 * Get name.
	 * @return {string}
	 */
	this.getName = function () {};
	
	/**
	 * Get value. This is intended for serverside processing.
	 * @return {string}
	 */
	this.getValue = function () {};
	
	/**
	 * Set value.
	 * @param {string} value
	 */
	this.setValue = function ( value ) {};
	
	/**
	 * Get result. This is intended for clientside processing.
	 * @return {object}
	 */
	this.getResult = function () {};
	
	/**
	 * Set result.
	 * @see {DataManager#populateDataBindings}
	 * @param {object} result
	 */
	this.setResult = function ( result ) {};
}