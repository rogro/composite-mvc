/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

/**
 * Button image handling have been separated out so that we 
 * can hardcode-declare it for certain types of buttons.
 * @param {object} object Any type of object with four special properties.
 */
function ImageProfile ( object ) {
	
	this._default		= object.image;
	this._hover 		= object.imageHover;
	this._active		= object.imageActive;
	this._disabled		= object.imageDisabled;
}

/**
 * Get default image.
 * @return {string}
 */
ImageProfile.prototype.getDefaultImage = function () {

	return this._default;
}

/**
 * Set default image.
 * @param {string} image
 */
ImageProfile.prototype.setDefaultImage = function ( image ) {
	
	this._default = image;
}

/**
 * Get hover image.
 * @return {string}
 */
ImageProfile.prototype.getHoverImage = function () {

	return this._hover;
}

/**
 * Set default image.
 * @param {string} image
 */
ImageProfile.prototype.setHoverImage = function ( image ) {
	
	this._hover = image;
}

/**
 * Get active image.
 * @return {string}
 */
ImageProfile.prototype.getActiveImage = function () {

	return this._active;
}

/**
 * Set active image.
 * @param {string} image
 */
ImageProfile.prototype.setActiveImage = function ( image ) {
	
	this._active = image;
}

/**
 * Get disabled image.
 * @return {string}
 */
ImageProfile.prototype.getDisabledImage = function () {

	return this._disabled;
}

/**
 * Set disabled image.
 * @param {string} image
 */
ImageProfile.prototype.setDisabledImage = function ( image ) {
	
	this._disabled = image;
}