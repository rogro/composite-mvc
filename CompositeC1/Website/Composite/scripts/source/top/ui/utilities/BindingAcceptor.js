/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

/**
 * @type {Binding}
 */
BindingAcceptor.acceptingBinding = null;

/** 
 * @param {Binding} binding
 */
function BindingAcceptor ( binding ) {

	/** 
	 * @type {SystemLogger} 
	 */
	this.logger = SystemLogger.getLogger ( "BindingDragger" );

	/** 
	 * @type {Binding} 
	 */
	this._binding = binding;	
	
	/**
	 * @type {HashMap<string><boolean>}
	 */
	this._acceptedList = {};
	
	/**
	 * @type {boolean}
	 */
	this._isAccepting = false;
	
	/**
	 * @type {CursorBinding}
	 */
	this._corsor = null;
	
	/* 
	 * Initialize.
	 */
	this._initialize ();
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Initialize.
 */
BindingAcceptor.prototype._initialize = function () {
	
	EventBroadcaster.subscribe ( BroadcastMessages.TYPEDRAG_START, this );
	EventBroadcaster.subscribe ( BroadcastMessages.TYPEDRAG_STOP, this );
	
	if ( this._binding.dragAccept ) {
	
		EventBroadcaster.subscribe ( BroadcastMessages.TYPEDRAG_PAUSE, this );
	
		var types = new List ( 
			this._binding.dragAccept.split ( " " )
		);
		while ( types.hasNext ()) {
			var type = types.getNext ();
			this._acceptedList [ type ] = true;
		}
	}
}

/**
 * @implements {IBroadcastListener}
 * @param {string} broadcast
 * @param {object} arg
 */
BindingAcceptor.prototype.handleBroadcast = function ( broadcast, arg ) {
	
	var type = arg;
	
	try {
	
	switch ( broadcast ) {
	
		case BroadcastMessages.TYPEDRAG_START :
			if ( this._cursor == null ) {
				this._cursor = app.bindingMap.dragdropcursor;
			}
			this._binding.addEventListener ( DOMEvents.MOUSEENTER, this );
			this._binding.addEventListener ( DOMEvents.MOUSELEAVE, this );
			if ( this.isAccepting ( type )) {
				this._isAccepting = true;
				this._startAccepting ();
			}
			break;
			
		case BroadcastMessages.TYPEDRAG_STOP :
			this._binding.removeEventListener ( DOMEvents.MOUSEENTER, this );
			this._binding.removeEventListener ( DOMEvents.MOUSELEAVE, this );
			if ( this.isAccepting ( type )) {
				this._isAccepting = false;
				this._stopAccepting ();
			}
			break;
			
		case BroadcastMessages.TYPEDRAG_PAUSE :	
			if ( this.isAccepting ( type )) {
				this._pauseAccepting ();
			}
			break;
	}
	
	} catch ( exception ) {
		this.logger.debug ( exception );
	}
}

/**
 * Is accepting type?
 * @param {string} type
 * @return {boolean}
 */
BindingAcceptor.prototype.isAccepting = function ( type ) {
	
	return Types.isDefined ( this._acceptedList [ type ]);
}

/**
 * Start accepting binding.
 */
BindingAcceptor.prototype._startAccepting = function () {
	
	if ( Types.isFunction ( this._binding.showGeneralAcceptance )) {
		this._binding.showGeneralAcceptance ();
	}
}

/**
 * Pause accepting binding.
 */
BindingAcceptor.prototype._pauseAccepting = function () { 
	
	/*
	if ( this._binding.hideGeneralAcceptance ) {
 		this._binding.hideGeneralAcceptance ();
 	}
 	*/
 	if ( this._binding.hideAcceptance ) {
		this._binding.hideAcceptance ();
	}
	
	this._cursor.hideAcceptance ();
	BindingAcceptor.acceptingBinding = null;
}
 
/**
 * Stop accepting binding.
 */
BindingAcceptor.prototype._stopAccepting = function () { 
	
	if ( this._binding.hideGeneralAcceptance ) {
 		this._binding.hideGeneralAcceptance ();
 	}
 	if ( this._binding.hideAcceptance ) {
		this._binding.hideAcceptance ();
	}
}

/**
 * Implements DOM2 EventListener.
 * @param {MouseEvent} e 
 */
BindingAcceptor.prototype.handleEvent = function ( e ) {

	switch ( e.type ) {
	
		case DOMEvents.MOUSEENTER :
		case DOMEvents.MOUSEOVER :
			if ( this._isAccepting ) {
				if ( BindingAcceptor.acceptingBinding != this._binding ) {
					BindingAcceptor.acceptingBinding = this._binding;
					this._cursor.showAcceptance ();
					if ( Types.isFunction ( this._binding.showAcceptance )) {
						this._binding.showAcceptance ();
					}
				}
			} else {
				EventBroadcaster.broadcast ( BroadcastMessages.TYPEDRAG_PAUSE );
				DOMEvents.stopPropagation ( e );
			}
			break;
			
		case DOMEvents.MOUSELEAVE :
		case DOMEvents.MOUSEOUT :
			if ( this._isAccepting ) {
				BindingAcceptor.acceptingBinding = null;
				this._cursor.hideAcceptance ();
				if ( Types.isFunction ( this._binding.hideAcceptance )) {
					this._binding.hideAcceptance ();
				}
			} else {
				DOMEvents.stopPropagation ( e );
			}
			break;
	}
	
	DOMEvents.stopPropagation ( e );
}

/**
 * Dispose (on binding dispose).
 */
BindingAcceptor.prototype.dispose = function () {
	
	EventBroadcaster.unsubscribe ( BroadcastMessages.TYPEDRAG_START, this );
	EventBroadcaster.unsubscribe ( BroadcastMessages.TYPEDRAG_STOP, this );
}