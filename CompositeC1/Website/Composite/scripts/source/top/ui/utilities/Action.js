/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

/**
 * To avoid spelling mistakes, always use predefined constants  
 * (eg. TreeNodeBinding.ONFOCUS) when specifying the actions
 * "type" parameter. This method doublechecks that the predefined 
 * constant is actually predefined.
 * @param {string} type
 */
Action.isValid = function ( type ) {

	return typeof type != Types.UNDEFINED;
}

/**
 * @class
 * @param {Binding} target
 * @param {string} type
 */
function Action ( target, type ) {
	
	/** 
	 * @type {Binding} 
	 */
	this.target	= target;
	
	/** 
	 * @type {string} 
	 */
	this.type = type;
	
	/** 
	 * @type {Binding} 
	 */
	this.listener = null;
	
	/** 
	 * @type {boolean} 
	 */
	this.isConsumed = false;
	
	/** 
	 * @type {boolean} 
	 */
	this.isCancelled = false;
}

/**
 * A Binding can call this method to prevent ancestor 
 * Bindings from dealing with the Action.
 */
Action.prototype.consume = function () {

	this.isConsumed = true;
}

/**
 * A Binding can call this method to cancel the action associated to 
 * an event dispatch. But only if the dispatcher handles this scenario! 
 * Actually it's just a flag, you decide what for. 
 */
Action.prototype.cancel = function () {

	this.isCancelled = true;
}