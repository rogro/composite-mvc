/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

Crawler.prototype = new BindingCrawler;
Crawler.prototype.constructor = Crawler;
Crawler.superclass = BindingCrawler.prototype;

/**
 * @class
 * The Crawler will climb all binding elements and 
 * invoke a method on the associated Binding.
 */
function Crawler () {
	
	/**
	 * The binding may recognize the intent of 
	 * the crawler by addressing the id property.
	 * @type {string}
	 */ 
	this.id = null;

	/**
	 * The binding may control the bindings behavior - eg skip, 
	 * stop or skip children - by modifying this property. We 
	 * do it like this because a response type on the method 
	 * <code>handleCrawler</code> would surely get lost in 
	 * method overloading...
	 * @type {string}
	 */
	this.response = null;
	
	/*
	 * Construct.
	 */
	this._construct ();
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Fires the method <code>handleCrawler</code> an any waiting binding. 
 * @overloads {BindingCrawler#_construct} 
 */
Crawler.prototype._construct = function () {
	
	Crawler.superclass._construct.call ( this );
	
	this.response = null;

	var self = this;
	this.addFilter ( function ( element, arg ) {
		var result = null;
		var binding = UserInterface.getBinding ( element );
		if ( Interfaces.isImplemented ( ICrawlerHandler, binding ) == true ) {
			self.response = null;
			binding.handleCrawler ( self );
			result = self.response;		
		}
		return result;
	});
}