/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

RadioButtonBinding.prototype = new ButtonBinding;
RadioButtonBinding.prototype.constructor = RadioButtonBinding;
RadioButtonBinding.superclass = ButtonBinding.prototype;

RadioButtonBinding.IMG_DEFAULT 		= "${skin}/buttons/radiobutton-default.png";
RadioButtonBinding.IMG_HOVER 		= "${skin}/buttons/radiobutton-hover.png";
RadioButtonBinding.IMG_ACTIVE 		= "${skin}/buttons/radiobutton-active.png";
RadioButtonBinding.IMG_DISABLED 	= "${skin}/buttons/radiobutton-disabled.png";

/**
 * @class
 */
function RadioButtonBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "RadioButtonBinding" );
	
	/**
	 * @overwrites {ButtonBinding#isRadioButton}
	 * @type {boolean}
	 */
	this.isRadioButton = true;
	
	/**
	 * @overwrites {MatrixBinding#hasMatrix}
	 * @type {boolean}
	 */
	this.hasMatrix = false;
	
	/**
	 * @type {ImageProfile}
	 */
	this.imageProfile = new ImageProfile ({
		image			: RadioButtonBinding.IMG_DEFAULT,
		imageHover 		: RadioButtonBinding.IMG_HOVER,
		imageActive		: RadioButtonBinding.IMG_ACTIVE,
		imageDisabled	: RadioButtonBinding.IMG_DISABLED 
	});
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
RadioButtonBinding.prototype.toString = function () {

	return "[RadioButtonBinding]";
}

/**
 * RadioButtonBinding factory.
 * @param {DOMDocument} ownerDocument
 * @return {RadioButtonBinding}
 */
RadioButtonBinding.newInstance = function ( ownerDocument ) {

	var element = DOMUtil.createElementNS ( Constants.NS_UI, "ui:radiobutton", ownerDocument );
	return UserInterface.registerBinding ( element, RadioButtonBinding );
}