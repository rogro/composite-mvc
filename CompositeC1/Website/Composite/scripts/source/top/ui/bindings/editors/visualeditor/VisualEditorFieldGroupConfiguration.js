/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

/**
 * Considered internal to this class.
 * @type {Map<string><VisualEditorFieldGroupConfiguration>}
 */
VisualEditorFieldGroupConfiguration._configurations = new Map ();

/**
 * Caching configurations to 
 * save a few server requests.
 * @param {string} classconfig
 * @return {VisualEditorFieldGroupConfiguration}
 */
VisualEditorFieldGroupConfiguration.getConfiguration = function ( fieldsconfig ) {
	
	var result = null;
	var configs = VisualEditorFieldGroupConfiguration._configurations;
	if ( !configs.has ( fieldsconfig )) {
		configs.set ( fieldsconfig, new VisualEditorFieldGroupConfiguration (
			EditorConfigurationService.GetEmbedableFieldGroupConfigurations ( fieldsconfig )
		));
	};
	return configs.get ( fieldsconfig );
}


/**
 * VisualEditor field config.
 * @param {object} object Provided by SOAP
 */
function VisualEditorFieldGroupConfiguration ( object ) {
	
	var groups = new Map ();
	new List ( object ).each ( function ( group ) {
		var map = new Map ();
		new List ( group.Fields ).each ( function ( field ) {
			map.set ( field.Name, {
				xhtml : field.XhtmlRepresentation,
				xml	: field.XhtmlRepresentation
			});
		});
		groups.set ( group.GroupName, map );
	});
	
	/**
	 * @type {Map<string><Map<string><object>>}
	 */
	this._groups = groups;
}

/**
 * Get field names for a group.
 * @return {List<string>}
 */
VisualEditorFieldGroupConfiguration.prototype.getGroupNames = function () {
	
	return this._groups.toList ( true );
}

/**
 * Get field names for a group.
 * @param {string} groupname
 * @return {List<string>}
 */
VisualEditorFieldGroupConfiguration.prototype.getFieldNames = function ( groupname ) {
	
	return this._groups.get ( groupname ).toList ( true );
}

/**
 * Get the VisualEditor markup for a given field.
 * @param {string} groupname
 * @param {string} fieldname
 * @return {string}
 */
VisualEditorFieldGroupConfiguration.prototype.getTinyMarkup = function ( groupname, fieldname ) {
	
	return this._groups.get ( groupname ).get ( fieldname ).xhtml;
}

/**
 * Get the sourcodeeditor markup for a given field.
 * @param {string} groupname
 * @param {string} fieldname
 * @return {string}
 */
VisualEditorFieldGroupConfiguration.prototype.getStructuredMarkup = function ( name ) {

	return this._groups.get ( groupname ).get ( fieldname ).xml;
}