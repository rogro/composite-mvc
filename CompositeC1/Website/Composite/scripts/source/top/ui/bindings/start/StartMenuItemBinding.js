/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

StartMenuItemBinding.prototype = new MenuItemBinding;
StartMenuItemBinding.prototype.constructor = StartMenuItemBinding;
StartMenuItemBinding.superclass = MenuItemBinding.prototype;

/**
 * @class
 */
function StartMenuItemBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "StartMenuItemBinding" );
	
	/**
	 * @overwrites {MenuItemBinding#type}
	 * @type {string}
	 */
	this.type = MenuItemBinding.TYPE_CHECKBOX;
}

/**
 * Identifies binding.
 */
StartMenuItemBinding.prototype.toString = function () {
	
	return "[StartMenuItemBinding]";
}

/**
 * @overloads {MenuItemBinding#onBindingRegister}
 */
StartMenuItemBinding.prototype.onBindingRegister = function () {
	
	StartMenuItemBinding.superclass.onBindingRegister.call ( this );
	this.subscribe ( BroadcastMessages.COMPOSITE_START );
	this.subscribe ( BroadcastMessages.COMPOSITE_STOP );
}

/**
 * @implements {IBroadcastListener}
 * @param {string} broadcast
 * @param {object} arg
 */
StartMenuItemBinding.prototype.handleBroadcast = function ( broadcast, arg ) {
	
	StartMenuItemBinding.superclass.handleBroadcast.call ( this, broadcast, arg );
	
	switch ( broadcast ) {
		case BroadcastMessages.COMPOSITE_START :
			if ( !this.isChecked ) {
				this.check ( true );
			}
		 	break;
		case BroadcastMessages.COMPOSITE_STOP :
			if ( this.isChecked ) {
				this.uncheck ( true );
			}
			break;
	}
}

/**
 * @overloads {MenuItemBinding#setChecked}
 * @param {boolean} isChecked
 * @param {boolean} isPreventCommand
 */
StartMenuItemBinding.prototype.setChecked = function ( isChecked, isPreventCommand ) {
	
	StartMenuItemBinding.superclass.setChecked.call ( 
		this, 
		isChecked, 
		isPreventCommand 
	);
	
	if ( !isPreventCommand ) {
		if ( this.isChecked ) {
			EventBroadcaster.broadcast ( BroadcastMessages.START_COMPOSITE );
		} else {
			EventBroadcaster.broadcast ( BroadcastMessages.STOP_COMPOSITE );
		}
	}
}


/**
* StartMenuItemBinding factory.
* @param {DOMDocument} ownerDocument
* @return {StartMenuItemBinding}
*/
StartMenuItemBinding.newInstance = function (ownerDocument) {

	var element = DOMUtil.createElementNS(Constants.NS_UI, "ui:menuitem", ownerDocument);
	UserInterface.registerBinding(element, StartMenuItemBinding);
	return UserInterface.getBinding(element);
}