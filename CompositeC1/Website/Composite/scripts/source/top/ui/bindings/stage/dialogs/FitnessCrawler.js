/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

FitnessCrawler.prototype = new Crawler;
FitnessCrawler.prototype.constructor = FitnessCrawler;
FitnessCrawler.superclass = Crawler.prototype;

FitnessCrawler.ID = "fitnesscrawler";
FitnessCrawler.MODE_BRUTAL = "brutal fitness";
FitnessCrawler.MODE_TRAINING = "train fitness";

/**
 * This crawler handles focus and blur.
 * @class
 */
function FitnessCrawler () {
	 
	this.id = FitnessCrawler.ID;
	this.mode = FitnessCrawler.MODE_TRAINING; 
	this._construct ();
	return this;
}

/**
 * @overloads {Crawler#_construct} 
 */
FitnessCrawler.prototype._construct = function () {
	
	FitnessCrawler.superclass._construct.call ( this );
	
	this.addFilter ( function ( element, list ) {
		
		var result = null;
		var binding = UserInterface.getBinding ( element );
		
		if ( !binding.isVisible ) {
			result = NodeCrawler.SKIP_NODE + NodeCrawler.SKIP_CHILDREN; 
		}
		
		return result;
	});
	
	/*
	 * Collecting unfit members.
	 */
	this.addFilter ( function ( element, list ) {
		
		var result = null;
		var binding = UserInterface.getBinding ( element );
		
		if ( binding.isAttached ) {
			if ( Interfaces.isImplemented ( IFit, binding )) {
				if ( !binding.isFit || this.mode == FitnessCrawler.MODE_BRUTAL ) {
					list.add ( binding );
				}
			}
		}
		return null;
	});
};