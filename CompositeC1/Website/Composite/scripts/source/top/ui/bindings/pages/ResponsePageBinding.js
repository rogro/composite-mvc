/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

ResponsePageBinding.prototype = new DialogPageBinding;
ResponsePageBinding.prototype.constructor = ResponsePageBinding;
ResponsePageBinding.superclass = DialogPageBinding.prototype;

/**
* @class
*/
function ResponsePageBinding() {

	/**
	* @type {SystemLogger}
	*/
	this.logger = SystemLogger.getLogger("ResponsePageBinding");
	
	/**
	 * @type {string}
	 */
	this.responseid = null;
}

/**
* Identifies binding.
*/
ResponsePageBinding.prototype.toString = function () {

	return "[ResponsePageBinding]";
};


/**
 * Parse DOM properties.
 */
ResponsePageBinding.prototype.parseDOMProperties = function() {

	ResponsePageBinding.superclass.parseDOMProperties.call(this);
	
	var responseid = this.getProperty("responseid");
	this.responseid = responseid;
}

/**
* @overloads {DialogPageBinding#onBindingAttach}
*/
ResponsePageBinding.prototype.onBindingAttach = function () {

	ResponsePageBinding.superclass.onBindingAttach.call(this);

	this.addActionListener(ResponseBinding.ACTION_SUCCESS);
	this.addActionListener(ResponseBinding.ACTION_FAILURE);
	this.addActionListener(ResponseBinding.ACTION_OOOOKAY);
};


/**
* @overloads {DialogPageBinding#handleAction}
* @param {Action} action
*/
ResponsePageBinding.prototype.handleAction = function (action) {

	ResponsePageBinding.superclass.handleAction.call(this, action);

	switch (action.type) {

		case ResponseBinding.ACTION_SUCCESS:
			this.onDialogAccept();
			break;

		case ResponseBinding.ACTION_FAILURE:
			// server action expected!
			break;
	}
};

/**
* @overloads {DialogPageBinding#onBindingAccept}
*/
ResponsePageBinding.prototype.onDialogAccept = function () {

	this.response = Dialog.RESPONSE_ACCEPT;
	if (this.responseid && this.bindingDocument.getElementById(this.responseid)) {
		this.result = this.bindingDocument.getElementById(this.responseid).value;
	}

	ResponsePageBinding.superclass.onDialogAccept.call(this);
};