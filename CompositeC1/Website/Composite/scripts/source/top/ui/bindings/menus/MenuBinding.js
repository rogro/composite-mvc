/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

MenuBinding.prototype = new MenuContainerBinding;
MenuBinding.prototype.constructor = MenuBinding;
MenuBinding.superclass = MenuContainerBinding.prototype;

function MenuBinding () {
	
	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "MenuBinding" );
	
	/**
	 * @type {LabelBinding}
	 */
	this.labelBinding = null;
	
	/**
	 * @type {boolean}
	 */
	this.isFocused = false;
}

/**
 * Identifies binding.
 */
MenuBinding.prototype.toString = function () {

	return "[MenuBinding]";
}

/**
 * @overloads {Binding#onBindingAttach}
 */
MenuBinding.prototype.onBindingAttach = function () {

	MenuBinding.superclass.onBindingAttach.call ( this );
	this.buildDOMContent ();
	this.assignDOMEvents ();
}

/**
 * Build DOM content.
 */
MenuBinding.prototype.buildDOMContent = function () {

	var image 		= this.getProperty ( "image" );
	var label 		= this.getProperty ( "label" );
	var tooltip 	= this.getProperty ( "tooltip" );

	this.labelBinding = LabelBinding.newInstance ( this.bindingDocument );
	this.labelBinding.attachClassName ( "menulabel" );
	this.add ( this.labelBinding );

	if ( label ) {
		this.setLabel ( label );
	}
	if ( image ) {
		this.setImage ( image );
	}
	if ( tooltip ) {
		this.setToolTip ( tooltip );
	}
}

/**
 * Reset visual appearance when menus are closed. 
 */
MenuBinding.prototype.reset = function () {
	
	this.detachClassName ( "hover" );
}

/**
 * Set image.
 * @param {string} url
 */
MenuBinding.prototype.setImage = function ( url ) {

	this.setProperty ( "image", url );
	if ( this.isAttached ) {
		this.labelBinding.setImage ( 
			Resolver.resolve ( url )
		);
	}
}

/**
 * Set label.
 * @param {string} label
 */
MenuBinding.prototype.setLabel = function ( label ) {

	this.setProperty ( "label", label );
	if ( this.isAttached ) {
		this.labelBinding.setLabel ( 
			Resolver.resolve ( label )
		);
	}
}

/**
 * Set tooltip.
 * @param {string} tooltip
 */
MenuBinding.prototype.setToolTip = function ( tooltip ) {

	this.setProperty ( "tooltip", tooltip );
	if ( this.isAttached ) {
		this.labelBinding.setToolTip ( 
			Resolver.resolve ( tooltip )
		);
	}
}

/**
 * Get image.
 * @return {string}
 */
MenuBinding.prototype.getImage = function () {

	return this.getProperty ( "image" );
}

/**
 * Get label.
 * @return {string}
 */
MenuBinding.prototype.getLabel = function () {

	return this.getProperty ( "label" );
}


/**
 * Get tooltip.
 * @return {string}
 */
MenuBinding.prototype.getToolTip = function () {

	return this.getProperty ( "tooltip" );
}

/**
 * Assigning DOM events.
 */
MenuBinding.prototype.assignDOMEvents = function () {
	
	this.addEventListener ( DOMEvents.MOUSEDOWN );
	this.addEventListener ( DOMEvents.MOUSEOVER );
	this.addEventListener ( DOMEvents.MOUSEOUT );
	this.addEventListener ( DOMEvents.MOUSEUP );
}

/**
 * @implements {IEventListener}
 * @overloads {Binding#handleEvent}
 * @param {MouseEvent} e
 */
MenuBinding.prototype.handleEvent = function ( e ) {
	
	MenuBinding.superclass.handleEvent.call ( this, e );
	
	var container = this.getMenuContainerBinding ();
	
	if ( !BindingDragger.isDragging ) {
	
		switch ( e.type ) {
			
			case DOMEvents.MOUSEDOWN :
				if ( container.isOpen ( this )) {
					DOMEvents.stopPropagation ( e );
				}
				break;
				
			case DOMEvents.MOUSEOVER :
				if ( container.isOpen () && !container.isOpen ( this )) {
					this.show ();
					this.menuPopupBinding.grabKeyboard ();
				}
				this.attachClassName ( "hover" );
				this.isFocused = true;
				break;
				
			case DOMEvents.MOUSEOUT :
				if ( !container.isOpen ()) {
					this.hide ();
				}
				this.isFocused = false;
				break;
				
			case DOMEvents.MOUSEUP :
				if ( !container.isOpen ( this )) {
					this.show ();
					this.menuPopupBinding.grabKeyboard ();
				}
				DOMEvents.stopPropagation ( e );
				break;
		}
	}
	
	/*
	 * Consuming the event!
	 */
	DOMEvents.stopPropagation ( e );
}
