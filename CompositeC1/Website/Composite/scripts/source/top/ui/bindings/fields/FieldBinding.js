/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

FieldBinding.prototype = new Binding;
FieldBinding.prototype.constructor = FieldBinding;
FieldBinding.superclass = Binding.prototype;

/**
 * @class
 */
function FieldBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "FieldBinding" );
	
	/**
	 * @type {string}
	 */
	this.bindingRelation = null;
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
FieldBinding.prototype.toString = function () {

	return "[FieldBinding]";
}

/**
 * @overloads {Binding#onBindingRegister}
 */
FieldBinding.prototype.onBindingRegister = function () {
	
	FieldBinding.superclass.onBindingRegister.call ( this );
	this.attachClassName ( Binding.CLASSNAME_CLEARFLOAT );
	
	var relation = this.getProperty ( "relation" );
	if ( relation != null ) {
		this.bindingRelation = relation;
		this.subscribe ( BroadcastMessages.BINDING_RELATE );
		this.hide ();
	}
}

/**
 * @implements {IBroadcastListener}
 * @param {string} broadcast
 * @param {object} arg
 */
FieldBinding.prototype.handleBroadcast = function ( broadcast, arg ) {
	
	FieldBinding.superclass.handleBroadcast.call ( this, broadcast, arg );
	
	switch ( broadcast ) {
		case BroadcastMessages.BINDING_RELATE :
			if ( arg.relate == this.bindingRelation && arg.origin == this.bindingDocument ) {
				if ( arg.result == true ) {
					if ( !this.isVisible ) {
						this.show ();
						this.dispatchAction ( Binding.ACTION_UPDATED );
					}
				} else {
					if ( this.isVisible ) {
						this.hide ();
						this.dispatchAction ( Binding.ACTION_UPDATED );
					}
				}
			}
			break;
	}
}

/**
 * FieldBinding factory.
 * @param {DOMDocument} ownerDocument
 * @return {FieldBinding}
 */
FieldBinding.newInstance = function ( ownerDocument ) {

	var element = DOMUtil.createElementNS ( Constants.NS_UI, "ui:field", ownerDocument );
	return UserInterface.registerBinding ( element, FieldBinding );
}                                                          