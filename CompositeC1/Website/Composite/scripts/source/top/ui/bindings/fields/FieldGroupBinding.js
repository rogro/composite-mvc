/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

FieldGroupBinding.prototype = new Binding;
FieldGroupBinding.prototype.constructor = FieldGroupBinding;
FieldGroupBinding.superclass = Binding.prototype;

FieldGroupBinding.ACTION_HIDE = "fieldgrouphide";
FieldGroupBinding.CLASSNAME_NOLABEL = "nolabel";
FieldGroupBinding.CLASSNAME_FIRST = "first"; // attached by FieldsBinding!

/**
 * @class
 */
function FieldGroupBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "FieldGroupBinding" );
}

/**
 * Identifies binding.
 */
FieldGroupBinding.prototype.toString = function () {

	return "[FieldGroupBinding]";
}

/**
 * Notice that we need to do this on register already!
 * @overloads {Binding#onBindingRegister}
 */
FieldGroupBinding.prototype.onBindingRegister = function () {

	FieldGroupBinding.superclass.onBindingRegister.call ( this );
	this.propertyMethodMap [ "label" ] = this.setLabel;
	this._buildDOMContent ();
}

/**
 * Build DOM content.
 */
FieldGroupBinding.prototype._buildDOMContent = function () {

	var label = this.getProperty ( "label" );
	if ( label ) {
		this.setLabel ( label );
	} else {
		this.attachClassName ( FieldGroupBinding.CLASSNAME_NOLABEL );
	}
}

/**
 * Set label.
 * @parm {string} label
 */
FieldGroupBinding.prototype.setLabel = function ( label ) {
	
	this.setProperty ( "label", label );
	
	if ( this.shadowTree.labelBinding == null ) {
	
		var labelBinding = LabelBinding.newInstance ( this.bindingDocument );
		var cell = this.shadowTree [ FieldGroupBinding.NORTH ];
		labelBinding.attachClassName ( "fieldgrouplabel" );
		this.bindingElement.insertBefore(labelBinding.bindingElement, this.bindingElement.firstChild);
		labelBinding.attach ();
		this.shadowTree.labelBinding = labelBinding;

		this.shadowTree.labelBinding.bindingElement.appendChild(DOMUtil.createElementNS(Constants.NS_XHTML, "div", this.bindingDocument));
	}
	
	this.shadowTree.labelBinding.setLabel ( 
		Resolver.resolve ( label )
	);
}

/** 
 * Get label.
 * @return {string}
 */
FieldGroupBinding.prototype.getLabel = function () {
	
	return this.getProperty ( "label" );
}