/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

DataInputButtonBinding.prototype = new DataInputBinding;
DataInputButtonBinding.prototype.constructor = DataInputButtonBinding;
DataInputButtonBinding.superclass = DataInputBinding.prototype;

/**
* @class
* @implements {IData}
*/
function DataInputButtonBinding() {

	/**
	* @type {SystemLogger}
	*/
	this.logger = SystemLogger.getLogger("DataInputButtonBinding");

	/**
	* @type {ToolBarButtonBinding}
	*/
	this._dialogButtonBinding = null;
}

/**
* Identifies binding.
*/
DataInputButtonBinding.prototype.toString = function () {

	return "[DataInputButtonBinding]";
}

/**
* @overloads {DataInputBinding#onBindingAttach}
*/
DataInputButtonBinding.prototype.onBindingAttach = function () {
	DataInputButtonBinding.superclass.onBindingAttach.call(this);

	if (this.hasCallBackID()) {
		Binding.dotnetify(this);
	}

}

/**
* Build button, build popup and populate by selection elements.
* @overloads {DataInputBinding#_buildDOMContent}
*/
DataInputButtonBinding.prototype._buildDOMContent = function () {

	DataInputSelectorBinding.superclass._buildDOMContent.call(this);
	this.buildButton();
}

/**
* Build button.
*/
DataInputButtonBinding.prototype.buildButton = function () {

	var button = ToolBarButtonBinding.newInstance(this.bindingDocument);
	var image = this.getProperty("image");
	if (image != null) {
		button.setImage(image);
	} else {
		button.setImage("${icon:popup}");
	}
	this.addFirst(button);
	button.attach();

	var self = this;

	button.oncommand = function () {
		self.dispatchAction(PageBinding.ACTION_DOPOSTBACK);
	}

	this._dialogButtonBinding = button;
};

/**
* Invoke dialog programatically.
*/
DataInputButtonBinding.prototype.oncommand = function () {

	var button = this._dialogButtonBinding;
	if (button != null) {
		button.oncommand();
	}
};
