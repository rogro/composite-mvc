/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

LocalStoreWindowBinding.prototype= new WindowBinding;
LocalStoreWindowBinding.prototype.constructor = LocalStoreWindowBinding;
LocalStoreWindowBinding.superclass = WindowBinding.prototype;
LocalStoreWindowBinding.URL = "${root}/content/misc/localstore/localstore.aspx";

/**
 * @class
 */
function LocalStoreWindowBinding () {
	
	/**
	 * @overloads {FlexBoxBinding#isFlexible}
	 * @type {boolean}
	 */
	this.isFlexible = false;
}

/**
 * Identifies binding.
 * @return {string}
 */
LocalStoreWindowBinding.prototype.toString = function () {
	
	return "[LocalStoreWindowBinding]";
}

/**
 * Load the localstore only if client has the correct Flash version installed.
 * @overloads {WindowBinding#onBindingRegister}
 */
LocalStoreWindowBinding.prototype.onBindingRegister = function () {
	
	LocalStoreWindowBinding.superclass.onBindingRegister.call ( this );
	if ( Client.hasFlash ) {
		this.setURL ( LocalStoreWindowBinding.URL );
	}
}