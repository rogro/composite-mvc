/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

FocusCrawler.prototype = new Crawler;
FocusCrawler.prototype.constructor = FocusCrawler;
FocusCrawler.superclass = Crawler.prototype;

FocusCrawler.ID = "focuscrawler";

FocusCrawler.MODE_INDEX = "index";
FocusCrawler.MODE_FOCUS = "focus";
FocusCrawler.MODE_BLUR = "blur";

/**
 * This crawler handles focus and blur.
 * @class
 */
function FocusCrawler () {
	 
	this.id = FocusCrawler.ID;
	this._construct ();
	return this;
}

/**
 * @overloads {Crawler#_construct} 
 */
FocusCrawler.prototype._construct = function () {
	
	FocusCrawler.superclass._construct.call ( this );
	
	this.addFilter ( function ( element, list ) {
		
		var result = null;
		var binding = UserInterface.getBinding ( element );
		
		if ( binding.isAttached == true ) {
			if ( Interfaces.isImplemented ( IFocusable, binding ) == true ) {
				if ( binding.isFocusable && binding.isVisible ) {
					switch ( this.mode ) {
						
						case FocusCrawler.MODE_INDEX :	
							list.add ( binding );
							break;
							
						case FocusCrawler.MODE_FOCUS :
							if ( !binding.isFocused ) {
								binding.focus ();
							}
							result = NodeCrawler.STOP_CRAWLING;
							break;
	
						case FocusCrawler.MODE_BLUR :
							if ( binding.isFocused == true ) {
								binding.blur ();
								result = NodeCrawler.STOP_CRAWLING;
							}
							break;
					}
				}
			}
		}
		//self._string += binding.toString () + ": " + result + "\n";
		
		return result;
	});
}

/*
FocusCrawler.prototype.onCrawlStart = function (){
	
	this._string = this.mode + "\n\n";
}

FocusCrawler.prototype.onCrawlStop = function (){
	
	this.logger.debug ( this._string );
}
*/