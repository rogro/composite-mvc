/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

CheckBoxGroupBinding.prototype = new Binding;
CheckBoxGroupBinding.prototype.constructor = CheckBoxGroupBinding;
CheckBoxGroupBinding.superclass = Binding.prototype;

/**
 * @class
 */
function CheckBoxGroupBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "CheckBoxGroupBinding" );
	
	/**
	 * @type {boolean}
	 */
	this.isRequired = false;
	
	/**
	 * @type {boolean}
	 */
	this.isValid = true;
}

/**
 * Identifies binding.
 */
CheckBoxGroupBinding.prototype.toString = function () {
	
	return "[CheckBoxGroupBinding]";
}

/**
 * Identifies binding.
 */
CheckBoxGroupBinding.prototype.onBindingAttach = function () {
	
	CheckBoxGroupBinding.superclass.onBindingAttach.call ( this );
	this.isRequired = this.getProperty ( "required" ) == true;
}

/**
 * @return {boolean}
 */
CheckBoxGroupBinding.prototype.validate = function () {
	
	var result = true;
	if ( this.isRequired ) {
		var checkboxes = this.getDescendantBindingsByLocalName ( "checkbox" );
		if ( checkboxes.hasEntries ()) {
			result = false;
			while ( checkboxes.hasNext () && !result ) {
				if ( checkboxes.getNext ().isChecked ) {
					result = true;
				}
			}
		}
		if ( result == false ) {
			this._showWarning ( true );
			this.dispatchAction ( Binding.ACTION_INVALID );
			this.addActionListener ( CheckBoxBinding.ACTION_COMMAND );
		}
	}
	return result;
}

/**
 * Show or hide warning.
 * @param {boolean} isShow
 */
CheckBoxGroupBinding.prototype._showWarning = function ( isShow ) {
	
	if ( isShow ) {
		if ( !this._labelBinding ) {
			var labelBinding = LabelBinding.newInstance ( this.bindingDocument );
			labelBinding.attachClassName ( "invalid" );
			labelBinding.setImage ( "${icon:error}" );
			labelBinding.setLabel ( "Selection required" );
			this._labelBinding = this.addFirst ( labelBinding );
			this._labelBinding.attach ();
		}
	} else if ( this._labelBinding ) {
		this._labelBinding.dispose ();
		this._labelBinding = null;
	}
}

/**
 * @implements {IActionListener}
 * @overloads {Binding#handleAction}
 * @param {Action} action
 */
CheckBoxGroupBinding.prototype.handleAction = function ( action ) {
	
	CheckBoxGroupBinding.superclass.handleAction.call ( this, action );
	
	switch ( action.type ) {
		case CheckBoxBinding.ACTION_COMMAND :
			this._showWarning ( false );
			this.dispatchAction ( Binding.ACTION_VALID );
			this.removeActionListener ( CheckBoxBinding.ACTION_COMMAND );
			break;
	}
}

/**
 * CheckBoxGroupBinding factory.
 * @param {DOMDocument} ownerDocument
 * @return {CheckBoxGroupBinding}
 */
CheckBoxGroupBinding.newInstance = function ( ownerDocument ) {

	var element = DOMUtil.createElementNS ( Constants.NS_UI, "ui:checkboxgroup", ownerDocument );
	return UserInterface.registerBinding ( element, CheckBoxGroupBinding );
}