/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

FieldHelpBinding.prototype = new Binding;
FieldHelpBinding.prototype.constructor = FieldHelpBinding;
FieldHelpBinding.superclass = Binding.prototype;

FieldHelpBinding.INDICATOR_IMAGE = "${skin}/fields/fieldhelpindicator.png";

/**
 * @class
 */
function FieldHelpBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "FieldHelpBinding" );
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
FieldHelpBinding.prototype.toString = function () {

	return "[FieldHelpBinding]";
}

/**
 * @overloads {Binding#onBindingAttach}
 */
FieldHelpBinding.prototype.onBindingAttach = function () {
	
	FieldHelpBinding.superclass.onBindingAttach.call ( this );
	
	this.buildPopupBinding ();
	this.buildPopupButton ();
}

/**
 * @overloads {Binding#onBindingDispose}
 */
FieldHelpBinding.prototype.onBindingDispose = function () {
	
	FieldHelpBinding.superclass.onBindingDispose.call ( this );
	
	var popup = this._fieldHelpPopupBinding;
	if ( popup ) {
		popup.dispose ();
	}
}

/**
 * Build popup.
 */
FieldHelpBinding.prototype.buildPopupBinding = function () {
	
	var popupSetBinding = app.bindingMap.fieldhelpopupset;
	var doc = popupSetBinding.bindingDocument;
	var popupBinding = popupSetBinding.add (
		PopupBinding.newInstance ( doc )
	);
	var bodyBinding = popupBinding.add (
		PopupBodyBinding.newInstance ( doc )
	);
	popupBinding.position = PopupBinding.POSITION_RIGHT;
	popupBinding.attachClassName ( "fieldhelppopup" );
	
	/*
	 * Help can be written inside the tag or supplied in the label attribute.
	 */
	if ( this.bindingElement.hasChildNodes ()) {
		bodyBinding.bindingElement.innerHTML = this.bindingElement.innerHTML;
	} else {
		var label = this.getProperty ( "label" );
		if ( label ) {
			bodyBinding.bindingElement.innerHTML = Resolver.resolve ( label );
		}
	}
	
	this.bindingElement.innerHTML = "";
	this._fieldHelpPopupBinding = popupBinding;
}

/**
 * Build DOM content.
 */
FieldHelpBinding.prototype.buildPopupButton = function () {

	var field = this.getAncestorBindingByLocalName ( "field" );
	
	if ( field ) {
	
		field.attachClassName ( "fieldhelp" );
		
		var button = ClickButtonBinding.newInstance ( this.bindingDocument );
		button.attachClassName ( "fieldhelp" ); 
	 	button.setImage ( FieldHelpBinding.INDICATOR_IMAGE );
	 	this.add ( button );
	 	button.attach ();
	 	
	 	var self = this;
	 	button.oncommand = function () {
			self.attachPopupBinding ();
		}
		
		button.setPopup ( this._fieldHelpPopupBinding );
	 	this._fieldHelpButton = button;
	}
}

/**
 * Attach popup. For faster page load time, the popup bindings 
 * get attached only when user handles the button.
 */
FieldHelpBinding.prototype.attachPopupBinding = function () {

	var popup = this._fieldHelpPopupBinding;
	if ( popup && !popup.isAttached ) {
		popup.attachRecursive ();
	}
}