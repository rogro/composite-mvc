/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

ControlGroupBinding.prototype = new Binding;
ControlGroupBinding.prototype.constructor = ControlGroupBinding;
ControlGroupBinding.superclass = Binding.prototype;

/**
 * @class
 */
function ControlGroupBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "ControlGroupBinding" );
	
	/**
	 * Block common crawlers.
	 * @type {Map<string><boolean>}
	 * @overwrites {Binding#crawlerFilters}
	 */
	this.crawlerFilters	= new List ([ FlexBoxCrawler.ID, FocusCrawler.ID ]);
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
ControlGroupBinding.prototype.toString = function () {

	return "[ControlGroupBinding]";
}

/**
 * Setup event listeners.
 */
ControlGroupBinding.prototype.onBindingAttach = function () {

	ControlGroupBinding.superclass.onBindingAttach.call ( this );
	this.assignDOMEvents ();
}

/**
 * Mouseevents should not be propagated to the titlebar when handling controls.
 * These listeners will take care of it.
 */
ControlGroupBinding.prototype.assignDOMEvents = function () {

	this.addEventListener ( DOMEvents.MOUSEDOWN );
	this.addEventListener ( DOMEvents.MOUSEUP );
}

/**
 * Activate. Forces a refresh on contained controls. This may 
 * update control image if containing ControlBoxBinding changed 
 * active state AND control isGhostable.
 */
ControlGroupBinding.prototype.onActivate = function () {
	
	var controls = this.getDescendantBindingsByLocalName ( "control" );
	controls.each ( function ( control ) {
		//if ( control.isGhostable ) {
			control.setControlType ( control.controlType );
		//}
	});
}

/**
 * Deactivate.
 */
ControlGroupBinding.prototype.onDeactivate = ControlGroupBinding.prototype.onActivate;

/**
 * Blocks event propagation.
 * @implements {IEventListener}
 * @overloads {Binding#handleEvent}
 * @param {MouseEvent} e
 */
ControlGroupBinding.prototype.handleEvent = function ( e ) {

	ControlGroupBinding.superclass.handleEvent.call ( this, e );
	
	DOMEvents.stopPropagation ( e );

	switch ( e.type ) {
		case DOMEvents.MOUSEDOWN :
			EventBroadcaster.broadcast ( BroadcastMessages.MOUSEEVENT_MOUSEDOWN, e );
			this.dispatchAction ( Binding.ACTION_ACTIVATED );
			break;
		case DOMEvents.MOUSEUP :
			EventBroadcaster.broadcast ( BroadcastMessages.MOUSEEVENT_MOUSEUP, e );
			break;
	}
}

/**
 * ControlGroupBinding factory.
 * @param {DOMDocument} ownerDocument
 * @return {ControlGroupBinding}
 */
ControlGroupBinding.newInstance = function ( ownerDocument ) {
	var element = DOMUtil.createElementNS ( Constants.NS_UI, "ui:controlgroup", ownerDocument );
	return UserInterface.registerBinding ( element, ControlGroupBinding );
}