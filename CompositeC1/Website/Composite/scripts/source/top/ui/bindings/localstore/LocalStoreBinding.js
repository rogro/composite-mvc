/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

LocalStoreBinding.prototype = new Binding;
LocalStoreBinding.prototype.constructor = LocalStoreBinding;
LocalStoreBinding.superclass = Binding.prototype;

LocalStoreBinding.STORE_PERSISTANCE = "localstorepersistance";

/**
 * Get local store.
 * @param id
 * @return {DOMDocument}
 */
LocalStoreBinding.getLocalStore = function ( id ) {
	
	
}

/**
 * @class
 */
function LocalStoreBinding () {
	
	/*
	 * Global pointer.
	 */
	LocalStore._bindingInstance = this;
	
	/*
	 * Returnable 
	 */
	return this;
}

/**
 * Identifies binding
 * @return {string}
 */
LocalStoreBinding.prototype.toString = function () {
	
	return "[LocalStoreBinding]";
}

/**
 * @param {string} id
 */
LocalStoreBinding.prototype.getLocalStore = function ( id ) {
	
}


LocalStoreBinding.prototype.onBindingRegister = function () {
	
	LocalStoreBinding.superclass.onBindingRegister.call ( this );
	
	if ( Client.isExplorer == true ) {
		
		/*
		this.bindingElement.setAttribute ( "fisse", "hej" );
		this.bindingElement.save ( "test" );
		this.bindingElement.removeAttribute ( "fisse" );
		*/
		
		this.bindingElement.load ( "persistance" );
		
		var doc = this.bindingElement.XMLDocument;
		
		/*
		var el = doc.createElement ( "FISTER" );
		doc.replaceChild ( el, doc.documentElement );
		*/
		
		alert ( DOMSerializer.serialize ( doc, true ));
		
		// this.bindingElement.save ( "persistance" );
		
		/*
		var doc = this.bindingElement.XMLDocument;
		doc.documentElement.removeChild ( doc.documentElement.firstChild );
		alert ( DOMSerializer.serialize ( doc, true ));
		
		if ( !doc.documentElement.hasChildNodes ()) {
			doc.documentElement.appendChild ( doc.createElement ( "Fisse" ));
			this.bindingElement.save ( "Fisterloegsovs" );
		}
		*/
		
	} else {
		
		var host = document.location.host;
		var storage = window.globalStorage;
		// var doc = Templates.getTemplateDocument ( "soapenvelope.xml" );
		//alert ( DOMSerializer.serialize ( doc, true ));
		/*
		try {
			storage [ host ].TEST = doc;
		} catch ( e ) {
			alert ( e );
		}
		*/
		//alert ( storage [ host ].TEST );
	}
}