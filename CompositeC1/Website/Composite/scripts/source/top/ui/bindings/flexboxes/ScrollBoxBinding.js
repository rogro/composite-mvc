/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

ScrollBoxBinding.prototype = new FlexBoxBinding;
ScrollBoxBinding.prototype.constructor = ScrollBoxBinding;
ScrollBoxBinding.superclass = FlexBoxBinding.prototype;

function ScrollBoxBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "ScrollBoxBinding" );
}

/**
 * Identifies binding.
 */
ScrollBoxBinding.prototype.toString = function () {

	return "[ScrollBoxBinding]";
}

/**
 * @overloads {MatrixBinding#onBindintRegister}
 */
ScrollBoxBinding.prototype.onBindingRegister = function () {
	
	ScrollBoxBinding.superclass.onBindingRegister.call ( this );
	this.addActionListener ( BalloonBinding.ACTION_INITIALIZE );
	// this._isFit = this.getProperty ( "fit" ) == true;
}

/**
 * Register as environment for balloons.
 * @implements {IActionListener}
 * @overloads {FlexBoxBinding#handleAction}
 * @param {Action} action
 */
ScrollBoxBinding.prototype.handleAction = function ( action ) {
	
	ScrollBoxBinding.superclass.handleAction.call ( this, action );
	
	/*
	 * Balloon tricks a go go.
	 */
	switch ( action.type ) {
		case BalloonBinding.ACTION_INITIALIZE :
			action.consume ();
			break;
	}
}

/**
 * Pathces a glitch where scrollboxes would otherwise resort to visible scrollbars. 
 * @param {int} height
 *
ScrollBoxBinding.prototype._setFitnessHeight = function ( height ) {
	
	/*
	 * TODO: Figure out where these extra pixels are coming from!
	 *
	ScrollBoxBinding.superclass._setFitnessHeight.call ( this, height + 5 );
}
*/

/**
 * Set scrollbox position.
 * @param {Point} point
 */
ScrollBoxBinding.prototype.setPosition = function ( point ) {
	
	this.bindingElement.scrollLeft = point.x;
	this.bindingElement.scrollTop = point.y;
}

/**
 * Get scrollbox position.
 * @return {Point}
 */
ScrollBoxBinding.prototype.getPosition = function () {
	
	return new Point (
		this.bindingElement.scrollLeft,
		this.bindingElement.scrollTop
	);	
}