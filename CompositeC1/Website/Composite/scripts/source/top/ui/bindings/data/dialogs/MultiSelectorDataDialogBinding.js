/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

MultiSelectorDataDialogBinding.prototype = new DataDialogBinding;
MultiSelectorDataDialogBinding.prototype.constructor = MultiSelectorDataDialogBinding;
MultiSelectorDataDialogBinding.superclass = DataDialogBinding.prototype;

MultiSelectorDataDialogBinding.ACTION_RESULT = "multiselectordatadialog result";

/**
 * @class
 * This is intended for use by the {@link MultiSelectorBinding} as 
 * an internal binding only. Don't use it for anything else.
 */
function MultiSelectorDataDialogBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "MultiSelectorDataDialogBinding" );
	
	/**
	 * Hardwired viewdefinition!
	 * @overwrites {DataDialogBinding#_dialogViewHandle}
	 * @type {string}
	 */
	this._dialogViewHandle = "Composite.Management.MultiSelectorDialog";
	
	/**
	 * @overwrites {DataBinding#isFocusable}
	 * @type {boolean}
	 */
	this.isFocusable = false;
	
	/**
	 * This property is set by the MultiSelectorBinding.
	 * @see {MultiSelectorBinding#_buildEditorButton}
	 * @type {List<SelectorBindingSelection>}
	 */
	this.selections = null;
	 
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
MultiSelectorDataDialogBinding.prototype.toString = function () {
	
	return "[MultiSelectorDataDialogBinding]";
}

/**
 * @overloads {StringDataDialogBinding#onBindingAttach}
 */
MultiSelectorDataDialogBinding.prototype.onBindingAttach = function () {
	
	this.setProperty ( "label", StringBundle.getString ( "ui", "Website.Misc.MultiSelector.LabelEditSelections" ) );
	MultiSelectorDataDialogBinding.superclass.onBindingAttach.call ( this );
}

/**
 * @overwrites {DataDialogBinding#fireCommand}
 */
MultiSelectorDataDialogBinding.prototype.fireCommand = function () {
	
	this.dispatchAction ( DataDialogBinding.ACTION_COMMAND );
	
	/*
	 * Build argument for selections editor.
	 */
	var argument = {
		label : DataBinding.getAssociatedLabel ( this ),
		selections : this.selections
	}
	
	/*
	 * Build dialog handler. Action intercepted 
	 * by hosting MultiSelecotorBinding.
	 */
	var self = this;
	var handler = {
		handleDialogResponse : function ( response, result ) {
			if ( response == Dialog.RESPONSE_ACCEPT ) {
				self.result = result;
				self.dispatchAction ( MultiSelectorDataDialogBinding.ACTION_RESULT );
			}
		}
	}
	
	/*
	 * Launch dialog.
	 */
	var definition = ViewDefinitions [ this._dialogViewHandle ];
	definition.handler = handler;
	definition.argument = argument;
	StageBinding.presentViewDefinition ( definition );
}

/**
 * MultiSelectorDataDialogBinding factory.
 * @param {DOMDocument} ownerDocument
 * @return {MultiSelectorDataDialogBinding}
 */
MultiSelectorDataDialogBinding.newInstance = function ( ownerDocument ) {

	var element = DOMUtil.createElementNS ( Constants.NS_UI, "ui:datadialog", ownerDocument );
	return UserInterface.registerBinding ( element, MultiSelectorDataDialogBinding );
}