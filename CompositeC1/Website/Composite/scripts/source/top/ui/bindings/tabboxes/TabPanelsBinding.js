/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

TabPanelsBinding.prototype = new FlexBoxBinding;
TabPanelsBinding.prototype.constructor = TabPanelsBinding;
TabPanelsBinding.superclass = FlexBoxBinding.prototype;

/**
 * @class
 */
function TabPanelsBinding () {
	
	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "TabPanelsBinding" );
	
	/**
	 * @type {TabBoxBinding}
	 */
	this.containingTabBoxBinding = null;
	
	/**
	 * Storing tabpanels dimensions in order to economize flex iterations.
	 * @see {TabPanelBinding#_invokeManagedRecursiveFlex}
	 * @type {Dimension}
	 */
	this._lastKnownDimension = null;

}

/**
 * Identifies binding.
 */
TabPanelsBinding.prototype.toString = function () {

	return "[TabPanelsBinding]";
}

/**
 * @overloads {FlexBoxBinding#onBindingRegister}
 */
TabPanelsBinding.prototype.onBindingRegister = function () {
	
	TabPanelsBinding.superclass.onBindingRegister.call ( this );
	this._lastKnownDimension = new Dimension ( 0, 0 );
}

/**
 * Returns true if dimensions changed since method was lastly invoked.
 * @see {TabPanelBinding#_invokeManagedRecursiveFlex}
 * @return {boolean}
 */
TabPanelsBinding.prototype.hasDimensionsChanged = function () {
	
	var result = false;
	var dim1 = this.boxObject.getDimension ();
	var dim2 = this._lastKnownDimension;
	
	if ( dim2 == null || !Dimension.isEqual ( dim1, dim2 )) {
		result = true;
		this._lastKnownDimension = dim1;
	}
	
	return result;
}

/**
 * @overloads {Binding#onBindingAttach}
 */
TabPanelsBinding.prototype.onBindingAttach = function () {

	TabPanelsBinding.superclass.onBindingAttach.call ( this );
	this.containingTabBoxBinding = this.getAncestorBindingByType ( TabBoxBinding );
	this.setFlexibility ( this.containingTabBoxBinding.isFlexible );
	this.dispatchAction ( Binding.ACTION_ATTACHED );
}
