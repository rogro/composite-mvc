/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

CheckButtonImageProfile.IMG_DEFAULT 		= "${skin}/buttons/checkbutton-default.png";
CheckButtonImageProfile.IMG_HOVER 			= "${skin}/buttons/checkbutton-hover.png";
CheckButtonImageProfile.IMG_ACTIVE 			= "${skin}/buttons/checkbutton-active.png";
CheckButtonImageProfile.IMG_ACTIVE_HOVER 	= "${skin}/buttons/checkbutton-active-hover.png";
CheckButtonImageProfile.IMG_DISABLED 		= null;
CheckButtonImageProfile.IMG_DISABLED_ON 	= null;

/**
 * Checkbutton image profile.
 * @param {CheckButtonBinding} binding
 * @implements {IImageProfile}
 */
function CheckButtonImageProfile ( binding ) {
	
	this._binding = binding;
}

/**
 * Get default image.
 * @return {string}
 */
CheckButtonImageProfile.prototype.getDefaultImage = function () {

	return CheckButtonImageProfile.IMG_DEFAULT;
}

/**
 * Get hover image.
 * @return {string}
 */
CheckButtonImageProfile.prototype.getHoverImage = function () {

	return this._binding.isChecked ? 
		CheckButtonImageProfile.IMG_ACTIVE_HOVER :
		CheckButtonImageProfile.IMG_HOVER;
}

/**
 * Get active image.
 * @return {string}
 */
CheckButtonImageProfile.prototype.getActiveImage = function () {

	return CheckButtonImageProfile.IMG_ACTIVE;
}

/**
 * Get disabled image.
 * @return {string}
 */
CheckButtonImageProfile.prototype.getDisabledImage = function () {

	return this._binding.isChecked ? 
		CheckButtonImageProfile.IMG_DISABLED :
		CheckButtonImageProfile.IMG_DISABLED_ON;
}