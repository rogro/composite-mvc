/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

StageMenuBarBinding.prototype = new MenuBarBinding;
StageMenuBarBinding.prototype.constructor = StageMenuBarBinding;
StageMenuBarBinding.superclass = MenuBarBinding.prototype;

/**
 * @class
 */
function StageMenuBarBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "StageMenuBarBinding" );
	
	/**
	 * @type {SystemNode}
	 */
	this._rootNode = null;
}

/**
 * Identifies binding.
 */
StageMenuBarBinding.prototype.toString = function () {
	
	return "[StageMenuBarBinding]";
}

/**
 * Intercept for system-hooked menuitem commands.
 * @overloads {Binding#onBindingAttach}
 */
StageMenuBarBinding.prototype.onBindingAttach = function () {
	
	StageMenuBarBinding.superclass.onBindingAttach.call ( this );
	if ( System.hasActivePerspectives ) {
		this.addActionListener ( MenuItemBinding.ACTION_COMMAND );
	} else {
		Binding.prototype.hide.call ( this ); // this.hide () burned by MenuContainerBinding#hide
	}
}


/** 
 * Invoke system actions. These are hardwired to act on the root SystemNode.
 * @implements {IActionListener}
 * @overloads {MenuBarBinding#handleAction}
 * @param {Action} action
 */
StageMenuBarBinding.prototype.handleAction = function ( action ) {
	
	StageMenuBarBinding.superclass.handleAction.call ( this, action );
	
	switch ( action.type ) {
		case MenuItemBinding.ACTION_COMMAND :
			var systemAction = action.target.associatedSystemAction;
			if ( Application.isLoggedIn ) { // otherwise this will trigger when "Exit" is pressed.
				if ( !this._rootNode ) {
					this._rootNode = System.getRootNode ();
				}
				if ( systemAction ) {
					SystemAction.invoke ( systemAction, this._rootNode );
				}
			}
			action.consume ();
			break;
	}
}