/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

TreeCrawler.prototype = new BindingCrawler;
TreeCrawler.prototype.constructor = TreeCrawler;
TreeCrawler.superclass = BindingCrawler.prototype;

TreeCrawler.ID = "treecrawler";
TreeCrawler.MODE_GETOPEN = "get open treenodes";

/**
 * @class
 * The TreeCrawler sees only TreeNodeBindings.
 */
function TreeCrawler () {
	
	this.mode = TreeCrawler.MODE_GETOPEN;
	this.id = TreeCrawler.ID;
	this._construct ();
	return this;
}

/**
 * * Filter all but Binding elements.
 * @overloads {ElementCrawler#_construct} 
 */
TreeCrawler.prototype._construct = function () {
	
	TreeCrawler.superclass._construct.call ( this );
	
	var self = this;
	
	/*
	 * See only treenodes.
	 */
	this.addFilter ( function ( element ) {
		
		var binding = UserInterface.getBinding ( element );
		var result = null;var result = null;
		
		if ( !binding instanceof TreeNodeBinding ) {
			result = NodeCrawler.SKIP_NODE;
		}
		
		return result;
	});
	
	/*
	 * Analyze treenode. 
	 */
	this.addFilter ( function ( element, list ) {
		
		var binding = UserInterface.getBinding ( element );
		var result = null;
		
		switch ( self.mode ) {
			case TreeCrawler.MODE_GETOPEN :
				if ( binding.isOpen ) {
					list.add ( binding );
				}
				break;
		}
		return result;
	});
}