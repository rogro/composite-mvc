/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

ControlBoxBinding.prototype = new FlexBoxBinding;
ControlBoxBinding.prototype.constructor = ControlBoxBinding;
ControlBoxBinding.superclass = FlexBoxBinding.prototype;

ControlBoxBinding.STATE_NORMAL			= "normal";
ControlBoxBinding.STATE_MAXIMIZED 		= "maximized";
ControlBoxBinding.STATE_MINIMIZED 		= "minimized";
ControlBoxBinding.ACTION_NORMALIZE 		= "controlbox normalizeaction";
ControlBoxBinding.ACTION_MAXIMIZE 		= "controlbox maximizeaction";
ControlBoxBinding.ACTION_MINIMIZE 		= "controlbox minimizeaction";
ControlBoxBinding.ACTION_STATECHANGE	= "controlbox statechangeacton";

/**
 * @class
 */
function ControlBoxBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "ControlBoxBinding" );
	
	/**
	 * @type {boolean}
	 */
	this.isNormalized = true;
	
	/**
	 * @type {boolean}
	 */
	this.isMaximized = false;
	
	/**
	 * @type {boolean}
	 */
	this.isMinimized = false;
}

/**
 * Identifies binding.
 */
ControlBoxBinding.prototype.toString = function () {

	return "[ControlBoxBinding]";
}

/**
 * @overloads {FlexBoxBinding#onBindingRegister}
 */
ControlBoxBinding.prototype.onBindingAttach = function () {

	ControlBoxBinding.superclass.onBindingAttach.call ( this );
	this.addActionListener ( ControlBinding.ACTION_COMMAND, this );
	this.attachClassName ( ControlBoxBinding.STATE_NORMAL );
}

/**
 * @implements {IActionListener}
 * @overloads {FlexBoxBinding#handleAction}
 * @param {Action} action
 */
ControlBoxBinding.prototype.handleAction = function ( action ) {

	ControlBoxBinding.superclass.handleAction.call ( this, action );

	switch ( action.type ) {
		case ControlBinding.ACTION_COMMAND :
			var controlBinding = action.target;
			Application.lock ( this );
			var self = this;
			setTimeout ( function () {
				self.handleInvokedControl ( controlBinding );
				Application.unlock ( self );
			}, 0 );
			action.consume ();
			break;
	}
}

/**
 * Invoked when descendant controls fire.
 * @param {ControlBinding} control
 */
ControlBoxBinding.prototype.handleInvokedControl = function ( control ) {
	
	switch ( control.controlType ) {
		case ControlBinding.TYPE_MAXIMIZE :
			this.maximize ();
			break;
		case ControlBinding.TYPE_MINIMIZE :
			this.minimize ();
			break;
		case ControlBinding.TYPE_UNMAXIMIZE :
		case ControlBinding.TYPE_UNMINIMIZE :
			this.normalize ();
			break;
	}
}

/**
 * Minimize.
 */
ControlBoxBinding.prototype.maximize = function () {

	this.dispatchAction ( ControlBoxBinding.ACTION_MAXIMIZE );
	this.setState ( ControlBoxBinding.STATE_MAXIMIZED );
	this.isNormalized = false;
	this.isMaximized = true;
	this.isMinimized = false;
}

/**
 * Minimize.
 */
ControlBoxBinding.prototype.minimize = function () {

	this.dispatchAction ( ControlBoxBinding.ACTION_MINIMIZE );
	this.setState ( ControlBoxBinding.STATE_MINIMIZED );
	this.isNormalized = false;
	this.isMaximized = false;
	this.isMinimized = true;
}

/**
 * Normalize.
 */
ControlBoxBinding.prototype.normalize = function () {

	this.dispatchAction ( ControlBoxBinding.ACTION_NORMALIZE );
	this.setState ( ControlBoxBinding.STATE_NORMAL );
	this.isNormalized = true;
	this.isMaximized = false;
	this.isMinimized = false;
}

/**
 * Updates the value of the "state" property. This also sets a specific CSS classname.
 * @param {string} state
 */
ControlBoxBinding.prototype.setState = function ( state ) {
	
	// backup the old value
	var prestate = this.getState ();

	// assert the new value
	this.setProperty ( "state", state );
	
	// synchronize the CSS classname
	this.detachClassName ( prestate );
	this.attachClassName ( state );
	
	// dispatching common action for all state changes
	this.dispatchAction ( ControlBoxBinding.ACTION_STATECHANGE );
}

/**
 * @return {string}
 */
ControlBoxBinding.prototype.getState = function () {

	var result = this.getProperty ( "state" );
	if ( !result ) {
		result = ControlBoxBinding.STATE_NORMAL;
	}
	return result;
}