/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

StageStatusBarBinding.prototype = new ToolBarBinding;
StageStatusBarBinding.prototype.constructor = StageStatusBarBinding;
StageStatusBarBinding.superclass = ToolBarBinding.prototype;

/**
 * @class
 */
function StageStatusBarBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "StageStatusBarBinding" );
	
	/**
	 * @type {ToolBarLabelBinding}
	 */
	this._label = null;
}

/**
 * Identifies binding.
 */
StageStatusBarBinding.prototype.toString = function () {
	
	return "[StageStatusBarBinding]";
}

/**
 * Initialize top level StatusBar object when ready.
 * @overloads {Binding#onBindingInitialize} binding
 */
StageStatusBarBinding.prototype.onBindingInitialize = function () {
	
	this._label = this.bindingWindow.bindingMap.statusbarlabel;
	StatusBar.initialize ( this );
	StageStatusBarBinding.superclass.onBindingInitialize.call ( this );
}

/**
 * Set statusbar text.
 * @param {string} message
 */
StageStatusBarBinding.prototype.setLabel = function ( message ) {
	
	this._label.setLabel ( message );
}

/**
 * Set statusbar image.
 * @param {string} image
 */
StageStatusBarBinding.prototype.setImage = function ( image ) {
	
	this._label.setImage ( image );
}

/**
 * Clear statusbar.
 * @param {string} image
 */
StageStatusBarBinding.prototype.clear = function () {
	
	this._label.setLabel ( null );
	this._label.setImage ( false );
}

/**
 * Fade out after a given amount of time. 
 * TODO: IMPLEMENT THIS!
 * @param {int} timeout
 */
StageStatusBarBinding.prototype.startFadeOut = function ( timeout ) {
	
	this.logger.debug ( "START FADEOUT" );
}