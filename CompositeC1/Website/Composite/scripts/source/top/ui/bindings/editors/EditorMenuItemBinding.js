/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

EditorMenuItemBinding.prototype = new MenuItemBinding;
EditorMenuItemBinding.prototype.constructor = EditorMenuItemBinding;
EditorMenuItemBinding.superclass = MenuItemBinding.prototype;

/**
 * @class
 * @deprecated
 * @implements {IEditorControlBinding}
 */
function EditorMenuItemBinding () {

	/** 
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "EditorMenuItemBinding" );
	
	/**
	 * Indicates that editors should not blur 
	 * the toolbars when binding is handled.
	 * @implements {IEditorControlBinding}
	 * @type {boolean}
	 */
	this.isEditorControlBinding = true;
}

/**
 * Identifies binding.
 */
EditorMenuItemBinding.prototype.toString = function () {
	
	return "[EditorMenuItemBinding]";
}

/**
 * @overloads {EditorMenuItemBinding#buildDOMContent}
 */
EditorMenuItemBinding.prototype.buildDOMContent = function () {
	
	EditorMenuItemBinding.superclass.buildDOMContent.call ( this );
	
	if (Client.isExplorer || Client.isExplorer11) {
		this._buildDesignModeSanitizer ();
	}
}

/**
 * Places an IMG element on top of all other elements. This feature is 
 * disabled for Mozilla because it makes the button draggable; it's not 
 * needed in Mozilla anyway.
 */
EditorMenuItemBinding.prototype._buildDesignModeSanitizer = function () {
	
	if ( Client.isExplorer || Client.isExplorer11) {
		var img = this.bindingDocument.createElement ( "img" );
		img.className = "designmodesanitizer";
		img.src = Resolver.resolve("${root}/images/blank.png");
		img.ondragstart = function (e) { e.preventDefault(); }
		this.shadowTree.designmodesanitizer = img;
		this.bindingElement.appendChild ( img );
	}
}

/**
 * EditorMenuItemBinding factory.
 * @param {DOMDocument} ownerDocument
 * @return {EditorMenuItemBinding}
 */
EditorMenuItemBinding.newInstance = function ( ownerDocument ) {

	var element = DOMUtil.createElementNS ( Constants.NS_UI, "ui:menuitem", ownerDocument );
	return UserInterface.registerBinding ( element, EditorMenuItemBinding );
}