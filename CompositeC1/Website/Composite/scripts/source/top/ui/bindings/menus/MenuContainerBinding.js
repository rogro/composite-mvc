/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

MenuContainerBinding.prototype = new Binding;
MenuContainerBinding.prototype.constructor = MenuContainerBinding;
MenuContainerBinding.superclass = Binding.prototype;

/**
 * @implements {IMenuContainer}
 */
function MenuContainerBinding () {
	
	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "MenuContainerBinding" );
	
	/**
	 * @type {boolean}
	 * @private
	 */
	this._isOpen = false;
	
	/**
	 * @type {MenuBinding} 
	 * @private
	 */
	this._openElement = null;
	
	/**
	 * @type {MenuContainerBinding}
	 * @private
	 */
	this.menuContainerBinding = null;
	
	/**
	 * @type {MenuPopupBinding}
	 * @private
	 */
	this.menuPopupBinding = null;
}

/**
 * Identifies binding.
 */
MenuContainerBinding.prototype.toString = function () {

	return "[MenuContainerBinding]";
}

/**
 * Is open?
 * @param {Binding} object TODO: update documentation around here!
 * @return {boolean}
 */
MenuContainerBinding.prototype.isOpen = function ( object ) {

	var result = null;
	if ( !object ) {
		result = this._isOpen;
	} else {
		result = ( object == this._openElement );
	}
	return result;
}

/**
 * @param {Binding} binding Ehm, this could actually also be a boolean value of false...
 */
MenuContainerBinding.prototype.setOpenElement = function ( binding ) {
	
	if ( binding ) {
		if ( this._openElement ) {
			this._openElement.hide ();
		}
		this._openElement = binding;
		this._isOpen = true;
	} else {
		this._openElement = null;
		this._isOpen = false;
	}
}

/**
 * Locating ancestor MenuContainerBinding.
 * @return {MenuContainerBinding}
 */
MenuContainerBinding.prototype.getMenuContainerBinding = function () {
	
	if ( !this.menuContainerBinding ) {
		this.menuContainerBinding = this.getAncestorBindingByType ( MenuContainerBinding );
	}
	return this.menuContainerBinding;
}

/**
 * Locating child MenuPopupBinding. Code allows for the menupopup to be replaced runtime.
 * @return {MenuPopupBinding}
 */
MenuContainerBinding.prototype.getMenuPopupBinding = function () {
	
	var menuPopup = this.getChildBindingByLocalName ( "menupopup" );
	if ( menuPopup && menuPopup != this.menuPopupBinding ) {
		this.menuPopupBinding = menuPopup;
		this.menuPopupBinding.addActionListener ( PopupBinding.ACTION_HIDE, this );
	}
	return this.menuPopupBinding;
}

/**
 * Show.
 * @overwrites {Binding#show}
 */
MenuContainerBinding.prototype.show = function () {
	
	var container = this.getMenuContainerBinding ();
	container.setOpenElement ( this );
	
	var popup = this.getMenuPopupBinding ();
	popup.snapTo ( this.bindingElement );
	popup.show ();
}

/**
 * Hide.
 * @overwrites {Binding#hide}
 */
MenuContainerBinding.prototype.hide = function () {
	
	this.reset ();
	this.getMenuPopupBinding ().hide ();
	if ( this._isOpen ) {
		this._openElement.hide ();
	}
}

/**
 * Reset visual appearance when menus are closed. Subclasses should define this method.
 */
MenuContainerBinding.prototype.reset = Binding.ABSTRACT_METHOD;

/**
 * @implements {IActionListener}
 * @overloads {Binding#handleAction}
 * @param {Action} action
 */
MenuContainerBinding.prototype.handleAction = function ( action ) {
	
	MenuContainerBinding.superclass.handleAction.call ( this, action );
	
	if ( action.type == PopupBinding.ACTION_HIDE ) {
		var container = this.getMenuContainerBinding ();
		container.setOpenElement ( false );
		this.reset ();
		action.consume ();
	}
}