/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

StageSplitterBodyBinding.prototype = new Binding;
StageSplitterBodyBinding.prototype.constructor = StageSplitterBodyBinding;
StageSplitterBodyBinding.superclass = Binding.prototype;

/**
 * When dragging stage splitters, this fellow needs to be rendered  
 * in a top level layer in order to appear above views.
 * @class
 */
function StageSplitterBodyBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "StageSplitterBodyBinding" );
	
	/**
	 * @type {string}
	 */
	this._orient = null;
}

/**
 * Identifies binding.
 */
StageSplitterBodyBinding.prototype.toString = function () {

	return "[StageSplitterBodyBinding]";
}

/**
 * Set orientation. This determines the draggable directions.
 */
StageSplitterBodyBinding.prototype.setOrient = function ( orient ) {
	
	this._orient = orient;
	this.attachClassName ( orient );
}

/**
 * Set position.
 * @param {Position} pos
 */
StageSplitterBodyBinding.prototype.setPosition = function ( pos ) {
	
	var isHorizontal = true;
	var isVertical = true;
	
	switch ( this._orient ) {
		case SplitBoxBinding.ORIENT_HORIZONTAL :
			isVertical = false;
			break;
 		case SplitBoxBinding.ORIENT_VERTICAL :
			isHorizontal = false;
			break;
	}
	if ( isHorizontal ) {
		this.bindingElement.style.left = pos.x + "px";
	}
	if ( isVertical ) {
		this.bindingElement.style.top = pos.y + "px";
	}
}

/**
 * Set dimension.
 * @param {Dimension} dim
 */
StageSplitterBodyBinding.prototype.setDimension = function ( dim ) {
	
	this.bindingElement.style.width = dim.w + "px";
	this.bindingElement.style.height = dim.h + "px";
}

/**
 * Show.
 */
StageSplitterBodyBinding.prototype.show = function () {
	
	this.bindingElement.style.display = "block";
}

/**
 * Hide. This will automatically reset drag session settings.
 * @overwrites {Binding#hide}
 */
StageSplitterBodyBinding.prototype.hide = function () {
	
	this.bindingElement.style.display = "none";
	this.detachClassName ( SplitBoxBinding.ORIENT_HORIZONTAL );
	this.detachClassName ( SplitBoxBinding.ORIENT_VERTICAL );
	this._orient = null;
}
