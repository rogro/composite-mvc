/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

MenuGroupBinding.prototype = new Binding;
MenuGroupBinding.prototype.constructor = MenuGroupBinding;
MenuGroupBinding.superclass = Binding.prototype;

MenuGroupBinding.LAYOUT_DEFAULT = 0;
MenuGroupBinding.LAYOUT_FIRST = 1;
MenuGroupBinding.LAYOUT_LAST = 2;

function MenuGroupBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "MenuGroupBinding" );
	
	/**
	 * @type {boolean}
	 */
	this.isVisible = true;
}

/**
 * Identifies binding.
 */
MenuGroupBinding.prototype.toString = function () {

	return "[MenuGroupBinding]";
}

/**
 * Set layout. This method is invoked by the {@link ToolBarBodyBinding}.
 * @param {int} layout
 */
MenuGroupBinding.prototype.setLayout = function ( layout ) {

	switch ( layout ) {
		case MenuGroupBinding.LAYOUT_DEFAULT :
			this.detachClassName ( "first" );
			this.detachClassName ( "last" );
			break;
		case MenuGroupBinding.LAYOUT_FIRST :
			this.attachClassName ( "first" );
			break;	
		case MenuGroupBinding.LAYOUT_LAST :
			this.attachClassName ( "last" );
			break;
	}
}

/**
 * Show group. The unnescessary visibility adjustment is nescessary for explorer.
 * @overwrites {Binding#show}
 */
MenuGroupBinding.prototype.show = function () {
	
	if ( !this.isVisible ) {
		this.bindingElement.style.display = "block";
		this.bindingElement.style.visibility = "visible";
		this.isVisible = true;
	}
}

/**
 * Hide group.
 * @overwrites {Binding#hide}
 */
MenuGroupBinding.prototype.hide = function () {
	
	if ( this.isVisible ) {
		this.bindingElement.style.display = "none";
		this.bindingElement.style.visibility = "hidden";
		this.isVisible = false;
	}
}


/**
* Empty content (of body).
*/
MenuGroupBinding.prototype.empty = function () {

	this.detachRecursive();
	this.bindingElement.innerHTML = "";
}

/**
 * MenuGroupBinding factory.
 * @param {DOMDocument} ownerDocument
 * @return {MenuGroupBinding}
 */
MenuGroupBinding.newInstance = function ( ownerDocument ) {

	var element = DOMUtil.createElementNS ( Constants.NS_UI, "ui:menugroup", ownerDocument );
	return UserInterface.registerBinding ( element, MenuGroupBinding );
}