/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

DialogSetBinding.prototype = new Binding;
DialogSetBinding.prototype.constructor = DialogSetBinding;
DialogSetBinding.superclass = Binding.prototype;

/**
 * @class
 */
function DialogSetBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "DialogSetBinding" );
}

/**
 * Identifies binding.
 */
DialogSetBinding.prototype.toString = function () {

	return "[DialogSetBinding]";
}

/**
 * Overloads {Binding#onBindingAttach}
 */
DialogSetBinding.prototype.onBindingAttach = function () {
	
	DialogSetBinding.superclass.onBindingAttach.call ( this );
	
	this.addActionListener ( Binding.ACTION_MOVETOTOP, this );
	this.addActionListener ( Binding.ACTION_MOVEDONTOP, this );
}

/**
 * @implements {IActionListener}
 * @overloads {Binding#handleAction}
 * @param {Action} action
 */
DialogSetBinding.prototype.handleAction = function ( action ) {

	DialogSetBinding.superclass.handleAction.call ( this, action );
	
	var binding = action.target;

	switch ( action.type ) {
		case Binding.ACTION_MOVETOTOP :
			if ( binding instanceof DialogBinding ) {
				this._moveToTop ( binding );
			}
			break;
		case Binding.ACTION_MOVEDONTOP :
			action.consume ();
			break;
	}
}

/**
 * Move dialog to highest z-index. Increment index by three so that the 
 * {@link DialogCoverBinding} can fit the slot between two dialogs.
 * @param {DialogBinding} binding
 */
DialogSetBinding.prototype._moveToTop = function ( binding ) {
	
	var maxIndex = 0;
	var dialogs = this.getChildBindingsByLocalName ( "dialog" );
	
	dialogs.each ( function ( dialog ) {
		var index = dialog.getZIndex ();
		maxIndex = index > maxIndex ? index : maxIndex;
	});
	
	binding.setZIndex ( maxIndex + 2 );
}