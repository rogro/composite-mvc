/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

StyleBinding.prototype = new Binding;
StyleBinding.prototype.constructor = StyleBinding;
StyleBinding.superclass = Binding.prototype;


/**
 * @class
 */
function StyleBinding() {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger("StyleBinding");

	/**
	 * @type {HtmlElement}
	 */
	this.style = null;

	/**
	 * @type {string}
	 */
	this.href = null;

	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
StyleBinding.prototype.toString = function () {

	return "[StyleBinding]";
}

/**
 * Notice that the binding disposes as soon as it attaches.
 * @overloads {Binding#onBindingAttach}
 */
StyleBinding.prototype.onBindingAttach = function () {

	StyleBinding.superclass.onBindingAttach.call(this);

	console.log("StyleBinding.prototype.onBindingAttach");

	this.href = this.getProperty("link");
	this.style = document.createElement('link');
	this.style.rel = 'stylesheet';
	this.style.type = 'text/css';
	this.style.href = Resolver.resolve(this.href);
	this.bindingDocument.getElementsByTagName('head')[0].appendChild(this.style);
}

/**
 * Handle element update.
 * @implements {IUpdateHandler}
 * @overwrites {Binding#handleElement}
 * @param {Element} element
 * @return {boolean}
 */
StyleBinding.prototype.handleElement = function (element) {

	return true;
}

/**
 * Update element.
 * @implements {IUpdateHandler}
 * @overwrites {Binding#updateElement}
 * @param {Element} element
 * @return {boolean}
 */
StyleBinding.prototype.updateElement = function (element) {

	var href = element.getAttribute("link");

	if (this.href != href) {
		this.href = href;
		this.style.href = Resolver.resolve(this.href);
	}
	return true;
};

/**
 * @overloads {Binding#onBindingDispose}
 */
StyleBinding.prototype.onBindingDispose = function () {

	StyleBinding.superclass.onBindingDispose.call(this);

	if (this.style && this.style.parentNode) {
		this.style.parentNode.removeChild(this.style);
	}
	this.href = null;
	this.style = null;
}