/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

ToolBarLabelBinding.prototype = new Binding;
ToolBarLabelBinding.prototype.constructor = ToolBarLabelBinding;
ToolBarLabelBinding.superclass = Binding.prototype;

/**
 * @class
 * Simple text (and possibly an image) on the toolbar.
 */
function ToolBarLabelBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "ToolBarLabelBinding" );
}

/**
 * Identifies binding.
 */
ToolBarLabelBinding.prototype.toString = function () {
	
	return "[ToolBarLabelBinding]";
}

/**
 * @overloads {Binding#onBindingAttach}
 */
ToolBarLabelBinding.prototype.onBindingAttach = function () {
	
	ToolBarLabelBinding.superclass.onBindingAttach.call ( this );
	
	this._labelBinding = this.add ( LabelBinding.newInstance ( this.bindingDocument ));
	this.shadowTree.label = this._labelBinding;
	
	var label = this.getProperty ( "label" );
	var image = this.getProperty ( "image" );
	
	if ( label ) {
		this.setLabel ( label );
	}
	
	if ( image ) {
		this.setImage ( image );
	}
}

/**
 * Set label.
 * @param {string} label
 * @param {boolean} isNotBuildingClassName Set to true for faster screen update.
 */
ToolBarLabelBinding.prototype.setLabel = function ( label, isNotBuildingClassName ) {
	
	if ( this.isAttached ) {
		this._labelBinding.setLabel ( label, isNotBuildingClassName );
	}
	this.setProperty ( "label", label );
}

/**
 * Set image.
 * @param {string} image
 * @param {boolean} isNotBuildingClassName Set to true for faster screen update.
 */
ToolBarLabelBinding.prototype.setImage = function ( image, isNotBuildingClassName ) {
	
	if ( this.isAttached ) {
		this._labelBinding.setImage ( image, isNotBuildingClassName );
	}
	this.setProperty ( "image", image );
}

/**
 * ToolBarLabelBinding factory.
 * @param {DOMDocument} ownerDocument
 * @return {ToolBarLabelBinding}
 */
ToolBarLabelBinding.newInstance = function ( ownerDocument ) {

	var element = DOMUtil.createElementNS ( Constants.NS_UI, "ui:toolbarlabel", ownerDocument );
	return UserInterface.registerBinding ( element, ToolBarLabelBinding );
}