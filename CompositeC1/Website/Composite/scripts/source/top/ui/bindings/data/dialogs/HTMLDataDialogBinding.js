/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

HTMLDataDialogBinding.prototype = new PostBackDataDialogBinding;
HTMLDataDialogBinding.prototype.constructor = HTMLDataDialogBinding;
HTMLDataDialogBinding.superclass = PostBackDataDialogBinding.prototype;

/**
 * @class
 * This will open a {@link WysiwygEditorBinding} in a dialog.
 */
function HTMLDataDialogBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "HTMLDataDialogBinding" );
}

/**
 * Identifies binding.
 */
HTMLDataDialogBinding.prototype.toString = function () {
	
	return "[HTMLDataDialogBinding]";
}

/**
 * @overloads {StringDataDialogBinding#onBindingAttach}
 */
HTMLDataDialogBinding.prototype.onBindingAttach = function () {
	
	if ( this.getProperty ( "label" ) == null ) {
		this.setProperty ( "label", "Edit HTML" ); // TODO: stringbundle this!
	}
	HTMLDataDialogBinding.superclass.onBindingAttach.call ( this );
}

/**
 * @overwrites {DataDialogBinding#fireCommand}
 */
HTMLDataDialogBinding.prototype.fireCommand = function () {
	
	this.dispatchAction ( DataDialogBinding.ACTION_COMMAND );
	
	/*
	 * Build argument for editor configuration.
	 */
	var argument = {
		label : DataBinding.getAssociatedLabel ( this ),
		value : decodeURIComponent ( this.getValue ()),
		configuration : {
			"formattingconfiguration"	: this.getProperty ( "formattingconfiguration" ),
			"embedablefieldstypenames"  : this.getProperty ( "embedablefieldstypenames"),
            "previewtemplateid"	        : this.getProperty ( "previewtemplateid" ),
            "previewplaceholder"	    : this.getProperty ( "previewplaceholder" ),
            "previewpageid"	            : this.getProperty ( "previewpageid" )
		}
	}
	
	/*
	 * The dialoghandler is defined by superclass.
	 * @see {DataDialogBinding}
	 */
	var definition = ViewDefinitions [ "Composite.Management.VisualEditorDialog" ];
	definition.handler = this._handler;
	definition.argument = argument;
	StageBinding.presentViewDefinition ( definition );
	
	this._releaseKeyboard ();
}