/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

ProgressBarBinding.prototype = new Binding;
ProgressBarBinding.prototype.constructor = ProgressBarBinding;
ProgressBarBinding.superclass = Binding.prototype;

ProgressBarBinding.WIDTH = 190;
ProgressBarBinding.NOTCH = 9;

/**
 * Considered private.
 * @type {ProgressBarBinding}
 */
ProgressBarBinding._bindingInstance = null;

/**
 * Notch progress.
 * @param {int} notches
 */
ProgressBarBinding.notch = function ( notches ) {
	
	var bar = ProgressBarBinding._bindingInstance;
	if ( bar != null ) {
		bar.notch ( notches );
	}
}

/**
 * @class
 * This is actually quite a fake progressbar, used only on app initialization.
 */
function ProgressBarBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "ProgressBarBinding" );
	
	/**
	 * @type {CoverBinding}
	 */
	this._cover = null;
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
ProgressBarBinding.prototype.toString = function () {
	
	return "[ProgressBarBinding]";
}

/**
 * @overloads {Binding#onBindingAttach}
 */
ProgressBarBinding.prototype.onBindingAttach = function () {
	
	ProgressBarBinding.superclass.onBindingAttach.call ( this );
	
	ProgressBarBinding._bindingInstance = this;
	
	this._cover = this.add ( CoverBinding.newInstance ( this.bindingDocument ));
	this._cover.setBusy ( false );
	this._cover.setWidth ( ProgressBarBinding.WIDTH );
	this.shadowTree.cover = this._cover;
}

/**
 * Notch progress. Defaults to a single notch if no argument is supplied.
 * @param {int} notches
 */
ProgressBarBinding.prototype.notch = function ( notches ) {
	
	notches = notches ? notches : 1;
	var width = this._cover.getWidth () - ( ProgressBarBinding.NOTCH * notches );
	this._cover.setWidth ( width >= 0 ? width : 0 );
}