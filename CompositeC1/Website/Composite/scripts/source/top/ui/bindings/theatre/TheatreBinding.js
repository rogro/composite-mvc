/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

TheatreBinding.prototype = new Binding;
TheatreBinding.prototype.constructor = TheatreBinding;
TheatreBinding.superclass = Binding.prototype;

TheatreBinding.CLASSNAME_INITIALIZED = "initialized";

/**
 * @class
 * The TheatreBinding currently handles only one scenario: The server offline show.
 */
function TheatreBinding () {
	
	/**
	 * @type {boolean}
	 */
	this._isPlaying = false;
	
	/**
	 * Don't use fade while the browser is negoatiating a HTTP connection.
	 * @type {boolean}
	 */
	this._isFading = false;
	
	/**
	 * @type {HTMLCanvasElement}
	 */
	this._canvas = null;
	
	/*
	 * Returnable 
	 */
	return this;
}

/**
 * Identifies binding
 * @return {string}
 */
TheatreBinding.prototype.toString = function () {
	
	return "[TheatreBinding]";
}

/**
 * @overloads {Binding#onBindingAttach}
 */
TheatreBinding.prototype.onBindingAttach = function () {
	
	TheatreBinding.superclass.onBindingAttach.call ( this );
	
	this._canvas = document.createElement ( "canvas" );
	this.bindingElement.appendChild ( this._canvas );
}

/**
 * Play.
 */
TheatreBinding.prototype.play = function ( isFading ) {
	
	this._isFading = isFading == true;
	
	if ( !this._isPlaying ) {
		Application.lock ( this );
		this.show ();
		this._isPlaying = true;
		if ( this._isFading ) {
			this._fade ();
		}
	}
}

TheatreBinding.prototype._fade = function () {
	
	var context = this._canvas.getContext ( "2d" );
	var alpha = parseInt ( 0 );

	TheatreBinding._interval = top.setInterval ( function () {
		if ( alpha < 0.5 ) {
			context.fillStyle = "rgba(0,0,0," + new String ( alpha ) + ")";
			context.clearRect ( 0, 0, 300, 150 );
			context.fillRect ( 0, 0, 300, 150 );
			alpha += 0.002;
		} else {
			top.clearInterval ( TheatreBinding._interval );
			TheatreBinding._interval = null;
		}
	}, 50 );
}

/**
 * Stop.
 */
TheatreBinding.prototype.stop = function () {
	
	if ( this._isPlaying ) {
		
		if ( this._isFading ) {
			if ( TheatreBinding._interval != null ) {
				top.clearInterval ( TheatreBinding._interval );
			}
			var context = this._canvas.getContext ( "2d" );
			context.clearRect ( 0, 0, 300, 150 );
		}
		
		Application.unlock ( this, true ); // boolean arg because Explorer may stay locked...
		this.hide ();
		this._isPlaying = false;
	}
}