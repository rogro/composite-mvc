/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

ResponseBinding.prototype = new Binding;
ResponseBinding.prototype.constructor = ResponseBinding;
ResponseBinding.superclass = Binding.prototype;

ResponseBinding.ACTION_SUCCESS = "response success";
ResponseBinding.ACTION_OOOOKAY = "response ooookay";
ResponseBinding.ACTION_FAILURE = "response failure";

/**
 * @class
 */
function ResponseBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "ResponseBinding" );
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
ResponseBinding.prototype.toString = function () {

	return "[ResponseBinding]";
}

/**
 * Note that we evaluate status as soon as binding attaches.
 * @overloads {Binding#onBindingAttach}
 */
ResponseBinding.prototype.onBindingAttach = function () {

	ResponseBinding.superclass.onBindingAttach.call ( this );
	this.propertyMethodMap [ "checksum" ] = this._update;
	this._update ();
}

/**
 * Update stuff!
 */
ResponseBinding.prototype._update = function () {

	// Mark as Dirty
	if (this.getProperty("dirty") === true) {
		this.dispatchAction(Binding.ACTION_DIRTY);
	}
	/*
	* Any status updates? These ations get intercepted somewhere in PageBinding.
	* @see {PageBinding#_setupDotNet}
	*/
	var status = this.getProperty("status");
	if (status != null) {
		switch (status) {
			case "success":
				this.dispatchAction(ResponseBinding.ACTION_SUCCESS);
				break;
			case "failure":
				this.dispatchAction(ResponseBinding.ACTION_FAILURE);
				break;
			case "ooookay":
				this.dispatchAction(ResponseBinding.ACTION_OOOOKAY);
				break;
		}
	}

	/*
	* Any messages?
	*/
	var index = this.getProperty("messagequeueindex");
	if (index != null) {
		if (index > MessageQueue.index) {
			MessageQueue.update(true);
		}
	}
}