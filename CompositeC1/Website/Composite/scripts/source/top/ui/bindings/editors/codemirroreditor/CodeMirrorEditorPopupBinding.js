/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

CodeMirrorEditorPopupBinding.prototype = new EditorPopupBinding;
CodeMirrorEditorPopupBinding.prototype.constructor = CodeMirrorEditorPopupBinding;
CodeMirrorEditorPopupBinding.superclass = EditorPopupBinding.prototype;

CodeMirrorEditorPopupBinding.CONTENT_TEMPLATE = "sourceeditor/popup.xml";

/**
 * @class
 */
function CodeMirrorEditorPopupBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "CodeMirrorEditorPopupBinding" );
	
	/**
	 * The containing editor.
	 * @type {SourceEditorBinding}
	 */
	this._editorBinding = null;
	
	/**
	 * This element has been spirited with some extendo  
	 * functions constituting the core CorePress ballyhoo.  
	 * @type {HTMLIframeElement}
	 */
	this._codePressFrame = null;
	
	/**
	 * Somehow there are two CodePress objects in this 
	 * version of CodePress. This is the "engine" version.
	 * @type {CodePress}
	 */
	this._codePressEngine = null;
}

/**
 * Identifies binding.
 */
CodeMirrorEditorPopupBinding.prototype.toString = function () {

	return "[CodeMirrorEditorPopupBinding]";
}

/**
 * Configure contextmenu for the current source code editor.
 * @implements {IWysiwygEditorComponent}
 * @param {SourceEditorBinding} editor
 * @param {HTMLIframeElement} frame
 * @param {CodePress} engine
 */
CodeMirrorEditorPopupBinding.prototype.configure = function ( editor, frame, engine ) {

	this._editorBinding = editor;
	this._codePressFrame = frame;
	this._codePressEngine = engine;
	
	WysiwygEditorPopupBinding.superclass.configure.call ( this );
}

/**
 * Configure stuff.
 */
CodeMirrorEditorPopupBinding.prototype._configure = function () {
	
	/*
	 * Show XML tools?
	 */
	switch ( this._editorBinding.syntax ) {
		case SourceEditorBinding.syntax.XML :
		case SourceEditorBinding.syntax.XSL :
		case SourceEditorBinding.syntax.HTML :
			this._showMenuGroups ( "xml" );
			break;
		default :
			this._hideMenuGroups ( "xml" );
			break;
	}
}

/**
 * Handle that command.
 * @param {string} cmd
 * @param [string} gui
 * @param {string} val
 */
CodeMirrorEditorPopupBinding.prototype.handleCommand = function ( cmd, gui, val ) {
	
	var win = this._editorBinding.getContentWindow ();
	var but = null;
	
	switch ( cmd ) {
		
		case "compositeInsert" :	
			but = win.bindingMap.insertbutton;
			break;
			
		case "compositeFormat" :
			but = win.bindingMap.formatbutton;
			break;
	}
	
	if ( but != null ) {
		but.handleCommand ( cmd, gui, val );
	}
}