/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

UncoverBinding.prototype = new Binding;
UncoverBinding.prototype.constructor = UncoverBinding;
UncoverBinding.superclass = Binding.prototype;

/**
 * Considered private.
 * @type {UncoverBinding}
 */
UncoverBinding._bindingInstance = null;

/**
 * Invoke to normalize cursor.
 * @param {Position} pos
 */
UncoverBinding.uncover = function ( pos ) {
	
	var binding = UncoverBinding._bindingInstance;
	if ( Binding.exists ( binding )) {
		binding.setPosition ( pos );
	}
}

/**
 * @class
 * When a "busy" cover gets hidden, the wait cursor may 
 * hang on, indicating activity until the mouse is moved. 
 * We force a cursor update by positioning the uncover 
 * at exact mouse position.
 */
function UncoverBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "UncoverBinding" );
	
	/*
	 * Register globally.
	 */
	UncoverBinding._bindingInstance = this;
}

/**
 * Identifies binding.
 */
UncoverBinding.prototype.toString = function () {
	
	return "[UncoverBinding]";
}

/**
 * This will place the bindings CENTER at the specified position.
 * @param {Position} pos
 */
UncoverBinding.prototype.setPosition = function ( pos ) {
	
	this.bindingElement.style.display = "block";
	var dim = this.boxObject.getDimension ();
	
	pos.x -= 0.5 * dim.w;
	pos.y -= 0.5 * dim.h;
	
	pos.x = pos.x < 0 ? 0 : pos.x;
	pos.y = pos.y < 0 ? 0 : pos.y;
	
	this.bindingElement.style.left = String ( pos.x ) + "px";
	this.bindingElement.style.top = String ( pos.y ) + "px";
	this.bindingElement.style.cursor = "wait";
	
	/*
	 * Flashing the cursor property, 
	 * forcing IE to update rendering.
	 */
	var self = this;
	setTimeout ( function () {
		self.bindingElement.style.cursor = "default";
		self.bindingElement.style.display = "none";
	}, 0 );
}