/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

ControlImageProfile.IMAGE_MINIMIZE = null;
ControlImageProfile.IMAGE_MAXIMIZE = null;
ControlImageProfile.IMAGE_RESTORE = null; 
ControlImageProfile.IMAGE_CLOSE = null;

/**
 * Please subclass this in order to use!
 * @param {ControlBinding} binding
 */
function ControlImageProfile ( binding ) {
	
	this.binding = binding;
}

/**
 * @param {string} string
 * @return {string}
 * @private
 */
ControlImageProfile.prototype._getImage = function ( string ) {
	
	var result = null;

	switch ( this.binding.controlType ) {
		case ControlBinding.TYPE_MINIMIZE :
			result = this.constructor.IMAGE_MINIMIZE;
			break;
		case ControlBinding.TYPE_MAXIMIZE :
			result = this.constructor.IMAGE_MAXIMIZE;
			break;
		case ControlBinding.TYPE_UNMAXIMIZE :
		case ControlBinding.TYPE_UNMINIMIZE :
			result = this.constructor.IMAGE_RESTORE;
			break;
		case ControlBinding.TYPE_CLOSE :
			result = this.constructor.IMAGE_CLOSE;
			break;
	}
	
	return result.replace ( "${string}", string );
}

/**
 * Get default image. Unlike hover and active, this may depend on container state.
 * @return {string}
 */
ControlImageProfile.prototype.getDefaultImage = function () {

	var isActive = true;
	if ( this.binding.isGhostable && this.binding.containingControlBoxBinding ) {
		isActive = this.binding.containingControlBoxBinding.isActive ? true : false;
	}
	return isActive ? this._getImage ( "default" ) : this._getImage ( "ghosted" );
}

/**
 * Get hover image.
 * @return {string}
 */
ControlImageProfile.prototype.getHoverImage = function () {

	return this._getImage ( "hover" );
}

/**
 * Get active image.
 * @return {string}
 */
ControlImageProfile.prototype.getActiveImage = function () {

	return this._getImage ( "active" );
}