/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

DialogBorderBinding.prototype = new Binding;
DialogBorderBinding.prototype.constructor = DialogBorderBinding;
DialogBorderBinding.superclass = Binding.prototype;

DialogBorderBinding.TYPE_NORTH	= "n";
DialogBorderBinding.TYPE_SOUTH 	= "s";
DialogBorderBinding.TYPE_EAST 	= "e";
DialogBorderBinding.TYPE_WEST 	= "w";
DialogBorderBinding.DIMENSION 	= 4;

/**
 * @class
 */
function DialogBorderBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "DialogBorderBinding" );
	
	/**
	 * Overwrites super property.
	 * @type {boolean}
	 */
	this.isDraggable = true;
	
	/**
	 * This property is set by the containing {@link DialogBinding}.
	 * @type {string}
	 * @private
	 */
	this._type = null;
}

/**
 * Identifies binding.
 */
DialogBorderBinding.prototype.toString = function () {

	return "[DialogBorderBinding]";
}

/**
 * @param {string} type
 */
DialogBorderBinding.prototype.setType = function ( type ) {
 
	this.attachClassName ( type );
	this._type = type;
}

/**
 * @return {string}
 */
DialogBorderBinding.prototype.getType = function () {

	return this._type;
}

/**
 * DialogBorderBinding factory.
 * @param {DOMDocument} ownerDocument
 * @return {DialogBorderBinding}
 */
DialogBorderBinding.newInstance = function ( ownerDocument ) {
	var element = DOMUtil.createElementNS ( Constants.NS_UI, "ui:dialogborder", ownerDocument );
	return UserInterface.registerBinding ( element, DialogBorderBinding );
}