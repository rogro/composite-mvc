/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

BlockBinding.prototype = new Binding;
BlockBinding.prototype.constructor = BlockBinding;
BlockBinding.superclass = Binding.prototype;

/**
 * @class
 */
function BlockBinding () {
	
	/**
	 * @type {boolean}
	 */
	this._isBlocking = false;
	
	/*
	 * Returnable 
	 */
	return this;
}

/**
 * Identifies binding
 * @return {string}
 */
BlockBinding.prototype.toString = function () {
	
	return "[BlockBinding]";
}

/**
 * Block GUI.
 */
BlockBinding.prototype.block = function () {
	
	if ( !this._isBlocking ) {
		Application.lock ( this );
		this._isBlocking = true;
		this.show ();
	}
}

/**
 * Unblock gui.
 */
BlockBinding.prototype.unblock = function () {
	
	if ( this._isBlocking == true ) {
		this.hide ();
	}
}