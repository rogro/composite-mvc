/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

MenuBarBinding.prototype = new MenuContainerBinding;
MenuBarBinding.prototype.constructor = MenuBarBinding;
MenuBarBinding.superclass = MenuContainerBinding.prototype;

function MenuBarBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "MenuBarBinding" );
	
	/**
	 * Block common crawlers.
	 * @type {Map<string><boolean>}
	 * @overwrites {Binding#crawlerFilters}
	 */
	this.crawlerFilters	= new List ([ FlexBoxCrawler.ID, FocusCrawler.ID ]);
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
MenuBarBinding.prototype.toString = function () {
	
	return "[MenuBarBinding]";
}

/**
 * Attach classname to clear stylesheet floats.
 * @overloads {Binding#onBindingRegister}
 */
MenuBarBinding.prototype.onBindingRegister = function () {
	
	MenuBarBinding.superclass.onBindingRegister.call ( this );
	this.addActionListener ( MenuBodyBinding.ACTION_UNHANDLED_LEFTRIGHTKEY );
	this.attachClassName ( Binding.CLASSNAME_CLEARFLOAT );
}

/**
 * @implements {IActionListener}
 * @overloads {Binding#handleAction}
 * @param {Action} action
 */
MenuBarBinding.prototype.handleAction = function ( action ) {
	
	MenuBarBinding.superclass.handleAction.call ( this, action );
	
	switch ( action.type ) {
		case MenuBodyBinding.ACTION_UNHANDLED_LEFTRIGHTKEY :
			
			var menuBody = action.target;
			
			var menus = this.getChildBindingsByLocalName ( "menu" );
			while ( menus.hasNext ()) {
				var menu = menus.getNext ();
			}
			
			switch ( menuBody.arrowKey ) {
				case KeyEventCodes.VK_LEFT :
					this.logger.debug ( "LEFTG" );
					break;
				case KeyEventCodes.VK_RIGHT :
					this.logger.debug ( "RIGHT" );
					break;
			}
			
			break;
	}
}