/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

ExplorerToolBarButtonBinding.prototype = new ToolBarButtonBinding;
ExplorerToolBarButtonBinding.prototype.constructor = ExplorerToolBarButtonBinding;
ExplorerToolBarButtonBinding.superclass = ToolBarButtonBinding.prototype;
ExplorerToolBarButtonBinding.TYPE_NORMAL = "normal";
ExplorerToolBarButtonBinding.TYPE_LARGE = "large";

/**
 * @class
 */
function ExplorerToolBarButtonBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "ExplorerToolBarButtonBinding" );
	
	/** 
	 * @type {boolean}
	 */
	this.isRadioButton = true;
	
	/** 
	 * @type {string}
	 */
	this.explorerToolBarButtonType = null;
	
	/**
	 * This property is set by the {@link ExplorerMenuBinding}
	 * @type {SystemNode}
	 */
	this.node = null;
}

/**
 * Identifies binding.
 */
ExplorerToolBarButtonBinding.prototype.toString = function () {

	return "[ExplorerToolBarButtonBinding]";
}

/**
 * Compute button image.
 * @overloads {Binding#onBindingAttach}
 */
ExplorerToolBarButtonBinding.prototype.onBindingAttach = function () {
	
	var isLargeButton = this.explorerToolBarButtonType == ExplorerToolBarButtonBinding.TYPE_LARGE;
	var imageSizeParameter = isLargeButton ? ToolBarBinding.IMAGESIZE_LARGE : ToolBarBinding.IMAGESIZE_NORMAL;
	this.imageProfile = this.node.getImageProfile ( imageSizeParameter );
	ExplorerToolBarButtonBinding.superclass.onBindingAttach.call ( this );
}

/**
 * ExplorerToolBarButtonBinding factory.
 * @param {DOMDocument} ownerDocument
 * @param {string} explorerToolBarButtonType
 * @return {ExplorerToolBarButtonBinding}
 */
ExplorerToolBarButtonBinding.newInstance = function ( ownerDocument, explorerToolBarButtonType ) {

	var nodename = ( 
		explorerToolBarButtonType == ExplorerToolBarButtonBinding.TYPE_LARGE ? 
		 	"ui:explorertoolbarbutton" :
		 	"ui:toolbarbutton"
	);
	
	var element = DOMUtil.createElementNS ( Constants.NS_UI, nodename, ownerDocument );
	var binding = UserInterface.registerBinding ( element, ExplorerToolBarButtonBinding );
	binding.explorerToolBarButtonType = explorerToolBarButtonType;
	return binding;
}