/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

MozEditorTextBoxBinding.prototype = new EditorTextBoxBinding;
MozEditorTextBoxBinding.prototype.constructor = MozEditorTextBoxBinding;
MozEditorTextBoxBinding.superclass = EditorTextBoxBinding.prototype;

/**
 * Tab indent, tab preservation, no soft text wrap.
 * @class
 */
function MozEditorTextBoxBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "MozEditorTextBoxBinding" );
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
MozEditorTextBoxBinding.prototype.toString = function () {
	
	return "[MozEditorTextBoxBinding]";
}

/**
 * Handle TAB key.
 * @param {boolean} isReverse
 */
MozEditorTextBoxBinding.prototype._handleTabKey = function ( isReverse ) {
	
	var lastx;
	var lasty;
	var oss;
	var osy;
	var i;
	var fnd;
	var selectedText = this._getSelectedText ();
	var el = this.shadowTree.input;
	
	lastx = el.scrollLeft;
	lasty = el.scrollTop;
	
	if (!selectedText.match(/\n/)) {
		oss = el.selectionStart;
		el.value = el.value.substr(0, el.selectionStart) + "\t" + el.value.substr(el.selectionEnd);
		el.selectionStart = oss + 1;
		el.selectionEnd = oss + 1;
	} else {
		oss = el.selectionStart;
		osy = el.selectionEnd;
		fnd = 0;
		for (i = oss - 1; i >= 0; i --) {
			if (el.value.charAt(i) == "\n") {
				oss = i + 1;
				fnd = 1;
				break;
			}
		} if (fnd == 0) {
			oss = 0;
		}
		fnd = 0;
		for (i = osy; i < el.value.length; i ++) {
			if (el.value.charAt(i) == "\n") {
				osy = i;
				fnd = 1;
				break;
			}
		} if (fnd == 0) {
			osy = el.value.length;
		}
		el.selectionStart = oss;
		el.selectionEnd = osy;
		selectedText = this._getSelectedText ();
		
		if ( isReverse ) {
			ntext = selectedText.replace( /^(\s)/mg, "" );
		} else {
			ntext = selectedText.replace( /^(.)/mg, "\t$1" );
		}
		el.value = el.value.substr(0, el.selectionStart) + ntext + el.value.substr(el.selectionEnd);
		el.selectionStart = oss;
		el.selectionEnd = osy + (ntext.length - selectedText.length);
	}
	el.scrollLeft = lastx;
	el.scrollTop  = lasty;		
}

/**
 * Handle ENTER key.
 */
MozEditorTextBoxBinding.prototype._handleEnterKey = function () {
	
	var lastx;
	var lasty;
	var oss;
	var osy;
	var el = this.shadowTree.input;

	lastx = el.scrollLeft;
	lasty = el.scrollTop;
	oss = el.selectionStart;
	osy = el.selectionEnd;
	var bfs = el.value.substr(0, el.selectionStart);
	var bfsm = bfs.split(/\r|\n/g);

	var spm = bfsm[bfsm.length - 1].match(/^(\s)*/);
	el.value = el.value.substr(0, el.selectionStart) + "\n" + spm[0] + el.value.substr(el.selectionEnd);
	el.selectionStart = oss + 1 + spm[0].length;
	el.selectionEnd = oss + 1 + spm[0].length;
	
	el.scrollLeft = lastx;
	el.scrollTop  = lasty;
}

/**
 * Get selected text.
 * @return {string}
 */
MozEditorTextBoxBinding.prototype._getSelectedText = function () {
	
	var value 	= this.shadowTree.input.value;
	var start 	= this.shadowTree.input.selectionStart;
	var end 	= this.shadowTree.input.selectionEnd;
	
	return value.substr ( start, end - start );
}