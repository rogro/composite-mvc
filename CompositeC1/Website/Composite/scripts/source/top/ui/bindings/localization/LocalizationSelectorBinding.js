/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

LocalizationSelectorBinding.prototype = new SelectorBinding;
LocalizationSelectorBinding.prototype.constructor = LocalizationSelectorBinding;
LocalizationSelectorBinding.superclass = SelectorBinding.prototype;

/**
 * @class
 */
function LocalizationSelectorBinding () {
	
	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "LocalizationSelectorBinding" );
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
LocalizationSelectorBinding.prototype.toString = function () {

	return "[LocalizationSelectorBinding]";
}

/**
 * @overloads {SelectorBinding#onBindingAttach}
 */
LocalizationSelectorBinding.prototype.onBindingAttach = function () {
	
	LocalizationSelectorBinding.superclass.onBindingAttach.call ( this );
	this.subscribe ( BroadcastMessages.UPDATE_LANGUAGES );
	this.subscribe ( BroadcastMessages.TOLANGUAGE_UPDATED );
	this._populateFromLanguages ( Localization.languages );
}

/**
 * @implements {IBroadcastListener}
 * @param {string} broadcast
 * @param {object} arg
 */
LocalizationSelectorBinding.prototype.handleBroadcast = function ( broadcast, arg ) {
	
	LocalizationSelectorBinding.superclass.handleBroadcast.call ( this, broadcast, arg );
	
	switch ( broadcast ) {
		
		case BroadcastMessages.TOLANGUAGE_UPDATED:
			ExplorerBinding.restoreFocuseNodes();
			break;
		case BroadcastMessages.UPDATE_LANGUAGES :
			this._populateFromLanguages ( arg );
			break;
			
		case BroadcastMessages.SAVE_ALL_DONE :
			this.unsubscribe ( BroadcastMessages.SAVE_ALL_DONE );
			EventBroadcaster.broadcast ( BroadcastMessages.CLOSE_VIEWS );
			this._invokeAction ();
			break;
	}
}

/**
 * Populate selector. If no argument, then hide the selector.
 * @param {List<object>} list A list of objects with the following properties: 
 * Name
 * IsoName
 * UrlMappingName
 * IsCurrent
 * SerializedActionToken         
*/
LocalizationSelectorBinding.prototype._populateFromLanguages = function ( list ) {
	
	if ( list != null && list.hasEntries () && list.getLength () > 1 ) {
		var selections = new List ();
		list.each ( function ( lang ) {
			selections.add ( 
				new SelectorBindingSelection (
					lang.Name,
					lang.SerializedActionToken,
					lang.IsCurrent,
					null
				)
			);
		});
		this.populateFromList ( selections );
		this.show ();
	} else {
		this.hide ();
	}
}

/**
 * Backup old value so that we may cancel selector change.
 * @overwrites {SelectorBinding#populateFromList}
 * @param {List<SelectorBindingSelection>} list
 */
LocalizationSelectorBinding.prototype.populateFromList = function ( list ) {
	
	LocalizationSelectorBinding.superclass.populateFromList.call ( this, list );
	this._backupSelectionValue = this._selectionValue;
}

/**
 * @overwrites {SelectorBinding#onValueChange}
 */
LocalizationSelectorBinding.prototype.onValueChange = function () {
	ExplorerBinding.saveFocusedNodes();
	var self = this;
	Dialog.warning ( 
		StringBundle.getString ( StringBundle.UI, "UserElementProvider.ChangeOtherActiveLocaleDialogTitle" ), 
		StringBundle.getString ( StringBundle.UI, "UserElementProvider.ChangeOtherActiveLocaleDialogText" ),
		Dialog.BUTTONS_ACCEPT_CANCEL, {
		handleDialogResponse : function ( response ) {
			switch ( response ) {
				case Dialog.RESPONSE_ACCEPT :
					if ( Application.hasDirtyDockTabs ()) {
						self.subscribe ( BroadcastMessages.SAVE_ALL_DONE );
						EventBroadcaster.broadcast ( BroadcastMessages.SAVE_ALL );
					} else {
						EventBroadcaster.broadcast ( BroadcastMessages.CLOSE_VIEWS );
						self._invokeAction ();
					}
					self._backupSelectionValue = self.getValue ();
					break;
				case Dialog.RESPONSE_CANCEL :
					self.selectByValue ( self._backupSelectionValue );
					break;
			}
		}
	})
}

/**
 * Invoke that action!
 */
LocalizationSelectorBinding.prototype._invokeAction = function () {
	
	var token = this.getValue ();
	var root = SystemNode.taggedNodes.get ( "Root" );
	var action = new SystemAction ({
		Label : "Generated Action: Change Locale",
		ActionToken : token
	});
	
	SystemAction.invoke ( action, root );
}