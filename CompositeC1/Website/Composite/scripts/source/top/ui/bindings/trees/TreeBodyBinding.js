/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

TreeBodyBinding.prototype = new FlexBoxBinding;
TreeBodyBinding.prototype.constructor = TreeBodyBinding;
TreeBodyBinding.superclass = FlexBoxBinding.prototype;

TreeBodyBinding.PADDING_TOP = 8;

/**
 * @class
 */
function TreeBodyBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "TreeBodyBinding" );
	
	/**
	 * @type {TreeBinding}
	 */
	this.containingTreeBinding = null;

	/*
	 * Returnable.
	 */
	return this;
}


/**
 * Identifies binding.
 */
TreeBodyBinding.prototype.toString = function () {

	return "[TreeBodyBinding]";
}

/**
 * @overloads {Binding#onBindingAttach}
 */
TreeBodyBinding.prototype.onBindingAttach = function () {

	TreeBodyBinding.superclass.onBindingAttach.call ( this );
	this.addActionListener ( TreeNodeBinding.ACTION_FOCUSED );
	this.containingTreeBinding = UserInterface.getBinding ( 
		this.bindingElement.parentNode
	);
}

/**
 * @implements {IAcceptable}
 * @param {Binding} binding
 */
TreeBodyBinding.prototype.accept = function ( binding ) {

	if ( binding instanceof TreeNodeBinding ) {
		this.logger.debug ( binding );
	}
}

/**
 * @implements {IActionListener}
 * @overloads {Binding#handleAction}
 * @param {Action} action
 */
TreeBodyBinding.prototype.handleAction = function ( action ) {
	
	TreeBodyBinding.superclass.handleAction.call ( this, action );
	
	switch ( action.type ) {
		case TreeNodeBinding.ACTION_FOCUSED :
			this._scrollIntoView ( action.target );
			action.consume ();
			break;
	}
}

/**
 * Adjust scroll position so that focused treenodes are always visible.
 * @param {TreeNodeBinding} treenode
 */
TreeBodyBinding.prototype._scrollIntoView = function ( treenode ) {
	
	var label = treenode.labelBinding.bindingElement;
	var a = this.bindingElement.clientHeight;
	var y = label.offsetTop;
	var h = label.offsetHeight;
	var t = this.bindingElement.scrollTop;
	var l = this.bindingElement.scrollLeft;
	
	/*
	 * Scroll into view.
	 */
	if ( y - t < 0 ) {
		label.scrollIntoView ( true );
	} else if ( y - t + h > a ) {
		label.scrollIntoView ( false );
	}
	
	/*
	 * IE may present an extreme horizontal scroll. 
	 * We hack it by locking scrollLeft completely. 
	 * Tough luck for deeply nested tree structures.
	 */
	if ( Client.isExplorer ) {
		this.bindingElement.scrollLeft = l;
	}
}

/**
 * @implements {IAcceptable}
 *
TreeBodyBinding.prototype.showAcceptance = function () {
	
	this.bindingElement.style.backgroundColor = "yellow";	
}

/**
 * @implements {IAcceptable}
 *
TreeBodyBinding.prototype.hideAcceptance = function () {
	
	this.bindingElement.style.backgroundColor = "Window";	
}
*/

/**
 * TreeBodyBinding factory.
 * @param {DOMDocument} ownerDocument
 * @return {TreeBodyBinding}
 */
TreeBodyBinding.newInstance = function ( ownerDocument ) {

	var element = DOMUtil.createElementNS ( Constants.NS_UI, "ui:treebody", ownerDocument );
	return UserInterface.registerBinding ( element, TreeBodyBinding );
}