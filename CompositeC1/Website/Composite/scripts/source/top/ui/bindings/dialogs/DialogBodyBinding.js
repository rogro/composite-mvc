/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

DialogBodyBinding.prototype = new FlexBoxBinding;
DialogBodyBinding.prototype.constructor = DialogBodyBinding;
DialogBodyBinding.superclass = FlexBoxBinding.prototype;

/**
 * @class
 */
function DialogBodyBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "DialogBodyBinding" );
	
	/**
	 * @type {DialogBinding}
	 */
	this.panelBinding = null;
	
	/**
	 * @type {boolean}
	 */
	this.isVisible = true;
	
	/**
	 * @type {DialogBinding}
	 */
	this._dialogBinding = null;
}

/**
 * Identifies binding.
 */
DialogBodyBinding.prototype.toString = function () {

	return "[DialogBodyBinding]";
}

/**
 * @overloads {Binding#onBindingAttach}
 */
DialogBodyBinding.prototype.onBindingAttach = function () {

	DialogBodyBinding.superclass.onBindingAttach.call ( this );
	this._dialogBinding = UserInterface.getBinding ( this.bindingElement.parentNode );
}

/**
 * Get position as requested by the {@link ViewBinding} 
 * compensating for dialog border dimensions.
 * @return {Dimension}
 */
DialogBodyBinding.prototype.getPosition = function () {
	
	var pos = this._dialogBinding.getPosition ();
	
	return new Position ( 
		pos.x + this.offsetLeft + DialogBorderBinding.DIMENSION,
		pos.y + this.offsetTop
	);
}

/**
 * Get dimension as requested by the {@link ViewBinding} 
 * compensating for dialog border dimensions.
 * @return {Dimension}
 */
DialogBodyBinding.prototype.getDimension = function () {

	var dim = this.boxObject.getDimension ();
	
	return new Dimension (
		dim.w - 2 * DialogBorderBinding.DIMENSION,
		dim.h - DialogBorderBinding.DIMENSION
	);
}

/**
 * DialogBodyBinding factory.
 * @param {DOMDocument} ownerDocument
 * @return {DialogBodyBinding}
 */
DialogBodyBinding.newInstance = function ( ownerDocument ) {

	var element = DOMUtil.createElementNS ( Constants.NS_UI, "ui:dialogbody", ownerDocument );
	return UserInterface.registerBinding ( element, DialogBodyBinding );
}