/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

ToolBoxToolBarButtonBinding.prototype = new ToolBarButtonBinding;
ToolBoxToolBarButtonBinding.prototype.constructor = ToolBoxToolBarButtonBinding;
ToolBoxToolBarButtonBinding.superclass = ToolBarButtonBinding.prototype;

/**
 * @class
 */
function ToolBoxToolBarButtonBinding () {
	
	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "ToolBoxToolBarButtonBinding" );
	
	/**
	 * Associates viewdefinitions to perspective tags. 
	 * @type {Map<string><List<<ViewDefinition>>}
	 */
	this._views = new Map ();
	
	/**
	 * Avoid excessive popup building.
	 * @type {string}
	 */
	this._lastGeneratedPerspective = null;
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
ToolBoxToolBarButtonBinding.prototype.toString = function () {

	return "[ToolBoxToolBarButtonBinding]";
}

/**
 * @overloads {ButtonBinding#onBindingAttach}
 */
ToolBoxToolBarButtonBinding.prototype.onBindingAttach = function () {
	
	ToolBoxToolBarButtonBinding.superclass.onBindingAttach.call ( this );
	
	if ( System.hasActivePerspectives ) {
	
		this.subscribe ( BroadcastMessages.PERSPECTIVE_CHANGED );
		
		/**
		 * Scan ViewDefinitions, indexing definitions associated  
		 * to a perspective (by the "perspective" property no less).
		 */
		var views = this._views;
		for ( var handle in ViewDefinitions ) {
			var def = ViewDefinitions [ handle ];
			var key = def.perspective; 
			if ( key != null ) {
				if ( !views.has ( key )) {
					views.set ( key, new List ());
				}
				var list = views.get ( key );
				list.add ( def );
			}
		}
	} else {
		this.hide ();
	}
};

/**
 * Build popup when perspective changes. If no 
 * views are associated, the button will disable.
 * @implements {IBroadcastListener}
 * @param {string} broadcast
 * @param {object} arg
 */
ToolBoxToolBarButtonBinding.prototype.handleBroadcast = function (broadcast, arg) {

	ToolBoxToolBarButtonBinding.superclass.handleBroadcast.call(this, broadcast, arg);

	switch (broadcast) {
		case BroadcastMessages.PERSPECTIVE_CHANGED:
			var tag = arg;
			//

			if (tag != this._lastGeneratedPerspective) {

				this._lastGeneratedPerspective = tag;

				var popup = this.bindingWindow.bindingMap.toolboxpopupgroup;
				popup.empty();

				if (this._views.has(tag)) {
					var list = this._views.get(tag);
					list.each(function (def) {
						var item = popup.add(StageViewMenuItemBinding.newInstance(popup.bindingDocument));
						item.setType(MenuItemBinding.TYPE_CHECKBOX);
						item.setHandle(def.handle);
						item.setLabel(def.label);
						item.setImage(def.image);
						item.setToolTip(def.toolTip);
						item.attach();
					});
					popup.show();
				}
				else {
					popup.hide();
				}
			}
			break;
	}
}