/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

DockPanelBinding.prototype = new TabPanelBinding;
DockPanelBinding.prototype.constructor = DockPanelBinding;
DockPanelBinding.superclass = TabPanelBinding.prototype;

/*
 * Descendant bindings may dispatch this  
 * action to select the associated tab.
 */
DockPanelBinding.ACTION_FORCE_SELECT = "dockpanel force select";

/**
 * @class
 */
function DockPanelBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "DockPanelBinding" );
	
	/**
	 * The ViewBinding currently snapped to this  
	 * dockpanel. This property is set by the view. 
	 * @see {ViewBinding#snapToBinding}
	 * @type {ViewBinding}
	 */
	this.viewBinding = null;
	
	/*
	 * Returnable.
	 */
	return this;
}


/**
 * Identifies binding.
 */
DockPanelBinding.prototype.toString = function () {

	return "[DockPanelBinding]";
}

/**
 * When closing, dispose associated view.
 */
DockPanelBinding.prototype.onBindingDispose = function () {

	DockPanelBinding.superclass.onBindingDispose.call ( this );
	this.dispatchAction ( Binding.ACTION_DISPOSED );
}

/**
 * @overloads {TabPanelBinding#select}.
 * @param {boolean} isManaged If set to true, application focus will not be updated.
 */
DockPanelBinding.prototype.select = function ( isManaged ) {
	
	DockPanelBinding.superclass.select.call ( this, isManaged );
	this.dispatchAction ( Binding.ACTION_VISIBILITYCHANGED );
}

/**
 * @overloads {TabPanelBinding#unselect}.
 */
DockPanelBinding.prototype.unselect = function () {
	
	DockPanelBinding.superclass.unselect.call ( this );
	this.dispatchAction ( Binding.ACTION_VISIBILITYCHANGED );
}

/**
 * Action dispatched to be intercepted by the {@link ViewBinding}.
 * @implements {IFlexible} 
 */
DockPanelBinding.prototype.flex = function () {
	
	this.dispatchAction ( Binding.ACTION_DIMENSIONCHANGED );
} 


/**
 * Handle crawler. 
 * @implements {ICrawlerHandler}
 * @overloads {Binding#handleCrawler}
 * @param {Crawler} crawler
 */
DockPanelBinding.prototype.handleCrawler = function ( crawler ) {

	DockPanelBinding.superclass.handleCrawler.call ( this, crawler );
	
	/*
	 * Relay descending crawlers to view. The view has been rigged 
	 * up to return the crawler back here when it has been crawled.
	 * @see {ViewBinding#handleCrawler}
	 */
	if ( crawler.response == null ) {
		if ( crawler.type == NodeCrawler.TYPE_DESCENDING ) {
			if ( this.viewBinding != null ) {
				if ( crawler.id == FocusCrawler.ID ) {
					crawler.nextNode = this.viewBinding.bindingElement;
				}
			}
		}
	}
}

/**
 * DockPanelBinding factory.
 * @param {DOMDocument} ownerDocument
 * @return {DockPanelBinding}
 */
DockPanelBinding.newInstance = function ( ownerDocument ) {

	var element = DOMUtil.createElementNS ( Constants.NS_UI, "ui:dockpanel", ownerDocument );
	return UserInterface.registerBinding ( element, DockPanelBinding );
}