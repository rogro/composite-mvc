/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

ViewDefinitionPostBackDataDialogBinding.prototype = new PostBackDataDialogBinding;
ViewDefinitionPostBackDataDialogBinding.prototype.constructor = ViewDefinitionPostBackDataDialogBinding;
ViewDefinitionPostBackDataDialogBinding.superclass = PostBackDataDialogBinding.prototype;

/**
 * This fellow will clone a ViewDefinition while  
 * allowing user to modify search properties and more.
 */
function ViewDefinitionPostBackDataDialogBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "ViewDefinitionPostBackDataDialogBinding" );
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
ViewDefinitionPostBackDataDialogBinding.prototype.toString = function () {

	return "[ViewDefinitionPostBackDataDialogBinding]";
}

/**
 * Temporarliy modify the all-functions dialog definition.
 * @overloads {DataDialogBinding#fireCommand}
 */
ViewDefinitionPostBackDataDialogBinding.prototype.fireCommand = function () {

	var label = this.getProperty("dialoglabel");
	var search = this.getProperty("providersearch");
	var key = this.getProperty("providerkey");
	var handle = this.getProperty("handle");
	var selectedToken = this.getProperty("selectedtoken");

	if (handle != null) {

		var def = ViewDefinition.clone(
			handle,
			"Generated.ViewDefinition.Handle." + KeyMaster.getUniqueKey()
		);

		/*
		* Label
		*/
		if (label != null) {
			if (def.argument == null) {
				def.argument = {};
			}
			def.argument.label = label;
		}

		/*
		* Search
		*/
		if (search != null) {
			if (def.argument == null) {
				def.argument = {};
			}
			if (def.argument.nodes == null) {
				def.argument.nodes = [];
			}
			def.argument.nodes[0].search = search;
		}

		/*
		* Key
		*/
		if (key != null) {
			if (def.argument == null) {
				def.argument = {};
			}
			if (def.argument.nodes == null) {
				def.argument.nodes = [];
			}
			def.argument.nodes[0].key = key;
		}

		/*
		* Token
		*/
		if (selectedToken != null) {
			if (def.argument == null) {
				def.argument = {};
			}
			def.argument.selectedToken = selectedToken;
		}

		/*
		* Super
		*/
		ViewDefinitionPostBackDataDialogBinding.superclass.fireCommand.call(this, def);

	} else {
		throw "Attribute \"handle\" required.";
	}
};

/**
 * ViewDefinitionPostBackDataDialogBinding factory.
 * @param {DOMDocument} ownerDocument
 * @return {ViewDefinitionPostBackDataDialogBinding}
 */
ViewDefinitionPostBackDataDialogBinding.newInstance = function ( ownerDocument ) {

	var element = DOMUtil.createElementNS ( Constants.NS_UI, "ui:postbackdialog", ownerDocument );
	return UserInterface.registerBinding ( element, ViewDefinitionPostBackDataDialogBinding );
}