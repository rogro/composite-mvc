/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

StageDeckRootBinding.prototype = new RootBinding;
StageDeckRootBinding.prototype.constructor = StageDeckRootBinding;
StageDeckRootBinding.superclass = RootBinding.prototype;

/**
 * Default deck layout to be loaded from "templates" folder.
 * UPDATE: Remote loading has been disabled for now...
 */
StageDeckRootBinding.DEFAULT_TEMPLATE = "defaultstagedeck.xml";

/**
 * @class
 * The content of the deck is generated dynamically. Point being 
 * that we can easily persist deck layout between sessions. We 
 * still need to implement this feature, though. DISABLED NOW!
 */
function StageDeckRootBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "StageDeckRootBinding" );
}

/**
 * Identifies binding.
 */
StageDeckRootBinding.prototype.toString = function () {
	
	return "[StageDeckRootBinding]";
}

/**
 * @overloads {RootBinding#onBindingAttach}
 *
StageDeckRootBinding.prototype.onBindingAttach = function () {
	
	StageDeckRootBinding.superclass.onBindingAttach.call ( this );
	this._defaultLayout ();
}

/**
 * Setup default layout.
 *
StageDeckRootBinding.prototype._defaultLayout = function () {
	
	var markup = Templates.getTemplateElementText ( 
		StageDeckRootBinding.DEFAULT_TEMPLATE 
	)
	this.subTreeFromString ( markup );
}
*/