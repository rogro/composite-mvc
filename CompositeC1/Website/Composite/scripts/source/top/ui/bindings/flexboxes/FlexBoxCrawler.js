/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

FlexBoxCrawler.prototype = new Crawler;
FlexBoxCrawler.prototype.constructor = FlexBoxCrawler;
FlexBoxCrawler.superclass = Crawler.prototype;

FlexBoxCrawler.ID = "flexboxcrawler";
FlexBoxCrawler.MODE_FORCE = "force";
FlexBoxCrawler.MODE_NORMAL = "normal";

/**
 * @class
 * Crawler handles recursive flex.
 * @see {Binding#reflex}
 * @see {FlexBoxBinding.reflex}
 */
function FlexBoxCrawler () {
	
	/**
	 * Identifies the crawler.
	 * @type {string}
	 */
	this.id = FlexBoxCrawler.ID;
	
	/**
	 * Switching Crawler agressiveness.
	 * @type {string}
	 */
	this.mode = FlexBoxCrawler.MODE_NORMAL;
	
	/**
	 * This binding who instantiated the reflex action.
	 * @type {Binding}
	 */
	this.startBinding = null;
	
	/*
	 * Construction time again.
	 */
	this._construct ();
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * @overloads {Crawler#_construct} 
 */
FlexBoxCrawler.prototype._construct = function () {
	
	FlexBoxCrawler.superclass._construct.call ( this );

	var self = this;
	
	this.addFilter ( function ( element, list ) {
		
		var result = null;
		var binding = UserInterface.getBinding ( element );
		
		if ( Interfaces.isImplemented ( IFlexible, binding ) == true ) {
			
			/*
			 * Note that we don't check the value of property 
			 * isFlexible. That's because the flex method 
			 * may be used for other purposes than flexboxing.
			 */
			switch ( self.mode ) {		
				case FlexBoxCrawler.MODE_FORCE :
					list.add ( binding );
					break;
					
				case FlexBoxCrawler.MODE_NORMAL :
					if ( binding.isFlexSuspended == true ) {
						result = NodeCrawler.SKIP_CHILDREN;
					} else {
						list.add ( binding );
					}
					break;
			}
		}
		
		return result;
	});
}