/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

StageViewMenuItemBinding.prototype = new MenuItemBinding;
StageViewMenuItemBinding.prototype.constructor = StageViewMenuItemBinding;
StageViewMenuItemBinding.superclass = MenuItemBinding.prototype;

/**
 * @class
 */
function StageViewMenuItemBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "StageViewMenuItemBinding" );
	
	/**
	 * @type {string}
	 */
	this._handle = null;
}

/**
 * Identifies binding.
 */
StageViewMenuItemBinding.prototype.toString = function () {

	return "[StageViewMenuItemBinding]";
}

/**
 * @overloads {MenuItemBinding.#onBindingAttach}
 */
StageViewMenuItemBinding.prototype.onBindingAttach = function () {

	StageViewMenuItemBinding.superclass.onBindingAttach.call ( this );
	
	if ( this.type == MenuItemBinding.TYPE_CHECKBOX ) {
		this.subscribe ( BroadcastMessages.VIEW_OPENED );
		this.subscribe ( BroadcastMessages.VIEW_CLOSED );
		this.subscribe ( BroadcastMessages.STAGE_INITIALIZED );
	}
}

/**
 * @overloads {MenuItemBinding#buildDOMContent}
 */
StageViewMenuItemBinding.prototype.buildDOMContent = function () {
 	
	StageViewMenuItemBinding.superclass.buildDOMContent.call ( this );
	
	var handle = this.getProperty ( "handle" );
	if ( handle ) {
		
		this._handle = handle;
		
		if ( StageBinding.isViewOpen ( handle )) {
			if ( this.type == MenuItemBinding.TYPE_CHECKBOX ) {
				this.check ( true );
			}
		}
		this.oncommand = function () {
			var self = this;
			Application.lock ( self );
			setTimeout ( function () {
				StageBinding.handleViewPresentation ( handle );
				Application.unlock ( self );
			}, Client.hasTransitions ? Animation.DEFAULT_TIME : 0 );
		}
		
	} else {
		throw new Error ( "StageViewMenuItemBinding: missing handle" );
	}
}

/**
 * Set handle.
 * @param {string} handle
 */
StageViewMenuItemBinding.prototype.setHandle = function ( handle ) {
	
	this.setProperty ( "handle", handle );
}

/*
 * @implements {IBroadcastListener}
 * @param {string} message
 * @param {object} arg
 */
StageViewMenuItemBinding.prototype.handleBroadcast = function ( broadcast, arg ) {
	
	StageViewMenuItemBinding.superclass.handleBroadcast.call ( this, broadcast, arg );
	
	if ( this.type == MenuItemBinding.TYPE_CHECKBOX ) {
		switch ( broadcast ) {
			case BroadcastMessages.STAGE_INITIALIZED :
				if ( this.isChecked ) {
					this.fireCommand ();
				}
				break;
			case BroadcastMessages.VIEW_OPENED :
				if ( arg == this._handle ){
					this.check ( true );
				}
				break;
			case BroadcastMessages.VIEW_CLOSED :
				if ( arg == this._handle ){
					this.uncheck ( true );
				}
				break;
		}
	}
}

/**
 * StageViewMenuItemBinding factory.
 * @param {DOMDocument} ownerDocument
 * @return {StageViewMenuItemBinding}
 */
StageViewMenuItemBinding.newInstance = function ( ownerDocument ) {

	var element = DOMUtil.createElementNS ( Constants.NS_UI, "ui:menuitem", ownerDocument );
	UserInterface.registerBinding ( element, StageViewMenuItemBinding );
	return UserInterface.getBinding ( element );
}