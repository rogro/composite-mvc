/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

/**
 * Considered internal to this class.
 * @type {Map<string><VisualEditorFormattingConfiguration>}
 */
VisualEditorFormattingConfiguration._configurations = new Map ();

/**
 * Populated when the editor is first loaded, see below.
 * @type {HashMap<string><string>}
 */
VisualEditorFormattingConfiguration._options = null;

/**
 * Caching configurations to save a few server requests.
 * @param {string} formatconfig
 * @return {VisualEditorFormattingConfiguration}
 */
VisualEditorFormattingConfiguration.getConfiguration = function ( formatconfig ) {
	
	var configs = VisualEditorFormattingConfiguration._configurations;
	if ( !configs.has ( formatconfig )) {
		configs.set ( formatconfig, new VisualEditorFormattingConfiguration ());
	};
	return configs.get ( formatconfig );
};

/**
 * Get all POSSIBLE formatting options.
 */
VisualEditorFormattingConfiguration._getOptions = function () {
	
	if ( VisualEditorFormattingConfiguration._options == null ) {
		
		var p = "Composite.Web.VisualEditor";	
		VisualEditorFormattingConfiguration._options = {
				"p" 			: StringBundle.getString ( p, "FormatSelector.LabelParagraph" ),
				"address" 		: StringBundle.getString ( p, "FormatSelector.LabelAddress" ),
				"blockquote" 	: StringBundle.getString ( p, "FormatSelector.LabelBlockQuote" ),
				"div"			: StringBundle.getString ( p, "FormatSelector.LabelDivision" ),
				"h1"			: StringBundle.getString ( p, "FormatSelector.LabelHeading1" ),
				"h2"			: StringBundle.getString ( p, "FormatSelector.LabelHeading2" ),
				"h3"			: StringBundle.getString ( p, "FormatSelector.LabelHeading3" ),
				"h4"			: StringBundle.getString ( p, "FormatSelector.LabelHeading4" ),
				"h5"			: StringBundle.getString ( p, "FormatSelector.LabelHeading5" ),
				"h6"			: StringBundle.getString ( p, "FormatSelector.LabelHeading6" )
		};
	}
	
	return VisualEditorFormattingConfiguration._options;
}

/**
 * @class
 * @param {string} config
 */
function VisualEditorFormattingConfiguration ( config ) {
	
	/**
	 * @type {HashMap<string><string>}
	 */
	this._options = VisualEditorFormattingConfiguration._getOptions ();
}

/**
 * Get formatting.
 */
VisualEditorFormattingConfiguration.prototype.getFormattingOptions = function () {
	
	return this._options;
}