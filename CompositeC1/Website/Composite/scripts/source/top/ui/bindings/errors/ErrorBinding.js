/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

ErrorBinding.prototype = new Binding;
ErrorBinding.prototype.constructor = ErrorBinding;
ErrorBinding.superclass = Binding.prototype;

ErrorBinding.ACTION_INITIALIZE = "error initialize";

/**
 * Display error in BalloonBinding somewhere near a DataBinding.
 * @param {object} error A simple object with one property "text" (for now).
 * @param {IData} binding
 */
ErrorBinding.presentError = function ( error, binding ) {
	
	if ( Interfaces.isImplemented ( IData, binding ) == true ) {
		
		/*
		 * Analyze environment in order to determine whether or not we are 
		 * in a dialog, in which case another BalloonSetBinding is used.
		 * TODO: refactor with getAncestorBindingByType ( xxx, true )???
		 */
		var isDialog, action = binding.dispatchAction ( ErrorBinding.ACTION_INITIALIZE );
		if ( action && action.isConsumed ) {
			switch ( action.listener.constructor ) {
				case StageBinding :
					isDialog = false;
					break;
				case StageDialogBinding :
					isDialog = true;
					break;
			}
		}
		var balloonset = isDialog ? 
			top.app.bindingMap.dialogballoonset : 
			top.app.bindingMap.balloonset;
		var balloon = balloonset.add (
			BalloonBinding.newInstance ( top.app.document )
		);
		balloon.setLabel ( error.text );
		balloon.snapTo ( binding );
		balloon.attach ();
	}
}

/**
 * @class
 */
function ErrorBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "ErrorBinding" );
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
ErrorBinding.prototype.toString = function () {
	
	return "[ErrorBinding]";
}

/**
 * Notice that the binding disposes as soon as it attaches.
 * @overloads {Binding#onBindingAttach}
 */
ErrorBinding.prototype.onBindingAttach = function () {
	
	ErrorBinding.superclass.onBindingAttach.call ( this );
	
	var dataManager = this.bindingWindow.DataManager;
	var text = this.getProperty ( "text" );
	var name = this.getProperty ( "targetname" );
	
	var binding = dataManager.getDataBinding ( name );
	if ( binding ) {
		ErrorBinding.presentError ({
			text : text
		}, binding );
	} else {
		alert ( "ErrorBinding dysfunction: No such DataBinding!\n" + name );
		if ( name.indexOf ( "_" ) >-1 ) {
			alert ( "Name contaings '_' - replace with '$' ?" );
		}
	}
	this.dispose ();
}