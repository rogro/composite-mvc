/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

NullPostBackDataDialogSelectorBinding.prototype = new SelectorBinding;
NullPostBackDataDialogSelectorBinding.prototype.constructor = NullPostBackDataDialogSelectorBinding;
NullPostBackDataDialogSelectorBinding.superclass = SelectorBinding.prototype;

/**
 * @class
 */
function NullPostBackDataDialogSelectorBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "NullPostBackDataDialogSelectorBinding" );

	/**
	 * @type {NullPostBackDataDialogBinding}
	 */
	this.master = null;
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
NullPostBackDataDialogSelectorBinding.prototype.toString = function () {

	return "[NullPostBackDataDialogSelectorBinding]";
}

/**
 * @overloads {SelectorBinding#select}
 * @param {MenuItemBinding} itemBinding
 * @param {boolean} isActionBlocked True while initializing to block action.
 * @return {boolean} True if something (new) was selected
 */
NullPostBackDataDialogSelectorBinding.prototype.select = function ( itemBinding, isActionBlocked ) {

	if ( NullPostBackDataDialogSelectorBinding.superclass.select.call ( this, itemBinding, true )) {
		this._buttonBinding.setImage ( null );
		this._updateImageLayout ();
		if ( this._selectionValue == NullPostBackDataDialogBinding.VALUE_SELECTED ) {
			if ( this.master.getValue () != null ) {
				
				//this._buttonBinding.setLabel ( this.master.getLabel ());
			}
		}
	}
}

NullPostBackDataDialogSelectorBinding.prototype.setLabel = function ( label ) {
	
	this._buttonBinding.setLabel ( label );
}

NullPostBackDataDialogSelectorBinding.prototype.setToolTip = function ( tooltip ) {
	
	this._buttonBinding.setToolTip ( tooltip );
}

/**
 * @implements {IActionListener}
 * @overloads {SelectorBinding#handleAction}
 * @param {Action} action
 */
NullPostBackDataDialogSelectorBinding.prototype.handleAction = function ( action ) {
	
	NullPostBackDataDialogSelectorBinding.superclass.handleAction.call ( this, action )
	
	switch ( action.type ) {
		case MenuItemBinding.ACTION_COMMAND :
			var menuitem = action.target;
			var master = this.master;
			if ( menuitem.selectionValue == NullPostBackDataDialogBinding.VALUE_SELECTED ) {
				this.setLabel ( menuitem.getLabel ());
				setTimeout ( function () {
					master.action ();
				}, 0 );
			} else {
			    if (master.getValue()) {
			        master.dirty();
			    }
			    master.setValue("");
			}
			break;
	}
}

/**
 * @overwrites {SelectorBinding#manifest}
 */
NullPostBackDataDialogSelectorBinding.prototype.manifest = function () {
	
	// do nothing
}

/**
 * NullPostBackDataDialogSelectorBinding factory.
 * @param {DOMDocument} ownerDocument
 * @return {NullPostBackDataDialogSelectorBinding}
 */
NullPostBackDataDialogSelectorBinding.newInstance = function ( ownerDocument ) {

	var element = DOMUtil.createElementNS ( Constants.NS_UI, "ui:selector", ownerDocument );
	return UserInterface.registerBinding ( element, NullPostBackDataDialogSelectorBinding );
}