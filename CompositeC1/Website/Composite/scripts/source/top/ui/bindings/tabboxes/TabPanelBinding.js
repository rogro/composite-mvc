/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

TabPanelBinding.prototype = new Binding;
TabPanelBinding.prototype.constructor = TabPanelBinding;
TabPanelBinding.superclass = Binding.prototype;

function TabPanelBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "TabPanelBinding" );
	
	/**
	 * This property is set by the TabBoxBinding
	 * @type {string}
	 */
	this.tabboxkey = null;
	
	/**
	 * @type {boolean}
	 */
	this.isVisible = false;
	
	/**
	 * @type {IFocusable}
	 */
	this._focusedBinding = null;

	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
TabPanelBinding.prototype.toString = function () {

	return "[TabPanelBinding]";
}

/**
 * Dispatching action to initialize containing tabboxbinding.
 * Overloads {Binding#onBindingAttach}
 */
TabPanelBinding.prototype.onBindingAttach = function () {

	TabPanelBinding.superclass.onBindingAttach.call ( this );
	this.dispatchAction ( Binding.ACTION_ATTACHED );
	this.addActionListener ( BalloonBinding.ACTION_INITIALIZE );
}

/**
 * Select tabpanel.
 * @param {boolean} isManaged If set to true, application focus will not be updated.
 */
TabPanelBinding.prototype.select = function ( isManaged ) {
	
	if ( !this.isSelected ) {
		
		if ( this.isLazy ) {
			
			this.wakeUp ( "select" );
			
		} else {
		
			this.isSelected = true;
			this.isVisible = true;
			this.bindingElement.style.position = "static";
			
			/*
			 * Start flex iterator?
			 */
			this._invokeManagedRecursiveFlex ();
			
			/*
			 * TODO: Even if seleted, focus shift should only be invoked   
			 * when no VISIBLE binding inside the dock has the current focus. 
			 */
			if ( isManaged != true ) {
				this.dispatchAction ( FocusBinding.ACTION_FOCUS );
			}
		}
	
	}
}

/**
 * Unselect tabpanel.
 */
TabPanelBinding.prototype.unselect = function () {
	
	if ( this.isSelected ) {
		
		/*
		 * Blur any focused binding within the tabpanel.
		 */
		this.dispatchAction ( FocusBinding.ACTION_BLUR );
		
		this.isSelected = false;
		this.isVisible = false;
		
		this.bindingElement.style.position = "absolute";
	}
}

/**
 * Invoke recursive flex ONLY IF tabpanels height 
 * has changed since last show (or when first shown).
 * UPDATE: THIS HAS BEEN DISABLED
 */
TabPanelBinding.prototype._invokeManagedRecursiveFlex = function () {
	
	this.reflex ( true );
	
	/*
	 * There's an issue here with lazy panels waking up. 
	 * It may be nescessary simply to reflex!
	 *
	if ( this.isAttached == true ) {
		
		var tabpanels = UserInterface.getBinding ( this.bindingElement.parentNode );
		if ( tabpanels.hasDimensionsChanged ()) {
			this.reflex ();
		}
	}
	*/
}

/**
 * @implements {IActionListener}
 * @overloads {Binding#handleAction}
 * @param {Action} action
 */
TabPanelBinding.prototype.handleAction = function ( action ) {
	
	TabPanelBinding.superclass.handleAction.call ( this, action );
	
	var binding = action.target;
	
	switch ( action.type ) {
		case BalloonBinding.ACTION_INITIALIZE :
			action.consume ();
			break;
	}
}

/**
 * TabPanelBinding factory.
 * @param {DOMDocument} ownerDocument
 * @return {TabPanelBinding}
 */
TabPanelBinding.newInstance = function ( ownerDocument ) {
	
	var tabpanel = DOMUtil.createElementNS ( Constants.NS_UI, "ui:tabpanel", ownerDocument );
	UserInterface.registerBinding ( tabpanel, TabPanelBinding );
	return UserInterface.getBinding ( tabpanel );
}