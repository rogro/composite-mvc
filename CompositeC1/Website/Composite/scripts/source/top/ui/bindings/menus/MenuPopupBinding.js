/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

MenuPopupBinding.prototype = new PopupBinding;
MenuPopupBinding.prototype.constructor = MenuPopupBinding;
MenuPopupBinding.superclass = PopupBinding.prototype;

function MenuPopupBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "MenuPopupBinding" );
	
	/*
	 * Returnable.
	 */
	return this;
}

/**
 * Identifies binding.
 */
MenuPopupBinding.prototype.toString = function () {

	return "[MenuPopupBinding]";
}

/**
 * Menupopups are always positioned in a local coordinate space.
 * @overwrites {PopupBinding#_getElementPosition}.
 * @param {DOMElement} element
 * @return {Point}
 */
MenuPopupBinding.prototype._getElementPosition = function ( element ) {

	return new Point ( element.offsetLeft, 0 );
}

/**
 * MenuPopupBinding factory.
 * @param {DOMDocument} ownerDocument
 * @return {MenuPopupBinding}
 */
MenuPopupBinding.newInstance = function ( ownerDocument ) {

	var element = DOMUtil.createElementNS ( Constants.NS_UI, "ui:menupopup", ownerDocument );
	return UserInterface.registerBinding ( element, MenuPopupBinding );
}