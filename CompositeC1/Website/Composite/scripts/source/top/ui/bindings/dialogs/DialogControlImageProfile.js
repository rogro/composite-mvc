/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

DialogControlImageProfile.prototype = new ControlImageProfile;
DialogControlImageProfile.prototype.constructor = DialogControlImageProfile;
DialogControlImageProfile.superclass = ControlImageProfile.prototype;

var os = Client.isVista ? "vista/" : ( !Client.isWindows ? "osx/" : "" ); // HACK!

DialogControlImageProfile.IMAGE_MINIMIZE = "${root}/skins/system/controls/" + os + "control-minimize-${string}.png";
DialogControlImageProfile.IMAGE_MAXIMIZE = "${root}/skins/system/controls/" + os + "control-maximize-${string}.png";
DialogControlImageProfile.IMAGE_RESTORE = "${root}/skins/system/controls/" + os + "control-restore-${string}.png"; 
DialogControlImageProfile.IMAGE_CLOSE = "${root}/skins/system/controls/" + os + "control-close-${string}.png";

/**
 * This functionality can be implemented in pure CSS when we ditch IE6.0!
 * @param {ControlBinding} binding
 */
function DialogControlImageProfile ( binding ) {
	
	this.binding = binding;
}