/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

DockTabPopupBinding.prototype = new PopupBinding;
DockTabPopupBinding.prototype.constructor = DockTabPopupBinding;
DockTabPopupBinding.superclass = PopupBinding.prototype;

DockTabPopupBinding.CMD_RESTORE 		= "restore";
DockTabPopupBinding.CMD_MINIMIZE 		= "minimize";
DockTabPopupBinding.CMD_MAXIMIZE 		= "maximize";
DockTabPopupBinding.CMD_REFRESH 		= "refreshview";
DockTabPopupBinding.CMD_MAKEDIRTY 		= "makedirty";
DockTabPopupBinding.CMD_CLOSETAB 		= "closetab";
DockTabPopupBinding.CMD_CLOSEOTHERS 	= "closeothers";
DockTabPopupBinding.CMD_CLOSEALL 		= "closeall";
DockTabPopupBinding.CMD_VIEWSOURCE 		= "viewsource";
DockTabPopupBinding.CMD_VIEWGENERATED 	= "viewgenerated";
DockTabPopupBinding.CMD_VIEWSERIALIZED 	= "viewserialized";

function DockTabPopupBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "DockTabPopupBinding" );
}

/**
 * Identifies binding.
 */
DockTabPopupBinding.prototype.toString = function () {

	return "[DockTabPopupBinding]";
}

/**
 * @overloads {PopupBinding#onBindingAttach}
 */
DockTabPopupBinding.prototype.onBindingAttach = function () {
	
	DockTabPopupBinding.superclass.onBindingAttach.call ( this );
	this._indexMenuContent ();
}