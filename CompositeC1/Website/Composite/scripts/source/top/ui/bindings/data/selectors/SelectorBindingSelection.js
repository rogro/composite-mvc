/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

/**
 * @class
 * This is *not* a binding! It is simply an object that 
 * can be fed to a SelectorBinding in order to populate it.
 * @implements {IData}
 * @param {string} label
 * @param {object} value
 * @param {boolean} isSelected
 * @param {ImageProfile} imageProfile
 */ 
function SelectorBindingSelection ( label, value, isSelected, imageProfile, tooltip ) {
	
	this._init ( label, value, isSelected, imageProfile, tooltip );
}

SelectorBindingSelection.prototype = {
	
	/**
	 * The visible label on the selection.
	 * @type {string}
	 */
	label : null,
	
	/**
	 * Value is stored internally as a string. You can extract the typecasted value, 
	 * dependant on the selectors type property, by using the getResult method.
	 * @see {SelectorBinding#getResult}
	 * @type {string}
	 */
	value : null,
	
	/**
	 * @type {String}
	 */
	tooltip : null,
	
	/**
	 * If set to true, the selection will be selected.
	 * @type {boolean}
	 */
	isSelected : null,
	
	/**
	 * Das image profile.
	 * @type {ImageProfile}
	 */
	imageProfile : null,
	
	/**
	 * This property is set by the SelectorBinding when selection is resolved.
	 * @type {MenuItemBinding}
	 */
	menuItemBinding : null,
	
	/**
	 * Initialize all of the above.
	 * @param {string} label
	 * @param {object} value
	 * @param {boolean} isSelected
	 * @param {ImageProfile} imageProfile
	 * @param {String} tooltip
	 */
	_init : function ( label, value, isSelected, imageProfile, tooltip ) {
		
		if ( label != null ) {
			this.label = String ( label );
		}
		if ( value != null ) {
			this.value = String ( value );
		}
		if ( imageProfile != null ) {
			this.imageProfile = imageProfile;
		}
		if ( tooltip != null ) {
			this.tooltip = tooltip;
		}
		this.isSelected = isSelected ? true : false;
	}
}