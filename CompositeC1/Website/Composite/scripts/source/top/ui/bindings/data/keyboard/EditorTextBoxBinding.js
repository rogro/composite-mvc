/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

EditorTextBoxBinding.prototype = new TextBoxBinding;
EditorTextBoxBinding.prototype.constructor = EditorTextBoxBinding;
EditorTextBoxBinding.superclass = TextBoxBinding.prototype;

/**
 * @class
 * Tab indent, tab preservation, no soft text wrap. Because of extremely different implementations, 
 * this has been split into two different bindings. Note that the box must be placed inside an 
 * <ui:flexbox> element to maximize it's sreen estate.
 * @see {MozEditorTextBoxBinding}
 * @see {IEEditorTextBoxBinding}
 */
function EditorTextBoxBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "EditorTextBoxBinding" );
	
	/**
	 * @overloads {TextBoxBinding#_hasWordWrap}
	 * @type {boolean}
	 */
	this._hasWordWrap = false;
}

/**
 * Identifies binding.
 */
EditorTextBoxBinding.prototype.toString = function () {
	
	return "[EditorTextBoxBinding]";
}

/**
 * @implements {IEventListener}
 * @overwrites {TextBoxBinding#handleEvent}
 * @param {Event} e
 */
EditorTextBoxBinding.prototype.handleEvent = function ( e ) {
	
	if ( this.isFocusable == true ) {
		switch ( e.type ) {
			
			case DOMEvents.FOCUS :
			case DOMEvents.BLUR :
				this._handleFocusAndBlur ( e.type == DOMEvents.FOCUS );
				break;
				
			case DOMEvents.KEYDOWN :
				this._handleKeyEvent ( e );
				break;
		}
	}
}

/**
 * @param {KeyEvent} e
 * @overloads {TextBoxBinding#_handleKeyEvent}
 */
EditorTextBoxBinding.prototype._handleKeyEvent = function ( e ) {
	
	switch ( e.keyCode ) {
		
		/*
		 * Handle TAB.
		 */
		case KeyEventCodes.VK_TAB :
			this._handleTabKey ( e.shiftKey );
			DOMEvents.stopPropagation ( e );
			DOMEvents.preventDefault ( e );
			break;
		
		/*
		 * Handle ENTER.
		 */
		case KeyEventCodes.VK_ENTER :
			this._handleEnterKey ();
			DOMEvents.stopPropagation ( e );
			DOMEvents.preventDefault ( e );
			break;
			
		/*
		 * Prevent ESC from reverting new value to original 
		 * value. This is default behavior in Explorer only. 
		 * We create input with JS, so our original is empty.
		 * TODO: This should also escape keyboard nav from editor!
		 */
		case KeyEventCodes.VK_ESCAPE :
			DOMEvents.preventDefault ( e );
			break;
	}
}

/**
 * Subclass must define this.
 * @param {boolean} isReverse
 */
EditorTextBoxBinding.prototype._handleTabKey = Binding.ABSTRACT_METHOD;

/**
 * Subclass must define this.
 */
EditorTextBoxBinding.prototype._handleEnterKey = Binding.ABSTRACT_METHOD;