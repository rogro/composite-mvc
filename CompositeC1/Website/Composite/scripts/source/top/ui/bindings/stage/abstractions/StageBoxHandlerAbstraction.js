/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

/**
 * This class is never instantiated, we just need to borrow it's methods for other 
 * classes (as if this class belonged in the inheritance chain of these classes).
 * This hack is very javascriptish, but it helps us not to copypaste some code.
 * @see {StageDeckBinding}
 * @see {StageBinding}
 */
function StageBoxHandlerAbstraction () {

	/**
	 * @type {boolean}
	 */
	this.isSubPanelMaximized = false;
}

/**
 * Registering listeners for maximization and unmaximization of panels.
 */
StageBoxHandlerAbstraction.onBindingRegister = function () {

	this.addActionListener ( ControlBoxBinding.ACTION_MAXIMIZE, this );
	this.addActionListener ( ControlBoxBinding.ACTION_NORMALIZE, this );
	this.addActionListener ( StageBoxAbstraction.ACTION_HIDDENSTUFF_UPDATED, this );
	this.addActionListener ( StageSplitPanelBinding.ACTION_LAYOUTUPDATE, this );
}

/**
 * @param {Action} action
 */
StageBoxHandlerAbstraction.handleAction = function ( action ) {
	
	var binding = action.target;
	
	switch ( action.type ) {
		case ControlBoxBinding.ACTION_MAXIMIZE :
		case ControlBoxBinding.ACTION_NORMALIZE :
			if ( binding instanceof StageSplitPanelBinding ) {
				StageBoxHandlerAbstraction.handleControlBoxAction.call ( this, action );
				action.consume ();
			}
			break;
		
		case StageBoxAbstraction.ACTION_HIDDENSTUFF_UPDATED :
			if ( this.isSubPanelMaximized ) {
				/*
				var filter = StageBoxHandlerAbstraction.unMaximizeFilter;
				*/
				this.iterateContainedStageBoxBindings ( StageCrawler.MODE_UNMAXIMIZE );
				this.isSubPanelMaximized = false;
			}
			action.consume ();
			break;
		
		/*
		 * see {StageBinding#handleAction}
		 */
		case StageSplitPanelBinding.ACTION_LAYOUTUPDATE :
		 	break;
	}
}

/**
 * Handle {@link ControlBoxBinding} actions.
 * @param {Action} action
 * @return {function}
 */
StageBoxHandlerAbstraction.handleControlBoxAction = function ( action ) {
	
	var mode = null;
	
	switch ( action.type ) {
		case ControlBoxBinding.ACTION_MAXIMIZE :
			if ( !this.isSubPanelMaximized ) {
				mode = StageCrawler.MODE_MAXIMIZE;
				this.isSubPanelMaximized = true;
			}
			break;
		case ControlBoxBinding.ACTION_NORMALIZE :
			if ( this.isSubPanelMaximized ) {
				mode = StageCrawler.MODE_UNMAXIMIZE;
				this.isSubPanelMaximized = false;
			}
			break;
	}
	if ( mode != null ) {
		this.iterateContainedStageBoxBindings ( mode );
	}
}