/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

TreePositionIndicatorBinding.prototype = new Binding;
TreePositionIndicatorBinding.prototype.constructor = TreePositionIndicatorBinding;
TreePositionIndicatorBinding.superclass = Binding.prototype;

/**
 * @class
 */
function TreePositionIndicatorBinding () {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "TreePositionIndicatorBinding" );
	
	/**
	 * TODO: REFACTOR TO POINT!
	 * @type {object}
	 */
	this._geometry = {
		x : 0,
		y : 0
	}
}

/**
 * Identifies binding.
 */
TreePositionIndicatorBinding.prototype.toString = function () {
	
	return "[TreePositionIndicatorBinding]";
}

/**
 * @overloads {Binding#onBindingAttach}
 */
TreePositionIndicatorBinding.prototype.onBindingAttach = function () {
	
	TreePositionIndicatorBinding.superclass.onBindingAttach.call ( this );
	this.hide ();
}

/**
 * @param {Point} point
 */
TreePositionIndicatorBinding.prototype.setPosition = function ( point ) {
	
	this.bindingElement.style.left = point.x + "px";
	this.bindingElement.style.top = point.y + "px";
	
	this._geometry.x = point.x;
	this._geometry.y = point.y;
}

/**
 * @return {Point}
 */
TreePositionIndicatorBinding.prototype.getPosition = function () {

	return new Point ( 
		this._geometry.x,
		this._geometry.y
	);
}

/**
 * TreePositionIndicatorBinding factory.
 * @param {DOMDocument} ownerDocument
 * @return {TreePositionIndicatorBinding}
 */
TreePositionIndicatorBinding.newInstance = function ( ownerDocument ) {

	var element = DOMUtil.createElementNS ( Constants.NS_UI, "ui:treepositionindicator", ownerDocument );
	return UserInterface.registerBinding ( element, TreePositionIndicatorBinding );
}