/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

StringDataDialogBinding.prototype = new DataDialogBinding;
StringDataDialogBinding.prototype.constructor = StringDataDialogBinding;
StringDataDialogBinding.superclass = DataDialogBinding.prototype;

/**
 * Engineered to carry a single string value.
 * @deprecated
 */
function StringDataDialogBinding () {
    
    /**
     * @type {DOMElement}
     */
    this.input = null;
    
    /*
     * Returnable.
     */
    return this;
}

/**
 * Overloads {@link Binding#onBindingAttach}
 */
StringDataDialogBinding.prototype.onBindingAttach = function () {
	
	StringDataDialogBinding.superclass.onBindingAttach.call ( this ); 
	
	this.input = this.getChildElementsByLocalName ( "input" ).getFirst ();
	
	// alert ( "DEPRECATED: " + this.toString () + ": " + input.name );
	
	if ( this.input != null ) {
		
		/**
		 * Special label setup.
		 */
	    this._setLabelSpeialSetup ();
	    
	    /*
	     * Construct a custom dialog handler. Cannot be done in constructor 
	     * (as first implemented) because we need to subclass.
	     */
	    var self = this;
	    
	    /**
	     * @overwrites {DataDialogBinding#_handler}
		 * @type {IDialogResponseHandler}
		 */
	    this._handler = {
	    	handleDialogResponse : function ( response, result ) {
	    		if ( response == Dialog.RESPONSE_ACCEPT ) {
	    			self._onDialogAccept ( result );
	    		} else {
	    			self._onDialogCancel ();
	    		}
	    	}
	    }
	} else {
		throw "Missing input element!";
	}
}

/**
 * @param {WHAT?} result THIS CAN BE A LIST (TREESELECTORS) PLEAR CLEAR THIS UP!
 * @returns
 */
StringDataDialogBinding.prototype._onDialogAccept = function ( result ) {
	
	result = new String ( result );
	if ( this.input.value != result ) {
		this.dispatchAction( Binding.ACTION_DIRTY );
	}
	this.input.value = result; // SHOULD THIS BE result.getFirst () ???
	this._setLabelSpeialSetup ();
	if ( this.getCallBackID () != null ) {
		var self = this;
	    setTimeout ( function () {
	    	self.dispatchAction( PageBinding.ACTION_DOPOSTBACK );
		}, 0 );
	}
};

StringDataDialogBinding.prototype._onDialogCancel = function () {};

/**
 * Get that URL. This is highly specialized for backend purposes. 
 * URL parameters have been embedded in the hidden input element.
 * @overwrites {DataDialogBinding#getURL}
 */
StringDataDialogBinding.prototype.getURL = function () {

	var url = this.getProperty ( "url" );
	var suf = encodeURIComponent ( this.input.value );
	return new String ( url + suf );
}

/**
 * @overwrites {DataDialogBinding#manifest}
 * @implements {IData}
 */
StringDataDialogBinding.prototype.manifest = function () {
	
	// do nothing
};

/**
 * @implements {IData}
 * @return {string}
 */
StringDataDialogBinding.prototype.setValue = function ( value ) {
	
	this.input.value = value;
};

/**
 * @overwrites {DataDialogBinding#getValue}
 * @implements {IData}
 * @return {string}
 */
StringDataDialogBinding.prototype.getValue = function () {
	
	return this.input.value;
};

/**
 * Special label setup.
 */
StringDataDialogBinding.prototype._setLabelSpeialSetup = function () {

    if ( this.input.value == "" && this.getProperty ( "label-onempty" ) ) {
        this._buttonBinding.setLabel ( this.getProperty ( "label-onempty" ) );
    } else {
        this._buttonBinding.setLabel ( this.getProperty ( "label" ) );
    }
};