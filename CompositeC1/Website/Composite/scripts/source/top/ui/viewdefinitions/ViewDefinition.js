/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

/**
 * @type {string}
 */
ViewDefinition.DEFAULT_URL = "${root}/blank.aspx";

/**
 * Clone a ViewDefinition, assigning a new handle.
 * @param {String} handle
 * @param {String} newhandle
 * @return {ViewDefinition}
 */
ViewDefinition.clone = function (handle, newhandle) {

	var result = null;
	var proto = ViewDefinitions[handle];

	if (proto.isMutable) {

		var impl = null;
		if (proto instanceof DialogViewDefinition) {
			impl = DialogViewDefinition;
		} else {
			impl = HostedViewDefinition;
		}
		if (newhandle != null && impl != null) {
			var def = new impl();
			for (var prop in proto) {
				def[prop] = ViewDefinition.cloneProperty(proto[prop]);
			}
			def.handle = newhandle;
			result = def;
		} else {
			throw "Cannot clone without newhandle";
		}
	} else {
		throw "Cannot clone non-mutable definition";
	}
	return result;
}

/**
* Clone .
* @param {Object} propertye
* @return {Object
*/
ViewDefinition.cloneProperty = function (object) {
	if (null == object) return object;

	if (typeof object === 'object') {
		var result = (object.constructor === Array)?[]:{};
		for (var prop in object) {
			result[prop] = ViewDefinition.cloneProperty(object[prop]);
		}
		return result;
	}
	return object;
}


/**
 * @class
 * Don't construct this fellow manually, please subclass first.
 */
function ViewDefinition () {}

ViewDefinition.prototype = {
	
	/**
	 * The URL to display.
	 * @type {string}
	 */
	url : ViewDefinition.DEFAULT_URL,
	
	/**
	 * Served to PageBinding.
	 * @see {PageBinding#setPageArgument}
	 * @type {object}
	 */
	argument : null,
	
	/**
	 * Backend handle (ElementKey).
	 * @type {string}
	 */
	handle : null,
	
	/**
	 * May associate the definition to a tree item.
	 * @type {string}
	 */
	entityToken : null,
	
	/**
	 * Backend flowhandle.
	 * @type {string}
	 */
	flowHandle : null,
	
	/**
	 * The label.
	 * @type {string}
	 */
	label : null,
	
	/**
	 * The image URL.
	 * @type {string}
	 */
	image : null,
	
	/**
	 * The tooltip.
	 * @type {string}
	 */
	toolTip : null
}