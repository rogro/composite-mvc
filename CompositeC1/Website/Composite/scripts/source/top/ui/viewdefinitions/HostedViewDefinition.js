/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

HostedViewDefinition.prototype = new ViewDefinition;
HostedViewDefinition.prototype.constructor = HostedViewDefinition;
HostedViewDefinition.superclass = ViewDefinition.prototype;
HostedViewDefinition.POSTBACK_URL = "${root}/postback.aspx";

/**
 * @class 
 * @param {object} arg Constructor argument properties maps directly into class properties.
 */
function HostedViewDefinition ( arg ) {
	
	/**
	 * The dock default position.
	 * @type {object}
	 */
	this.position = DockBinding.MAIN;
	
	/**
	 * Associates the View to a given perspective. This property matches 
	 * the "TagValue" property of the ClientElement objects sent from server.
	 * @type {string}
	 */
	this.perspective = null;
	
	/**
	 * @overwrites {ViewDefinition#entityToken}
	 * @type {string}
	 */
	this.entityToken = null;
	
	/**
	 * @overwrites {ViewDefinition#label}
	 * @type {string}
	 */
	this.label = null;
	
	/**
	 * @overwrites {ViewDefinition#image}
	 * @type {string}
	 */
	this.image = null;
	
	/*
	 * Initialize all of the above from argument.
	 */
	if ( arg ) {
		for ( var prop in arg ) {
			if ( this [ prop ] || this.prop == null ) {
				this [ prop ] = arg [ prop ];
				if ( this.url ) {
					this.url = Resolver.resolve ( this.url );
				}
			} else {
				throw "Property not recognized: " + prop;
			}
		}
	}
}