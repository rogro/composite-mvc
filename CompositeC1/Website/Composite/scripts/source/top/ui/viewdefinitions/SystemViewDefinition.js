/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

SystemViewDefinition.prototype = new ViewDefinition;
SystemViewDefinition.prototype.constructor = SystemViewDefinition;
SystemViewDefinition.superclass = ViewDefinition.prototype;
SystemViewDefinition.DEFAULT_URL = "${root}/content/views/systemview/systemview.aspx";

/**
 * @class
 * @param {SystemNode} node
 */
function SystemViewDefinition ( node ) {
	
	/**
	 * Unique for system viewdefinitions.
	 * @type {SystemNode}
	 */
	this.node = node;
	
	/**
	 * Served for the {@link SystemPageBinding}.
	 * @overwrites {ViewDefinition#argument}
	 * @type {SystemNode}
	 */
	this.argument = node;
	
	/**
	 * All systemviews start with the same URL. The content is later
	 * generated on basis of the the SystemNode supplied in constructor.
	 * @overwrites {ViewDefinition#url}
	 * @type {string}	 
	 */
	this.url = SystemViewDefinition.DEFAULT_URL;
	
	/**
	 * @type {string}
	 */
	this.handle	= node.getHandle ();
	
	/**
	 * @type {string}
	 */
	this.label = node.getLabel ();
	
	/** 
	 * TODO: are we using this?
	 * @type {string}
	 */
	this.image = node.getImageProfile ().getDefaultImage ();
	
	/**
	 * @type {string}
	 */
	this.toolTip = node.getToolTip ();
}