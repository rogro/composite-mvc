/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

/**
 * @class
 * SystemNode list
 * Save handlers
 */
function SystemNodeList() {

	this._entityTokens = new List([]);
	return this;
}

/**
* Clear nodes.
*/
SystemNodeList.prototype.clear = function () {
	this._entityTokens.clear();
};

/**
* Add node
* @param {SystenNode} node
*/
SystemNodeList.prototype.add = function (node) {
	if (node.getEntityToken) {
		var entityToken = node.getEntityToken();
		this._entityTokens.add(entityToken);
	}
};

/**
* Does list contain SystemNode?
* @param {string} key
*/
SystemNodeList.prototype.has = function (node) {
	if (node.getEntityToken) {
		var entityToken = node.getEntityToken();
		return this._entityTokens.has(entityToken);
	}
	return false;
};

/**
* Return entityTokens
*/
SystemNodeList.prototype.getEntityTokens = function () {
	return this._entityTokens.copy();
};
