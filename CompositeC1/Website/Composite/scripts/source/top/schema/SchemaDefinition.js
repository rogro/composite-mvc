/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

/**
 * TODO: place this around here?
 */
SchemaDefinition.TYPE_XML_DOCUMENT = "xmldocument";


/**
 * @class
 * @param {DOMElement} element
 */
function SchemaDefinition ( element ) {

	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "SchemaDefinition" );

	/** 
	 * @type {boolean} 
	 */
	this.isRequired	= null;
	
	/** 
	 * @type {string} 
	 */
	this.type = null;
	
	/*
	 * Populate me! 
	 */
	this._parse ( element );
}

/**
 * @param {DOMElement} element
 * @private
 */
SchemaDefinition.prototype._parse = function ( element ) {
	
	var min 	= element.getAttribute ( "minOccurs" );
	var max 	= element.getAttribute ( "maxOccurs" );
	var type	= element.getAttribute ( "type" );
	
	this.name = element.getAttribute ( "name" );
	this.isRequired	= min != "0";
	
	if ( type ) {
	
		var split	= type.split ( ":" );
		var sort	= split [ 0 ];
		var typedef	= split [ 1 ];
		
		this.isSimpleValue  = sort != "tns";
		this.type 			= typedef;	
	
		//alert ( "OK\n" + DOMSerializer.serialize ( element, true ));
	
	} else {
		
		/* 
		 * TODO: rewrite to xpath, fetch a resolver somehow...
		 */
		var elm = element.getElementsByTagName ( "*" ).item ( 0 );
		if ( elm && DOMUtil.getLocalName ( elm ) == "complexType" && elm.getAttribute ( "mixed" ) == "true" ) {
			elm = elm.getElementsByTagName ( "*" ).item ( 0 );
			if ( elm && DOMUtil.getLocalName ( elm ) == "sequence" ) {
				elm = elm.getElementsByTagName ( "*" ).item ( 0 );
				if ( DOMUtil.getLocalName ( elm ) == "any" ) {
					this.type = SchemaDefinition.TYPE_XML_DOCUMENT;
				}
			}
		}
	}
}