/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

SchemaSimpleType.prototype = new SchemaType;
SchemaSimpleType.prototype.constructor = SchemaSimpleType;
SchemaSimpleType.superclass = SchemaType.prototype;

/**
 * @class
 * @param {Schema} schema
 * @param {DOMElement} element
 * @throws Schema.notSupportedException 
 */
function SchemaSimpleType ( schema, element ) {
	
	/**
	 * @type {string}
	 */
	this.restrictionType = null;
	
	this._parse ( schema, element );
}

/**
 * TODO: Investigate what needs to be supported here besides enumerations.
 * @param {Schema} schema
 * @param {DOMElement} element
 * @throws Schema.notSupportedException 
 */
SchemaSimpleType.prototype._parse = function ( schema, element ) {
	
	var restriction = schema.resolve ( "s:restriction", element );
	if ( restriction ) {
		this.restrictionType = restriction.getAttribute ( "base" ).split ( ":" )[ 1 ];	
	} else {
		throw Schema.notSupportedException;
	}
}