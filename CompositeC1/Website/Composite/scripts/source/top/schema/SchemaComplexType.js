/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

SchemaComplexType.prototype = new SchemaType;
SchemaComplexType.prototype.constructor = SchemaComplexType;
SchemaComplexType.superclass = SchemaType.prototype;

/**
 * @param {Schema} schema
 * @param {DOMElement} element
 */
function SchemaComplexType ( schema, element ) {
	
	/** 
	 * @type {List} 
	 * @private
	 */
	this._definitions = new List ();
	this._parseListedDefinitions ( schema, element );
	
	/**
	 * TODO: Use schema structure instead of name? This could be very MS specific...
	 * @type {boolean}
	 */
	this.isArray = element.getAttribute ( "name" ).indexOf ( "ArrayOf" ) >-1;
}

/**
 * @param {Schema} schema
 * @param {DOMElement} element
 * @throws Schema.notSupportedException 
 * @private
 */
SchemaComplexType.prototype._parseListedDefinitions = function ( schema, element ) {

	var els = schema.resolveAll ( "s:sequence/s:element", element );
	
	if ( els.hasEntries ()) {
		while ( els.hasNext ()) {
			var el = els.getNext ();
			this._definitions.add ( 
				new SchemaDefinition ( el )
			);
			
			/*
			var name = el.getAttribute ( "name" );
			this [ name ] = new SchemaDefinition ( el );
			alert ( el.nodeName + ": " + name );
			*/
		}
	} else throw Schema.notSupportedException;	
}

/** 
 * @return {List} 
 */
SchemaComplexType.prototype.getListedDefinitions = function () {

	return this._definitions.copy ();
}