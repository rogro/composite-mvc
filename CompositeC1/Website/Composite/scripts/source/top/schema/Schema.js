/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

Schema.prototype = new XPathResolver;
Schema.prototype.constructor = Schema;
Schema.superclass = XPathResolver.prototype;

/**
 * @type {HashMap<string><string>}
 */
Schema.types = {
	STRING	: "string",
	INT		: "int",
	FLOAT	: "float",
	DOUBLE	: "double",
	BOOLEAN	: "boolean"
}

/**
 * @type {Error}
 */
Schema.notSupportedException = new Error ( 
	"Schema: Schema structure not supported!"
);

/**
 * @class
 * @param {DOMElement} element
 */
function Schema ( element ) {
	
	/**
	 * @type {SystemLogger}
	 */
	this.logger = SystemLogger.getLogger ( "Schema" );
	
	/**
	 * @type {HashMap<string><object>)
	 */
	this._map = this._parseSchema ( element );
}

/**
 * @param {DOMElement} element
 * @return {HashMmap<string><object>}
 */
Schema.prototype._parseSchema = function ( element ) {
	
	this.setNamespacePrefixResolver ({
		"wsdl"	: Constants.NS_WSDL,
		"soap"	: Constants.NS_SOAP,
		"s" 	: Constants.NS_SCHEMA
	});
	
	var result = {};
	var entry = null;
	var rules = this.resolveAll ( "s:*[@name]", element );
	
	while ( rules.hasNext ()) {	
		var rule = rules.getNext ();
		switch ( DOMUtil.getLocalName ( rule )) {
			case "element" :
				entry = new SchemaElementType ( this, rule );
				break;
			case "complexType" :
				entry = new SchemaComplexType ( this, rule );
				break;
			case "simpleType" :
				entry = new SchemaSimpleType ( this, rule );
				break;
		}
		result [ rule.getAttribute ( "name" )] = entry;
	};
	
	return result;
}

/**
 * @param {string} name
 * @return {SchemaType}
 */
Schema.prototype.lookup = function ( name ) {
	
	return this._map [ name ];
}
