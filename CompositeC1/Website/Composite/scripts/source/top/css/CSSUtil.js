/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

/**
 * Handy interface for messing what an elements classname. 
 */ 
function _CSSUtil () {}

_CSSUtil.prototype = {
	
	/**
	 * Get the current CSS classname of a DOM element.
	 * @param {DOMElement} element
	 * @return {string}
	 */
	_getCurrent : function ( element ) {
		var current = element.style ? element.className : element.getAttribute ( "class" );
		current = current ? current : "";
		return current;
	},
	
	/**
	 * Check for occurance of substring.
	 * @param {string} current
	 * @param {string} sub
	 * @return boolean
	 */
	_contains : function ( current, sub ) {
		return current.indexOf ( sub ) >-1;
	},
	
	/**
	 * Cumulative builder for whitespace-separated strings
	 * @param {string} current
	 * @param {string} sub
	 * @return {string}
	 */
	_attach : function ( current, sub ) {
		return current + ( current == "" ? "" : " " ) + sub;
	},
	
	/**
	 * Cumulative destroyer of whitespace-separated strings. 
	 * @param {string} current
	 * @param {string} sub
	 * @return {string}
	 */
	_detach : function ( current, sub ) {
		if ( this._contains ( current, " " + sub )) {
			sub = " " + sub;
		}
		return current.replace ( sub, "" );
	},
	
	/**
	 * Attach CSS classname to a DOM element.
	 * @param {DOMElement} element
	 * @param {string} classname
	 */
	attachClassName : function ( element, classname ) {
	
		if ( element.classList != null ) {
			if ( !element.classList.contains ( classname )) {
				element.classList.add ( classname );
			}
		} else {
			var current = this._getCurrent ( element );
			if ( !this._contains ( current, classname )) {
				current = this._attach ( current, classname );
			}
			if ( element.style != null ) {
				element.className = current;
			} else {
				element.setAttribute ( "class", current );
			}
		}
	},

	/**
	 * Detach CSS classname from a DOM element.
	 * @param {DOMElement} element
	 * @param {string} classname
	 */
	detachClassName : function ( element, classname ) {
		
		if ( element.classList != null ) {
			if ( element.classList.contains ( classname )) {
				element.classList.remove ( classname );
			}
		} else {
			var current = this._getCurrent ( element );
			if ( this._contains ( current, classname )) {
				current = this._detach ( current, classname );
			}
			if ( element.style != null ) {
				element.className = current;
			} else {
				if ( current == "" ) {
					element.removeAttribute ( "class" );
				} else {
					element.setAttribute ( "class", current );
				}
			}
		}
	},
	
	/**
	 * @param {DOMElement} element
	 * @param {string} classname
	 */
	hasClassName : function ( element, classname ) {
		
		var result = false;
		if ( element.classList != null ) {
			result = element.classList.contains ( classname );
		} else {
			result = this._contains ( this._getCurrent ( element ), classname );
		}
		return result;
	}
}

/**
 * The instance that does it.
 * @type {_CSSUtil}
 */
var CSSUtil = new _CSSUtil ();