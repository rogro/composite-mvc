/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

/**
 * @class 
 * This class functions primarily as an assistant for {@link FlexBoxBinding}.
 */
function _CSSComputer () {}

_CSSComputer.prototype = {
	
	_margins : {
		top		: Client.isExplorer ? "marginTop" : "margin-top",
		right	: Client.isExplorer ? "marginRight" : "margin-right",
		bottom	: Client.isExplorer ? "marginBottom" : "margin-bottom",
		left	: Client.isExplorer ? "marginLeft" : "margin-left"
	},
	
	_paddings : {
		top		: Client.isExplorer ? "paddingTop" : "padding-top",
		right	: Client.isExplorer ? "paddingRight" : "padding-right",
		bottom	: Client.isExplorer ? "paddingBottom" : "padding-bottom",
		left	: Client.isExplorer ? "paddingLeft" : "padding-left"
	},
	
	_borders : {
		top		: Client.isExplorer ? "borderTopWidth" : "border-top-width",
		right	: Client.isExplorer ? "borderRightWidth" : "border-right-width",
		bottom	: Client.isExplorer ? "borderBottomWidth" : "border-bottom-width",
		left	: Client.isExplorer ? "borderLeftWidth" : "border-left-width"
	},
	
	/** 
	 * @param {object} comples
	 * @param {DOMElement} element
	 * @return {object}
	 */
	_getComplexResult : function ( complex, element ) {
	
		var result = {};
		for ( var entry in complex ) {
			var ent = parseInt ( 
				DOMUtil.getComputedStyle ( element, complex [ entry ])
			);
			result [ entry ] = isNaN ( ent ) ? 0 : ent;
		}
		return result;
	},
	
	/**
	 * This should only be expected to work well for "px" units.
	 * Returns an object with four properties: top, right, bottom, left.
	 * @param {DOMElement} element
	 * @return {object}
	 */
	_getMargin : function ( element ) {
		return this._getComplexResult ( this._margins, element );
	},
	
	/**
	 * This should only be expected to work well for "px" units.
	 * Returns an object with four properties: top, right, bottom, left.
	 * @param {DOMElement} element
	 * @return {object}
	 */
	getPadding : function ( element ) {
		return this._getComplexResult ( this._paddings, element );
	},
	
	/**
	 * This should only be expected to work well for "px" units.
	 * Returns an object with four properties: top, right, bottom, left.
	 * @param {DOMElement} element
	 * @return {object}
	 */
	getBorder : function ( element ) {
		return this._getComplexResult ( this._borders, element );
	},
	
	/**
	 * TODO: Rename this.
	 * @param {DOMElement} element
	 * @return {string}
	 */
	getPosition : function ( element ) {
		return DOMUtil.getComputedStyle ( element, "position" );
	},
	
	/**
	 * @param {DOMElement} element
	 * @return {string}
	 */
	getFloat : function ( element ) {
		return DOMUtil.getComputedStyle ( element, Client.isExplorer ? "styleFloat" : "float" );
	},
	
	/**
	 * @param {DOMElement} element
	 * @return {int}
	 */
	getZIndex : function ( element ) {
		return parseInt (
			DOMUtil.getComputedStyle ( element, Client.isExplorer ? "zIndex" : "z-index" )
		);
	},
	
	/**
	 * @param {DOMElement} element
	 * @return {string}
	 */
	getBackgroundColor : function ( element ) {
		return DOMUtil.getComputedStyle ( element, Client.isExplorer ? "backgroundColor" : "background-color" );
	}
}

/**
 * The instance that does it.
 * @type {_CSSComputer}
 */
var CSSComputer = new _CSSComputer ();