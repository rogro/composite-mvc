/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

var topLevelClassNames = [ // Don't edit! This file is automatically generated.
	"IAcceptable",
	"IActionListener",
	"IActivatable",
	"IActivationAware",
	"IBroadcastListener",
	"ICrawlerHandler",
	"IData",
	"IDialogResponseHandler",
	"IDOMHandler",
	"IDraggable",
	"IDragHandler",
	"IEditorControlBinding",
	"IEventListener",
	"IFit",
	"IFlexible",
	"IFocusable",
	"IImageProfile",
	"IKeyEventHandler",
	"ILabel",
	"ILoadHandler",
	"IMenuContainer",
	"IResizeHandler",
	"ISourceEditorComponent",
	"IUpdateHandler",
	"IWysiwygEditorComponent",
	"IWysiwygEditorContentChangeHandler",
	"IWysiwygEditorNodeChangeHandler",
	"List",
	"Map",
	"SystemNodeList",
	"BroadcastMessages",
	"EventBroadcaster",
	"Constants",
	"Client",
	"SystemLogger",
	"SystemTimer",
	"SystemDebug",
	"Interfaces",
	"Types",
	"MimeTypes",
	"SearchTokens",
	"StringBundle",
	"KeyMaster",
	"ImageProvider",
	"Resolver",
	"Download",
	"Cookies",
	"StatusBar",
	"Localization",
	"Validator",
	"DOMEvents",
	"DOMSerializer",
	"DOMFormatter",
	"DOMUtil",
	"XMLParser",
	"XPathResolver",
	"XSLTransformer",
	"CSSUtil",
	"CSSComputer",
	"System",
	"SystemNode",
	"SystemAction",
	"Application",
	"Installation",
	"Keyboard",
	"Preferences",
	"Persistance",
	"LocalStore",
	"StandardEventHandler",
	"Action",
	"Animation",
	"Point",
	"Dimension",
	"Geometry",
	"BindingAcceptor",
	"BindingBoxObject",
	"BindingDragger",
	"BindingParser",
	"BindingSerializer",
	"ImageProfile",
	"BindingFinder",
	"NodeCrawler",
	"ElementCrawler",
	"BindingCrawler",
	"Crawler",
	"FlexBoxCrawler",
	"FocusCrawler",
	"FitnessCrawler",
	"Templates",
	"DialogButton",
	"Dialog",
	"Commands",
	"Prism",
	"Uri",
	"ViewDefinition",
	"SystemViewDefinition",
	"HostedViewDefinition",
	"DialogViewDefinition",
	"Binding",
	"DataBinding",
	"RootBinding",
	"StyleBinding",
	"MatrixBinding",
	"FlexBoxBinding",
	"ScrollBoxBinding",
	"LabelBinding",
	"TextBinding",
	"BroadcasterSetBinding",
	"BroadcasterBinding",
	"ButtonBinding",
	"ButtonStateManager",
	"ClickButtonBinding",
	"RadioButtonBinding",
	"CheckButtonBinding",
	"CheckButtonImageProfile",
	"ViewButtonBinding",
	"ControlGroupBinding",
	"ControlBinding",
	"ControlImageProfile",
	"ControlBoxBinding",
	"MenuContainerBinding",
	"MenuBarBinding",
	"MenuBinding",
	"MenuBodyBinding",
	"MenuGroupBinding",
	"MenuItemBinding",
	"PopupSetBinding",
	"PopupBinding",
	"PopupBodyBinding",
	"MenuPopupBinding",
	"DialogBinding",
	"DialogHeadBinding",
	"DialogBodyBinding",
	"DialogMatrixBinding",
	"DialogSetBinding",
	"DialogBorderBinding",
	"DialogCoverBinding",
	"DialogTitleBarBinding",
	"DialogTitleBarBodyBinding",
	"DialogControlBinding",
	"DialogControlImageProfile",
	"DialogTitleBarPopupBinding",
	"WindowBindingHighlightNodeCrawler",
	"WindowBinding",
	"PreviewWindowBinding",
	"RadioGroupBinding",
	"DataBindingMap",
	"DataInputBinding",
	"TextBoxBinding",
	"EditorTextBoxBinding",
	"IEEditorTextBoxBinding",
	"MozEditorTextBoxBinding",
	"SelectorBinding",
	"SimpleSelectorBinding",
	"SelectorBindingSelection",
	"DataInputSelectorBinding",
	"DataInputDialogBinding",
	"UrlInputDialogBinding",
	"DataInputButtonBinding",
	"DataDialogBinding",
	"PostBackDataDialogBinding",
	"ViewDefinitionPostBackDataDialogBinding",
	"NullPostBackDataDialogBinding",
	"NullPostBackDataDialogSelectorBinding",
	"MultiSelectorBinding",
	"HTMLDataDialogBinding",
	"MultiSelectorDataDialogBinding",
	"LazyBindingSetBinding",
	"LazyBindingBinding",
	"EditorDataBinding",
	"FunctionEditorDataBinding",
	"ParameterEditorDataBinding",
	"FilePickerBinding",
	"RequestBinding",
	"FieldGroupBinding",
	"FieldBinding",
	"FieldsBinding",
	"FieldDescBinding",
	"FieldDataBinding",
	"FieldHelpBinding",
	"RadioDataGroupBinding",
	"RadioDataBinding",
	"CheckBoxBinding",
	"CheckBoxGroupBinding",
	"BalloonSetBinding",
	"BalloonBinding",
	"ErrorBinding",
	"FocusBinding",
	"TabsButtonBinding",
	"TabBoxBinding",
	"TabsBinding",
	"TabBinding",
	"TabPanelsBinding",
	"TabPanelBinding",
	"SplitBoxBinding",
	"SplitPanelBinding",
	"SplitterBinding",
	"DecksBinding",
	"DeckBinding",
	"ToolBarBinding",
	"ToolBarBodyBinding",
	"ToolBarGroupBinding",
	"ToolBarButtonBinding",
	"ToolBarLabelBinding",
	"DialogToolBarBinding",
	"ComboBoxBinding",
	"ToolBarComboButtonBinding",
	"ToolBoxToolBarButtonBinding",
	"TreeBinding",
	"TreeBodyBinding",
	"TreeNodeBinding",
	"TreeContentBinding",
	"TreePositionIndicatorBinding",
	"TreeCrawler",
	"DockControlImageProfile",
	"DockTabsButtonBinding",
	"DockBinding",
	"DockTabsBinding",
	"DockTabBinding",
	"DockPanelsBinding",
	"DockPanelBinding",
	"DockControlBinding",
	"DockTabPopupBinding",
	"ViewSetBinding",
	"ViewBinding",
	"PageBinding",
	"DialogPageBinding",
	"DialogPageBodyBinding",
	"EditorPageBinding",
	"ResponsePageBinding",
	"WizardPageBinding",
	"MarkupAwarePageBinding",
	"SystemToolBarBinding",
	"SystemTreeBinding",
	"SystemTreePopupBinding",
	"SystemTreeNodeBinding",
	"SystemPageBinding",
	"StageContainerBinding",
	"StageBinding",
	"StageCrawler",
	"StageDialogSetBinding",
	"StageDialogBinding",
	"FitnessCrawler",
	"StageDecksBinding",
	"StageDeckBinding",
	"StageDeckRootBinding",
	"StageSplitBoxBinding",
	"StageSplitPanelBinding",
	"StageSplitterBinding",
	"StageSplitterBodyBinding",
	"StageBoxAbstraction",
	"StageBoxHandlerAbstraction",
	"StageMenuBarBinding",
	"StageViewMenuItemBinding",
	"StageStatusBarBinding",
	"ExplorerBinding",
	"ExplorerDecksBinding",
	"ExplorerDeckBinding",
	"ExplorerSplitterBinding",
	"ExplorerMenuBinding",
	"ExplorerToolBarBinding",
	"ExplorerToolBarButtonBinding",
	"EditorBinding",
	"EditorPopupBinding",
	"EditorClickButtonBinding",
	"EditorToolBarButtonBinding",
	"EditorSelectorBinding",
	"EditorMenuItemBinding",
	"VisualEditorBinding",
	"VisualEditorPopupBinding",
	"VisualEditorFormattingConfiguration",
	"VisualEditorFieldGroupConfiguration",
	"VisualMultiEditorBinding",
	"VisualMultiTemplateEditorBinding",
	"CodeMirrorEditorPopupBinding",
	"CodeMirrorEditorBinding",
	"ThrobberBinding",
	"ProgressBarBinding",
	"StartMenuItemBinding",
	"KeySetBinding",
	"CursorBinding",
	"CoverBinding",
	"UncoverBinding",
	"TheatreBinding",
	"SourceCodeViewerBinding",
	"PersistanceBinding",
	"LocalizationSelectorBinding",
	"ResponseBinding",
	"UserInterfaceMapping",
	"UserInterface",
	"SOAPMessage",
	"SOAPRequest",
	"SOAPRequestResponse",
	"SOAPFault",
	"SOAPEncoder",
	"SOAPDecoder",
	"Schema",
	"SchemaDefinition",
	"SchemaType",
	"SchemaElementType",
	"SchemaComplexType",
	"SchemaSimpleType",
	"WebServiceResolver",
	"WebServiceOperation",
	"WebServiceProxy",
	"ConfigurationService",
	"ConsoleMessageQueueService",
	"EditorConfigurationService",
	"FlowControllerService",
	"InstallationService",
	"LocalizationService",
	"LoginService",
	"MarkupFormatService",
	"PageService",
	"ReadyService",
	"SecurityService",
	"SEOService",
	"SourceValidationService",
	"StringService",
	"TreeService",
	"XhtmlTransformationsService",
	"FunctionService",
	"MessageQueue",
	"ViewDefinitions",
	"KickStart"];
