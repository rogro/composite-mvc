/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

var Help = new function () {
	
	/**
	 * Initialize.
	 */
	this.initialize = function () {
		
		/*
		 * Handle navigationlists.
		 */
		var i = 0, ul, uls = document.getElementsByTagName ( "ul" );
		while ( ul = uls.item ( i++ )) {
			if ( ul.className.indexOf ( "nl" ) >-1 ) {
				navigationlist ( ul );
			}
		}
	}
	
	/**
	 * Applied to navigationlist links as event handler.
	 * @param {MouseEvent} e
	 */
	function navigationlistaction ( e ) {
		
		var isValid = true;
		e = e ? e : window.event; 
		if ( e.type == "keydown" && e.keyCode != 13 ) {
			isValid = false;
		}
		if ( isValid ) {
			var a = e.target ? e.target : e.srcElement;
			if ( a.nodeName.toLowerCase () == "a" ) {
				var ul = a.parentNode.getElementsByTagName ( "ul" ).item ( 0 );
				if ( ul ) {
					switch ( a.className ) {
						case "label" :
							a.className = "label on";
							ul.className = "on";
							break;
						case "label on" :
							a.className = "label";
							ul.className = "";
							break;
					}
				}
			}
		}
		if ( e.stopPropagation ) {
			e.stopPropagation ();
		} else {
			e.cancelBubble = true;
		}
	}
	
	/**
	 * Convert list to navigationlist.
	 * @param {HTMLUListElement}
	 */
	function navigationlist ( ul ) {
		
		ul.onmousedown = navigationlistaction;
		ul.onkeydown = navigationlistaction;
	}
	
}

/*
 * Initialize onload.
 */
window.onload = Help.initialize;