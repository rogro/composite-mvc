/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Composite.C1Console.Actions;
using Composite.Data;
using Composite.Core.Types;
using Composite.C1Console.Workflow;
using Composite.Data.Types;
using Composite.Core.ResourceSystem;
using Composite.C1Console.Events;
using System.Workflow.Activities;


namespace Composite.C1Console.Elements.ElementProviderHelpers.AssociatedDataElementProviderHelper
{
    [EntityTokenLock()]
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class DeleteMetaDataWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public DeleteMetaDataWorkflow()
        {
            InitializeComponent();
        }


        private IPage GetCurrentPage()
        {
            if ((this.EntityToken is DataEntityToken))
            {
                return this.GetDataItemFromEntityToken<IPage>();
            }
            else
            {
                return null;
            }
        }



        private void DoesDefinedMetaDataTypesExists(object sender, ConditionalEventArgs e)
        {
            IPage page = GetCurrentPage();

            e.Result = page.GetDefinedMetaDataTypes().Count() > 0;
        }



        private void initializeCodeActivity_ShowMessage_ExecuteCode(object sender, EventArgs e)
        {
            this.ShowMessage(
                DialogType.Message,
                StringResourceSystemFacade.GetString("Composite.Management", "AssociatedDataElementProviderHelper.DeleteMetaDataWorkflow.NoDefinedTypesExists.Title"),
                StringResourceSystemFacade.GetString("Composite.Management", "AssociatedDataElementProviderHelper.DeleteMetaDataWorkflow.NoDefinedTypesExists.Message"));
        }



        private void confirmCodeActivity_Initialize_ExecuteCode(object sender, EventArgs e)
        {
            IPage page = GetCurrentPage();

            Dictionary<Pair<string, Type>, string> fieldGroupNames = new Dictionary<Pair<string, Type>, string>();
            foreach (Tuple<Type, string> typeName in page.GetDefinedMetaDataTypeAndNames().OrderBy(f => f.Item2))
            {
                fieldGroupNames.Add(new Pair<string, Type>(typeName.Item2, typeName.Item1), typeName.Item2);
            }

            this.UpdateBinding("FieldGroupNames", fieldGroupNames);
            this.UpdateBinding("SelectedFieldGroupName", fieldGroupNames.Keys.First());
        }



        private void deleteCodeActivity_Delete_ExecuteCode(object sender, EventArgs e)
        {
            IPage page = GetCurrentPage();

            Pair<string, Type> metaDataPair = this.GetBinding<Pair<string, Type>>("SelectedFieldGroupName");

            page.RemoveMetaDataDefinition(metaDataPair.First);

            ParentTreeRefresher parentTreeRefresher = this.CreateParentTreeRefresher();
            parentTreeRefresher.PostRefreshMesseges(this.EntityToken);
        }        
    }
}
