/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Workflow.Activities;
using Composite.C1Console.Actions;
using Composite.Data;
using Composite.Data.Types;
using Composite.Core.Types;
using Composite.C1Console.Workflow;


namespace Composite.C1Console.Elements.ElementProviderHelpers.AssociatedDataElementProviderHelper
{
    [EntityTokenLock()]
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class DeleteDataFolderWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public DeleteDataFolderWorkflow()
        {
            InitializeComponent();
        }



        private IPage GetPage()
        {
            AssociatedDataElementProviderHelperEntityToken entityToken = (AssociatedDataElementProviderHelperEntityToken)this.EntityToken;

            return Composite.Data.PageManager.GetPageById(new Guid(entityToken.Id));
        }



        private Type GetFolderType()
        {
            AssociatedDataElementProviderHelperEntityToken entityToken = (AssociatedDataElementProviderHelperEntityToken)this.EntityToken;

            return TypeManager.GetType(entityToken.Payload);
        }



        private void confirmCodeActivity_Initialize_ExecuteCode(object sender, EventArgs e)
        {
            this.Bindings.Add("DeleteDatas", true);
        }



        private void HasDataReferences(object sender, ConditionalEventArgs e)
        {
            IPage page = GetPage();

            Type folderType = GetFolderType();
            IEnumerable<IData> dataToDelete = page.GetFolderData(folderType);

            var brokenReferences = new List<IData>();

            foreach (var data in dataToDelete)
            {
                var references = DataReferenceFacade.GetNotOptionalReferences(data);
                foreach (var reference in references)
                {
                    DataSourceId dataSourceId = reference.DataSourceId;
                    if (brokenReferences.Any(brokenRef => brokenRef.DataSourceId.Equals(dataSourceId))
                        || dataToDelete.Any(elem => elem.DataSourceId.Equals(dataSourceId)))
                    {
                        continue;
                    }

                    brokenReferences.Add(reference);
                }
            }

            e.Result = brokenReferences.Count > 0;
            if (brokenReferences.Count == 0)
            {
                return;
            }

            Bindings.Add("ReferencedData", DataReferenceFacade.GetBrokenReferencesReport(brokenReferences));
        }



        private void ShouldDeleteData(object sender, ConditionalEventArgs e)
        {
            e.Result = this.GetBinding<bool>("DeleteDatas");
        }



        private void deleteCodeActivity_Delete_ExecuteCode(object sender, EventArgs e)
        {
            IPage page = GetPage();
            Type folderType = GetFolderType();

            page.RemoveFolderDefinition(folderType, this.GetBinding<bool>("DeleteDatas"));

            SpecificTreeRefresher specificTreeRefresher = this.CreateSpecificTreeRefresher();
            specificTreeRefresher.PostRefreshMesseges(page.GetDataEntityToken());
        }
    }
}
