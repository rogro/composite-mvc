/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Workflow.Activities;
using Composite.Core.Logging;
using Composite.Core.ResourceSystem;
using Composite.Core.Types;
using Composite.C1Console.Workflow;
using Composite.Data;


namespace Composite.C1Console.Users.Workflows
{
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class ChangeOwnCultureWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public ChangeOwnCultureWorkflow()
        {
            InitializeComponent();
        }


        private void stepInitialize_codeActivity_ExecuteCode(object sender, EventArgs e)
        {
            CultureInfo userCulture = UserSettings.CultureInfo; // Copy admins settings
            CultureInfo c1ConsoleUiLanguage = UserSettings.C1ConsoleUiLanguage; // Copy admins settings

            List<KeyValuePair> regionLanguageList = StringResourceSystemFacade.GetSupportedCulturesList();
            Dictionary<string, string> culturesDictionary = StringResourceSystemFacade.GetAllCultures();

            this.Bindings.Add("AllCultures", culturesDictionary);
            this.Bindings.Add("CultureName", userCulture.Name);

            this.Bindings.Add("C1ConsoleUiCultures", regionLanguageList);
            this.Bindings.Add("C1ConsoleUiLanguageName", c1ConsoleUiLanguage.Name);
        }


        private void stepFinalize_codeActivity_ExecuteCode(object sender, EventArgs e)
        {
            string cultureName = this.GetBinding<string>("CultureName");
            string c1ConsoleUiLanguageName = this.GetBinding<string>("C1ConsoleUiLanguageName");

            UserSettings.CultureInfo = new CultureInfo(cultureName);
            UserSettings.C1ConsoleUiLanguage = new CultureInfo(c1ConsoleUiLanguageName);

            LoggingService.LogVerbose("ChangeOwnCultureWorkflow", string.Format("Changed culture for user to {0}", cultureName));
        }



        private void CultureHasChanged(object sender, ConditionalEventArgs e)
        {
            string cultureName = this.GetBinding<string>("CultureName");
            string c1ConsoleUiLanguageName = this.GetBinding<string>("C1ConsoleUiLanguageName");

            e.Result = UserSettings.CultureInfo.Name != cultureName || UserSettings.C1ConsoleUiLanguage.Name != c1ConsoleUiLanguageName;
        }



        private void rebootConsoleActivity_ExecuteCode(object sender, EventArgs e)
        {
            this.RebootConsole();
        }

    }
}
