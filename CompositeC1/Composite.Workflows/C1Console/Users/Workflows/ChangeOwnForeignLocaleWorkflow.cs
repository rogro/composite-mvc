/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using Composite.Data;
using System.Collections.Generic;
using System.Globalization;
using Composite.Core.ResourceSystem;
using Composite.C1Console.Events;


namespace Composite.C1Console.Users.Workflows
{
    public sealed partial class ChangeOwnForeignLocaleWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public ChangeOwnForeignLocaleWorkflow()
        {
            InitializeComponent();
        }



        private void HasActiveLocales(object sender, System.Workflow.Activities.ConditionalEventArgs e)
        {
            e.Result = DataLocalizationFacade.ActiveLocalizationCultures.Count() > 1;
        }



        private void step1CodeActivity_Initialize_ExecuteCode(object sender, EventArgs e)
        {
            IEnumerable<CultureInfo> foreignCultures =
                    from l in DataLocalizationFacade.ActiveLocalizationCultures
                    select l;

            List<KeyValuePair<string, string>> foreignCulturesDictionary = foreignCultures.Select(f => new KeyValuePair<string, string>(f.Name, DataLocalizationFacade.GetCultureTitle(f))).ToList();
            foreignCulturesDictionary.Insert(0, new KeyValuePair<string, string>("NONE", StringResourceSystemFacade.GetString("Composite.C1Console.Users", "ChangeForeignLocaleWorkflow.NoForeignLocaleLabel")));

            string selectedForeignCultureName = "NONE";
            if (UserSettings.ForeignLocaleCultureInfo != null)
            {
                selectedForeignCultureName = UserSettings.ForeignLocaleCultureInfo.Name;
            }

            this.Bindings.Add("ForeignCultureName", selectedForeignCultureName);
            this.Bindings.Add("ForeignCulturesList", foreignCulturesDictionary);
        }



        private void finalizeCodeActivity_Finalize_ExecuteCode(object sender, EventArgs e)
        {
            string foreignCultureName = this.GetBinding<string>("ForeignCultureName");
            CultureInfo foreignCultureInfo = null;
            if (foreignCultureName != "NONE")
            {
                foreignCultureInfo = CultureInfo.CreateSpecificCulture(foreignCultureName);
            }

            UserSettings.ForeignLocaleCultureInfo = foreignCultureInfo;

            foreach (string consoleId in GetConsoleIdsOpenedByCurrentUser())
            {
                ConsoleMessageQueueFacade.Enqueue(new BroadcastMessageQueueItem { Name = "ForeignLocaleChanged", Value = "" }, consoleId);
            }

            this.CloseCurrentView();
            this.CollapseAndRefresh();
        }
    }
}
