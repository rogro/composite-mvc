/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Transactions;
using System.Workflow.Activities;
using Composite.C1Console.Actions;
using Composite.C1Console.Events;
using Composite.Data;
using Composite.Data.ProcessControlled;
using Composite.Data.ProcessControlled.ProcessControllers.GenericPublishProcessController;
using Composite.Core.Linq;
using Composite.Core.ResourceSystem;
using Composite.Data.Transactions;
using Composite.Core.Types;
using Composite.C1Console.Users;
using Composite.C1Console.Security;


namespace Composite.C1Console.Trees.Workflows
{
    public sealed partial class LocalizeDataWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public LocalizeDataWorkflow()
        {
            InitializeComponent();
        }



        private void ValidateReferencingPropertyies(object sender, ConditionalEventArgs e)
        {
            DataEntityToken dataEntityToken = (DataEntityToken)this.EntityToken;
            ILocalizedControlled data = dataEntityToken.Data as ILocalizedControlled;

            IEnumerable<ReferenceFailingPropertyInfo> referenceFailingPropertyInfos = DataLocalizationFacade.GetReferencingLocalizeFailingProperties(data).Evaluate();

            if (referenceFailingPropertyInfos.Any(f => f.OptionalReferenceWithValue == false))
            {
                List<string> row = new List<string>();

                row.Add(StringResourceSystemFacade.GetString("Composite.C1Console.Trees", "LocalizeData.ShowError.Description"));

                foreach (ReferenceFailingPropertyInfo referenceFailingPropertyInfo in referenceFailingPropertyInfos.Where(f => f.OptionalReferenceWithValue == false))
                {
                    row.Add(string.Format(StringResourceSystemFacade.GetString("Composite.C1Console.Trees", "LocalizeData.ShowError.FieldErrorFormat"), referenceFailingPropertyInfo.DataFieldDescriptor.Name, referenceFailingPropertyInfo.ReferencedType.GetTypeTitle(), referenceFailingPropertyInfo.OriginLocaleDataValue.GetLabel()));
                }

                List<List<string>> rows = new List<List<string>> { row };

                this.UpdateBinding("ErrorHeader", new List<string> { "Fields" });
                this.UpdateBinding("Errors", rows);

                e.Result = false;
            }
            else
            {
                e.Result = true;
            }
        }



        private void localizeDataCodeActivity_Localize_ExecuteCode(object sender, EventArgs e)
        {
            DataEntityToken dataEntityToken = (DataEntityToken)this.EntityToken;
            ILocalizedControlled data = dataEntityToken.Data as ILocalizedControlled;

            CultureInfo targetCultureInfo = UserSettings.ActiveLocaleCultureInfo;

            if (ExistsInLocale(data, targetCultureInfo))
            {
                this.ShowMessage(
                    DialogType.Message,
                    StringResourceSystemFacade.GetString("Composite.C1Console.Trees", "LocalizeData.ShowError.Layout.Label"),
                    StringResourceSystemFacade.GetString("Composite.C1Console.Trees", "LocalizeData.ShowError.AlreadyTranslated")
                );

                return;
            }


            IEnumerable<ReferenceFailingPropertyInfo> referenceFailingPropertyInfos = DataLocalizationFacade.GetReferencingLocalizeFailingProperties(data).Evaluate();

            IData newData;

            using (TransactionScope transactionScope = TransactionsFacade.CreateNewScope())
            {
                data = data.GetTranslationSource();

                using (new DataScope(targetCultureInfo))
                {
                    newData = DataFacade.BuildNew(data.DataSourceId.InterfaceType);

                    data.ProjectedCopyTo(newData);

                    ILocalizedControlled localizedControlled = newData as ILocalizedControlled;
                    localizedControlled.SourceCultureName = targetCultureInfo.Name;

                    if (newData is IPublishControlled)
                    {
                        IPublishControlled publishControlled = newData as IPublishControlled;
                        publishControlled.PublicationStatus = GenericPublishProcessController.Draft;
                    }

                    foreach (ReferenceFailingPropertyInfo referenceFailingPropertyInfo in referenceFailingPropertyInfos)
                    {
                        PropertyInfo propertyInfo = data.DataSourceId.InterfaceType.GetPropertiesRecursively().Single(f => f.Name == referenceFailingPropertyInfo.DataFieldDescriptor.Name);
                        if (propertyInfo.PropertyType.IsGenericType &&
                            propertyInfo.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                        {
                            propertyInfo.SetValue(newData, null, null);
                        }
                        else if (propertyInfo.PropertyType == typeof(string))
                        {
                            propertyInfo.SetValue(newData, null, null);
                        }
                        else
                        {
                            propertyInfo.SetValue(newData, referenceFailingPropertyInfo.DataFieldDescriptor.DefaultValue.Value, null);
                        }
                    }

                    newData = DataFacade.AddNew(newData);
                }

                EntityTokenCacheFacade.ClearCache(data.GetDataEntityToken());
                EntityTokenCacheFacade.ClearCache(newData.GetDataEntityToken());

                transactionScope.Complete();
            }

            ParentTreeRefresher parentTreeRefresher = this.CreateParentTreeRefresher();
            parentTreeRefresher.PostRefreshMesseges(newData.GetDataEntityToken(), 2);
        }



        private static bool ExistsInLocale(IData data, CultureInfo locale)
        {
            Type dataType = data.DataSourceId.InterfaceType;

            MethodInfo getDataFromOtherScopeMethodInfo = typeof(DataFacade).GetMethod("GetDataFromOtherLocale", BindingFlags.Public | BindingFlags.Static);

            MethodInfo genericMethod = getDataFromOtherScopeMethodInfo.MakeGenericMethod(new[] { dataType });

            object result = genericMethod.Invoke(null, new object[] { data, locale });

            if (result == null) return false;

            var enumerable = result as IEnumerable;
            Verify.IsNotNull(enumerable, "Enumeration expected");

            foreach (object o in enumerable)
            {
                return true;
            }

            return false;
        }
    }
}
