/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Workflow.Activities;
using Composite.C1Console.Events;
using Composite.Data;
using Composite.Data.Types;
using Composite.Data.Transactions;
using Composite.C1Console.Trees;
using Composite.C1Console.Trees.Foundation.AttachmentPoints;
using Composite.Core.Types;
using Composite.C1Console.Workflow;


namespace Composite.C1Console.Trees.Workflows
{
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class RemoveApplicationWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public RemoveApplicationWorkflow()
        {
            InitializeComponent();
        }



        private void IsThereAnyTrees(object sender, ConditionalEventArgs e)
        {
            e.Result = this.BindingExist("SelectedTreeId");
        }



        private void initializeCodeActivity_UpdateBindings_ExecuteCode(object sender, EventArgs e)
        {
            Dictionary<string, string> selectableTreeIds = new Dictionary<string, string>();
            foreach (Tree tree in TreeFacade.AllTrees)
            {
                if (tree.HasAttachmentPoints(this.EntityToken) == false) continue;

                selectableTreeIds.Add(tree.TreeId, tree.AllowedAttachmentApplicationName);
            }


            if (selectableTreeIds.Count > 0)
            {
                this.UpdateBinding("SelectedTreeId", selectableTreeIds.First().Key);
                this.UpdateBinding("SelectableTreeIds", selectableTreeIds);
            }
        }



        private void finalizeCodeActivity_Finalize_ExecuteCode(object sender, EventArgs e)
        {
            string treeId = this.GetBinding<string>("SelectedTreeId");

            Tree tree = TreeFacade.GetTree(treeId);
            
            DynamicDataItemAttachmentPoint dataItemAttachmentPoint = (DynamicDataItemAttachmentPoint)tree.GetAttachmentPoints(this.EntityToken).Single();

            TreeFacade.RemovePersistedAttachmentPoint(treeId, dataItemAttachmentPoint.InterfaceType, dataItemAttachmentPoint.KeyValue);            

            this.RefreshCurrentEntityToken();
        }



        private void initializeCodeActivity_ShowNoTreesMessage_ExecuteCode(object sender, EventArgs e)
        {
            this.ShowMessage(DialogType.Message, "${Composite.C1Console.Trees, RemoveApplication.NoTrees.Title}", "${Composite.C1Console.Trees, RemoveApplication.NoTrees.Message}");
        }
    }
}
