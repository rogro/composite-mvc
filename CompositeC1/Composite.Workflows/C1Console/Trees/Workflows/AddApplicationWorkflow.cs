/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Workflow.Activities;
using Composite.C1Console.Events;
using Composite.Data;
using Composite.Data.Types;
using Composite.C1Console.Elements.Plugins.ElementAttachingProvider;
using Composite.Data.Transactions;
using Composite.C1Console.Trees;
using Composite.Core.Types;
using Composite.C1Console.Workflow;


namespace Composite.C1Console.Trees.Workflows
{
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class AddApplicationWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public AddApplicationWorkflow()
        {
            InitializeComponent();
        }



        private void IsThereAnyTrees(object sender, ConditionalEventArgs e)
        {
            e.Result = this.BindingExist("SelectedTreeId");
        }



        private void initializeCodeActivity_UpdateBindings_ExecuteCode(object sender, EventArgs e)
        {
            Dictionary<string, string> selectableTreeIds = new Dictionary<string, string>();
            foreach (Tree tree in TreeFacade.AllTrees)
            {
                if (tree.HasPossibleAttachmentPoints(this.EntityToken) == false) continue;
                if (tree.HasAttachmentPoints(this.EntityToken)) continue;

                selectableTreeIds.Add(tree.TreeId, tree.AllowedAttachmentApplicationName);
            }


            if (selectableTreeIds.Count > 0)
            {
                this.UpdateBinding("SelectedTreeId", selectableTreeIds.First().Key);
                this.UpdateBinding("SelectableTreeIds", selectableTreeIds);

                Dictionary<string, string> selectablePositions = Enum.GetNames(typeof(ElementAttachingProviderPosition)).ToDictionary(f => f);
                this.UpdateBinding("SelectedPosition", selectablePositions.First().Key);
                this.UpdateBinding("SelectablePositions", selectablePositions);
            }
        }



        private void finalizeCodeActivity_Finalize_ExecuteCode(object sender, EventArgs e)
        {
            DataEntityToken dataEntityToken = (DataEntityToken)this.EntityToken;

            object keyValue = dataEntityToken.DataSourceId.GetKeyValue();

            string treeId = this.GetBinding<string>("SelectedTreeId");
            string serializedPosition = this.GetBinding<string>("SelectedPosition");

            ElementAttachingProviderPosition position = (ElementAttachingProviderPosition)Enum.Parse(typeof(ElementAttachingProviderPosition), serializedPosition);

            TreeFacade.AddPersistedAttachmentPoint(treeId, dataEntityToken.InterfaceType, keyValue, position);

            this.RefreshCurrentEntityToken();
        }



        private void initializeCodeActivity_ShowNoTreesMessage_ExecuteCode(object sender, EventArgs e)
        {
            this.ShowMessage(DialogType.Message, "${Composite.C1Console.Trees, AddApplication.NoTrees.Title}", "${Composite.C1Console.Trees, AddApplication.NoTrees.Message}");
        }
    }
}
