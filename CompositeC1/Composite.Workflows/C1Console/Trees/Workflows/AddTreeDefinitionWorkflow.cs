/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.IO;
using Composite.C1Console.Workflow;
using Composite.Core.Configuration;
using Composite.Core.IO;


namespace Composite.C1Console.Trees.Workflows
{
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class AddTreeDefinitionWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public AddTreeDefinitionWorkflow()
        {
            InitializeComponent();
        }



        private void initializeCodeActivity_Initialize_ExecuteCode(object sender, EventArgs e)
        {
            this.Bindings.Add("DefinitionName", "");

            this.Bindings.Add("TemplatesList", new Dictionary<string, string> {
                { "Empty", "Empty" },
                { "Folder grouping", "Folder grouping" },
                { "Parent filtering", "Parent filtering" }
            });
            this.Bindings.Add("TemplateName", "Empty");

            this.Bindings.Add("PositionsList", new Dictionary<string, string> {
                  { "Content", "Content" },
                  { "Media", "Media" },
                  { "Layout", "Layout" },
                  { "Data", "Data" },
                  { "Function", "Function" },
                  { "User", "User" },
                  { "System", "System" },
                  { "PerspectivesRoot", "PerspectivesRoot" }
            });
            this.Bindings.Add("PositionName", "Content");
        }



        private void finalizeCodeActivity_Finalize_ExecuteCode(object sender, EventArgs e)
        {
            string definitionName = this.GetBinding<string>("DefinitionName");
            string templateName = this.GetBinding<string>("TemplateName");
            string positionName = this.GetBinding<string>("PositionName");

            string template;

            switch (templateName)
            {
                case "Empty":
                    template = string.Format(@"<?xml version=""1.0"" encoding=""UTF-8""?>
<ElementStructure xmlns=""http://http://www.composite.net/ns/management/trees/treemarkup/1.0"" xmlns:f=""http://www.composite.net/ns/function/1.0"">

  <ElementStructure.AutoAttachments>
    <NamedParent Name=""{0}"" Position=""Top""/>
  </ElementStructure.AutoAttachments>

  <ElementRoot>
    <Children>      
        <Element Label=""Simple tree"" Id=""SimpleTree"" />
    </Children>
  </ElementRoot>
</ElementStructure>", positionName);
                    break;

                case "Folder grouping":
                    template = string.Format(@"<?xml version=""1.0"" encoding=""UTF-8""?>
<ElementStructure xmlns=""http://http://www.composite.net/ns/management/trees/treemarkup/1.0"" xmlns:f=""http://www.composite.net/ns/function/1.0"">


  <ElementStructure.AutoAttachments>
    <NamedParent Name=""{0}"" Position=""Top""/>
  </ElementStructure.AutoAttachments>

  <ElementRoot>
    <Children>      
      <Element Label=""My DSL Tree Demo"" Id=""FolderGroupingId1"">
        <Children>
          <Element Label=""Simple Element"" Id=""FolderGroupingId2"">            
            <Children>              
              <DataFolderElements Type=""Composite.Data.Types.IPage"" FieldGroupingName=""ChangeDate"" DateFormat=""yyyy - MMMM"">
                <Children>
                  <DataFolderElements FieldGroupingName=""Description"">
                    <Children>
                      <DataElements Type=""Composite.Data.Types.IPage"">
                      </DataElements>
                    </Children>
                  </DataFolderElements>
                </Children>
              </DataFolderElements>
            </Children>
          </Element>
        </Children>
      </Element>
    </Children>
  </ElementRoot>
</ElementStructure>", positionName);
                    break;

                case "Parent filtering":
                    template = string.Format(@"<?xml version=""1.0"" encoding=""UTF-8""?>
<ElementStructure xmlns=""http://http://www.composite.net/ns/management/trees/treemarkup/1.0"" xmlns:f=""http://www.composite.net/ns/function/1.0"">


  <ElementStructure.AutoAttachments>
    <NamedParent Name=""{0}"" Position=""Top""/>
  </ElementStructure.AutoAttachments>

  <ElementRoot>
    <Children>      
      <Element Label=""My DSL Tree Demo"" Id=""ParentFilteringId1"">
        <Children>
          <Element Label=""Simple Element"" Id=""ParentFilteringId2"">            
            <Children>              
              <DataElements Type=""Composite.Data.Types.IPageTemplate"">
                <Children>
                  <DataElements Type=""Composite.Data.Types.IPage"">
                    <Filters>
                      <ParentIdFilter ParentType=""Composite.Data.Types.IPageTemplate"" ReferenceFieldName=""TemplateId"" />
                    </Filters>
                  </DataElements>
                </Children>
              </DataElements>
            </Children>
          </Element>
        </Children>
      </Element>
    </Children>
  </ElementRoot>
</ElementStructure>", positionName);
                    break;

                default:
                    throw new InvalidOperationException();
            }


            string filename = Path.Combine(PathUtil.Resolve(GlobalSettingsFacade.TreeDefinitionsDirectory), definitionName + ".xml");

            C1File.WriteAllText(filename, template);

            this.RefreshRootEntityToken();
        }



        private void IsTreeIdFree(object sender, System.Workflow.Activities.ConditionalEventArgs e)
        {
            string definitionName = this.GetBinding<string>("DefinitionName");

            string filename = Path.Combine(PathUtil.Resolve(GlobalSettingsFacade.TreeDefinitionsDirectory), definitionName + ".xml");

            e.Result = C1File.Exists(filename) == false;

            if (e.Result == false)
            {
                this.ShowFieldMessage("DefinitionName", "Definition name is already used");
            }
        }
    }
}
