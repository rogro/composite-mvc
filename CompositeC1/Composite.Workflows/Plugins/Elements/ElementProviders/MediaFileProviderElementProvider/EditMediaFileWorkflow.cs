/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using Composite.C1Console.Actions;
using Composite.Data;
using Composite.Data.Types;
using Composite.C1Console.Workflow;
using System.Workflow.Activities;
using System.Collections.Generic;
using Composite.Data.Validation.ClientValidationRules;
using Composite.Data.Validation;


namespace Composite.Plugins.Elements.ElementProviders.MediaFileProviderElementProvider
{
    [EntityTokenLock()]
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class EditMediaFileWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public EditMediaFileWorkflow()
        {
            InitializeComponent();
        }



        private void initializeCodeActivity_ExecuteCode(object sender, EventArgs e)
        {
            DataEntityToken token = (DataEntityToken)this.EntityToken;
            IMediaFile file = (IMediaFile)token.Data;
            IMediaFileStore store = DataFacade.GetData<IMediaFileStore>(x => x.Id == file.StoreId).First();

            this.Bindings.Add("FileDataFileName", file.FileName);
            this.Bindings.Add("FileDataTitle", file.Title);
            this.Bindings.Add("FileDataDescription", file.Description);
            this.Bindings.Add("ProvidesMetaData", store.ProvidesMetadata);

            this.BindingsValidationRules.Add("FileDataTitle", new List<ClientValidationRule> { new StringLengthClientValidationRule(0, 256) });
        }



        private void finalizeCodeActivity_ExecuteCode(object sender, EventArgs e)
        {
            UpdateTreeRefresher updateTreeRefresher = this.CreateUpdateTreeRefresher(this.EntityToken);

            DataEntityToken token = (DataEntityToken)this.EntityToken;
            IMediaFile file = (IMediaFile)token.Data;
            IMediaFileStore store = DataFacade.GetData<IMediaFileStore>(x => x.Id == file.StoreId).First();

            file.FileName = this.GetBinding<string>("FileDataFileName");
            file.Title = this.GetBinding<string>("FileDataTitle");
            file.Description = this.GetBinding<string>("FileDataDescription");

            DataFacade.Update(file);

            SetSaveStatus(true);

            updateTreeRefresher.PostRefreshMesseges(file.GetDataEntityToken());
        }



        private void ValidateInputs(object sender, ConditionalEventArgs e)
        {
            IMediaFile file = this.GetDataItemFromEntityToken<IMediaFile>();

            string filename = this.GetBinding<string>("FileDataFileName");

            string compositePath = IMediaFileExtensions.GetCompositePath(file.StoreId, file.FolderPath, filename);
            if (compositePath.Length > 2048)
            {
                this.ShowFieldMessage("FileDataFileName", "${Composite.Management, Website.Forms.Administrative.EditMediaFile.TotalFilenameToLong.Message}");
                e.Result = false;
                return;
            }

            Guid mediaFileId = file.Id;
            if(DataFacade.GetData<IMediaFile>()
                .Any(mediaFile => string.Compare(mediaFile.CompositePath, compositePath, StringComparison.InvariantCultureIgnoreCase) == 0
                                  && mediaFile.Id != mediaFileId))
            {
                this.ShowFieldMessage("FileDataFileName", "${Composite.Management, Website.Forms.Administrative.EditMediaFile.FileExists.Message}");
                e.Result = false;
                return;
            }

            e.Result = true;
        }
    }
}
