/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Workflow.Activities;
using Composite.C1Console.Actions;
using Composite.C1Console.Events;
using Composite.Data;
using Composite.Data.Types;
using Composite.Data.Types.StoreIdFilter;
using Composite.C1Console.Elements.ElementProviderHelpers.AssociatedDataElementProviderHelper;
using Composite.Core.Linq;
using Composite.Core.ResourceSystem;
using Composite.Data.Transactions;
using Composite.C1Console.Workflow;


namespace Composite.Plugins.Elements.ElementProviders.MediaFileProviderElementProvider
{
    [EntityTokenLock()]
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class DeleteMediaFolderWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public DeleteMediaFolderWorkflow()
        {
            InitializeComponent();
        }

        private void HasDataReferences(object sender, ConditionalEventArgs e)
        {
            DataEntityToken token = (DataEntityToken)this.EntityToken;
            IMediaFileFolder folder = (IMediaFileFolder)token.Data;

            string storeId = folder.StoreId;
            string parentPath = folder.Path;

            string innerElementsPathPrefix = string.Format("{0}/", parentPath);

            var fileQueryable = new StoreIdFilterQueryable<IMediaFile>(DataFacade.GetData<IMediaFile>(), storeId);
            IEnumerable<IMediaFile> childFiles =
                (from item in fileQueryable
                 where item.FolderPath.StartsWith(innerElementsPathPrefix) || item.FolderPath == parentPath
                 select item).Evaluate();

            var brokenReferences = new List<IData>();

            foreach (IMediaFile mediaFile in childFiles)
            {
                List<IData> references = DataReferenceFacade.GetNotOptionalReferences(mediaFile);
                foreach (IData reference in references)
                {
                    if (brokenReferences.Any(data => data.DataSourceId.Equals(reference.DataSourceId)))
                    {
                        continue;
                    }
                    brokenReferences.Add(reference);
                }
            }

            e.Result = brokenReferences.Count > 0;
            if (brokenReferences.Count == 0)
            {
                return;
            }

            Bindings.Add("ReferencedData", DataReferenceFacade.GetBrokenReferencesReport(brokenReferences));
        }


        private void deleteCodeActivity_ExecuteCode(object sender, EventArgs e)
        {
            DeleteTreeRefresher treeRefresher = this.CreateDeleteTreeRefresher(this.EntityToken);
            DataEntityToken token = (DataEntityToken)this.EntityToken;

            using (TransactionScope transactionScope = TransactionsFacade.CreateNewScope())
            {
                IMediaFileFolder folder = DataFacade.GetDataFromDataSourceId(token.DataSourceId, false) as IMediaFileFolder;

                // Media folder may already be deleted at this point
                if (folder != null)
                {
                    if (!DataFacade.WillDeleteSucceed(folder))
                    {
                        this.ShowMessage(
                                DialogType.Error,
                                StringResourceSystemFacade.GetString("Composite.Management", "DeleteMediaFolderWorkflow.CascadeDeleteErrorTitle"),
                                StringResourceSystemFacade.GetString("Composite.Management", "DeleteMediaFolderWorkflow.CascadeDeleteErrorMessage")
                            );

                        return;
                    }

                    DataFacade.Delete(folder);
                }

                transactionScope.Complete();
            }

            treeRefresher.PostRefreshMesseges();
        }

        private void codeActivity1_ExecuteCode(object sender, EventArgs e)
        {
            DataEntityToken token = (DataEntityToken)this.EntityToken;
            IMediaFileFolder folder = (IMediaFileFolder)token.Data;

            string storeId = folder.StoreId;
            string parentPath = folder.Path;

            StoreIdFilterQueryable<IMediaFileFolder> folderQueryable = new StoreIdFilterQueryable<IMediaFileFolder>(DataFacade.GetData<IMediaFileFolder>(), storeId);
            StoreIdFilterQueryable<IMediaFile> fileQueryable = new StoreIdFilterQueryable<IMediaFile>(DataFacade.GetData<IMediaFile>(), storeId);

            string innerElementsPathPrefix = string.Format("{0}/", parentPath);

            bool anyFiles =
                (from item in fileQueryable
                 where item.FolderPath.StartsWith(innerElementsPathPrefix) || item.FolderPath == parentPath
                 select item).Any();

            bool anyFolders =
                (from item in folderQueryable
                 where item.Path.StartsWith(innerElementsPathPrefix)
                 select item).Any();


            if ((anyFiles == false) && (anyFolders == false))
            {
                this.Bindings.Add("MessageText", StringResourceSystemFacade.GetString("Composite.Management", "Website.Forms.Administrative.DeleteMediaFolder.Text"));
            }
            else
            {
                this.Bindings.Add("MessageText", StringResourceSystemFacade.GetString("Composite.Management", "Website.Forms.Administrative.DeleteMediaFolder.HasChildringText"));
            }
        }
    }
}
