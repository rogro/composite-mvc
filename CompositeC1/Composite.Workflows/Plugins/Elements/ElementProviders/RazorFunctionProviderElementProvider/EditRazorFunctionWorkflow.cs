/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.IO;
using System.Reflection;
using System.Web.WebPages;
using Composite.AspNet.Razor;
using Composite.AspNet.Security;
using Composite.C1Console.Events;
using Composite.C1Console.Workflow;
using Composite.Core;
using Composite.Core.Extensions;
using Composite.Core.IO;
using Composite.Core.ResourceSystem;
using Composite.Plugins.Elements.ElementProviders.Common;
using Composite.Plugins.Elements.ElementProviders.WebsiteFileElementProvider;
using Composite.Plugins.Functions.FunctionProviders.FileBasedFunctionProvider;
using Composite.Plugins.Functions.FunctionProviders.RazorFunctionProvider;

namespace Composite.Plugins.Elements.ElementProviders.RazorFunctionProviderElementProvider
{
    [EntityTokenLock]
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class EditRazorFunctionWorkflow : BaseFunctionWorkflow
    {
        private static readonly string LogTitle = typeof(EditRazorFunctionWorkflow).Name;

        public EditRazorFunctionWorkflow()
        {
            InitializeComponent();
        }

        private string GetFile(FileBasedFunction<RazorBasedFunction> function)
        {
            return PathUtil.Resolve(function.VirtualPath);
        }

        private void initializeCodeActivity_ExecuteCode(object sender, EventArgs e)
        {
            FileBasedFunctionProvider<RazorBasedFunction> provider;
            FileBasedFunction<RazorBasedFunction> function;

            GetProviderAndFunction((FileBasedFunctionEntityToken)this.EntityToken, out provider, out function);
            
            string title = Path.GetFileName(function.VirtualPath);

            this.Bindings.Add("Title", title);

            string file = GetFile(function);

            var websiteFile = new WebsiteFile(file);

            this.Bindings.Add("FileContent", websiteFile.ReadAllText());
            this.Bindings.Add("FileName", websiteFile.FileName);
            this.Bindings.Add("FileMimeType", websiteFile.MimeType);

        }


        private void saveCodeActivity_ExecuteCode(object sender, EventArgs e)
        {
            var functionEntityToken = (FileBasedFunctionEntityToken)this.EntityToken;

            FileBasedFunctionProvider<RazorBasedFunction> provider;
            FileBasedFunction<RazorBasedFunction> function;

            GetProviderAndFunction(functionEntityToken, out provider, out function);

            string file = GetFile(function);

            string fileContent = this.GetBinding<string>("FileContent");
            string fixedFileContent = PageTemplateHelper.FixHtmlEscapeSequences(fileContent);

            if (!CompileAndValidate(file, fixedFileContent))
            {
                SetSaveStatus(false);
                return;
            }

            var websiteFile = new WebsiteFile(file);
            websiteFile.WriteAllText(fixedFileContent);


            provider.ReloadFunctions();

            this.CreateParentTreeRefresher().PostRefreshMesseges(this.EntityToken);

            SetSaveStatus(true);

            if(fixedFileContent != fileContent)
            {
                UpdateBinding("FileContent", fixedFileContent);
                RerenderView();
            }
        }

        private bool CompileAndValidate(string file, string fileContent)
        {
            string tempMarkupFile = GetTempFilePath(file);

            try
            {
                File.WriteAllText(tempMarkupFile, fileContent);
                string tempFileVirtualPath = "~" + PathUtil.GetWebsitePath(tempMarkupFile);

                WebPageBase webPageBase;

                try
                {
                    webPageBase = WebPage.CreateInstanceFromVirtualPath(tempFileVirtualPath);
                }
                catch(Exception ex)
                {
                    Log.LogWarning(LogTitle, "Failed to compile CSHTML file");
                    Log.LogWarning(LogTitle, ex);

                    Exception compilationException = (ex is TargetInvocationException) ? ex.InnerException : ex;

                    // Replacing file path and temp file name from error message as it is irrelevant to the user
                    string markupFileName = Path.GetFileName(file);
                    string errorMessage = compilationException.Message;

                    if (errorMessage.StartsWith(tempMarkupFile, StringComparison.OrdinalIgnoreCase))
                    {
                        errorMessage = markupFileName + errorMessage.Substring(tempMarkupFile.Length);
                    }

                    ShowWarning(GetText("EditRazorFunctionWorkflow.Validation.CompilationFailed")
                                .FormatWith(errorMessage));

                    return false;
                }

                return Validate(webPageBase);
            }
            finally
            {
                // Deleting temporary file
                File.Delete(tempMarkupFile);
            }
        }

        private bool Validate(WebPageBase webPageBase)
        {
            var razorFunction = webPageBase as RazorFunction;
            if (razorFunction == null)
            {
                ShowWarning(GetText("EditRazorFunctionWorkflow.Validation.IncorrectBaseClass")
                            .FormatWith(typeof(RazorFunction).FullName));
                return false;
            }


            return true;
        }


        private void ShowWarning(string warning)
        {
            this.ShowMessage(DialogType.Warning,
                 GetText("EditRazorFunctionWorkflow.Validation.DialogTitle"),
                 warning);
        }

        private string GetText(string text)
        {
            return StringResourceSystemFacade.GetString("Composite.Plugins.RazorFunction", text);
        }


        private string GetTempFilePath(string filePath)
        {
            string fileName = Path.GetFileName(filePath);
            string folderPath = Path.GetDirectoryName(filePath);

            return Path.Combine(folderPath, "_temp_" + fileName);
        }
    }
}
