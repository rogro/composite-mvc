/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Workflow.Runtime;
using System.Xml.Linq;
using Composite.C1Console.Actions;
using Composite.C1Console.Events;
using Composite.Data;
using Composite.Data.DynamicTypes;
using Composite.Data.DynamicTypes.Foundation;
using Composite.Data.GeneratedTypes;
using Composite.Core.Types;
using Composite.C1Console.Workflow;


namespace Composite.Plugins.Elements.ElementProviders.GeneratedDataTypesElementProvider
{
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class EditFormWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public EditFormWorkflow()
        {
            InitializeComponent();
        }


        
        private void initializeCodeActivity_ExecuteCode(object sender, EventArgs e)
        {
            DataTypeDescriptor dataTypeDescriptor = GetDataTypeDescriptor();

            string formMarkup;

            XDocument formMarkupDocument = DynamicTypesCustomFormFacade.GetCustomFormMarkup(dataTypeDescriptor);


            if (formMarkupDocument != null)
            {
                formMarkup = formMarkupDocument.ToString();
            }
            else
            {
                var formsHelper = new DataTypeDescriptorFormsHelper(dataTypeDescriptor);

                GeneratedTypesHelper generatedTypesHelper = new GeneratedTypesHelper(dataTypeDescriptor);
                formsHelper.AddReadOnlyFields(generatedTypesHelper.NotEditableDataFieldDescriptorNames);

                formMarkup = formsHelper.GetForm();
            }

            this.Bindings.Add("Title", string.Format("{0}.{1} XML", dataTypeDescriptor.Namespace, dataTypeDescriptor.Name));
            this.Bindings.Add("FileContent", formMarkup);
        }



        private void saveCodeActivity_ExecuteCode(object sender, EventArgs e)
        {
            string fileContent = this.GetBinding<string>("FileContent");

            DataTypeDescriptor dataTypeDescriptor = GetDataTypeDescriptor();

            try
            {
                DynamicTypesAlternateFormFacade.SetAlternateForm(dataTypeDescriptor, fileContent);

                SetSaveStatus(true);
            }
            catch (Exception ex)
            {
                FlowControllerServicesContainer serviceContainer = WorkflowFacade.GetFlowControllerServicesContainer(WorkflowEnvironment.WorkflowInstanceId);
                var consoleMessageService = serviceContainer.GetService<IManagementConsoleMessageService>();
                consoleMessageService.ShowMessage(
                    DialogType.Error,
                    "Error",
                    ex.Message);
            }

        }



        private DataTypeDescriptor GetDataTypeDescriptor()
        {
            GeneratedDataTypesElementProviderTypeEntityToken entityToken = (GeneratedDataTypesElementProviderTypeEntityToken)this.EntityToken;
            Type type = TypeManager.GetType(entityToken.SerializedTypeName);

            Guid guid = type.GetImmutableTypeId();

            return DataMetaDataFacade.GetDataTypeDescriptor(guid);
        }
    }
}
