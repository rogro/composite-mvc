/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Composite.C1Console.Actions;
using Composite.C1Console.Events;
using Composite.Data;
using Composite.Data.DynamicTypes;
using Composite.Data.DynamicTypes.Foundation;
using Composite.Data.GeneratedTypes;
using Composite.Core.Types;
using Composite.Data.Types;
using Composite.Data.Validation.ClientValidationRules;
using Composite.C1Console.Workflow;

using Texts = Composite.Core.ResourceSystem.LocalizationFiles.Composite_Plugins_GeneratedDataTypesElementProvider;

namespace Composite.Plugins.Elements.ElementProviders.GeneratedDataTypesElementProvider
{
    [EntityTokenLock()]
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class EditInterfaceTypeWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public EditInterfaceTypeWorkflow()
        {
            InitializeComponent();
        }


        private DataTypeDescriptor GetDataTypeDescriptor()
        {
            Type type = GetTypeFromEntityToken();

            Guid guid = type.GetImmutableTypeId();

            return DataMetaDataFacade.GetDataTypeDescriptor(guid);
        }


        private Type GetTypeFromEntityToken()
        {
            GeneratedDataTypesElementProviderTypeEntityToken entityToken = (GeneratedDataTypesElementProviderTypeEntityToken)this.EntityToken;
            return TypeManager.GetType(entityToken.SerializedTypeName);
        }


        private void initialStateCodeActivity_ExecuteCode(object sender, EventArgs e)
        {
            DataTypeDescriptor dataTypeDescriptor = GetDataTypeDescriptor();

            Dictionary<string, object> bindings = new Dictionary<string, object>();

            GeneratedTypesHelper helper = new GeneratedTypesHelper(dataTypeDescriptor);

            List<DataFieldDescriptor> fieldDescriptors = helper.EditableOwnDataFields.ToList();

            bindings.Add("TypeName", dataTypeDescriptor.Name);
            bindings.Add("TypeNamespace", dataTypeDescriptor.Namespace);
            bindings.Add("TypeTitle", dataTypeDescriptor.Title);
            bindings.Add("LabelFieldName", dataTypeDescriptor.LabelFieldName);
            bindings.Add("DataFieldDescriptors", fieldDescriptors);
            bindings.Add("HasCaching", helper.IsCachable);
            bindings.Add("HasPublishing", helper.IsPublishControlled);
            bindings.Add("HasLocalization", helper.IsLocalizedControlled);
            bindings.Add("EditProcessControlledAllowed", helper.IsEditProcessControlledAllowed);
            bindings.Add("OldTypeName", dataTypeDescriptor.Name);
            bindings.Add("OldTypeNamespace", dataTypeDescriptor.Namespace);

            this.UpdateBindings(bindings);

            this.BindingsValidationRules.Add("TypeName", new List<ClientValidationRule> { new NotNullClientValidationRule() });
            this.BindingsValidationRules.Add("TypeNamespace", new List<ClientValidationRule> { new NotNullClientValidationRule() });
            this.BindingsValidationRules.Add("TypeTitle", new List<ClientValidationRule> { new NotNullClientValidationRule() });
        }


        private Type GetOldTypeFromBindings()
        {
            string typeFullName = GetBinding<string>("OldTypeNamespace") + "." + GetBinding<string>("OldTypeName");

            return TypeManager.GetType(typeFullName);
        }

        private void finalizeStateCodeActivity_ExecuteCode(object sender, EventArgs e)
        {
            Type oldType = GetOldTypeFromBindings();

            string typeName = this.GetBinding<string>("TypeName");
            string typeNamespace = this.GetBinding<string>("TypeNamespace");
            string typeTitle = this.GetBinding<string>("TypeTitle");
            bool hasCaching = this.GetBinding<bool>("HasCaching");
            bool hasPublishing = this.GetBinding<bool>("HasPublishing");
            bool hasLocalization = this.GetBinding<bool>("HasLocalization");
            string labelFieldName = this.GetBinding<string>("LabelFieldName");
            List<DataFieldDescriptor> dataFieldDescriptors = this.GetBinding<List<DataFieldDescriptor>>("DataFieldDescriptors");

            GeneratedTypesHelper helper = new GeneratedTypesHelper(oldType);

            string errorMessage;
            if (!helper.ValidateNewTypeName(typeName, out errorMessage))
            {
                this.ShowFieldMessage("TypeName", errorMessage);
                return;
            }

            if (!helper.ValidateNewTypeNamespace(typeNamespace, out errorMessage))
            {
                this.ShowFieldMessage("TypeNamespace", errorMessage);
                return;
            }

            if (!helper.ValidateNewTypeFullName(typeName, typeNamespace, out errorMessage))
            {
                this.ShowFieldMessage("TypeName", errorMessage);
                return;
            }

            if (!helper.ValidateNewFieldDescriptors(dataFieldDescriptors, out errorMessage))
            {
                this.ShowMessage(
                        DialogType.Warning,
                        Texts.EditInterfaceTypeStep1_ErrorTitle,
                        errorMessage
                    );
                return;
            }


            helper.SetNewTypeFullName(typeName, typeNamespace);
            helper.SetNewTypeTitle(typeTitle);
            helper.SetNewFieldDescriptors(dataFieldDescriptors, labelFieldName);

            if (helper.IsEditProcessControlledAllowed)
            {
                helper.SetCachable(hasCaching);
                helper.SetPublishControlled(hasPublishing);
                helper.SetLocalizedControlled(hasLocalization);
            }

            bool originalTypeHasData = DataFacade.HasDataInAnyScope(oldType);

            if (!helper.TryValidateUpdate(originalTypeHasData, out errorMessage))
            {
                this.ShowMessage(
                        DialogType.Warning,
                        Texts.EditInterfaceTypeStep1_ErrorTitle,
                        errorMessage
                    );
                return;
            }

            //string newOldDataTypeFullName = typeNamespace + "." + typeName;
            
            string oldSerializedTypeName = GetSerializedTypeName(GetBinding<string>("OldTypeNamespace"), GetBinding<string>("OldTypeName"));
            string newSerializedTypeName = GetSerializedTypeName(typeNamespace, typeName);
            if (newSerializedTypeName != oldSerializedTypeName)
            {
                UpdateWhiteListedGeneratedTypes(oldSerializedTypeName, newSerializedTypeName);
            }

            helper.CreateType(originalTypeHasData);

            UpdateBinding("OldTypeName", typeName);
            UpdateBinding("OldTypeNamespace", typeNamespace);

            SetSaveStatus(true);

            var rootEntityToken = new GeneratedDataTypesElementProviderRootEntityToken(this.EntityToken.Source, 
                GeneratedDataTypesElementProviderRootEntityToken.GlobalDataTypeFolderId);

            SpecificTreeRefresher specificTreeRefresher = this.CreateSpecificTreeRefresher();
            specificTreeRefresher.PostRefreshMesseges(rootEntityToken);

            IFile markupFile = DynamicTypesAlternateFormFacade.GetAlternateFormMarkupFile(typeNamespace, typeName);
            if (markupFile != null)
            {
                ShowMessage(DialogType.Message, 
                    Texts.FormMarkupInfo_Dialog_Label,
                    Texts.FormMarkupInfo_Message(Texts.EditFormMarkup, markupFile.GetRelativeFilePath()));
            }
        }

        private static void UpdateWhiteListedGeneratedTypes(string oldTypeName, string newTypeName)
        {
            var referencesToOldTypeName = DataFacade.GetData<IGeneratedTypeWhiteList>(item => item.TypeManagerTypeName == oldTypeName).ToList();
            if (!referencesToOldTypeName.Any())
            {
                return;
            }

            if(!DataFacade.GetData<IGeneratedTypeWhiteList>(item => item.TypeManagerTypeName == newTypeName).Any())
            {
                var newWhiteListItem = DataFacade.BuildNew<IGeneratedTypeWhiteList>();
                newWhiteListItem.TypeManagerTypeName = newTypeName;

                DataFacade.AddNew(newWhiteListItem);
            }

            DataFacade.Delete<IGeneratedTypeWhiteList>(referencesToOldTypeName);
        }

        private static string GetSerializedTypeName(string typeNamespace, string typeName)
        {
            return "DynamicType:" + (typeName.Length == 0 ? typeName : typeNamespace + "." + typeName);
        }
    }
}
