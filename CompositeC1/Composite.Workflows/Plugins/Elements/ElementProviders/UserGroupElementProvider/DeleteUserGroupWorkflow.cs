/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using Composite.Data;
using Composite.Data.Types;
using Composite.C1Console.Actions;
using System.Workflow.Activities;
using Composite.C1Console.Events;
using Composite.Core.ResourceSystem;
using Composite.Core.Logging;
using Composite.C1Console.Security;


namespace Composite.Plugins.Elements.ElementProviders.UserGroupElementProvider
{
    public sealed partial class DeleteUserGroupWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public DeleteUserGroupWorkflow()
        {
            InitializeComponent();
        }



        private void HasUsers(object sender, ConditionalEventArgs e)
        {
            DataEntityToken dataEntityToken = (DataEntityToken)this.EntityToken;

            IUserGroup userGroup = (IUserGroup)dataEntityToken.Data;

            e.Result = DataFacade.GetData<IUserUserGroupRelation>(f => f.UserGroupId == userGroup.Id).Any();
        }



        private void initializeCodeActivity_ShowMessage_ExecuteCode(object sender, EventArgs e)
        {
            ShowMessage(DialogType.Message,
                StringResourceSystemFacade.GetString("Composite.Plugins.UserGroupElementProvider", "DeleteUserGroup.DeleteUserGroupInitialStep.UserGroupHasUsersTitle"),
                StringResourceSystemFacade.GetString("Composite.Plugins.UserGroupElementProvider", "DeleteUserGroup.DeleteUserGroupInitialStep.UserGroupHasUsersMessage")
                );
        }


        private void finalizeCodeActivity_Delete_ExecuteCode(object sender, EventArgs e)
        {
            DeleteTreeRefresher deleteTreeRefresher = CreateDeleteTreeRefresher(this.EntityToken);

            DataEntityToken dataEntityToken = (DataEntityToken)this.EntityToken;

            IUserGroup userGroup = (IUserGroup)dataEntityToken.Data;

            DataFacade.Delete(userGroup);

            LoggingService.LogVerbose("UserManagement", String.Format("C1 Console user group '{0}' deleted by '{1}'.", userGroup.Name, UserValidationFacade.GetUsername()), LoggingService.Category.Audit);

            deleteTreeRefresher.PostRefreshMesseges();
        }
    }
}
