/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Drawing;
using System.Linq;
using System.Workflow.ComponentModel.Compiler;
using System.Workflow.ComponentModel.Serialization;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Design;
using System.Workflow.Runtime;
using System.Workflow.Activities;
using System.Workflow.Activities.Rules;
using Composite.C1Console.Workflow;
using Composite.Data.Types;
using Composite.Data;
using Composite.Core.ResourceSystem;
using Composite.C1Console.Actions;
using Composite.Data.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Composite.Core.Logging;
using Composite.C1Console.Security;


namespace Composite.Plugins.Elements.ElementProviders.UserGroupElementProvider
{
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class AddNewUserGroupWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public AddNewUserGroupWorkflow()
        {
            InitializeComponent();
        }



        private void initializeCodeActivity_Initialize_ExecuteCode(object sender, EventArgs e)
        {
            IUserGroup userGroup = DataFacade.BuildNew<IUserGroup>();
            userGroup.Id = Guid.NewGuid();

            this.Bindings.Add("NewUserGroup", userGroup);
        }



        private void ValidateGroupName(object sender, ConditionalEventArgs e)
        {
            IUserGroup userGroup = this.GetBinding<IUserGroup>("NewUserGroup");

            bool exists =
                (from ug in DataFacade.GetData<IUserGroup>()
                 where ug.Name == userGroup.Name
                 select ug).Any();

            e.Result = exists == false;
        }



        private void ShowGroupValidationError(object sender, EventArgs e)
        {
            this.ShowFieldMessage(
                "NewUserGroup.Name",
                StringResourceSystemFacade.GetString("Composite.Plugins.UserGroupElementProvider", "AddNewUserGroup.AddNewUserGroupStep1.UserGroupNameAlreadyExists"));
        }



        private void finalizeCodeActivity_Finalize_ExecuteCode(object sender, EventArgs e)
        {
            AddNewTreeRefresher addNewTreeRefresher = this.CreateAddNewTreeRefresher(this.EntityToken);

            IUserGroup userGroup = this.GetBinding<IUserGroup>("NewUserGroup");

            userGroup = DataFacade.AddNew<IUserGroup>(userGroup);

            this.CloseCurrentView();

            LoggingService.LogVerbose("UserManagement", String.Format("New C1 Console user group '{0}' created by '{1}'.", userGroup.Name, UserValidationFacade.GetUsername()), LoggingService.Category.Audit);

            addNewTreeRefresher.PostRefreshMesseges(userGroup.GetDataEntityToken());

            this.ExecuteWorklow(userGroup.GetDataEntityToken(), typeof(EditUserGroupWorkflow));

        }



        private void ValidateData(object sender, ConditionalEventArgs e)
        {
            IUserGroup userGroup = this.GetBinding<IUserGroup>("NewUserGroup");

            ValidationResults validationResults = ValidationFacade.Validate<IUserGroup>(userGroup);
            e.Result = validationResults.IsValid;
        }



        private void ShowDataValidateErrorCodeActivity_ExecuteCode(object sender, EventArgs e)
        {
            IUserGroup userGroup = this.GetBinding<IUserGroup>("NewUserGroup");

            ValidationResults validationResults = ValidationFacade.Validate<IUserGroup>(userGroup);
            
            this.ShowFieldMessage(
                "NewUserGroup.Name",
                validationResults.First().Message);
        }
    }
}
