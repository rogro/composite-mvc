/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Workflow.Activities;
using Composite.C1Console.Actions;
using Composite.Data;
using Composite.Data.Types;
using Composite.Core.Extensions;
using Composite.Core.Types;
using Composite.C1Console.Workflow;


namespace Composite.Plugins.Elements.ElementProviders.MethodBasedFunctionProviderElementProvider
{
    [EntityTokenLock()]
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class EditMethodBasedFunctionWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public EditMethodBasedFunctionWorkflow()
        {
            InitializeComponent();
        }


        private void initializeCodeActivity_ExecuteCode(object sender, EventArgs e)
        {
            DataEntityToken dataEntityToken = (DataEntityToken)this.EntityToken;

            this.Bindings.Add("CurrentMethodFunctionInfo", dataEntityToken.Data);
            this.Bindings.Add("ErrorMessage", null);
        }



        private void IsValidData(object sender, ConditionalEventArgs e)
        {
            e.Result = false;

            IMethodBasedFunctionInfo function = this.GetBinding<IMethodBasedFunctionInfo>("CurrentMethodFunctionInfo");

            if (function.UserMethodName == String.Empty)
            {
                this.ShowFieldMessage("CurrentMethodFunctionInfo.UserMethodName", "${Composite.Plugins.MethodBasedFunctionProviderElementProvider, EditFunction.MethodNameEmpty}");
                return;
            }

            if (!function.Namespace.IsCorrectNamespace('.'))
            {
                this.ShowFieldMessage("CurrentMethodFunctionInfo.UserMethodName", "${Composite.Plugins.MethodBasedFunctionProviderElementProvider, EditFunction.InvalidNamespace}");
                return;
            }

            Type type = TypeManager.TryGetType(function.Type);

            if (type == null)
            {
                this.ShowFieldMessage("CurrentMethodFunctionInfo.Type", "${Composite.Plugins.MethodBasedFunctionProviderElementProvider, EditFunction.TypeNotFound}");
                return;
            }

            List<string> methodNames =
                (from methodInfo in type.GetMethods()
                 select methodInfo.Name).ToList();


            if (!methodNames.Contains(function.MethodName))
            {
                this.ShowFieldMessage("CurrentMethodFunctionInfo.MethodName", "${Composite.Plugins.MethodBasedFunctionProviderElementProvider, EditFunction.MethodNotInType}");
                return;
            }


            if (methodNames.Count == 0)
            {
                this.ShowFieldMessage("CurrentMethodFunctionInfo.Type", "${Composite.Plugins.MethodBasedFunctionProviderElementProvider, EditFunction.NoValidMethod}");
                return;
            }

            int destinctCount = methodNames.Distinct().Count();
            if (destinctCount != methodNames.Count)
            {
                this.ShowFieldMessage("CurrentMethodFunctionInfo.Type", "${Composite.Plugins.MethodBasedFunctionProviderElementProvider, EditFunction.MethodOverloadsNotAllowed}");
                return;
            }           

            e.Result = true;
        }



        private void saveCodeActivity_ExecuteCode(object sender, EventArgs e)
        {
            UpdateTreeRefresher updateTreeRefresher = this.CreateUpdateTreeRefresher(this.EntityToken);

            IMethodBasedFunctionInfo methodBasedFunctionInfo = this.GetBinding<IMethodBasedFunctionInfo>("CurrentMethodFunctionInfo");

            DataFacade.Update(methodBasedFunctionInfo);

            SetSaveStatus(true);

            updateTreeRefresher.PostRefreshMesseges(methodBasedFunctionInfo.GetDataEntityToken());
        }
    }
}
