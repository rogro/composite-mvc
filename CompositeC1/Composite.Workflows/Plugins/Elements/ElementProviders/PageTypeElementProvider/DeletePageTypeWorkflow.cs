/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Linq;
using System.Workflow.Activities;
using Composite.Data.Types;
using Composite.C1Console.Workflow;
using Composite.Data;
using Composite.Core.ResourceSystem;
using Composite.C1Console.Actions;
using Composite.Core.Linq;
using System.Collections.Generic;


namespace Composite.Plugins.Elements.ElementProviders.PageTypeElementProvider
{
    [EntityTokenLock()]
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class DeletePageTypeWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public DeletePageTypeWorkflow()
        {
            InitializeComponent();
        }



        private void IsPageReferingPageType(object sender, ConditionalEventArgs e)
        {            
            IPageType pageType = (IPageType)((DataEntityToken)this.EntityToken).Data;

            e.Result = DataFacade.GetData<IPage>().Where(f => f.PageTypeId == pageType.Id).Any();
        }



        private void confirmCodeActivity_Initialize_ExecuteCode(object sender, EventArgs e)
        {
            IPageType pageType = (IPageType)((DataEntityToken)this.EntityToken).Data;

            this.Bindings.Add("MessageText", string.Format(StringResourceSystemFacade.GetString("Composite.Plugins.PageTypeElementProvider", "PageType.DeletePageTypeWorkflow.Confirm.Layout.Messeage"), pageType.Name));
        }



        private void showPageReferingCodeActivity_Initialize_ExecuteCode(object sender, EventArgs e)
        {
            IPageType pageType = (IPageType)((DataEntityToken)this.EntityToken).Data;

            this.Bindings.Add("MessageText", string.Format(StringResourceSystemFacade.GetString("Composite.Plugins.PageTypeElementProvider", "PageType.DeletePageTypeWorkflow.PagesRefering.Layout.Message"), pageType.Name));
        }



        private void finalizeCodeActivity_Finalize_ExecuteCode(object sender, EventArgs e)
        {
            IPageType pageType = (IPageType)((DataEntityToken)this.EntityToken).Data;

            DeleteTreeRefresher deleteTreeRefresher = this.CreateDeleteTreeRefresher(this.EntityToken);

            IEnumerable<IPageTypeMetaDataTypeLink> pageTypeMetaDataTypeLinks = 
                DataFacade.GetData<IPageTypeMetaDataTypeLink>().
                Where(f => f.PageTypeId == pageType.Id).
                Evaluate();

            foreach (IPageTypeMetaDataTypeLink pageTypeMetaDataTypeLink in pageTypeMetaDataTypeLinks)
            {
                PageMetaDataFacade.RemoveDefinition(pageType.Id, pageTypeMetaDataTypeLink.Name);
            }

            DataFacade.Delete<IPageType>(pageType);

            deleteTreeRefresher.PostRefreshMesseges();
        }
    }
}
