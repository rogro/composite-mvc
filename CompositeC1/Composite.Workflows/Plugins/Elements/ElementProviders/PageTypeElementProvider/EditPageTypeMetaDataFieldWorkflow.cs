/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Workflow.Activities;
using Composite.C1Console.Actions;
using Composite.C1Console.Events;
using Composite.Data;
using Composite.Data.DynamicTypes;
using Composite.Data.Types;
using Composite.Core.ResourceSystem;
using Composite.C1Console.Workflow;


namespace Composite.Plugins.Elements.ElementProviders.PageTypeElementProvider
{
    [EntityTokenLock()]
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class EditPageTypeMetaDataFieldWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public EditPageTypeMetaDataFieldWorkflow()
        {
            InitializeComponent();
        }



        private void ValidateTypeExistence(object sender, ConditionalEventArgs e)
        {
            e.Result = true;

            IPageTypeMetaDataTypeLink pageTypeMetaDataTypeLink = this.GetDataItemFromEntityToken<IPageTypeMetaDataTypeLink>();

            DataTypeDescriptor dataTypeDescriptor;
            if (!DynamicTypeManager.TryGetDataTypeDescriptor(pageTypeMetaDataTypeLink.DataTypeId, out dataTypeDescriptor))
            {
                e.Result = false;

                DeleteTreeRefresher deleteTreeRefresher = CreateDeleteTreeRefresher(this.EntityToken);

                DataFacade.Delete<IPageTypeMetaDataTypeLink>(pageTypeMetaDataTypeLink);

                this.ShowMessage(
                    DialogType.Warning,
                    GetText("PageType.EditPageTypeMetaDataFieldWorkflow.ValidationError.MetaDataTypeNotExisting.Title"),
                    GetText("PageType.EditPageTypeMetaDataFieldWorkflow.ValidationError.MetaDataTypeNotExisting.Message"));

                deleteTreeRefresher.PostRefreshMesseges();                
            }            
        }



        private void initializeCodeActivity_UpdateBindings_ExecuteCode(object sender, EventArgs e)
        {
            IPageTypeMetaDataTypeLink pageTypeMetaDataTypeLink = this.GetDataItemFromEntityToken<IPageTypeMetaDataTypeLink>();

            IPageMetaDataDefinition pageMetaDataDefinition = PageMetaDataFacade.GetMetaDataDefinition(pageTypeMetaDataTypeLink.PageTypeId, pageTypeMetaDataTypeLink.Name);

            //this.UpdateBinding("CompositionDescriptionName", compositionDescription.Name);
            this.UpdateBinding("CompositionDescriptionLabel", pageMetaDataDefinition.Label);

            List<KeyValuePair<Guid, string>> metaDataContainerOptions = PageMetaDataFacade.GetAllMetaDataContainers();

            this.Bindings.Add("MetaDataContainerOptions", metaDataContainerOptions);
            this.Bindings.Add("CompositionContainerId", pageMetaDataDefinition.MetaDataContainerId);

            var dataTypeDescriptor = DynamicTypeManager.GetDataTypeDescriptor(pageTypeMetaDataTypeLink.DataTypeId);

            this.Bindings.Add("MetaTypeName", dataTypeDescriptor.TypeManagerTypeName);
        }


        private void ValidateBindings(object sender, ConditionalEventArgs e)
        {
            IPageTypeMetaDataTypeLink pageTypeMetaDataTypeLink = this.GetDataItemFromEntityToken<IPageTypeMetaDataTypeLink>();

            IPageMetaDataDefinition pageMetaDataDefinition = PageMetaDataFacade.GetMetaDataDefinition(pageTypeMetaDataTypeLink.PageTypeId, pageTypeMetaDataTypeLink.Name);

            string metaDataDescriptionLabel = this.GetBinding<string>("CompositionDescriptionLabel");
            Guid containerId = this.GetBinding<Guid>("CompositionContainerId");

            e.Result = true;

            if (pageMetaDataDefinition.Label != metaDataDescriptionLabel)
            {
                if (PageMetaDataFacade.IsDefinitionAllowed(pageTypeMetaDataTypeLink.PageTypeId, pageMetaDataDefinition.Name, metaDataDescriptionLabel, pageMetaDataDefinition.MetaDataTypeId) == false)
                {
                    this.ShowFieldMessage("CompositionDescriptionLabel", GetText("PageType.EditPageTypeMetaDataFieldWorkflow.ValidationError.MetaDataFieldNameAlreadyUsed"));
                    SetSaveStatus(false);
                    e.Result = false;
                }
            }

            if (pageMetaDataDefinition.MetaDataContainerId != containerId)
            {
                if (PageMetaDataFacade.IsNewContainerIdAllowed(pageTypeMetaDataTypeLink.PageTypeId, pageMetaDataDefinition.Name, containerId) == false)
                {
                    this.ShowFieldMessage("CompositionContainerId", GetText("PageType.EditPageTypeMetaDataFieldWorkflow.ValidationError.MetaDataContainerChangeNotAllowed"));
                    SetSaveStatus(false);
                    e.Result = false;
                }
            }
        }



        private void saveCodeActivity_Save_ExecuteCode(object sender, EventArgs e)
        {
            IPageTypeMetaDataTypeLink pageTypeMetaDataTypeLink = this.GetDataItemFromEntityToken<IPageTypeMetaDataTypeLink>();
            string metaDataDescriptionLabel = this.GetBinding<string>("CompositionDescriptionLabel");
            Guid containerId = this.GetBinding<Guid>("CompositionContainerId");

            PageMetaDataFacade.UpdateDefinition(pageTypeMetaDataTypeLink.PageTypeId, pageTypeMetaDataTypeLink.Name, metaDataDescriptionLabel, containerId);

            SetSaveStatus(true);
            this.RefreshCurrentEntityToken();
        }

        string GetText(string key)
        {
            return StringResourceSystemFacade.GetString("Composite.Plugins.PageTypeElementProvider", key);
        }
    }
}
