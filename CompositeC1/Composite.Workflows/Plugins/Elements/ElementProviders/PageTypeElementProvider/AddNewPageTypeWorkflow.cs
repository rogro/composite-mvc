/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Linq;
using Composite.Data.Types;
using Composite.Data;
using System.Workflow.Activities;
using Composite.C1Console.Workflow;


namespace Composite.Plugins.Elements.ElementProviders.PageTypeElementProvider
{
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class AddNewPageTypeWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public AddNewPageTypeWorkflow()
        {
            InitializeComponent();
        }



        private void initializeCodeActivity_UpdateBindings_ExecuteCode(object sender, EventArgs e)
        {
            IPageType pageType = DataFacade.BuildNew<IPageType>();
            pageType.Id = Guid.NewGuid();
            pageType.Available = true;
            pageType.PresetMenuTitle = true;
            pageType.HomepageRelation = PageTypeHomepageRelation.NoRestriction.ToPageTypeHomepageRelationString();
            pageType.DefaultTemplateId = Guid.Empty;
            pageType.DefaultChildPageType = Guid.Empty;

            this.Bindings.Add("NewPageType", pageType);
        }



        private void ValidateBindings(object sender, ConditionalEventArgs e)
        {
            e.Result = true;
        }



        private void finalizeCodeActivity_SavePageType_ExecuteCode(object sender, EventArgs e)
        {
            IPageType pageType = this.GetBinding<IPageType>("NewPageType");

            pageType = DataFacade.AddNew<IPageType>(pageType);

            this.RefreshCurrentEntityToken();

            this.CloseCurrentView();
            this.RefreshCurrentEntityToken();
            this.ExecuteWorklow(pageType.GetDataEntityToken(), WorkflowFacade.GetWorkflowType("Composite.Plugins.Elements.ElementProviders.PageTypeElementProvider.EditPageTypeWorkflow"));
        }
    }
}
