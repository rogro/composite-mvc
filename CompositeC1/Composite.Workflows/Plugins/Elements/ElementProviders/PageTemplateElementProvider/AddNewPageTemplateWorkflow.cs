/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Composite.C1Console.Workflow;
using Composite.Core.Extensions;
using Composite.Core.PageTemplates;
using Composite.Core.ResourceSystem;


namespace Composite.Plugins.Elements.ElementProviders.PageTemplateElementProvider
{
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class AddNewPageTemplateWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        private static readonly string Binding_TemplateTypeOptions = "TemplateTypeOptions";
        private static readonly string Binding_TemplateTypeId = "TemplateTypeId";

        public AddNewPageTemplateWorkflow()
        {
            InitializeComponent();
        }

        private void codeActivity1_ExecuteCode(object sender, EventArgs e)
        {
            var templateTypes = new List<Tuple<string, string, int>>();

            foreach (var providerInfo in PageTemplateFacade.GetProviders())
            {
                var provider = providerInfo.Value;

                if (provider.AddNewTemplateWorkflow == null) continue;

                templateTypes.Add(new Tuple<string, string, int>(
                    providerInfo.Key, !provider.AddNewTemplateLabel.IsNullOrEmpty()
                        ? StringResourceSystemFacade.ParseString(provider.AddNewTemplateLabel) 
                        : providerInfo.Key,
                    provider.GetPageTemplates().Count()));
            }
            
            Verify.That(templateTypes.Any(), "No page templates supporting adding new templates defined in configuration");

            // Most used page template type will be first in the list and preselected
            string preseceltedTemplate = templateTypes.OrderByDescending(t => t.Item3).Select(t => t.Item1).First();

            List<KeyValuePair<string, string>> options = templateTypes
                .OrderBy(t => t.Item2) // Sorting alphabetically by label 
                .Select(t => new KeyValuePair<string, string>(t.Item1, t.Item2)).ToList();

            this.Bindings.Add(Binding_TemplateTypeOptions, options);
            this.Bindings.Add(Binding_TemplateTypeId, preseceltedTemplate);
        }


        private void codeActivity2_ExecuteCode(object sender, EventArgs e)
        {
            string providerName = this.GetBinding<string>(Binding_TemplateTypeId);

            Type workflowType = PageTemplateFacade.GetProviders()
                                                  .First(p => p.Key == providerName)
                                                  .Value.AddNewTemplateWorkflow;

            this.ExecuteAction(new PageTemplateRootEntityToken(), new WorkflowActionToken(workflowType));
        }
    }
}
