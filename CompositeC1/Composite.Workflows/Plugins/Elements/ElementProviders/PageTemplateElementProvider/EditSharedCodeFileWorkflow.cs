/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using Composite.C1Console.Workflow;
using Composite.Core.IO;
using Composite.Core.PageTemplates;
using Composite.Plugins.Elements.ElementProviders.WebsiteFileElementProvider;

namespace Composite.Plugins.Elements.ElementProviders.PageTemplateElementProvider
{
    [EntityTokenLock()]
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class EditSharedCodeFileWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        private string GetFilePath()
        {
            var entityToken = (SharedCodeFileEntityToken)this.EntityToken;

            string relativeFilePath = entityToken.VirtualPath;

            // Security check that validates that the file is a Shared code file 
            var sharedFiles = PageTemplateFacade.GetSharedFiles();

            Verify.That(sharedFiles.Any(sf => string.Compare(sf.RelativeFilePath, relativeFilePath, StringComparison.OrdinalIgnoreCase) == 0),
                        "There's no page template provider that would claim ownership over shared code file '{0}'");

            return PathUtil.Resolve(relativeFilePath);
        }

        public EditSharedCodeFileWorkflow()
        {
            InitializeComponent();
        }

        private void initializeCodeActivity_ExecuteCode(object sender, EventArgs e)
        {
            string filePath = GetFilePath();

            WebsiteFile websiteFile = new WebsiteFile(filePath);

            this.Bindings.Add("FileContent", websiteFile.ReadAllText());
            this.Bindings.Add("FileName", websiteFile.FileName);
            this.Bindings.Add("FileMimeType", websiteFile.MimeType);
        }


        private void saveCodeActivity_ExecuteCode(object sender, EventArgs e)
        {
            string filePath = GetFilePath();

            WebsiteFile websiteFile = new WebsiteFile(filePath);

            string content = this.GetBinding<string>("FileContent");

            websiteFile.WriteAllText(content);

            SetSaveStatus(true);
        }
    }
}
