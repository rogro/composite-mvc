/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.IO;
using Composite.C1Console.Workflow;
using Composite.Core.Configuration;
using Composite.Core.IO;
using Composite.Core.ResourceSystem;
using Composite.Core.Xml;
using Composite.Core.WebClient.Renderings.Template;
using System.Xml.Linq;


namespace Composite.Plugins.Elements.ElementProviders.PageTemplateFeatureElementProvider
{
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class AddPageTemplateFeatureWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public AddPageTemplateFeatureWorkflow()
        {
            InitializeComponent();
        }



        private void initializeCodeActivity_Initialize_ExecuteCode(object sender, EventArgs e)
        {
            this.Bindings.Add("Name", "");
            this.Bindings.Add("EditorType", "html");

            Dictionary<string, string> editorOptions = new Dictionary<string, string>();
            editorOptions.Add("html", StringResourceSystemFacade.GetString("Composite.Plugins.PageTemplateFeatureElementProvider", "AddWorkflow.LabelTemplateFeatureEditorType.html"));
            editorOptions.Add("xml", StringResourceSystemFacade.GetString("Composite.Plugins.PageTemplateFeatureElementProvider", "AddWorkflow.LabelTemplateFeatureEditorType.xml"));

            this.Bindings.Add("EditorTypeOptions", editorOptions);
        }



        private void finalizeCodeActivity_Finalize_ExecuteCode(object sender, EventArgs e)
        {
            string name = this.GetBinding<string>("Name");
            string editorType = this.GetBinding<string>("EditorType");

            string filename = PageTemplateFeatureFacade.GetNewPageTemplateFeaturePath(name, editorType);

            XhtmlDocument template = new XhtmlDocument();
            template.Root.Add(new XAttribute(XNamespace.Xmlns + "f", Namespaces.Function10));
            template.Head.Add("");
            template.Body.Add(new XElement(Namespaces.Xhtml + "p", ""));

            C1File.WriteAllText(filename, template.ToString());

            this.CloseCurrentView();
            this.RefreshRootEntityToken();

            this.ExecuteAction(PageTemplateFeatureEntityToken.BuildFeatureEntityToken(name),
                new WorkflowActionToken(typeof(EditPageTemplateFeatureWorkflow)));
        }



        private void IfFeatureNameFree(object sender, System.Workflow.Activities.ConditionalEventArgs e)
        {
            string name = this.GetBinding<string>("Name");

            if (name.Length > 50)
            {
                e.Result = false;
                this.ShowFieldMessage("Name", StringResourceSystemFacade.GetString("Composite.Plugins.PageTemplateFeatureElementProvider", "AddWorkflow.NameTooLong"));
                return;
            }

            if (!C1Directory.Exists(PathUtil.Resolve(GlobalSettingsFacade.PageTemplateFeaturesDirectory)))
            {
                try
                {
                    C1Directory.CreateDirectory(PathUtil.Resolve(GlobalSettingsFacade.PageTemplateFeaturesDirectory));
                }
                catch (Exception)
                {
                    e.Result = false;
                    this.ShowFieldMessage("Name", string.Format("Can not create directory '{0}'", GlobalSettingsFacade.PageTemplateFeaturesDirectory));
                }
            }

            string xmlFilename = Path.Combine(PathUtil.Resolve(GlobalSettingsFacade.PageTemplateFeaturesDirectory), name + ".xml");
            string htmlFilename = Path.Combine(PathUtil.Resolve(GlobalSettingsFacade.PageTemplateFeaturesDirectory), name + ".html");

            e.Result = !C1File.Exists(xmlFilename) && !C1File.Exists(htmlFilename);

            if (e.Result == false)
            {
                this.ShowFieldMessage("Name", StringResourceSystemFacade.GetString("Composite.Plugins.PageTemplateFeatureElementProvider", "AddWorkflow.NameInUse"));
                return;
            }

            try
            {
                C1File.WriteAllText(xmlFilename, "tmp");
                C1File.Delete(xmlFilename);
            }
            catch (Exception)
            {
                e.Result = false;
                this.ShowFieldMessage("Name", StringResourceSystemFacade.GetString("Composite.Plugins.PageTemplateFeatureElementProvider", "AddWorkflow.NameNotValidInFilename"));
            }
        }
    }
}
