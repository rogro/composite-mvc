/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.IO;
using Composite.C1Console.Events;
using Composite.C1Console.Workflow;
using Composite.Core.IO;
using Composite.Core.WebClient.Renderings.Template;
using Composite.Core.Xml;



namespace Composite.Plugins.Elements.ElementProviders.PageTemplateFeatureElementProvider
{
    [EntityTokenLock()]
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class EditPageTemplateFeatureWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public EditPageTemplateFeatureWorkflow()
        {
            InitializeComponent();
        }



        private void initializeCodeActivity_ExecuteCode(object sender, EventArgs e)
        {
            string markup;

            if (C1File.Exists(this.FilePath))
            {
                markup = C1File.ReadAllText(this.FilePath);
            }
            else
            {
                // someone deleted the feature file, but that won't stop us!
                XhtmlDocument template = new XhtmlDocument();
                template.Head.Add("");
                template.Body.Add("");
                markup = template.ToString();
            }

            this.Bindings.Add("FeatureName", this.FeatureName);
            this.Bindings.Add("Markup", markup);

            if (Path.GetExtension(this.FilePath)==".html")
            {
                this.documentFormActivity1.FormDefinitionFileName = @"\Administrative\PageTemplateFeature\EditVisual.xml";
            }
            else
            {
                this.documentFormActivity1.FormDefinitionFileName = @"\Administrative\PageTemplateFeature\EditMarkup.xml";
            }
        }



        private void saveStateCodeActivity_ExecuteCode(object sender, EventArgs e)
        {
            string content = this.GetBinding<string>("Markup");

            FileUtils.RemoveReadOnly(this.FilePath);

            C1File.WriteAllText(this.FilePath, content);

            this.SetSaveStatus(true);
        }


        private string FeatureName
        {
            get
            {
                PageTemplateFeatureEntityToken castedEntityToken = (PageTemplateFeatureEntityToken)this.EntityToken;
                return castedEntityToken.FeatureName;
            }
        }


        private string FilePath
        {
            get
            {
                PageTemplateFeatureEntityToken castedEntityToken = (PageTemplateFeatureEntityToken)this.EntityToken;
                return PageTemplateFeatureFacade.GetPageTemplateFeaturePath(castedEntityToken.FeatureName);
            }
        }


        private void IsMarkupValid(object sender, System.Workflow.Activities.ConditionalEventArgs e)
        {
            PageTemplateFeatureEntityToken castedEntityToken = (PageTemplateFeatureEntityToken)this.EntityToken;

            string content = this.GetBinding<string>("Markup");
            
            this.UpdateBinding("Errors", "");

            XhtmlDocument document = null;
            try
            {
                document = XhtmlDocument.Parse(content);
            }
            catch (Exception ex)
            {
                this.UpdateBinding("Errors", ex.Message);
                e.Result = false;
                return;
            }

            e.Result = true;
        }




        private void editCodeActivity_ShowErrorMessage_ExecuteCode(object sender, EventArgs e)
        {
            
            this.ShowMessage(DialogType.Error, "Error", this.GetBinding<string>("Errors")); 
        }
    }
}
