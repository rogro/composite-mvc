/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using Composite.C1Console.Actions;
using Composite.C1Console.Events;
using Composite.C1Console.Workflow;
using Composite.Core.Extensions;
using Composite.Core.IO;
using Composite.Core.Logging;
using Composite.Core.ResourceSystem;
using Composite.Data;
using Composite.Data.Plugins.DataProvider.Streams;
using Composite.Data.Types;
using Composite.Plugins.Elements.ElementProviders.Common;


namespace Composite.Plugins.Elements.ElementProviders.XsltBasedFunctionProviderElementProvider
{
    [EntityTokenLock()]
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class DeleteXsltFunctionWorkflow : BaseFunctionWorkflow
    {
        public DeleteXsltFunctionWorkflow()
        {
            InitializeComponent();
        }

        private void DeleteFile(IFile file)
        {
            FileSystemFileBase baseFile = file as FileSystemFileBase;
            if(baseFile == null) return;

            string filePath = baseFile.SystemPath;
            try
            {
                C1File.Delete(filePath);
            }
            catch
            {
                LoggingService.LogWarning(typeof(DeleteXsltFunctionWorkflow).Name, "Failed to delete file '{0}'".FormatWith(filePath));
            }
        }

        private void codeActivity1_ExecuteCode(object sender, EventArgs e)
        {            
            DataEntityToken dataEntityToken = (DataEntityToken)this.EntityToken;

            IXsltFunction xsltFunction = (IXsltFunction)dataEntityToken.Data;

            if (DataFacade.WillDeleteSucceed<IXsltFunction>(xsltFunction))
            {
                DeleteTreeRefresher deleteTreeRefresher = this.CreateDeleteTreeRefresher(this.EntityToken);


                IFile file = IFileServices.TryGetFile<IXsltFile>(xsltFunction.XslFilePath);
                DataFacade.Delete<IParameter>(f => f.OwnerId == xsltFunction.Id);
                DataFacade.Delete<INamedFunctionCall>(f => f.XsltFunctionId == xsltFunction.Id);
                DataFacade.Delete(xsltFunction);

                DeleteFile(file);

                int count =
                    (from info in DataFacade.GetData<IXsltFunction>()
                     where info.Namespace == xsltFunction.Namespace
                     select info).Count();

                if (count == 0)
                {
                    RefreshFunctionTree();
                }
                else
                {
                    deleteTreeRefresher.PostRefreshMesseges();
                }
            }
            else
            {
                this.ShowMessage(
                        DialogType.Error,
                        StringResourceSystemFacade.GetString("Composite.Plugins.XsltBasedFunction", "DeleteXsltFunctionWorkflow.CascadeDeleteErrorTitle"),
                        StringResourceSystemFacade.GetString("Composite.Plugins.XsltBasedFunction", "DeleteXsltFunctionWorkflow.CascadeDeleteErrorMessage")
                    );
            }
        }
    }
}
