/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using Composite.C1Console.Users;
using Composite.Core.Configuration;
using Composite.Core.PackageSystem;
using Composite.Core.PackageSystem.Workflow;
using Composite.C1Console.Forms.DataServices;
using Composite.C1Console.Workflow;
using Composite.C1Console.Events;


namespace Composite.Plugins.Elements.ElementProviders.PackageElementProvider
{
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class ViewInstalledPackageInfoWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public ViewInstalledPackageInfoWorkflow()
        {
            InitializeComponent();
        }



        private void viewStateCodeActivity_Initialize_ExecuteCode(object sender, EventArgs e)
        {
            var castedToken = (PackageElementProviderInstalledPackageItemEntityToken)this.EntityToken;

            if (!this.BindingExist("InstalledPackageInformation"))
            {                
                InstalledPackageInformation installedAddOnInformation =
                    (from info in PackageManager.GetInstalledPackages()
                     where info.Id == castedToken.PackageId
                     select info).Single();

                string name = installedAddOnInformation.Name;
                string documentTitle = (name.Contains('.') && !name.EndsWith(".") ?
                    string.Format("{0} ({1})", name.Substring(name.LastIndexOf('.') + 1), name.Substring(0,name.LastIndexOf('.'))) : 
                    name);

                this.Bindings.Add("DocumentTitle", documentTitle);
                this.Bindings.Add("InstalledPackageInformation", installedAddOnInformation);
                this.Bindings.Add("InstallDate", installedAddOnInformation.InstallDate.ToLocalTime().ToString());

                PackageLicenseDefinition licenseInfo = PackageLicenseHelper.GetLicenseDefinition(installedAddOnInformation.Id);
                bool isTrial = (licenseInfo != null && !licenseInfo.Permanent);
                this.Bindings.Add("IsTrial", isTrial);

                this.Bindings.Add("ShowPurchaseThisButton", isTrial && !string.IsNullOrWhiteSpace(licenseInfo.PurchaseUrl));

                PackageDescription packageDescription = null;
                try
                {
                    Guid packageId = new Guid(castedToken.Id);

                    var allPackages = PackageServerFacade.GetAllPackageDescriptions(InstallationInformationFacade.InstallationId,
                            UserSettings.CultureInfo);

                    packageDescription = allPackages.FirstOrDefault(p => p.Id == packageId);
                }
                catch
                {
                }

                if (packageDescription != null && !string.IsNullOrEmpty(packageDescription.ReadMoreUrl))
                {
                    this.Bindings.Add("ReadMoreUrl", packageDescription.ReadMoreUrl);
                }

                if (isTrial)
                {
                    this.Bindings.Add("TrialExpire", licenseInfo.Expires.ToLocalTime().ToString());
                    if (!string.IsNullOrWhiteSpace(licenseInfo.PurchaseUrl))
                    {
                        //string url = string.Format("{0}{1}installationId={2}", licenseInfo.PurchaseUrl, (licenseInfo.PurchaseUrl.Contains('?') ? "&" : "?"), Composite.Core.Configuration.InstallationInformationFacade.InstallationId);
                        string url = licenseInfo.PurchaseUrl;
                        this.Bindings.Add("TrialPurchaseUrl", url);
                    }

                }
            }

            if (castedToken.CanBeUninstalled)
            {
                this.SetCustomToolbarDefinition(new FormDefinitionFileMarkupProvider(@"\Administrative\PackageElementProviderViewInstalledPackageInformationToolbar.xml"));
            }
        }



        private void uninstallCodeActivity_ExecuteCode(object sender, EventArgs e)
        {
            var castedToken = (PackageElementProviderInstalledPackageItemEntityToken)this.EntityToken;

            InstalledPackageInformation installedAddOnInformation =
                (from info in PackageManager.GetInstalledPackages()
                 where info.Id == castedToken.PackageId
                 select info).FirstOrDefault();

            if (installedAddOnInformation != null)
            {
                if (installedAddOnInformation.IsLocalInstalled)
                {
                    this.ExecuteWorklow(this.EntityToken, typeof(UninstallLocalPackageWorkflow));
                }
                else
                {
                    this.ExecuteWorklow(this.EntityToken, typeof(UninstallRemotePackageWorkflow));
                }
            }
            else
            {
                this.ShowMessage(
                    DialogType.Message,
                    "${Composite.Plugins.PackageElementProvider, ViewInstalledInformation.ShowError.MessageTitle}",
                    "${Composite.Plugins.PackageElementProvider, ViewInstalledInformation.ShowError.MessageMessage}");
            }   
        }
    }
}
