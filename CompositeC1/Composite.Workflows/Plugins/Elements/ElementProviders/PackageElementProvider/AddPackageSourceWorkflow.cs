/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using Composite.C1Console.Workflow;
using Composite.Core.PackageSystem;
using Composite.Data.Types;
using Composite.Data;
using Composite.C1Console.Actions;


namespace Composite.Plugins.Elements.ElementProviders.PackageElementProvider
{
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class AddPackageSourceWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        private bool _urlIsValid = false;


        public AddPackageSourceWorkflow()
        {
            InitializeComponent();
        }



        private void IsUrlValid(object sender, System.Workflow.Activities.ConditionalEventArgs e)
        {
            e.Result = _urlIsValid;
        }



        private void initializeCodeActivity_Initialize_ExecuteCode(object sender, EventArgs e)
        {
            this.Bindings.Add("Url", "");
            this.Bindings.Add("HttpOnly", true);
        }



        private void step1CodeActivity_ValidateServerUrl_ExecuteCode(object sender, EventArgs e)
        {
            string url = this.GetBinding<string>("Url");

            try
            {
                UriBuilder uriBuilder = new UriBuilder(url);

                string cleanedUrl = uriBuilder.Uri.ToString().Remove(0, uriBuilder.Scheme.Length + 3);
                if (cleanedUrl.EndsWith("/"))
                {
                    cleanedUrl = cleanedUrl.Remove(cleanedUrl.Length - 1);
                }

                ServerUrlValidationResult serverUrlValidationResult = PackageServerFacade.ValidateServerUrl(cleanedUrl);

                _urlIsValid = true;
                if (serverUrlValidationResult == ServerUrlValidationResult.Invalid)
                {
                    this.ShowFieldMessage("Url", "${Composite.Plugins.PackageElementProvider, AddPackageSource.Step1.UrlNonPackageServer}");
                    _urlIsValid = false;
                }
                else if (serverUrlValidationResult == ServerUrlValidationResult.Https)
                {
                    cleanedUrl = string.Format("https://{0}", cleanedUrl);
                    this.UpdateBinding("HttpOnly", false);
                }
                else if (serverUrlValidationResult == ServerUrlValidationResult.Http)
                {
                    cleanedUrl = string.Format("http://{0}", cleanedUrl);
                }

                this.UpdateBinding("CleanedUrl", cleanedUrl);
            }
            catch (Exception)
            {
                this.ShowFieldMessage("Url", "${Composite.Plugins.PackageElementProvider, AddPackageSource.Step1.UrlNotValid}");
            }
        }



        private void step2CodeActivity_Finalize_ExecuteCode(object sender, EventArgs e)
        {
            IPackageServerSource packageServerSource = DataFacade.BuildNew<IPackageServerSource>();
            packageServerSource.Id = Guid.NewGuid();
            packageServerSource.Url = this.GetBinding<string>("CleanedUrl");

            DataFacade.AddNew<IPackageServerSource>(packageServerSource);

            SpecificTreeRefresher specificTreeRefresher = this.CreateSpecificTreeRefresher();
            specificTreeRefresher.PostRefreshMesseges(new PackageElementProviderRootEntityToken());
        }
    }
}
