/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.IO;
using System.Workflow.Activities;
using Composite.C1Console.Actions;
using Composite.C1Console.Elements;
using Composite.C1Console.Events;
using Composite.C1Console.Forms.CoreUiControls;
using Composite.C1Console.Workflow;
using Composite.Core.IO;


namespace Composite.Plugins.Elements.ElementProviders.WebsiteFileElementProvider
{
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class UploadWebsiteFileWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public UploadWebsiteFileWorkflow()
        {
            InitializeComponent();
        }


        private string GetCurrentPath()
        {
            if (this.EntityToken is WebsiteFileElementProviderRootEntityToken)
            {
                string rootPath = (string)ElementFacade.GetData(new ElementProviderHandle(this.EntityToken.Source), "RootPath");

                return rootPath;
            }
            else if (this.EntityToken is WebsiteFileElementProviderEntityToken)
            {
                return (this.EntityToken as WebsiteFileElementProviderEntityToken).Path;
            }
            else
            {
                throw new NotImplementedException();
            }
        }



        private void IsUploadTypeOk(object sender, ConditionalEventArgs e)
        {
            e.Result = true;
        }



        private void FileExist(object sender, ConditionalEventArgs e)
        {
            UploadedFile file = this.GetBinding<UploadedFile>("UploadedFile");

            if (file.HasFile)
            {
                string currentPath = GetCurrentPath();

                e.Result = C1Directory.GetFiles(currentPath, file.FileName).Length > 0;
            }
            else
            {
                e.Result = false;
            }
        }



        private void initializeCodeActivity_Initialize_ExecuteCode(object sender, EventArgs e)
        {
            UploadedFile file = new UploadedFile();

            this.Bindings.Add("UploadedFile", file);
        }



        private void finalizeCodeActivity_SaveFile_ExecuteCode(object sender, EventArgs e)
        {
            UploadedFile uploadedFile = this.GetBinding<UploadedFile>("UploadedFile");

            if (uploadedFile.HasFile)
            {
                string currentPath = GetCurrentPath();
                string filename = uploadedFile.FileName;

                string fullFilename = System.IO.Path.Combine(currentPath, filename);

                if (C1File.Exists(fullFilename))
                {
                    FileUtils.Delete(fullFilename);
                }

                using (C1FileStream fs = new C1FileStream(fullFilename, FileMode.CreateNew))
                {
                    uploadedFile.FileStream.CopyTo(fs);
                }

                SpecificTreeRefresher specificTreeRefresher = this.CreateSpecificTreeRefresher();
                specificTreeRefresher.PostRefreshMesseges(this.EntityToken);

                if (this.EntityToken is WebsiteFileElementProviderEntityToken)
                {
                    WebsiteFileElementProviderEntityToken folderToken = (WebsiteFileElementProviderEntityToken)this.EntityToken;
                    var newFileToken = new WebsiteFileElementProviderEntityToken(folderToken.ProviderName, fullFilename, folderToken.RootPath);
                    SelectElement(newFileToken);
                }
            }
        }



        private void finalizeCodeActivity_ShowErrorMessage_ExecuteCode(object sender, EventArgs e)
        {
            this.ShowMessage(
                DialogType.Error,
                "${Composite.Plugins.WebsiteFileElementProvider, UploadFile.Error.WrongTypeTitle}",
                "${Composite.Plugins.WebsiteFileElementProvider, UploadFile.Error.WrongTypeMessage}"
            );
        }



        //private void finalizeCodeActivity_ShowFileExistMessage_ExecuteCode(object sender, EventArgs e)
        //{
        //    this.ShowMessage(
        //        DialogType.Error,
        //        "${Composite.Plugins.WebsiteFileElementProvider, UploadFile.Error.FileExistTitle}",
        //        "${Composite.Plugins.WebsiteFileElementProvider, UploadFile.Error.FileExistMessage}"
        //    );
        //}
    }
}
