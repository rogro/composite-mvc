/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using Composite.Data;
using Composite.Data.Types;
using System.Collections.Generic;
using System.Transactions;
using Composite.Data.Transactions;
using Composite.Data.ProcessControlled;


namespace Composite.Plugins.Elements.ElementProviders.PageElementProvider
{
    public sealed partial class UndoUnpublishedChangesWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public UndoUnpublishedChangesWorkflow()
        {
            InitializeComponent();
        }

        private void undoCodeActivity_Undo_ExecuteCode(object sender, EventArgs e)
        {
            DataEntityToken dataEntityToken = (DataEntityToken)this.EntityToken;

            List<string> propertyNamesToIgnore = new List<string> { "PublicationStatus" };

            using (TransactionScope transactionScope = TransactionsFacade.CreateNewScope())
            {
                IPage administrativePage = (IPage)dataEntityToken.Data;
                IPage publicPage = DataFacade.GetDataFromOtherScope<IPage>(administrativePage, DataScopeIdentifier.Public).Single();

                List<IData> administrativeCompositions = administrativePage.GetMetaData(DataScopeIdentifier.Administrated).ToList();
                List<IData> publicCompositions = publicPage.GetMetaData(DataScopeIdentifier.Public).ToList();

                List<IPagePlaceholderContent> administrativePlaceholders;
                using (DataScope dataScope = new DataScope(DataScopeIdentifier.Administrated))
                {
                    administrativePlaceholders =
                        (from ph in DataFacade.GetData<IPagePlaceholderContent>()
                         where ph.PageId == administrativePage.Id
                         select ph).ToList();
                }

                List<IPagePlaceholderContent> publicPlaceholders;
                using (DataScope dataScope = new DataScope(DataScopeIdentifier.Public))
                {
                    publicPlaceholders =
                        (from ph in DataFacade.GetData<IPagePlaceholderContent>()
                         where ph.PageId == publicPage.Id
                         select ph).ToList();
                }

                using (ProcessControllerFacade.NoProcessControllers)
                {
                    publicPage.FullCopyChangedTo(administrativePage, propertyNamesToIgnore);
                    DataFacade.Update(administrativePage);

                    foreach (IData publicComposition in publicCompositions)
                    {
                        IData administrativeComposition =
                            (from com in administrativeCompositions
                             where com.DataSourceId.DataId.CompareTo(publicComposition.DataSourceId.DataId, false) 
                             select com).Single();

                        publicComposition.FullCopyChangedTo(administrativeComposition, propertyNamesToIgnore);
                        DataFacade.Update(administrativeComposition);
                    }

                    foreach (IPagePlaceholderContent publicPagePlaceholderContent in publicPlaceholders)
                    {
                        IPagePlaceholderContent administrativePagePlaceholderContent =
                            (from pc in administrativePlaceholders
                             where pc.PlaceHolderId == publicPagePlaceholderContent.PlaceHolderId
                             select pc).Single();

                        publicPagePlaceholderContent.FullCopyChangedTo(administrativePagePlaceholderContent, propertyNamesToIgnore);
                        DataFacade.Update(administrativePagePlaceholderContent);
                    }
                }

                transactionScope.Complete();
            }
        }
    }
}
