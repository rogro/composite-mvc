/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using Composite.C1Console.Actions;
using Composite.C1Console.Events;
using Composite.Data;
using Composite.Data.Types;
using Composite.Core.ResourceSystem;
using Composite.C1Console.Workflow;


namespace Composite.Plugins.Elements.ElementProviders.SqlFunctionElementProvider
{
    [EntityTokenLock()]
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class DeleteSqlFunctionProviderWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public DeleteSqlFunctionProviderWorkflow()
        {
            InitializeComponent();
        }




        private void finalizeCodeActivity_ExecuteCode(object sender, EventArgs e)
        {
            DataEntityToken token = this.EntityToken as DataEntityToken;

            ISqlFunctionInfo sqlFunctionInfo = (ISqlFunctionInfo)token.Data;

            if (DataFacade.WillDeleteSucceed(token.Data))
            {
                DeleteTreeRefresher treeRefresher = this.CreateDeleteTreeRefresher(this.EntityToken);

                DataFacade.Delete(sqlFunctionInfo);

                int count =
                    (from info in DataFacade.GetData<ISqlFunctionInfo>()
                     where info.Namespace == sqlFunctionInfo.Namespace
                     select info).Count();                

                if (count == 0)
                {
                    ISqlConnection sqlConnection =
                        (from connection in DataFacade.GetData<ISqlConnection>()
                         where connection.Id == sqlFunctionInfo.ConnectionId
                         select connection).Single();

                    SpecificTreeRefresher specificTreeRefresher = this.CreateSpecificTreeRefresher();
                    specificTreeRefresher.PostRefreshMesseges(sqlConnection.GetDataEntityToken());
                }
                else
                {                    
                    treeRefresher.PostRefreshMesseges();
                }
            }
            else
            {
                this.ShowMessage(
                        DialogType.Error,
                        StringResourceSystemFacade.GetString("Composite.Plugins.SqlFunction", "CascadeDeleteErrorTitle"),
                        StringResourceSystemFacade.GetString("Composite.Plugins.SqlFunction", "CascadeDeleteErrorMessage")
                    );
            }            
        }
    }
}
