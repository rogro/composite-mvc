/*

The contents of this web application are subject to the Mozilla Public License Version 1.1 (the "License"); you may not use this web application except in compliance with the License. 
You may obtain a copy of the License at http://www.mozilla.org/MPL/.

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
See the License for the specific language governing rights and limitations under the License.

The Original Code is owned by and the Initial Developer of the Original Code is Composite A/S (Danish business reg.no. 21744409). All Rights Reserved

Section 11 of the License is EXPRESSLY amended to include a provision stating that any dispute, including but not limited to disputes related to the enforcement of the License, to which Composite A/S as owner of the Original Code, as Initial Developer or in any other role, becomes a part to shall be governed by Danish law and be initiated before the Copenhagen City Court ("K�benhavns Byret")
                        
*/

using System;
using System.Linq;
using System.Transactions;
using Composite.C1Console.Actions;
using Composite.Data;
using Composite.Data.Types;
using Composite.Data.Transactions;
using Composite.C1Console.Workflow;


namespace Composite.Plugins.Elements.ElementProviders.SqlFunctionElementProvider
{
    [EntityTokenLock()]
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class DeleteSqlConnectionWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public DeleteSqlConnectionWorkflow()
        {
            InitializeComponent();
        }


      
        private void finalizeCodeActivity_ExecuteCode(object sender, EventArgs e)
        {
            DeleteTreeRefresher deleteTreeRefresher = this.CreateDeleteTreeRefresher(this.EntityToken);

            DataEntityToken dataEntityToken = (DataEntityToken)this.EntityToken;

            ISqlConnection connection = (ISqlConnection)dataEntityToken.Data;

            var queries = from item in DataFacade.GetData<ISqlFunctionInfo>()
                          where item.ConnectionId == connection.Id
                          select item;
            using (TransactionScope transationScope = TransactionsFacade.CreateNewScope())
            {
                DataFacade.Delete(connection);
                foreach (ISqlFunctionInfo query in queries)
                {
                    DataFacade.Delete(query);
                }
                transationScope.Complete();
            }

            deleteTreeRefresher.PostRefreshMesseges();
        }
    }
}
