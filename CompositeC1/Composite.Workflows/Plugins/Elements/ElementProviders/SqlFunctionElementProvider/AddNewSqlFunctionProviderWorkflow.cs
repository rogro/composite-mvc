/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Linq;
using Composite.C1Console.Actions;
using Composite.Data;
using Composite.Data.Types;
using Composite.C1Console.Users;
using Composite.C1Console.Workflow;


namespace Composite.Plugins.Elements.ElementProviders.SqlFunctionElementProvider
{
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class AddNewSqlFunctionProviderWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public AddNewSqlFunctionProviderWorkflow()
        {
            InitializeComponent();
        }



        private void initializeActivity_ExecuteCode(object sender, EventArgs e)
        {
            string namespaceName;
            ISqlConnection sqlConnection;

            if ((this.EntityToken is DataEntityToken))
            {
                DataEntityToken dataEntityToken = (DataEntityToken)this.EntityToken;

                namespaceName = UserSettings.LastSpecifiedNamespace;
                sqlConnection = (ISqlConnection)dataEntityToken.Data;
            }
            else if ((this.EntityToken is SqlFunctionProviderFolderEntityToken))
            {
                Guid connectionId = new Guid(((SqlFunctionProviderFolderEntityToken)this.EntityToken).ConnectionId);

                namespaceName = this.EntityToken.Id;
                sqlConnection = DataFacade.GetData<ISqlConnection>(x => x.Id == connectionId).First();                
            }
            else
            {
                throw new NotImplementedException();
            }
             

            ISqlFunctionInfo sqlFunctionInfo = DataFacade.BuildNew<ISqlFunctionInfo>();
            sqlFunctionInfo.IsQuery = true;
            sqlFunctionInfo.ConnectionId = sqlConnection.Id;
            sqlFunctionInfo.Id = Guid.NewGuid();
            sqlFunctionInfo.Name = "";
            sqlFunctionInfo.Namespace = namespaceName;
            sqlFunctionInfo.Description = "";

            this.Bindings.Add("NewSqlQueryInfo", sqlFunctionInfo);
        }



        private void finalizeActivity_ExecuteCode(object sender, EventArgs e)
        {
            AddNewTreeRefresher addNewTreeRefresher = this.CreateAddNewTreeRefresher(this.EntityToken);

            ISqlFunctionInfo sqlFunctionInfo = this.GetBinding<ISqlFunctionInfo>("NewSqlQueryInfo");

            sqlFunctionInfo = DataFacade.AddNew<ISqlFunctionInfo>(sqlFunctionInfo);

            UserSettings.LastSpecifiedNamespace = sqlFunctionInfo.Namespace;

            addNewTreeRefresher.PostRefreshMesseges(sqlFunctionInfo.GetDataEntityToken());

            this.ExecuteWorklow(sqlFunctionInfo.GetDataEntityToken(), typeof(EditSqlFunctionProviderWorkflow));
        }
    }
}
