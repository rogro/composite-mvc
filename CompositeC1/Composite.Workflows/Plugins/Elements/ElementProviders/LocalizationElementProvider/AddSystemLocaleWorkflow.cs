/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Workflow.Activities;
using Composite.C1Console.Actions;
using Composite.C1Console.Events;
using Composite.C1Console.Workflow;
using Composite.Core.Localization;
using Composite.Core.ResourceSystem;
using Composite.Data;
using Composite.Data.Types;


namespace Composite.Plugins.Elements.ElementProviders.LocalizationElementProvider
{
    [EntityTokenLock()]
    [AllowPersistingWorkflow(WorkflowPersistingType.Idle)]
    public sealed partial class AddSystemLocaleWorkflow : Composite.C1Console.Workflow.Activities.FormsWorkflow
    {
        public AddSystemLocaleWorkflow()
        {
            InitializeComponent();
        }



        private void HasAnyWhiteListedLocales(object sender, ConditionalEventArgs e)
        {
            List<string> alreadyAddedCultureNames =
                (from d in DataFacade.GetData<ISystemActiveLocale>()
                 select d.CultureName).ToList();

            IEnumerable<CultureInfo> cultures =
                from cul in DataLocalizationFacade.WhiteListedLocales
                where alreadyAddedCultureNames.Contains(cul.Name) == false
                orderby cul.DisplayName
                select cul;

            e.Result = cultures.Any();
        }



        private void UrlMappingNameInUse(object sender, ConditionalEventArgs e)
        {
            string urlMappingName = this.GetBinding<string>("UrlMappingName");

            e.Result = LocalizationFacade.IsUrlMappingNameInUse(urlMappingName);
        }



        private void initializeCodeActivity_Initialize_ExecuteCode(object sender, EventArgs e)
        {
            List<string> alreadyAddedCultureNames =
                (from d in DataFacade.GetData<ISystemActiveLocale>()
                 select d.CultureName).ToList();

            IEnumerable<CultureInfo> cultures =
                from cul in DataLocalizationFacade.WhiteListedLocales
                where alreadyAddedCultureNames.Contains(cul.Name) == false
                orderby cul.DisplayName
                select cul;

            Dictionary<string, string> culturesDictionary = cultures.ToDictionary(f => f.Name, DataLocalizationFacade.GetCultureTitle);

            this.Bindings.Add("CultureName", "");
            this.Bindings.Add("RegionLanguageList", culturesDictionary);
            this.Bindings.Add("UrlMappingName", "");
            this.Bindings.Add("AccessToAllUsers", true);
        }



        private void finalizeCodeActivity_Finalize_ExecuteCode(object sender, EventArgs e)
        {
            string cultureName = this.GetBinding<string>("CultureName");
            string urlMappingName = this.GetBinding<string>("UrlMappingName");
            bool accessToAllUsers = this.GetBinding<bool>("AccessToAllUsers");

            LocalizationFacade.AddLocale(cultureName, urlMappingName, accessToAllUsers);    

            this.CloseCurrentView();

            ConsoleMessageQueueFacade.Enqueue(new CollapseAndRefreshConsoleMessageQueueItem(), null);
            ConsoleMessageQueueFacade.Enqueue(new BroadcastMessageQueueItem { Name = "LocalesUpdated", Value = "" }, null);


            SpecificTreeRefresher specificTreeRefresher = this.CreateSpecificTreeRefresher();
            specificTreeRefresher.PostRefreshMesseges(this.EntityToken);
        }



        private void updateRulMappingNameCodeActivity_Update_ExecuteCode(object sender, EventArgs e)
        {
            string urlMappingName = GetDefaultUrlMappingNameFromCultureName( this.GetBinding<string>("CultureName") );

            this.UpdateBinding("UrlMappingName", urlMappingName);
        }



        private string GetDefaultUrlMappingNameFromCultureName(string cultureName)
        {
            string urlMappingName = cultureName;
            if (urlMappingName.Contains("-"))
            {
                urlMappingName = urlMappingName.Substring(0, urlMappingName.IndexOf("-"));
            }
            return urlMappingName;
        }



        private void codeActivity_ShowBalloon_ExecuteCode(object sender, EventArgs e)
        {
            this.ShowFieldMessage("UrlMappingName", StringResourceSystemFacade.GetString("Composite.Plugins.LocalizationElementProvider", "AddSystemLocaleWorkflow.UrlMappingName.InUseMessage"));
        }
    }
}
