/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.IO;
using System.Linq;
using Composite.AspNet.Security;
using Composite.C1Console.Actions;
using Composite.C1Console.Elements.Foundation.PluginFacades;
using Composite.C1Console.Workflow;
using Composite.C1Console.Workflow.Activities;
using Composite.Core.Configuration;
using Composite.Core.IO;
using Composite.Functions;
using Composite.Functions.Foundation.PluginFacades;
using Composite.Functions.Plugins.FunctionProvider;
using Composite.Functions.Plugins.FunctionProvider.Runtime;
using Composite.Plugins.Elements.ElementProviders.BaseFunctionProviderElementProvider;
using Composite.Plugins.Functions.FunctionProviders.FileBasedFunctionProvider;
using BaseFunctionElementProvider = Composite.Plugins.Elements.ElementProviders.BaseFunctionProviderElementProvider.BaseFunctionProviderElementProvider;

namespace Composite.Plugins.Elements.ElementProviders.Common
{
    /// <summary>    
    /// </summary>
    /// <exclude />
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)] 
    public class BaseFunctionWorkflow : FormsWorkflow
    {
        /// <summary>
        /// Refreshed function tree for the currently used function provider element provider.
        /// </summary>
        public void RefreshFunctionTree()
        {
            WorkflowActionToken actionToken = (WorkflowActionToken)this.ActionToken;

            var entityToken = new BaseFunctionFolderElementEntityToken(actionToken.Payload, string.Empty);

            SpecificTreeRefresher specificTreeRefresher = this.CreateSpecificTreeRefresher();
            specificTreeRefresher.PostRefreshMesseges(entityToken);
        }

        public T GetFunctionProvider<T>() where T: class, IFunctionProvider
        {
            var functionFolderEntityToken = (this.EntityToken as BaseFunctionFolderElementEntityToken);
            if (functionFolderEntityToken != null)
            {
                var elementProvider = ElementProviderPluginFacade.GetElementProvider(functionFolderEntityToken.ElementProviderName) as BaseFunctionElementProvider;
                if (elementProvider != null)
                {
                    var provider = FunctionProviderPluginFacade.GetFunctionProvider(elementProvider.FunctionProviderName);

                    if (provider is T)
                    {
                        return provider as T;
                    }
                }
            }

            var functionProviderSettings = ConfigurationServices.ConfigurationSource.GetSection(FunctionProviderSettings.SectionName) 
                                           as FunctionProviderSettings;

            var providerNames = functionProviderSettings.FunctionProviderPlugins.Select(plugin => plugin.Name);

            foreach (string providerName in providerNames)
            {
                var provider = FunctionProviderPluginFacade.GetFunctionProvider(providerName);
                if (provider is T)
                {
                    return provider as T;
                }
            }

            throw new InvalidOperationException("Failed to get instance of " + typeof(T).Name);
        }

        internal void GetProviderAndFunction<FunctionType>(
            FileBasedFunctionEntityToken entityToken,
            out FileBasedFunctionProvider<FunctionType> provider,
            out FileBasedFunction<FunctionType> function) where FunctionType : FileBasedFunction<FunctionType>
        {
            string functionProviderName = entityToken.FunctionProviderName;

            provider = (FileBasedFunctionProvider<FunctionType>)FunctionProviderPluginFacade.GetFunctionProvider(functionProviderName);
            IFunction func = FunctionFacade.GetFunction(entityToken.FunctionName);

            Verify.IsNotNull(func, "Failed to get function '{0}'", entityToken.FunctionName);

            if (func is FunctionWrapper)
            {
                func = (func as FunctionWrapper).InnerFunction;
            }

            function = (FileBasedFunction<FunctionType>) func;
        }

        internal static void DeleteEmptyAncestorFolders(string filePath)
        {
            string folder = Path.GetDirectoryName(filePath);

            while (!C1Directory.GetFiles(folder).Any() && !C1Directory.GetDirectories(folder).Any())
            {
                C1Directory.Delete(folder);

                folder = folder.Substring(0, folder.LastIndexOf('\\'));
            }
        }
    }
}

