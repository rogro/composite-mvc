/*
 * The contents of this web application are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this web application except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/.
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the License.
 * 
 * The Original Code is owned by and the Initial Developer of the Original Code is 
 * Composite A/S (Danish business reg.no. 21744409). All Rights Reserved
 * 
 * Section 11 of the License is EXPRESSLY amended to include a provision stating 
 * that any dispute, including but not limited to disputes related to the enforcement 
 * of the License, to which Composite A/S as owner of the Original Code, as Initial 
 * Developer or in any other role, becomes a part to shall be governed by Danish law 
 * and be initiated before the Copenhagen City Court ("K�benhavns Byret")            
 */

using System;
using System.Collections.Generic;
using Composite.Core.Extensions;
using Composite.Core.IO;
using Composite.Data;
using Composite.Data.Types;

namespace Composite.Plugins.Elements.ElementProviders.Common
{
    internal class PageTemplateHelper
    {
        public static string LoadDefaultTemplateFile(string fileName)
        {
            return C1File.ReadAllText(PathUtil.Resolve("~/Composite/templates/PageTemplates/" + fileName))
                  .Replace("    ", "\t");
        }

        /// <summary>
        /// Replaces html escape sequences with their XML alternative.
        /// Fixes casing in DOCTYPE declaration
        /// </summary>
        public static string FixHtmlEscapeSequences(string xhtml)
        {
            return xhtml
                .Replace("&nbsp;", "&#160;", StringComparison.OrdinalIgnoreCase)
                .Replace("&ldquo;", "“", StringComparison.OrdinalIgnoreCase)
                .Replace("&rdguo;", "”", StringComparison.OrdinalIgnoreCase)
                .Replace("&lsquo;", "‘", StringComparison.OrdinalIgnoreCase)
                .Replace("&rsquo;", "’", StringComparison.OrdinalIgnoreCase)
                .Replace("&laquo;", "«", StringComparison.OrdinalIgnoreCase)
                .Replace("&raquo;", "»", StringComparison.OrdinalIgnoreCase)
                .Replace("&lsaquo;", "‹", StringComparison.OrdinalIgnoreCase)
                .Replace("&rsaquo;", "›", StringComparison.OrdinalIgnoreCase)
                .Replace("&bull;", "•", StringComparison.OrdinalIgnoreCase)
                .Replace("&deg;", "°", StringComparison.OrdinalIgnoreCase)
                .Replace("&hellip;", "…", StringComparison.OrdinalIgnoreCase)
                .Replace("&trade;", "™", StringComparison.OrdinalIgnoreCase)
                .Replace("&copy;", "©", StringComparison.OrdinalIgnoreCase)
                .Replace("&reg;", "®", StringComparison.OrdinalIgnoreCase)
                .Replace("&mdash;", "—", StringComparison.OrdinalIgnoreCase)
                .Replace("&ndash;", "–", StringComparison.OrdinalIgnoreCase)
                .Replace("&sup2;", "²", StringComparison.OrdinalIgnoreCase)
                .Replace("&sup3;", "³", StringComparison.OrdinalIgnoreCase)
                .Replace("&frac14;", "¼", StringComparison.OrdinalIgnoreCase)
                .Replace("&frac12;", "½", StringComparison.OrdinalIgnoreCase)
                .Replace("&frac34;", "¾", StringComparison.OrdinalIgnoreCase)
				.Replace("&times;", "×", StringComparison.OrdinalIgnoreCase)
				.Replace("&larr;", "←", StringComparison.OrdinalIgnoreCase)
				.Replace("&rarr;", "→", StringComparison.OrdinalIgnoreCase)
				.Replace("&uarr;", "↑", StringComparison.OrdinalIgnoreCase)
				.Replace("&darr;", "↓", StringComparison.OrdinalIgnoreCase)
			    .Replace("&middot;", "·", StringComparison.OrdinalIgnoreCase)
				.Replace("<!doctype", "<!DOCTYPE", StringComparison.OrdinalIgnoreCase);
        }

        public static Guid GetTheMostUsedTemplate(IEnumerable<Guid> templateIds)
        {
            IDictionary<Guid, int> usageStats = TemplateUsageStatictic();
            Guid mostUsed = Guid.Empty;
            int usages = 0;

            foreach (Guid templateId in templateIds)
            {
                if (usageStats.ContainsKey(templateId)
                    && usages < usageStats[templateId])
                {
                    mostUsed = templateId;
                    usages = usageStats[templateId];
                }
                else if(usages == 0 && mostUsed == Guid.Empty)
                {
                    mostUsed = templateId;
                }
            }

            return mostUsed;
        }

        private static IDictionary<Guid, int> TemplateUsageStatictic()
        {
            var result = new Dictionary<Guid, int>();
            foreach (IPage page in DataFacade.GetData<IPage>())
            {
                Guid templateId = page.TemplateId;

                if (!result.ContainsKey(templateId))
                {
                    result.Add(templateId, 1);
                }
                else
                {
                    result[templateId] = result[templateId] + 1;
                }
            }
            return result;
        }
    }
}
